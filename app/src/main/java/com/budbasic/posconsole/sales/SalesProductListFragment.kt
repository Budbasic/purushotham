package com.budbasic.posconsole.sales

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.StrictMode
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v4.widget.CircularProgressDrawable
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.text.Editable
import android.text.InputFilter
import android.text.TextWatcher
import android.util.DisplayMetrics
import android.util.Log
import android.view.*
import android.view.inputmethod.EditorInfo
import android.widget.*
import com.budbasic.customer.global.Preferences
import com.budbasic.global.CUC
import com.budbasic.posconsole.R
import com.budbasic.posconsole.global.DigitsInputFilter
import com.budbasic.posconsole.global.FilterData
import com.budbasic.posconsole.global.KeyboardUtils
import com.budbasic.posconsole.model.APIClass
import com.budbasic.posconsole.webservice.RequetsInterface
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DecodeFormat
import com.bumptech.glide.request.RequestOptions
import com.google.gson.Gson
import com.kotlindemo.model.*
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.sales_product_fragment.view.*
import kotlin.math.nextUp

@SuppressLint("ValidFragment")
class SalesProductListFragment(passedContext: Context) : Fragment(), SalesCategoryActivity.OnAboutDataReceivedListener, APIClass.onCallListner {
    override fun onFailed(requestCode: Int) {
    }

    override fun onSuccess(requestCode: Int) {
        if (requestCode == EMPLOYEEPATIENTADDTOCART) {
            callEmployeePatientAddtocart()
        } else if (requestCode == EMPLOYEESCANPRODUCTS) {
            callEmployeeScanProducts()
        }
    }

    val EMPLOYEEPATIENTADDTOCART = 1
    val EMPLOYEESCANPRODUCTS = 2

    override fun onDataReceived(result: String) {
        Log.i("ScanBarCode", "$result")
        product_tag = result
        callEmployeeScanProducts()
    }

    lateinit var root: View
    private var productList = ArrayList<GetSalesProduct>()
    private lateinit var productAdapter: ProductAdapter
    val gson = Gson()
    lateinit var dialogProductQty: Dialog

    var productQytValue = 1f
    var qty = ""
    var package_id = ""
    var packetList = ArrayList<GetProductPackets>()
    var getCategory: ArrayList<GetCategory> = ArrayList<GetCategory>()
    lateinit var mPacketAdapter: PacketAdapter

    lateinit var mDialog: Dialog
    lateinit var logindata: GetEmployeeLogin
    lateinit var mAppBasic: GetEmployeeAppSetting
    lateinit var edtSearch: EditText

    var product_id = ""
    var category_id = ""

    var product_tag = ""

    // var height=0
    //   var width=0
    companion object {
        val ARG_LIST_TYPE = "LIST_TYPE"
        var imagePath = ""
        var queue_patient_id = ""
        var queue_id = ""
        //var position: Int = 0
        var Currency_value = ""
        var height = 0
        var width = 0

        fun newInstance(listType: GetCategory, position: Int, imagePath: String, queue_patient_id: String, queue_id: String, Currency_value: String, context: Context): SalesProductListFragment {
            this.imagePath = imagePath
            this.queue_patient_id = queue_patient_id
            this.queue_id = queue_id
            this.Currency_value = Currency_value
            val fragment = SalesProductListFragment(context)
            Log.i(SalesProductListFragment::class.java.simpleName, "listType : $listType")
            val args = Bundle()
            val toJson = Gson().toJson(listType) as String
            args.putString(ARG_LIST_TYPE, toJson)
            fragment.arguments = args
            //this.position = position

            return fragment
        }
    }

    var apiClass: APIClass? = null
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        if (view != null)
            root = view!!
        else
            root = inflater.inflate(R.layout.sales_product_fragment, container, false)

        apiClass = APIClass(context!!, this)
        val listType = this.arguments!!.getString(ARG_LIST_TYPE)
        mAppBasic = Gson().fromJson(Preferences.getPreference(activity!!, "AppSetting"), GetEmployeeAppSetting::class.java)
        logindata = Gson().fromJson(Preferences.getPreference(activity!!, "logindata"), GetEmployeeLogin::class.java)
        val cateDetails = gson.fromJson(listType, GetCategory::class.java)
      //  productList = cateDetails.product!!
        for(i in productList.indices){
            productList.get(i).category_name=cateDetails.category_name

        }
      //  Log.d("PSASASAS",""+cateDetails)
//        val category=GetCategory("","","","","",cateDetails.category_name,
//                cateDetails.p_map_category_type,"","","","",null)
//        getCategory.add(category)
        edtSearch = root!!.findViewById(R.id.edtSearch)
        val displayMetrics = DisplayMetrics()
        activity!!.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics)
        height = displayMetrics!!.heightPixels
        width = (displayMetrics!!.widthPixels / 2) / 2
        //   Log.d("heasas",""+height)
        //  Log.d("asasa",""+width+"  "+(width/2)/2)

        /* CUC.setFilterdata(object : FilterData {
             override fun onfilterData(s: String?) {
                 filterData(s!!)
                 Log.d("FilterDataResponse", s)
             }

         }, position)*/
        init()
        return root
    }

    var mActivity: SalesCategoryActivity? = null
    private fun init() {
        if (productList != null && productList.size > 0) {
            root.rl_empty_view.visibility = View.GONE
        } else {
            root.rl_empty_view.visibility = View.VISIBLE
        }

        root.rv_product_list!!.layoutManager = GridLayoutManager(context, 2) as RecyclerView.LayoutManager?

        edtSearch.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(target: Editable?) {
                filterData(target.toString())
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

        })

        productAdapter = ProductAdapter(productList, this!!.context!!, object : onClickListener {
            override fun callClick(data: GetSalesProduct) {
//                val data = Gson().toJson(data)
//                val goIntent = Intent(context, SalesProductDetails::class.java)
//                goIntent.putExtra("data", "$data")
//                goIntent.putExtra("imagePath", "$imagePath")
//                goIntent.putExtra("queue_patient_id", "$queue_patient_id")
//                goIntent.putExtra("queue_id", "$queue_id")
//                startActivity(goIntent)
            }


            override fun onAddToCart(data: GetSalesProduct) {
                callEmployeeProductQty(data)
                //DisplayProductQtyDialog(data)
            }

        })

        root.rv_product_list!!.adapter = productAdapter
        productAdapter.filterData("")

        mActivity = (activity as SalesCategoryActivity?)!!
        mActivity!!.setAboutDataListener(this)
    }

    class ProductAdapter(val list: ArrayList<GetSalesProduct>, val context: Context, val onClick: onClickListener) : RecyclerView.Adapter<ProductAdapter.ViewHolder>() {

        var tempListData = ArrayList<GetSalesProduct>()


        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            holder!!.bindItems(tempListData[position], context, onClick)

        }

        override fun getItemCount(): Int {
            return tempListData.size
        }

        fun filterData(filterValue: String) {
            tempListData.clear()

            if (filterValue.isEmpty()) {
                tempListData.addAll(list)
                notifyDataSetChanged()
                return
            }

            list.forEach { item ->
                if (item.product_name.toLowerCase().contains(filterValue.toLowerCase())) {
                    tempListData.add(item)
                }

            }
            notifyDataSetChanged()


        }


        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            val v = LayoutInflater.from(parent!!.context).inflate(R.layout.product_items, parent, false)



            return ViewHolder(v)
        }

        class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
            val title: TextView = itemView.findViewById(R.id.tv_title)
            val img_1: ImageView = itemView.findViewById(R.id.iv_item)
            val item_load: ProgressBar = itemView.findViewById(R.id.item_load)
            val ll_item: RelativeLayout = itemView.findViewById(R.id.ll_item)
            val tv_pro_price: TextView = itemView.findViewById(R.id.tv_pro_price)
            val tv_pro_category:TextView=itemView.findViewById(R.id.tv_pro_category)
            val tv_tottal_qty: TextView = itemView.findViewById(R.id.tv_tottal_qty)
            val tv_addtocart: TextView = itemView.findViewById(R.id.tv_addtocart)
            val noOfAmountProduct: EditText = itemView.findViewById(R.id.noOfAmountProduct)
            val ll_main: LinearLayout = itemView.findViewById(R.id.ll_main)

            fun bindItems(data: GetSalesProduct, mContext: Context, onClick: onClickListener) {
                val layoutParams = LinearLayout.LayoutParams(width, width)
                ll_main.layoutParams = layoutParams
                ll_item.setOnClickListener {
                    onClick.callClick(data)
                }
                tv_addtocart.setOnClickListener {
                    onClick.onAddToCart(data)
                }
                title.text = data.p_map_store_name
                //Log.d("CategoryName======",""+data.category_name)
                tv_pro_category.text=data.category_name

                if (data.p_map_base_price != null && data.p_map_base_price != "0" && data.p_map_base_price != "0.0" && data.p_map_base_price != "0.00") {
                    tv_pro_price.setText(CUC.xxx("", "$Currency_value${data.p_map_base_price}",
                            ContextCompat.getColor(mContext, R.color.black),
                            ContextCompat.getColor(mContext, R.color.blue)
                    ), TextView.BufferType.SPANNABLE)
                } else {
                    tv_pro_price.setText(CUC.xxx("", "$Currency_value${data.product_price}",
                            ContextCompat.getColor(mContext, R.color.black),
                            ContextCompat.getColor(mContext, R.color.blue)), TextView.BufferType.SPANNABLE)
                }

                if (data.p_map_qty == "" || data.p_map_qty == null || data.p_map_qty.toFloat() <= 0) {
                    tv_tottal_qty.text = "Unavailable"
                    tv_tottal_qty.setTextColor(ContextCompat.getColor(mContext, R.color.red))
                } else {
                    tv_tottal_qty.text = "${data.p_map_qty} ${data.unit_short_name}"
                }
//                val listPro: ArrayList<ImageList> = data.imgData!!
                if (data.product_images != null && data.product_images != "") {
                    val circularProgressDrawable = CircularProgressDrawable(mContext)
                    circularProgressDrawable.strokeWidth = 5f
                    circularProgressDrawable.centerRadius = 30f
                    circularProgressDrawable.start()
                    Glide.with(mContext)
                            .applyDefaultRequestOptions(RequestOptions().format(DecodeFormat.PREFER_ARGB_8888).error(R.drawable.no_image).placeholder(circularProgressDrawable))
                            .load("$imagePath${data.product_images}")
                            .into(img_1)
                }
                //img_1.setImageResource(data.product_display_image)
            }
        }
    }

    interface onClickListener {
        fun callClick(data: GetSalesProduct)
        fun onAddToCart(data: GetSalesProduct)
    }


    fun filterData(filterValue: String) {
        if (::productAdapter.isInitialized) {
            productAdapter.filterData(filterValue)
        }
    }

    fun DisplayProductQtyDialog(productData: GetSalesProduct) {
        package_id = ""
        product_id = ""
        category_id = ""

        var countDeci = 0
        try {
            countDeci = "${productData.product_default_qty}".split("\\.".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()[1].length
        } catch (e: Exception) {
            e.printStackTrace()
        }

        val strForm = "%.${countDeci}f"
        println("CountFormat -  countDeci : $countDeci")
        productQytValue = "${productData.product_default_qty}".toFloat()
        println("CountFormat -  productQytValue : $productQytValue")
        val incriVal = "${productData.product_qty}".toFloat()
        println("CountFormat -  incriVal : $incriVal")

//        val view = layoutInflater.inflate(R.layout.product_qty_dialog, null)
//        dialogChangePin = BottomSheetDialog(activity!!)
//        dialogChangePin.setContentView(view)
        dialogProductQty = Dialog(activity, R.style.TransparentProgressDialog)
        val policy = StrictMode.ThreadPolicy.Builder().permitAll().build()
        StrictMode.setThreadPolicy(policy)
        //dialog_changepass.window.setLayout(resources.getDimensionPixelSize(R.dimen._10sdp) ,ViewGroup.LayoutParams.WRAP_CONTENT)
        dialogProductQty.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialogProductQty.setContentView(R.layout.product_qty_dialog_new)
        dialogProductQty.window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)
        dialogProductQty.show()

        val tv_unit_name = dialogProductQty.findViewById<TextView>(R.id.tv_unit_name)!!
        val tv_product_name = dialogProductQty.findViewById<TextView>(R.id.tv_product_name)!!
        val tv_tottal_qty = dialogProductQty.findViewById<TextView>(R.id.tv_tottal_qty)!!
        val tv_first_text = dialogProductQty.findViewById<EditText>(R.id.tv_first_text)!!
        val ll_add_cart = dialogProductQty.findViewById<LinearLayout>(R.id.ll_add_cart)!!
        val rl_first_minus = dialogProductQty.findViewById<RelativeLayout>(R.id.rl_first_minus)!!
        val rl_first_plus = dialogProductQty.findViewById<RelativeLayout>(R.id.rl_first_plus)
        val tv_packet_title = dialogProductQty.findViewById<TextView>(R.id.tv_packet_title)!!
        val rv_packets = dialogProductQty.findViewById<RecyclerView>(R.id.rv_packets)
        val tv_tottal_price = dialogProductQty.findViewById<TextView>(R.id.tv_tottal_price)

        tv_tottal_price.text = "$ " + "${productData.product_price}"
        tv_first_text!!.filters = arrayOf<InputFilter>(DigitsInputFilter(5, countDeci, Double.MAX_VALUE))
        tv_first_text.setText("${productData.product_default_qty}")
        tv_unit_name.text = "${productData.product_unit_name} "
        tv_product_name.text = "${productData.product_name}"
        tv_tottal_qty.text = "${productData.p_map_qty} ${productData.unit_short_name}"
        rv_packets!!.layoutManager = LinearLayoutManager(activity!!, LinearLayoutManager.HORIZONTAL, false)
        packetList.clear()
       // packetList.add(productData.product_packets)
        for (data in packetList) {
            if (data.packet_qty == tv_first_text.text.toString()) {
                package_id = data.packet_id
                data.isSelected = true
            } else {
                data.isSelected = false
            }
        }
        val mClickListner = object : ClickListner {
            override fun onClick(getProductPackets: GetProductPackets) {
                if (getProductPackets.isSelected) {
                    for (data in packetList) {
                        data.isSelected = false
                    }
                    productQytValue = "${productData.product_default_qty}".toFloat()
                    package_id = ""
                    tv_first_text.setText(String.format(strForm, productQytValue.nextUp()))
                } else {
                    for (data in packetList) {
                        if (data.packet_id == getProductPackets.packet_id) {
                            if (productData.p_map_qty != "" && productData.p_map_qty != null) {
                                if (productData.p_map_qty.toFloat() >= data.packet_qty.toFloat()) {
                                    productQytValue = data.packet_qty.toFloat()
                                    tv_first_text.setText(String.format(strForm, productQytValue.nextUp()))
                                    println("CountFormat -  plus : ${String.format(strForm, productQytValue.nextUp())}")
                                    //tv_first_text.setText(productQytValue.toString())
                                    data.isSelected = true
                                    package_id = data.packet_id
                                } else {
                                    data.isSelected = false
                                    package_id = ""
                                    CUC.displayToast(activity!!, "0", "You can't set ${productData.product_unit_name} more then ${productData.p_map_qty}")
                                }
                            } else {
                                data.isSelected = false
                                package_id = ""
                                CUC.displayToast(activity!!, "0", "You can't set ${productData.product_unit_name} more then ${productData.p_map_qty}")
                            }

                        } else {
                            data.isSelected = false
                            package_id = ""
                        }
                    }
                }
                mPacketAdapter.notifyDataSetChanged()
            }
        }

        mPacketAdapter = PacketAdapter(activity!!, packetList, productData.unit_short_name, Currency_value, mClickListner)
        rv_packets.adapter = mPacketAdapter
        mPacketAdapter.notifyDataSetChanged()
        if (packetList.isEmpty()) {
            tv_packet_title.visibility = View.GONE
        }

        if (productData.p_map_qty == "" || productData.p_map_qty == null || productData.p_map_qty.toFloat() <= 0) {
            ll_add_cart.isClickable = false
            ll_add_cart.isEnabled = false
            ll_add_cart.setBackgroundColor(ContextCompat.getColor(activity!!, R.color.gray))
            tv_tottal_qty.text = "Unavailable"
            tv_tottal_qty.setTextColor(ContextCompat.getColor(activity!!, R.color.red))
        }

        KeyboardUtils.addKeyboardToggleListener(activity!!, object : KeyboardUtils.SoftKeyboardToggleListener {
            override fun onToggleSoftKeyboard(isVisible: Boolean) {
                if (isVisible) {
                    println("CountFormat -  isVisible $isVisible")
                } else {
                    println("CountFormat -  isVisible $isVisible")
                }
            }
        })

        tv_first_text.setOnEditorActionListener(object : TextView.OnEditorActionListener {
            override fun onEditorAction(p0: TextView?, p1: Int, p2: KeyEvent?): Boolean {
                if (p1 == EditorInfo.IME_ACTION_DONE) {
                    if (tv_first_text.text.isEmpty() || tv_first_text.text.toString() == ".") {
                        tv_first_text.setText("${productData.product_default_qty}")
                    }
                    if (productData.p_map_qty != "" && productData.p_map_qty != null) {
                        if ("${productData.product_default_qty}".toFloat() <= tv_first_text.text.toString().toFloat()) {
                            if (productData.p_map_qty.toFloat() >= tv_first_text.text.toString().toFloat()) {
                                //StrictMath.nextUp()
                                productQytValue = tv_first_text.text.toString().toFloat()// + incriVal // Math.nextUp()
                                tv_first_text.setText(String.format(strForm, productQytValue.nextUp()))
                                println("CountFormat -  plus : ${String.format(strForm, productQytValue.nextUp())}")
                                var isSelect = false
                                var packetId = ""
//                                for (data in packetList) {
//                                    if (data.packet_qty == tv_first_text.text.toString()) {
//                                        packetId = data.packet_id
//                                        isSelect = true
//                                        break
//                                    }
//                                }
                                if (isSelect) {
                                    for (data in packetList) {
                                        data.isSelected = data.packet_id == packetId
                                    }
                                    package_id = packetId
                                } else {
                                    for (data in packetList) {
                                        data.isSelected = false
                                    }
                                    package_id = ""
                                }
                                mPacketAdapter.notifyDataSetChanged()
                            } else {
                                for (data in packetList) {
                                    data.isSelected = false
                                }
                                package_id = ""
                                tv_first_text.setText("${productData.product_default_qty}")
                                mPacketAdapter.notifyDataSetChanged()
                                CUC.displayToast(activity!!, "0", "You can't set ${productData.product_unit_name} more then ${productData.p_map_qty}")
                            }
                        } else {
                            for (data in packetList) {
                                data.isSelected = false
                            }
                            package_id = ""
                            tv_first_text.setText("${productData.product_default_qty}")
                            mPacketAdapter.notifyDataSetChanged()
                            CUC.displayToast(activity!!, "0", "You can't set ${productData.product_unit_name} less then ${productData.product_default_qty}")
                        }
                    } else {
                        for (data in packetList) {
                            data.isSelected = false
                        }
                        package_id = ""
                        tv_first_text.setText("${productData.product_default_qty}")
                        mPacketAdapter.notifyDataSetChanged()
                        CUC.displayToast(activity!!, "0", "You can't set ${productData.product_unit_name} more then ${productData.p_map_qty}")
                    }
                }
                return false
            }

        })
        rl_first_minus.setOnClickListener {
            if (tv_first_text.text.isEmpty() || tv_first_text.text.toString() == ".") {
                tv_first_text.setText("${productData.product_default_qty}")
            }
            if (tv_first_text.text.toString().toFloat() != "${productData.product_default_qty}".toFloat()) {
                productQytValue = tv_first_text.text.toString().toFloat() - incriVal // Math.nextUp()
                tv_first_text.setText(String.format(strForm, productQytValue.nextUp()))
                var isSelect: Boolean = false
                var packetId = ""
//                for (data in packetList) {
//                    if (data.packet_qty == tv_first_text.text.toString()) {
//                        packetId = data.packet_id
//                        isSelect = true
//                        break
//                    }
//                }
                if (isSelect) {
                    for (data in packetList) {
                        data.isSelected = data.packet_id == packetId
                    }
                    package_id = packetId
                } else {
                    for (data in packetList) {
                        data.isSelected = false
                    }
                    package_id = ""
                }
                mPacketAdapter.notifyDataSetChanged()
            } else {
                for (data in packetList) {
                    data.isSelected = false
                }
                package_id = ""
                CUC.displayToast(activity!!, "0", "You can't set ${productData.product_unit_name} less then ${productData.product_default_qty}")
            }
        }

        rl_first_plus!!.setOnClickListener {
            if (tv_first_text.text.isEmpty() || tv_first_text.text.toString() == ".") {
                tv_first_text.setText("${productData.product_default_qty}")
            }
            if (productData.p_map_qty != "" && productData.p_map_qty != null) {
                if (productData.p_map_qty.toFloat() > productQytValue) {
                    //StrictMath.nextUp()
                    productQytValue = tv_first_text.text.toString().toFloat() + incriVal // Math.nextUp()
                    tv_first_text.setText(String.format(strForm, productQytValue.nextUp()))
                    println("CountFormat -  plus : ${String.format(strForm, productQytValue.nextUp())}")
                    var isSelect = false
                    var packetId = ""
//                    for (data in packetList) {
//                        if (data.packet_qty == tv_first_text.text.toString()) {
//                            packetId = data.packet_id
//                            isSelect = true
//                            break
//                        }
//                    }
                    if (isSelect) {
                        for (data in packetList) {
                            data.isSelected = data.packet_id == packetId
                        }
                        package_id = packetId
                    } else {
                        for (data in packetList) {
                            data.isSelected = false
                        }
                        package_id = ""
                    }
                    mPacketAdapter.notifyDataSetChanged()
                } else {
                    CUC.displayToast(activity!!, "0", "You can't set ${productData.product_unit_name} more then ${productData.p_map_qty}")
                }
            } else {
                CUC.displayToast(activity!!, "0", "You can't set ${productData.product_unit_name} more then ${productData.p_map_qty}")
            }
        }

        ll_add_cart.setOnClickListener {
            if (tv_first_text.text.isEmpty() || tv_first_text.text.toString() == ".") {
                tv_first_text.setText("${productData.product_default_qty}")
            }
            if ("${tv_first_text.text}" != "") {
                if (productData.p_map_qty.toFloat() >= "${tv_first_text.text}".toFloat()) {
                    var isSelect = false
                    var packetId = ""
//                    for (data in packetList) {
//                        if (data.packet_qty.toFloat() == tv_first_text.text.toString().toFloat()) {
//                            packetId = data.packet_id
//                            isSelect = true
//                            break
//                        }
//                    }
//                    if (isSelect) {
//                        for (data in packetList) {
//                            data.isSelected = data.packet_id == packetId
//                        }
//                        package_id = packetId
//                    } else {
//                        for (data in packetList) {
//                            data.isSelected = false
//                        }
//                        package_id = ""
//                    }
                    mPacketAdapter.notifyDataSetChanged()

                    qty = "${tv_first_text.text}"
                    product_id = productData.product_id
                    category_id = productData.p_map_store_category_id
                    callEmployeePatientAddtocart()
                } else {
                    package_id = ""
                    tv_first_text.setText(String.format(strForm, "${productData.product_default_qty}".toFloat().nextUp()))
                    var isSelect = false
                    var packetId = ""
//                    for (data in packetList) {
//                        if (data.packet_qty == tv_first_text.text.toString()) {
//                            packetId = data.packet_id
//                            isSelect = true
//                            break
//                        }
//                    }
//                    if (isSelect) {
//                        for (data in packetList) {
//                            data.isSelected = data.packet_id == packetId
//                        }
//                        package_id = packetId
//                    } else {
//                        for (data in packetList) {
//                            data.isSelected = false
//                        }
//                        package_id = ""
//                    }
                    mPacketAdapter.notifyDataSetChanged()
                    CUC.displayToast(activity!!, "0", "You can't set ${productData.product_unit_name} more then ${productData.p_map_qty}")
                }
            } else {
                package_id = ""
                tv_first_text.setText(String.format(strForm, "${productData.product_default_qty}".toFloat().nextUp()))
                var isSelect = false
                var packetId = ""
//                for (data in packetList) {
//                    if (data.packet_qty == tv_first_text.text.toString()) {
//                        packetId = data.packet_id
//                        isSelect = true
//                        break
//                    }
//                }
//                if (isSelect) {
//                    for (data in packetList) {
//                        data.isSelected = data.packet_id == packetId
//                    }
//                    package_id = packetId
//                } else {
//                    for (data in packetList) {
//                        data.isSelected = false
//                    }
//                    package_id = ""
//                }
                mPacketAdapter.notifyDataSetChanged()
                CUC.displayToast(activity!!, "0", "At list ${productData.product_default_qty} ${productData.product_unit_name} compulsory")
            }
        }
        //dialogChangePin.show()
    }

    class PacketAdapter(val mContext: Context, val packetList: ArrayList<GetProductPackets>, val unit_name: String, val Currency_value: String, val mClickListner: ClickListner) : RecyclerView.Adapter<PacketAdapter.PacketHolder>() {
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PacketHolder {
            val mView = LayoutInflater.from(parent.context).inflate(R.layout.packet_item, parent, false)
            return PacketHolder(mView)
        }

        override fun getItemCount(): Int {
            return packetList.size
        }

        override fun onBindViewHolder(holder: PacketHolder, position: Int) {
            holder.bindVliew(mContext, packetList[position], unit_name, Currency_value, mClickListner)
        }

        class PacketHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
            val tv_packet_qty = itemView.findViewById<TextView>(R.id.tv_packet_qty)
            val tv_packet_price = itemView.findViewById<TextView>(R.id.tv_packet_price)
            val rl_packet_item = itemView.findViewById<RelativeLayout>(R.id.rl_packet_item)

            @SuppressLint("SetTextI18n")
            fun bindVliew(mContext: Context, getProductPackets: GetProductPackets, unit_name: String, Currency_value: String, mClickListner: ClickListner) {
                tv_packet_qty.text = "${getProductPackets.packet_qty}\n$unit_name"
                tv_packet_price.text = "$Currency_value${getProductPackets.packet_total_price}"
                if (getProductPackets.isSelected) {
                    tv_packet_qty.setTextColor(ContextCompat.getColor(mContext, R.color.white))
                    rl_packet_item.setBackgroundResource(R.drawable.packet_select_ring)
                } else {
                    tv_packet_qty.setTextColor(ContextCompat.getColor(mContext, R.color.black))
                    rl_packet_item.setBackgroundResource(R.drawable.packet_ring)
                }
                rl_packet_item.setOnClickListener {
                    mClickListner.onClick(getProductPackets)

                }
            }
        }
    }

    interface ClickListner {
        fun onClick(getProductPackets: GetProductPackets)
    }

    fun callEmployeePatientAddtocart() {
        if (CUC.isNetworkAvailablewithPopup(activity!!)) {
            mDialog = CUC.createDialog(activity!!)
            val apiService = RequetsInterface.Factory.create()
            val parameterMap = HashMap<String, String>()
            parameterMap["api_key"] = "${Preferences.getPreference(activity!!, "API_KEY")}"
            if (logindata != null) {
                parameterMap["store_id"] = logindata.user_store_id
                parameterMap["employee_id"] = logindata.user_id
            }
            parameterMap["patient_id"] = queue_patient_id
            parameterMap["product_id"] = product_id
            parameterMap["category_id"] = category_id
            parameterMap["qty"] = "$qty"
            parameterMap["package_id"] = "$package_id"
            parameterMap["terminal_id"] = "${mAppBasic.terminal_id}"
            parameterMap["emp_lat"] = "${Preferences.getPreference(activity!!, "latitude")}"
            parameterMap["emp_log"] = "${Preferences.getPreference(activity!!, "longitude")}"
            parameterMap["queue_id"] = "$queue_id"
            Log.i("Result", "parameterMap : $parameterMap")
            CompositeDisposable().add(apiService.callEmployeeSalesAddcart(parameterMap)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe({ result ->
                        if (mDialog != null && mDialog.isShowing)
                            mDialog.dismiss()
                        if (result != null) {
                            Log.i("Result", "" + result.toString())
                            if (result.status == "0") {
                                //Preferences.setPreference(this@EmployeeProductDetails, "total_qty", "${result.total_qty}")
                                val i = Intent("android.intent.action.MAINQTY")
                                activity!!.sendBroadcast(i)
                                val sendOrder = Intent("android.intent.action.MAINORD")
                                activity!!.sendBroadcast(sendOrder)
                                if (dialogProductQty != null && dialogProductQty.isShowing) {
                                    dialogProductQty.dismiss()
                                }
                                // CUC.displayToast(activity!!, result.show_status, result.message)
                            } else if (result.status == "10") {
                                apiClass!!.callApiKey(EMPLOYEEPATIENTADDTOCART)
                            } else {
                                CUC.displayToast(activity!!, result.show_status, result.message)
                            }

                        } else {
                            //  CUC.displayToast(activity!!, "0", getString(R.string.connection_to_database_failed))
                        }

                    }, { error ->
                        if (mDialog != null && mDialog.isShowing)
                            mDialog.dismiss()
                        CUC.displayToast(activity!!, "0", getString(R.string.connection_to_database_failed))
                        error.printStackTrace()
                    })
            )
        }
    }

    fun callEmployeeProductQty(productData: GetSalesProduct) {
        if (CUC.isNetworkAvailablewithPopup(activity!!)) {
            mDialog = CUC.createDialog(activity!!)
            val apiService = RequetsInterface.create()
            val parameterMap = HashMap<String, String>()
            parameterMap["api_key"] = "${Preferences.getPreference(activity!!, "API_KEY")}"
            if (logindata != null) {
                parameterMap["store_id"] = logindata.user_store_id
            }
            parameterMap["cart_detail_id"] = ""
            parameterMap["product_id"] = productData.product_id
            Log.i("Result", "parameterMap : $parameterMap")
            CompositeDisposable().add(apiService.callEmployeeProductQty(parameterMap)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe({ result ->
                        if (mDialog != null && mDialog.isShowing)
                            mDialog.dismiss()
                        if (result != null) {
                            Log.i("Result77", "" + result.toString())
                            if (result.status == "0") {
                                if (result.inventory_qty != "")
                                    productData.p_map_qty = result.inventory_qty

//                                if (productData.p_map_qty == "" || productData.p_map_qty == null || productData.p_map_qty == "0") {
//                                    tv_tottal_qty.text = "Unavailable"
//                                    tv_tottal_qty.setTextColor(ContextCompat.getColor(activity!!, R.color.red))
//                                    ll_add_to_cart.visibility = View.GONE
//                                } else {
//                                    tv_tottal_qty.text = "${productData.p_map_qty} ${productData.unit_short_name}"
//                                }

                                DisplayProductQtyDialog(productData)
                                CUC.displayToast(activity!!, result.show_status, result.message)
                            } else if (result.status == "10") {
                                //callApiKey()
                            } else {
                                CUC.displayToast(activity!!, result.show_status, result.message)
                            }

                        } else {
                            //  CUC.displayToast(activity!!, "0", getString(R.string.connection_to_database_failed))
                        }

                    }, { error ->
                        if (mDialog != null && mDialog.isShowing)
                            mDialog.dismiss()
                        CUC.displayToast(activity!!, "0", getString(R.string.connection_to_database_failed))
                        error.printStackTrace()
                    })
            )
        }
    }

    fun callEmployeeScanProducts() {
        if (CUC.isNetworkAvailablewithPopup(activity!!)) {
            mDialog = CUC.createDialog(activity!!)
            val apiService = RequetsInterface.Factory.create()
            val parameterMap = HashMap<String, String>()
            parameterMap["api_key"] = "${Preferences.getPreference(activity!!, "API_KEY")}"
            if (logindata != null) {
                parameterMap["store_id"] = logindata.user_store_id
                parameterMap["employee_id"] = logindata.user_id
            }
            parameterMap["product_tag"] = "$product_tag"
            Log.i("Result", "parameterMap : $parameterMap")
            CompositeDisposable().add(apiService.callEmployeeScanProducts(parameterMap)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe({ result ->
                        if (mDialog != null && mDialog.isShowing)
                            mDialog.dismiss()
                        if (result != null) {
                            Log.i("Result", "" + result.toString())
                            if (result.status == "0") {
                                product_tag = ""
                                //DisplayProductQtyDialog(result.product_block.data.category.product)
                                CUC.displayToast(activity!!, result.show_status, result.message)
                            } else if (result.status == "10") {
                                apiClass!!.callApiKey(EMPLOYEESCANPRODUCTS)
                            } else {
                                CUC.displayToast(activity!!, result.show_status, result.message)
                            }
                        } else {
                            //  CUC.displayToast(activity!!, "0", getString(R.string.connection_to_database_failed))
                        }
                    }, { error ->
                        if (mDialog != null && mDialog.isShowing)
                            mDialog.dismiss()
                        CUC.displayToast(activity!!, "0", getString(R.string.connection_to_database_failed))
                        error.printStackTrace()
                    })
            )
        }
    }


}