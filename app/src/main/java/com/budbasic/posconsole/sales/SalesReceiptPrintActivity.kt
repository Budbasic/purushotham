package com.budbasic.posconsole.sales

import android.annotation.SuppressLint
import android.app.Activity
import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.bluetooth.BluetoothSocket
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Bundle
import android.os.Handler
import android.os.Message
import android.support.v4.widget.CircularProgressDrawable
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.*
import android.widget.TextView
import android.widget.Toast
import com.budbasic.customer.global.Preferences
import com.budbasic.global.CUC
import com.budbasic.posconsole.R
import com.budbasic.posconsole.global.Apppreference
import com.budbasic.posconsole.global.RasterDocument
import com.budbasic.posconsole.global.StarBitmap
import com.budbasic.posconsole.printer.fragment.BluetoothScan
import com.budbasic.posconsole.printer.sdk.BluetoothService
import com.budbasic.posconsole.printer.sdk.Command
import com.budbasic.posconsole.reception.fragment.PrinterConnectionActivity
import com.budbasic.posconsole.webservice.RequetsInterface
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DecodeFormat
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.SimpleTarget
import com.bumptech.glide.request.transition.Transition
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.google.zxing.BarcodeFormat
import com.google.zxing.WriterException
import com.kotlindemo.model.*
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_receipt_print.*
import zj.com.customize.sdk.Other
import java.io.IOException
import java.io.InputStream
import java.io.OutputStream
import java.io.UnsupportedEncodingException
import java.text.DecimalFormat
import java.util.*

class SalesReceiptPrintActivity : Activity() {

    // Local Bluetooth adapter
    var printerstatusrecep: String? = null
    var printertyperecep: String? = null
    var coonecteddeviceadd: String? = null

    lateinit var data: GetEmployeeQenerateInvoice
    private var mBluetoothAdapter: BluetoothAdapter? = null
    // Member object for the services
    var mService: BluetoothService? = null
    var invoiceType: String? = null
    var mBitmap: Bitmap? = null
    // Intent request codes
    private val REQUEST_CONNECT_DEVICE = 1
    private val REQUEST_ENABLE_BT = 2

    // Message types sent from the BluetoothService Handler
    val MESSAGE_STATE_CHANGE = 1
    val MESSAGE_READ = 2
    val MESSAGE_WRITE = 3
    val MESSAGE_DEVICE_NAME = 4
    val MESSAGE_TOAST = 5
    val MESSAGE_CONNECTION_LOST = 6
    val MESSAGE_UNABLE_CONNECT = 7

    internal lateinit var mmSocket: BluetoothSocket
    internal lateinit var mmDevice: BluetoothDevice
    internal lateinit var mmOutputStream: OutputStream
    internal lateinit var mmInputStream: InputStream
    internal lateinit var workerThread: Thread
    internal lateinit var readBuffer: ByteArray
    internal var readBufferPosition: Int = 0
    @Volatile
    internal var stopWorker: Boolean = false

    private val TAG = "Main_Activity"
    private val DEBUG = true

    // Key names received from the BluetoothService Handler
    val DEVICE_NAME = "device_name"
    val TOAST = "toast"
    // Name of the connected device
    private val CHINESE = "GBK"

    private var mConnectedDeviceName: String? = null
    var Currency_value = ""
    lateinit var logindata: GetEmployeeLogin
    lateinit var appSettingData: GetEmployeeAppSetting

    val df: DecimalFormat = DecimalFormat("0.00")

    var store_id = ""
    var patient_id = ""
    var mainCartId = ""
    var invoice_taxes = ""
    var invoice_total_qty = ""
    var invoice_sub_total = ""
    var invoice_total_payble = ""
    var loyalty_amount = "0.00"
    var order_type = ""
    //var all_data = ""
    var invoiceStr = ""

    lateinit var appreference:Apppreference
    //lateinit var invoiceData: GetStoreOrders
    lateinit var result: GetEmployeeQenerateInvoice

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN)
        setContentView(R.layout.activity_receipt_print)
        Log.d("kjjhjj1", "-1.00.99")
        Log.i("kjjhjj1", "-1.00")
        appreference= Apppreference(this@SalesReceiptPrintActivity)
        if (intent != null) {
            if (intent.hasExtra("order_type"))
                order_type = "${intent.getStringExtra("order_type")}"
//            if (intent.hasExtra("all_data")) {
//                all_data = intent.getStringExtra("all_data")
//                if (all_data.isNotEmpty())
//                    invoiceData = Gson().fromJson(all_data, GetStoreOrders::class.java)
//            }
            if (intent.hasExtra("invoiceStr")) {
                invoiceStr = intent.getStringExtra("invoiceStr")
                //if (all_data.isNotEmpty())
                result = Gson().fromJson(invoiceStr, GetEmployeeQenerateInvoice::class.java)
            }
        }
        appSettingData = Gson().fromJson(Preferences.getPreference(this@SalesReceiptPrintActivity, "AppSetting"), GetEmployeeAppSetting::class.java)
        logindata = Gson().fromJson(Preferences.getPreference(this@SalesReceiptPrintActivity, "logindata"), GetEmployeeLogin::class.java)
        Currency_value = "${appSettingData.Currency_value}"
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter()
        init()

    }

    fun init() {

        try {
            findBT()
            openBT()
        } catch (ex: IOException) {
            ex.printStackTrace()
        }

        if (result != null) {
            store_id = "${result!!.store_data!!.store_id}"
            patient_id = "${result!!.invoice_data!!.invoice_patient_id}"
            mainCartId = "${result!!.invoice_data!!.invoice_id}"
            invoice_taxes = "${result!!.invoice_data!!.invoice_taxes}"
            invoice_total_qty = "${result!!.invoice_data!!.invoice_total_qty}"
            invoice_sub_total = "${result!!.invoice_data!!.invoice_sub_total}"
            loyalty_amount = "${result!!.invoice_data!!.invoice_loyalty_redeem}"
            invoice_total_payble = "${result!!.invoice_data!!.invoice_grand_total}"
        }
        // If the adapter is null, then Bluetooth is not supported
        if (mBluetoothAdapter == null) {
            Toast.makeText(this@SalesReceiptPrintActivity, "Bluetooth is not available", Toast.LENGTH_LONG).show()
            //finish()
        }
        showReceipt(order_type, result)
    }

    override fun onResume() {
        super.onResume()
        if (mService != null) {
            if (mService?.state == BluetoothService.STATE_NONE) {
                // Start the Bluetooth services
                mService?.start()
            }
        }
        startPrinter()
    }

    override fun onPause() {
        super.onPause()
        if (mService != null)
            mService!!.stop()
    }

    fun startPrinter() {
        try {
            if (!mBluetoothAdapter!!.isEnabled) run {
                val enableIntent = Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE)
                startActivityForResult(enableIntent, REQUEST_ENABLE_BT)
                // Otherwise, setup the session
            } else {
                if (mService == null) {
                    mService = BluetoothService(this@SalesReceiptPrintActivity, mHandler)

                    mConnectedDeviceName = ""
                   // val address = Preferences.getPreference(this@SalesReceiptPrintActivity, DEVICE_NAME)
                    val address = appreference.printerName
                    Log.i("AddQueueActivity", "address : $address")
                    if (address != null && address.isNotEmpty()) {
                        //val mDevice = mBluetoothAdapter!!.getRemoteDevice(address)
                        //BluetoothDevice mDevice= (BluetoothDevice)address
                        println("aaaaaaaaa   printer  iff "+mmDevice)
                       // Log.i("AddQueueActivity", "mDevice : " + mDevice.address)
                       // Log.i("AddQueueActivity", "mDevice : " + mDevice.name)
                       // Log.i("AddQueueActivity", "mDevice : " + mDevice.uuids)
                        mService!!.connect(mmDevice)
                    }
                } else {
                    mConnectedDeviceName = ""
                  //  val address = Preferences.getPreference(this@SalesReceiptPrintActivity, DEVICE_NAME)
                    val address = appreference.printerName
                    Log.i("AddQueueActivity", "address : $address")
                    if (address != null && address.isNotEmpty()) {
                       // val mDevice = mBluetoothAdapter!!.getRemoteDevice(address)
                        //BluetoothDevice mDevice= (BluetoothDevice)address
                       // Log.i("AddQueueActivity", "mDevice : " + mDevice.address)
                        //Log.i("AddQueueActivity", "mDevice : " + mDevice.name)
                     //   Log.i("AddQueueActivity", "mDevice : " + mDevice.uuids)
                        mService!!.connect(mmDevice)
                    }
                }
            }
        } catch (ee: Exception) {
            ee.printStackTrace()
        }
    }

    val mHandler = @SuppressLint("HandlerLeak")
    object : Handler() {
        override fun handleMessage(msg: Message?) {
            if (msg != null) {
                when (msg.what) {
                    MESSAGE_STATE_CHANGE -> {
                        if (DEBUG)
                            Log.i(TAG, "MESSAGE_STATE_CHANGE: " + msg.arg1)
                        when (msg.arg1) {
                            BluetoothService.STATE_CONNECTED -> {
                            }
                            BluetoothService.STATE_CONNECTING -> {
                                //mTitle.setText(R.string.title_connecting)
                            }
                            BluetoothService.STATE_LISTEN, BluetoothService.STATE_NONE -> {
                                //mTitle.setText(R.string.title_not_connected)
                            }
                        }
                    }
                    MESSAGE_WRITE -> {
                    }
                    MESSAGE_READ -> {
                    }
                    MESSAGE_DEVICE_NAME -> {
                        // save the connected device's name
                        mConnectedDeviceName = msg.data.getString(DEVICE_NAME)
                        Toast.makeText(applicationContext, "Connected to $mConnectedDeviceName", Toast.LENGTH_SHORT).show()
                    }
                    MESSAGE_TOAST -> {
                        Toast.makeText(applicationContext, msg.data.getString(TOAST), Toast.LENGTH_SHORT).show()
                    }
                    MESSAGE_CONNECTION_LOST    //蓝牙已断开连接
                    -> {
                        mService = null
                        startPrinter()
                        Toast.makeText(applicationContext, "Device connection was lost", Toast.LENGTH_SHORT).show()
                    }
                    MESSAGE_UNABLE_CONNECT     //无法连接设备
                    -> {
                        //onStart()
                        Toast.makeText(applicationContext, "Unable to connect device", Toast.LENGTH_SHORT).show()
                    }
                }
            }

        }
    }

    /*
     *SendDataByte
     */
    private fun SendDataByte(data: ByteArray) {
        if (mService!!.state != BluetoothService.STATE_CONNECTED) {
            Toast.makeText(this@SalesReceiptPrintActivity, R.string.not_connected, Toast.LENGTH_SHORT)
                    .show()
            return
        }
        mService?.write(data)
    }

    private fun SendDataString(data: String) {
        Log.i("BTPWRITE", data)
        if (mService!!.getState() !== BluetoothService.STATE_CONNECTED) {
            Toast.makeText(this@SalesReceiptPrintActivity, getText(R.string.not_connected), Toast.LENGTH_SHORT)
                    .show()
            return
        }
        if (data.length > 0) {
            try {
                Log.i("BTPWRITE", data)
                mService!!.write(data.toByteArray(charset("GBK")))
            } catch (e: UnsupportedEncodingException) {
                e.printStackTrace()
            }

        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        Log.i("Result", "Main onActivityResult(), $requestCode : $resultCode")
        //fragment.onActivityResult(requestCode, resultCode, data)
        if (DEBUG)
            Log.d(TAG, "onActivityResult $resultCode")
        when (requestCode) {
            2121 -> {
                //mService = null
                //startPrinter()
            }
            REQUEST_CONNECT_DEVICE -> {
                // When DeviceListActivity returns with a device to connect
                if (resultCode == Activity.RESULT_OK) {
                    // Get the device MAC address
                    val address = data?.getExtras()!!.getString(
                            BluetoothScan.EXTRA_DEVICE_ADDRESS)
                    // Get the BLuetoothDevice object
                    if (BluetoothAdapter.checkBluetoothAddress(address)) {
                        val device = mBluetoothAdapter!!.getRemoteDevice(address)
                        coonecteddeviceadd = device.name + " " + device.address
                        // Attempt to connect to the device
                        mService?.connect(device)
                    }
                }
            }
            REQUEST_ENABLE_BT -> {
                // When the request to enable Bluetooth returns
                if (resultCode == Activity.RESULT_OK) {
                    if (mService == null)
                        mService = BluetoothService(this@SalesReceiptPrintActivity, mHandler)
                    // Bluetooth is now enabled, so set up a session
                } else {
                    // User did not enable Bluetooth or an error occured
                    Log.d(TAG, "BT not enabled")
                    Toast.makeText(this@SalesReceiptPrintActivity, R.string.bt_not_enabled_leaving, Toast.LENGTH_SHORT).show()
                    //finish()
                }
            }
        }
    }

    /*
    * invoiceType
    * 0 = Generate Invoice
    * 1 = Payment Invoice
    * */
    fun showReceipt(invoiceType: String, data: GetEmployeeQenerateInvoice) {

        txtsetting.setOnClickListener {

            val myJson = Gson().toJson(logindata)
            val goIntent = Intent(this@SalesReceiptPrintActivity, PrinterConnectionActivity::class.java)
            goIntent.putExtra("logindata",myJson)
            startActivityForResult(goIntent, 2121)

           // val goIntent = Intent(this@SalesReceiptPrintActivity, BluetoothScan::class.java)
           // startActivityForResult(goIntent, 2121)
        }



        txtaddress.text = "${data.store_data.store_name}"
        var receiptDate = "-"
        if (data.invoice_data.invoice_created_date != null && data.invoice_data.invoice_created_date != "") {
            receiptDate = CUC.getdatefromoneformattoAnother("yyyy-MM-dd HH:mm:ss", "MM-dd-yyyy", "${data.invoice_data.invoice_created_date}")
        }
        txtdate.text = "${getString(R.string.lbl_date_colon)} $receiptDate"
        txttelphno.text = "${getString(R.string.lbl_tel_colon)} ${data.store_data.store_contact}"
        //txtcustomer.text = "${getString(R.string.lbl_customer_name_colon)} ${data.patient_data.patient_fname} ${data.patient_data.patient_lname}"
        txtcustomer.text = " ${data.patient_data.patient_fname} ${data.patient_data.patient_lname}"
        txtinvoice.text = "${receiptDate}"

        if (invoiceType == "2") {
            var serve_type = "-"
            if (data.invoice_data.invoice_serve_type == "2") {
                serve_type = "${getString(R.string.lbl_delivery)}"
            } else {
                serve_type = "${getString(R.string.lbl_pickup)}"
            }
            txtdispatchtype.text = "${getString(R.string.lbl_dispatch_type_colon)} $serve_type"
            txtpaymenttype.text = "${getString(R.string.lbl_payment_type_colon)} ${data.payment_data.payment_method}"
        } else {
            txtpaymenttype.text = "${getString(R.string.lbl_payment_type_colon)} ${data.payment_data.payment_method}"
        }
        txtitem.text = "${getString(R.string.lbl_item_capital)}"
        txtqty.text = "${getString(R.string.lbl_unit_capital)}"
        txtprice.text = "${getString(R.string.lbl_price_capital)}($Currency_value)"
        txtamt.text = "${getString(R.string.lbl_amt_capital)}($Currency_value)"

        txttotalunit.text = "$invoice_total_qty"
        txtsubtotal.text = "$Currency_value$invoice_sub_total"
        try {
            if ("${data.invoice_data.invoice_manual_discount}" != null && "${data.invoice_data.invoice_manual_discount}" != "") {
                if ("${data.invoice_data.invoice_manual_discount}".toFloat() > 0) {
                    if ("${data.invoice_data.invoice_manual_discount_type}" == "2") {
                        txtloyaltyamount.text = "${data.invoice_data.invoice_manual_discount}%"
                        ll_loyaltyamount.visibility = View.VISIBLE
                        //tempTaxList.add(GetTaxData("2", "${resources.getString(R.string.lbl_discount)}", "${data.invoice_data.invoice_manual_discount}", "2"))
                    } else {
                        txtloyaltyamount.text = "$Currency_value${data.invoice_data.invoice_manual_discount}"
                        ll_loyaltyamount.visibility = View.VISIBLE
                        //tempTaxList.add(GetTaxData("2", "${resources.getString(R.string.lbl_discount)}", "${data.invoice_data.invoice_manual_discount}", "1"))
                    }
                }
            }
        } catch (e: Exception) {
            ll_loyaltyamount.visibility = View.GONE
            e.printStackTrace()
        }
        try {
            if (loyalty_amount != null && loyalty_amount != "") {
                if ("$loyalty_amount".toFloat() > 0) {
                    txtLoyaltyAmount.text = "$Currency_value$loyalty_amount"
                    llLoyaltyAmount.visibility = View.VISIBLE
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
        if (data.invoice_data.invoice_promo_code != "") {
            ll_promo_code.visibility = View.VISIBLE
            txtpromoname.text = "PromoCode(${data.invoice_data.invoice_promo_code})"
            txtpromocode.text = "$Currency_value${data.invoice_data.invoice_promo_discount}"
        }

        txtgrandtotal.text = "$Currency_value${data.invoice_data.invoice_total_payble}"

        tv_paymentMethodName.text="${data.payment_data.payment_method}"
        txtamountpaid.text = "$Currency_value${data.invoice_data.invoice_paid_amount}"
        //txttendercash.text = "$Currency_value${data.invoice_data.invoice_change}"
        txtChangeDue.text = "$Currency_value${data.invoice_data.invoice_change}"
        testingData.text = "$Currency_value${data.invoice_data.invoice_change}"
        val istream = assets.open("tvlogo.bmp")
        var mBitmap = BitmapFactory.decodeStream(istream)
        try {
            if (appSettingData.receiptlogo != null && appSettingData.receiptlogo != "") {
                val circularProgressDrawable = CircularProgressDrawable(this@SalesReceiptPrintActivity)
                circularProgressDrawable.strokeWidth = 5f
                circularProgressDrawable.centerRadius = 30f
                circularProgressDrawable.start()

                Glide.with(this@SalesReceiptPrintActivity)
                        .applyDefaultRequestOptions(RequestOptions().error(R.drawable.no_image_available).placeholder(circularProgressDrawable))
                        .asBitmap()
                        .load("${appSettingData.receiptlogo}")
                        .into(object : SimpleTarget<Bitmap>() {
                            override fun onResourceReady(resource: Bitmap, transition: Transition<in Bitmap>?) {
                                mBitmap = resource
                                imglogo.setImageBitmap(mBitmap)
                            }
                        })
            }
        } catch (e: Exception) {
            e.printStackTrace()
            mBitmap = BitmapFactory.decodeStream(istream)
            imglogo.setImageBitmap(mBitmap)
        }
//        if (appSettingData.receiptlogo.isNotEmpty()) {
//            try {
//                val url = URL(appSettingData.receiptlogo)
//                val connection = url.openConnection() as HttpURLConnection
//                connection.doInput = true
//                connection.connect()
//                val input = connection.inputStream
//                mBitmap = BitmapFactory.decodeStream(input)
//                imglogo.setImageBitmap(mBitmap)
//            } catch (e: IOException) {
//                e.printStackTrace()
//                mBitmap = BitmapFactory.decodeStream(istream)
//                imglogo.setImageBitmap(mBitmap)
//            }
//        } else {
//            mBitmap = BitmapFactory.decodeStream(istream)
//            imglogo.setImageBitmap(mBitmap)
//        }

        try {
            val bitmap = CUC.encodeAsBitmap("${data.invoice_data.invoice_order_unique_no}", BarcodeFormat.QR_CODE, 600, 600)
            imgbarcode.setImageBitmap(bitmap)
        } catch (e: WriterException) {
            e.printStackTrace()
        }

        recycleitemlist!!.layoutManager = LinearLayoutManager(this@SalesReceiptPrintActivity)
        val receiptItemAdapter = ReceiptItemAdapter(data.invoice_details, Currency_value)
        recycleitemlist!!.adapter = receiptItemAdapter
        var tempTaxList: ArrayList<GetTaxData> = ArrayList()
        if (invoice_taxes != null && invoice_taxes.isNotEmpty()) {
            tempTaxList = Gson().fromJson(invoice_taxes,
                    object : TypeToken<ArrayList<GetTaxData>>() {

                    }.type) as ArrayList<GetTaxData>
            dot6.visibility = View.VISIBLE
        } else {
            dot6.visibility = View.GONE
        }

        rvtextlist!!.layoutManager = LinearLayoutManager(this@SalesReceiptPrintActivity)
        val receiptTaxItemAdapter = ReceiptTaxItemAdapter(tempTaxList, Currency_value)
        rvtextlist!!.adapter = receiptTaxItemAdapter

        tv_cancel.setOnClickListener {
            setResult(Activity.RESULT_OK)
            finish()
        }

        txt_print.setOnClickListener {
            try {
                if (mBluetoothAdapter!!.isEnabled) {
                    if (mService != null) {
                        if (mService!!.state != BluetoothService.STATE_CONNECTED) {
                           // callEmployeeTerminalPrinter(invoiceType, data, mBitmap)
                            println("aaaaaaaaaaaaa   iffff   ")
                            //   callEmployeeTerminalPrinter(invoiceType,data,mBitmap)
                            Toast.makeText(this@SalesReceiptPrintActivity, getText(R.string.not_connected), Toast.LENGTH_SHORT).show()
//                            setResult(Activity.RESULT_OK)
//                            finish()

                        } else {
                            callEmployeeTerminalPrinter(invoiceType, data, mBitmap)
                            //put comment
                            //   sendPrintReceipt(invoiceType, data, mBitmap)
                        }
                    }
                } else {
                    println("aaaaaaaaaaaaa   elseee   ")
                    Toast.makeText(this@SalesReceiptPrintActivity, getText(R.string.not_connected), Toast.LENGTH_SHORT).show()
//                    setResult(Activity.RESULT_OK)
//                    finish()
                }
            } catch (e: Exception) {
                println("aaaaaaaaaaaaa   errror   "+e.message)
                e.printStackTrace()
                Toast.makeText(this@SalesReceiptPrintActivity, getText(R.string.not_connected), Toast.LENGTH_SHORT).show()
//                setResult(Activity.RESULT_OK)
//                finish()
            }
        }
    }


    override fun onBackPressed() {
        //super.onBackPressed()
        setResult(Activity.RESULT_OK)
        finish()
    }

    fun callEmployeeTerminalPrinter(invoiceType: String, data: GetEmployeeQenerateInvoice, mBitmap: Bitmap) {
        val apiService = RequetsInterface.create()
        val parameterMap = HashMap<String, String>()

        if (logindata != null) {
            parameterMap["api_key"] = "${Preferences.getPreference(this@SalesReceiptPrintActivity, "API_KEY")}"
            parameterMap["store_id"] = logindata!!.user_store_id
            parameterMap["emp_id"] = logindata!!.user_id
        }

        if (appSettingData != null) {
            //  parameterMap["store_id"]="${}"
            parameterMap["terminal_id"] = "${appSettingData!!.terminal_id}"
        }
        Log.i("Result", "parameterMapprinternew : $parameterMap")

        CompositeDisposable().add(apiService.callEmployeeTerminalPrinter(parameterMap)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe({ result ->
                    Log.i("kjjhjj1", "1" + result.toString())

                    //   printerdata=  Gson().fromJson(Preferences.getPreference(applicationContext, "printerdata"),GetTerminalPrinter::class.java)
                    if (result.show_status == "1") {
                        Log.i("kjjhjj1", "1.1")
                        if (result.get_printers != null && result.get_printers.size > 0) {
                            for (i in result.get_printers.indices) {
                                result.get_printers.get(i)

                                //if (result.get_printers.get(i).hardware_manufacturer!!.equals(coonecteddeviceadd)) {
                                if (result.get_printers[i].hardware_status!!.equals("1", false)) {
                                    if (result.get_printers[i].hardware_type_id!!.equals("1", false)) {
                                        printerstatusrecep = result.get_printers!![0].hardware_status.toString()
                                        printertyperecep = result.get_printers!![0].hardware_type_id.toString()
                                        //  Log.i("kjjhjj1", "3")
                                    }
                                }
                                //}


                            }
                            // Log.i("kjjhjj1", "2")


                            // Toast.makeText(this@BackRoomReceiptPrintActivity,printertyperecep+printerstatusrecep,Toast.LENGTH_LONG).show()

                            Log.i("printerstatustype", printerstatusrecep + "  " + printertyperecep)
                            //  Toast.makeText(this@ProductSearch,"statutype"+printerstatus+printertype,Toast.LENGTH_LONG).show()
                            //   sendPrintReceipt(invoiceType:String)
                        }

                        //recepiet printer
                        Log.i("pnamedevice", mConnectedDeviceName)
                        // Log.i("pname selectin",printername+ "mService"+mService.DEVICE_NAME);
                        //   Log.i("condition",printerstatusrecep.equals("1") +""+ printertyperecep.equals("1"))
                        if (printerstatusrecep.equals("1") && printertyperecep.equals("1")) {
                            Toast.makeText(this@SalesReceiptPrintActivity, "Find Recepit  Printer Scessfully", Toast.LENGTH_LONG).show()
                            Log.i("pnamedevice", mConnectedDeviceName)

                            //code for print
                            sendPrintReceipt(invoiceType!!, data!!, mBitmap!!)

                        } else {
                            Toast.makeText(this@SalesReceiptPrintActivity, "Find Other Recepit Printer", Toast.LENGTH_LONG).show()
                        }
                        /*
                        * [END BarCode]
                        * */
                    } else {

                    }
                    //   CUC.displayToast(this,"printetres",result.toString())

                }, { error ->
                    error.printStackTrace()
                })
        )

    }

    fun sendPrintReceipt(invoiceType: String, data: GetEmployeeQenerateInvoice, mBitmap: Bitmap) = if (mBluetoothAdapter != null) {
        if (mBluetoothAdapter!!.isEnabled) {
            if (mService != null) {
                if (mService!!.getState() !== BluetoothService.STATE_CONNECTED) {
                    Toast.makeText(this@SalesReceiptPrintActivity, getText(R.string.not_connected), Toast.LENGTH_SHORT).show()
                    this@SalesReceiptPrintActivity.setResult(Activity.RESULT_OK)
                    finish()
                } else {
                    val nMode = 0
                    val nPaperWidth = 576
                   // val nPaperWidth = 700
                    val mCommands: ArrayList<ByteArray> = ArrayList()

                    val rasterDoc = RasterDocument(RasterDocument.RasSpeed.Medium, RasterDocument.RasPageEndMode.None,
                            RasterDocument.RasPageEndMode.None, RasterDocument.RasTopMargin.Small, 0, 0, 0)
                    rlLogos.isDrawingCacheEnabled =true
                    rlLogos.buildDrawingCache()

                    var newBitmap  = rlLogos.getDrawingCache()
// val starbitmap = StarBitmap(mBitmap, false, nPaperWidth)
                    val starbitmap = StarBitmap(newBitmap, false, nPaperWidth)
                    Command.alignmentCommand[3] = 49
                    SendDataByte(Command.alignmentCommand)
                    mCommands.add(rasterDoc.BeginDocumentCommandData())
                    mCommands.add(starbitmap.getImageRasterDataForPrinting_Standard(true))
                    mCommands.add(rasterDoc.EndDocumentCommandData())
                    for (data in mCommands) {
                        SendDataByte(data)
                    }

                    SendDataString("\n")
                    SendDataByte(Command.alignmentCommand)
                    SendDataByte(Command.bold_off)
                    SendDataString("${data.store_data.store_name}\n" +
                            "${data.store_data.store_contact}\n\n")

                    Command.alignmentCommand[3] = 48
                    SendDataByte(Command.alignmentCommand)
                    SendDataString("------------------------------------------------\n")

                    Command.alignmentCommand[3] = 48
                    SendDataByte(Command.alignmentCommand)

                    var receiptDate = "-"
                    if (data.invoice_data.invoice_created_date != null && data.invoice_data.invoice_created_date != "") {
                        //receiptDate = CUC.getdatefromoneformattoAnother("yyyy-MM-dd HH:mm:ss", "MM-dd-yyyy HH:mm:ss", "${data.invoice_data.invoice_created_date}")
                        receiptDate = CUC.getdatefromoneformattoAnother("yyyy-MM-dd HH:mm:ss", "MM-dd-yyyy", "${data.invoice_data.invoice_created_date}")
                    }

                    if (invoiceType == "2") {
                        var serve_type = "-"
                        if (data.invoice_data.invoice_serve_type == "2") {
                            serve_type = "${getString(R.string.lbl_delivery)}"
                        } else {
                            serve_type = "${getString(R.string.lbl_pickup)}"
                        }

                        SendDataString("${data.patient_data.patient_fname} ${data.patient_data.patient_lname}\n")
                        SendDataByte(Other.byteArraysToBytes(arrayOf(String.format("%-37s %-10s", "${getString(R.string.lbl_invoice_date)}",
                                RightAlignText("${receiptDate}\n")).toByteArray(charset("GBK")))))
                        SendDataString("------------------------------------------------\n")

//                        SendDataString("${getString(R.string.lbl_date_colon)} $receiptDate\n"
//                                + "${getString(R.string.lbl_customer_name_colon)} ${data.patient_data.patient_fname} ${data.patient_data.patient_lname}\n"
//                                + "${getString(R.string.lbl_invoice_no)} ${data.invoice_data.invoice_no}\n"
//                                + "${getString(R.string.lbl_dispatch_type_colon)} $serve_type\n"
//                                + "${getString(R.string.lbl_payment_type_colon)} ${data.payment_data.payment_method}\n"
//                                + "------------------------------------------------")
                    } else {

                        SendDataString("${data.patient_data.patient_fname} ${data.patient_data.patient_lname}\n")
                        SendDataByte(Other.byteArraysToBytes(arrayOf(String.format("%-37s %-10s", "${getString(R.string.lbl_invoice_date)}",
                                RightAlignText("${receiptDate}\n")).toByteArray(charset("GBK")))))
                        SendDataString("------------------------------------------------\n")


//                        SendDataString("${getString(R.string.lbl_date_colon)} $receiptDate\n"
//                                + "${getString(R.string.lbl_customer_name_colon)} ${data.patient_data.patient_fname} ${data.patient_data.patient_lname}\n"
//                                + "${getString(R.string.lbl_invoice_no)} ${data.invoice_data.invoice_no}\n"
//                                + "${getString(R.string.lbl_payment_type_colon)} ${data.payment_data.payment_method}\n"
//                                + "------------------------------------------------")
                    }
//                    val allbuf: Array<ByteArray>
//                    try {
//                        allbuf = arrayOf(String.format("%-22s %-7s %-9s %-6s \n", "${getString(R.string.lbl_item_capital)}", "${getString(R.string.lbl_unit_capital)}", "${getString(R.string.lbl_price_capital)}($Currency_value)", "${getString(R.string.lbl_amt_capital)}($Currency_value)").toByteArray(charset("GBK")))
//                        val buf = Other.byteArraysToBytes(allbuf)
//                        SendDataByte(buf)
//                    } catch (e: UnsupportedEncodingException) {
//                        e.printStackTrace()
//                    }

                    //  SendDataString("------------------------------------------------")
                    for (i in 0 until data.invoice_details.size) {

                        val mCartItemDataSet = data.invoice_details[i]
                        //mCartItemDataSet.product_name += " You may use this barcode generator"
                        val mStringlength = mCartItemDataSet.product_name.length
                        var prodouctdata: Array<ByteArray>

                        Command.alignmentCommand[3] = 48
                        SendDataByte(Command.alignmentCommand)

                        try {
                            if (mStringlength > 19) {
                                prodouctdata = arrayOf(String.format("%-18s %-9s %-9s %-9s", mCartItemDataSet.product_name.substring(0, 23),
                                        AmtRightAlignText("${mCartItemDataSet.invoice_detail_qty} ${mCartItemDataSet.unit_short_name}"),
                                        AmtRightAlignText(""),
                                        AmtRightAlignText("$Currency_value${df.format((mCartItemDataSet.invoice_detail_total_price).toDouble())}")).toByteArray(charset("GBK")))
                                val buf = Other.byteArraysToBytes(prodouctdata)
                                SendDataByte(buf)
                            } else {
                                prodouctdata = arrayOf(String.format("%-18s %-9s %-9s %-9s", mCartItemDataSet.product_name.substring(0),
                                        AmtRightAlignText("${mCartItemDataSet.invoice_detail_qty} ${mCartItemDataSet.unit_short_name}"),
                                        AmtRightAlignText(""),
                                        AmtRightAlignText("$Currency_value${df.format((mCartItemDataSet.invoice_detail_total_price).toDouble())}")).toByteArray(charset("GBK")))
                                val buf = Other.byteArraysToBytes(prodouctdata)
                                SendDataByte(buf)
                            }

                            var start = 19
                            var end = 46
                            if (mStringlength > 19) {
                                while (start < mStringlength) {
                                    if (mStringlength > end) {
                                        prodouctdata = arrayOf(String.format("%-18s %-9s %-9s %-9s", mCartItemDataSet.product_name.substring(start, end),
                                                "", "", "").toByteArray(charset("GBK")))
                                        val buf1 = Other.byteArraysToBytes(prodouctdata)
                                        SendDataByte(buf1)
                                        start = start + 19
                                        end = end + 19

                                    } else {
                                        prodouctdata = arrayOf(String.format("%-18s %-9s %-9s %-9s", mCartItemDataSet.product_name.substring(start),
                                                "", "", "").toByteArray(charset("GBK")))
                                        val buf1 = Other.byteArraysToBytes(prodouctdata)
                                        SendDataByte(buf1)
                                        start = start + 19
                                    }
                                }
                            }
                        } catch (e: UnsupportedEncodingException) {
                            e.printStackTrace()
                        }
                    }
//total _price start \n add 4-1-2019
                    SendDataString("\n------------------------------------------------\n")
                    var SubTotal: Array<ByteArray>
                    SubTotal = arrayOf(String.format("%-11s %-11s %-12s %-11s", "${getString(R.string.lbl_total_unit)}",
                            QtyRightAlignText("$invoice_total_qty"),
                            RightAlignText("${getString(R.string.lbl_subtotal)}"),
                            RightAlignText("$Currency_value$invoice_sub_total")).toByteArray(charset("GBK")))

                    val amttender = Other.byteArraysToBytes(SubTotal)


                    SendDataByte(amttender)
                    SendDataString("------------------------------------------------\n")

                    try {
                        var amountdata: Array<ByteArray>
                        var tempTaxList: ArrayList<GetTaxData> = ArrayList()
                        if (invoice_taxes != null && invoice_taxes.isNotEmpty()) {
                            tempTaxList = Gson().fromJson(invoice_taxes,
                                    object : TypeToken<ArrayList<GetTaxData>>() {

                                    }.type) as ArrayList<GetTaxData>
                        }
                        Command.alignmentCommand[3] = 48
                        SendDataByte(Command.alignmentCommand)
                        if (tempTaxList.size > 0) {
                            for (taxs in tempTaxList) {
                                amountdata = arrayOf(String.format("%-37s %-10s", "${taxs.tax_name}", RightAlignText("$Currency_value${taxs.tax_total}")).toByteArray(charset("GBK")))
                                val article1 = Other.byteArraysToBytes(amountdata)
                                SendDataByte(article1)
                                SendDataString("\n------------------------------------------------\n")
                            }
                            //  SendDataString("------------------------------------------------\n")
                        } else {
                        }
                        Command.alignmentCommand[3] = 48
                        SendDataByte(Command.alignmentCommand)
                        SendDataByte(Command.bold_off)
                        SendDataByte(Other.byteArraysToBytes(arrayOf(String.format("%-37s %-10s", "${getString(R.string.lbl_grand_total)}",
                                RightAlignText("$Currency_value${data.invoice_data.invoice_total_payble}\n")).toByteArray(charset("GBK")))))
                        SendDataString("------------------------------------------------\n")
                        val cash ="${data.payment_data.payment_method}"
                        val discount = "Discount: "
//                        try {
//                            if (loyalty_amount != null && loyalty_amount != "") {
//                                if ("$loyalty_amount".toFloat() > 0) {
//                                    Command.alignmentCommand[3] = 48
//                                    SendDataByte(Command.alignmentCommand)
//                                    SendDataByte(Other.byteArraysToBytes(arrayOf(String.format("%-37s %-10s", "${getString(R.string.lbl_loyalty_amount)}",
//                                            RightAlignText("-$Currency_value$loyalty_amount")).toByteArray(charset("GBK")))))
//                                }
//                            }else{
//
//                            }
//                        } catch (e: Exception) {
//                            e.printStackTrace()
//                        }



                        if (data.invoice_data.invoice_promo_code != "") {
                            println("aaaaaaaaa   iffff111 "+discount+" "+Currency_value+data.invoice_data.invoice_manual_discount)
                            Command.alignmentCommand[3] = 48
                            SendDataByte(Command.alignmentCommand)
                            Log.d("AAAA","AAAA")
//                            SendDataByte(Other.byteArraysToBytes(arrayOf(String.format("%-37s %-10s", "Promo Code (${data.invoice_data.invoice_promo_code})",
//                                    RightAlignText("$Currency_value${data.invoice_data.invoice_promo_discount}")).toByteArray(charset("GBK")))))
                            SendDataByte(Other.byteArraysToBytes(arrayOf(String.format("%-26s %-10s %-11s", "Amount Paid ",RightAlignText("$cash"),
                                    RightAlignText("$Currency_value${data.invoice_data.invoice_paid_amount}\n")).toByteArray(charset("GBK")))))
                            SendDataByte(Other.byteArraysToBytes(arrayOf(String.format("%-26s %-9s %-11s", "",RightAlignText("$discount"),
                                    RightAlignText("$Currency_value${data.invoice_data.invoice_manual_discount + data.invoice_data.invoice_promo_discount}\n")).toByteArray(charset("GBK")))))
                            SendDataString("-----------------------------------------------\n")
                        } else {
                            Log.d("AAAA11","AAAA")
                            if ("${data.invoice_data.invoice_manual_discount}" != null && "${data.invoice_data.invoice_manual_discount}" != "") {
                                if ("${data.invoice_data.invoice_manual_discount}".toFloat() > 0) {
                                    if ("${data.invoice_data.invoice_manual_discount_type}" == "2") {
                                        Log.d("AAAA22","AAAA")
                                        println("aaaaaaaaa   iffff "+discount+" "+Currency_value+data.invoice_data.invoice_manual_discount)
                                        Command.alignmentCommand[3] = 48
                                        SendDataByte(Command.alignmentCommand)
                                        SendDataByte(Other.byteArraysToBytes(arrayOf(String.format("%-26s %-10s %-11s", "Amount Paid ",RightAlignText("$cash"),
                                                RightAlignText("$Currency_value${data.invoice_data.invoice_paid_amount}\n")).toByteArray(charset("GBK")))))
                                        SendDataByte(Other.byteArraysToBytes(arrayOf(String.format("%-26s %-9s %-11s", "",RightAlignText("$discount"),
                                                RightAlignText("$Currency_value${data.invoice_data.invoice_manual_discount}\n")).toByteArray(charset("GBK")))))
                                        SendDataString("-----------------------------------------------\n")
                                    } else {
                                        println("aaaaaaaaa   else111  "+discount+"  0.00")
                                        Log.d("AAAA33","AAAA")
                                        Command.alignmentCommand[3] = 48
                                        SendDataByte(Command.alignmentCommand)
                                        SendDataByte(Other.byteArraysToBytes(arrayOf(String.format("%-26s %-10s %-11s", "Amount Paid ",RightAlignText("$cash"),
                                                RightAlignText("$Currency_value${data.invoice_data.invoice_paid_amount}\n")).toByteArray(charset("GBK")))))
                                        SendDataByte(Other.byteArraysToBytes(arrayOf(String.format("%-26s %-9s %-11s", " ",RightAlignText("$discount"),
                                                RightAlignText("$0.00\n")).toByteArray(charset("GBK")))))
                                        SendDataString("-----------------------------------------------\n")
                                    }
                                } else {
                                    println("aaaaaaaaa   else  "+discount+"  0.00")
                                    Log.d("AAAA44","AAAA")
                                    Command.alignmentCommand[3] = 48
                                    SendDataByte(Command.alignmentCommand)
                                    SendDataByte(Other.byteArraysToBytes(arrayOf(String.format("%-26s %-10s %-11s", "Amount Paid ",RightAlignText("$cash"),
                                            RightAlignText("$Currency_value${data.invoice_data.invoice_paid_amount}\n")).toByteArray(charset("GBK")))))
                                    SendDataByte(Other.byteArraysToBytes(arrayOf(String.format("%-26s %-9s %-11s", " ",RightAlignText("$discount"),
                                            RightAlignText("$0.00\n")).toByteArray(charset("GBK")))))
                                    SendDataString("-----------------------------------------------\n")
                                }
                            } else {
                                println("aaaaaaaaa   "+discount+"  0.00")
                                Log.d("AAAA55","AAAA")
                                Command.alignmentCommand[3] = 48
                                SendDataByte(Command.alignmentCommand)
                                SendDataByte(Other.byteArraysToBytes(arrayOf(String.format("%-26s %-10s %-11s", "Amount Paid ",RightAlignText("$cash"),
                                        RightAlignText("$Currency_value${data.invoice_data.invoice_paid_amount}\n")).toByteArray(charset("GBK")))))
                                SendDataByte(Other.byteArraysToBytes(arrayOf(String.format("%-26s %-9s %-11s", " ",RightAlignText("$discount"),
                                        RightAlignText("$0.00\n")).toByteArray(charset("GBK")))))
                                SendDataString("-----------------------------------------------\n")
                            }
                        }
//                        Command.alignmentCommand[3] = 48
//                        SendDataByte(Command.alignmentCommand)
//                        SendDataByte(Other.byteArraysToBytes(arrayOf(String.format("%-37s %-10s", "Amount Paid ",
//                                RightAlignText("$cash$Currency_value${data.invoice_data.invoice_paid_amount}\n")+
//                                        RightAlignText("$discount$Currency_value${data.invoice_data.invoice_manual_discount}\n")).toByteArray(charset("GBK")))))
//                        SendDataString("------------------------------------------------\n")

//                        try {
//                            if ("${data.invoice_data.invoice_manual_discount}" != null && "${data.invoice_data.invoice_manual_discount}" != "") {
//                                if ("${data.invoice_data.invoice_manual_discount}".toFloat() > 0) {
//                                    if ("${data.invoice_data.invoice_manual_discount_type}" == "2") {
//                                        Command.alignmentCommand[3] = 48
//                                        SendDataByte(Command.alignmentCommand)
//                                        SendDataByte(Other.byteArraysToBytes(arrayOf(String.format("%-37s %-10s", "${resources.getString(R.string.lbl_discount)}",
//                                                RightAlignText("${data.invoice_data.invoice_manual_discount}%")).toByteArray(charset("GBK")))))
//                                    } else {
//                                        Command.alignmentCommand[3] = 48
//                                        SendDataByte(Command.alignmentCommand)
//                                        SendDataByte(Other.byteArraysToBytes(arrayOf(String.format("%-37s %-10s", "${resources.getString(R.string.lbl_discount)}",
//                                                RightAlignText("-$Currency_value${data.invoice_data.invoice_manual_discount}")).toByteArray(charset("GBK")))))
//                                    }
//                                }
//                            }
//                        } catch (e: Exception) {
//                            e.printStackTrace()
//                        }


                        Command.alignmentCommand[3] = 48
                        SendDataByte(Command.alignmentCommand)
                        SendDataByte(Other.byteArraysToBytes(arrayOf(String.format("%-37s %-10s", "Change Due",
                                RightAlignText("$Currency_value${data.invoice_data.invoice_change}")).toByteArray(charset("GBK")))))
                        SendDataByte(Command.bold_off)
                        SendDataString("------------------------------------------------\n")

                        Command.alignmentCommand[3] = 49
                        SendDataByte(Command.alignmentCommand)

                        SendDataString("\n")
                        Command.alignmentCommand[3] = 49
                        SendDataByte(Command.alignmentCommand)
                        SendDataString("${getString(R.string.lbl_recipet_thankyou)}")
                        SendDataString("\n\n\n\n\n")
                        SendDataByte(Command.ESC_d)
                        setResult(Activity.RESULT_OK)
                        try {
                            val OpenCashDrawer = ArrayList<ByteArray>()
                            OpenCashDrawer.add(byteArrayOf(7.toByte()))
                            for (Qrdata in OpenCashDrawer) {
                                SendDataByte(Qrdata)
                            }
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                        finish()
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            } else {
                setResult(Activity.RESULT_OK)
                finish()
            }
        } else {
//            val enableIntent = Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE)
//            startActivityForResult(enableIntent, REQUEST_ENABLE_BT)
        }
    } else {
        Toast.makeText(this@SalesReceiptPrintActivity, "Bluetooth is not available", Toast.LENGTH_LONG).show()
    }

     internal fun findBT() {

        try {
            mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter()



            if (mBluetoothAdapter == null) {
                println("aaaaaaaaaaa    no blooth connected")
            }

            if (mBluetoothAdapter!!.isEnabled()) {
                val enableBluetooth = Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE)
                startActivityForResult(enableBluetooth, 0)
            }

            val pairedDevices = mBluetoothAdapter!!.getBondedDevices()

            if (pairedDevices.size > 0) {
                for (device in pairedDevices) {
                    println("aaaaaaaaaa address  " + device.address + "  name  " + device.name + "  uuid " + device.uuids)
                    // RPP300 is the name of the bluetooth printer device
                    // we got this name from the list of paired devices
                    if (device.name == appreference.printerName) {
                        mmDevice = device

                       // openBT()
                        println("aaaaaaaaaaa  ifff   equal")
                        break
                    }
                }
            }


        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    @Throws(IOException::class)
    internal fun openBT() {
        try {

            // Standard SerialPortService ID
            val uuid = UUID.fromString("00001101-0000-1000-8000-00805f9b34fb")
            mmSocket = mmDevice.createRfcommSocketToServiceRecord(uuid)
            mmSocket.connect()
            mmOutputStream = mmSocket.getOutputStream()
            mmInputStream = mmSocket.getInputStream()

            beginListenForData()

            println("aaaaaaaaaaaa  blooth opend ")

        } catch (e: Exception) {
            e.printStackTrace()
            println("aaaaaaaaaaaa  blooth not opend "+e.message)
        }

    }


    internal fun beginListenForData() {
        try {
            val handler = Handler()

            // this is the ASCII code for a newline character
            val delimiter: Byte = 10

            stopWorker = false
            readBufferPosition = 0
            readBuffer = ByteArray(1024)

            workerThread = Thread(Runnable {
                while (!Thread.currentThread().isInterrupted && !stopWorker) {

                    try {

                        val bytesAvailable = mmInputStream.available()

                        if (bytesAvailable > 0) {

                            val packetBytes = ByteArray(bytesAvailable)
                            mmInputStream.read(packetBytes)

                            for (i in 0 until bytesAvailable) {

                                val b = packetBytes[i]
                                if (b == delimiter) {

                                    val encodedBytes = ByteArray(readBufferPosition)
                                    System.arraycopy(
                                            readBuffer, 0,
                                            encodedBytes, 0,
                                            encodedBytes.size
                                    )

                                    // specify US-ASCII encoding
                                   // val data = String(encodedBytes, "US-ASCII")
                                    readBufferPosition = 0

                                    // tell the user data were sent to bluetooth printer device
                                    handler.post {
                                      //  myLabel.setText(data)
                                    }

                                } else {
                                    readBuffer[readBufferPosition++] = b
                                }
                            }
                        }

                    } catch (ex: IOException) {
                        stopWorker = true
                    }

                }
            })

            workerThread.start()

        } catch (e: Exception) {
            e.printStackTrace()
        }

    }


    class ReceiptItemAdapter(val list: java.util.ArrayList<GetInvoiceDetails>, val Currency_value: String) : RecyclerView.Adapter<ReceiptItemAdapter.ViewHolder>() {

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            holder!!.bindItems(list[position], Currency_value)
        }

        override fun getItemCount(): Int {
            return list.size
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            val v = LayoutInflater.from(parent!!.context).inflate(R.layout.receipt_item, parent, false)
            return ViewHolder(v)
        }

        class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
            val txtitem: TextView = itemView.findViewById(R.id.txtitem)
            val txtqty: TextView = itemView.findViewById(R.id.txtqty)
            val txtprice: TextView = itemView.findViewById(R.id.txtprice)
            val txtamt: TextView = itemView.findViewById(R.id.txtamt)

            fun bindItems(data: GetInvoiceDetails, Currency_value: String) {
                txtitem.text = "${data.product_name}"
                txtqty.text = "${data.invoice_detail_qty} ${data.unit_short_name}"
                txtprice.text = "$Currency_value${data.invoice_detail_price}"
                txtamt.text = "$Currency_value${data.invoice_detail_total_price}"
                //tv_patienttype.text = "${data.type_name}"
            }
        }
    }

    class ReceiptTaxItemAdapter(val list: java.util.ArrayList<GetTaxData>, val Currency_value: String) : RecyclerView.Adapter<ReceiptTaxItemAdapter.ViewHolder>() {

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            holder!!.bindItems(list[position], Currency_value)
        }

        override fun getItemCount(): Int {
            return list.size
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            val v = LayoutInflater.from(parent!!.context).inflate(R.layout.receipttax_data_item, parent, false)
            return ViewHolder(v)
        }

        class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
            val tv_tax_title: TextView = itemView.findViewById(R.id.tv_tax_title)
            val tv_tax_amount: TextView = itemView.findViewById(R.id.tv_tax_amount)

            fun bindItems(data: GetTaxData, Currency_value: String) {
                tv_tax_title.text = "${data.tax_name}"
                //tv_tax_amount.text = "$Currency_value${data.tax_total}"
                if (data.tax_status == "2") {
                    tv_tax_amount.text = "${data.tax_total}%"
                } else {
                    tv_tax_amount.text = "$Currency_value${data.tax_total}"
                }
            }
        }
    }

    fun RightAlignText(mString: String): String {
        return if (mString.length == 1) {
            "         $mString"
        } else if (mString.length == 2) {
            "        $mString"
        } else if (mString.length == 3) {
            "       $mString"
        } else if (mString.length == 4) {
            "      $mString"
        } else if (mString.length == 5) {
            "     $mString"
        } else if (mString.length == 6) {
            "    $mString"

        } else if (mString.length == 7) {
            "   $mString"
        } else if (mString.length == 8) {

            "  $mString"

        } else if (mString.length == 9) {

            " $mString"
        } else {
            mString
        }
    }

    fun AmtRightAlignText(mString: String): String {
        println("Amt" + mString.length)
        return if (mString.length == 1) {
            "        $mString"
        } else if (mString.length == 2) {
            "       $mString"
        } else if (mString.length == 3) {
            "      $mString"
        } else if (mString.length == 4) {
            "     $mString"
        } else if (mString.length == 5) {
            "    $mString"
        } else if (mString.length == 6) {
            "   $mString"

        } else if (mString.length == 7) {
            "  $mString"
        } else if (mString.length == 8) {

            " $mString"

        } else {
            mString
        }
    }

    fun QtyRightAlignText(mString: String): String {
        println("Amt" + mString.length)
        return if (mString.length == 1) {
            "  $mString"
        } else if (mString.length == 2) {
            " $mString"
        } else {
            mString
        }
    }
}