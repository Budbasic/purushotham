package com.budbasic.posconsole.sales

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.support.v4.app.Fragment
import android.support.v7.app.AlertDialog
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.budbasic.customer.global.Preferences
import com.budbasic.global.CUC
import com.budbasic.posconsole.R
import com.budbasic.posconsole.model.APIClass
import com.budbasic.posconsole.reception.SalesActivity
import com.budbasic.posconsole.reception.StoreManagerActivity
import com.budbasic.posconsole.webservice.RequetsInterface
import com.bumptech.glide.Glide
import com.google.gson.Gson
import com.kotlindemo.model.GetEmployeeAppSetting
import com.kotlindemo.model.GetEmployeeLogin
import com.kotlindemo.model.GetQueueList
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_sales_menager.view.*
import java.util.*
import java.util.concurrent.TimeUnit

class SalesMenagerFragment : Fragment(), APIClass.onCallListner {
    override fun onFailed(requestCode: Int) {
    }

    override fun onSuccess(requestCode: Int) {
        if (requestCode == EMPLOYEESALESQUEUESELECT) {
            callEmployeeSalesQueueSelect()
        } else if (requestCode == EMPLOYEEQUEUELIST) {
            callEmployeeQueueList()
        }
    }
    val EMPLOYEESALESQUEUESELECT = 1
    val EMPLOYEEQUEUELIST = 2

    var mDialog: Dialog? = null
    var list: ArrayList<GetQueueList> = ArrayList()
    var mView: View? = null
    lateinit var productAdapter: ProductAdapter
    lateinit var logindata: GetEmployeeLogin
    val mRequestCode = 8428
    lateinit var subscription: Disposable
    var appSettingData: GetEmployeeAppSetting? = null
    var queue_id = ""
    var queue_patient_id = ""
    var QueueData = ""
    var selectDate = ""
    var apiClass: APIClass? = null
    var queue_type = "1"
    var storeopen = "0"
    var tillopen = "0"

    var queueStatus: Array<String>? = null
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        if (view != null)
            mView = view!!
        else
            mView = inflater.inflate(R.layout.fragment_sales_menager, container, false)
        apiClass = APIClass(context!!, this)
        logindata = Gson().fromJson(Preferences.getPreference(mView!!.context, "logindata"), GetEmployeeLogin::class.java)
        init()
        return mView
    }

    private fun init() {
        mView!!.ll_employee_ordermain.setOnClickListener {

        }
        mView!!.btn_back.setOnClickListener {
          activity!!.onBackPressed()

          //  (context!! as StoreManagerActivity).showImage()
        }
        mView!!.rv_employee_orders_fragment!!.layoutManager = LinearLayoutManager(mView!!.context)
        appSettingData = Gson().fromJson(Preferences.getPreference(context!!, "AppSetting"), GetEmployeeAppSetting::class.java)

        productAdapter = ProductAdapter(list, mView!!.context, object : onClickListener {
            override fun callClick(data: GetQueueList) {

                if (data.queue_pushed_accept == "0") {
                    queue_id = "${data.queue_id}"
                    queue_patient_id = "${data.queue_patient_id}"
                    QueueData = Gson().toJson(data)
                    callEmployeeSalesQueueSelect()
                } else {
                    queue_id = "${data.queue_id}"
                    queue_patient_id = "${data.queue_patient_id}"
                    QueueData = Gson().toJson(data)
                    passProduct()
                }

                /*if (data.queue_pushed_accept == "0") {
                    val builder = AlertDialog.Builder(mView!!.context)
                    builder.setMessage("Are you sure you want to accept this order?")

                    builder.setPositiveButton("Yes") { dialog, which ->
                        dialog.dismiss()
                        queue_id = "${data.queue_id}"
                        queue_patient_id = "${data.queue_patient_id}"
                        QueueData = Gson().toJson(data)
                        callEmployeeSalesQueueSelect()
                    }

                    builder.setNegativeButton("No") { dialog, which -> dialog.dismiss() }
                    val alert = builder.create()
                    alert.show()
                } else {
                    queue_id = "${data.queue_id}"
                    queue_patient_id = "${data.queue_patient_id}"
                    QueueData = Gson().toJson(data)
                    passProduct()
                }*/

            }
        })

        queueStatus = resources.getStringArray(R.array.sales_queue_status_)
        var mobileappAdapter = ArrayAdapter<String>(mView!!.context, R.layout.spinner_item, R.id.dropdown_item, queueStatus)
        mView!!.bs_status.setText("${resources.getStringArray(R.array.sales_queue_status_)[0]}")
        mView!!.bs_status.setAdapter(mobileappAdapter)
        mView!!.bs_status.setOnItemClickListener { adapterView, view, i, l ->
            if (mView!!.bs_status.text.toString() == "Left Unassisted") {
                queue_type = "3"
              //  setVisibilityRemoveButton(true)
            }  else {
                queue_type = "1"
              //  setVisibilityRemoveButton(false)
            }
            if (!storeopen.equals("0")){
                if (!tillopen.equals("0")){
                    callEmployeeQueueList()
                }
            }

            //Log.i(CreateAccountActivity::class.java.simpleName, "queue_type  : $queue_type")
        }

        mView!!.rv_employee_orders_fragment!!.adapter = productAdapter
        mView!!.mSwipeRefreshLayout.setOnRefreshListener {
            if (!storeopen.equals("0")){
                if (!tillopen.equals("0")){
                    callEmployeeQueueList()
                }
            }
            //callEmployeeQueueList()
        }
        callCheckStoreOpen()
       // callEmployeeQueueList()
    }

    override fun onStart() {
        super.onStart()
        Log.i(SalesMenagerFragment::class.java.simpleName, "onStart()")
        startInterval()
    }

    fun startInterval() {
        subscription = Observable.interval(30000, 60000, TimeUnit.MILLISECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    Log.i(SalesMenagerFragment::class.java.simpleName, "Interval : $it ")
                    callEmployeeQueueList()
                }
    }

    fun stopInterval() {
        subscription.dispose()
    }

    override fun onStop() {
        super.onStop()
        Log.i(SalesMenagerFragment::class.java.simpleName, "onStop()")
        stopInterval()
    }


    override fun onDestroy() {
        super.onDestroy()
        Log.i(SalesMenagerFragment::class.java.simpleName, "onDestroy()")
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        Log.i("Result", "onActivityResult(), $requestCode : $resultCode")
        if (requestCode == mRequestCode) {
            if (resultCode == Activity.RESULT_OK) {
                callEmployeeQueueList()
            }
        }
    }
    fun callEmployeeQueueClose(queue_remove_id: String) {
        mDialog = CUC.createDialog(activity!!)
        val apiService = RequetsInterface.create()
        val parameterMap = HashMap<String, String>()
        parameterMap["api_key"] = "${Preferences.getPreference(activity!!, "API_KEY")}"
        parameterMap["queue_id"] = "$queue_remove_id"
        Log.i("Result", "parameterMap : $parameterMap")
        CompositeDisposable().add(apiService.callEmployeeQueueClose(parameterMap)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe({ result ->
                    if (mDialog != null && mDialog!!.isShowing)
                        mDialog!!.dismiss()
                    if (result != null) {
                        Log.i("Result", "" + result.toString())
                        if (result.status == "0") {
                            callEmployeeQueueList()
                        } else {
                            CUC.displayToast(activity!!, result.show_status, result.message)
                        }
                    } else {
                        // CUC.displayToast(activity!!, "0", getString(R.string.connection_to_database_failed))
                    }
                }, { error ->
                    if (mDialog != null && mDialog!!.isShowing)
                        mDialog!!.dismiss()
                    CUC.displayToast(activity!!, "0", getString(R.string.connection_to_database_failed))

                    error.printStackTrace()
                })
        )
    }
    fun callEmployeeQueueList() {
        Log.i("Result", "callEmployeeSalesReport()")
        if (!activity!!.isFinishing) {
            if (mView!!.mSwipeRefreshLayout != null) {
                mView!!.mSwipeRefreshLayout.isRefreshing = false
            }
            if (mDialog != null && mDialog!!.isShowing)
                mDialog!!.dismiss()
            mDialog = CUC.createDialog(activity!!)
            val apiService = RequetsInterface.Factory.create()
            val parameterMap = HashMap<String, String>()
            parameterMap["api_key"] = "${Preferences.getPreference(activity!!, "API_KEY")}"
            if (logindata != null) {
                parameterMap["employee_id"] = "${logindata.user_id}"
                parameterMap["store_id"] = "${logindata.user_store_id}"
            }
            selectDate = "${Calendar.getInstance().get(Calendar.YEAR)}-" +
                    "${(if (Calendar.getInstance().get(Calendar.MONTH) + 1 >= 10) Calendar.getInstance().get(Calendar.MONTH) + 1 else "0" + (Calendar.getInstance().get(Calendar.MONTH) + 1))}-" +
                    "${(if (Calendar.getInstance().get(Calendar.DAY_OF_MONTH) >= 10) Calendar.getInstance().get(Calendar.DAY_OF_MONTH) else "0${Calendar.getInstance().get(Calendar.DAY_OF_MONTH)}")}"
            parameterMap["queue_date"] = "$selectDate"
            parameterMap["queue_type"] = "$queue_type"
            Log.i("Result", "$parameterMap")
            CompositeDisposable().add(apiService.callEmployeeQueueList(parameterMap)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe({ result ->
                        if (mDialog != null && mDialog!!.isShowing)
                            mDialog!!.dismiss()
                        if (result != null) {
                            Log.i("Result", "" + result.toString())
                            if (result.status == "0") {
                                if (result.queue_list != null && result.queue_list.size > 0) {
                                    list.clear()
                                    list.addAll(result.queue_list)
                                    productAdapter.notifyDataSetChanged()
                                    mView!!.rl_empty_view.visibility = View.GONE
                                } else {
                                    list.clear()
                                    productAdapter.notifyDataSetChanged()
                                    mView!!.rl_empty_view.visibility = View.VISIBLE
                                }
                                if (!activity!!.isFinishing)
                                    CUC.displayToast(activity!!, result.show_status, result.message)
                            } else if (result.status == "10") {
                                apiClass!!.callApiKey(EMPLOYEEQUEUELIST)
                            } else {
                                list.clear()
                                productAdapter.notifyDataSetChanged()
                                mView!!.rl_empty_view.visibility = View.VISIBLE
                            }
                        } else {
                            if (!activity!!.isFinishing) {
                                list.clear()
                                productAdapter.notifyDataSetChanged()
                                mView!!.rl_empty_view.visibility = View.VISIBLE
                              //  CUC.displayToast(activity!!, "0", getString(R.string.connection_to_database_failed))
                            }
                        }

                    }, { error ->
                        if (!activity!!.isFinishing) {
                            list.clear()
                            productAdapter.notifyDataSetChanged()
                            mView!!.rl_empty_view.visibility = View.VISIBLE
                            CUC.displayToast(activity!!, "0", getString(R.string.connection_to_database_failed))
                        }
                        error.printStackTrace()
                    })
            )
        }

    }

    fun callEmployeeSalesQueueSelect() {
        mDialog = CUC.createDialog(activity!!)
        val apiService = RequetsInterface.create()
        val parameterMap = HashMap<String, String>()
        parameterMap["api_key"] = "${Preferences.getPreference(activity!!, "API_KEY")}"
        parameterMap["queue_id"] = "$queue_id"
        if (logindata != null) {
            parameterMap["employee_id"] = "${logindata.user_id}"
            parameterMap["store_id"] = "${logindata.user_store_id}"
        }
        Log.i("Result", "parameterMap : $parameterMap")
        CompositeDisposable().add(apiService.callEmployeeSalesQueueSelect(parameterMap)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe({ result ->
                    if (mDialog != null && mDialog!!.isShowing)
                        mDialog!!.dismiss()
                    if (result != null) {
                        Log.i("Result", "" + result.toString())
                        if (result.status == "0") {
                            //QueueData = Gson().toJson(result.Queue)
                            passProduct()
                        } else if (result.status == "10") {
                            apiClass!!.callApiKey(EMPLOYEESALESQUEUESELECT)
                        } else {
                            CUC.displayToast(activity!!, result.show_status, result.message)
                        }
                    } else {
                      //  CUC.displayToast(activity!!, "0", getString(R.string.connection_to_database_failed))
                    }
                }, { error ->
                    if (mDialog != null && mDialog!!.isShowing)
                        mDialog!!.dismiss()
                    CUC.displayToast(activity!!, "0", getString(R.string.connection_to_database_failed))

                    error.printStackTrace()
                })
        )
    }

    fun passProduct() {
        val gson = Gson()
        val myJson = gson.toJson(logindata)
        val goIntent = Intent(activity!!, SalesActivity::class.java)
        goIntent.putExtra("queue_patient_id", "$queue_patient_id")
        goIntent.putExtra("queue_id", "$queue_id")
        goIntent.putExtra("QueueData", "$QueueData")
        goIntent.putExtra("logindata",myJson)
        activity!!.startActivityForResult(goIntent, mRequestCode)
        //finish()
    }

    inner class ProductAdapter(val list: ArrayList<GetQueueList>, val context: Context, val onClick: onClickListener) : RecyclerView.Adapter<ProductAdapter.ViewHolder>() {

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            val data = list[position]
            holder.tv_name.text = "${data.queue_number}"
            holder.tv_mobilenumber.text = CUC.getCapsSentences("${data.first_name} ${data.last_name}")
            holder.tv_phonenumber.text = "${data.patient_licence_number}"
            if (data.patient_mmmp_licence == "1") {
                holder.tv_patienttype.text = "YES"
            } else {
                holder.tv_patienttype.text = "NO"
            }
            var minutes = "${data.queue_minutes}".toInt()
            holder.tv_timing.text = "${data.queue_minutes}"
            if (minutes >= 30) {
                holder.tv_timing.setTextColor(context.resources.getColor(R.color.redeem_color))
            } else if (minutes >= 20) {
                holder.tv_timing.setTextColor(context.resources.getColor(R.color.roti))
            } else {
                holder.tv_timing.setTextColor(context.resources.getColor(R.color.yellowlight))
            }

            Glide.with(context).load(data.patient_image).into(holder.img_profile)

            holder.tv_mobileapp.text = "${data.queue_desc}"
            //    tv_loyaltygroup.text = "${data.queue_minutes}"


            holder.ll_remove.setOnClickListener {
                if(queue_type=="1"){
                    callAppqValidateQueueRemoveStatus(data.queue_id)
                }else{
                    callEmployeeQueueClose(data.queue_id)
                }

             //   callEmployeeQueueClose(data.queue_id)
            }

            holder.ll_queuemain.setOnClickListener {
                onClick.callClick(data)
            }
        }

        override fun getItemCount(): Int {
            return list.size
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            val v = LayoutInflater.from(parent!!.context).inflate(R.layout.sales_queue_item, parent, false)
            return ViewHolder(v)
        }

        inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
            val tv_name: TextView = itemView.findViewById(R.id.tv_name)
            val tv_mobilenumber: TextView = itemView.findViewById(R.id.tv_mobilenumber)
            val tv_phonenumber: TextView = itemView.findViewById(R.id.tv_phonenumber)
            val tv_patienttype: TextView = itemView.findViewById(R.id.tv_patienttype)
            val tv_mobileapp: TextView = itemView.findViewById(R.id.tv_mobileapp)
            val tv_loyaltygroup: TextView = itemView.findViewById(R.id.tv_loyaltygroup)
            //   val tv_close: TextView = itemView.findViewById(R.id.tv_close)
            val ll_item_close: LinearLayout = itemView.findViewById(R.id.ll_item_close)
            val ll_queuemain: TextView = itemView.findViewById(R.id.ll_queuemain)
            val tv_timing: TextView = itemView.findViewById(R.id.tv_timing)
            val img_profile: ImageView = itemView.findViewById(R.id.img_profile)
            val ll_remove: LinearLayout = itemView.findViewById(R.id.ll_remove)

        }
    }

    interface onClickListener {
        fun callClick(data: GetQueueList)
    }

    fun callAppqValidateQueueRemoveStatus(queue_remove_id: String) {
        var mDialog = CUC.createDialog(activity!!)
        val apiService = RequetsInterface.create()
        val parameterMap = HashMap<String, String>()
        parameterMap["api_key"] = "${Preferences.getPreference(activity!!, "API_KEY")}"
        parameterMap["queue_id"] = "$queue_remove_id"
        if (logindata != null) {
            parameterMap["employee_id"] = "${logindata.user_id}"
            parameterMap["store_id"] = "${logindata.user_store_id}"
        }
        parameterMap["queue_status"] = "3"
        Log.i("Result", "parameterMap : $parameterMap")
        CompositeDisposable().add(apiService.callAppqValidateQueuePush(parameterMap)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe({ result ->
                    if (mDialog != null && mDialog.isShowing)
                        mDialog.dismiss()
                    if (result != null) {
                        Log.i("Result", "" + result.toString())
                        if (result.status == "0") {
                            Handler().postDelayed({
                                callEmployeeQueueList()
                            }, 200)

                            /*if ("$QueueStatus" == "2") {
                                val QueueData = Gson().toJson(result.queue)
                                val goIntent = Intent(activity!!, SalesCategoryActivity::class.java)
                                goIntent.putExtra("queue_patient_id", "$patient_id")
                                goIntent.putExtra("queue_id", "${result.queue.queue_id}")
                                goIntent.putExtra("QueueData", "$QueueData")
                                activity!!.startActivityForResult(goIntent, 664)
                            }*/
                        } else if (result.status == "10") {
                            apiClass!!.callApiKey(EMPLOYEESALESQUEUESELECT)
                        } else {
                            CUC.displayToast(activity!!, result.show_status, result.message)
                        }
                    } else {
                      //  CUC.displayToast(activity!!, "0", getString(R.string.connection_to_database_failed))
                    }
                }, { error ->
                    if (mDialog != null && mDialog.isShowing)
                        mDialog.dismiss()
                   CUC.displayToast(activity!!, "0", getString(R.string.connection_to_database_failed))

                    error.printStackTrace()
                })
        )
    }

    fun callCheckStoreOpen() {

        val terminalid = appSettingData?.terminal_id
        if (CUC.isNetworkAvailablewithPopup(activity!!)) {
            mDialog = CUC.createDialog(activity!!)
            val apiService = RequetsInterface.Factory.create()
            val parameterMap = HashMap<String, String>()
            parameterMap["api_key"] = "${Preferences.getPreference(activity!!, "API_KEY")}"
            parameterMap["terminal_id"] = "${terminalid}"
            if (logindata != null) {
                parameterMap["emp_id"] = "${logindata.user_id}"
                parameterMap["store_id"] = "${logindata.user_store_id}"
            }
            println("aaaaaaaaaaa  storecheck  "+parameterMap)
            Log.i("aaaa patient Result", "parameterMap : $parameterMap")
            CompositeDisposable().add(apiService.callCheckStoreOpen(parameterMap)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe({ result ->
                        if (mDialog != null && mDialog!!.isShowing)
                            mDialog!!.dismiss()
                        println("aaaaaaaaaa  storecheck  Result  "+result.toString())
                        Log.i("api Result", "" + result.toString())
                        if (result != null) {
                            if (result.store_chk == "0") {
                                // CUC.displayToast(activity!!, result.show_status, result.message)
                                showdialog(result.message)
                            } else {
                                storeopen="1"
                                calltillCheck()
                                //CUC.displayToast(activity!!, result.show_status, result.message)
                            }
                        } else {
                            // CUC.displayToast(activity!!, "0", getString(R.string.connection_to_database_failed))
                        }
                    }, { error ->
                        if (mDialog != null && mDialog!!.isShowing)
                            mDialog!!.dismiss()
                        CUC.displayToast(activity!!, "0", getString(R.string.connection_to_database_failed))
                        error.printStackTrace()
                    })
            )
        }
    }

    fun calltillCheck() {

        val terminalid = appSettingData?.terminal_id
        if (CUC.isNetworkAvailablewithPopup(activity!!)) {
            mDialog = CUC.createDialog(activity!!)
            val apiService = RequetsInterface.Factory.create()
            val parameterMap = HashMap<String, String>()
            parameterMap["api_key"] = "${Preferences.getPreference(activity!!, "API_KEY")}"
            parameterMap["terminal_id"] = "${terminalid}"
            if (logindata != null) {
                parameterMap["emp_id"] = "${logindata.user_id}"
                parameterMap["store_id"] = "${logindata.user_store_id}"
            }
            println("aaaaaaaaaaa  tillcheck  "+parameterMap)
            Log.i("aaaa patient Result", "parameterMap : $parameterMap")
            CompositeDisposable().add(apiService.callTillOpenCheck(parameterMap)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe({ result ->
                        if (mDialog != null && mDialog!!.isShowing)
                            mDialog!!.dismiss()
                        println("aaaaaaaaaa  tillcheck  Result  "+result.toString())
                        Log.i("api Result", "" + result.toString())
                        if (result != null) {
                            if (result.till_check == "0") {
                                //CUC.displayToast(activity!!, result.show_status, result.message)
                                showdialog(result.message)
                            } else {
                                tillopen="1"

                                callEmployeeQueueList()
                                // CUC.displayToast(activity!!, result.show_status, result.message)
                            }
                        } else {
                            // CUC.displayToast(activity!!, "0", getString(R.string.connection_to_database_failed))
                        }
                    }, { error ->
                        if (mDialog != null && mDialog!!.isShowing)
                            mDialog!!.dismiss()
                        CUC.displayToast(activity!!, "0", getString(R.string.connection_to_database_failed))
                        error.printStackTrace()
                    })
            )
        }
    }

    fun showdialog(msg:String) {
        val builder1 = android.app.AlertDialog.Builder(context)
        builder1.setMessage("" + msg)
        builder1.setCancelable(true)

        builder1.setPositiveButton(
                "Ok"
        ) { dialog, id ->
            dialog.cancel()

        }

        val alert11 = builder1.create()
        alert11.show()
    }

}