package com.budbasic.posconsole.sales;

import android.annotation.SuppressLint;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.budbasic.posconsole.R;
import com.kotlindemo.model.GetPaymentTypes;

import java.util.ArrayList;

@SuppressLint("ValidFragment")
public class PaymentFragment extends Fragment {

    TextView text;
    GetPaymentTypes payment_types;
    int position;

    @SuppressLint("ValidFragment")
    public PaymentFragment(GetPaymentTypes payment_types,int position) {
        this.payment_types=payment_types;
        this.position=position;
    }

    public static PaymentFragment newInstance(int position, GetPaymentTypes payment_types) {
        return new PaymentFragment(payment_types,position);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View v= inflater.inflate(R.layout.payment_fragment, container, false);
        text=v.findViewById(R.id.text);
        text.setText(""+payment_types.getPayment_detail());


        return v;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

}
