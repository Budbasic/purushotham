package com.budbasic.posconsole.sales

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.BottomSheetDialog
import android.support.v4.content.ContextCompat
import android.support.v4.view.PagerAdapter
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.text.SpannableString
import android.text.SpannableStringBuilder
import android.text.style.ForegroundColorSpan
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView
import com.bumptech.glide.Glide
import com.google.gson.Gson
import com.kotlindemo.model.*
import com.budbasic.posconsole.webservice.RequetsInterface
import com.budbasic.posconsole.R
import com.budbasic.customer.global.Preferences
import com.budbasic.global.CUC
import com.budbasic.posconsole.model.APIClass
import com.bumptech.glide.load.DecodeFormat
import com.bumptech.glide.request.RequestOptions
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.product_details.*
import kotlin.math.nextUp


class SalesProductDetails : AppCompatActivity(), APIClass.onCallListner {
    override fun onFailed(requestCode: Int) {
    }

    override fun onSuccess(requestCode: Int) {
        if (requestCode == EMPLOYEEPATIENTADDTOCART) {
            callEmployeePatientAddtocart()
        }
    }

    val EMPLOYEEPATIENTADDTOCART = 1

    private var mAdapter: ViewPagerAdapter? = null
    lateinit var productData: GetSalesProduct

    lateinit var mDialog: Dialog

    lateinit var logindata: GetEmployeeLogin
    lateinit var mAppBasic: GetEmployeeAppSetting

    var productQytValue = 1f
    var package_id = ""
    var Currency_value = ""
    var qty = ""

    var queue_patient_id = ""
    var queue_id = ""

    var imagePath = ""
    lateinit var dialogProductQty: BottomSheetDialog
    lateinit var mPacketAdapter: PacketAdapter
    var packetList = ArrayList<GetProductPackets>()
    var apiClass: APIClass? = null
    //lateinit var productQtyDialog: Dialog
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.product_details)
        apiClass = APIClass(this@SalesProductDetails, this)
        init()
    }

    fun init() {
        mAppBasic = Gson().fromJson(Preferences.getPreference(this@SalesProductDetails, "AppSetting"), GetEmployeeAppSetting::class.java)
        logindata = Gson().fromJson(Preferences.getPreference(this@SalesProductDetails, "logindata"), GetEmployeeLogin::class.java)
        productData = Gson().fromJson(intent.getStringExtra("data"), GetSalesProduct::class.java)
        imagePath = intent.getStringExtra("imagePath")
        Currency_value = "${mAppBasic.Currency_value}"
        Log.i("ProductDetails", "product_id : $productData")
        if (intent != null && intent.hasExtra("queue_patient_id"))
            queue_patient_id = intent.getStringExtra("queue_patient_id")
        if (intent != null && intent.hasExtra("queue_id"))
            queue_id = intent.getStringExtra("queue_id")

        val listPro = ArrayList<GetProductSubImage>()
        if (productData != null) {
            listPro.add(GetProductSubImage("", "", productData.product_images, "", "", "", ""))
            listPro.addAll(productData.product_sub_image)
            mAdapter = ViewPagerAdapter(this@SalesProductDetails, listPro!!)
            photos_viewpager.adapter = mAdapter
            tab_indicator.setupWithViewPager(photos_viewpager)
            tv_pro_title.text = productData.p_map_store_name
            tv_pro_name.text = productData.p_map_store_name

            if (productData.p_map_base_price != null && productData.p_map_base_price != "0" && productData.p_map_base_price != "0.0" && productData.p_map_base_price != "0.00") {
                tv_pro_price.setText(xxx("Price : ", "$Currency_value${productData.p_map_base_price}",
                        ContextCompat.getColor(this@SalesProductDetails, R.color.black),
                        ContextCompat.getColor(this@SalesProductDetails, R.color.colorPrimary)
                ), TextView.BufferType.SPANNABLE)
            } else {
                tv_pro_price.setText(xxx("Price : ", "$Currency_value${productData.product_price}",
                        ContextCompat.getColor(this@SalesProductDetails, R.color.black),
                        ContextCompat.getColor(this@SalesProductDetails, R.color.colorPrimary)), TextView.BufferType.SPANNABLE)
            }
            if (productData.product_indica != null && productData.product_indica != "" && productData.product_indica != "0")
                tv_indica.text = "${productData.product_indica}%"
            else
                tv_indica.text = ""
            if (productData.product_sativa != null && productData.product_sativa != "" && productData.product_sativa != "0")
                tv_sativa.text = "${productData.product_sativa}%"
            else
                tv_sativa.text = ""

            //var lab_test_str = ""
            if (productData.product_thc != null && productData.product_thc != "")
                tv_thc.text = "${productData.product_thc}%"
            else
                tv_thc.text = ""

            if (productData.product_thca != null && productData.product_thca != "" && productData.product_thca != "0")
                tv_thca.text = "${productData.product_thca}%"
            else
                tv_thca.text = ""

            if (productData.product_cbd != null && productData.product_cbd != "" && productData.product_cbd != "0")
                tv_cbd.text = "${productData.product_cbd}%"
            else
                tv_cbd.text = ""

            if (productData.product_cbda != null && productData.product_cbda != "" && productData.product_cbda != "0")
                tv_cbda.text = "${productData.product_cbda}%"
            else
                tv_cbda.text = ""

            if (productData.product_cbn != null && productData.product_cbn != "" && productData.product_cbn != "0")
                tv_cbn.text = "${productData.product_cbn}%"
            else
                tv_cbn.text = ""
            val mColor1 = ContextCompat.getColor(this@SalesProductDetails, R.color.colorPrimary)
            val mColor2 = ContextCompat.getColor(this@SalesProductDetails, R.color.dark_gray)
            tv_who_tested_medicin.setText(xxx("Who tested this medicine?\n", " -${productData.product_who_tested_medicine}", mColor1, mColor2), TextView.BufferType.SPANNABLE)
            tv_genetics.setText(xxx("Genetics (ex. Blueberry X Haze)\n", " -${productData.products_genetics}", mColor1, mColor2), TextView.BufferType.SPANNABLE)
            tv_sclabs_report_id.setText(xxx("SCLabs Report ID\n", " -${productData.product_sclabs_report_id}", mColor1, mColor2), TextView.BufferType.SPANNABLE)
            tv_lab_batch_number.setText(xxx("Lab Batch Number\n", " -${productData.product_lab_batch_number}", mColor1, mColor2), TextView.BufferType.SPANNABLE)
            tv_lab_licenceno.setText(xxx("Lab Licence Number\n", " -${productData.product_lab_licenceno}", mColor1, mColor2), TextView.BufferType.SPANNABLE)
            tv_medicine_amount.setText(xxx("Medicine Amount\n", " -${productData.product_medicine_amount}", mColor1, mColor2), TextView.BufferType.SPANNABLE)
            tv_medicine_measurement.setText(xxx("Medicine Measurement\n", " -${productData.product_medicine_measurement}", mColor1, mColor2), TextView.BufferType.SPANNABLE)
            tv_ingredients.setText(xxx("Ingredients\n", " -${productData.product_ingredients}", mColor1, mColor2), TextView.BufferType.SPANNABLE)

            if (productData.p_map_qty == "" || productData.p_map_qty == null || productData.p_map_qty == "0") {
                tv_tottal_qty.text = "Unavailable"
                tv_tottal_qty.setTextColor(ContextCompat.getColor(this@SalesProductDetails, R.color.red))
                ll_add_to_cart.visibility = View.GONE
            } else {
                tv_tottal_qty.text = "${productData.p_map_qty} ${productData.unit_short_name}"
            }
            val productTaxes = productData.product_taxes
            val poductTaxesAdapter = ProductTaxesAdapter(this@SalesProductDetails, productTaxes)
            rv_product_taxes.layoutManager = LinearLayoutManager(this@SalesProductDetails)
            rv_product_taxes.adapter = poductTaxesAdapter

        } else {
            finish()
        }
        ll_add_to_cart.setOnClickListener {
            DisplayProductQtyDialog()

        }

        iv_back.setOnClickListener {
            finish()
        }
    }


    override fun onBackPressed() {
        //super.onBackPressed()

    }

    fun xxx(textlbl: String, textvalue: String, textlblColor: Int, textvalueColor: Int): SpannableStringBuilder {
        val builder = SpannableStringBuilder()
        val str1 = SpannableString(textlbl)
        //str1.setSpan( RelativeSizeSpan(2f), 0,5, 0);
        str1.setSpan(ForegroundColorSpan(textlblColor), 0, str1.length, 0)
        builder.append(str1)
        val str2 = SpannableString(textvalue)
        str2.setSpan(ForegroundColorSpan(textvalueColor), 0, str2.length, 0)
        builder.append(str2)
        return builder
    }

    class ProductTaxesAdapter(val mContext: Context, val productTaxes: ArrayList<GetProductTaxes>) : RecyclerView.Adapter<ProductTaxesAdapter.ProductTaxesHolder>() {
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductTaxesHolder {
            val mView = LayoutInflater.from(parent.context).inflate(R.layout.product_taxes_item, parent, false)
            return ProductTaxesHolder(mView)
        }

        override fun getItemCount(): Int {
            return productTaxes.size
        }

        override fun onBindViewHolder(holder: ProductTaxesHolder, position: Int) {
            holder.bindVliew(mContext, productTaxes[position])
        }

        class ProductTaxesHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
            val tv_taxeslbl = itemView.findViewById<TextView>(R.id.tv_taxeslbl)
            val tv_taxesvalue = itemView.findViewById<TextView>(R.id.tv_taxesvalue)
            fun bindVliew(mContext: Context, productTaxes: GetProductTaxes) {
                tv_taxeslbl.text = productTaxes.tax_name
                tv_taxesvalue.text = productTaxes.tax_rate
            }
        }
    }

//    fun zoomDialog(imgPath: String) {
//        val displayMetrics = DisplayMetrics()
//        this.windowManager.defaultDisplay.getMetrics(displayMetrics)
//        val dialog = Dialog(this@EmployeeProductDetails, R.style.TransparentProgressDialog)
//        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
//        dialog.setContentView(R.layout.zoom_imageview)
//        dialog.window!!.setLayout(displayMetrics.widthPixels - resources.getDimension(R.dimen._30sdp).toInt(), displayMetrics.heightPixels - resources.getDimension(R.dimen._100sdp).toInt())
//
//        val photoView: PhotoView = dialog.findViewById(R.id.iv_pro_fullimg)
//
//        // set zoomable tag on views that is to be zoomed
//        Glide.with(this@EmployeeProductDetails)
//                .load(imgPath)
//                .into(photoView)
//
//        dialog.show()
//        //dialog.setCancelable(false)
//        //dialog.setCanceledOnTouchOutside(false)
//    }


    inner class ViewPagerAdapter(private val mContext: Context, private val imgData: ArrayList<GetProductSubImage>) : PagerAdapter() {

        override fun getCount(): Int {
            return imgData.size
        }

        override fun isViewFromObject(view: View, `object`: Any): Boolean {
            return view === `object` as LinearLayout
        }

        override fun instantiateItem(container: ViewGroup, position: Int): Any {
            val itemView = LayoutInflater.from(mContext).inflate(R.layout.pager_item, container, false)

            val imageView = itemView.findViewById(R.id.img_pager_item) as ImageView

            Glide.with(mContext)
                    .applyDefaultRequestOptions(RequestOptions().format(DecodeFormat.PREFER_ARGB_8888).error(R.drawable.no_image_available))
                    .asBitmap()
                    .load("$imagePath${imgData!![position].product_sub_image}")
                    .into(imageView)
            container.addView(itemView)

            imageView.setOnClickListener {
                //zoomDialog("$imagePath${imgData!![position].product_sub_image}")
            }

            return itemView
        }

        override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
            container.removeView(`object` as LinearLayout)
        }
    }

    fun DisplayProductQtyDialog() {
        //productQytValue = 1
        package_id = ""
        var countDeci = 0
        try {
            countDeci = "${productData.product_default_qty}".split("\\.".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()[1].length
        } catch (e: Exception) {
            e.printStackTrace()
        }
        val strForm = "%.${countDeci}f"
        println("CountFormat -  countDeci : $countDeci")
        productQytValue = "${productData.product_default_qty}".toFloat()
        println("CountFormat -  productQytValue : $productQytValue")
        val incriVal = "${productData.product_qty}".toFloat()
        println("CountFormat -  incriVal : $incriVal")

        val view = layoutInflater.inflate(R.layout.product_qty_dialog, null)
        dialogProductQty = BottomSheetDialog(this)
        dialogProductQty.setContentView(view)

        val tv_unit_name = dialogProductQty.findViewById<TextView>(R.id.tv_unit_name)!!
        val tv_product_name = dialogProductQty.findViewById<TextView>(R.id.tv_product_name)!!
        val tv_tottal_qty = dialogProductQty.findViewById<TextView>(R.id.tv_tottal_qty)!!
        val tv_first_text = dialogProductQty.findViewById<TextView>(R.id.tv_first_text)!!
        val ll_add_cart = dialogProductQty.findViewById<LinearLayout>(R.id.ll_add_cart)!!
        val rl_first_minus = dialogProductQty.findViewById<RelativeLayout>(R.id.rl_first_minus)!!
        val rl_first_plus = dialogProductQty.findViewById<RelativeLayout>(R.id.rl_first_plus)
        val tv_packet_title = dialogProductQty.findViewById<TextView>(R.id.tv_packet_title)!!
        val rv_packets = dialogProductQty.findViewById<RecyclerView>(R.id.rv_packets)

        tv_first_text.text = "${productData.product_default_qty}"
        tv_unit_name.text = "${productData.unit_short_name} "
        tv_product_name.text = "${productData.product_name}"
        tv_tottal_qty.text = "${productData.p_map_qty} ${productData.unit_short_name}"
        rv_packets!!.layoutManager = LinearLayoutManager(this@SalesProductDetails, LinearLayoutManager.HORIZONTAL, false)
        packetList.clear()
       // packetList.add(productData.product_packets)
        for (data in packetList) {
            data.isSelected = false
        }
        val mClickListner = object : ClickListner {
            override fun onClick(getProductPackets: GetProductPackets) {
                if (getProductPackets.isSelected) {
                    for (data in packetList) {
                        data.isSelected = false
                    }
                    productQytValue = "${productData.product_default_qty}".toFloat()
                    package_id = ""
                    tv_first_text.text = productQytValue.toString()
                } else {
                    for (data in packetList) {
                        if (data.packet_id == getProductPackets.packet_id) {
                            if (productData.p_map_qty != "" && productData.p_map_qty != null) {
                                if (productData.p_map_qty.toInt() > data.packet_qty.toInt()) {
                                    productQytValue = data.packet_qty.toFloat()
                                    tv_first_text.text = productQytValue.toString()
                                    data.isSelected = true
                                    package_id = data.packet_id
                                } else {
                                    data.isSelected = false
                                    package_id = ""
                                    CUC.displayToast(this@SalesProductDetails, "0", "You can't set ${productData.product_unit_name} more then ${productData.p_map_qty}")
                                }
                            } else {
                                data.isSelected = false
                                package_id = ""
                                CUC.displayToast(this@SalesProductDetails, "0", "You can't set ${productData.product_unit_name} more then ${productData.p_map_qty}")
                            }

                        } else {
                            data.isSelected = false
                            package_id = ""
                        }
                    }
                }
                mPacketAdapter.notifyDataSetChanged()
            }
        }
        mPacketAdapter = PacketAdapter(this@SalesProductDetails, packetList, productData.unit_short_name, Currency_value, mClickListner)
        rv_packets.adapter = mPacketAdapter
        mPacketAdapter.notifyDataSetChanged()
        if (packetList.isEmpty()) {
            tv_packet_title.visibility = View.GONE
        }

        if (productData.p_map_qty == "" || productData.p_map_qty == null || productData.p_map_qty == "0") {
            ll_add_cart.isClickable = false
            ll_add_cart.isEnabled = false
            ll_add_cart.setBackgroundColor(ContextCompat.getColor(this@SalesProductDetails, R.color.gray))
            tv_tottal_qty.text = "Unavailable"
            tv_tottal_qty.setTextColor(ContextCompat.getColor(this@SalesProductDetails, R.color.red))
        }

        rl_first_minus.setOnClickListener {
            if (tv_first_text.text.toString() != "1") {
                productQytValue = tv_first_text.text.toString().toFloat() - incriVal // Math.nextUp()
                tv_first_text.text = String.format(strForm, productQytValue.nextUp())
                var isSelect: Boolean = false
                var packetId = ""
                for (data in packetList) {
                    if (data.packet_qty == productQytValue.toString()) {
                        packetId = data.packet_id
                        isSelect = true
                        break
                    }
                }
                if (isSelect) {
                    for (data in packetList) {
                        data.isSelected = data.packet_id == packetId
                    }
                    package_id = packetId
                } else {
                    for (data in packetList) {
                        data.isSelected = false
                    }
                    package_id = ""
                }
                mPacketAdapter.notifyDataSetChanged()
            }
        }

        rl_first_plus!!.setOnClickListener {
            if (productData.p_map_qty != "" && productData.p_map_qty != null) {
                if (productData.p_map_qty.toInt() > productQytValue) {
                    productQytValue = tv_first_text.text.toString().toFloat() + incriVal // Math.nextUp()
                    tv_first_text.text = String.format(strForm, productQytValue.nextUp())
                    println("CountFormat -  plus : ${String.format(strForm, productQytValue.nextUp())}")
                    var isSelect: Boolean = false
                    var packetId = ""
                    for (data in packetList) {
                        if (data.packet_qty == productQytValue.toString()) {
                            packetId = data.packet_id
                            isSelect = true
                            break
                        }
                    }
                    if (isSelect) {
                        for (data in packetList) {
                            data.isSelected = data.packet_id == packetId
                        }
                        package_id = packetId
                    } else {
                        for (data in packetList) {
                            data.isSelected = false
                        }
                        package_id = ""
                    }
                    mPacketAdapter.notifyDataSetChanged()
                } else {
                    CUC.displayToast(this@SalesProductDetails, "0", "You can't set ${productData.product_unit_name} more then ${productData.p_map_qty}")
                }
            } else {
                CUC.displayToast(this@SalesProductDetails, "0", "You can't set ${productData.product_unit_name} more then ${productData.p_map_qty}")
            }
        }

        ll_add_cart.setOnClickListener {
            qty = "${tv_first_text.text}"
            callEmployeePatientAddtocart()
        }
        dialogProductQty.show()
    }

    class PacketAdapter(val mContext: Context, val packetList: ArrayList<GetProductPackets>, val unit_name: String, val Currency_value: String, val mClickListner: ClickListner) : RecyclerView.Adapter<PacketAdapter.PacketHolder>() {
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PacketHolder {
            val mView = LayoutInflater.from(parent.context).inflate(R.layout.packet_item, parent, false)
            return PacketHolder(mView)
        }

        override fun getItemCount(): Int {
            return packetList.size
        }

        override fun onBindViewHolder(holder: PacketHolder, position: Int) {
            holder.bindVliew(mContext, packetList[position], unit_name, Currency_value, mClickListner)
        }

        class PacketHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
            val tv_packet_qty = itemView.findViewById<TextView>(R.id.tv_packet_qty)
            val tv_packet_price = itemView.findViewById<TextView>(R.id.tv_packet_price)
            val rl_packet_item = itemView.findViewById<RelativeLayout>(R.id.rl_packet_item)

            @SuppressLint("SetTextI18n")
            fun bindVliew(mContext: Context, getProductPackets: GetProductPackets, unit_name: String, Currency_value: String, mClickListner: ClickListner) {
                tv_packet_qty.text = "${getProductPackets.packet_qty}\n$unit_name"
                tv_packet_price.text = "$Currency_value${getProductPackets.packet_total_price}"
                if (getProductPackets.isSelected) {
                    tv_packet_qty.setTextColor(ContextCompat.getColor(mContext, R.color.white))
                    rl_packet_item.setBackgroundResource(R.drawable.packet_select_ring)
                } else {
                    tv_packet_qty.setTextColor(ContextCompat.getColor(mContext, R.color.black))
                    rl_packet_item.setBackgroundResource(R.drawable.packet_ring)
                }
                rl_packet_item.setOnClickListener {
                    mClickListner.onClick(getProductPackets)

                }
            }
        }
    }

    interface ClickListner {
        fun onClick(getProductPackets: GetProductPackets)
    }


    fun callEmployeePatientAddtocart() {
        if (CUC.isNetworkAvailablewithPopup(this@SalesProductDetails)) {
            mDialog = CUC.createDialog(this@SalesProductDetails)
            val apiService = RequetsInterface.Factory.create()
            val parameterMap = HashMap<String, String>()
            parameterMap["api_key"] = "${Preferences.getPreference(this@SalesProductDetails, "API_KEY")}"
            if (logindata != null) {
                parameterMap["store_id"] = logindata.user_store_id
                parameterMap["employee_id"] = logindata.user_id
            }
            parameterMap["patient_id"] = queue_patient_id
            parameterMap["product_id"] = productData.product_id
            parameterMap["category_id"] = productData.p_map_store_category_id
            parameterMap["qty"] = "$qty"
            parameterMap["package_id"] = "$package_id"
            parameterMap["terminal_id"] = "${mAppBasic.terminal_id}"
            parameterMap["emp_lat"] = "${Preferences.getPreference(this@SalesProductDetails, "latitude")}"
            parameterMap["emp_log"] = "${Preferences.getPreference(this@SalesProductDetails, "longitude")}"
            parameterMap["queue_id"] = "$queue_id"
            Log.i("Result", "parameterMap : $parameterMap")
            CompositeDisposable().add(apiService.callEmployeeSalesAddcart(parameterMap)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe({ result ->
                        if (mDialog != null && mDialog.isShowing)
                            mDialog.dismiss()
                        if (result != null) {
                            Log.i("Result", "" + result.toString())
                            if (result.status == "0") {
                                //Preferences.setPreference(this@EmployeeProductDetails, "total_qty", "${result.total_qty}")
                                val i = Intent("android.intent.action.MAINQTY")
                                this@SalesProductDetails.sendBroadcast(i)
                                if (dialogProductQty != null && dialogProductQty.isShowing) {
                                    dialogProductQty.dismiss()
                                }
                                CUC.displayToast(this@SalesProductDetails, result.show_status, result.message)
                            } else if (result.status == "10") {
                                apiClass!!.callApiKey(EMPLOYEEPATIENTADDTOCART)
                            } else {
                                CUC.displayToast(this@SalesProductDetails, result.show_status, result.message)
                            }

                        } else {
                         //   CUC.displayToast(this@SalesProductDetails, "0", getString(R.string.connection_to_database_failed))
                        }

                    }, { error ->
                        if (mDialog != null && mDialog.isShowing)
                            mDialog.dismiss()
                        CUC.displayToast(this@SalesProductDetails, "0", getString(R.string.connection_to_database_failed))
                        error.printStackTrace()
                    })
            )
        }
    }
}