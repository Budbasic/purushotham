package com.budbasic.posconsole.sales;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.budbasic.posconsole.reception.ProductsFragment;
import com.kotlindemo.model.GetCartData;
import com.kotlindemo.model.GetData;
import com.kotlindemo.model.GetEmployeeLogin;
import com.kotlindemo.model.GetPaymentTypes;
import com.kotlindemo.model.GetQueueList;

import java.util.ArrayList;
import java.util.List;

public class PayViewPagerAdapter extends FragmentPagerAdapter {

    List<Fragment> mFragmentCollection = new ArrayList<>();
    List<String> mTitleCollection = new ArrayList<>();

    public PayViewPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    public void addFragment(String title, Fragment fragment)
    {
        mTitleCollection.add(title);
        mFragmentCollection.add(fragment);
    }

    //Needed for
    @Override
    public CharSequence getPageTitle(int position) {
        return mTitleCollection.get(position);
    }

    @Override
    public Fragment getItem(int position) {
        return mFragmentCollection.get(position);
    }

    @Override
    public int getCount() {
        return mFragmentCollection.size();
    }


}
