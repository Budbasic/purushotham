package com.budbasic.posconsole.sales

import android.annotation.SuppressLint
import android.app.Activity
import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.bluetooth.BluetoothSocket
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Color
import android.os.Bundle
import android.os.Handler
import android.os.Message
import android.support.v4.widget.CircularProgressDrawable
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.*
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import com.budbasic.customer.global.Preferences
import com.budbasic.global.CUC
import com.budbasic.posconsole.R
import com.budbasic.posconsole.global.RasterDocument
import com.budbasic.posconsole.global.StarBitmap
import com.budbasic.posconsole.printer.fragment.BluetoothScan
import com.budbasic.posconsole.printer.sdk.BluetoothService
import com.budbasic.posconsole.printer.sdk.Command
import com.budbasic.posconsole.webservice.RequetsInterface
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DecodeFormat
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.SimpleTarget
import com.bumptech.glide.request.transition.Transition
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.google.zxing.BarcodeFormat
import com.google.zxing.EncodeHintType
import com.google.zxing.Writer
import com.google.zxing.WriterException
import com.google.zxing.oned.Code128Writer
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel
import com.kotlindemo.model.*
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_receipt_print.*
import kotlinx.android.synthetic.main.print_barcode.rlLogos
import kotlinx.android.synthetic.main.print_barcode.*
import zj.com.customize.sdk.Other
import java.io.IOException
import java.io.InputStream
import java.io.OutputStream
import java.io.UnsupportedEncodingException
import java.text.DecimalFormat
import java.util.*

class CheckPrintBarcode : Activity() {

    // Local Bluetooth adapter
    var printerstatusrecep: String? = null
    var printertyperecep: String? = null
    var coonecteddeviceadd: String? = null

    lateinit var data: GetEmployeeQenerateInvoice

    // Member object for the services
    var mService: BluetoothService? = null
    var invoiceType: String? = null
    var mBitmap: Bitmap? = null
    // Intent request codes
    private val REQUEST_CONNECT_DEVICE = 1
    private val REQUEST_ENABLE_BT = 2

    // Message types sent from the BluetoothService Handler
    val MESSAGE_STATE_CHANGE = 1
    val MESSAGE_READ = 2
    val MESSAGE_WRITE = 3
    val MESSAGE_DEVICE_NAME = 4
    val MESSAGE_TOAST = 5
    val MESSAGE_CONNECTION_LOST = 6
    val MESSAGE_UNABLE_CONNECT = 7

    private val TAG = "Main_Activity"
    private val DEBUG = true

    // Key names received from the BluetoothService Handler
    val DEVICE_NAME = "device_name"
    val TOAST = "toast"
    // Name of the connected device
    private val CHINESE = "GBK"

    private var mConnectedDeviceName: String? = null
    var Currency_value = ""
    lateinit var logindata: GetEmployeeLogin
    lateinit var appSettingData: GetEmployeeAppSetting

    private var mBluetoothAdapter: BluetoothAdapter? = null
   // internal var mBluetoothAdapter: BluetoothAdapter? = null
    internal lateinit var mmSocket: BluetoothSocket
    internal lateinit var mmDevice: BluetoothDevice

    // needed for communication to bluetooth device / network
    internal lateinit var mmOutputStream: OutputStream
    internal lateinit var mmInputStream: InputStream
    internal lateinit var workerThread: Thread

    internal lateinit var readBuffer: ByteArray
    internal var readBufferPosition: Int = 0
    @Volatile
    internal var stopWorker: Boolean = false
    internal lateinit var btMap: Bitmap
    lateinit var  imgbarcode:ImageView

    val df: DecimalFormat = DecimalFormat("0.00")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN)
        setContentView(R.layout.print_barcode)
        Log.d("kjjhjj1", "-1.00.99")
        Log.i("kjjhjj1", "-1.00")


        appSettingData = Gson().fromJson(Preferences.getPreference(this@CheckPrintBarcode, "AppSetting"), GetEmployeeAppSetting::class.java)
        logindata = Gson().fromJson(Preferences.getPreference(this@CheckPrintBarcode, "logindata"), GetEmployeeLogin::class.java)
        Currency_value = "${appSettingData.Currency_value}"
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter()
        init()

    }

    fun init() {

        try {
            findBT()
            openBT()
        } catch (ex: IOException) {
            ex.printStackTrace()
        }

        imgbarcode=findViewById(R.id.imgbarcode)
        // startPrinter()
        if (mBluetoothAdapter == null) {
            Toast.makeText(this@CheckPrintBarcode, "Bluetooth is not available", Toast.LENGTH_LONG).show()
            //finish()
        }

        tv_print.setOnClickListener {

            try {
                sendData()
            } catch (ex: IOException) {
                ex.printStackTrace()
            }

            /*try {
                val productId = "hello"
                val hintMap = Hashtable<EncodeHintType, ErrorCorrectionLevel>()
                hintMap[EncodeHintType.ERROR_CORRECTION] = ErrorCorrectionLevel.L
                val codeWriter: Writer
                codeWriter = Code128Writer()
                val byteMatrix = codeWriter.encode(productId, BarcodeFormat.CODE_128, 400, 200, hintMap)
                val width = byteMatrix.width
                val height = byteMatrix.height
                val bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888)
                for (i in 0 until width) {
                    for (j in 0 until height) {
                        bitmap.setPixel(i, j, if (byteMatrix.get(i, j)) Color.BLACK else Color.WHITE)
                    }
                }
                imgbarcode.setImageBitmap(bitmap)
                sendPrintReceipt(bitmap)

            } catch (e: Exception) {
                Toast.makeText(applicationContext, e.message, Toast.LENGTH_LONG).show()
            }*/

        }

    }
    fun sendData()  {
        try {
            // the text typed by the user
            var msg = "hello"

            println("aaaaaaaaaaa  msg $msg")
            // mmOutputStream.write(msg.getBytes());
            mmOutputStream.write(msg.toByteArray())

            // tell the user data were sent

            println("aaaaaaaa   Data Sent")

        } catch (e: Exception) {
            e.printStackTrace()
            println("aaaaaaaaaaaaa  error  " + e.message)
            Toast.makeText(this, "" + e.message, Toast.LENGTH_SHORT).show()
        }

    }

    override fun onResume() {
        super.onResume()
        if (mService != null) {
            if (mService?.state == BluetoothService.STATE_NONE) {
                // Start the Bluetooth services
                mService?.start()
            }
        }
      //  startPrinter()
    }

    override fun onPause() {
        super.onPause()
        if (mService != null)
            mService!!.stop()
    }




    /*
     *SendDataByte
     */
    private fun SendDataByte(data: ByteArray) {
        if (mService!!.state != BluetoothService.STATE_CONNECTED) {
            Toast.makeText(this@CheckPrintBarcode, R.string.not_connected, Toast.LENGTH_SHORT)
                    .show()
            return
        }
        mService?.write(data)
    }

    private fun SendDataString(data: String) {
        Log.i("BTPWRITE", data)
        if (mService!!.getState() !== BluetoothService.STATE_CONNECTED) {
            Toast.makeText(this@CheckPrintBarcode, getText(R.string.not_connected), Toast.LENGTH_SHORT)
                    .show()
            return
        }
        if (data.length > 0) {
            try {
                Log.i("BTPWRITE", data)
                mService!!.write(data.toByteArray(charset("GBK")))
            } catch (e: UnsupportedEncodingException) {
                e.printStackTrace()
            }

        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        Log.i("Result", "Main onActivityResult(), $requestCode : $resultCode")
        //fragment.onActivityResult(requestCode, resultCode, data)
        if (DEBUG)
            Log.d(TAG, "onActivityResult $resultCode")
        when (requestCode) {
            2121 -> {
                //mService = null
                //startPrinter()
            }
            REQUEST_CONNECT_DEVICE -> {
                // When DeviceListActivity returns with a device to connect
                if (resultCode == Activity.RESULT_OK) {
                    // Get the device MAC address
                    val address = data?.getExtras()!!.getString(
                            BluetoothScan.EXTRA_DEVICE_ADDRESS)
                    // Get the BLuetoothDevice object
                    if (BluetoothAdapter.checkBluetoothAddress(address)) {
                        val device = mBluetoothAdapter!!.getRemoteDevice(address)
                        coonecteddeviceadd = device.name + " " + device.address
                        // Attempt to connect to the device
                        mService?.connect(device)
                    }
                }
            }
            REQUEST_ENABLE_BT -> {
                // When the request to enable Bluetooth returns
               /* if (resultCode == Activity.RESULT_OK) {
                    if (mService == null)
                      //  mService = BluetoothService(this@CheckPrintBarcode, mHandler)
                    // Bluetooth is now enabled, so set up a session
                } else {
                    // User did not enable Bluetooth or an error occured
                    Log.d(TAG, "BT not enabled")
                    Toast.makeText(this@CheckPrintBarcode, R.string.bt_not_enabled_leaving, Toast.LENGTH_SHORT).show()
                    //finish()
                }*/
            }
        }
    }

    override fun onBackPressed() {
        //super.onBackPressed()
        setResult(Activity.RESULT_OK)
        finish()
    }


    fun sendPrintReceipt(mBitmap: Bitmap) = if (mBluetoothAdapter != null) {
        if (mBluetoothAdapter!!.isEnabled) {
           // if (mService != null) {
                /*if (mService!!.getState() !== BluetoothService.STATE_CONNECTED) {
                    Toast.makeText(this@CheckPrintBarcode, getText(R.string.not_connected), Toast.LENGTH_SHORT).show()
                    //this@CheckPrintBarcode.setResult(Activity.RESULT_OK)
                   // finish()
                } else {*/
                    println("aaaaaaaaa   else print connected")
                    val nMode = 0
                    val nPaperWidth = 576
                   // val nPaperWidth = 700
                    val mCommands: ArrayList<ByteArray> = ArrayList()

                    val rasterDoc = RasterDocument(RasterDocument.RasSpeed.Medium, RasterDocument.RasPageEndMode.None,
                            RasterDocument.RasPageEndMode.None, RasterDocument.RasTopMargin.Small, 0, 0, 0)
                    rlLogos.isDrawingCacheEnabled =true
                    rlLogos.buildDrawingCache()

                    var newBitmap  = rlLogos.getDrawingCache()
                    val starbitmap = StarBitmap(newBitmap, false, nPaperWidth)
                    Command.alignmentCommand[3] = 49
                    SendDataByte(Command.alignmentCommand)
                    mCommands.add(rasterDoc.BeginDocumentCommandData())
                    mCommands.add(starbitmap.getImageRasterDataForPrinting_Standard(true))
                    mCommands.add(rasterDoc.EndDocumentCommandData())
                    for (data in mCommands) {
                        SendDataByte(data)
                    }

               // }
            /*} else {
                println("aaaaaaaaaaaa   not enabled")
               // setResult(Activity.RESULT_OK)
               // finish()
            }*/
        } else {
            println("aaaaaaaaaaaa   not enabled else")
//            val enableIntent = Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE)
//            startActivityForResult(enableIntent, REQUEST_ENABLE_BT)
        }
    } else {
        Toast.makeText(this@CheckPrintBarcode, "Bluetooth is not available", Toast.LENGTH_LONG).show()
    }

    internal fun findBT() {

        try {
            mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter()



            if (mBluetoothAdapter == null) {
                println("aaaaaaaaaaa    no blooth connected")
            }

            if (mBluetoothAdapter!!.isEnabled()) {
                val enableBluetooth = Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE)
                startActivityForResult(enableBluetooth, 0)
            }

            val pairedDevices = mBluetoothAdapter!!.getBondedDevices()

            if (pairedDevices.size > 0) {
                for (device in pairedDevices) {
                    println("aaaaaaaaaa address  " + device.address + "  name  " + device.name + "  uuid " + device.uuids)
                    // RPP300 is the name of the bluetooth printer device
                    // we got this name from the list of paired devices
                    if (device.name == "NTPL Label Print") {
                        mmDevice = device
                       // openBT()
                        println("aaaaaaaaaaa  ifff   equal")
                        break
                    }
                }
            }


        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    @Throws(IOException::class)
    internal fun openBT() {
        try {

            // Standard SerialPortService ID
            val uuid = UUID.fromString("00001101-0000-1000-8000-00805f9b34fb")
            mmSocket = mmDevice.createRfcommSocketToServiceRecord(uuid)
            mmSocket.connect()
            mmOutputStream = mmSocket.getOutputStream()
            mmInputStream = mmSocket.getInputStream()

            beginListenForData()

            println("aaaaaaaaaaaa  blooth opend ")

        } catch (e: Exception) {
            e.printStackTrace()
            println("aaaaaaaaaaaa  blooth not opend "+e.message)
        }

    }


    internal fun beginListenForData() {
        try {
            val handler = Handler()

            // this is the ASCII code for a newline character
            val delimiter: Byte = 10

            stopWorker = false
            readBufferPosition = 0
            readBuffer = ByteArray(1024)

            workerThread = Thread(Runnable {
                while (!Thread.currentThread().isInterrupted && !stopWorker) {

                    try {

                        val bytesAvailable = mmInputStream.available()

                        if (bytesAvailable > 0) {

                            val packetBytes = ByteArray(bytesAvailable)
                            mmInputStream.read(packetBytes)

                            for (i in 0 until bytesAvailable) {

                                val b = packetBytes[i]
                                if (b == delimiter) {

                                    val encodedBytes = ByteArray(readBufferPosition)
                                    System.arraycopy(
                                            readBuffer, 0,
                                            encodedBytes, 0,
                                            encodedBytes.size
                                    )

                                    // specify US-ASCII encoding
                                   // val data = String(encodedBytes, "US-ASCII")
                                    readBufferPosition = 0

                                    // tell the user data were sent to bluetooth printer device
                                    handler.post {
                                      //  myLabel.setText(data)
                                    }

                                } else {
                                    readBuffer[readBufferPosition++] = b
                                }
                            }
                        }

                    } catch (ex: IOException) {
                        stopWorker = true
                    }

                }
            })

            workerThread.start()

        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

}