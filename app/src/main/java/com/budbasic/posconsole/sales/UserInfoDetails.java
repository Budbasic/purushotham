package com.budbasic.posconsole.sales;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.budbasic.customer.global.Preferences;
import com.budbasic.global.CUC;
import com.budbasic.posconsole.R;
import com.budbasic.posconsole.adapter.DocumentShowAdapter;
import com.budbasic.posconsole.adapter.NotesAdaper;
import com.budbasic.posconsole.adapter.PatientAdapterInfo;
import com.budbasic.posconsole.adapter.PattientFavouriteAdapter;
import com.budbasic.posconsole.adapter.PattientRecomendedAdapter;
import com.budbasic.posconsole.dialogs.AddProductDialog;
import com.budbasic.posconsole.reception.ProductsFragment;
import com.budbasic.posconsole.reception.SalesActivity;
import com.budbasic.posconsole.webservice.RequetsInterface;
import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.kotlindemo.model.EmployeePatientInfo;
import com.kotlindemo.model.Favourite;
import com.kotlindemo.model.GetEmployeeAppSetting;
import com.kotlindemo.model.GetEmployeeLogin;
import com.kotlindemo.model.GetEmployeeSalesProducts;
import com.kotlindemo.model.GetSalesProduct;
import com.kotlindemo.model.GetStatus;
import com.kotlindemo.model.InvoiceHistory;
import com.kotlindemo.model.getEmployeeBannedPatient;
import com.kotlindemo.model.notesdata;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import kotlin.jvm.internal.Intrinsics;

public class UserInfoDetails extends AppCompatActivity {

    private ImageView imgUserProfile,closeActivity;
    private TextView tv_name,tv_dob,tv_licence,tv_avgSpeed,tv_avgSpeedData,tv_spentLimit,tv_member_since,txtNodataFound,txtNoDataFoundFavourite,
            txtNoDataFoundRecommended,tvSelectDate,tv_search,textDocument,textNotes,tv_nodata;
    private RelativeLayout rl_speed,ll_addnote;
    private RecyclerView rc_purchase_history,recycle_favourite,recycler_recommended,rc_document,rc_noterecycle;
    public GetEmployeeLogin logindata;
    private Gson gson;
    private String patient_id,queue_id;
    private GetEmployeeAppSetting appSettingData;
    private PatientAdapterInfo patientAdapterInfo;
    private PattientFavouriteAdapter pattientFavouriteAdapter;
    public Dialog mDialog;
    GetEmployeeAppSetting mBAsis;
    private int mYear,mMonth,mDay,cyear,cmonth,cdate;
    private EmployeePatientInfo result;
    private NotesAdaper notesAdaper;
    private ArrayList<InvoiceHistory> historyinvoice,historymodified;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_user_info_details);

        imgUserProfile=findViewById(R.id.imgUserProfile);
        tv_name=findViewById(R.id.tv_name);
        tv_dob=findViewById(R.id.tv_dob);
        tv_licence=findViewById(R.id.tv_licence);
        rl_speed=findViewById(R.id.rl_speed);
        tv_avgSpeed=findViewById(R.id.tv_avgSpeed);
        tv_avgSpeedData=findViewById(R.id.tv_avgSpeedData);
        tv_spentLimit=findViewById(R.id.tv_spentLimit);
        tv_member_since=findViewById(R.id.tv_member_since);
        txtNodataFound=findViewById(R.id.txtNodataFound);
        rc_purchase_history=findViewById(R.id.rc_purchase_history);
        recycle_favourite=findViewById(R.id.recycle_favourite);
        txtNoDataFoundFavourite=findViewById(R.id.txtNoDataFoundFavourite);
        txtNoDataFoundRecommended=findViewById(R.id.txtNoDataFoundRecommended);
        recycler_recommended=findViewById(R.id.recycler_recommended);
        rc_document=findViewById(R.id.rc_document);
        tvSelectDate=findViewById(R.id.tvSelectDate);
        closeActivity=findViewById(R.id.closeActivity);
        tv_search=findViewById(R.id.tv_search);
        ll_addnote=findViewById(R.id.ll_addnote);
        rc_noterecycle=findViewById(R.id.rc_noterecycle);
        textDocument=findViewById(R.id.textDocument);
        textNotes=findViewById(R.id.textNotes);
        tv_nodata=findViewById(R.id.tv_nodata);

        historyinvoice=new ArrayList<InvoiceHistory>();
        historymodified=new ArrayList<InvoiceHistory>();

        rc_purchase_history.setLayoutManager(new GridLayoutManager(UserInfoDetails.this,1));
        rc_noterecycle.setLayoutManager(new GridLayoutManager(UserInfoDetails.this,1));
       /* recycle_favourite.setLayoutManager(new GridLayoutManager(UserInfoDetails.this,1));
        recycler_recommended.setLayoutManager(new GridLayoutManager(UserInfoDetails.this,1));
*/
        patient_id=getIntent().getStringExtra("patient_id");
        queue_id=getIntent().getStringExtra("queue_id");
        gson=new Gson();
        logindata = gson.fromJson(getIntent().getStringExtra("logindata"), GetEmployeeLogin.class);
        appSettingData = (GetEmployeeAppSetting)(new Gson()).fromJson(Preferences.INSTANCE.getPreference((Context)this, "AppSetting"), GetEmployeeAppSetting.class);
        mBAsis = gson.fromJson(Preferences.INSTANCE.getPreference(UserInfoDetails.this, "AppSetting"), GetEmployeeAppSetting.class);

        System.out.println("aaaaaaaaaaaa  login  "+logindata.toString());

        getapiCallForEmployeeDocument();

         Calendar c = Calendar.getInstance();
         mYear = c.get(Calendar.YEAR);
         mMonth = c.get(Calendar.MONTH);
         mDay = c.get(Calendar.DAY_OF_MONTH);
        tvSelectDate.setText(mDay+"-"+(mMonth+1)+"-"+mYear);
        tvSelectDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DatePickerDialog datePickerDialog = new DatePickerDialog(UserInfoDetails.this,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {

                                tvSelectDate.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);
                                cmonth=(monthOfYear+1);
                                cyear=year;
                                cdate=dayOfMonth;

                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.show();
            }
        });
        tv_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (tv_search.getText().toString().equalsIgnoreCase("Clear")){
                    patientAdapterInfo.setChanged(historyinvoice);
                    txtNodataFound.setVisibility(View.GONE);
                    rc_purchase_history.setVisibility(View.VISIBLE);
                    tv_search.setText("Search");
                }else {
                    historymodified.clear();
                    for (int i=0;i<historyinvoice.size();i++){
                        String[] historydate = historyinvoice.get(i).getInvoice_created_date().split(" ");
                        if (historydate[0].equals(cyear+"-"+cmonth+"-"+cdate)){
                            historymodified.add(historyinvoice.get(i));
                        }
                    }
                    tv_search.setText("Clear");
                    if (historymodified.size()!=0){
                        txtNodataFound.setVisibility(View.GONE);
                        rc_purchase_history.setVisibility(View.VISIBLE);
                        patientAdapterInfo.setChanged(historymodified);
                    }else {
                        txtNodataFound.setVisibility(View.VISIBLE);
                        rc_purchase_history.setVisibility(View.GONE);
                    }
                }
            }
        });
        ll_addnote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AddProductDialog addProductDialog=new AddProductDialog();
                addProductDialog.showDialog(UserInfoDetails.this,"0");
            }
        });
        closeActivity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        textNotes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                textNotes.setBackgroundDrawable(getResources().getDrawable(R.drawable.gradiant_line_background));
                textDocument.setBackgroundDrawable(getResources().getDrawable(R.drawable.gray_line_background_toside));

                if (result.getNotes_data().size()>0){
                    rc_noterecycle.setVisibility(View.VISIBLE);
                    rc_document.setVisibility(View.GONE);
                    tv_nodata.setVisibility(View.GONE);
                    notesAdaper.dataChanged(result.getNotes_data());
                }else {
                    tv_nodata.setVisibility(View.VISIBLE);
                    tv_nodata.setText("No Notes Found");
                    rc_noterecycle.setVisibility(View.GONE);
                    rc_document.setVisibility(View.GONE);
                }
            }
        });textDocument.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                textDocument.setBackgroundDrawable(getResources().getDrawable(R.drawable.gradiant_line_background));
                textNotes.setBackgroundDrawable(getResources().getDrawable(R.drawable.gray_line_background_toside));

                if (result.getDocuments().size()>0){
                    rc_noterecycle.setVisibility(View.GONE);
                    tv_nodata.setVisibility(View.GONE);
                    rc_document.setVisibility(View.VISIBLE);
                    notesAdaper.dataChanged(result.getNotes_data());
                }else {
                    tv_nodata.setVisibility(View.VISIBLE);
                    tv_nodata.setText("No Documents Found");
                    rc_noterecycle.setVisibility(View.GONE);
                    rc_document.setVisibility(View.GONE);
                }
            }
        });
    }

    public void getapiCallForEmployeeDocument(){
        if (CUC.Companion.isNetworkAvailablewithPopup((Context)this)) {
            mDialog = CUC.Companion.createDialog(UserInfoDetails.this);
            // mDialog = CUC.Companion.createDialog(SalesActivity.this);
            RequetsInterface apiService = RequetsInterface.Factory.create();
            Map parameterMap =new HashMap<String, String>();
            parameterMap.put("api_key", String.valueOf(Preferences.INSTANCE.getPreference((Context)this, "API_KEY")));

            if (logindata != null) {
                parameterMap.put("employee_id", logindata.getUser_store_id());
                parameterMap.put("patient_id", patient_id);
                parameterMap.put("store_id", logindata.getUser_store_id());
            }

            Log.i("aaaaaaaa   Result", "parameterMap : " + parameterMap);
            (new CompositeDisposable()).add(apiService.callEmployeePatientInfo(parameterMap).observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io()).subscribe((Consumer)(new Consumer() {
                // $FF: synthetic method
                // $FF: bridge method
                public void accept(Object var1) {
                    this.accept((EmployeePatientInfo)var1);
                }

                public final void accept(EmployeePatientInfo result) {
                     mDialog.cancel();
                    if (result != null) {
                        System.out.println("aaaaaaaaa  result  "+result.toString());
                        Log.i("aaaaaaa   Result", "" + result.toString());
                        if (Intrinsics.areEqual(result.getStatus(), "0")) {
                            if (result.getStatus().equals("0")) {
                                System.out.println("aaaaaaaaaaa  favourites  "+result.getFavourites());
                                setData(result);
                                if (appSettingData != null) {
                                    Glide.with(UserInfoDetails.this).load(appSettingData.getPatient_image_path() + result.getPatient().getPatient_image()).into(imgUserProfile);
                                } else {
                                    Glide.with(UserInfoDetails.this).load(result.getPatient().getPatient_image()).into(imgUserProfile);
                                }
                            }
                            CUC.Companion.displayToast(UserInfoDetails.this, result.getShow_status(), result.getMessage());
                        }  else {
                            Toast.makeText(UserInfoDetails.this, ""+result.getShow_status(), Toast.LENGTH_SHORT).show();
                            CUC.Companion.displayToast((Context)UserInfoDetails.this, result.getShow_status(), result.getMessage());
                        }
                    } else {
                    }
                }
            }), (Consumer)(new Consumer() {
                // $FF: synthetic method
                // $FF: bridge method
                public void accept(Object var1) {
                    this.accept((Throwable)var1);
                }

                public final void accept(Throwable error) {
                    mDialog.cancel();
                    Toast.makeText(UserInfoDetails.this, getString(R.string.connection_to_database_failed), Toast.LENGTH_SHORT).show();
                    error.printStackTrace();
                    System.out.println("aaaaaaaa  error "+error.getMessage());
                }
            })));

        }else {
            System.out.println("aaaaaaaaaa   else ");
        }
    }

    public void setData(EmployeePatientInfo result){
        this.result=result;
        tv_name.setText(result.getPatient().getPatient_fname()+" "+ result.getPatient().getPatient_lname());
        tv_dob.setText(result.getPatient().getPatient_dob());
        tv_licence.setText(result.getPatient().getPatient_licence_no());
        tv_avgSpeedData.setText( result.getAverage_spent());
        tv_spentLimit.setText(result.getLimit());
        tv_member_since.setText("Member since:"+ result.getRegistered_since());

        if (result.getInvoice_history() != null && result.getInvoice_history().size() > 0) {
            txtNodataFound.setVisibility(View.GONE);
            rc_purchase_history.setVisibility(View.VISIBLE);
            historyinvoice.addAll(result.getInvoice_history());
            String patientName =  result.getPatient().getPatient_fname()+" "+ result.getPatient().getPatient_lname();
            patientAdapterInfo =new PatientAdapterInfo(this, patientName, result.getInvoice_history());
            rc_purchase_history.setAdapter(patientAdapterInfo);
        } else {
            txtNodataFound.setVisibility(View.VISIBLE);
            rc_purchase_history.setVisibility(View.GONE);
        }

        if (result.getFavourites().size() > 0) {
           // System.out.println("aaaaaaaaaaaaaaa  getPacket_unit   "+result.getFavourites().get(0).getProduct_packats().getPacket_unit());
            txtNoDataFoundFavourite.setVisibility(View.GONE);
            recycle_favourite.setVisibility(View.VISIBLE);
             pattientFavouriteAdapter =new PattientFavouriteAdapter(this, patient_id, appSettingData, logindata, result.getFavourites());
            recycle_favourite.setLayoutManager(new LinearLayoutManager(UserInfoDetails.this, LinearLayoutManager.HORIZONTAL, false));
            recycle_favourite.setAdapter(pattientFavouriteAdapter);
        } else {
            recycle_favourite.setVisibility(View.GONE);
            txtNoDataFoundFavourite.setVisibility(View.VISIBLE);
        }

        if (result.getRecomended().size() > 0) {
            recycler_recommended.setVisibility(View.VISIBLE);
            txtNoDataFoundRecommended.setVisibility(View.GONE);
            PattientRecomendedAdapter favouriteAdapter1 =new PattientRecomendedAdapter(this, result.getRecomended());
            recycler_recommended.setLayoutManager(new LinearLayoutManager(UserInfoDetails.this,LinearLayoutManager.HORIZONTAL,false));
            recycler_recommended.setAdapter(favouriteAdapter1);
        } else {
            recycler_recommended.setVisibility(View.GONE);
            txtNoDataFoundRecommended.setVisibility(View.VISIBLE);
        }

        if (result.getNotes_data().size()>0){
             notesAdaper=new NotesAdaper(UserInfoDetails.this,result.getNotes_data());
            rc_noterecycle.setAdapter(notesAdaper);
        }else {
            tv_nodata.setText("No Notes Found");
        }

        if (result.getDocuments().size() > 0) {
            DocumentShowAdapter documentShowAdapter = new DocumentShowAdapter(this, result.getDocument_path(), this, result.getDocuments());
            rc_document.setLayoutManager(new GridLayoutManager(UserInfoDetails.this,1));
            rc_document.setAdapter(documentShowAdapter);
        }
    }

    public void setFavourite(Favourite favourite,int position) {
        getStores(favourite,position);
    }

    public void getStores(final Favourite favourite, final int position){
        System.out.println("aaaaaaaaaa  position  "+position+"    "+favourite.toString());
        mDialog = CUC.Companion.createDialog(UserInfoDetails.this);
        RequetsInterface apiService = RequetsInterface.Factory.create();
        Map parameterMap =new HashMap<String, String>();
        parameterMap.put("api_key", String.valueOf(Preferences.INSTANCE.getPreference((UserInfoDetails.this), "API_KEY")));

        if (logindata != null) {
            parameterMap.put("store_id", logindata.getUser_store_id());
            parameterMap.put("cart_detail_id", "");
            parameterMap.put("product_id", patient_id);
            parameterMap.put("packet_id", favourite.getPacket_id());
        }

        System.out.println("aaaaaaaaaaa  parameterMap "+parameterMap);
        Log.i("aaaaaaaa   Result", "parameterMap : " + parameterMap);
        (new CompositeDisposable()).add(apiService.callEmployeeProductQty(parameterMap).
                observeOn(AndroidSchedulers.mainThread()).
                subscribeOn(Schedulers.io()).subscribe((Consumer)(new Consumer() {
            // $FF: synthetic method
            // $FF: bridge method
            public void accept(Object var1) {
                this.accept((GetStatus)var1);
            }

            public final void accept(GetStatus result) {
                mDialog.cancel();
                if (result != null) {
                    mDialog.cancel();
                    System.out.println("aaaaaaaaa  result  "+result.toString());
                    Log.i("aaaaaaa   Result", "" + result.toString());
                    if (Intrinsics.areEqual(result.getStatus(), "0")) {
                      /* if (result.getInventory_qty().equals("0")){
                           Toast.makeText(getContext(), "Currently Products Not There", Toast.LENGTH_SHORT).show();
                           CUC.Companion.displayToast(getContext(), "Currently Products Not There", result.getMessage());
                       }else {*/

                        AddProductDialog addProductDialog=new AddProductDialog();
                        addProductDialog.showDialog(UserInfoDetails.this,result,favourite,position);
                        // }
                        CUC.Companion.displayToast(UserInfoDetails.this, result.getShow_status(), result.getMessage());
                    } else if (Intrinsics.areEqual(result.getStatus(), "10")) {

                    } else {
                        Toast.makeText(UserInfoDetails.this, ""+result.getShow_status(), Toast.LENGTH_SHORT).show();
                        CUC.Companion.displayToast((Context)UserInfoDetails.this, result.getShow_status(), result.getMessage());
                    }
                } else {
                    mDialog.cancel();
                }
            }
        }), (Consumer)(new Consumer() {
            // $FF: synthetic method
            // $FF: bridge method
            public void accept(Object var1) {
                this.accept((Throwable)var1);
            }

            public final void accept(Throwable error) {
                mDialog.cancel();
                Toast.makeText(UserInfoDetails.this, getString(R.string.connection_to_database_failed), Toast.LENGTH_SHORT).show();
                error.printStackTrace();
                System.out.println("aaaaaaaa  error "+error.getMessage());
            }
        })));
    }

    public void setAddCart(final Favourite favourite, Float value){

        mDialog = CUC.Companion.createDialog(UserInfoDetails.this);
        RequetsInterface apiService = RequetsInterface.Factory.create();
        Map parameterMap =new HashMap<String, String>();
        parameterMap.put("api_key", String.valueOf(Preferences.INSTANCE.getPreference((UserInfoDetails.this), "API_KEY")));

        if (logindata != null) {
            parameterMap.put("store_id", logindata.getUser_store_id());
            parameterMap.put("employee_id", logindata.getUser_id());
        }
        parameterMap.put("package_id",favourite.getPacket_id());
        parameterMap.put("patient_id",patient_id);
        parameterMap.put("product_id", favourite.getPacket_id());
        parameterMap.put("category_id", favourite.getPacket_category_id());
        parameterMap.put("package_id", favourite.getPacket_id());
        parameterMap.put("qty", ""+value);
        parameterMap.put("terminal_id", mBAsis.getTerminal_id());
        parameterMap.put("emp_lat", Preferences.INSTANCE.getPreference(UserInfoDetails.this, "latitude"));
        parameterMap.put("emp_log", Preferences.INSTANCE.getPreference(UserInfoDetails.this, "longitude"));
        parameterMap.put("queue_id", queue_id);

        System.out.println("aaaaaaaaaaa  parameterMap add cart "+parameterMap);
        Log.i("aaaaaaaa   Result", "parameterMap : " + parameterMap);
        (new CompositeDisposable()).add(apiService.callEmployeeSalesAddcart(parameterMap).
                observeOn(AndroidSchedulers.mainThread()).
                subscribeOn(Schedulers.io()).subscribe((Consumer)(new Consumer() {
            // $FF: synthetic method
            // $FF: bridge method
            public void accept(Object var1) {
                this.accept((GetEmployeeSalesProducts)var1);
            }

            public final void accept(GetEmployeeSalesProducts result) {
                mDialog.cancel();
                if (result != null) {
                    mDialog.cancel();

                    System.out.println("aaaaaaaaa  result  "+result.toString());
                    Log.i("aaaaaaa   Result", "" + result.toString());
                    if (Intrinsics.areEqual(result.getStatus(), "0")) {
                        System.out.println("aaaaaaaaaa  addcart sucess");
                        finish();
                        CUC.Companion.displayToast(UserInfoDetails.this, result.getShow_status(), result.getMessage());
                    } else if (Intrinsics.areEqual(result.getStatus(), "10")) {

                    } else {
                        Toast.makeText(UserInfoDetails.this, ""+result.getShow_status(), Toast.LENGTH_SHORT).show();
                        CUC.Companion.displayToast((Context)UserInfoDetails.this, result.getShow_status(), result.getMessage());
                    }
                } else {
                    mDialog.cancel();
                }
            }
        }), (Consumer)(new Consumer() {
            // $FF: synthetic method
            // $FF: bridge method
            public void accept(Object var1) {
                this.accept((Throwable)var1);
            }

            public final void accept(Throwable error) {
                mDialog.cancel();
                Toast.makeText(UserInfoDetails.this, getString(R.string.connection_to_database_failed), Toast.LENGTH_SHORT).show();
                error.printStackTrace();
                System.out.println("aaaaaaaa  error "+error.getMessage());
            }
        })));
    }

    public void sendnote(String notename, String notemsg,int value) {
        System.out.println("aaaaaaaaaaaaa   value  "+value);
        mDialog = CUC.Companion.createDialog(UserInfoDetails.this);
        RequetsInterface apiService = RequetsInterface.Factory.create();
        Map parameterMap =new HashMap<String, String>();
        parameterMap.put("api_key", String.valueOf(Preferences.INSTANCE.getPreference((UserInfoDetails.this), "API_KEY")));
        if (logindata != null) {
            parameterMap.put("employee_id", logindata.getUser_id());
        }
        parameterMap.put("patient_id",patient_id);
        parameterMap.put("notes_title",notename);
        parameterMap.put("notes",notemsg);
        parameterMap.put("notes_priority",""+value);

        System.out.println("aaaaaaaaaaa  parameterMap add note "+parameterMap);
        Log.i("aaaaaaaa   Result", "parameterMap : " + parameterMap);
        (new CompositeDisposable()).add(apiService.callEmployeeSendNote(parameterMap).
                observeOn(AndroidSchedulers.mainThread()).
                subscribeOn(Schedulers.io()).subscribe((Consumer)(new Consumer() {
            // $FF: synthetic method
            // $FF: bridge method
            public void accept(Object var1) {
                this.accept((GetStatus)var1);
            }

            public final void accept(GetStatus result) {
                mDialog.cancel();
                if (result != null) {
                    mDialog.cancel();
                    System.out.println("aaaaaaaaa  result add note  "+result.toString());
                    Log.i("aaaaaaa   Result", "" + result.toString());
                    if (result.getStatus().equalsIgnoreCase("0")){
                        getapiCallForEmployeeDocument();
                        CUC.Companion.displayToast((Context)UserInfoDetails.this, result.getMessage(), result.getMessage());
                    }else {
                        if (result.getShow_status().equals("0")){
                            getapiCallForEmployeeDocument();
                            CUC.Companion.displayToast((Context)UserInfoDetails.this, result.getMessage(), result.getMessage());
                        }else {
                            CUC.Companion.displayToast(UserInfoDetails.this, result.getMessage(), result.getMessage());
                        }
                    }
                } else {
                    mDialog.cancel();
                }
            }
        }), (Consumer)(new Consumer() {
            // $FF: synthetic method
            // $FF: bridge method
            public void accept(Object var1) {
                this.accept((Throwable)var1);
            }

            public final void accept(Throwable error) {
                mDialog.cancel();
                Toast.makeText(UserInfoDetails.this, getString(R.string.connection_to_database_failed), Toast.LENGTH_SHORT).show();
                error.printStackTrace();
                System.out.println("aaaaaaaa  error add note "+error.getMessage());
            }
        })));
    }

    public void deletenote(notesdata notesdata) {
        mDialog = CUC.Companion.createDialog(UserInfoDetails.this);
        RequetsInterface apiService = RequetsInterface.Factory.create();
        Map parameterMap =new HashMap<String, String>();
        parameterMap.put("api_key", String.valueOf(Preferences.INSTANCE.getPreference((UserInfoDetails.this), "API_KEY")));
        if (logindata != null) {
            parameterMap.put("employee_id", logindata.getUser_id());
        }
        parameterMap.put("patient_id",patient_id);
        parameterMap.put("notes_id",notesdata.getPnote_id());

        System.out.println("aaaaaaaaaaa  parameterMap add note "+parameterMap);
        Log.i("aaaaaaaa   Result", "parameterMap : " + parameterMap);
        (new CompositeDisposable()).add(apiService.callDdeleteNOte(parameterMap).
                observeOn(AndroidSchedulers.mainThread()).
                subscribeOn(Schedulers.io()).subscribe((Consumer)(new Consumer() {
            // $FF: synthetic method
            // $FF: bridge method
            public void accept(Object var1) {
                this.accept((GetStatus)var1);
            }

            public final void accept(GetStatus result) {
                mDialog.cancel();
                if (result != null) {
                    mDialog.cancel();
                    System.out.println("aaaaaaaaa  result add note  "+result.toString());
                    Log.i("aaaaaaa   Result", "" + result.toString());
                    if (result.getStatus().equalsIgnoreCase("0")){
                        getapiCallForEmployeeDocument();
                        CUC.Companion.displayToast((Context)UserInfoDetails.this, result.getShow_status(), result.getMessage());
                    }else {
                        if (result.getShow_status().equals("0")){
                            getapiCallForEmployeeDocument();
                            CUC.Companion.displayToast((Context)UserInfoDetails.this, result.getShow_status(), result.getMessage());
                        }else {
                            CUC.Companion.displayToast(UserInfoDetails.this, result.getShow_status(), result.getMessage());
                        }
                    }
                } else {
                    mDialog.cancel();
                }
            }
        }), (Consumer)(new Consumer() {
            // $FF: synthetic method
            // $FF: bridge method
            public void accept(Object var1) {
                this.accept((Throwable)var1);
            }

            public final void accept(Throwable error) {
                mDialog.cancel();
                Toast.makeText(UserInfoDetails.this, getString(R.string.connection_to_database_failed), Toast.LENGTH_SHORT).show();
                error.printStackTrace();
                System.out.println("aaaaaaaa  error update "+error.getMessage());
            }
        })));
    }

    public void setupdatenote(notesdata notesdata) {
        AddProductDialog addProductDialog=new AddProductDialog();
        addProductDialog.showDialog(UserInfoDetails.this,notesdata);
    }
}
