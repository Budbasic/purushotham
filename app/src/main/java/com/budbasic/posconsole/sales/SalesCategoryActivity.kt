package com.budbasic.posconsole.sales

import android.annotation.SuppressLint
import android.app.Activity
import android.app.Dialog
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.graphics.Color
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.StrictMode
import android.provider.Settings
import android.support.design.widget.Snackbar
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter
import android.support.v4.content.ContextCompat
import android.support.v4.view.PagerAdapter
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.CardView
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.text.Editable
import android.text.InputFilter
import android.text.TextWatcher
import android.util.Log
import android.view.*
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.*
import com.budbasic.customer.global.Preferences
import com.budbasic.global.BSV
import com.budbasic.global.CUC
import com.budbasic.posconsole.R
import com.budbasic.posconsole.ScanQRCodeActivity
import com.budbasic.posconsole.UserInfoDetailActivity
import com.budbasic.posconsole.dialogs.PromoCodeDialog
import com.budbasic.posconsole.driver.SignatureActivity
import com.budbasic.posconsole.global.DigitsInputFilter
import com.budbasic.posconsole.global.EmployeeSearchProducts
import com.budbasic.posconsole.global.SingleSectionNotificationHoverMenuService
import com.budbasic.posconsole.model.APIClass
import com.budbasic.posconsole.webservice.RequetsInterface
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DecodeFormat
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.signature.ObjectKey
import com.google.gson.Gson
import com.kotlindemo.model.*
import com.tbruyelle.rxpermissions2.RxPermissions
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.functions.Consumer
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.raw_payment_method.view.*
import kotlinx.android.synthetic.main.sales_products.*
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.File
import java.util.*
import kotlin.math.nextUp


class SalesCategoryActivity : AppCompatActivity(), APIClass.onCallListner {

    override fun onFailed(requestCode: Int) {
    }

    override fun onSuccess(requestCode: Int) {
        if (requestCode == EMPLOYEEPATIENTPRODUCTS) {
            callEmployeePatientProducts()
        } else if (requestCode == EMPLOYEEPATIENTCART) {
            callEmployeePatientCart()
        } else if (requestCode == EMPLOYEESALESCANCELORDER) {
            callEmployeeSalesCancelOrder()
        } else if (requestCode == EMPLOYEEPATIENTREMOVECART) {
            callEmployeePatientRemovecart()
        } else if (requestCode == EMPLOYEEPATIENTUPDATECART) {
            callEmployeePatientUpdatecart()
        } else if (requestCode == EMPLOYEEPATIENTCONFIRMORDER) {
            callEmployeePatientConfirmOrder()
        } else if (requestCode == EMPLOYEEPATIENTAPPLYLOYALTY) {
            callEmployeePatientApplyLoyalty()
        } else if (requestCode == EMPLOYEESALESAPPLYSTMANDISCOUNT) {
            callEmployeeSalesApplyStmanDiscount()
        } else if (requestCode == EMPLOYEESALESREMOVESTMANDISCOUNT) {
            callEmployeeSalesRemoveStmanDiscount()
        } else if (requestCode == EMPLOYEEPATIENTREMOVELOYALTY) {
            callEmployeePatientRemoveLoyalty()
        } else if (requestCode == 11) {
            callEmployeeHelpdesk()
        }

    }

    val EMPLOYEEPATIENTPRODUCTS = 1
    val EMPLOYEEPATIENTCART = 2
    val EMPLOYEESALESCANCELORDER = 3
    val EMPLOYEEPATIENTREMOVECART = 4
    val EMPLOYEEPATIENTUPDATECART = 5
    val EMPLOYEEPATIENTCONFIRMORDER = 6
    val EMPLOYEEPATIENTAPPLYLOYALTY = 7
    val EMPLOYEESALESAPPLYSTMANDISCOUNT = 8
    val EMPLOYEESALESREMOVESTMANDISCOUNT = 9
    val EMPLOYEEPATIENTREMOVELOYALTY = 10

    lateinit var mDialog: Dialog
    private var mSectionsPagerAdapter: PageAdapter? = null
    lateinit var logindata: GetEmployeeLogin
    private var cateList = ArrayList<GetData>()

    var queue_patient_id = ""
    var queue_id = ""
    var QueueData = ""

    lateinit var bottomUp: Animation
    lateinit var bottomDown: Animation

    lateinit var mAppBasic: GetEmployeeAppSetting

    var list: ArrayList<GetCartData> = ArrayList()
    lateinit var productAdapter: ProductAdapter

    var taxList: ArrayList<GetTaxData> = ArrayList()
    lateinit var taxItemAdapter: TaxItemAdapter
    var deleteId = ""
    var editId = ""
    var category_id = ""
    var product_id = ""
    var changeQty = ""
    var mainCartId = ""
    var Store_sign_required = ""
    var paymentId = ""
    var payment_path = ""
    var total_payble = ""

    var productQytValue = 1f
    var Currency_value = ""

    var patient_point = "0.00"
    var cart_manual_discount_type = "1"

    lateinit var dialogProductQty: Dialog

    lateinit var intentFilter: IntentFilter
    lateinit var mReceiver: BroadcastReceiver
    lateinit var customDialog: Dialog
    lateinit var paymentAdapter: PaymentAdapter
    var paymentTypeList = java.util.ArrayList<GetPaymentTypes>()
    var currantPage = 0

    lateinit var selecteQueue: GetQueueList

    var request_code = 7070
    var rxPermissions: RxPermissions? = null
    var apiClass: APIClass? = null
    var pieGraphLimite: Float = 0f
    var noOfAddedItem: Float = 0f
    var patientImage = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN)
        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN or WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN)
        setContentView(R.layout.sales_products)
        apiClass = APIClass(this@SalesCategoryActivity, this)
        rxPermissions = RxPermissions(this)
        mAppBasic = Gson().fromJson(Preferences.getPreference(this@SalesCategoryActivity, "AppSetting"), GetEmployeeAppSetting::class.java)
        logindata = Gson().fromJson(Preferences.getPreference(this@SalesCategoryActivity, "logindata"), GetEmployeeLogin::class.java)
        Currency_value = mAppBasic.Currency_value
        if (intent != null && intent.hasExtra("queue_patient_id"))
            queue_patient_id = intent.getStringExtra("queue_patient_id")
        if (intent != null && intent.hasExtra("queue_id"))
            queue_id = intent.getStringExtra("queue_id")

        if (intent != null && intent.hasExtra("QueueData")) {
            QueueData = intent.getStringExtra("QueueData")
        }
        if (intent != null && intent.hasExtra("request_code")) {
            request_code = intent.getIntExtra("request_code", 0)
        }
        if (QueueData != "") {
            selecteQueue = Gson().fromJson(QueueData, GetQueueList::class.java)
            queue_patient_id = selecteQueue.queue_patient_id
        }
        Log.i("QueueData", "QueueData : $QueueData")
        init()
    }

//    override fun onStart() {
//        super.onStart()
//        isStarted = true
//        if (IsVisible && isStarted) {
//        }
//    }

//    override fun setUserVisibleHint(isVisibleToUser: Boolean) {
//        super.setUserVisibleHint(isVisibleToUser)
//        IsVisible = isVisibleToUser
//        if (isStarted && IsVisible) {
//                init()
//        }
//    }

    override fun onResume() {
        super.onResume()
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            applicationContext.startForegroundService(Intent(applicationContext, SingleSectionNotificationHoverMenuService::class.java))
        } else {
            applicationContext.startService(Intent(applicationContext, SingleSectionNotificationHoverMenuService::class.java))
        }
    }

    override fun onPause() {
        super.onPause()
        applicationContext.stopService(Intent(applicationContext, SingleSectionNotificationHoverMenuService::class.java))
    }

    private fun init() {
        apiCallForEmployeeDocument()
        // setChartData()
        if (intent != null && intent.hasExtra("QueueData")) {
            tvcustomername.text = "${selecteQueue.first_name} ${selecteQueue.last_name}"
            //    tv_licnence.text = "${selecteQueue.patient_licence_number}"
            //tv_limit_value.text = "${selecteQueue.pati}"
            // Log.d("SASASASAS"," = "+selecteQueue.patient_image)
            Glide.with(this).load(selecteQueue.patient_image).into(ivpatientcart)
        }

        if (intent != null && intent.hasExtra("patientImage")) {
            patientImage = intent.getStringExtra("patientImage")
            Glide.with(this).load(patientImage).into(ivpatientcart)
        }
        callEmployeePatientProducts()
        bottomUp = AnimationUtils.loadAnimation(this@SalesCategoryActivity, R.anim.slide_in_left)
        bottomDown = AnimationUtils.loadAnimation(this@SalesCategoryActivity, R.anim.slide_out_left)

        tv_cancelorder.setOnClickListener {
            val builder = AlertDialog.Builder(this@SalesCategoryActivity)
            builder.setMessage(getString(R.string.are_you_sure_you_want_to_cancel_order))
            builder.setPositiveButton("YES") { dialog, which ->
                dialog.dismiss()
                callEmployeeSalesCancelOrder()
            }

            builder.setNegativeButton("NO") { dialog, which -> dialog.dismiss() }
            val alert = builder.create()
            alert.show()
        }

        ivbackcart.setOnClickListener {
            finish()
        }

        tvGetPromoCode.setOnClickListener {
            if (tvGetPromoCode.text == "REMOVE") {
//                val builder = AlertDialog.Builder(this@SalesCategoryActivity)
//                builder.setMessage(getString(R.string.are_you_sure_you_want_to_remove_promo_code))
//
//                builder.setPositiveButton("Yes") { dialog, which ->
//                    dialog.dismiss()
//                    callEmployeeSalesRemovePromocode()
//                }
//
//                builder.setNegativeButton("No") { dialog, which -> dialog.dismiss() }
//                val alert = builder.create()
//                alert.show()
                callEmployeeSalesRemovePromocode()
            } else {
                val mDialog = PromoCodeDialog(this@SalesCategoryActivity, object : PromoCodeDialog.onCallListner {
                    override fun onRefresh() {
                        callEmployeePatientCart()
                    }
                })

                if (viewCard != null) {
                    val myBundle = Bundle()
                    myBundle.putString("cart_id", viewCard!!.main_cart.cart_id)
                    myBundle.putString("patient_id", viewCard!!.main_cart.cart_patient_id)
                    myBundle.putString("app_status", "1")
                    myBundle.putString("queue_id", queue_id)

                    mDialog.arguments = myBundle
                }
                mDialog.show(fragmentManager, "promo_code")
            }
        }

        ivreferecart.setOnClickListener {
            callEmployeePatientCart()
        }

        img_exmlator.setOnClickListener {

            val intent = Intent(this@SalesCategoryActivity, UserInfoDetailActivity::class.java)
            intent.putExtra("patient_id", queue_patient_id)
            startActivity(intent)
        }
        relTop1.setOnClickListener {
            val intent = Intent(this@SalesCategoryActivity, UserInfoDetailActivity::class.java)
            intent.putExtra("patient_id", queue_patient_id)
            startActivity(intent)
        }
        ivpatientcart.setOnClickListener {
            //            val intent = Intent(this@SalesCategoryActivity, UserInfoDetailActivity::class.java)
//            intent.putExtra("patient_id", queue_patient_id)
//            startActivity(intent)

        }
        ivComment.setOnClickListener {
            showDialogHelp()
        }
        ivproductinfocart.setOnClickListener {
            val mDialog = EmployeeSearchProducts(this@SalesCategoryActivity)
            mDialog.show(fragmentManager, "product")
        }

        /*tab_layout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabReselected(tab: TabLayout.Tab?) {

            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {

            }

            override fun onTabSelected(tab: TabLayout.Tab?) {

            }

        })*/

        /* mViewPager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
             override fun onPageScrollStateChanged(state: Int) {

             }

             override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {

             }

             override fun onPageSelected(position: Int) {

                 // salesFragment = (mSectionsPagerAdapter?.getItem(position) as SalesProductListFragment)
                 currantPage = position

             }

         })*/


        /*edtSearch.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {

            }

            override fun beforeTextChanged(filtervalue: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(filtervalue: CharSequence?, p1: Int, p2: Int, p3: Int) {

                if (CUC.mfilterdata.size > 0 && CUC.mfilterdata.size > currantPage && CUC.mfilterdata[currantPage] != null) {
                    *//*if (salesFragment != null) {
                        salesFragment!!.filterData(filtervalue!!.toString())
                    }*//*
                    CUC.mfilterdata!![currantPage]!!.onfilterData(filtervalue!!.toString())
                }
            }

        })*/

        ivclearcart.setOnClickListener {
            val builder = AlertDialog.Builder(this@SalesCategoryActivity)
            builder.setMessage(getString(R.string.are_you_sure_you_want_to_cancel_order))

            builder.setPositiveButton("Yes") { dialog, which ->
                dialog.dismiss()
                callEmployeeSalesCancelOrder()
            }

            builder.setNegativeButton("No") { dialog, which -> dialog.dismiss() }
            val alert = builder.create()
            alert.show()
        }
        ll_cancel.setOnClickListener {

            if (list != null && list.size > 0) {
                val builder = AlertDialog.Builder(this@SalesCategoryActivity)
                builder.setMessage(getString(R.string.are_you_sure_you_want_to_cancel_order))

                builder.setPositiveButton("Yes") { dialog, which ->
                    dialog.dismiss()
                    callEmployeeSalesCancelOrder()
                }

                builder.setNegativeButton("No") { dialog, which -> dialog.dismiss() }
                val alert = builder.create()
                alert.show()
            } else {
                val builder = AlertDialog.Builder(this@SalesCategoryActivity)
                builder.setMessage(getString(R.string.are_you_want_to_leave_sales_pos))

                builder.setPositiveButton("Yes") { dialog, which ->
                    dialog.dismiss()
                    finish()
                }

                builder.setNegativeButton("No") { dialog, which ->
                    dialog.dismiss()
                }
                val alert = builder.create()
                alert.setCanceledOnTouchOutside(false)
                alert.setCancelable(false)
                alert.show()
                //CUC.displayToast(this@SalesCategoryActivity, "0", "Your cart is empty!")
            }

        }
        tv_refreshorder.setOnClickListener {
            callEmployeePatientCart()
        }
        /*iv_menu.setOnClickListener {
            bottomViewShow(ll_orderview)
        }

        rl_closeview.setOnClickListener {
            bottomViewShow(ll_orderview)
        }*/
        mSwipeRefreshLayout.setOnRefreshListener {
            callEmployeePatientProducts()
        }

        iv_barcode.setOnClickListener {
            rxPermissions!!.request(
                    android.Manifest.permission.CAMERA)
                    .subscribe { granted ->
                        if (granted!!) { // Always true pre-M
                            // I can control the camera now
                            startActivityForResult(Intent(this@SalesCategoryActivity, ScanQRCodeActivity::class.java), 444)
                        } else {
                            // Oups permission denied
                            Snackbar.make(findViewById(R.id.rl_store_main), resources.getString(R.string.permissionenable),
                                    Snackbar.LENGTH_LONG)
                                    .setActionTextColor(Color.WHITE)
                                    .setAction(resources.getString(R.string.ok), object : View.OnClickListener {
                                        override fun onClick(p0: View?) {
                                            val intent = Intent()
                                            intent.action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
                                            val uri = Uri.fromParts("package", packageName, null)
                                            intent.data = uri
                                            startActivity(intent)
                                        }


                                    }).show()
                        }
                    }
        }

        intentFilter = IntentFilter("android.intent.action.MAINORD")
        mReceiver = object : BroadcastReceiver() {

            override fun onReceive(context: Context, intent: Intent) {
                callEmployeePatientCart()
            }
        }

        //registering our receiver
        registerReceiver(mReceiver, intentFilter)

        rv_tax_list!!.layoutManager = LinearLayoutManager(this@SalesCategoryActivity)
        taxItemAdapter = TaxItemAdapter(taxList, this@SalesCategoryActivity, Currency_value)
        rv_tax_list!!.adapter = taxItemAdapter

        rv_cart_list!!.layoutManager = LinearLayoutManager(this@SalesCategoryActivity)
        productAdapter = ProductAdapter(list, this@SalesCategoryActivity, Currency_value, object : onClickListener {
            override fun deleteClick(data: GetCartData) {
//                val builder = AlertDialog.Builder(this@SalesCategoryActivity)
//                builder.setMessage(getString(R.string.are_you_sure_you_want_to_delete))
//
//                builder.setPositiveButton("Yes") { dialog, which ->
//                    dialog.dismiss()
//                    Log.d("CartItem"," = "+list.size)
//                    if(list!=null && list.size>0){
//                        if(list.size==1){
//                            deleteId = data.cart_detail_id
//                            callEmployeeSalesCancelOrder()
//                        }else{
//                            deleteId = data.cart_detail_id
//                            callEmployeePatientRemovecart()
//                        }
//
//                    }
////                    deleteId = data.cart_detail_id
////                    callEmployeePatientRemovecart()
//                }
//
//                builder.setNegativeButton("No") { dialog, which -> dialog.dismiss() }
//                val alert = builder.create()
//                alert.show()
                if (list != null && list.size > 0) {
                    if (list.size == 1) {
                        deleteId = data.cart_detail_id
                        callEmployeeSalesCancelOrder()
                    } else {
                        deleteId = data.cart_detail_id
                        callEmployeePatientRemovecart()
                    }

                }
            }

            override fun editClick(data: GetCartData) {
               // callEmployeeProductQty(data)
                DisplayProductQtyDialog(data)
            }

        })
        rv_cart_list!!.adapter = productAdapter

        ll_send_order.setOnClickListener {
            //            logindata = Gson().fromJson(Preferences.getPreference(applicationContext, "logindata"), GetEmployeeLogin::class.java)
//            val lockManager = LockManager.getInstance()
//            if (lockManager.isAppLockEnabled) {
//                lockManager.enableAppLock(applicationContext, AppLockActivity::class.java)
//                lockManager.appLock.setPasscode(logindata!!.user_pin)
//                val intent = Intent(applicationContext, SessionTimeOutDialog::class.java)
//                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TOP
//                startActivityForResult(intent, 7577)
//
//            }
            if (list.size > 0) {
                callInvoiceGenerated()
            } else {
                CUC.displayToast(this@SalesCategoryActivity, "0", "Your cart is empty!")
            }
            //callEmployeePatientPreorder()
        }
        ll_redeem_option.setOnClickListener {
            if (list != null && list.size > 0) {
                if (tv_applyremove.text == "Remove Discount") {
                    DisplayRemoveStmanDiscount()
                } else {
                    DisplayRedeemPoints()
                }
            } else {
                CUC.displayToast(this@SalesCategoryActivity, "0", "Your cart is empty!")
            }

            /*logindata = Gson().fromJson(Preferences.getPreference(applicationContext, "logindata"), GetEmployeeLogin::class.java)
            val lockManager = LockManager.getInstance()
            if (lockManager.isAppLockEnabled) {
                lockManager.enableAppLock(applicationContext, AppLockActivity::class.java)
                lockManager.appLock.setPasscode("${logindata!!.user_pin}")
                val intent = Intent(applicationContext, SessionTimeOutDialog::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TOP
                startActivityForResult(intent, 7576)

            }*/
        }


    }


    var patientDialog: Dialog? = null

    fun PatientProfile() {
        patientDialog = Dialog(this@SalesCategoryActivity, R.style.TransparentProgressDialog)
        val policy = StrictMode.ThreadPolicy.Builder().permitAll().build()
        StrictMode.setThreadPolicy(policy)
        patientDialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        patientDialog!!.setContentView(R.layout.customer_profile)
        patientDialog!!.window.setLayout(WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.WRAP_CONTENT)
        patientDialog!!.window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)
        patientDialog!!.show()
        val tvpatientname = patientDialog!!.findViewById<TextView>(R.id.tvpatientname)
        val tvpatientlicence = patientDialog!!.findViewById<TextView>(R.id.tvpatientlicence)
        val tv_cancel = patientDialog!!.findViewById<TextView>(R.id.tv_cancel)
        tv_cancel.setOnClickListener {
            patientDialog!!.dismiss()
        }
        tvpatientname.text = selecteQueue.first_name + " " + selecteQueue.last_name
        tvpatientlicence.text = selecteQueue.patient_licence_number
    }

    var dialogHelp: Dialog? = null
    var help_message = ""
    var edt_help_message: EditText? = null
    fun showDialogHelp() {
        dialogHelp = Dialog(this@SalesCategoryActivity, R.style.TransparentProgressDialog)
        val policy = StrictMode.ThreadPolicy.Builder().permitAll().build()
        StrictMode.setThreadPolicy(policy)
        dialogHelp!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialogHelp!!.setContentView(R.layout.sales_help_popup_window)
        dialogHelp!!.window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT)
        dialogHelp!!.window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)
        dialogHelp!!.show()
        edt_help_message = dialogHelp!!.findViewById<EditText>(R.id.edt_help_message)
        val cv_submit = dialogHelp!!.findViewById<CardView>(R.id.cv_submit)
        val cv_cancel = dialogHelp!!.findViewById<CardView>(R.id.cv_cancel)

        cv_submit.setOnClickListener {
            if (edt_help_message!!.text.toString() == "") {
                CUC.displayToast(this@SalesCategoryActivity, "0", "Enter Query Message")
            } else {
                help_message = edt_help_message!!.text.toString()
                callEmployeeHelpdesk()
            }

        }
        cv_cancel.setOnClickListener {
            help_message = ""
            edt_help_message!!.setText("")
            if (dialogHelp != null && dialogHelp!!.isShowing)
                dialogHelp!!.dismiss()
        }
    }


    fun callEmployeeHelpdesk() {
        mDialog = CUC.createDialog(this@SalesCategoryActivity)
        val apiService = RequetsInterface.create()
        val parameterMap = HashMap<String, String>()
        parameterMap["api_key"] = "${Preferences.getPreference(this@SalesCategoryActivity, "API_KEY")}"
        parameterMap["employee_id"] = "${logindata!!.user_id}"
        parameterMap["query_msg"] = help_message
        Log.i("Result", "parameterMap : $parameterMap")
        CompositeDisposable().add(apiService.callEmployeeHelpdesk(parameterMap)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe({ result ->
                    if (mDialog != null && mDialog!!.isShowing)
                        mDialog!!.dismiss()
                    if (result != null) {
                        Log.i("Result", "" + result.toString())
                        if (result.status == "0") {
                            help_message = ""
                            edt_help_message!!.setText("")
                            if (dialogHelp != null && dialogHelp!!.isShowing)
                                dialogHelp!!.dismiss()
                            CUC.displayToast(this@SalesCategoryActivity, result.show_status, result.message)
                        } else if (result.status == "10") {
                            apiClass!!.callApiKey(11)
                        } else {
                            CUC.displayToast(this@SalesCategoryActivity, result.show_status, result.message)
                        }
                    } else {
                        //  CUC.displayToast(this@SalesCategoryActivity, "0", this@SalesCategoryActivity.getString(R.string.connection_to_database_failed))
                    }
                }, { error ->
                    if (mDialog != null && mDialog!!.isShowing)
                        mDialog!!.dismiss()
                    CUC.displayToast(this@SalesCategoryActivity, "0", this@SalesCategoryActivity.getString(R.string.connection_to_database_failed))
                    error.printStackTrace()
                })
        )
    }

    var siganture_path = ""
    var salesFragment: SalesProductListFragment? = null
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            7577 -> {
                if (list != null && list.size > 0) {
                    callInvoiceGenerated()
                } else {
                    CUC.displayToast(this@SalesCategoryActivity, "0", "Your cart is empty!")
                }
            }
            7576 -> {
                if (tv_applyremove.text == "Remove Discount") {
                    val builder = AlertDialog.Builder(this)
                    builder.setMessage(getString(R.string.are_you_sure_you_want_to_remove_discount))

                    builder.setPositiveButton("Yes") { dialog, which ->
                        dialog.dismiss()
                        callEmployeeSalesRemoveStmanDiscount()
                    }

                    builder.setNegativeButton("No") { dialog, which -> dialog.dismiss() }
                    val alert = builder.create()
                    alert.show()

                } else {
                    DisplayRedeemPoints()
                }
            }
            444 -> if (resultCode == Activity.RESULT_OK) {
                if (data != null && data.hasExtra("qrcode")) {
                    mAboutDataListener!!.onDataReceived(data.getStringExtra("qrcode"))
                }
            }
            8989 -> if (resultCode === Activity.RESULT_OK) {
                if (data != null && data.hasExtra("siganture_path")) {
                    siganture_path = data.getStringExtra("siganture_path")
                    if (siganture_path != null && siganture_path.isNotEmpty()) {
                        callEmployeePatientConfirmOrder()
                    }
//                    if (::iv_siganture.isInitialized) {
//                        val newPhotoKey = ObjectKey(System.currentTimeMillis().toString())
//                        Glide.with(this@SalesCategoryActivity)
//                                .load(siganture_path)
//                                .apply(RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.NONE)
//                                        .dontTransform().dontAnimate()
//                                        .signature(newPhotoKey))
//                                .into(iv_siganture)
//                    }
                }
            }
        }
    }

    inner class PageAdapter(fm: FragmentManager, val context: Context, val cateList: ArrayList<GetData>, val imagePath: String, val queue_patient_id: String,
                            val queue_id: String, val Currency_value: String) : FragmentStatePagerAdapter(fm) {


        override fun getItem(position: Int): Fragment {
            Log.i(SalesCategoryActivity::class.java.simpleName, "category : ${cateList[position].category}")
            //edtSearch.setText("")
            salesFragment = SalesProductListFragment.newInstance(cateList[position].category, position, imagePath, queue_patient_id, queue_id, Currency_value, context)
            return salesFragment!!
        }

        override fun getItemPosition(`object`: Any): Int {
            return PagerAdapter.POSITION_NONE
        }

        override fun getCount(): Int {
            // Show 5 total pages.
            return cateList.size
        }

        override fun getPageTitle(position: Int): CharSequence? {
            // return null to show no title.
            Log.i(SalesCategoryActivity::class.java.simpleName, "category_name : ${cateList[position].category.category_name}")
            return cateList[position].category.category_name

        }
    }


    private fun apiCallForEmployeeDocument() {
        var mDialog = Dialog(this)
        try {
            if (!this!!.isFinishing) {
                mDialog = CUC.createDialog(this)
                mDialog.show()
                val apiService = RequetsInterface.Factory.create()
                val parameterMap = java.util.HashMap<String, String>()

                parameterMap["api_key"] = "${Preferences.getPreference(this!!, "API_KEY")}"
                parameterMap["employee_id"] = "${logindata.user_id}"
                parameterMap["patient_id"] = "${queue_patient_id}"
                parameterMap["store_id"] = "${logindata.user_store_id}"

                CompositeDisposable().add(apiService.callEmployeePatientInfo(parameterMap)
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.io())
                        .subscribe({ result ->
                            if (mDialog != null && mDialog.isShowing)
                                mDialog.dismiss()
                            if (result != null) {
                                if (result.Limit != null) {
                                    textLimitUsage.text = result.Limit
                                    tv_limit_value.text = result.spent_limit
                                    Log.d("SpenLimit", " = " + result.spent_limit)
                                    var str = result.Limit
                                    noOfAddedItem = result.spent_limit!!.toFloat()
                                    pieGraphLimite = (str?.replace("[^\\d.]", "", true))!!.toFloat()
                                    setChartData()
                                }
                            }

                        }, { error ->
                            if (mDialog != null && mDialog.isShowing)
                                mDialog.dismiss()
                            CUC.displayToast(this!!, "0", this.getString(R.string.connection_to_database_failed))
                            error.printStackTrace()
                        })
                )
            }
        } catch (e: Exception) {
            if (mDialog != null && mDialog.isShowing) {
                mDialog.dismiss()
            }
            e.printStackTrace()
        }

    }


    fun callEmployeePatientProducts() {
        if (CUC.isNetworkAvailablewithPopup(this@SalesCategoryActivity)) {
            if (mSwipeRefreshLayout != null) {
                mSwipeRefreshLayout.isRefreshing = false
            }
            mDialog = CUC.createDialog(this@SalesCategoryActivity)
            val apiService = RequetsInterface.create()
            val parameterMap = HashMap<String, String>()
            parameterMap["api_key"] = Preferences.getPreference(this@SalesCategoryActivity, "API_KEY")
            if (logindata != null) {
                parameterMap["store_id"] = logindata.user_store_id
                parameterMap["patient_id"] = queue_patient_id
            }
            Log.i("Result", "parameterMap : $parameterMap")
            CompositeDisposable().add(apiService.callEmployeeSalesProducts(parameterMap)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe({ result ->
                        if (mDialog != null && mDialog.isShowing)
                            mDialog.dismiss()
                        if (result != null) {
                            Log.i("Result", "" + result.toString())
                            if (result.status == "0") {
                                callEmployeePatientCart()
                                if (result.pro_cat.data.size > 0) {
                                    cateList.clear()
                                    cateList = result.pro_cat.data
                                    mSectionsPagerAdapter = PageAdapter(supportFragmentManager, this@SalesCategoryActivity.applicationContext, cateList, result.image_path, queue_patient_id, queue_id, Currency_value)

                                    mViewPager!!.adapter = mSectionsPagerAdapter
                                    mViewPager.offscreenPageLimit = cateList.size
                                    tab_layout.setupWithViewPager(mViewPager)
                                    for (i in 0 until tab_layout.getTabCount()) {
                                        val tab = (tab_layout.getChildAt(0) as ViewGroup).getChildAt(i)
                                        val p = tab.getLayoutParams() as ViewGroup.MarginLayoutParams
                                        p.setMargins(0, 0, 20, 0)
                                        tab.requestLayout()
                                    }
                                    mViewPager.currentItem = 0
                                    mViewPager.setOnTouchListener(object : View.OnTouchListener {
                                        override fun onTouch(p0: View?, p1: MotionEvent?): Boolean {
                                            mSwipeRefreshLayout.isEnabled = false
                                            when (p1!!.action) {
                                                MotionEvent.ACTION_UP -> {
                                                    mSwipeRefreshLayout.isEnabled = true
                                                }
                                            }
                                            return false
                                        }
                                    })
                                    rl_empty_view.visibility = View.GONE
                                } else {
                                    rl_empty_view.visibility = View.VISIBLE
                                }
                                CUC.displayToast(this@SalesCategoryActivity, result.show_status, result.message)
                            } else if (result.status == "10") {
                                apiClass!!.callApiKey(EMPLOYEEPATIENTPRODUCTS)
                            } else {
                                CUC.displayToast(this@SalesCategoryActivity, result.show_status, result.message)
                            }
                        } else {
                            rl_empty_view.visibility = View.VISIBLE
                            //  CUC.displayToast(this@SalesCategoryActivity, "0", getString(R.string.connection_to_database_failed))
                        }
                    }, { error ->
                        if (mDialog != null && mDialog.isShowing)
                            mDialog.dismiss()
                        CUC.displayToast(this@SalesCategoryActivity, "0", getString(R.string.connection_to_database_failed))
                        error.printStackTrace()
                    })
            )
        }
    }

//    fun callApiKey() {
//        if (CommonUtils.isNetworkAvailablewithPopup(this@SalesProductsActivity)) {
//            val apiService = RequetsInterface.Factory.create()
//            val parameterMap = java.util.HashMap<String, String>()
//            CompositeDisposable().add(apiService.callGetApiKey(parameterMap)
//                    .observeOn(AndroidSchedulers.mainThread())
//                    .subscribeOn(Schedulers.io())
//                    .subscribe({ result ->
//                        Preferences.setPreference(this@SalesProductsActivity, "API_KEY", "${result.api_key}")
//                        callEmployeePatientProducts()
//
//                        Log.i("Result", "" + result.toString())
//                    }, { error ->
//                        error.printStackTrace()
//                    })
//            )
//        }
//    }

    fun bottomViewShow(footer: View) {
        if (footer.visibility == View.VISIBLE) {
            footer.startAnimation(bottomDown)
            footer.visibility = View.INVISIBLE
            window.setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE, WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
            bottomDown.setAnimationListener(object : Animation.AnimationListener {
                override fun onAnimationStart(animation: Animation) {

                }

                override fun onAnimationEnd(animation: Animation) {
                    footer.visibility = View.GONE
                    //Toast.makeText(ParticipateActivity.this, "Hide View.", Toast.LENGTH_LONG).show();
                    window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
                }

                override fun onAnimationRepeat(animation: Animation) {

                }
            })
        } else {
            footer.startAnimation(bottomUp)
            footer.visibility = View.INVISIBLE
            window.setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE, WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
            bottomUp.setAnimationListener(object : Animation.AnimationListener {
                override fun onAnimationStart(animation: Animation) {

                }

                override fun onAnimationEnd(animation: Animation) {
                    footer.visibility = View.VISIBLE
                    callEmployeePatientCart()
                    //Toast.makeText(ParticipateActivity.this, "Show View.", Toast.LENGTH_LONG).show();
                    window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
                }

                override fun onAnimationRepeat(animation: Animation) {

                }
            })
        }
    }

    var viewCard: GetEmployeePatientCart? = null
    fun callEmployeePatientCart() {
        if (CUC.isNetworkAvailablewithPopup(this@SalesCategoryActivity)) {
            mDialog = CUC.createDialog(this@SalesCategoryActivity)
            val apiService = RequetsInterface.create()
            val parameterMap = HashMap<String, String>()
            parameterMap["api_key"] = "${Preferences.getPreference(this@SalesCategoryActivity, "API_KEY")}"
            if (logindata != null) {
                parameterMap["store_id"] = logindata.user_store_id
            }
            parameterMap["patient_id"] = "$queue_patient_id"
            parameterMap["queue_id"] = "$queue_id"
            Log.i("Result", "$parameterMap")
            CompositeDisposable().add(apiService.callEmployeeSalesViewcart(parameterMap)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe({ result ->
                        if (mDialog != null && mDialog.isShowing)
                            mDialog.dismiss()
                        if (result != null) {
                            viewCard = result
                            Log.d("ViewCart", "" + result.toString())
                            Log.i("Result", "" + result.toString())
                            when {
                                result.status == "0" -> {
                                    mainCartId = result.main_cart.cart_id
                                    payment_path = result.payment_path
                                    Store_sign_required = result.Store_sign_required
                                    paymentTypeList.clear()
                                    paymentTypeList.addAll(result.payment_types)
                                    if (result.cart_data != null && result.cart_data.size > 0) {
                                        list.clear()

                                        patient_point = result.patient_point
                                        discount_value = result.main_cart.cart_manual_discount
                                        cart_manual_discount_type = result.main_cart.cart_manual_discount_type

//                                        noOfAddedItem = result.spent_limit!!.toFloat()
////                                        result.cart_data.forEach {
////                                            noOfAddedItem += (it.cart_detail_qty).toFloat()
////                                        }
//
//                                        setChartData()
                                        val str = result.Limit
                                        noOfAddedItem = result.spent_limit!!.toFloat()
                                        pieGraphLimite = (str?.replace("[^\\d.]", "", true))!!.toFloat()
                                        setChartData()
                                        if (logindata != null) {
                                            if (logindata.user_role == "2") {
                                                ll_redeem_option.visibility = View.VISIBLE
                                                ll_send_order.visibility = View.VISIBLE
                                                ll_cancel.visibility = View.VISIBLE
                                                ll_promoTab.visibility = View.VISIBLE
                                                //   rl_bottom.visibility = View.VISIBLE
                                                total_unit.visibility = View.VISIBLE
                                            } else {
                                                ll_redeem_option.visibility = View.GONE
                                                ll_send_order.visibility = View.GONE
                                                ll_cancel.visibility = View.GONE
                                                //   rl_bottom.visibility = View.GONE
                                                ll_promoTab.visibility = View.GONE
                                                total_unit.visibility = View.GONE
                                            }
                                        } else {
                                            ll_redeem_option.visibility = View.GONE
                                            ll_send_order.visibility = View.GONE
                                            ll_cancel.visibility = View.GONE
                                            // rl_bottom.visibility = View.GONE
                                            ll_promoTab.visibility = View.GONE
                                            total_unit.visibility = View.GONE
                                        }

                                        if (cart_manual_discount_type == "1" || cart_manual_discount_type == "2") {
                                            tv_applyremove.text = "Remove Discount"
                                        } else {
                                            tv_applyremove.text = "Discount"
                                        }
                                        tv_cartisempty.visibility = View.GONE
                                        tv_total_qty.text = "${result.total_qty}"
                                        tv_subtotal.text = "$Currency_value${result.subtotal}"
                                        list.addAll(result.cart_data)
                                        productAdapter.notifyDataSetChanged()
                                        taxList.clear()
                                        taxList.addAll(result.tax_data)
                                        try {
                                            if (result.loyalty_amount != null && result.loyalty_amount != "") {
                                                if ("${result.loyalty_amount}".toFloat() > 0) {
                                                    taxList.add(GetTaxData("989", "Loyalty Amount", result.loyalty_amount))
                                                }
                                            }
                                        } catch (e: Exception) {
                                            e.printStackTrace()
                                        }
                                        try {
                                            if (discount_value != null && discount_value != "") {
                                                if ("$discount_value".toFloat() > 0) {
                                                    if (cart_manual_discount_type == "2") {
                                                        taxList.add(GetTaxData("202", "${resources.getString(R.string.lbl_discount)}", discount_value, "2"))
                                                    } else {
                                                        taxList.add(GetTaxData("202", "${resources.getString(R.string.lbl_discount)}", discount_value, "1"))
                                                    }
                                                }
                                            }
                                        } catch (e: Exception) {
                                            e.printStackTrace()
                                        }
                                        if (result.promocode_name != "") {
                                            ll_promoTab.visibility = View.VISIBLE
                                            tv_promocode.text = result.promocode_name
                                            taxList.add(GetTaxData("204", "Promo Code(${result.promocode_name})", result.promosales_amount))
                                            tvGetPromoCode.text = "REMOVE"
                                        } else {
                                            ll_promoTab.visibility = View.VISIBLE
                                            tv_promocode.text = ""
                                            tvGetPromoCode.text = "Have A Promo Code?"
                                        }
                                        total_payble = "${result.total_payble}"
                                        taxList.add(GetTaxData("201", "Grand Total", result.total_payble, "1"))
                                        taxItemAdapter.notifyDataSetChanged()

                                    } else {
                                        if (result.promocode_name != "") {
                                            ll_promoTab.visibility = View.GONE
                                            tv_promocode.text = result.promocode_name
                                            tvGetPromoCode.text = "REMOVE"
                                        } else {
                                            ll_promoTab.visibility = View.GONE
                                            tv_promocode.text = ""
                                            tvGetPromoCode.text = "Have A Promo Code?"
                                        }
                                        tv_total_qty.text = ""
                                        tv_subtotal.text = ""
                                        tv_cartisempty.visibility = View.VISIBLE
                                        total_unit.visibility = View.GONE
                                        list.clear()
                                        productAdapter.notifyDataSetChanged()
                                        taxList.clear()
                                        taxItemAdapter.notifyDataSetChanged()
                                        //finish()
                                    }
                                    Preferences.setPreference(this@SalesCategoryActivity, "total_qty", result.total_qty)
                                    CUC.displayToast(this@SalesCategoryActivity, result.show_status, result.message)
                                }
                                result.status == "10" -> apiClass!!.callApiKey(EMPLOYEEPATIENTCART)
                                else -> {
                                    tv_total_qty.text = ""
                                    tv_subtotal.text = ""
                                    if (result.promocode_name != "") {
                                        ll_promoTab.visibility = View.GONE
                                        tv_promocode.text = result.promocode_name
                                        tvGetPromoCode.text = "REMOVE"
                                    } else {
                                        ll_promoTab.visibility = View.GONE
                                        tv_promocode.text = ""
                                        tvGetPromoCode.text = "Have A Promo Code?"
                                    }
                                    tv_applyremove.text = "Discount"
                                    tv_cartisempty.visibility = View.VISIBLE
                                    total_unit.visibility = View.GONE
                                    list.clear()
                                    productAdapter.notifyDataSetChanged()
                                    taxList.clear()
                                    taxItemAdapter.notifyDataSetChanged()
                                    Preferences.setPreference(this@SalesCategoryActivity, "total_qty", "0")
                                    CUC.displayToast(this@SalesCategoryActivity, result.show_status, result.message)
                                    //finish()
                                }
                            }

                        } else {
                            //   CUC.displayToast(this@SalesCategoryActivity, "0", getString(R.string.connection_to_database_failed))
                        }

                    }, { error ->
                        if (mDialog != null && mDialog.isShowing)
                            mDialog.dismiss()
                        CUC.displayToast(this@SalesCategoryActivity, "0", getString(R.string.connection_to_database_failed))
                        error.printStackTrace()
                    })
            )
        }
    }


    fun callEmployeeSalesCancelOrder() {
        if (CUC.isNetworkAvailablewithPopup(this@SalesCategoryActivity)) {
            mDialog = CUC.createDialog(this@SalesCategoryActivity)
            val apiService = RequetsInterface.Factory.create()
            val parameterMap = HashMap<String, String>()
            parameterMap["api_key"] = "${Preferences.getPreference(this@SalesCategoryActivity, "API_KEY")}"
            parameterMap["cart_id"] = mainCartId
            Log.i("Result", "parameterMap : $parameterMap")
            CompositeDisposable().add(apiService.callEmployeeSalesCancelOrder(parameterMap)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe({ result ->
                        if (mDialog != null && mDialog.isShowing)
                            mDialog.dismiss()
                        Log.i("Result", "" + result.toString())
                        if (result != null) {
                            if (result.status == "0") {
//                                val builder = AlertDialog.Builder(this@SalesCategoryActivity)
//                                builder.setMessage(getString(R.string.are_you_want_to_leave_sales_pos))
//
//                                builder.setPositiveButton("Yes") { dialog, which ->
//                                    dialog.dismiss()
//                                    finish()
//                                }
//
//                                builder.setNegativeButton("No") { dialog, which ->
//                                    dialog.dismiss()
//                                }
//                                val alert = builder.create()
//                                alert.setCanceledOnTouchOutside(false)
//                                alert.setCancelable(false)
//                                alert.show()
                                finish()
                                //   callEmployeePatientCart()
                                CUC.displayToast(this@SalesCategoryActivity, result.show_status, result.message)
                            } else if (result.status == "10") {
                                apiClass!!.callApiKey(EMPLOYEESALESCANCELORDER)
                            } else {
                                CUC.displayToast(this@SalesCategoryActivity, result.show_status, result.message)
                            }
                        } else {
                            //   CUC.displayToast(this@SalesCategoryActivity, "0", getString(R.string.connection_to_database_failed))
                        }
                    }, { error ->
                        if (mDialog != null && mDialog.isShowing)
                            mDialog.dismiss()
                        CUC.displayToast(this@SalesCategoryActivity, "0", getString(R.string.connection_to_database_failed))
                        error.printStackTrace()
                    })
            )
        }
    }

    fun callEmployeePatientRemovecart() {
        if (CUC.isNetworkAvailablewithPopup(this@SalesCategoryActivity)) {
            mDialog = CUC.createDialog(this@SalesCategoryActivity)
            val apiService = RequetsInterface.create()
            val parameterMap = HashMap<String, String>()
            parameterMap["api_key"] = "${Preferences.getPreference(this@SalesCategoryActivity, "API_KEY")}"
            parameterMap["cart_detail_id"] = "$deleteId"

            if (logindata != null) {
                //parameterMap["store_id"] = logindata.user_store_id
                parameterMap["employee_id"] = logindata.user_id
            }
            parameterMap["patient_id"] = queue_patient_id
            parameterMap["terminal_id"] = "${mAppBasic.terminal_id}"
            parameterMap["emp_lat"] = "${Preferences.getPreference(this@SalesCategoryActivity, "latitude")}"
            parameterMap["emp_log"] = "${Preferences.getPreference(this@SalesCategoryActivity, "longitude")}"

            Log.i("Result222", "parameterMap : $parameterMap")
            CompositeDisposable().add(apiService.callEmployeePatientRemovecart(parameterMap)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe({ result ->
                        if (mDialog != null && mDialog.isShowing)
                            mDialog.dismiss()
                        Log.i("Result", "" + result.toString())
                        if (result != null) {
                            if (result.status == "0") {
                                deleteId = ""
                                callEmployeePatientCart()
                                //  CUC.displayToast(this@SalesCategoryActivity, result.show_status, result.message)
                            } else if (result.status == "10") {
                                apiClass!!.callApiKey(EMPLOYEEPATIENTREMOVECART)
                            } else {
                                CUC.displayToast(this@SalesCategoryActivity, result.show_status, result.message)
                            }
                        } else {
                            //   CUC.displayToast(this@SalesCategoryActivity, "0", getString(R.string.connection_to_database_failed))
                        }
                    }, { error ->
                        if (mDialog != null && mDialog.isShowing)
                            mDialog.dismiss()
                        CUC.displayToast(this@SalesCategoryActivity, "0", getString(R.string.connection_to_database_failed))
                        error.printStackTrace()
                    })
            )
        }
    }

    fun callEmployeePatientUpdatecart() {
        if (CUC.isNetworkAvailablewithPopup(this@SalesCategoryActivity)) {
            mDialog = CUC.createDialog(this@SalesCategoryActivity)
            val apiService = RequetsInterface.Factory.create()
            val parameterMap = HashMap<String, String>()
            parameterMap["api_key"] = "${Preferences.getPreference(this@SalesCategoryActivity, "API_KEY")}"
            parameterMap["cart_detail_id"] = "$editId"
            if (logindata != null) {
                parameterMap["store_id"] = logindata.user_store_id
                parameterMap["employee_id"] = logindata.user_id
            }
            parameterMap["patient_id"] = queue_patient_id
            parameterMap["product_id"] = "$product_id"
            parameterMap["category_id"] = "$category_id"
            parameterMap["package_id"] = "$package_id"
            parameterMap["qty"] = "$changeQty"
            parameterMap["terminal_id"] = "${mAppBasic.terminal_id}"
            parameterMap["emp_lat"] = "${Preferences.getPreference(this@SalesCategoryActivity, "latitude")}"
            parameterMap["emp_log"] = "${Preferences.getPreference(this@SalesCategoryActivity, "longitude")}"
            parameterMap["queue_id"] = "$queue_id"

            Log.i("Result", "parameterMap : $parameterMap")
            CompositeDisposable().add(apiService.callEmployeeSalesUpdatecart(parameterMap)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe({ result ->
                        if (mDialog != null && mDialog.isShowing)
                            mDialog.dismiss()
                        Log.i("Result", "" + result.toString())
                        if (result != null) {
                            if (result.status == "0") {
                                changeQty = ""
                                editId = ""
                                category_id = ""
                                package_id = ""
                                category_id = ""
//                                if (productQtyDialog != null && productQtyDialog.isShowing) {
//                                    productQtyDialog.dismiss()
//                                }
                                if (dialogProductQty != null && dialogProductQty.isShowing) {
                                    dialogProductQty.dismiss()
                                }
                                callEmployeePatientCart()
                                CUC.displayToast(this@SalesCategoryActivity, result.show_status, result.message)
                            } else if (result.status == "10") {
                                apiClass!!.callApiKey(EMPLOYEEPATIENTUPDATECART)
                            } else {
                                CUC.displayToast(this@SalesCategoryActivity, result.show_status, result.message)
                            }
                        } else {
                            //  CUC.displayToast(this@SalesCategoryActivity, "0", getString(R.string.connection_to_database_failed))
                        }
                    }, { error ->
                        if (mDialog != null && mDialog.isShowing)
                            mDialog.dismiss()
                        CUC.displayToast(this@SalesCategoryActivity, "0", getString(R.string.connection_to_database_failed))
                        error.printStackTrace()
                    })
            )
        }
    }

//    fun callEmployeePatientPreorder() {
//        if (CommonUtils.isNetworkAvailablewithPopup(this@SalesProductsActivity)) {
//            mDialog = CommonUtils.createDialog(this@SalesProductsActivity)
//            val apiService = RequetsInterface.create()
//            val parameterMap = HashMap<String, String>()
//            parameterMap["api_key"] = "${Preferences.getPreference(this@SalesProductsActivity, "API_KEY")}"
//            parameterMap["store_id"] = "${storeData.store_id}"
//            parameterMap["patient_id"] = "${logindata.patient_data.patient_id}"
//            Log.i("Result", "parameterMap : $parameterMap")
//            CompositeDisposable().add(apiService.callEmployeePatientPreorder(parameterMap)
//                    .observeOn(AndroidSchedulers.mainThread())
//                    .subscribeOn(Schedulers.io())
//                    .subscribe({ result ->
//                        if (mDialog != null && mDialog.isShowing)
//                            mDialog.dismiss()
//                        Log.i("Result", "" + result.toString())
//                        if (result != null) {
//                            if (result.status == "0") {
//                                val dispatchType = Gson().toJson(result)
//                                val goIntent = Intent(this@SalesProductsActivity, VerticalStepperAdapterFragment::class.java)
//                                goIntent.putExtra("dispatchType", "$dispatchType")
//                                goIntent.putExtra("mainCartId", "$mainCartId")
//                                startActivityForResult(goIntent, 666)
//                                //DisplayDispatchTypeDialog()
//                            } else if (result.status == "10") {
//                                API_STATUS = EMPLOYEEPATIENTPREORDER
//                                callApiKey()
//                            } else {
//                                mainCartId = result.cart_id
//                                serve_type = result.serve_type
//                                delivery_address = result.delivery_address
//                                delivery_lat = result.delivery_lat
//                                delivery_log = result.delivery_log
//                                callEmployeePatientConfirmOrder()
//                                CommonUtils.displayToast(this@SalesProductsActivity, result.show_status, result.message)
//                            }
//                        } else {
//                            CommonUtils.displayToast(this@SalesProductsActivity, "0", getString(R.string.connection_to_database_failed))
//                        }
//                    }, { error ->
//                        if (mDialog != null && mDialog.isShowing)
//                            mDialog.dismiss()
//                        CommonUtils.displayToast(this@SalesProductsActivity, "0", getString(R.string.connection_to_database_failed))
//                        error.printStackTrace()
//                    })
//            )
//        }
//    }

    fun callEmployeePatientConfirmOrder() {
        if (CUC.isNetworkAvailablewithPopup(this@SalesCategoryActivity)) {
            mDialog = CUC.createDialog(this@SalesCategoryActivity)
            val apiService = RequetsInterface.create()
            //val parameterMap = HashMap<String, String>()
            val parameterMap = java.util.HashMap<String, RequestBody>()

            parameterMap["api_key"] = RequestBody.create(MediaType.parse("text/plain"),
                    "${Preferences.getPreference(this@SalesCategoryActivity, "API_KEY")}")
            parameterMap["cart_id"] = RequestBody.create(MediaType.parse("text/plain"), "$mainCartId")
            if (logindata != null) {
                parameterMap["employee_id"] = RequestBody.create(MediaType.parse("text/plain"), "${logindata.user_id}")
            }
            parameterMap["payment_id"] = RequestBody.create(MediaType.parse("text/plain"), "$paymentId")
            parameterMap["terminal_id"] = RequestBody.create(MediaType.parse("text/plain"), "${mAppBasic.terminal_id}")
            parameterMap["emp_lat"] = RequestBody.create(MediaType.parse("text/plain"), "${Preferences.getPreference(this@SalesCategoryActivity, "latitude")}")
            parameterMap["emp_log"] = RequestBody.create(MediaType.parse("text/plain"), "${Preferences.getPreference(this@SalesCategoryActivity, "longitude")}")
            parameterMap["queue_id"] = RequestBody.create(MediaType.parse("text/plain"), "$queue_id")
            parameterMap["payment_amount"] = RequestBody.create(MediaType.parse("text/plain"), "$change_amount")
            val changeAmount = change_amount.toFloat() - payment_amount.toFloat()
            parameterMap["change_amount"] = RequestBody.create(MediaType.parse("text/plain"), "$changeAmount")
            Log.i("Result", "parameterMap : $parameterMap")
            var disposable: Observable<GetEmployeeQenerateInvoice>
            if (Store_sign_required == "1") {
                val sigantureFile = File(siganture_path)
                disposable = apiService.callEmployeeSalesInvoice(parameterMap,
                        MultipartBody.Part.createFormData("digital_siganture", "${sigantureFile.name}",
                                RequestBody.create(MediaType.parse("image*//*"), sigantureFile)),
                        RequestBody.create(MediaType.parse("text/plain"), "${sigantureFile.name}"))
            } else {
                disposable = apiService.callEmployeeSalesInvoice(parameterMap)
            }
            CompositeDisposable().add(disposable
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe({ result ->
                        if (mDialog != null && mDialog.isShowing)
                            mDialog.dismiss()

                        Log.i("Result", "" + result.toString())
                        if (result != null) {
                            if (result.status == "0") {
                                if (customDialog != null && customDialog.isShowing)
                                    customDialog.dismiss()
                                CUC.displayToast(this@SalesCategoryActivity, result.show_status, result.message)
                                //setResult(Activity.RESULT_OK)
                                Preferences.setPreference(this@SalesCategoryActivity, "total_qty", "0")
                                val i = Intent("android.intent.action.MAINQTY")
                                this@SalesCategoryActivity.sendBroadcast(i)
                                setResult(Activity.RESULT_OK)
                                val invoiceStr = Gson().toJson(result)
                                val myIntent = Intent(this@SalesCategoryActivity, SalesReceiptPrintActivity::class.java)
                                //myIntent.putExtra("all_data", "$all_data")
                                myIntent.putExtra("invoiceStr", "$invoiceStr")
                                startActivityForResult(myIntent, request_code)
                                finish()
                            } else if (result.status == "10") {
                                apiClass!!.callApiKey(EMPLOYEEPATIENTCONFIRMORDER)
                            } else {
                                CUC.displayToast(this@SalesCategoryActivity, result.show_status, result.message)
                            }
                        } else {
                            //   CUC.displayToast(this@SalesCategoryActivity, "0", getString(R.string.connection_to_database_failed))
                        }
                    }, { error ->
                        if (mDialog != null && mDialog.isShowing)
                            mDialog.dismiss()
                        CUC.displayToast(this@SalesCategoryActivity, "0", getString(R.string.connection_to_database_failed))
                        error.printStackTrace()
                    })
            )
        }
    }

    fun callEmployeePatientApplyLoyalty() {
        if (CUC.isNetworkAvailablewithPopup(this@SalesCategoryActivity)) {
            mDialog = CUC.createDialog(this@SalesCategoryActivity)
            val apiService = RequetsInterface.create()
            val parameterMap = HashMap<String, String>()
            parameterMap["api_key"] = "${Preferences.getPreference(this@SalesCategoryActivity, "API_KEY")}"
            if (logindata != null) {
                parameterMap["store_id"] = logindata.user_store_id
            }
            parameterMap["patient_id"] = queue_patient_id
            //parameterMap["loyalty_points"] = "$loyalty_points"
            Log.i("Result", "parameterMap : $parameterMap")
            CompositeDisposable().add(apiService.callEmployeePatientApplyLoyalty(parameterMap)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe({ result ->
                        if (mDialog != null && mDialog.isShowing)
                            mDialog.dismiss()
                        Log.i("Result", "" + result.toString())
                        if (result != null) {
                            if (result.status == "0") {
                                dialogProductQty.dismiss()
                                callEmployeePatientCart()
                                CUC.displayToast(this@SalesCategoryActivity, result.show_status, result.message)
                            } else if (result.status == "10") {
                                apiClass!!.callApiKey(EMPLOYEEPATIENTAPPLYLOYALTY)
                            } else {
                                CUC.displayToast(this@SalesCategoryActivity, result.show_status, result.message)
                            }
                        } else {
                            //   CUC.displayToast(this@SalesCategoryActivity, "0", getString(R.string.connection_to_database_failed))
                        }
                    }, { error ->
                        if (mDialog != null && mDialog.isShowing)
                            mDialog.dismiss()
                        CUC.displayToast(this@SalesCategoryActivity, "0", getString(R.string.connection_to_database_failed))
                        error.printStackTrace()
                    })
            )
        }
    }

    fun callEmployeeSalesApplyStmanDiscount() {
        if (CUC.isNetworkAvailablewithPopup(this@SalesCategoryActivity)) {
            mDialog = CUC.createDialog(this@SalesCategoryActivity)
            val apiService = RequetsInterface.create()
            val parameterMap = HashMap<String, String>()
            parameterMap["api_key"] = "${Preferences.getPreference(this@SalesCategoryActivity, "API_KEY")}"
            if (logindata != null) {
                parameterMap["store_id"] = "${logindata.user_store_id}"
                parameterMap["employee_id"] = "${logindata.user_id}"
            }
            parameterMap["patient_id"] = "$queue_patient_id"
            //parameterMap["loyalty_points"] = "$loyalty_points"
            parameterMap["terminal_id"] = "${mAppBasic.terminal_id}"
            parameterMap["queue_id"] = "$queue_id"
            parameterMap["discount_type"] = "$discount_type"
            parameterMap["discount_value"] = "$discount_value"
            parameterMap["employee_pin"] = "$pin_value"
            parameterMap["emp_lat"] = "${Preferences.getPreference(this@SalesCategoryActivity, "latitude")}"
            parameterMap["emp_log"] = "${Preferences.getPreference(this@SalesCategoryActivity, "longitude")}"
            Log.i("Result", "parameterMap : $parameterMap")
            CompositeDisposable().add(apiService.callEmployeeSalesApplyStmanDiscount(parameterMap)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe({ result ->
                        if (mDialog != null && mDialog.isShowing)
                            mDialog.dismiss()
                        Log.i("Result", "" + result.toString())
                        if (result != null) {
                            if (result.status == "0") {
                                pin_value = ""
                                dialogProductQty.dismiss()
                                callEmployeePatientCart()
                                CUC.displayToast(this@SalesCategoryActivity, result.show_status, result.message)
                            } else if (result.status == "10") {
                                apiClass!!.callApiKey(EMPLOYEESALESAPPLYSTMANDISCOUNT)
                            } else {
                                CUC.displayToast(this@SalesCategoryActivity, result.show_status, result.message)
                            }
                        } else {
                            // CUC.displayToast(this@SalesCategoryActivity, "0", getString(R.string.connection_to_database_failed))
                        }
                    }, { error ->
                        if (mDialog != null && mDialog.isShowing)
                            mDialog.dismiss()
                        CUC.displayToast(this@SalesCategoryActivity, "0", getString(R.string.connection_to_database_failed))
                        error.printStackTrace()
                    })
            )
        }
    }

    fun callEmployeeSalesRemoveStmanDiscount() {
        if (CUC.isNetworkAvailablewithPopup(this@SalesCategoryActivity)) {
            mDialog = CUC.createDialog(this@SalesCategoryActivity)
            val apiService = RequetsInterface.create()
            val parameterMap = HashMap<String, String>()
            parameterMap["api_key"] = "${Preferences.getPreference(this@SalesCategoryActivity, "API_KEY")}"
            if (logindata != null) {
                parameterMap["store_id"] = "${logindata.user_store_id}"
                parameterMap["employee_id"] = "${logindata.user_id}"
            }
            parameterMap["patient_id"] = "$queue_patient_id"
            parameterMap["terminal_id"] = "${mAppBasic.terminal_id}"
            parameterMap["queue_id"] = "$queue_id"
            parameterMap["employee_pin"] = "$pin_value"
            parameterMap["emp_lat"] = "${Preferences.getPreference(this@SalesCategoryActivity, "latitude")}"
            parameterMap["emp_log"] = "${Preferences.getPreference(this@SalesCategoryActivity, "longitude")}"
            Log.i("Result", "parameterMap : $parameterMap")
            CompositeDisposable().add(apiService.callEmployeeSalesRemoveStmanDiscount(parameterMap)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe({ result ->
                        if (mDialog != null && mDialog.isShowing)
                            mDialog.dismiss()
                        Log.i("Result", "" + result.toString())
                        if (result != null) {
                            if (result.status == "0") {
                                discount_value = "0"
                                pin_value = ""
                                dialogProductQty.dismiss()
                                callEmployeePatientCart()
                                CUC.displayToast(this@SalesCategoryActivity, result.show_status, result.message)
                            } else if (result.status == "10") {
                                apiClass!!.callApiKey(EMPLOYEESALESREMOVESTMANDISCOUNT)
                            } else {
                                CUC.displayToast(this@SalesCategoryActivity, result.show_status, result.message)
                            }
                        } else {
                            // CUC.displayToast(this@SalesCategoryActivity, "0", getString(R.string.connection_to_database_failed))
                        }
                    }, { error ->
                        if (mDialog != null && mDialog.isShowing)
                            mDialog.dismiss()
                        CUC.displayToast(this@SalesCategoryActivity, "0", getString(R.string.connection_to_database_failed))
                        error.printStackTrace()
                    })
            )
        }
    }

    fun callEmployeeSalesRemovePromocode() {
        if (CUC.isNetworkAvailablewithPopup(this@SalesCategoryActivity)) {
            mDialog = CUC.createDialog(this@SalesCategoryActivity)
            val apiService = RequetsInterface.create()
            val parameterMap = HashMap<String, String>()
            parameterMap["api_key"] = Preferences.getPreference(this@SalesCategoryActivity, "API_KEY")
            if (logindata != null) {
                parameterMap["employee_id"] = logindata!!.user_id
                parameterMap["store_id"] = logindata!!.user_store_id
            }
            if (mAppBasic != null) {
                parameterMap["terminal_id"] = "${mAppBasic!!.terminal_id}"
            }
            parameterMap["employee_lat"] = "${Preferences.getPreference(this@SalesCategoryActivity, "latitude")}"
            parameterMap["employee_log"] = "${Preferences.getPreference(this@SalesCategoryActivity, "longitude")}"
            parameterMap["queue_id"] = queue_id
            parameterMap["patient_id"] = viewCard!!.main_cart.cart_patient_id
            Log.i("Result", "parameterMap : $parameterMap")
            CompositeDisposable().add(apiService.callEmployeeSalesRemovePromocode(parameterMap)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe({ result ->
                        if (mDialog != null && mDialog!!.isShowing)
                            mDialog!!.dismiss()
                        if (result != null) {
                            Log.i("Result", "" + result.toString())
                            if (result.status == "0") {
                                callEmployeePatientCart()
                            } else if (result.status == "10") {
                                //callApiKey()
                            } else {
                                CUC.displayToast(this@SalesCategoryActivity, result.show_status, result.message)
                            }
                        } else {
                            //  CUC.displayToast(this@SalesCategoryActivity, "0", getString(R.string.connection_to_database_failed))
                        }
                    }, { error ->
                        if (mDialog != null && mDialog!!.isShowing)
                            mDialog!!.dismiss()
                        CUC.displayToast(this@SalesCategoryActivity, "0", getString(R.string.connection_to_database_failed))
                        error.printStackTrace()
                    })
            )
        }
    }

    fun callEmployeePatientRemoveLoyalty() {
        if (CUC.isNetworkAvailablewithPopup(this@SalesCategoryActivity)) {
            mDialog = CUC.createDialog(this@SalesCategoryActivity)
            val apiService = RequetsInterface.Factory.create()
            val parameterMap = HashMap<String, String>()
            parameterMap["api_key"] = "${Preferences.getPreference(this@SalesCategoryActivity, "API_KEY")}"
            if (logindata != null) {
                parameterMap["store_id"] = logindata.user_store_id
            }
            parameterMap["patient_id"] = queue_patient_id
            Log.i("Result", "parameterMap : $parameterMap")
            CompositeDisposable().add(apiService.callEmployeePatientRemoveLoyalty(parameterMap)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe({ result ->
                        if (mDialog != null && mDialog.isShowing)
                            mDialog.dismiss()
                        Log.i("Result", "" + result.toString())
                        if (result != null) {
                            if (result.status == "0") {
                                dialogProductQty.dismiss()
                                callEmployeePatientCart()
                                CUC.displayToast(this@SalesCategoryActivity, result.show_status, result.message)
                            } else if (result.status == "10") {
                                apiClass!!.callApiKey(EMPLOYEEPATIENTREMOVELOYALTY)
                            } else {
                                CUC.displayToast(this@SalesCategoryActivity, result.show_status, result.message)
                            }
                        } else {
                            // CUC.displayToast(this@SalesCategoryActivity, "0", getString(R.string.connection_to_database_failed))
                        }
                    }, { error ->
                        if (mDialog != null && mDialog.isShowing)
                            mDialog.dismiss()
                        CUC.displayToast(this@SalesCategoryActivity, "0", getString(R.string.connection_to_database_failed))
                        error.printStackTrace()
                    })
            )
        }
    }

    var discount_type: String = "1"
    var discount_value: String = "0"
    var pin_value: String = ""
    fun DisplayRedeemPoints() {
        discount_type = "1"
        //loyalty_points = "0.00"
//        val view = layoutInflater.inflate(R.layout.redeem_point_dialog, null)
//        dialogChangePin = BottomSheetDialog(this)
//        dialogChangePin.setContentView(view)
        dialogProductQty = Dialog(this@SalesCategoryActivity, R.style.TransparentProgressDialog)
        val policy = StrictMode.ThreadPolicy.Builder().permitAll().build()
        StrictMode.setThreadPolicy(policy)
        //dialog_changepass.window.setLayout(resources.getDimensionPixelSize(R.dimen._10sdp) ,ViewGroup.LayoutParams.WRAP_CONTENT)
        dialogProductQty.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialogProductQty.setContentView(R.layout.redeem_point_dialog)
        dialogProductQty.window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)
        dialogProductQty.show()

        val cv_approve_request = dialogProductQty.findViewById<LinearLayout>(R.id.cv_approve_request)
        val cv_generate_invoice = dialogProductQty.findViewById<LinearLayout>(R.id.cv_generate_invoice)
        val rgdiscounttype = dialogProductQty.findViewById<RadioGroup>(R.id.rgdiscounttype)
        val edt_value = dialogProductQty.findViewById<EditText>(R.id.edt_value)
        val tv_discounttitle = dialogProductQty.findViewById<TextView>(R.id.tv_discounttitle)
        val edt_pin = dialogProductQty.findViewById<EditText>(R.id.edt_pin)
        val ll_discount_type = dialogProductQty.findViewById<LinearLayout>(R.id.ll_discount_type)
        val ll_fix_discount = dialogProductQty.findViewById<LinearLayout>(R.id.ll_fix_discount)
        ll_discount_type.visibility = View.VISIBLE
        ll_fix_discount.visibility = View.VISIBLE

        rgdiscounttype.setOnCheckedChangeListener(object : RadioGroup.OnCheckedChangeListener {
            override fun onCheckedChanged(p0: RadioGroup?, p1: Int) {
                when (p1) {
                    R.id.rbFixDiscount -> {
                        discount_type = "1"
                        edt_value.hint = "Enter Fix Discount"
                        tv_discounttitle.text = "Enter Fix Discount"
                    }
                    R.id.rbPercentageDiscount -> {
                        discount_type = "2"
                        edt_value.hint = "Enter Percentage Discount"
                        tv_discounttitle.text = "Enter Percentage Discount"
                    }
                }
            }
        })
        cv_approve_request.setOnClickListener {
            dialogProductQty.dismiss()
        }
        dialogProductQty.setCancelable(false)

        cv_generate_invoice.setOnClickListener {
            if (edt_value.text.toString() == "") {
                CUC.displayToast(this@SalesCategoryActivity, "0", "Please ${tv_discounttitle.text}")
            } else if (edt_value.text.toString().toFloat() <= 0) {
                CUC.displayToast(this@SalesCategoryActivity, "0", "Please ${tv_discounttitle.text} greater than 0")
            } else if (edt_pin.text.toString() == "") {
                CUC.displayToast(this@SalesCategoryActivity, "0", "Please Enter Your PIN")
            } else {
                discount_value = "${edt_value.text}"
                pin_value = "${edt_pin.text}"
                callEmployeeSalesApplyStmanDiscount()
            }
        }
    }

    private fun DisplayRemoveStmanDiscount() {
        dialogProductQty = Dialog(this@SalesCategoryActivity, R.style.TransparentProgressDialog)
        val policy = StrictMode.ThreadPolicy.Builder().permitAll().build()
        StrictMode.setThreadPolicy(policy)
        dialogProductQty.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialogProductQty.setContentView(R.layout.redeem_point_dialog)
        dialogProductQty.window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)
        dialogProductQty.show()

        val cv_approve_request = dialogProductQty.findViewById<LinearLayout>(R.id.cv_approve_request)
        val cv_generate_invoice = dialogProductQty.findViewById<LinearLayout>(R.id.cv_generate_invoice)
        val edt_value = dialogProductQty.findViewById<EditText>(R.id.edt_value)
        val tv_discounttitle = dialogProductQty.findViewById<TextView>(R.id.tv_discounttitle)
        val edt_pin = dialogProductQty.findViewById<EditText>(R.id.edt_pin)
        val ll_discount_type = dialogProductQty.findViewById<LinearLayout>(R.id.ll_discount_type)
        val ll_fix_discount = dialogProductQty.findViewById<LinearLayout>(R.id.ll_fix_discount)
        val tv_submit = dialogProductQty.findViewById<TextView>(R.id.tv_submit)

        ll_discount_type.visibility = View.GONE
        ll_fix_discount.visibility = View.GONE
        tv_submit.text = "Remove"

        cv_approve_request.setOnClickListener {
            dialogProductQty.dismiss()
        }

        cv_generate_invoice.setOnClickListener {
            if (edt_pin.text.toString() == "") {
                CUC.displayToast(this@SalesCategoryActivity, "0", "Please Enter Your PIN")
            } else {
                pin_value = "${edt_pin.text}"
                callEmployeeSalesRemoveStmanDiscount()
            }
        }
    }


    var packetList = java.util.ArrayList<GetProductPackets>()
    lateinit var mPacketAdapter: PacketAdapter
    var package_id = ""

    fun DisplayProductQtyDialog(productData: GetCartData) {

        package_id = ""

        var countDeci = 0

        try {
            countDeci = "${productData.product_mapping.product_default_qty}".split("\\.".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()[1].length
        } catch (e: Exception) {
            e.printStackTrace()
        }

        val strForm = "%.${countDeci}f"
        println("CountFormat -  countDeci : $countDeci")
        productQytValue = "${productData.product_mapping.product_default_qty}".toFloat()
        println("CountFormat -  productQytValue : $productQytValue")
        val incriVal = "${productData.product_mapping.product_qty}".toFloat()
        println("CountFormat -  incriVal : $incriVal")

        dialogProductQty = Dialog(this@SalesCategoryActivity, R.style.TransparentProgressDialog)
        val policy = StrictMode.ThreadPolicy.Builder().permitAll().build()
        StrictMode.setThreadPolicy(policy)
        //dialog_changepass.window.setLayout(resources.getDimensionPixelSize(R.dimen._10sdp) ,ViewGroup.LayoutParams.WRAP_CONTENT)
        dialogProductQty.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialogProductQty.setContentView(R.layout.product_qty_dialog_new)
        dialogProductQty.window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)
        dialogProductQty.show()

        val tv_unit_name = dialogProductQty.findViewById<TextView>(R.id.tv_unit_name)!!
        val tv_product_name = dialogProductQty.findViewById<TextView>(R.id.tv_product_name)!!
        val tv_tottal_qty = dialogProductQty.findViewById<TextView>(R.id.tv_tottal_qty)!!
        val tv_first_text = dialogProductQty.findViewById<TextView>(R.id.tv_first_text)!!
        val ll_add_cart = dialogProductQty.findViewById<LinearLayout>(R.id.ll_add_cart)!!
        val tv_tottal_price = dialogProductQty.findViewById<TextView>(R.id.tv_tottal_price)

        val rl_first_minus = dialogProductQty.findViewById<RelativeLayout>(R.id.rl_first_minus)!!
        val rl_first_plus = dialogProductQty.findViewById<RelativeLayout>(R.id.rl_first_plus)
        val tv_packet_title = dialogProductQty.findViewById<TextView>(R.id.tv_packet_title)!!
        val rv_packets = dialogProductQty.findViewById<RecyclerView>(R.id.rv_packets)
        val tv_add_cart = dialogProductQty.findViewById<TextView>(R.id.tv_add_cart)

        tv_add_cart!!.text = "Update"
        tv_tottal_price.text = "$ " + "${productData.cart_detail_total_price}"
        tv_unit_name.text = "${productData.product_mapping.product_unit_name} "
        tv_product_name.text = "${productData.product_name}"
        tv_tottal_qty.text = "${productData.product_mapping.p_map_qty} ${productData.product_mapping.product_unit_name}"
        rv_packets!!.layoutManager = LinearLayoutManager(this@SalesCategoryActivity, LinearLayoutManager.HORIZONTAL, false)
        packetList.clear()
        packetList.add(productData.product_packets)
        if (productData.cart_detail_qty != "") {
            tv_first_text.text = String.format(strForm, "${productData.cart_detail_qty}".toFloat().nextUp())
            productQytValue = tv_first_text.text.toString().toFloat()
            //productData.product_mapping.p_map_qty = "${productData.product_mapping.p_map_qty.toFloat() + productData.cart_detail_qty.toFloat()}"
            //tv_first_text.text = "${productData.cart_detail_qty}"
        }
        tv_first_text!!.filters = arrayOf<InputFilter>(DigitsInputFilter(5, countDeci, Double.MAX_VALUE))

        if (productData.cart_detail_packet_id != "" && productData.cart_detail_packet_id != "0") {
            package_id = productData.cart_detail_packet_id
            for (data in packetList) {
                if (data.packet_id == productData.cart_detail_packet_id) {
                    data.isSelected = true
                } else {
                    data.isSelected = false
                }
            }
        } else {
            for (data in packetList) {
                if (data.packet_qty == tv_first_text.text.toString()) {
                    package_id = data.packet_id
                    data.isSelected = true
                } else {
                    data.isSelected = false
                }
            }
        }


        val mClickListner = object : ClickListner {
            override fun onClick(getProductPackets: GetProductPackets) {
                if (getProductPackets.isSelected) {
                    for (data in packetList) {
                        data.isSelected = false
                    }
                    productQytValue = "${productData.product_mapping.product_default_qty}".toFloat()
                    package_id = ""
                    tv_first_text.text = String.format(strForm, productQytValue.nextUp())
                } else {
                    for (data in packetList) {
                        if (data.packet_id == getProductPackets.packet_id) {
                            if (productData.product_mapping.p_map_qty != "" && productData.product_mapping.p_map_qty != null) {
                                if (productData.product_mapping.p_map_qty.toFloat() >= data.packet_qty.toFloat()) {
                                    productQytValue = data.packet_qty.toFloat()
                                    tv_first_text.text = productQytValue.toString()
                                    data.isSelected = true
                                    package_id = data.packet_id
                                } else {
                                    for (data in packetList) {
                                        data.isSelected = false
                                    }
                                    data.isSelected = false
                                    package_id = ""
                                    CUC.displayToast(this@SalesCategoryActivity, "0", "You can't set ${productData.product_mapping.product_unit_name} more then ${productData.product_mapping.p_map_qty}")
                                }
                            } else {
                                data.isSelected = false
                                package_id = ""
                                CUC.displayToast(this@SalesCategoryActivity, "0", "You can't set ${productData.product_mapping.product_unit_name} more then ${productData.product_mapping.p_map_qty}")
                            }
                        } else {
                            data.isSelected = false
                            package_id = ""
                        }
                    }
                }
                mPacketAdapter.notifyDataSetChanged()
            }
        }
        mPacketAdapter = PacketAdapter(this@SalesCategoryActivity, packetList, productData.product_mapping.product_unit_name, Currency_value, mClickListner)
        rv_packets.adapter = mPacketAdapter
        mPacketAdapter.notifyDataSetChanged()
        if (packetList.isEmpty()) {
            tv_packet_title.visibility = View.GONE
        }

        if (productData.product_mapping.p_map_qty == "" || productData.product_mapping.p_map_qty == null || productData.product_mapping.p_map_qty == "0") {
            ll_add_cart.isClickable = false
            ll_add_cart.isEnabled = false
            ll_add_cart.setBackgroundColor(ContextCompat.getColor(this@SalesCategoryActivity, R.color.gray))
            tv_tottal_qty.text = "Unavailable"
            tv_tottal_qty.setTextColor(ContextCompat.getColor(this@SalesCategoryActivity, R.color.red))
        }

        rl_first_minus.setOnClickListener {
            if (tv_first_text.text.isEmpty() || tv_first_text.text.toString() == ".") {
                tv_first_text.text = "${productData.product_mapping.product_default_qty}"
            }
            if (tv_first_text.text.toString().toFloat() != "${productData.product_mapping.product_default_qty}".toFloat()) {
                productQytValue = tv_first_text.text.toString().toFloat() - incriVal // Math.nextUp()
                tv_first_text.text = String.format(strForm, productQytValue.nextUp())
                var isSelect: Boolean = false
                var packetId = ""
//                for (data in packetList) {
//                    if (data.packet_qty == tv_first_text.text.toString()) {
//                        packetId = data.packet_id
//                        isSelect = true
//                        break
//                    }
//                }
                if (isSelect) {
                    for (data in packetList) {
                        data.isSelected = data.packet_id == packetId
                    }
                    package_id = packetId
                } else {
                    for (data in packetList) {
                        data.isSelected = false
                    }
                    package_id = ""
                }
                mPacketAdapter.notifyDataSetChanged()
            } else {
                CUC.displayToast(this@SalesCategoryActivity, "0", "You can't set ${productData.product_mapping.product_unit_name} less then ${productData.product_mapping.product_default_qty}")
            }
        }

        rl_first_plus!!.setOnClickListener {
            if (tv_first_text.text.isEmpty() || tv_first_text.text.toString() == ".") {
                tv_first_text.text = "${productData.product_mapping.product_default_qty}"
            }
            if (productData.product_mapping.p_map_qty != "" && productData.product_mapping.p_map_qty != null) {
                if (productData.product_mapping.p_map_qty.toFloat() > productQytValue) {
                    //StrictMath.nextUp()
                    productQytValue = tv_first_text.text.toString().toFloat() + incriVal // Math.nextUp()
                    tv_first_text.text = String.format(strForm, productQytValue.nextUp())
                    println("CountFormat -  plus : ${String.format(strForm, productQytValue.nextUp())}")
                    var isSelect = false
                    var packetId = ""
//                    for (data in packetList) {
//                        if (data.packet_qty == tv_first_text.text.toString()) {
//                            packetId = data.packet_id
//                            isSelect = true
//                            break
//                        }
//                    }
                    if (isSelect) {
                        for (data in packetList) {
                            data.isSelected = data.packet_id == packetId
                        }
                        package_id = packetId
                    } else {
                        for (data in packetList) {
                            data.isSelected = false
                        }
                        package_id = ""
                    }
                    mPacketAdapter.notifyDataSetChanged()
                } else {
                    CUC.displayToast(this@SalesCategoryActivity, "0", "You can't set ${productData.product_mapping.product_unit_name} more then ${productData.product_mapping.p_map_qty}")
                }
            } else {
                CUC.displayToast(this@SalesCategoryActivity, "0", "You can't set ${productData.product_mapping.product_unit_name} more then ${productData.product_mapping.p_map_qty}")
            }
        }

        ll_add_cart.setOnClickListener {
            if (tv_first_text.text.isEmpty() || tv_first_text.text.toString() == ".") {
                tv_first_text.setText("${productData.product_mapping.product_default_qty}")
            }
            if ("${tv_first_text.text}" != "") {
                if (productData.product_mapping.p_map_qty.toFloat() >= "${tv_first_text.text}".toFloat()) {
                    var isSelect = false
                    var packetId = ""
//                    for (data in packetList) {
//                        if (data.packet_qty.toFloat() == tv_first_text.text.toString().toFloat()) {
//                            packetId = data.packet_id
//                            isSelect = true
//                            break
//                        }
//                    }
//                    if (isSelect) {
//                        for (data in packetList) {
//                            data.isSelected = data.packet_id == packetId
//                        }
//                        package_id = packetId
//                    } else {
//                        for (data in packetList) {
//                            data.isSelected = false
//                        }
//                        package_id = ""
//                    }
                    mPacketAdapter.notifyDataSetChanged()

                    editId = productData.cart_detail_id
                    changeQty = "${tv_first_text.text}"
                    category_id = productData.product_mapping.p_map_store_category_id
                    product_id = productData.product_mapping.product_id

                    callEmployeePatientUpdatecart()
                } else {
                    package_id = ""
                    tv_first_text.text = String.format(strForm, "${productData.cart_detail_qty}".toFloat().nextUp())
                    var isSelect = false
                    var packetId = ""
//                    for (data in packetList) {
//                        if (data.packet_qty == tv_first_text.text.toString()) {
//                            packetId = data.packet_id
//                            isSelect = true
//                            break
//                        }
//                    }
//                    if (isSelect) {
//                        for (data in packetList) {
//                            data.isSelected = data.packet_id == packetId
//                        }
//                        package_id = packetId
//                    } else {
//                        for (data in packetList) {
//                            data.isSelected = false
//                        }
//                        package_id = ""
//                    }
                    mPacketAdapter.notifyDataSetChanged()
                    CUC.displayToast(this@SalesCategoryActivity, "0", "You can't set ${productData.product_mapping.product_unit_name} more then ${productData.product_mapping.p_map_qty}")
                }
            } else {
                package_id = ""
                tv_first_text.text = String.format(strForm, "${productData.cart_detail_qty}".toFloat().nextUp())
                var isSelect = false
                var packetId = ""
//                for (data in packetList) {
//                    if (data.packet_qty == tv_first_text.text.toString()) {
//                        packetId = data.packet_id
//                        isSelect = true
//                        break
//                    }
//                }
//                if (isSelect) {
//                    for (data in packetList) {
//                        data.isSelected = data.packet_id == packetId
//                    }
//                    package_id = packetId
//                } else {
//                    for (data in packetList) {
//                        data.isSelected = false
//                    }
//                    package_id = ""
//                }
                mPacketAdapter.notifyDataSetChanged()
                CUC.displayToast(this@SalesCategoryActivity, "0", "At list ${productData.product_mapping.product_default_qty} ${productData.product_mapping.product_unit_name} compulsory")
            }
        }
    }

    class PacketAdapter(val mContext: Context, val packetList: java.util.ArrayList<GetProductPackets>, val unit_name: String, val Currency_value: String, val mClickListner: ClickListner) : RecyclerView.Adapter<PacketAdapter.PacketHolder>() {
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PacketHolder {
            val mView = LayoutInflater.from(parent.context).inflate(R.layout.packet_item, parent, false)
            return PacketHolder(mView)
        }

        override fun getItemCount(): Int {
            return packetList.size
        }

        override fun onBindViewHolder(holder: PacketHolder, position: Int) {
            holder.bindVliew(mContext, packetList[position], unit_name, Currency_value, mClickListner)
        }

        class PacketHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
            val tv_packet_qty = itemView.findViewById<TextView>(R.id.tv_packet_qty)
            val tv_packet_price = itemView.findViewById<TextView>(R.id.tv_packet_price)
            val rl_packet_item = itemView.findViewById<RelativeLayout>(R.id.rl_packet_item)

            @SuppressLint("SetTextI18n")
            fun bindVliew(mContext: Context, getProductPackets: GetProductPackets, unit_name: String, Currency_value: String, mClickListner: ClickListner) {
                tv_packet_qty.text = "${getProductPackets.packet_qty}\n$unit_name"
                tv_packet_price.text = "$Currency_value${getProductPackets.packet_total_price}"
                if (getProductPackets.isSelected) {
                    tv_packet_qty.setTextColor(ContextCompat.getColor(mContext, R.color.white))
                    rl_packet_item.setBackgroundResource(R.drawable.packet_select_ring)
                } else {
                    tv_packet_qty.setTextColor(ContextCompat.getColor(mContext, R.color.black))
                    rl_packet_item.setBackgroundResource(R.drawable.packet_ring)
                }
                rl_packet_item.setOnClickListener {
                    mClickListner.onClick(getProductPackets)

                }
            }
        }
    }

    interface ClickListner {
        fun onClick(getProductPackets: GetProductPackets)
    }

//    interface onDispatchClickListener {
//        fun onDispatchClick(data: GetDispatchType)
//    }

    class ProductAdapter(val list: java.util.ArrayList<GetCartData>, val context: Context, val Currency_value: String, val onClick: onClickListener) : RecyclerView.Adapter<ProductAdapter.ViewHolder>() {

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            holder!!.bindItems(list[position], context, Currency_value, onClick)
        }

        override fun getItemCount(): Int {
            return list.size
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            val v = LayoutInflater.from(parent!!.context).inflate(R.layout.cart_product_items, parent, false)
            return ViewHolder(v)
        }

        class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
            val tv_name: TextView = itemView.findViewById(R.id.tv_name)
            val tv_mobilenumber: TextView = itemView.findViewById(R.id.tv_mobilenumber)
            val tv_phonenumber: TextView = itemView.findViewById(R.id.tv_phonenumber)
            val iv_delete: ImageView = itemView.findViewById(R.id.iv_delete)
            val iv_edit: ImageView = itemView.findViewById(R.id.iv_edit)
            val ll_edit:LinearLayout=itemView.findViewById(R.id.ll_edit)

            fun bindItems(data: GetCartData, context: Context, Currency_value: String, onClick: onClickListener) {
                tv_name.text = "${data.product_mapping.p_map_store_name}"
                tv_mobilenumber.text = "${data.cart_detail_qty} ${data.product_mapping.unit_short_name}"
                tv_phonenumber.text = "$Currency_value${data.cart_detail_total_price}"
                iv_delete.setOnClickListener {
                    onClick.deleteClick(data)
                }
                iv_edit.setOnClickListener {
                    onClick.editClick(data)
                }
                ll_edit.setOnClickListener{
                    onClick.editClick(data)
                }
            }
        }
    }

    interface onClickListener {
        fun deleteClick(data: GetCartData)
        fun editClick(data: GetCartData)
    }

    class TaxItemAdapter(val list: java.util.ArrayList<GetTaxData>, val context: Context, val Currency_value: String) : RecyclerView.Adapter<TaxItemAdapter.ViewHolder>() {

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            holder!!.bindItems(list[position], context, Currency_value)
        }

        override fun getItemCount(): Int {
            return list.size
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            val v = LayoutInflater.from(parent!!.context).inflate(R.layout.tax_data_item, parent, false)
            return ViewHolder(v)
        }

        class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
            val tv_tax_title: TextView = itemView.findViewById(R.id.tv_tax_title)
            val tv_tax_amount: TextView = itemView.findViewById(R.id.tv_tax_amount)


            fun bindItems(data: GetTaxData, context: Context, Currency_value: String) {
                tv_tax_title.setTextColor(context.resources.getColor(R.color.graylight))
                tv_tax_title.text = "${data.tax_name}"

                if (data.tax_status == "2") {
                    tv_tax_amount.text = "-${data.tax_total}%"
                } else {
                    if ("${data.tax_name}" == "Loyalty Amount") {
                        tv_tax_amount.text = "-$Currency_value${data.tax_total}"
                    } else if ("${data.tax_name}" == "Discount") {
                        tv_tax_amount.text = "-$Currency_value${data.tax_total}"
                    } else {
                        tv_tax_amount.text = "$Currency_value${data.tax_total}"
                    }
                }
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        unregisterReceiver(mReceiver)
    }

    override fun onBackPressed() {
        super.onBackPressed()
    }

    class PaymentAdapter(context: Context?, val resource: Int, val companyList: java.util.ArrayList<GetPaymentTypes>, val payment_path: String, val clickListner: onClickPayment) : ArrayAdapter<GetPaymentTypes>(context, resource, companyList) {

        override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
            var rootView = convertView
            if (rootView == null)
                rootView = LayoutInflater.from(context!!).inflate(resource, null, false)

            val ll_ = rootView!!.findViewById<RelativeLayout>(R.id.ll_)
            val tv_payment = rootView!!.findViewById<TextView>(R.id.tv_payment)
            val iv_payment = rootView!!.findViewById<ImageView>(R.id.iv_payment)
            val data = companyList[position]
            Glide.with(context!!)
                    .applyDefaultRequestOptions(RequestOptions().format(DecodeFormat.PREFER_ARGB_8888).error(R.drawable.no_image_available))
                    .load("$payment_path${data.payment_logo}")
                    .into(iv_payment)
            ll_.isLongClickable = false
            tv_payment.text = "${data.payment_method}"
            ll_.setOnClickListener {
                clickListner.callClick(data)
            }
            return rootView!!
        }
    }

    interface onClickPayment {
        fun callClick(data: GetPaymentTypes)
    }

    var payment_amount = ""
    var change_amount = ""
    lateinit var iv_siganture: ImageView


    fun callInvoiceGenerated() {
        paymentId = ""
        customDialog = Dialog(this@SalesCategoryActivity, R.style.TransparentProgressDialog)
        val policy = StrictMode.ThreadPolicy.Builder().permitAll().build()
        StrictMode.setThreadPolicy(policy)
        customDialog.window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE)
        customDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        customDialog.setContentView(R.layout.sales_invoice_dialog_new)
        customDialog.window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)
        customDialog.show()

        val amount_field = customDialog.findViewById<EditText>(R.id.text_amount)
        //amount_field.setText("$total_payble")
        amount_field.setSelection(amount_field.text.toString().length)


        val onClickListner = View.OnClickListener {
            val editable = amount_field.getText().toString()
            val valueString = editable + (it.tag as String)

            if (valueString.contains(".")) {
                val f = java.lang.Float.valueOf(valueString)
                amount_field.setText(String.format("%.2f", f))
            } else {
                amount_field.setText(valueString)
            }
        }

        val tv_total_amount = customDialog.findViewById<TextView>(R.id.tv_total_amount)
        val edt_total_amt = customDialog.findViewById<EditText>(R.id.edt_total_amt)
        val tv_customer_name = customDialog.findViewById<TextView>(R.id.tv_customer_name)
        val bs_payment = customDialog.findViewById<BSV>(R.id.bs_payment)
        val cv_generate_invoice = customDialog.findViewById<TextView>(R.id.cv_generate_invoice)
        val ll_digital_siganture = customDialog.findViewById<LinearLayout>(R.id.ll_digital_siganture)
        val tv_digital_siganture: TextView = customDialog.findViewById(R.id.tv_digital_siganture)
        val tv_cancel = customDialog.findViewById<TextView>(R.id.tv_cancel)
        val recy_payment = customDialog.findViewById<RecyclerView>(R.id.recy_payment)
        val llPaymentType = customDialog.findViewById<LinearLayout>(R.id.llPaymentType)

        val t9_key_1 = customDialog.findViewById<TextView>(R.id.t9_key_1)
        val t9_key_2 = customDialog.findViewById<TextView>(R.id.t9_key_2)
        val t9_key_3 = customDialog.findViewById<TextView>(R.id.t9_key_3)
        val t9_key_4 = customDialog.findViewById<TextView>(R.id.t9_key_4)
        val t9_key_5 = customDialog.findViewById<TextView>(R.id.t9_key_5)
        val t9_key_6 = customDialog.findViewById<TextView>(R.id.t9_key_6)
        val t9_key_7 = customDialog.findViewById<TextView>(R.id.t9_key_7)
        val t9_key_8 = customDialog.findViewById<TextView>(R.id.t9_key_8)
        val t9_key_9 = customDialog.findViewById<TextView>(R.id.t9_key_9)
        val t9_key_0 = customDialog.findViewById<TextView>(R.id.t9_key_0)
        val t9_key_dote = customDialog.findViewById<TextView>(R.id.t9_key_dote)
        var width = 0
        var height = 0
        val vto = llPaymentType.viewTreeObserver
        vto.addOnGlobalLayoutListener(object : ViewTreeObserver.OnGlobalLayoutListener {
            @Override
            override fun onGlobalLayout() {
                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                    llPaymentType.getViewTreeObserver().removeGlobalOnLayoutListener(this)
                } else {
                    llPaymentType.getViewTreeObserver().removeOnGlobalLayoutListener(this)
                }
                if (paymentTypeList != null && paymentTypeList.size > 0) {
                    if (paymentTypeList.size == 1) {
                        width = llPaymentType.measuredWidth
                        height = llPaymentType.measuredHeight
                        recy_payment.adapter = PaymentMethodAdapter(this@SalesCategoryActivity, width)
                        //  Log.d("Width111"," = "+width)
                    } else if (paymentTypeList.size == 2) {
                        width = llPaymentType.measuredWidth / 2
                        height = llPaymentType.measuredHeight
                        recy_payment.adapter = PaymentMethodAdapter(this@SalesCategoryActivity, width)
                        //   Log.d("Width122"," = "+width)
                    } else {
                        width = llPaymentType.measuredWidth / 3
                        height = llPaymentType.measuredHeight
                        recy_payment.adapter = PaymentMethodAdapter(this@SalesCategoryActivity, width)
                        // Log.d("Width33"," = "+width)
                    }
                }

            }
        })
        //Log.d("Width33"," = "+width)
        recy_payment.layoutManager = LinearLayoutManager(this, LinearLayout.HORIZONTAL, false)

        t9_key_1.setOnClickListener(onClickListner)
        t9_key_2.setOnClickListener(onClickListner)
        t9_key_3.setOnClickListener(onClickListner)
        t9_key_4.setOnClickListener(onClickListner)
        t9_key_5.setOnClickListener(onClickListner)
        t9_key_6.setOnClickListener(onClickListner)
        t9_key_7.setOnClickListener(onClickListner)
        t9_key_8.setOnClickListener(onClickListner)
        t9_key_9.setOnClickListener(onClickListner)
        t9_key_0.setOnClickListener(onClickListner)
        t9_key_dote.setOnClickListener { amount_field.append(".") }


        amount_field.setOnClickListener {
            CUC.hideSoftKeyboard(this@SalesCategoryActivity)
        }
        amount_field.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(text: CharSequence?, p1: Int, p2: Int, p3: Int) {

                /*if (amount_field.text.toString().equals()) {

                }*/
            }

            override fun afterTextChanged(textdata: Editable?) {

            }

        })

        customDialog.findViewById<TextView>(R.id.t9_key_backspace).setOnClickListener {
            amount_field.setText("")
        }


        tv_cancel.setOnClickListener {
            customDialog.dismiss()
        }
        iv_siganture = customDialog.findViewById(R.id.iv_siganture)

        /*val onClickPayment = object : onClickPayment {
            override fun callClick(data: GetPaymentTypes) {
                paymentId = "${data.payment_id}"
                bs_payment.setText("${data.payment_method}")
                bs_payment.dismissDropDown()
                bs_payment.clearFocus()
            }
        }*/


        /* if (Store_sign_required != "" && Store_sign_required == "1") {
             ll_digital_siganture.visibility = View.VISIBLE
         } else {
             ll_digital_siganture.visibility = View.GONE
         }

         iv_siganture.setOnClickListener {
             openSignatureActivity()
         }
         tv_digital_siganture.setOnClickListener {
             openSignatureActivity()
         }
 */

        //paymentAdapter = PaymentAdapter(this@SalesCategoryActivity, R.layout.payment_type_item, paymentTypeList, payment_path, onClickPayment)
        //bs_payment.setAdapter(paymentAdapter)
        //tv_customer_name.text = "${selecteQueue.first_name} ${selecteQueue.last_name}"
        tv_total_amount.text = "$total_payble"

        try {
            if (paymentTypeList != null && paymentTypeList.size > 0) {
                //if (paymentTypeList.size == 1) {
                paymentId = "${paymentTypeList[0].payment_id}"
                bs_payment.setText("${paymentTypeList[0].payment_method}")
                bs_payment.dismissDropDown()
                bs_payment.clearFocus()
                //}
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }

        cv_generate_invoice.setOnClickListener {

            // Log.d("PayMentId",""+paymentId)
            if (paymentId == "") {
                CUC.displayToast(this@SalesCategoryActivity, "0", "Please select payment")
            } else {
                if (amount_field.text.toString() != "") {

                    //"$total_payble"
                    if (amount_field.text.toString().toFloat() >= "$total_payble".toFloat()) {
                        val newstring =
                                if ("${amount_field.text}".toFloat() >= "${amount_field!!.text}".toFloat()) {
                                    if (Store_sign_required == "1") {
                                        //  Log.d("AAAA","AAA")
                                        payment_amount = "${amount_field.text}"
                                        change_amount = "${amount_field.text}"
                                        openSignatureActivity()
//                                    if (siganture_path != "") {
//                                        Log.d("AAAA11","AAA")
//                                        payment_amount = "${amount_field.text}"
//                                        change_amount = "${amount_field.text}"
//                                        callEmployeePatientConfirmOrder()
//                                    } else {
//                                        Log.d("AAAA22","AAA")
//                                        CUC.displayToast(this@SalesCategoryActivity, "0", "Patient signature required")
//                                    }
                                    } else {
                                        //     Log.d("AAAA33","AAA")
                                        siganture_path = ""
                                        payment_amount = "${amount_field.text}"
                                        change_amount = "${amount_field.text}"
                                        callEmployeePatientConfirmOrder()
                                    }
                                } else {
                                    CUC.displayToast(this@SalesCategoryActivity, "0", "Please pay complete amount")
                                }
                    } else {
                        CUC.displayToast(this@SalesCategoryActivity, "0", "Please pay full amount")
                    }

                } else {
                    CUC.displayToast(this@SalesCategoryActivity, "0", "Please Enter Paid Amount")
                }
            }
        }
    }

    inner class PaymentMethodAdapter(context: Context, widths: Int) : RecyclerView.Adapter<PaymentMethodAdapter.PaymentMethodAdapter>() {
        var row_index = 0
        var width = 0

        init {
            this.width = widths
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PaymentMethodAdapter {
            val v = LayoutInflater.from(parent.context).inflate(R.layout.raw_payment_method, parent, false)

            return PaymentMethodAdapter(v)

        }

        override fun getItemCount(): Int {
            return paymentTypeList.size
        }


        override fun onBindViewHolder(holder: PaymentMethodAdapter, position: Int) {

            holder.tv_paymetMethod.text = paymentTypeList.get(position).payment_method

            holder.tv_paymetMethod.setOnClickListener {
                paymentId = paymentTypeList.get(position).payment_id
                row_index = position
                notifyDataSetChanged()
            }
            val linearLayout = LinearLayout.LayoutParams(width, LinearLayout.LayoutParams.WRAP_CONTENT)

            //   Log.d("",""+width)
            //  Log.d("SASASASASS",""+width)
            holder.ll_linear.layoutParams = linearLayout
            //   holder.tv_paymetMethod.height=height
            //  holder.tv_paymetMethod.setWidth(100)
            if (row_index == position) {
                holder.tv_paymetMethod.setBackgroundColor(resources.getColor(R.color.blue))
            } else {
                holder.tv_paymetMethod.setBackgroundColor(resources.getColor(R.color.lightgray))
            }
        }

        inner class PaymentMethodAdapter(itemView: View) : RecyclerView.ViewHolder(itemView) {
            val tv_paymetMethod = itemView.tv_paymetMethod
            val ll_linear = itemView.ll_linear
        }
    }

    @SuppressLint("CheckResult")
    fun openSignatureActivity() {
        rxPermissions!!.request(
                android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .subscribe(object : Consumer<Boolean> {
                    override fun accept(granted: Boolean?) {
                        if (granted!!) { // Always true pre-M
                            // I can control the camera now

                            val siantureIntent = Intent(this@SalesCategoryActivity, SignatureActivity::class.java)
                            startActivityForResult(siantureIntent, 8989)
                        } else {
                            // Oups permission denied
                            Snackbar.make(coordinator_sales, resources.getString(R.string.permissionenable),
                                    Snackbar.LENGTH_LONG)
                                    .setActionTextColor(Color.WHITE)
                                    .setAction(resources.getString(R.string.ok), object : View.OnClickListener {
                                        override fun onClick(p0: View?) {
                                            val intent = Intent()
                                            intent.action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
                                            val uri = Uri.fromParts("package", this@SalesCategoryActivity.packageName, null)
                                            intent.data = uri
                                            startActivity(intent)
                                        }
                                    }).show()
                        }
                    }
                })


    }

    fun callEmployeeProductQty(productData: GetCartData) {
        if (CUC.isNetworkAvailablewithPopup(this@SalesCategoryActivity)) {
            mDialog = CUC.createDialog(this@SalesCategoryActivity)
            val apiService = RequetsInterface.create()
            val parameterMap = HashMap<String, String>()
            parameterMap["api_key"] = "${Preferences.getPreference(this@SalesCategoryActivity, "API_KEY")}"
            if (logindata != null) {
                parameterMap["store_id"] = logindata.user_store_id
            }
            parameterMap["cart_detail_id"] = productData.cart_detail_id
            parameterMap["product_id"] = productData.product_mapping.product_id
            Log.i("Result", "parameterMap : $parameterMap")
            CompositeDisposable().add(apiService.callEmployeeProductQty(parameterMap)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe({ result ->
                        if (mDialog != null && mDialog.isShowing)
                            mDialog.dismiss()
                        if (result != null) {
                            Log.i("Result", "" + result.toString())
                            if (result.status == "0") {
                                if (result.inventory_qty != "")
                                    productData.product_mapping.p_map_qty = result.inventory_qty

//                                if (productData.p_map_qty == "" || productData.p_map_qty == null || productData.p_map_qty == "0") {
//                                    tv_tottal_qty.text = "Unavailable"
//                                    tv_tottal_qty.setTextColor(ContextCompat.getColor(this@SalesCategoryActivity, R.color.red))
//                                    ll_add_to_cart.visibility = View.GONE
//                                } else {
//                                    tv_tottal_qty.text = "${productData.p_map_qty} ${productData.unit_short_name}"
//                                }

                                DisplayProductQtyDialog(productData)
                                CUC.displayToast(this@SalesCategoryActivity, result.show_status, result.message)
                            } else if (result.status == "10") {
                                //callApiKey()
                            } else {
                                CUC.displayToast(this@SalesCategoryActivity, result.show_status, result.message)
                            }

                        } else {
                            //   CUC.displayToast(this@SalesCategoryActivity, "0", getString(R.string.connection_to_database_failed))
                        }

                    }, { error ->
                        if (mDialog != null && mDialog.isShowing)
                            mDialog.dismiss()
                        CUC.displayToast(this@SalesCategoryActivity, "0", getString(R.string.connection_to_database_failed))
                        error.printStackTrace()
                    })
            )
        }
    }

    private var mAboutDataListener: OnAboutDataReceivedListener? = null

    interface OnAboutDataReceivedListener {
        fun onDataReceived(result: String)
    }

    fun setAboutDataListener(listener: OnAboutDataReceivedListener) {
        this.mAboutDataListener = listener
    }


    fun setChartData() {

        if (pieGraphLimite >= 0f && noOfAddedItem >= 0f) {

            textLimitUsage.text = String.format("%.2f G", pieGraphLimite)

            tv_limit_value.text = "${noOfAddedItem}G/"

            val usesPersntage = ((noOfAddedItem * 100) / pieGraphLimite)
            val data: FloatArray = FloatArray(2)
            data[0] = usesPersntage
            data[1] = (100 - usesPersntage)
            chart.setData(data)
        }

    }
}