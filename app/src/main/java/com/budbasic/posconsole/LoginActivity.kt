package com.budbasic.posconsole

import android.Manifest
import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.Settings
import android.support.annotation.RequiresApi
import android.support.v4.app.ActivityCompat
import android.support.v4.app.ActivityCompat.checkSelfPermission
import android.support.v4.widget.CircularProgressDrawable
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.view.inputmethod.EditorInfo
import com.budbasic.customer.global.Preferences
import com.budbasic.global.CUC
import com.budbasic.posconsole.model.APIClass
import com.budbasic.posconsole.reception.StoreManagerActivity
import com.budbasic.posconsole.webservice.RequetsInterface
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DecodeFormat
import com.bumptech.glide.request.RequestOptions
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.GoogleApiAvailability
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.common.api.ResultCallback
import com.google.android.gms.location.*
import com.google.gson.Gson
import com.kotlindemo.model.GetEmployeeAppSetting
import com.kotlindemo.model.GetEmployeeLogin
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.login_screen.*
import java.text.DecimalFormat

class LoginActivity : AppCompatActivity(), GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        com.google.android.gms.location.LocationListener,
        ResultCallback<LocationSettingsResult>, APIClass.onCallListner {
    override fun onFailed(requestCode: Int) {
    }

    override fun onSuccess(requestCode: Int) {
        println("aaaaaaaaaa   login  "+requestCode)
        if (requestCode == 2) {
            callEmployeeLogin()
        } else if (requestCode == 3) {
            callEmployeeLocationUpdate()
        }
    }

    /**
     * Provides the entry point to Google Play services.
     */
    lateinit var mGoogleApiClient: GoogleApiClient
    /**
     * Stores parameters for requests to the FusedLocationProviderApi.
     */
    lateinit var mLocationRequest: LocationRequest
    /**
     * Stores the types of location services the client is interested in using. Used for checking
     * settings to determine if the device has optimal location settings.
     */
    lateinit var mLocationSettingsRequest: LocationSettingsRequest
    /**
     * Represents a geographical location.
     */
    /**
     * Tracks the status of the location updates request. Value changes when the user presses the
     * Start Updates and Stop Updates buttons.
     */
    var mRequestingLocationUpdates: Boolean = false
    /**
     * The desired interval for location updates. Inexact. Updates may be more or less frequent.
     */
    val UPDATE_INTERVAL_IN_MILLISECONDS: Long = 10000
    /**
     * The fastest rate for active location updates. Exact. Updates will never be more frequent
     * than this value.
     */
    val FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS = UPDATE_INTERVAL_IN_MILLISECONDS / 2
    //for overlay
    val REQUEST_CODE=70
    //Any random number you can take
    val REQUEST_PERMISSION_LOCATION = 10
    /**
     * Constant used in the location settings dialog.
     */
    protected val REQUEST_CHECK_SETTINGS = 0x1

    internal var RQS_GooglePlayServices = 0
    var mCurrentLocation: Location? = null

    internal var current_latitude = 0.0
    internal var current_longitude = 0.0

    lateinit var mDialog: Dialog
    lateinit var appSettingData: GetEmployeeAppSetting
    lateinit var logindata: GetEmployeeLogin
    lateinit var apiClass: APIClass

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN)
        setContentView(R.layout.login_screen)
        appSettingData = Gson().fromJson(Preferences.getPreference(this@LoginActivity, "AppSetting"), GetEmployeeAppSetting::class.java)

        //step 1
        buildGoogleApiClient()

        //step 2
        createLocationRequest()

        //step 3
        buildLocationSettingsRequest()

        init()
    }

    override fun onStart() {
        super.onStart()
        apiClass = APIClass(this@LoginActivity, this)
        val googleAPI = GoogleApiAvailability.getInstance()
        val resultCode = googleAPI.isGooglePlayServicesAvailable(this)
        if (resultCode == ConnectionResult.SUCCESS) {
            mGoogleApiClient.connect()
        } else {
            googleAPI.getErrorDialog(this, resultCode, RQS_GooglePlayServices)
        }
    }

    override fun onPause() {
        super.onPause()
        if (mGoogleApiClient.isConnected) {
            stopLocationUpdates()
        }
    }

    override fun onResume() {
        super.onResume()
        if (mGoogleApiClient.isConnected && mRequestingLocationUpdates) {
            //  Toast.makeText(FusedLocationWithSettingsDialog.this, "location was already on so detecting location now", Toast.LENGTH_SHORT).show();
            startLocationUpdates()
        }
    }

    /**
     * Requests location updates from the FusedLocationApi.
     */
    protected fun startLocationUpdates() {
        if (checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION), REQUEST_PERMISSION_LOCATION)
        } else {
            goAndDetectLocation()
        }

    }
    protected  fun checkoverlay()
    {
        if (checkSelfPermission(this, Manifest.permission.SYSTEM_ALERT_WINDOW) != PackageManager.PERMISSION_GRANTED) {
         //  ActivityCompat.requestPermissions(this, android.Manifest.permission.SYSTEM_ALERT_WINDOW, REQUEST_PERMISSION_LOCATION)
        } else {
            goAndDetectLocation()
        }
    }
//add for overlap permission


    fun goAndDetectLocation()
    {
        if (checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return
        }
        LocationServices.FusedLocationApi.requestLocationUpdates(
                mGoogleApiClient,
                mLocationRequest,
                this
        ).setResultCallback {
            mRequestingLocationUpdates = true
            //     setButtonsEnabledState();
        }
    }

    override fun onStop() {
        super.onStop()
        mGoogleApiClient.disconnect()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            // Check for the integer request code originally supplied to startResolutionForResult().
            REQUEST_CHECK_SETTINGS -> when (resultCode) {
                Activity.RESULT_OK -> {
                    Log.i("OrderCancel", "User agreed to make required location settings changes.")
                    startLocationUpdates()
                }
                Activity.RESULT_CANCELED -> Log.i("OrderCancel", "User chose not to make required location settings changes.")
            }
            REQUEST_CODE -> when (resultCode) {
                //startService(Intent(this@LoginActivity,
                //   CheckServicesForApps::class.java));


            }
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            REQUEST_PERMISSION_LOCATION -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    goAndDetectLocation()
                }
            }
        }
    }

    /**
     * Removes location updates from the FusedLocationApi.
     */
    protected fun stopLocationUpdates() {
        // It is a good practice to remove location requests when the activity is in a paused or
        // stopped state. Doing so helps battery performance and is especially
        // recommended in applications that request frequent location updates.
        LocationServices.FusedLocationApi.removeLocationUpdates(
                mGoogleApiClient,
                this
        ).setResultCallback {
            mRequestingLocationUpdates = false
            //   setButtonsEnabledState();
        }
    }


    //step 1
    @Synchronized
    protected fun buildGoogleApiClient() {
        Log.i("OrderCancel", "Building GoogleApiClient")
        mGoogleApiClient = GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build()
    }

    //step 2
    protected fun createLocationRequest() {
        mLocationRequest = LocationRequest()

        // Sets the desired interval for active location updates. This interval is
        // inexact. You may not receive updates at all if no location sources are available, or
        // you may receive them slower than requested. You may also receive updates faster than
        // requested if other applications are requesting location at a faster interval.
        mLocationRequest.interval = UPDATE_INTERVAL_IN_MILLISECONDS

        // Sets the fastest rate for active location updates. This interval is exact, and your
        // application will never receive updates faster than this value.
        mLocationRequest.fastestInterval = FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS

        mLocationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
    }

    //step 3
    protected fun buildLocationSettingsRequest() {
        val builder = LocationSettingsRequest.Builder()
        builder.addLocationRequest(mLocationRequest)
        builder.setAlwaysShow(true)
        mLocationSettingsRequest = builder.build()
    }

    //step 4
    protected fun checkLocationSettings() {
        val result = LocationServices.SettingsApi.checkLocationSettings(
                mGoogleApiClient,
                mLocationSettingsRequest
        )


        callEmployeeLogin()
        println("aaaaaaaaaa   login  locationsettings ")
        callEmployeeLogin()
        result.setResultCallback(this)
    }
    @RequiresApi(api = Build.VERSION_CODES.M)
    fun checkDrawOverlayPermission()
    {
        if (Settings.canDrawOverlays(this)) {
   Log.v("App", "Requesting Permission" + Settings.canDrawOverlays(this));
   // if not construct intent to request permission
   val   intent = Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
    Uri.parse("package:" + getApplicationContext().getPackageName()));
   // request permission via start activity for result
   startActivityForResult(intent, REQUEST_CODE); //It will call onActivityResult Function After you press Yes/No and go Back after giving permission
  } else {
   Log.v("App", "We already have permission for it.");
   // disablePullNotificationTouch();
   // Do your stuff, we got permission captain
  }

    }
    fun init() {
        //add overlay

        edt_username.setImeOptions(EditorInfo.IME_FLAG_NO_EXTRACT_UI);
        edt_password.setImeOptions(EditorInfo.IME_FLAG_NO_EXTRACT_UI);

//        edt_username.setText("storedetroit@mailinator.com")
//        edt_password.setText("123")

        if (appSettingData.company_image != null && appSettingData.company_image != "")
        {
            iv_loginstorelogo.visibility = View.VISIBLE
            val circularProgressDrawable = CircularProgressDrawable(this@LoginActivity)
            circularProgressDrawable.strokeWidth = 5f
            circularProgressDrawable.centerRadius = 30f
            circularProgressDrawable.start()
//            Glide.with(this@LoginActivity)
//                    .applyDefaultRequestOptions(RequestOptions().format(DecodeFormat.PREFER_ARGB_8888).error(R.drawable.no_image_available).placeholder(circularProgressDrawable))
//                    .load("${appSettingData.company_image}")
//                    .into(iv_loginstorelogo)
        }

        cv_login.setOnClickListener {
            if (edt_username.text.toString().trim() == "") {
                CUC.displayToast(this@LoginActivity, "0", "Enter your username")
            }else if (edt_password.text.toString().trim() == "") {
                CUC.displayToast(this@LoginActivity, "0", "Enter your password")
            } else {
                println("aaaaaaaaaa   login  btn click ")
                checkLocationSettings()
            }
        }

        tv_login_forgot_paasord.setOnClickListener {
            val goIntent = Intent(this@LoginActivity, ForgotPasswordActivity::class.java)
            startActivity(goIntent)
            //CommonUtils.displayToast(this@LoginActivity, "0", "work in progress on this module")
        }
    }

    companion object {
        fun logout(mContext: Context) {
            val mIntent = Intent(mContext, LoginActivity::class.java)
            mIntent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TOP
            mContext.startActivity(mIntent)
        }
    }

    fun callEmployeeLogin() {
        println("aaaaaaaaaa   login  ")
        mDialog = CUC.createDialog(this@LoginActivity)
        val apiService = RequetsInterface.create()
        val parameterMap = HashMap<String, String>()
        parameterMap["api_key"] = "${Preferences.getPreference(this@LoginActivity, "API_KEY")}"
        parameterMap["employee_username"] = "${edt_username.text}"
        parameterMap["employee_password"] = "${edt_password.text}"
        parameterMap["terminal_id"] = "${appSettingData.terminal_id}"
        parameterMap["device_id"] = "${appSettingData.terminal_device_id}"
        println("aaaaaaaaaa   login parameters "+parameterMap)
        Log.i("Result", "parameterMap : $parameterMap")
        CompositeDisposable().add(apiService.callGetEmployeeLogin(parameterMap)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe({ result ->
                    if (mDialog != null && mDialog.isShowing)
                        mDialog.dismiss()
                    if (result != null) {
                        println("aaaaaaaaaa   login result "+result)
                        Log.i("Result", "My Login : " + result.toString())
                        if (result.status == "0") {
                            Preferences.setPreference(this@LoginActivity, "logindata", Gson().toJson(result))
                            logindata = Gson().fromJson(Preferences.getPreference(this@LoginActivity, "logindata"), GetEmployeeLogin::class.java)
                            if (current_latitude != 0.0 || current_longitude != 0.0) {
                                callEmployeeLocationUpdate()
                            } else {
                                //call here
                            }
                           // Log.d("SASASA"," = "+result.message)
                          //  CUC.displayToast(this@LoginActivity, result.show_status, result.message)
                        } else if (result.status == "10") {
                            apiClass.callApiKey(2)
                        } else {
                            CUC.displayToast(this@LoginActivity, result.show_status, result.message)
                        }
                    } else {
                      //  CUC.displayToast(this@LoginActivity, "0", getString(R.string.connection_to_database_failed))
                    }
                }, { error ->
                    if (mDialog != null && mDialog.isShowing)
                        mDialog.dismiss()
                    CUC.displayToast(this@LoginActivity, "0", getString(R.string.connection_to_database_failed))

                    error.printStackTrace()
                })
        )
    }

    fun callEmployeeLocationUpdate() {
        mDialog = CUC.createDialog(this@LoginActivity)
        val apiService = RequetsInterface.create()
        val parameterMap = HashMap<String, String>()
        parameterMap["api_key"] = "${Preferences.getPreference(this@LoginActivity, "API_KEY")}"
        parameterMap["employee_lat"] = "${Preferences.getPreference(this@LoginActivity, "latitude")}"
        parameterMap["employee_log"] = "${Preferences.getPreference(this@LoginActivity, "longitude")}"
        parameterMap["employee_id"] = "${logindata.user_id}"
        Log.i("Result", "parameterMap : $parameterMap")
        CompositeDisposable().add(apiService.callGetEmployeeLocationUpdate(parameterMap)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe({ result ->
                    if (mDialog != null && mDialog.isShowing)
                        mDialog.dismiss()
                    if (result != null) {
                        Log.i("Result", "" + result.toString())
                        if (result.status == "0") {
                            val goIntent = Intent(this, StoreManagerActivity::class.java)
                            goIntent.putExtra("to", "login")
                            startActivity(goIntent)
                            finish()
                            CUC.displayToast(this@LoginActivity, result.show_status, result.message)
                        } else if (result.status == "10") {
                            apiClass.callApiKey(3)
                        } else {
                            CUC.displayToast(this@LoginActivity, result.show_status, result.message)
                        }
                    } else {
                      //  CUC.displayToast(this@LoginActivity, "0", getString(R.string.connection_to_database_failed))
                    }
                }, { error ->
                    if (mDialog != null && mDialog.isShowing)
                        mDialog.dismiss()
                    CUC.displayToast(this@LoginActivity, "0", getString(R.string.connection_to_database_failed))

                    error.printStackTrace()
                })
        )
    }

    override fun onConnected(p0: Bundle?) {
        if (mCurrentLocation == null) {
            if (checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return
            }
            mCurrentLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient)
            //mLastUpdateTime = DateFormat.getTimeInstance().format(Date())
            //updateLocationUI()
        }
    }

    override fun onConnectionSuspended(p0: Int) {
        Log.i("OrderCancel", "Connection suspended")
    }

    override fun onConnectionFailed(p0: ConnectionResult) {
        Log.i("OrderCancel", "Connection failed: ConnectionResult.getErrorCode() = " + p0.errorCode)
    }

    fun updateLocationUI() {
        if (mCurrentLocation!!.latitude != 0.0 && mCurrentLocation!!.longitude != 0.0) {
            current_latitude = DecimalFormat("##.#####").format(mCurrentLocation!!.latitude).toDouble()
            current_longitude = DecimalFormat("##.#####").format(mCurrentLocation!!.longitude).toDouble()
            Preferences.setPreference(this@LoginActivity, "latitude", "$current_latitude")
            Preferences.setPreference(this@LoginActivity, "longitude", "$current_longitude")
            Log.i("OrderCancel", "latitude : $current_latitude , longitude : $current_longitude")
            stopLocationUpdates()
            callEmployeeLogin()
        }
    }

    override fun onLocationChanged(p0: Location?) {
        Log.i("OrderCancel", "onLocationChanged()")
        mCurrentLocation = p0
        //mLastUpdateTime = DateFormat.getTimeInstance().format(Date())
        updateLocationUI()
    }

    override fun onResult(p0: LocationSettingsResult) {
        val status = p0.status
        when (status.statusCode) {
            LocationSettingsStatusCodes.SUCCESS -> {
                Log.i("OrderCancel", "All location settings are satisfied.")
                //Toast.makeText(this@LoginActivity, "Location is already on.", Toast.LENGTH_SHORT).show()
                startLocationUpdates()
            }
            LocationSettingsStatusCodes.RESOLUTION_REQUIRED -> {
                Log.i("OrderCancel", "Location settings are not satisfied. Show the user a dialog to upgrade location settings")
                try {
                    // Show the dialog by calling startResolutionForResult(), and check the result
                    // in onActivityResult().
                    CUC.displayToast(this@LoginActivity, "0", "Location dialog will be open")
                    //Toast.makeText(this@LoginActivity, "Location dialog will be open", Toast.LENGTH_SHORT).show()
                    //move to step 6 in onActivityResult to check what action user has taken on settings dialog
                    status.startResolutionForResult(this@LoginActivity, REQUEST_CHECK_SETTINGS)
                } catch (e: Exception) {
                    Log.i("OrderCancel", "PendingIntent unable to execute request.")
                }
            }
            LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE -> Log.i("OrderCancel", "Location settings are inadequate, and cannot be fixed here. Dialog " + "not created.")
        }
    }
}
