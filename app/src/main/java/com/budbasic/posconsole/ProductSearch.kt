package com.budbasic.posconsole

import android.annotation.SuppressLint
import android.app.Activity
import android.app.Dialog
import android.bluetooth.BluetoothAdapter
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Message
import android.os.StrictMode
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.SearchView
import android.util.Log
import android.view.*
import android.widget.*
import com.budbasic.customer.global.Preferences
import com.budbasic.global.CUC
import com.budbasic.posconsole.printer.sdk.BluetoothService
import com.budbasic.posconsole.printer.sdk.Command
import com.budbasic.posconsole.printer.sdk.PrinterCommand
import com.budbasic.posconsole.webservice.RequetsInterface
import com.google.gson.Gson
import com.google.zxing.BarcodeFormat
import com.google.zxing.WriterException
import com.kotlindemo.model.GetEmployeeAppSetting
import com.kotlindemo.model.GetEmployeeLogin
import com.kotlindemo.model.GetProducts
import com.mikepenz.iconics.utils.Utils
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.store_search.*
import java.io.ByteArrayOutputStream
import java.util.*

/**
 * Created by Dev009 on 8/2/2017 2.
 */

class ProductSearch : AppCompatActivity() {

    // Local Bluetooth adapter
    private var mBluetoothAdapter: BluetoothAdapter? = null
    // Member object for the services
    var mService: BluetoothService? = null

    // Intent request codes
    private val REQUEST_CONNECT_DEVICE = 1
    private val REQUEST_ENABLE_BT = 2

    // Message types sent from the BluetoothService Handler
    val MESSAGE_STATE_CHANGE = 1
    val MESSAGE_READ = 2
    val MESSAGE_WRITE = 3
    val MESSAGE_DEVICE_NAME = 4
    val MESSAGE_TOAST = 5
    val MESSAGE_CONNECTION_LOST = 6
    val MESSAGE_UNABLE_CONNECT = 7
    var printerstatuspsearch:String?=null
    var printertypepsearch:String?=null
    //add for printer manufacture check 8/12/2018
    var connecteddeviceaddress:String?=null
    var pname:String?=null
    var ptag:String?=null
    private val TAG = "Main_Activity"
    private val DEBUG = true

    // Key names received from the BluetoothService Handler
    val DEVICE_NAME = "device_name"
    val TOAST = "toast"
    // Name of the connected device
    private var mConnectedDeviceName: String? = null

    lateinit var mDialog: Dialog
    lateinit var logindata: GetEmployeeLogin
    lateinit var mAppBasic: GetEmployeeAppSetting

    internal var mSearchProductName = ""


    lateinit var adapter: ReceiptTaxItemAdapter

    companion object {
        var searchdataArrayList = ArrayList<GetProducts>()
    }

    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.store_search)
        mAppBasic = Gson().fromJson(Preferences.getPreference(this@ProductSearch, "AppSetting"), GetEmployeeAppSetting::class.java)
        logindata = Gson().fromJson(Preferences.getPreference(this@ProductSearch, "logindata"), GetEmployeeLogin::class.java)
        init()
    }

    fun init() {
        // Get local Bluetooth adapter
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter()
        // If the adapter is null, then Bluetooth is not supported
        if (mBluetoothAdapter == null) {
            Toast.makeText(this, "Bluetooth is not available",
                    Toast.LENGTH_LONG).show()
            //finish()
        }

        val builder = StrictMode.VmPolicy.Builder()
        StrictMode.setVmPolicy(builder.build())
        //setSupportActionBar(toolbar_searchproduct);
        iv_back.setOnClickListener(View.OnClickListener {
            searchdataArrayList.clear()
            adapter!!.notifyDataSetChanged()
            setResult(Activity.RESULT_OK)
            finish()
        })
        setupRecylcer()
        customizeSearchView()
    }

    override fun onStop() {
        super.onStop()
        if (mService != null)
            mService!!.stop()
    }

    override fun onResume() {
        super.onResume()
        if (mService != null) {
            if (mService?.state == BluetoothService.STATE_NONE) {
                // Start the Bluetooth services
                mService?.start()
            }
        }
    }

    override fun onStart() {
        super.onStart()
        // If Bluetooth is not on, request that it be enabled.
        // setupChat() will then be called during onActivityResult
        if (!mBluetoothAdapter!!.isEnabled) run {
            val enableIntent = Intent(
                    BluetoothAdapter.ACTION_REQUEST_ENABLE)
            startActivityForResult(enableIntent, REQUEST_ENABLE_BT)
            // Otherwise, setup the session
        } else {
            if (mService == null) {
                mService = BluetoothService(this, mHandler)

                mConnectedDeviceName = "";
                //val address = Preferences.getPreference(this, DEVICE_NAME)
                val address = "00:12:F3:3A:15:3B"
                Log.i("AddQueueActivity", "address : " + address)

                if (address != null && !address.equals("")) {
                    val mDevice = mBluetoothAdapter!!.getRemoteDevice(address)
                    //BluetoothDevice mDevice= (BluetoothDevice)address;
                    connecteddeviceaddress= mDevice.name+ " "+mDevice.address
                    Log.i("AddQueueActivity", "mDevice : " + mDevice.getAddress());
                    Log.i("AddQueueActivity", "mDevice : " + mDevice.getName());
                    Log.i("AddQueueActivity", "mDevice : " + mDevice.getUuids());
                    mService!!.connect(mDevice)
                }
            }
        }

        callEmployeeScanProducts("")
    }

    private fun setupRecylcer() {
        recycle_searchproduct!!.layoutManager = LinearLayoutManager(this@ProductSearch)
        recycle_searchproduct.itemAnimator = DefaultItemAnimator()
        adapter = ReceiptTaxItemAdapter(object : onClickListener {
            override fun callClick(data: GetProducts) {
                barCodePrintDialog(data)
            }

        })
        recycle_searchproduct.adapter = adapter
    }

    lateinit var dialogProductQty: Dialog
    fun barCodePrintDialog(data: GetProducts) {
        dialogProductQty = Dialog(this@ProductSearch, R.style.TransparentProgressDialog)
        dialogProductQty.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialogProductQty.setContentView(R.layout.barcode_dialog)
        dialogProductQty.window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)
        dialogProductQty.show()
        if (::dialogProductQty.isInitialized) {
            val tvproductname = dialogProductQty.findViewById<TextView>(R.id.tvproductname)
            val ivbarcode = dialogProductQty.findViewById<ImageView>(R.id.ivbarcode)
            val edtbarcodeqty = dialogProductQty.findViewById<EditText>(R.id.edtbarcodeqty)
            val ll_sentprint = dialogProductQty.findViewById<TextView>(R.id.tv_sentprint)
            val tv_calcel=dialogProductQty.findViewById<TextView>(R.id.tv_calcel)
            tvproductname.text = "${data.packet_name}"

            try {
                val bitmap = CUC.encodeAsBitmap("${data.product_tag}", BarcodeFormat.CODE_128, 600, 300)
                ivbarcode.setImageBitmap(bitmap)
            } catch (e: WriterException) {
                e.printStackTrace()
            }
            ll_sentprint.setOnClickListener {
                if (edtbarcodeqty == null || edtbarcodeqty.text.toString() == "") {
                    println("aaaaaaaaaaaa   snippet  ")
                } else {
                    val count = "${edtbarcodeqty.text}".toInt()
                    synchronized(this) {

                        for (i in 0 until count) {
                           // print("${data.product_name}", "${data.product_tag}")
                            pname="${data.packet_name}"
                            ptag="${data.packet_barcode}"
                            println("aaaaaaaaaaaa    pname  "+pname)
                            Toast.makeText(this@ProductSearch, "pname "+pname, Toast.LENGTH_LONG).show()
                            printbarcode()

                        }
                    }
                    if (dialogProductQty != null && dialogProductQty.isShowing)
                        dialogProductQty.dismiss()
                }
            }
            tv_calcel.setOnClickListener {
                dialogProductQty.dismiss()
            }
        }
    }

    fun callEmployeeScanProducts(name: String)
    {

        if (CUC.isNetworkAvailablewithPopup(this@ProductSearch)) {
            mDialog = CUC.createDialog(this@ProductSearch)
            val apiService = RequetsInterface.Factory.create()
            val parameterMap = HashMap<String, String>()
            parameterMap["api_key"] = "${Preferences.getPreference(this@ProductSearch, "API_KEY")}"
            //if (::logindata.isInitialized)
            if (logindata != null) {
                parameterMap["store_id"] = logindata.user_store_id
                parameterMap["employee_id"] = logindata.user_id
            }
            parameterMap["product"] = "$name"

            Log.i("aaaaa Result", "parameterMap : $parameterMap")
            CompositeDisposable().add(apiService.callEmployeeSearchProducts(parameterMap)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe({ result ->
                        if (mDialog != null && mDialog.isShowing)
                            mDialog.dismiss()
                        if (result != null) {
                            println("aaaaaaaaa  product  "+result.toString())
                            Log.i("Result", "" + result.toString())
                            if (result.status == "0") {
                                if (result.products != null && result.products.size > 0) {
                                    searchdataArrayList.clear()
                                    searchdataArrayList.addAll(result.products)
                                    adapter!!.notifyDataSetChanged()
                                    tv_nodata.visibility = View.GONE
                                    recycle_searchproduct.visibility = View.VISIBLE
                                    simpleProgressBar!!.visibility = View.INVISIBLE
                                } else {
                                    searchdataArrayList.clear()
                                    adapter!!.notifyDataSetChanged()
                                    tv_nodata.visibility = View.VISIBLE
                                    recycle_searchproduct.visibility = View.GONE
                                    simpleProgressBar!!.visibility = View.INVISIBLE
                                }
                                CUC.displayToast(this@ProductSearch, result.show_status, result.message)
                            } else if (result.status == "10") {
                                //callApiKey()
                            } else {
                                CUC.displayToast(this@ProductSearch, result.show_status, result.message)
                            }

                        } else {
                          //  CUC.displayToast(this@ProductSearch, "0", getString(R.string.connection_to_database_failed))
                        }

                    }, { error ->
                        if (mDialog != null && mDialog.isShowing)
                            mDialog.dismiss()
                        CUC.displayToast(this@ProductSearch, "0", getString(R.string.connection_to_database_failed))
                        error.printStackTrace()
                    })
            )
        }
    }

    private fun customizeSearchView() {
        search_product.setFocusable(true)
        search_product.setIconified(false)
        search_product.setOnSearchClickListener(View.OnClickListener {
            //iv_back.setVisibility(View.GONE);
        })
       /* val searchButton = search_product.findViewById(android.support.v7.appcompat.R.id.search_button) as ImageView
        searchButton.setColorFilter(Color.parseColor("#FFFFFF"), PorterDuff.Mode.SRC_ATOP)

        val searchBox = search_product.findViewById(android.support.v7.appcompat.R.id.search_src_text) as EditText
        searchBox.setBackgroundResource(R.drawable.null_selector)*/
        val searchBox = search_product.findViewById(android.support.v7.appcompat.R.id.search_src_text) as EditText
        // searchBox.setBackgroundResource(R.drawable.null_selector)
        searchBox.setTextColor(resources.getColor(R.color.border_gray))
        searchBox.setHintTextColor(resources.getColor(R.color.graylight))
        searchBox.hint = "Search"
        /*   val searchClose= search_product.findViewById<ImageView>(android.support.v7.appcompat.R.id.search_close_btn)
           // searchClose.setImageResource(R.drawable.ic_)
           searchClose.setImageBitmap(CUC.IconicsBitmap(this, FontAwesome.Icon.faw_times, ContextCompat.getColor(this, R.color.graylight)))

   */

        //  val img_close=mView!!.findViewById<ImageView>(R.id.img_close)
        //  img_close.setImageBitmap(CUC.IconicsBitmap(activity, FontAwesome.Icon.faw_times, ContextCompat.getColor(activity, R.color.graylight)))
        img_close.setOnClickListener {
            finish()
        }

        //View v = search_product.findViewById(android.support.v7.appcompat.R.id.search_plate);
        //v.setBackgroundColor(ContextCompat.getColor(CountryCitySearch.this, R.color.search));

        search_product.setOnCloseListener(SearchView.OnCloseListener {
            searchdataArrayList.clear()
            adapter!!.notifyDataSetChanged()
            setResult(Activity.RESULT_OK)
            finish()
            false
        })

        search_product.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String): Boolean {
                return false
            }

            override fun onQueryTextChange(newText: String): Boolean {

                if (adapter != null) {
                    if (!newText.isEmpty()) {
                        if (newText.length > 2) {
                            mSearchProductName = newText
                            callEmployeeScanProducts(mSearchProductName)
                        }

                    } else {
                        mSearchProductName = newText
                        searchdataArrayList.clear()
                        adapter!!.notifyDataSetChanged()
                        //mProductAdapter.notifyDataSetChanged();
                        tv_nodata.setVisibility(View.VISIBLE)
                        recycle_searchproduct.setVisibility(View.GONE)
                    }
                } else {
                    if (newText.length > 2) {
                        mSearchProductName = newText
                        callEmployeeScanProducts(mSearchProductName)
                    }
                }
                return false
            }
        })
    }

    class ReceiptTaxItemAdapter(val listner: onClickListener) : RecyclerView.Adapter<ReceiptTaxItemAdapter.ViewHolder>() {

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            holder!!.bindItems(searchdataArrayList[position], listner)
        }

        override fun getItemCount(): Int {
            return searchdataArrayList.size
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            val v = LayoutInflater.from(parent!!.context).inflate(R.layout.product_search_item, parent, false)
            return ViewHolder(v)
        }

        class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
            val tv_text: TextView = itemView.findViewById(R.id.tv_text)
            val tv_texttag: TextView = itemView.findViewById(R.id.tv_texttag)

            fun bindItems(data: GetProducts, listner: onClickListener) {
                tv_text.text = "Product Name : ${data.packet_name}"
                tv_texttag.text = "Product Tag : ${data.packet_barcode}"
                tv_text.setOnClickListener {
                    listner.callClick(data)
                }
            }
        }
    }

    interface onClickListener {
        fun callClick(data: GetProducts)
    }

    val mHandler = @SuppressLint("HandlerLeak")
    object : Handler() {
        override fun handleMessage(msg: Message?) {
            if (msg != null) {
                when (msg.what) {
                    MESSAGE_STATE_CHANGE -> {
                        if (DEBUG)
                            Log.i(TAG, "MESSAGE_STATE_CHANGE: " + msg.arg1)
                        when (msg.arg1) {
                            BluetoothService.STATE_CONNECTED -> {
                            }
                            BluetoothService.STATE_CONNECTING -> {
                                //mTitle.setText(R.string.title_connecting)
                            }
                            BluetoothService.STATE_LISTEN, BluetoothService.STATE_NONE -> {
                                //mTitle.setText(R.string.title_not_connected)
                            }
                        }
                    }
                    MESSAGE_WRITE -> {
                    }
                    MESSAGE_READ -> {
                    }
                    MESSAGE_DEVICE_NAME -> {
                        // save the connected device's name
                        mConnectedDeviceName = msg.data.getString(DEVICE_NAME)
                        Toast.makeText(applicationContext,
                                "Connected to $mConnectedDeviceName",
                                Toast.LENGTH_SHORT).show()
                    }
                    MESSAGE_TOAST -> Toast.makeText(applicationContext,
                            msg.data.getString(TOAST), Toast.LENGTH_SHORT)
                            .show()
                    MESSAGE_CONNECTION_LOST    //蓝牙已断开连接
                    -> {
                        Toast.makeText(applicationContext, "Device connection was lost",
                                Toast.LENGTH_SHORT).show()
                    }
                    MESSAGE_UNABLE_CONNECT     //无法连接设备
                    -> Toast.makeText(applicationContext, "Unable to connect device",
                            Toast.LENGTH_SHORT).show()
                }
            }

        }
    }

    /*
     *SendDataByte
     */
    private fun SendDataByte(data: ByteArray) {
        if (mService!!.state != BluetoothService.STATE_CONNECTED) {
            Toast.makeText(this, R.string.not_connected, Toast.LENGTH_SHORT)
                    .show()
            return
        }
        mService?.write(data)
    }

    fun printbarcode() = if (mBluetoothAdapter != null)
    {
        if (mBluetoothAdapter!!.isEnabled) {
            if (mService != null) {
                if (mService!!.getState() !== BluetoothService.STATE_CONNECTED) {
                    Toast.makeText(this@ProductSearch, getText(R.string.not_connected), Toast.LENGTH_SHORT).show()
                    this@ProductSearch.setResult(Activity.RESULT_OK)
                    finish()
                } else {

                    callEmployeeTerminalPrinter()


                }
            } else {
                println("aaaaaaaaa   bluetooth printer is not available  mService ")
            }
        } else {
            println("aaaaaaaaa   bluetooth printer is not available ")
            Toast.makeText(this@ProductSearch, "Bluetooth printer is not available", Toast.LENGTH_LONG).show()
        }
    } else {
        Toast.makeText(this@ProductSearch, "Bluetooth is not available", Toast.LENGTH_LONG).show()
    }


    fun callEmployeeTerminalPrinter()
    {

        val apiService = RequetsInterface.create()
        val parameterMap = HashMap<String, String>()

        if (logindata != null) {
            parameterMap["api_key"] = "${Preferences.getPreference(this@ProductSearch, "API_KEY")}"
            parameterMap["emp_id"] = logindata!!.user_id
        }

        if (mAppBasic != null)
        {
            //  parameterMap["store_id"]="${}"
            parameterMap["terminal_id"] = "${mAppBasic!!.terminal_id}"
        }

        Log.i("Result", "parameterMapprinternew : $parameterMap")
        CompositeDisposable().add(apiService.callEmployeeTerminalPrinter(parameterMap)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe({ result ->
                    Log.i("Resultprinternew", "" + result.toString())

                    //   printerdata=  Gson().fromJson(Preferences.getPreference(applicationContext, "printerdata"),GetTerminalPrinter::class.java)
                    if(result.show_status=="1"|| result!!.message=="Available terminals")

                    {
                        if (result.get_printers != null && result.get_printers.size > 0)
                        {
                           for(i in result.get_printers.indices)
                           {
                               result.get_printers.get(i)
                              // mDevice.getAddress() add new code 8/12/2018

                               if (result.get_printers.get(i).hardware_manufacturer!!.equals(connecteddeviceaddress))
                               {
                                   if (result.get_printers[i].hardware_status!!.equals("1", false))
                                   {
                                        if(result.get_printers[i].hardware_type_id!!.equals("2",false))
                                                {
                                                    printerstatuspsearch = result.get_printers!![0].hardware_status.toString()
                                                    printertypepsearch = result.get_printers!!.get(0).hardware_type_id.toString()
                                                    println("aaaaaaaaa  printerstatuspsearch0  "+printerstatuspsearch+"   "+printertypepsearch)
                                                    Log.i("printerstatus", printerstatuspsearch)
                                                }
                               }
                               }

                           }
                          //  Toast.makeText(this@ProductSearch,"statutype"+printerstatus+printertype,Toast.LENGTH_LONG).show()

                        }
                        try {
                            SendDataByte(Command.ESC_Init)

                            Log.i("pnamedevice", mConnectedDeviceName)
                            println("aaaaaaaaa  printerstatuspsearch "+printerstatuspsearch   +"   "+printertypepsearch)
                                // Log.i("pname selectin",printername+ "mService"+mService.DEVICE_NAME);
                                if (printerstatuspsearch.equals("1",false) /*&& printertypepsearch.equals("2",false)*/)
                                {
                                    Toast.makeText(this@ProductSearch,"Find Label  Printer Scessfully", Toast.LENGTH_LONG).show()
                                    Log.i("pnamedevice", mConnectedDeviceName)

                                    println("aaaaaaa   ptag  "+ptag.toString())

                                    Command.alignmentCommand[3] = 49
                                    SendDataByte(Command.bold_on)
                                    SendDataByte(Command.alignmentCommand)
                                    SendDataByte(pname!!.toByteArray())

                                    SendDataByte(Command.bold_off)
                                    Command.alignmentCommand[3] = 49
                                    SendDataByte(Command.alignmentCommand)
                                    val barcodeData = ptag!!.toByteArray()
                                    val commands = ArrayList<ByteArray>()
                                    val command = ByteArray(barcodeData.size + 6 + 1)
                                    command[0] = 27.toByte()
                                    command[1] = 98.toByte()
                                    command[2] = 54.toByte()
                                    command[3] = 50 // option
                                    command[4] = 50 //width
                                    command[5] = 120 //height
                                    for (index in barcodeData.indices) {
                                        command[index + 6] = barcodeData[index]
                                    }
                                    command[command.size - 1] = 30.toByte()
                                    commands.add(command)


                                    for (data in commands) {
                                        SendDataByte(data)
                                    }



                                    SendDataByte("\n\n\n".toByteArray())
                                    SendDataByte(Command.LF)
                                    SendDataByte(PrinterCommand.POS_Set_PrtAndFeedPaper(2))
                                    SendDataByte(Command.ESC_d)
                                    SendDataByte(PrinterCommand.POS_Set_PrtInit())

                                } else {
                                    Toast.makeText(this@ProductSearch, "Find Other Label Printer", Toast.LENGTH_LONG).show()
                                }
                                /*
                                * [END BarCode]
                                * */
                            }
                        catch (e: Exception) {
                            e.printStackTrace()
                        }
                    }
                    else
                    {

                    }
                    //   CUC.displayToast(this,"printetres",result.toString())

                }, { error ->
                    error.printStackTrace()
                })
        )

    }

}
