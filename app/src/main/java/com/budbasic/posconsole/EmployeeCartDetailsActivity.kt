package com.budbasic.posconsole

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.TextView
import com.google.gson.Gson
import com.kotlindemo.model.*
import com.budbasic.posconsole.webservice.RequetsInterface
import com.budbasic.customer.global.Preferences
import com.budbasic.global.CUC
import com.budbasic.posconsole.model.APIClass
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.employee_cart_details_layout.*
import java.text.DecimalFormat
import java.util.*
import kotlin.collections.HashMap
import kotlin.collections.set


class EmployeeCartDetailsActivity : AppCompatActivity(), APIClass.onCallListner {
    override fun onFailed(requestCode: Int) {
       // CUC.displayToast(this@EmployeeCartDetailsActivity, "0", getString(R.string.connection_to_database_failed))
    }

    override fun onSuccess(requestCode: Int) {

        if (requestCode == EMPLOYEEPATIENTCART) {
            callEmployeePatientCart()
        } else if (requestCode == EMPLOYEEGENERATEINVOICE) {
            callEmployeeAcceptOrder()
        }
    }

    lateinit var mDialog: Dialog
    val EMPLOYEEPATIENTCART = 5
    val EMPLOYEEGENERATEINVOICE = 6

    lateinit var logindata: GetEmployeeLogin
    lateinit var appSettingData: GetEmployeeAppSetting
    lateinit var dialogPaymentType: Dialog

    var list: ArrayList<GetCartData> = ArrayList()
    lateinit var productAdapter: ProductAdapter

    var taxList: ArrayList<GetTaxData> = ArrayList()
    lateinit var taxItemAdapter: TaxItemAdapter

    var store_id = ""
    var patient_id = ""
    var mainCartId = ""

    var loyalty_amount = "0.00"
    var cart_serve_type = ""

    var Currency_value = ""
    lateinit var apiClass: APIClass
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
        setContentView(R.layout.employee_cart_details_layout)
        apiClass = APIClass(this@EmployeeCartDetailsActivity, this)
        if (intent != null) {
            store_id = intent.getStringExtra("store_id")
            patient_id = intent.getStringExtra("patient_id")
            mainCartId = intent.getStringExtra("cart_id")

        }
        appSettingData = Gson().fromJson(Preferences.getPreference(this@EmployeeCartDetailsActivity, "AppSetting"), GetEmployeeAppSetting::class.java)
        logindata = Gson().fromJson(Preferences.getPreference(this@EmployeeCartDetailsActivity, "logindata"), GetEmployeeLogin::class.java)
        Currency_value = "${appSettingData.Currency_value}"
        init()
    }

    override fun onStop() {
        super.onStop()
    }

    override fun onStart() {
        super.onStart()
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        Log.i("Result", "Main onActivityResult(), $requestCode : $resultCode")

    }

    override fun onResume() {
        super.onResume()
    }

    fun init() {
        tv_generate_bill.visibility = View.VISIBLE
        tv_generate_bill.text = "Accept Order"
        ll_redeem_option.visibility = View.GONE
        iv_back.setOnClickListener {
            finish()
        }
        rv_tax_list!!.layoutManager = LinearLayoutManager(this@EmployeeCartDetailsActivity)
        taxItemAdapter = TaxItemAdapter(taxList, this@EmployeeCartDetailsActivity, Currency_value)
        rv_tax_list!!.adapter = taxItemAdapter

        rv_cart_list!!.layoutManager = LinearLayoutManager(this@EmployeeCartDetailsActivity)
        productAdapter = ProductAdapter(list, this@EmployeeCartDetailsActivity, Currency_value, object : onClickListener {
            override fun deleteClick(data: GetCartData) {
            }

        })
        rv_cart_list!!.adapter = productAdapter


        ll_send_order.setOnClickListener {
        }

        tv_generate_bill.setOnClickListener {
            callEmployeeAcceptOrder()

        }

        callEmployeePatientCart()
    }

    fun callEmployeePatientCart() {
        if (CUC.isNetworkAvailablewithPopup(this@EmployeeCartDetailsActivity)) {
            mDialog = CUC.createDialog(this@EmployeeCartDetailsActivity)
            val apiService = RequetsInterface.create()
            val parameterMap = HashMap<String, String>()
            parameterMap["api_key"] = "${Preferences.getPreference(this@EmployeeCartDetailsActivity, "API_KEY")}"
            parameterMap["store_id"] = "$store_id"
            parameterMap["patient_id"] = "$patient_id"
            parameterMap["cart_id"] = "$mainCartId"

            Log.i("Result", "parameterMap : $parameterMap")
            CompositeDisposable().add(apiService.callEmployeePatientCart(parameterMap)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe({ result ->
                        if (mDialog != null && mDialog.isShowing)
                            mDialog.dismiss()
                        if (result != null) {
                            Log.i("Result", "" + result.toString())
                            if (result.status == "0") {
                                cart_serve_type = "${result.main_cart.cart_serve_type}"
                                if (result.cart_data != null && result.cart_data.size > 0) {
                                    loyalty_amount = result.loyalty_amount
                                    tv_total_qty.text = "${result.total_qty}"
                                    tv_subtotal.text = "$Currency_value${result.subtotal}"
                                    list.clear()
                                    list.addAll(result.cart_data)
                                    productAdapter.notifyDataSetChanged()
                                    taxList.clear()
                                    taxList.addAll(result.tax_data)
                                    try {
                                        if (loyalty_amount != null && loyalty_amount != "") {
                                            if ("$loyalty_amount".toFloat() > 0) {
                                                taxList.add(GetTaxData("989", "Loyalty Amount", loyalty_amount))
                                            }
                                        }
                                    } catch (ee: Exception) {
                                        ee.printStackTrace()
                                    }
                                    if (result.promocode_name != "") {
                                        taxList.add(GetTaxData("1789", "Promo Code (${result.promocode_name})", result.promosales_amount))
                                    }
                                    //taxList.add(GetTaxData("0", "Loyalty Amount", loyalty_amount))
                                    taxList.add(GetTaxData("100", "Grand Total", result.total_payble))
                                    taxItemAdapter.notifyDataSetChanged()
                                } else {
                                    list.clear()
                                    productAdapter.notifyDataSetChanged()
                                    taxList.clear()
                                    taxItemAdapter.notifyDataSetChanged()
                                }
                                Preferences.setPreference(this@EmployeeCartDetailsActivity, "total_qty", result.total_qty)
                                CUC.displayToast(this@EmployeeCartDetailsActivity, result.show_status, result.message)
                            } else if (result.status == "10") {
                                apiClass.callApiKey(EMPLOYEEPATIENTCART)
                            } else {
                                list.clear()
                                productAdapter.notifyDataSetChanged()
                                taxList.clear()
                                taxItemAdapter.notifyDataSetChanged()
                                CUC.displayToast(this@EmployeeCartDetailsActivity, result.show_status, result.message)
                            }

                        } else {
                           // CUC.displayToast(this@EmployeeCartDetailsActivity, "0", getString(R.string.connection_to_database_failed))
                        }

                    }, { error ->
                        if (mDialog != null && mDialog.isShowing)
                            mDialog.dismiss()
                        CUC.displayToast(this@EmployeeCartDetailsActivity, "0", getString(R.string.connection_to_database_failed))
                        error.printStackTrace()
                    })
            )
        }
    }

    fun callEmployeeAcceptOrder() {
        mDialog = CUC.createDialog(this@EmployeeCartDetailsActivity)
        val apiService = RequetsInterface.Factory.create()
        val parameterMap = HashMap<String, String>()
        parameterMap["api_key"] = "${Preferences.getPreference(this@EmployeeCartDetailsActivity, "API_KEY")}"
        parameterMap["cart_id"] = "$mainCartId"
        if (logindata != null) {
            parameterMap["employee_id"] = "${logindata.user_id}"
        }
        parameterMap["terminal_id"] = "${appSettingData.terminal_id}"
        parameterMap["emp_lat"] = "${Preferences.getPreference(this@EmployeeCartDetailsActivity, "latitude")}"
        parameterMap["emp_log"] = "${Preferences.getPreference(this@EmployeeCartDetailsActivity, "longitude")}"
        Log.i("Result", "parameterMap : $parameterMap")
        CompositeDisposable().add(apiService.callGetEmployeeAcceptOrder(parameterMap)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe({ result ->
                    if (mDialog != null && mDialog.isShowing)
                        mDialog.dismiss()
                    if (result != null) {
                        Log.i("Result", "result : $result")
                        if (result.status == "0") {
                            try {
                                if (cart_serve_type == "2") {
                                    if (dialogPaymentType != null && dialogPaymentType.isShowing) {
                                        dialogPaymentType.dismiss()
                                    }
                                }
                            } catch (e: Exception) {
                                e.printStackTrace()
                            }

                            CUC.displayToast(this@EmployeeCartDetailsActivity, result.show_status, result.message)
                            setResult(Activity.RESULT_OK)
                            finish()
                        } else if (result.status == "10") {
                            apiClass.callApiKey(EMPLOYEEGENERATEINVOICE)
                        } else {
                            CUC.displayToast(this@EmployeeCartDetailsActivity, result.show_status, result.message)
                        }
                    } else {
                    //    CUC.displayToast(this@EmployeeCartDetailsActivity, "0", getString(R.string.connection_to_database_failed))
                    }
                }, { error ->
                    if (mDialog != null && mDialog.isShowing)
                        mDialog.dismiss()
                    CUC.displayToast(this@EmployeeCartDetailsActivity, "0", getString(R.string.connection_to_database_failed))
                    error.printStackTrace()
                })
        )
    }

    class ProductAdapter(val list: ArrayList<GetCartData>, val context: Context, val Currency_value: String, val onClick: onClickListener) : RecyclerView.Adapter<ProductAdapter.ViewHolder>() {

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            holder!!.bindItems(list[position], context, Currency_value, onClick)
        }

        override fun getItemCount(): Int {
            return list.size
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            val v = LayoutInflater.from(parent!!.context).inflate(R.layout.employee_cart_item, parent, false)
            return ViewHolder(v)
        }

        class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
            val tv_name: TextView = itemView.findViewById(R.id.tv_name)
            val tv_mobilenumber: TextView = itemView.findViewById(R.id.tv_mobilenumber)
            val tv_phonenumber: TextView = itemView.findViewById(R.id.tv_phonenumber)

            fun bindItems(data: GetCartData, context: Context, Currency_value: String, onClick: onClickListener) {
                tv_name.text = "${data.product_name}"
                tv_mobilenumber.text = "${data.cart_detail_qty} ${data.product_mapping.unit_short_name}"
                tv_phonenumber.text = "$Currency_value${data.cart_detail_total_price}"
                //tv_patienttype.text = "${data.type_name}"
            }
        }
    }

    interface onClickListener {
        fun deleteClick(data: GetCartData)
    }

    class TaxItemAdapter(val list: ArrayList<GetTaxData>, val context: Context, val Currency_value: String) : RecyclerView.Adapter<TaxItemAdapter.ViewHolder>() {

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            holder!!.bindItems(list[position], context, Currency_value)
        }

        override fun getItemCount(): Int {
            return list.size
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            val v = LayoutInflater.from(parent!!.context).inflate(R.layout.tax_data_item, parent, false)
            return ViewHolder(v)
        }

        class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
            val tv_tax_title: TextView = itemView.findViewById(R.id.tv_tax_title)
            val tv_tax_amount: TextView = itemView.findViewById(R.id.tv_tax_amount)

            fun bindItems(data: GetTaxData, context: Context, Currency_value: String) {
                tv_tax_title.text = "${data.tax_name}"
                if ("${data.tax_name}" == "Loyalty Amount") {
                    tv_tax_amount.text = "-$Currency_value${data.tax_total}"
                } else {
                    tv_tax_amount.text = "$Currency_value${data.tax_total}"
                }

            }
        }
    }
}