package com.budbasic.posconsole.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.budbasic.posconsole.R;
import com.budbasic.posconsole.reception.DailyReportActivity;
import com.budbasic.posconsole.reception.SettingsFragment;
import com.google.gson.Gson;
import com.kotlindemo.model.GetDailyReport;
import com.kotlindemo.model.GetSettingList;

import java.util.ArrayList;

public class SettingAdapter extends RecyclerView.Adapter<SettingAdapter.MyViewHolder>  {

    private Context context;
    ArrayList<GetSettingList> settinglist;
    SettingsFragment settingsFragment;

    public SettingAdapter(Context context, ArrayList<GetSettingList> dialyreports, SettingsFragment settingsFragment) {
        this.settinglist = dialyreports;
        this.context = context;
        this.settingsFragment=settingsFragment;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.setting_printer_item, parent, false);

        return new MyViewHolder(itemView);
    }


    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {

        holder.imageView.setImageResource(settinglist.get(position).getImageID());
        holder.tv_category_title.setText(settinglist.get(position).getItemName());
        holder.ll_setting_item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                settingsFragment.setClick(settinglist.get(position),position);
            }
        });


    }

    @Override
    public int getItemCount() {
        return settinglist.size();
    }

    public void setRefresh(ArrayList<GetDailyReport> dialyreports) {
        notifyDataSetChanged();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_category_title;
        private ImageView imageView;
        private LinearLayout ll_setting_item;

        public MyViewHolder(View view) {
            super(view);
            tv_category_title = (TextView) view.findViewById(R.id.tv_category_title);
            imageView =  view.findViewById(R.id.imageView);
            ll_setting_item =  view.findViewById(R.id.ll_setting_item);
        }
    }

}
