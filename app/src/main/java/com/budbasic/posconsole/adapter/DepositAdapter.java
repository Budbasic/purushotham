package com.budbasic.posconsole.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.budbasic.posconsole.R;
import com.budbasic.posconsole.reception.DepositFragment;
import com.budbasic.posconsole.reception.ExpensesFragment;
import com.kotlindemo.model.GetData;
import com.kotlindemo.model.GetDepositItems;
import com.kotlindemo.model.GetDeposits;
import com.kotlindemo.model.GetEmployeeExpanses;
import com.kotlindemo.model.GetEmployeeLogin;

import java.util.Calendar;
import java.util.List;

public class DepositAdapter extends RecyclerView.Adapter<DepositAdapter.MyViewHolder> {


    private Context context;
    List<GetDepositItems> getDepositsList;
    GetEmployeeLogin login;
    private DepositFragment depositFragment;

    public DepositAdapter(Context context, List<GetDepositItems> getEmpExpenses, GetEmployeeLogin logindata, DepositFragment expensesFragment) {
        this.context = context;
        this.getDepositsList = getEmpExpenses;
        this.login=logindata;
        this.depositFragment=expensesFragment;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.expensevelist, parent, false);

        return new MyViewHolder(itemView);
    }
    public void dataChanged(List<GetData> moviesList){

        notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        holder.tv_expense_date.setText(getDepositsList.get(position).getDate());
        holder.tv_expstore.setText(getDepositsList.get(position).getCompany_name());
        holder.tv_exstore.setText(getDepositsList.get(position).getStore_name());
        holder.tv_employee.setText(getDepositsList.get(position).getEmp_first_name()+" "+getDepositsList.get(position).getEmp_last_name());
        holder.tv_exitem.setText("$"+getDepositsList.get(position).getDepositamount());
        holder.tv_amount.setText(getDepositsList.get(position).getDeposit());

        String[] spilt=getDepositsList.get(position).getDate().split("-");
        Calendar calendar=Calendar.getInstance();
        int day=calendar.get(Calendar.DAY_OF_MONTH);
        int month=calendar.get(Calendar.MONTH);
        int year=calendar.get(Calendar.YEAR);
        if (getDepositsList.get(position).getDate().equalsIgnoreCase((month+1)+"-"+day+"-"+year)){
            holder.layout_currentdate.setVisibility(View.VISIBLE);
        }else {
            holder.layout_currentdate.setVisibility(View.INVISIBLE);
        }

        holder.tv_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                depositFragment.setEdit(getDepositsList.get(position));
                /*String getdata=new Gson().toJson(getEmployeeExpansesList.get(position));
                String logindata=new Gson().toJson(login);
                Intent i=new Intent(context, AddExpensesActivity.class);
                i.putExtra("position",1);
                i.putExtra("expansedata",getdata);
                i.putExtra("logindata",logindata);
                context.startActivity(i);*/
            }
        });
        holder.img_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                depositFragment.setDelete(getDepositsList.get(position));
            }
        });
    }

    @Override
    public int getItemCount() {
        return getDepositsList.size();
    }

    public void setRefresh(List<GetDepositItems> result) {
        this.getDepositsList=result;
        notifyDataSetChanged();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_expense_date, tv_expstore,tv_change_table,tv_exstore,tv_employee,tv_exitem,tv_amount,tv_edit;
        private LinearLayout layout_currentdate;
        private ImageView img_delete;

        public MyViewHolder(View view) {
            super(view);
            tv_expense_date = itemView.findViewById(R.id.tv_expense_date);
            tv_expstore = itemView.findViewById(R.id.tv_expstore);
            tv_change_table = itemView.findViewById(R.id.tv_change_table);
            tv_exstore = itemView.findViewById(R.id.tv_exstore);
            tv_employee = itemView.findViewById(R.id.tv_employee);
            tv_exitem = itemView.findViewById(R.id.tv_exitem);
            tv_amount = itemView.findViewById(R.id.tv_amount);
            layout_currentdate = itemView.findViewById(R.id.layout_currentdate);
            tv_edit = itemView.findViewById(R.id.tv_edit);
            img_delete = itemView.findViewById(R.id.img_delete);



        }
    }

}
