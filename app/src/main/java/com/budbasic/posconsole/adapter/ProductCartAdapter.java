package com.budbasic.posconsole.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.budbasic.posconsole.R;
import com.budbasic.posconsole.reception.SalesActivity;
import com.kotlindemo.model.GetCartData;
import com.kotlindemo.model.GetEmployeePatientCart;
import com.kotlindemo.model.GetPatient;
import com.kotlindemo.model.GetProducts;

import java.util.ArrayList;
import java.util.List;

public class ProductCartAdapter extends RecyclerView.Adapter<ProductCartAdapter.MyViewHolder> {

    private List<GetCartData> productlist;
    String currency_value;
    private Context context;
    private GetEmployeePatientCart viewCard;

    public ProductCartAdapter(Context receptionSalesPatientActivity, List<GetCartData> moviesList, String currency_value,GetEmployeePatientCart viewCard) {
        this.productlist = moviesList;
        this.currency_value = currency_value;
        this.context = receptionSalesPatientActivity;
        this.viewCard = viewCard;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.cart_product_items, parent, false);
        return new MyViewHolder(itemView);
    }
    public void dataChanged(List<GetCartData> moviesList, GetEmployeePatientCart viewCard){
        this.productlist=moviesList;
        this.viewCard=viewCard;
        notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        System.out.println("aaaaaaaaaaa     "+productlist.get(position));
        holder.tv_name.setText(""+productlist.get(position).getPacket_name());
        holder.tv_mobilenumber.setText(""+productlist.get(position).getCart_detail_qty()+" "+productlist.get(position).getProduct_mapping().getUnit_short_name());
       // holder.tv_catname.setText(""+productlist.get(position).getPacket_name());
        holder.tv_phonenumber.setText(currency_value+""+productlist.get(position).getCart_detail_total_price());
        try {
            System.out.println("aaaaaaaaaa  fixed "+productlist.get(position).getCart_detail_fix_disc()+" percentage   "+productlist.get(position).getCart_detail_per_disc());
            if (!productlist.get(position).getCart_detail_per_disc().equalsIgnoreCase("0.00") ){
                if (productlist.get(position).getCart_detail_per_disc().equalsIgnoreCase("0")){
                    if (!productlist.get(position).getCart_detail_fix_disc().equalsIgnoreCase("0.00")){
                        if (productlist.get(position).getCart_detail_fix_disc().equalsIgnoreCase("0")){
                            holder.tv_discount.setVisibility(View.GONE);
                        }else {
                            holder.tv_discount.setVisibility(View.VISIBLE);
                            holder.tv_discount.setText(currency_value+" "+productlist.get(position).getCart_detail_fix_disc()+" off");
                        }
                    }else {
                        holder.tv_discount.setVisibility(View.GONE);
                    }
                }else {
                    holder.tv_discount.setVisibility(View.VISIBLE);
                    holder.tv_discount.setText(productlist.get(position).getCart_detail_per_disc()+" % off");
                }

            }else if (!productlist.get(position).getCart_detail_fix_disc().equalsIgnoreCase("0.00") || !productlist.get(position).getCart_detail_fix_disc().equalsIgnoreCase("0")){

                holder.tv_discount.setVisibility(View.VISIBLE);
                holder.tv_discount.setText(currency_value+" "+productlist.get(position).getCart_detail_fix_disc()+" off");
            }else {
                holder.tv_discount.setVisibility(View.GONE);
            }
        }catch (NullPointerException e){

        }

        holder.ll_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((SalesActivity)context).setUpdateCart(productlist.get(position));
            }
        });holder.iv_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (productlist.size()==1){
                    ((SalesActivity)context).removecart(productlist.get(position),viewCard);
                }else {
                    ((SalesActivity)context).showdialogask(productlist.get(position));
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return productlist.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_name, tv_mobilenumber,tv_phonenumber,tv_catname,tv_discount;
        private LinearLayout ll_edit;
        private ImageView img_cat,iv_delete;

        public MyViewHolder(View view) {
            super(view);
            tv_name = itemView.findViewById(R.id.tv_name);
            tv_mobilenumber = itemView.findViewById(R.id.tv_mobilenumber);
            img_cat = itemView.findViewById(R.id.img_cat);
            tv_phonenumber = itemView.findViewById(R.id.tv_phonenumber);
            tv_catname = itemView.findViewById(R.id.tv_catname);
            ll_edit = itemView.findViewById(R.id.ll_edit);
            iv_delete = itemView.findViewById(R.id.iv_delete);
            tv_discount = itemView.findViewById(R.id.tv_discount);

        }
    }

}
