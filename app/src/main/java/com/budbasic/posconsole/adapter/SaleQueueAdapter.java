package com.budbasic.posconsole.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.budbasic.posconsole.R;
import com.budbasic.posconsole.reception.ProductsFragment;
import com.budbasic.posconsole.reception.SalesActivity;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DecodeFormat;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.kotlindemo.model.GetData;
import com.kotlindemo.model.GetEmployeeLogin;
import com.kotlindemo.model.GetQueueList;
import com.kotlindemo.model.GetSalesProduct;

import java.util.ArrayList;
import java.util.List;

public class SaleQueueAdapter extends RecyclerView.Adapter<SaleQueueAdapter.MyViewHolder> implements Filterable {


    private Context context;
    List<GetQueueList> cardlist;
    List<GetQueueList> filtercardlist;
    Gson gson;
    GetEmployeeLogin logindata;
    public SaleQueueAdapter(Context context, ArrayList<GetQueueList> queuelist, GetEmployeeLogin logindata) {
        this.context = context;
        this.cardlist=queuelist;
        this.filtercardlist=queuelist;
        this.logindata=logindata;
        gson=new Gson();

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.sales_dailog_queue, parent, false);

        return new MyViewHolder(itemView);
    }
    public void dataChanged(List<GetQueueList> queuelist){
        this.cardlist=queuelist;
        this.filtercardlist=queuelist;
        notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {


        holder.tv_name.setText(filtercardlist.get(position).getFirst_name()+" "+filtercardlist.get(position).getLast_name());
        holder.tv_name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String myJson = gson.toJson(logindata);
                String QueueData = gson.toJson(filtercardlist.get(position));
                Intent goIntent = new Intent(context, SalesActivity.class);
                goIntent.putExtra("queue_patient_id", ""+filtercardlist.get(position).getQueue_patient_id());
                goIntent.putExtra("queue_id", ""+filtercardlist.get(position).getQueue_id());
                goIntent.putExtra("QueueData", ""+QueueData);
                goIntent.putExtra("logindata",myJson);
                context.startActivity(goIntent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return filtercardlist.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_name, p_tag,tv_catamt,tv_catname;
        private LinearLayout layout_product;
        private ImageView img_cat;

        public MyViewHolder(View view) {
            super(view);
            tv_name = itemView.findViewById(R.id.tv_name);


        }
    }
    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    filtercardlist = cardlist;
                } else {
                    List<GetQueueList> filteredList = new ArrayList<>();
                    for (GetQueueList row : cardlist) {
                        System.out.println("aaaaaaaa  text  "+charString);
                        if ((row.getFirst_name().toLowerCase()+row.getLast_name().toLowerCase()).contains(charString.toLowerCase())) {
                            filteredList.add(row);
                        }
                    }

                    filtercardlist = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = filtercardlist;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                filtercardlist = (ArrayList<GetQueueList>) filterResults.values;
                System.out.println("aaaaaaaaaaaaa  size  "+filtercardlist.size());
                notifyDataSetChanged();
            }
        };
    }

    public interface ContactsAdapterListener {
        void onContactSelected(GetSalesProduct contact);
    }

}
