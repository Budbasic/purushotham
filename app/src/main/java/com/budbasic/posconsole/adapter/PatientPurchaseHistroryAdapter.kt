package com.budbasic.posconsole.adapter

import android.app.Activity
import android.content.Intent
import android.os.Build
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.budbasic.global.CUC
import com.budbasic.posconsole.EmployeeInvoiceDetailsActivity
import com.budbasic.posconsole.R
import com.google.gson.Gson
import com.kotlindemo.model.InvoiceHistory

class PatientPurchaseHistroryAdapter() : RecyclerView.Adapter<PatientPurchaseHistroryAdapter.Holder>() {

    var activity: Activity? = null
    var data: ArrayList<InvoiceHistory>? = null
    var tempData = ArrayList<InvoiceHistory>()
    var patientName: String? = null

    constructor(activity: Activity, patientName: String, data: ArrayList<InvoiceHistory>) : this() {
        this.data = data
        this.tempData.addAll(data)
        this.activity = activity
        this.patientName = patientName
    }


    var priviousView: View? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
        return Holder(LayoutInflater.from(activity!!).inflate(R.layout.sales_patient_detail_purchase_history_item_view, parent, false))
    }

    override fun getItemCount(): Int {
        return tempData!!.size
    }

    fun setFilterValue(value: String) {
        //dataTemp = ArrayList<InvoiceHistory>()
        if (value.isNotEmpty()) {
            tempData.clear()
            data?.forEach {
                var date = CUC.getdatefromoneformattoAnother("yyyy-MM-dd HH:mm:ss", "MM-dd-yyyy", "${it.invoice_created_date}")
                if (data!!.isNotEmpty() && date.trim().equals(value.trim(), true)) {
                    tempData.add(it)
                }
            }
            notifyDataSetChanged()
        } else {
            tempData.clear()
            tempData.addAll(data!!)
            notifyDataSetChanged()
        }

    }

    /*fun setNewData(newdata: ArrayList<InvoiceHistory>) {
        tempData!!.clear()
        tempData!!.addAll(newdata)
        notifyDataSetChanged()

    }*/

    override fun onBindViewHolder(holder: Holder, position: Int) {
        holder.prodctName.text = patientName
       // holder.orderId.text = tempData!![position].invoice_order_unique_no
        holder.itemView.setOnClickListener {


            if (priviousView != null) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    priviousView!!.setBackgroundColor(activity!!.getColor(R.color.white))
                } else {
                    priviousView!!.setBackgroundColor(activity!!.resources.getColor(R.color.white))
                }
            }

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                it.setBackgroundColor(activity!!.getColor(R.color.blue))
            } else {
                it.setBackgroundColor(activity!!.resources.getColor(R.color.blue))
            }
            priviousView = it


            val intent = Intent(activity, EmployeeInvoiceDetailsActivity::class.java)
            intent.putExtra("user_info_page", patientName)
            var strData = Gson().toJson(tempData!![position])
            intent.putExtra("user_info_page_data", strData)
            activity!!.startActivity(intent)

        }


        val date = CUC.getdatefromoneformattoAnother("yyyy-MM-dd HH:mm:ss", "MM/dd/yyyy", "${tempData[position].invoice_created_date}")
        if (date.isNotEmpty()) {

            holder.orderDate.text = date
        }


        /*holder.llDelete.setOnClickListener {
            apiCallForEmployeeDocument(data[position].document_id)
        }*/
        /* if (data[position].document_title.equals("No data found", true)) {
             holder.llDelete.visibility = View.GONE
             holder.llView.visibility = View.GONE
         } else {
             holder.llDelete.visibility = View.VISIBLE
             holder.llView.visibility = View.VISIBLE
         }*/
    }


    inner class Holder(v: View) : RecyclerView.ViewHolder(v) {
        val prodctName: TextView = v.findViewById(R.id.prodctName)
        val orderId: TextView = v.findViewById(R.id.orderId)
        val orderDate: TextView = v.findViewById(R.id.orderDate)

    }


    /*private fun apiCallForEmployeeDocument(document_id: String) {
        var mDialog = Dialog(mcontext)

        try {
            if (!activity!!.isFinishing) {
                mDialog = CUC.createDialog(activity!!)
                mDialog.show()
                val apiService = RequetsInterface.Factory.create()
                val parameterMap = HashMap<String, String>()

                parameterMap["api_key"] = "${Preferences.getPreference(mcontext!!, "API_KEY")}"
                parameterMap["employee_id"] = employee_id
                parameterMap["patient_id"] = patient_id
                parameterMap["document_id"] = document_id

                CompositeDisposable().add(apiService.callEmployeePatientDeleteDocument(parameterMap)
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.io())
                        .subscribe({ result ->
                            if (mDialog != null && mDialog.isShowing)
                                mDialog.dismiss()
                            if (result?.documents?.size!! > 0) {
                                data.clear()
                                data.addAll(result.documents)
                                notifyDataSetChanged()

                            } else {
                                data.clear()
                                data.add(PatientDocumentModel("", "", "No data found", "", "", "", "", ""))
                                notifyDataSetChanged()
                            }

                        }, { error ->
                            if (mDialog != null && mDialog.isShowing)
                                mDialog.dismiss()
                            CUC.displayToast(mcontext!!, "0", mcontext.getString(R.string.connection_to_database_failed))
                            error.printStackTrace()
                        })
                )
            }
        } catch (e: Exception) {
            if (mDialog != null && mDialog.isShowing) {
                mDialog.dismiss()
            }
            e.printStackTrace()
        }

    }*/
}