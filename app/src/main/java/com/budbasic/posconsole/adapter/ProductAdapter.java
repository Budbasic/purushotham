package com.budbasic.posconsole.adapter;

import android.content.Context;
import android.graphics.Point;
import android.support.v7.widget.RecyclerView;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.budbasic.posconsole.R;
import com.budbasic.posconsole.reception.ProductsFragment;
import com.budbasic.posconsole.reception.ReceptionSalesPatientActivity;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DecodeFormat;
import com.bumptech.glide.request.RequestOptions;
import com.kotlindemo.model.GetCartData;
import com.kotlindemo.model.GetData;
import com.kotlindemo.model.GetPatient;
import com.kotlindemo.model.GetProducts;
import com.kotlindemo.model.GetSalesProduct;

import java.util.ArrayList;
import java.util.List;

public class ProductAdapter extends RecyclerView.Adapter<ProductAdapter.MyViewHolder> implements Filterable {


    private Context context;
    List<GetSalesProduct> cardlist;
    List<GetSalesProduct> filtercardlist;
    String image_path;
    ProductsFragment productsFragment;
    private String Currency_value;

    public ProductAdapter(Context receptionSalesPatientActivity,
                          List<GetSalesProduct> list, String image_path, ProductsFragment productsFragment, String currency_value) {
        this.context = receptionSalesPatientActivity;
        this.cardlist=list;
        this.filtercardlist=list;
        this.image_path=image_path;
        this.productsFragment=productsFragment;
        this.Currency_value=currency_value;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.product_search_items, parent, false);

        return new MyViewHolder(itemView);
    }
    public void dataChanged(List<GetData> moviesList){

        notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {

            holder.p_name.setText(""+filtercardlist.get(position).getPacket_name());
            holder.tv_catamt.setText(Currency_value+" "+filtercardlist.get(position).getPacket_base_price());
            Glide.with(context).load(image_path+""+filtercardlist.get(position).getProduct_images())
                    .apply(RequestOptions.formatOf(DecodeFormat.PREFER_ARGB_8888).error(R.drawable.no_image))
                   // .applyDefaultRequestOptions(RequestOptions().format(DecodeFormat.PREFER_ARGB_8888).error(R.drawable.no_image).placeholder(circularProgressDrawable))
                    .into(holder.img_cat);
            holder.tv_catname.setText(""+filtercardlist.get(position).getCategory_name());
        System.out.println("$ "+filtercardlist.get(position).getProduct_price()+"  "+filtercardlist.get(position).getP_map_base_price()+"  "+
                filtercardlist.get(position).getPacket_base_price());

        holder.layout_product.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                productsFragment.onContactSelected(filtercardlist.get(position));
            }
        });

    }

    @Override
    public int getItemCount() {
        return filtercardlist.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView p_name, p_tag,tv_catamt,tv_catname;
        private LinearLayout layout_product;
        private ImageView img_cat;

        public MyViewHolder(View view) {
            super(view);
            p_name = itemView.findViewById(R.id.tv_text);
            p_tag = itemView.findViewById(R.id.tv_texttag);
            img_cat = itemView.findViewById(R.id.img_cat);
            tv_catamt = itemView.findViewById(R.id.tv_catamt);
            tv_catname = itemView.findViewById(R.id.tv_catname);
            layout_product = itemView.findViewById(R.id.layout_product);

           /* WindowManager wm = (WindowManager)context.getSystemService(Context.WINDOW_SERVICE);
            Display display = wm.getDefaultDisplay();

            int width = display.getWidth();
            int height = display.getHeight();
            LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(width,
                    width/4); // or set height to any fixed value you want

            layout_product.setLayoutParams(lp);*/

        }
    }
    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    filtercardlist = cardlist;
                } else {
                    List<GetSalesProduct> filteredList = new ArrayList<>();
                    for (GetSalesProduct row : cardlist) {

                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                       // System.out.println("aaaaaaaaaaaaa  search  "+row.getProduct_name()+"   "+charString.toString());
                        if (row.getProduct_name().toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(row);
                        }
                    }

                    filtercardlist = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = filtercardlist;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                filtercardlist = (ArrayList<GetSalesProduct>) filterResults.values;
                System.out.println("aaaaaaaaaaaaa  size  "+filtercardlist.size());
                notifyDataSetChanged();
            }
        };
    }

    public interface ContactsAdapterListener {
        void onContactSelected(GetSalesProduct contact);
    }

}
