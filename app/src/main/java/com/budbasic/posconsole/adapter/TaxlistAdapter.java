package com.budbasic.posconsole.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.budbasic.posconsole.R;
import com.budbasic.posconsole.reception.EmployeeInvoiceDetails;
import com.budbasic.posconsole.sales.UserInfoDetails;
import com.kotlindemo.model.GetTaxData;
import com.kotlindemo.model.notesdata;

import java.util.ArrayList;

public class TaxlistAdapter extends RecyclerView.Adapter<TaxlistAdapter.MyViewHolder> {


    private Context context;
    ArrayList<GetTaxData> taxlist;
    String currencyvalue;

    public TaxlistAdapter(Context context, String currency_value, ArrayList<GetTaxData> taxList) {
        this.context=context;
        this.taxlist=taxList;
        this.currencyvalue=currency_value;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.tax_data_item, parent, false);

        return new MyViewHolder(itemView);
    }
    public void dataChanged(ArrayList<GetTaxData> moviesList){
        this.taxlist=moviesList;
        notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        holder.tv_tax_title.setText(""+taxlist.get(position).getTax_name());
        holder.tv_tax_amount.setText(""+taxlist.get(position).getTax_total());

        if (taxlist.get(position).getTax_status().equalsIgnoreCase("2")) {
            holder.tv_tax_amount.setText(taxlist.get(position).getTax_total());
        } else {
            if (taxlist.get(position).getTax_name().equalsIgnoreCase("Loyalty Amount")) {
                holder.tv_tax_amount.setText(currencyvalue+""+taxlist.get(position).getTax_total());
            } else if (taxlist.get(position).getTax_name().equalsIgnoreCase("Discount")) {
                holder.tv_tax_amount.setText(currencyvalue+""+taxlist.get(position).getTax_total());
            } else {
                holder.tv_tax_amount.setText(currencyvalue+""+taxlist.get(position).getTax_total());
            }
        }
    }

    @Override
    public int getItemCount() {
        return taxlist.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_tax_title,tv_tax_amount;

        public MyViewHolder(View view) {
            super(view);
            tv_tax_title = itemView.findViewById(R.id.tv_tax_title);
            tv_tax_amount = itemView.findViewById(R.id.tv_tax_amount);

        }
    }


}
