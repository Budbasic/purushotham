package com.budbasic.posconsole.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.budbasic.posconsole.R;
import com.budbasic.posconsole.reception.SalesActivity;
import com.kotlindemo.model.GetCartData;
import com.kotlindemo.model.GetPatient;
import com.kotlindemo.model.GetTaxData;

import java.util.List;

public class TaxitemAdapter extends RecyclerView.Adapter<TaxitemAdapter.MyViewHolder> {

    private Context context;
    private List<GetTaxData> taxList;
    String currency_value;


    public TaxitemAdapter(Context salesActivity, List<GetTaxData> taxList, String currency_value) {
        this.context=salesActivity;
        this.taxList=taxList;
        this.currency_value=currency_value;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.tax_data_item, parent, false);

        return new MyViewHolder(itemView);
    }
    public void dataChanged(List<GetTaxData> taxList){
        this.taxList=taxList;
        notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        holder.tv_tax_title.setTextColor(context.getResources().getColor(R.color.graylight));
      holder.tv_tax_title.setText(""+taxList.get(position).getTax_name());
      holder.tv_tax_amount.setText(""+taxList.get(position).getTax_total());

        /*if (taxList.get(position).getTax_status().equals("2")) {
            holder.tv_tax_amount.setText(taxList.get(position).getTax_total()+"%");
        } else {
            if (taxList.get(position).getTax_name().equals("Loyalty Amount")) {
                holder.tv_tax_amount.setText(""+currency_value+" "+taxList.get(position).getTax_total()+"%");
            } else if (taxList.get(position).getTax_name().equals("Discount")) {
                holder.tv_tax_amount.setText(""+currency_value+" "+taxList.get(position).getTax_total());
            } else {
                holder.tv_tax_amount.setText(""+currency_value+" "+taxList.get(position).getTax_total());
            }
        }*/

    }

    @Override
    public int getItemCount() {
        return taxList.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_tax_title, tv_tax_amount;

        public MyViewHolder(View view) {
            super(view);
            tv_tax_title = itemView.findViewById(R.id.tv_tax_title);
            tv_tax_amount = itemView.findViewById(R.id.tv_tax_amount);


        }
    }

    public interface ContactsAdapterListener {
        void onContactSelected(GetPatient contact);
    }

}
