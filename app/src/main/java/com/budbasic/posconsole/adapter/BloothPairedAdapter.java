package com.budbasic.posconsole.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.budbasic.posconsole.R;
import com.budbasic.posconsole.reception.ProductsFragment;
import com.budbasic.posconsole.reception.fragment.PrinterConnectionActivity;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DecodeFormat;
import com.bumptech.glide.request.RequestOptions;
import com.kotlindemo.model.GetData;
import com.kotlindemo.model.GetSalesProduct;
import com.kotlindemo.model.getprinterdata;

import java.util.ArrayList;
import java.util.List;

public class BloothPairedAdapter extends RecyclerView.Adapter<BloothPairedAdapter.MyViewHolder> {


    private Context context;

    ArrayList<String> list;
    private int whichactivity;
    ArrayList<getprinterdata> printerlist;

    public BloothPairedAdapter(PrinterConnectionActivity context, ArrayList<String> list,int i) {
        this.context=context;
        this.list=list;
        this.whichactivity=i;
    }

    public BloothPairedAdapter(PrinterConnectionActivity context,int i,ArrayList<getprinterdata> printerlist) {
        this.context=context;
        this.whichactivity=i;
        this.printerlist=printerlist;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.raw_devicelist1, parent, false);

        return new MyViewHolder(itemView);
    }


    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        if (whichactivity==0){
            holder.tv_edit.setVisibility(View.GONE);
            holder.txt_devicename.setText(""+list.get(position));

            holder.txt_devicename.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ((PrinterConnectionActivity)context).setConnect(list.get(position));
                }
            });
        }else {
            holder.tv_edit.setVisibility(View.VISIBLE);
            holder.txt_devicename.setText(""+printerlist.get(position).getHardware_manufacturer());

            holder.tv_edit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ((PrinterConnectionActivity)context).setEditprinter(printerlist.get(position));
                }
            });
        }


    }

    @Override
    public int getItemCount() {
        if (whichactivity==0){
            return list.size();
        }else {
            return printerlist.size();
        }

    }

    public void setRefresh(ArrayList<String> list) {
        this.list=list;
        notifyDataSetChanged();
    }

    public void setrefreshPrinters(ArrayList<getprinterdata> printerlist) {
        this.printerlist=printerlist;
        notifyDataSetChanged();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView txt_devicename, tv_edit,tv_catamt,tv_catname;
        private LinearLayout layout_product;
        private ImageView img_cat;

        public MyViewHolder(View view) {
            super(view);
            txt_devicename = itemView.findViewById(R.id.txt_devicename);
            tv_edit = itemView.findViewById(R.id.tv_edit);


        }
    }
}
