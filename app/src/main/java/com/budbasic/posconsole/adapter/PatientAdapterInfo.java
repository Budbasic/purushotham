package com.budbasic.posconsole.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.budbasic.posconsole.EmployeeInvoiceDetailsActivity;
import com.budbasic.posconsole.R;
import com.budbasic.posconsole.reception.EmployeeInvoiceDetails;
import com.budbasic.posconsole.reception.ReceptionSalesPatientActivity;
import com.budbasic.posconsole.sales.UserInfoDetails;
import com.google.gson.Gson;
import com.kotlindemo.model.GetPatient;
import com.kotlindemo.model.InvoiceHistory;

import java.util.ArrayList;
import java.util.List;

public class PatientAdapterInfo extends RecyclerView.Adapter<PatientAdapterInfo.MyViewHolder>  {

    private Context context;
    private String patientname;
    ArrayList<InvoiceHistory> invoice_history;
    public PatientAdapterInfo(Context userInfoDetails, String patientName, ArrayList<InvoiceHistory> invoice_history) {
        this.context=userInfoDetails;
        this.patientname=patientName;
        this.invoice_history=invoice_history;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.sales_patient_detail_purchase_history_item_view, parent, false);

        return new MyViewHolder(itemView);
    }
    public void dataChanged(ArrayList<InvoiceHistory> moviesList){
        this.invoice_history=moviesList;
        notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
       holder.prodctName.setText(""+patientname);
        String[] historydate = invoice_history.get(position).getInvoice_created_date().split(" ");
        String[] historydatespilt = historydate[0].split("-");


       holder.orderDate.setText(historydatespilt[1]+"-"+historydatespilt[2]+"-"+historydatespilt[0]);

       holder.ll_itemamin.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {
               Intent intent =new Intent(context, EmployeeInvoiceDetails.class);
               intent.putExtra("user_info_page", patientname);
               intent.putExtra("order_type", "4");
               String strData = new Gson().toJson(invoice_history.get(position));
             //  intent.putExtra("user_info_page_data", strData);
               intent.putExtra("all_data", strData);
               context.startActivity(intent);
           }
       });
    }

    @Override
    public int getItemCount() {
        return invoice_history.size();
    }

    public void setChanged(ArrayList<InvoiceHistory> historymodified) {
        this.invoice_history=historymodified;
        notifyDataSetChanged();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView prodctName, orderDate;
        private LinearLayout ll_itemamin;

        public MyViewHolder(View view) {
            super(view);
            prodctName = (TextView) view.findViewById(R.id.prodctName);
            orderDate = (TextView) view.findViewById(R.id.orderDate);
            ll_itemamin =  view.findViewById(R.id.main);
        }
    }

    public interface ContactsAdapterListener {
        void onContactSelected(GetPatient contact);
    }

}
