package com.budbasic.posconsole.adapter;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.budbasic.posconsole.R;
import com.budbasic.posconsole.dialogs.AddDocumentDailog;
import com.budbasic.posconsole.dialogs.AddDocumentNotesDialog;
import com.budbasic.posconsole.dialogs.AddEditPatientNoteDialog;
import com.budbasic.posconsole.reception.PatientEdit;
import com.budbasic.posconsole.reception.fragment.CustomerSearchFragment;
import com.budbasic.posconsole.sales.UserInfoDetails;
import com.kotlindemo.model.GetEmployeeAppSetting;
import com.kotlindemo.model.GetEmployeeLogin;
import com.kotlindemo.model.GetNotes;
import com.kotlindemo.model.GetNotesDatashow;
import com.kotlindemo.model.GetPatient;
import com.kotlindemo.model.Patient_data;
import com.kotlindemo.model.notesdata;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;

public class NotesDialogAdaper extends RecyclerView.Adapter<NotesDialogAdaper.MyViewHolder> {

    private Context context;
    ArrayList<GetNotes> notes_data;

    public NotesDialogAdaper(PatientEdit patientEdit, ArrayList<GetNotes> noteslist) {
        this.context=patientEdit;
        this.notes_data=noteslist;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.notes_dailog_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        holder.tv_title.setText(""+notes_data.get(position).getPnote_title());

        holder.tv_title.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((PatientEdit)context).noteClick(notes_data.get(position));

            }
        });
    }

    @Override
    public int getItemCount() {
        return notes_data.size();
    }

    public void setRefresh(ArrayList<GetNotes> noteslist) {

        notifyDataSetChanged();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_title;

        public MyViewHolder(View view) {
            super(view);
            tv_title = itemView.findViewById(R.id.tv_title);


        }
    }

    public interface ClickNOte {
        void onClickNote(GetNotes getNotes);
    }
}
