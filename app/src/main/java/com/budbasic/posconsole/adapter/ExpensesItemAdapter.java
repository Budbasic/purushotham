package com.budbasic.posconsole.adapter;

import android.app.Dialog;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.budbasic.posconsole.R;
import com.budbasic.posconsole.reception.AddExpensesActivity;
import com.budbasic.posconsole.reception.ExpensesFragment;
import com.budbasic.posconsole.reception.ProductsFragment;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DecodeFormat;
import com.bumptech.glide.request.RequestOptions;
import com.kotlindemo.model.GetData;
import com.kotlindemo.model.GetExpensesItems;
import com.kotlindemo.model.GetSalesProduct;

import java.util.ArrayList;
import java.util.List;

public class ExpensesItemAdapter extends RecyclerView.Adapter<ExpensesItemAdapter.MyViewHolder> {


    private Context context;
    ArrayList<GetExpensesItems> getExpensesLists;
    Dialog dialog;
    ExpensesFragment expensesFragment;

    public ExpensesItemAdapter(Context context, ArrayList<GetExpensesItems> getExpensesLists, Dialog dialog, ExpensesFragment expensesFragment) {
        this.context = context;
        this.getExpensesLists = getExpensesLists;
        this.dialog = dialog;
        this.expensesFragment = expensesFragment;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.expansesitems, parent, false);

        return new MyViewHolder(itemView);
    }


    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {

        holder.tv_text.setText(getExpensesLists.get(position).getExpensename());

        holder.tv_text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                expensesFragment.setexpansesItem(getExpensesLists.get(position));
               // ((AddExpensesActivity)context).setexpansesItem(getExpensesLists.get(position));
            }
        });
    }

    @Override
    public int getItemCount() {
        return getExpensesLists.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_text;


        public MyViewHolder(View view) {
            super(view);
            tv_text = itemView.findViewById(R.id.tv_text);

        }
    }


}
