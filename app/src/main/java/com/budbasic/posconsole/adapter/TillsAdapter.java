package com.budbasic.posconsole.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.budbasic.customer.global.Preferences;
import com.budbasic.posconsole.R;
import com.budbasic.posconsole.dialogs.AddCashDrop;
import com.budbasic.posconsole.reception.CashDrawerFragment;
import com.budbasic.posconsole.reception.DailyReportActivity;
import com.google.gson.Gson;
import com.kotlindemo.model.GetDailyReport;
import com.kotlindemo.model.GetEmployeeAppSetting;
import com.kotlindemo.model.GetEmployeeLogin;
import com.kotlindemo.model.GetsalesTerminalsdata;

import java.util.ArrayList;

public class TillsAdapter extends RecyclerView.Adapter<TillsAdapter.MyViewHolder>  {


    private Context context;
    ArrayList<GetsalesTerminalsdata> getsalesTerminals;
    CashDrawerFragment cashDrawerFragment;
    GetEmployeeAppSetting mBAsis;
    GetEmployeeLogin logindata;

    public TillsAdapter(Context receptionSalesPatientActivity, ArrayList<GetsalesTerminalsdata> dialyreports,
                        CashDrawerFragment cashDrawerFragment, GetEmployeeLogin logindata) {
        this.getsalesTerminals = dialyreports;
        this.context = receptionSalesPatientActivity;
        this.cashDrawerFragment = cashDrawerFragment;
        this.logindata=logindata;
        mBAsis = new Gson().fromJson(Preferences.INSTANCE.getPreference(context, "AppSetting"), GetEmployeeAppSetting.class);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.tills_items, parent, false);

        return new MyViewHolder(itemView);
    }


    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {

        holder.tv_name.setText(""+getsalesTerminals.get(position).getTerminal_name());
        holder.tv_sub_name.setText(""+getsalesTerminals.get(position).getTerminal_employee_name());
        holder.tv_start_cast.setText(""+getsalesTerminals.get(position).getTerminal_deposit());
        holder.tv_drop_alert.setText(""+getsalesTerminals.get(position).getTerminal_alert());

        holder.tv_sales.setText(""+getsalesTerminals.get(position).getTerminal_totalsale());
        holder.tv_current_cash.setText(""+getsalesTerminals.get(position).getCurrent_cash());
        holder.tv_cash_taken.setText(""+getsalesTerminals.get(position).getTerminal_closingcash());
        holder.tv_open_status.setText(""+getsalesTerminals.get(position).getTill_start());
        holder.tv_close_status.setText(""+getsalesTerminals.get(position).getTill_close());

        if (TextUtils.isEmpty(getsalesTerminals.get(position).getTerminal_dropcount())) {
            holder.tv_cash_drops.setText("0");
        }else {
            holder.tv_cash_drops.setText(""+getsalesTerminals.get(position).getTerminal_dropcount());
        }

        if (TextUtils.isEmpty(getsalesTerminals.get(position).getTill_start())) {
            holder.tv_open_status.setText("Cash Drawer Not Opened..!");
        }else {
            holder.tv_open_status.setText(""+getsalesTerminals.get(position).getTill_start());
        }

        if (TextUtils.isEmpty(getsalesTerminals.get(position).getTill_close())) {
            holder.tv_close_status.setText("Cash Drawer Not Closed..!");
        }else {
            holder.tv_close_status.setText(""+getsalesTerminals.get(position).getTill_close());
        }

        if (mBAsis.getTerminal_id().equalsIgnoreCase(getsalesTerminals.get(position).getTerminal_id())){
            System.out.println("aaaaaaaaa  terminal id  ");
           // if (TextUtils.isEmpty(getsalesTerminals.get(position).getTill_close())){
                holder.cardview.setBackgroundColor(context.getResources().getColor(R.color.light_blue_500));
           /* }else {
                holder.cardview.setBackgroundColor(context.getResources().getColor(R.color.dark_grey_color));

            }
*/
        }else {
            holder.cardview.setBackgroundColor(context.getResources().getColor(R.color.dark_grey_color));
        }
       /* if (getsalesTerminals.get(position).getTill_start().equals(null) || getsalesTerminals.get(position).getTill_start().equals("0")){
            holder.tv_open_status.setText("Cash Drawer Not Opened..!");
        }else {
            holder.tv_open_status.setText(""+getsalesTerminals.get(position).getTill_start());
        }
            if (getsalesTerminals.get(position).getTill_close().equals(null) || getsalesTerminals.get(position).getTill_close().equals("0")){
            holder.tv_close_status.setText("Cash Drawer Not Closed..!");
        }else {
            holder.tv_close_status.setText(""+getsalesTerminals.get(position).getTill_close());
        }*/



        holder.tv_drawer_open.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cashDrawerFragment.setTillOpen(getsalesTerminals.get(position),1,"");
            }
        }); holder.tv_cash_drop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cashDrawerFragment.setCashDrop(getsalesTerminals.get(position));
            }
        });
        holder.img_storeclose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                AddCashDrop addCashDrop=new AddCashDrop();
                addCashDrop.showDialog(context,getsalesTerminals.get(position),logindata,cashDrawerFragment);

            }
        });
    }

    @Override
    public int getItemCount() {
        return getsalesTerminals.size();
    }

    public void setRefresh(ArrayList<GetsalesTerminalsdata> dialyreports) {
        this.getsalesTerminals=dialyreports;
        notifyDataSetChanged();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_name, tv_sub_name,tv_start_cast,tv_drop_alert,tv_cash_drops,tv_sales,tv_current_cash,tv_cash_taken,tv_drawer_open,
                tv_open_status,tv_cash_drop,tv_close_status;
        private ImageView img_storeclose;
        private LinearLayout ll_view;
        private CardView cardview;

        public MyViewHolder(View view) {
            super(view);
            tv_name = (TextView) view.findViewById(R.id.tv_name);
            tv_sub_name = (TextView) view.findViewById(R.id.tv_sub_name);
            tv_start_cast = (TextView) view.findViewById(R.id.tv_start_cast);
            tv_drop_alert = (TextView) view.findViewById(R.id.tv_drop_alert);
            tv_cash_drops = (TextView) view.findViewById(R.id.tv_cash_drops);
            tv_sales = (TextView) view.findViewById(R.id.tv_sales);
            tv_current_cash = (TextView) view.findViewById(R.id.tv_current_cash);
            tv_cash_taken = (TextView) view.findViewById(R.id.tv_cash_taken);
            tv_drawer_open = (TextView) view.findViewById(R.id.tv_drawer_open);
            tv_open_status = (TextView) view.findViewById(R.id.tv_open_status);
            tv_cash_drop = (TextView) view.findViewById(R.id.tv_cash_drop);
            tv_close_status = (TextView) view.findViewById(R.id.tv_close_status);
            img_storeclose = (ImageView) view.findViewById(R.id.img_storeclose);
            cardview = (CardView) view.findViewById(R.id.cardview);

        }
    }

}
