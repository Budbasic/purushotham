package com.budbasic.posconsole.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.budbasic.posconsole.R;
import com.budbasic.posconsole.reception.ProductsFragment;
import com.budbasic.posconsole.sales.UserInfoDetails;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DecodeFormat;
import com.bumptech.glide.request.RequestOptions;
import com.kotlindemo.model.GetData;
import com.kotlindemo.model.GetSalesProduct;
import com.kotlindemo.model.notesdata;

import java.util.ArrayList;
import java.util.List;

public class NotesAdaper extends RecyclerView.Adapter<NotesAdaper.MyViewHolder> {


    private Context context;
    ArrayList<notesdata> notes_data;


    public NotesAdaper(UserInfoDetails userInfoDetails, ArrayList<notesdata> notes_data) {
        this.context=userInfoDetails;
        this.notes_data=notes_data;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.notes_item, parent, false);

        return new MyViewHolder(itemView);
    }
    public void dataChanged(ArrayList<notesdata> moviesList){
        this.notes_data=moviesList;
        notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        holder.tv_title.setText(""+notes_data.get(position).getPnote_title());
        holder.tv_date.setText(""+notes_data.get(position).getPnote_last_updated());

        holder.tv_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((UserInfoDetails)context).setupdatenote(notes_data.get(position));
            }
        });
    }

    @Override
    public int getItemCount() {
        return notes_data.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_title,tv_date,tv_view;

        public MyViewHolder(View view) {
            super(view);
            tv_title = itemView.findViewById(R.id.tv_title);
            tv_date = itemView.findViewById(R.id.tv_date);
            tv_view = itemView.findViewById(R.id.tv_view);


        }
    }


}
