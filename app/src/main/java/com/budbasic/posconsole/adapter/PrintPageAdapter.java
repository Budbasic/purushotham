package com.budbasic.posconsole.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.budbasic.posconsole.R;
import com.budbasic.posconsole.reception.ProductsFragment;
import com.budbasic.posconsole.reception.fragment.PrintPageFragment;
import com.budbasic.posconsole.reception.fragment.PrinterConnectionActivity;
import com.budbasic.posconsole.sales.CheckPrintBarcode;
import com.budbasic.posconsole.weight_scale.ScanActivity;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DecodeFormat;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.kotlindemo.model.GetData;
import com.kotlindemo.model.GetEmployeeLogin;
import com.kotlindemo.model.GetSalesProduct;
import com.kotlindemo.model.GetSettingList;

import java.util.ArrayList;
import java.util.List;

public class PrintPageAdapter extends RecyclerView.Adapter<PrintPageAdapter.MyViewHolder> {


    Context context;
    ArrayList<GetSettingList> settingListing;
    private PrintPageFragment printPageFragment;
    private GetEmployeeLogin logindata;

    public PrintPageAdapter(Context context, ArrayList<GetSettingList> settingListing, PrintPageFragment printPageFragment,
                            GetEmployeeLogin logindata) {
        this.context=context;
        this.settingListing=settingListing;
        this.printPageFragment=printPageFragment;
        this.logindata=logindata;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.setting_printer_item, parent, false);

        return new MyViewHolder(itemView);
    }
    public void dataChanged(List<GetData> moviesList){

        notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {

        holder.tv_category_title.setText(""+settingListing.get(position).getItemName());
        holder. imageView.setImageResource(settingListing.get(position).getImageID());
        holder.ll_setting_item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (position==1){
                    printPageFragment.showprintlable();
                }else if (position==3){
                    String myJson = new Gson().toJson(logindata);
                    Intent i=new Intent(context, PrinterConnectionActivity.class);
                    i.putExtra("logindata",myJson);
                    context.startActivity(i);
                }else if (position==2){
                   // Toast.makeText(context, "Not Implemented", Toast.LENGTH_SHORT).show();
                   // Intent i=new Intent(context, CheckPrintBarcode.class);
                   // context.startActivity(i);
                    printPageFragment.showPrintlable(1);
                }else if (position==0){
                    printPageFragment.showPrintlable(0);
                }else if(position==4){
                    Intent i=new Intent(context, ScanActivity.class);
                    context.startActivity(i);
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return settingListing.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_category_title;
        private ImageView imageView;
        private LinearLayout ll_setting_item;

        public MyViewHolder(View view) {
            super(view);
            imageView=view.findViewById(R.id.imageView);
            tv_category_title=view.findViewById(R.id.tv_category_title);
            ll_setting_item=view.findViewById(R.id.ll_setting_item);

        }
    }


}
