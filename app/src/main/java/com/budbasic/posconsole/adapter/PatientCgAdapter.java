package com.budbasic.posconsole.adapter;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.budbasic.posconsole.R;
import com.budbasic.posconsole.reception.DepositFragment;
import com.budbasic.posconsole.reception.PatientEdit;
import com.kotlindemo.model.GetData;
import com.kotlindemo.model.GetDepositItems;
import com.kotlindemo.model.GetEmployeeLogin;
import com.kotlindemo.model.getCaregiver;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class PatientCgAdapter extends RecyclerView.Adapter<PatientCgAdapter.MyViewHolder> {


    private Context context;

    private ArrayList<getCaregiver> caregiverlist;
    String documentpath;

    public PatientCgAdapter(Context context, ArrayList<getCaregiver> caregiverlist,String documentpath) {
        this.context = context;
        this.caregiverlist = caregiverlist;
        this.documentpath = documentpath;


    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.caregivers_layout, parent, false);

        return new MyViewHolder(itemView);
    }
    public void dataChanged(List<GetData> moviesList){

        notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {

        holder.tv_name.setText(""+caregiverlist.get(position).getPatient_fname()+" "+caregiverlist.get(position).getPatient_lname());
        holder.tv_licence_no.setText(""+caregiverlist.get(position).getPatient_mmpp_licence_no());

        try {

            Picasso.with(context)
                    .load(documentpath + caregiverlist.get(position).getPatient_image())
                    .placeholder(R.drawable.no_image_available)
                    .error(R.drawable.no_image_available)
                    .into(holder.img_patient);

        } catch (Exception e) {

            e.printStackTrace();
        }
        holder.cv_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((PatientEdit)context).setRemovecgpatient(caregiverlist.get(position));
            }
        });
    }

    @Override
    public int getItemCount() {
        return caregiverlist.size();
    }

    public void setRefresh(ArrayList<getCaregiver> result) {
        this.caregiverlist=result;
        notifyDataSetChanged();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_name, tv_licence_no;
        private CardView cv_delete;
        private ImageView img_patient;

        public MyViewHolder(View view) {
            super(view);
            tv_name = itemView.findViewById(R.id.tv_name);
            tv_licence_no = itemView.findViewById(R.id.tv_licence_no);
            img_patient = itemView.findViewById(R.id.img_patient);
            cv_delete = itemView.findViewById(R.id.cv_delete);

        }
    }

}
