package com.budbasic.posconsole.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.budbasic.posconsole.R;
import com.budbasic.posconsole.reception.SalesActivity;
import com.kotlindemo.model.GetData;
import com.kotlindemo.model.getCaregiver;

import java.util.ArrayList;
import java.util.List;

public class CgAdapter extends RecyclerView.Adapter<CgAdapter.MyViewHolder>{
    private Context context;
    ArrayList<getCaregiver> cglist;
    private int change;
    public CgAdapter(Context context, ArrayList<getCaregiver> cglist) {
        this.context = context;
        this.cglist=cglist;
        this.change=0;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.cg_patients, parent, false);

        return new MyViewHolder(itemView);
    }
    public void dataChanged(ArrayList<getCaregiver> moviesList){
        this.cglist=moviesList;
        notifyDataSetChanged();
    }
    public void backgroundchange(int change)
    {
        this.change=change;
        notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        holder.tv_cg_name.setText(cglist.get(position).getPatient_fname()+" "+cglist.get(position).getPatient_lname());

        if (change==1){
            holder.tv_cg_name.setBackground(context.getResources().getDrawable(R.drawable.black_border_text));
            holder.tv_cg_name.setTextColor(context.getResources().getColor(R.color.blue));
        }
        holder.tv_cg_name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                holder.tv_cg_name.setBackground(context.getResources().getDrawable(R.drawable.select_cg));
                holder.tv_cg_name.setTextColor(context.getResources().getColor(R.color.white));
                ((SalesActivity)context).setCareGiver(cglist.get(position));
            }
        });
    }

    @Override
    public int getItemCount() {
        return cglist.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_cg_name;
        private ImageView iv_icon;

        public MyViewHolder(View view) {
            super(view);
            tv_cg_name = itemView.findViewById(R.id.tv_cg_name);

        }
    }


}
