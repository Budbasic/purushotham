package com.budbasic.posconsole.adapter;

import android.app.Dialog;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.budbasic.posconsole.R;
import com.budbasic.posconsole.reception.ProductsFragment;
import com.budbasic.posconsole.reception.fragment.PrintPageFragment;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DecodeFormat;
import com.bumptech.glide.request.RequestOptions;
import com.kotlindemo.model.GetData;
import com.kotlindemo.model.GetProducts;
import com.kotlindemo.model.GetSalesProduct;

import java.util.ArrayList;
import java.util.List;

public class ProductprintAdapter extends RecyclerView.Adapter<ProductprintAdapter.MyViewHolder> implements Filterable {


    private Context context;
    List<GetProducts> cardlist;
    List<GetProducts> filtercardlist;
    String image_path;
    private String Currency_value;
    PrintPageFragment printPageFragment;
    Dialog dialog;


    public ProductprintAdapter(Context context, ArrayList<GetProducts> productlist, PrintPageFragment printPageFragment, Dialog dialog) {
        this.context=context;
        this.cardlist=productlist;
        this.filtercardlist=productlist;
        this.printPageFragment=printPageFragment;
        this.dialog=dialog;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.product_item_dialog, parent, false);

        return new MyViewHolder(itemView);
    }


    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        holder.tv_text.setText(""+filtercardlist.get(position).getPacket_name());
        holder.tv_catamt.setText(""+filtercardlist.get(position).getPacket_barcode());
        holder.layout_product.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                printPageFragment.showlabledialog(filtercardlist.get(position),dialog);
            }
        });
    }

    @Override
    public int getItemCount() {
        return filtercardlist.size();
    }

    public void setRefresh(ArrayList<GetProducts> productlist) {
        this.cardlist=productlist;
        this.filtercardlist=productlist;
        notifyDataSetChanged();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_text, tv_catamt;
        private LinearLayout layout_product;

        public MyViewHolder(View view) {
            super(view);
             tv_text = itemView.findViewById(R.id.tv_text);
            tv_catamt = itemView.findViewById(R.id.tv_catamt);
            layout_product = itemView.findViewById(R.id.layout_product);

        }
    }
    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    filtercardlist = cardlist;
                } else {
                    List<GetProducts> filteredList = new ArrayList<>();
                    for (GetProducts row : cardlist) {

                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                       // System.out.println("aaaaaaaaaaaaa  search  "+row.getProduct_name()+"   "+charString.toString());
                        if (row.getPacket_name().toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(row);
                        }
                    }

                    filtercardlist = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = filtercardlist;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                filtercardlist = (ArrayList<GetProducts>) filterResults.values;
                System.out.println("aaaaaaaaaaaaa  size  "+filtercardlist.size());
                notifyDataSetChanged();
            }
        };
    }

    public interface ContactsAdapterListener {
        void onContactSelected(GetSalesProduct contact);
    }

}
