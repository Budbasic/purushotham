package com.budbasic.posconsole.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.budbasic.posconsole.R;
import com.budbasic.posconsole.reception.EmployeeInvoiceDetails;
import com.budbasic.posconsole.reception.ReceptionSalesPatientActivity;
import com.kotlindemo.model.GetInvoiceData;
import com.kotlindemo.model.GetInvoiceDetail;
import com.kotlindemo.model.GetInvoiceOrders;
import com.kotlindemo.model.GetPatient;

import java.util.ArrayList;
import java.util.List;

public class InvoiceAdapter extends RecyclerView.Adapter<InvoiceAdapter.MyViewHolder> {

    private List<GetInvoiceDetail> invoicelist;
    private Context context;
    private String currencyvalue;
    GetInvoiceData getinvoice;

    public InvoiceAdapter(Context context, ArrayList<GetInvoiceDetail> moviesList, String currency_value,GetInvoiceData getinvoice) {
        this.invoicelist = moviesList;
        this.context = context;
        this.currencyvalue = currency_value;
        this.getinvoice = getinvoice;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.employee_cart_item, parent, false);

        return new MyViewHolder(itemView);
    }


    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {

        holder.tv_name.setText(invoicelist.get(position).getProduct_name());
        holder.tv_phonenumber.setText(""+currencyvalue+""+invoicelist.get(position).getInvoice_detail_total_price());
        holder.tv_mobilenumber.setText(invoicelist.get(position).getInvoice_detail_qty()+" "+invoicelist.get(position).getUnit_short_name());
        if (invoicelist.get(position).getInvoice_detail_void().equalsIgnoreCase("0")){
            holder.tv_void.setText("Void");
        }else {
            holder.tv_void.setText("Voided");
        }

        if (!getinvoice.getInvoice_void().isEmpty()){
            holder.layout_void.setVisibility(View.INVISIBLE);
        }else {
            holder.layout_void.setVisibility(View.VISIBLE);
        }
        holder.layout_void.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (invoicelist.get(position).getInvoice_detail_void().equalsIgnoreCase("0")){
                    ((EmployeeInvoiceDetails)context).setVoid(invoicelist.get(position));
                }else {

                }

            }
        });


    }

    @Override
    public int getItemCount() {
        return invoicelist.size();
    }

    public void setDatachanged(ArrayList<GetInvoiceDetail> invoicelist, GetInvoiceData getinvoice) {
        this.invoicelist=invoicelist;
        this.getinvoice=getinvoice;
        notifyDataSetChanged();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_name, tv_mobilenumber,tv_loyaltygroup,tv_close,tv_phonenumber,tv_void;
        private LinearLayout layout_void;

        public MyViewHolder(View view) {
            super(view);
            tv_name = (TextView) view.findViewById(R.id.tv_name);
            tv_mobilenumber = (TextView) view.findViewById(R.id.tv_mobilenumber);
            tv_loyaltygroup =  view.findViewById(R.id.tv_loyaltygroup);
            tv_close =  view.findViewById(R.id.tv_close);
            tv_phonenumber =  view.findViewById(R.id.tv_phonenumber);
            layout_void =  view.findViewById(R.id.layout_void);
            tv_void =  view.findViewById(R.id.tv_void);
        }
    }



}
