package com.budbasic.posconsole.adapter

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.*
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.budbasic.customer.global.Preferences
import com.budbasic.global.CUC
import com.budbasic.posconsole.R
import com.budbasic.posconsole.webservice.RequetsInterface
import com.kotlindemo.model.PatientDocumentModel
import com.squareup.picasso.Picasso
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class DocumentShowAdapter(val activity: Activity, val document_path: String, val mcontext: Context, var data: ArrayList<PatientDocumentModel>) : RecyclerView.Adapter<DocumentShowAdapter.Holder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
        return Holder(LayoutInflater.from(mcontext).inflate(R.layout.document_list_show_item_view, parent, false))
    }

    override fun getItemCount(): Int {
        return data.size
    }

    override fun onBindViewHolder(holder: Holder, position: Int) {
        holder.tv_name.text = data[position].document_title

        /*holder.llDelete.setOnClickListener {
            apiCallForEmployeeDocument(data[position].document_id)
        }*/

        /*if (data[position].document_title.equals("No data found", true)) {
            holder.llDelete.visibility = View.GONE
            holder.llView.visibility = View.GONE
        } else {
           // holder.llDelete.visibility = View.VISIBLE
            holder.llView.visibility = View.VISIBLE
        }*/

        holder.llView.setOnClickListener {
            val dialog = Dialog(activity, R.style.TransparentProgressDialog)
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
            dialog.setContentView(R.layout.zoom_image_dialog)
            dialog.show()
            val iv_img_zoom = dialog.findViewById<ImageView>(R.id.iv_img_zoom)
            val iv_zoomclose = dialog.findViewById<ImageView>(R.id.iv_zoomclose)
            try {

                Picasso.with(activity)
                        .load(document_path + data[position].document_path)
                        .placeholder(R.drawable.ic_camera)
                        .error(R.drawable.ic_camera)
                        .into(iv_img_zoom)

            } catch (e: Exception) {

                e.printStackTrace()
            }


            iv_zoomclose.setOnClickListener(View.OnClickListener { dialog.dismiss() })

            dialog.setCancelable(true)
            dialog.setCanceledOnTouchOutside(true)
        }

    }


    inner class Holder(v: View) : RecyclerView.ViewHolder(v) {
        val tv_name: TextView = v.findViewById(R.id.tv_name)
        //val llDelete: TextView = v.findViewById(R.id.llDelete)
        val llView: TextView = v.findViewById(R.id.llView)
    }


    /*private fun apiCallForEmployeeDocument(document_id: String) {
        var mDialog = Dialog(mcontext)

        try {
            if (!activity!!.isFinishing) {
                mDialog = CUC.createDialog(activity!!)
                mDialog.show()
                val apiService = RequetsInterface.Factory.create()
                val parameterMap = HashMap<String, String>()

                parameterMap["api_key"] = "${Preferences.getPreference(mcontext!!, "API_KEY")}"
                parameterMap["employee_id"] = employee_id
                parameterMap["patient_id"] = patient_id
                parameterMap["document_id"] = document_id

                CompositeDisposable().add(apiService.callEmployeePatientDeleteDocument(parameterMap)
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.io())
                        .subscribe({ result ->
                            if (mDialog != null && mDialog.isShowing)
                                mDialog.dismiss()
                            if (result?.documents?.size!! > 0) {
                                data.clear()
                                data.addAll(result.documents)
                                notifyDataSetChanged()

                            } else {
                                data.clear()
                                data.add(PatientDocumentModel("", "", "No data found", "", "", "", "", ""))
                                notifyDataSetChanged()
                            }

                        }, { error ->
                            if (mDialog != null && mDialog.isShowing)
                                mDialog.dismiss()
                            CUC.displayToast(mcontext!!, "0", mcontext.getString(R.string.connection_to_database_failed))
                            error.printStackTrace()
                        })
                )
            }
        } catch (e: Exception) {
            if (mDialog != null && mDialog.isShowing) {
                mDialog.dismiss()
            }
            e.printStackTrace()
        }

    }*/
}

/*

final Dialog dialog = new Dialog(ProductDialogScreen.this, R.style.TransparentProgressDialog);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.zoom_image_dialog);
            dialog.getWindow().setLayout(Consts.width - 50, WindowManager.LayoutParams.WRAP_CONTENT);
            dialog.show();
            TouchImageView iv_img_zoom = ButterKnife.findById(dialog, R.id.iv_img_zoom);
            ImageView iv_zoomclose = ButterKnife.findById(dialog, R.id.iv_zoomclose);
            try {

                Picasso.with(ProductDialogScreen.this)
                        .load(mProduct_details.getMst_product_image())
                        .placeholder(R.drawable.loadinggray)
                        .error(R.drawable.loadinggray)
                        .into(iv_img_zoom);

            } catch (Exception e) {

                e.printStackTrace();
            }

            iv_zoomclose.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                }
            });

            dialog.setCancelable(true);
            dialog.setCanceledOnTouchOutside(true);
        }


 */
