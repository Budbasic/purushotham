package com.budbasic.posconsole.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.budbasic.posconsole.EmployeeInvoiceDetailsActivity;
import com.budbasic.posconsole.R;
import com.budbasic.posconsole.sales.UserInfoDetails;
import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.kotlindemo.model.Favourite;
import com.kotlindemo.model.GetEmployeeAppSetting;
import com.kotlindemo.model.GetEmployeeLogin;
import com.kotlindemo.model.GetPatient;
import com.kotlindemo.model.InvoiceHistory;

import java.util.ArrayList;

public class PattientFavouriteAdapter extends RecyclerView.Adapter<PattientFavouriteAdapter.MyViewHolder>  {

    private Context context;
    String patient_id;
    GetEmployeeAppSetting appSettingData;
    GetEmployeeLogin logindata;
    ArrayList<Favourite> favourites;


    public PattientFavouriteAdapter(Context userInfoDetails, String patient_id, GetEmployeeAppSetting appSettingData,
                                    GetEmployeeLogin logindata, ArrayList<Favourite> favourites) {
        this.context=userInfoDetails;
        this.patient_id=patient_id;
        this.appSettingData=appSettingData;
        this.logindata=logindata;
        this.favourites=favourites;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.sales_patient_detail_product_item_view, parent, false);

        return new MyViewHolder(itemView);
    }


    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
       holder.productName.setText(""+favourites.get(position).getPacket_name());
       holder.text_price.setText(favourites.get(position).getPacket_price());
        Glide.with(context).load(favourites.get(position).getPacket_image()).into(holder.imgProduct);
       holder.llMain.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {
               ((UserInfoDetails)context).setFavourite(favourites.get(position),position);
           }
       });
    }

    @Override
    public int getItemCount() {
        return favourites.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView productName, text_price,subCategoryName,aboutQuantity;
        private ImageView imgProduct;
        private LinearLayout llMain;

        public MyViewHolder(View view) {
            super(view);
            productName = (TextView) view.findViewById(R.id.productName);
            text_price = (TextView) view.findViewById(R.id.text_price);
            subCategoryName =  view.findViewById(R.id.subCategoryName);
            aboutQuantity =  view.findViewById(R.id.aboutQuantity);
            imgProduct =  view.findViewById(R.id.imgProduct);
            llMain =  view.findViewById(R.id.llMain);
        }
    }

    public interface ContactsAdapterListener {
        void onContactSelected(GetPatient contact);
    }

}
