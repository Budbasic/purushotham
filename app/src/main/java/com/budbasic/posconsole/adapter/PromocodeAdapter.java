package com.budbasic.posconsole.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.budbasic.posconsole.R;
import com.budbasic.posconsole.reception.ProductsFragment;
import com.budbasic.posconsole.reception.SalesActivity;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DecodeFormat;
import com.bumptech.glide.request.RequestOptions;
import com.kotlindemo.model.GetData;
import com.kotlindemo.model.GetPromosales;
import com.kotlindemo.model.GetSalesProduct;

import java.util.ArrayList;
import java.util.List;

public class PromocodeAdapter extends RecyclerView.Adapter<PromocodeAdapter.MyViewHolder> implements Filterable {
    Context context;
    ArrayList<GetPromosales> promocedelist;
    ArrayList<GetPromosales> filterpromocedelist;


    public PromocodeAdapter(Context context, ArrayList<GetPromosales> peomocedelist) {
        this.context=context;
        this.promocedelist=peomocedelist;
        this.filterpromocedelist=peomocedelist;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.promo_code_item, parent, false);

        return new MyViewHolder(itemView);
    }
    public void dataChanged(List<GetData> moviesList){

        notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {

           holder.tvPromoCodeName.setText(""+filterpromocedelist.get(position).getDiscount_name());
           holder.tvPromoCodeMsg.setText(""+filterpromocedelist.get(position).getDiscount_notification_msg());
           holder.tvPromoCodeApply.setOnClickListener(new View.OnClickListener() {
               @Override
               public void onClick(View view) {
                   ((SalesActivity)context).setapplyPromocard(promocedelist.get(position));
               }
           });
    }

    @Override
    public int getItemCount() {
        return filterpromocedelist.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tvPromoCodeName, tvPromoCodeMsg,tvPromoCodeApply,tv_catname;
        private LinearLayout layout_product;
        private ImageView img_cat;

        public MyViewHolder(View view) {
            super(view);
            tvPromoCodeName = itemView.findViewById(R.id.tvPromoCodeName);
            tvPromoCodeMsg = itemView.findViewById(R.id.tvPromoCodeMsg);
            img_cat = itemView.findViewById(R.id.img_cat);
            tvPromoCodeApply = itemView.findViewById(R.id.tvPromoCodeApply);

        }
    }
    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    filterpromocedelist = promocedelist;
                } else {
                    ArrayList<GetPromosales> filteredList = new ArrayList<>();
                    for (GetPromosales row : promocedelist) {

                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                       // System.out.println("aaaaaaaaaaaaa  search  "+row.getProduct_name()+"   "+charString.toString());
                        if (row.getDiscount_name().toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(row);
                        }
                    }

                    filterpromocedelist = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = filterpromocedelist;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                filterpromocedelist = (ArrayList<GetPromosales>) filterResults.values;
                System.out.println("aaaaaaaaaaaaa  size  "+promocedelist.size());
                notifyDataSetChanged();
            }
        };
    }

    public interface ContactsAdapterListener {
        void onContactSelected(GetSalesProduct contact);
    }

}
