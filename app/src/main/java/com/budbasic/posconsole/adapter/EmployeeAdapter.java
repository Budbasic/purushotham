package com.budbasic.posconsole.adapter;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableStringBuilder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;

import com.budbasic.global.CUC;
import com.budbasic.global.CUC.Companion;
import com.budbasic.posconsole.R;
import com.budbasic.posconsole.reception.ProductsFragment;
import com.budbasic.posconsole.reception.fragment.EmployeeManagerFragment;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DecodeFormat;
import com.bumptech.glide.request.RequestOptions;
import com.kotlindemo.model.GetData;
import com.kotlindemo.model.GetDrivers;
import com.kotlindemo.model.GetSalesProduct;

import net.cachapa.expandablelayout.ExpandableLayout;

import java.util.ArrayList;
import java.util.List;

public class EmployeeAdapter extends RecyclerView.Adapter<EmployeeAdapter.MyViewHolder> implements Filterable {


    private Context context;
    List<GetSalesProduct> cardlist;
    List<GetSalesProduct> filtercardlist;
    String image_path;
    ProductsFragment productsFragment;
    private String Currency_value;

    ArrayList<GetDrivers> employeelist;
    ArrayList<GetDrivers> filteremployeelist;
    EmployeeManagerFragment employeeManagerFragment;
    private int checkposition;
    
    public EmployeeAdapter(Context context, ArrayList<GetDrivers> employeelist, EmployeeManagerFragment employeeManagerFragment) {
        this.context=context;
        this.employeelist=employeelist;
        this.filteremployeelist=employeelist;
        this.employeeManagerFragment=employeeManagerFragment;
        
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.employee_item, parent, false);

        return new MyViewHolder(itemView);
    }
    public void dataChanged(List<GetData> moviesList){
        this.employeelist=employeelist;
        notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        holder.tv_name.setText(""+employeelist.get(position).getEmp_first_name()+" "+employeelist.get(position).getEmp_last_name());
        holder.tv_mobilenumber.setText(""+employeelist.get(position).getEmp_email());
        holder.tv_mobileapp.setText(""+employeelist.get(position).getEmp_mobile_number());
        holder.tv_licenseexpire.setText(""+employeelist.get(position).getEmp_licence_expire_date());
        holder.tv_drivinglicense.setText(""+employeelist.get(position).getEmp_driver_licence());
        holder.tv_joingdate.setText(""+employeelist.get(position).getEmp_joining_date());
        holder.tv_address.setText(""+employeelist.get(position).getEmp_address_one());
        holder.tv_city.setText(""+employeelist.get(position).getEmp_city());
        holder.tv_zipcode.setText(""+employeelist.get(position).getEmp_zipcode());
        holder.tv_state.setText(""+employeelist.get(position).getEmp_status());
        holder.tv_country.setText(""+employeelist.get(position).getEmp_country());

        holder.expandable_layout.setVisibility(View.GONE);

        holder.expandable_arrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (checkposition == position){
                    holder.expandable_layout.setExpanded(false);
                }else {
                    holder.expandable_layout.setExpanded(false);
                    holder.expandable_layout.setExpanded(true);
                }
            }
        }); holder.layout_click.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (checkposition == position){
                    holder.expandable_layout.setExpanded(false);
                }else {
                    holder.expandable_layout.setExpanded(false);
                    holder.expandable_layout.setExpanded(true);
                }
            }
        });
        holder.cv_change_pin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                employeeManagerFragment.setChangedpin(employeelist.get(position));
            }
        });
        holder.cv_change_password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                employeeManagerFragment.setChangepassword(employeelist.get(position));
            }
        });

        holder.pin_switch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                employeeManagerFragment.setREquirepin(employeelist.get(position));
            }
        });


    }

    @Override
    public int getItemCount() {
        return filteremployeelist.size();
    }

    public void setRefresh(ArrayList<GetDrivers> employeelist) {
        this.employeelist=employeelist;
        notifyDataSetChanged();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView tv_name,tv_mobileapp,tv_mobilenumber,tv_content,tv_drivinglicense,tv_licenseexpire,
                tv_joingdate,tv_address,tv_city,tv_zipcode,tv_country,tv_state;
        private ImageView expandable_arrow;
        private ExpandableLayout expandable_layout;
        private CardView cv_change_pin,cv_change_password;
        private Switch pin_switch;
        private LinearLayout layout_click;

        public MyViewHolder(View view) {
            super(view);
            tv_name=view.findViewById(R.id.tv_name);
            tv_mobileapp=view.findViewById(R.id.tv_mobileapp);
            tv_mobilenumber=view.findViewById(R.id.tv_mobilenumber);
            expandable_arrow=view.findViewById(R.id.expandable_arrow);
            expandable_layout=view.findViewById(R.id.expandable_layout);
            tv_content=view.findViewById(R.id.tv_content);
            cv_change_pin=view.findViewById(R.id.cv_change_pin);
            cv_change_password=view.findViewById(R.id.cv_change_password);
            pin_switch=view.findViewById(R.id.pin_switch);
            tv_drivinglicense=view.findViewById(R.id.tv_drivinglicense);
            tv_licenseexpire=view.findViewById(R.id.tv_licenseexpire);
            tv_joingdate=view.findViewById(R.id.tv_joingdate);
            tv_city=view.findViewById(R.id.tv_city);
            tv_address=view.findViewById(R.id.tv_address);
            tv_zipcode=view.findViewById(R.id.tv_zipcode);
            tv_country=view.findViewById(R.id.tv_country);
            tv_state=view.findViewById(R.id.tv_state);
            layout_click=view.findViewById(R.id.layout_click);


        }
    }
    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    filtercardlist = cardlist;
                } else {
                    List<GetSalesProduct> filteredList = new ArrayList<>();
                    for (GetSalesProduct row : cardlist) {

                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                       // System.out.println("aaaaaaaaaaaaa  search  "+row.getProduct_name()+"   "+charString.toString());
                        if (row.getPacket_name().toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(row);
                        }
                    }

                    filtercardlist = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = filtercardlist;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                filtercardlist = (ArrayList<GetSalesProduct>) filterResults.values;
                System.out.println("aaaaaaaaaaaaa  size  "+filtercardlist.size());
                notifyDataSetChanged();
            }
        };
    }

    public interface ContactsAdapterListener {
        void onContactSelected(GetSalesProduct contact);
    }

}
