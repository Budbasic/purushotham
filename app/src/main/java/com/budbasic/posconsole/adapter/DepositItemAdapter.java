package com.budbasic.posconsole.adapter;

import android.app.Dialog;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.budbasic.posconsole.R;
import com.budbasic.posconsole.reception.DepositFragment;
import com.budbasic.posconsole.reception.ExpensesFragment;
import com.kotlindemo.model.GetDepositItems;
import com.kotlindemo.model.GetDepositbanklist;
import com.kotlindemo.model.GetExpensesItems;

import java.util.ArrayList;

public class DepositItemAdapter extends RecyclerView.Adapter<DepositItemAdapter.MyViewHolder> {


    private Context context;
    ArrayList<GetDepositbanklist> getExpensesLists;
    Dialog dialog;
    DepositFragment expensesFragment;

    public DepositItemAdapter(Context context, ArrayList<GetDepositbanklist> getExpensesLists, Dialog dialog, DepositFragment expensesFragment) {
        this.context = context;
        this.getExpensesLists = getExpensesLists;
        this.dialog = dialog;
        this.expensesFragment = expensesFragment;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.expansesitems, parent, false);

        return new MyViewHolder(itemView);
    }


    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {

        holder.tv_text.setText(getExpensesLists.get(position).getDeposit());

        holder.tv_text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                expensesFragment.setexpansesItem(getExpensesLists.get(position));
               // ((AddExpensesActivity)context).setexpansesItem(getExpensesLists.get(position));
            }
        });
    }

    @Override
    public int getItemCount() {
        return getExpensesLists.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_text;


        public MyViewHolder(View view) {
            super(view);
            tv_text = itemView.findViewById(R.id.tv_text);

        }
    }


}
