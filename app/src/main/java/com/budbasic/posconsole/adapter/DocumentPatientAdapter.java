package com.budbasic.posconsole.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.budbasic.posconsole.R;
import com.budbasic.posconsole.reception.PatientEdit;
import com.budbasic.posconsole.reception.ReceptionSalesPatientActivity;
import com.kotlindemo.model.DocumentModel;
import com.kotlindemo.model.GetEmployeeAppSetting;
import com.kotlindemo.model.GetPatient;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class DocumentPatientAdapter extends RecyclerView.Adapter<DocumentPatientAdapter.MyViewHolder>  {


    private Context context;
    ArrayList<DocumentModel> documentlist;
    GetEmployeeAppSetting mBAsis;
    String documentpath;

    public DocumentPatientAdapter(Context context, ArrayList<DocumentModel> moviesList, GetEmployeeAppSetting mBAsis, String documentpath) {
        this.documentlist = moviesList;
        this.context = context;
        this.mBAsis = mBAsis;
        this.documentpath = documentpath;
    }



    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.document_list_item_view_new, parent, false);

        return new MyViewHolder(itemView);
    }


    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {

        holder.tv_name.setText(""+documentlist.get(position).getDocument_title());
        if (documentlist.get(position).getDocument_status().equalsIgnoreCase("0")){
            holder.llDelete.setVisibility(View.GONE);
        }else {
            holder.llDelete.setVisibility(View.VISIBLE);
        }

        System.out.println("aaaaaaaaa  image  "+documentpath+" "+documentlist.get(position));
        try {
            Picasso.with(context)
                    .load(documentpath + documentlist.get(position).getDocument_path())
                    .placeholder(R.drawable.no_image_available)
                    .error(R.drawable.no_image_available)
                    .into(holder.llView);

        } catch (Exception e) {

            e.printStackTrace();
        }

        holder.llView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((PatientEdit)context).setImageSee(documentlist.get(position));
            }
        });
        holder.llDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((PatientEdit)context).setDocumentDelete(documentlist.get(position));
            }
        });

    }

    @Override
    public int getItemCount() {
        return documentlist.size();
    }

    public void setRefresh(ArrayList<DocumentModel> documentlist, String documentpath) {
        this.documentlist=documentlist;
        this.documentpath=documentpath;
        notifyDataSetChanged();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_name;
        private ImageView llView,llDelete;

        public MyViewHolder(View view) {
            super(view);
            tv_name = (TextView) view.findViewById(R.id.tv_name);
            llView =  view.findViewById(R.id.llView);
            llDelete =  view.findViewById(R.id.llDelete);
        }
    }


}
