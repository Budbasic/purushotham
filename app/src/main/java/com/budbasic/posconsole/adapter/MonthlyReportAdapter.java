package com.budbasic.posconsole.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.budbasic.posconsole.R;
import com.budbasic.posconsole.reception.ReceptionSalesPatientActivity;
import com.kotlindemo.model.GetMonthlyreport;
import com.kotlindemo.model.GetPatient;

import java.util.ArrayList;
import java.util.List;

public class MonthlyReportAdapter extends RecyclerView.Adapter<MonthlyReportAdapter.MyViewHolder>  {

    ArrayList<GetMonthlyreport> monthlyreports;
    private Context context;


    public MonthlyReportAdapter(Context receptionSalesPatientActivity, ArrayList<GetMonthlyreport> moviesList) {
        this.monthlyreports = moviesList;
        this.context = receptionSalesPatientActivity;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.monthly_report, parent, false);

        return new MyViewHolder(itemView);
    }


    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {

        holder.tv_opening_balance.setText("$"+monthlyreports.get(position).getOpeningbalance());
        holder.tv_month.setText(monthlyreports.get(position).getMonth_year());
        holder.tv_ccmonthd.setText("$"+monthlyreports.get(position).getCredit_cash_month_date());
        holder.tv_grossrevenue.setText("$"+monthlyreports.get(position).getCash_transactions_revenue());
        holder.tv_cashbalance.setText("$"+monthlyreports.get(position).getTotal_sales_cash_balance());
        holder.tv_ctrevenue.setText("$"+monthlyreports.get(position).getCash_transactions_revenue());
        holder.tv_cashexpense.setText("$"+monthlyreports.get(position).getExpenses());
        holder.tv_cashdeposit.setText("$"+monthlyreports.get(position).getCash_bank_deposit());
        holder.tv_cctr.setText("$"+monthlyreports.get(position).getCredit_card_txn());
        holder.tv_totalrevenue.setText("$"+monthlyreports.get(position).getTotal_net_sales_revenue());
        holder.tv_closebalance.setText("$"+monthlyreports.get(position).getClosingbalance());

    }

    @Override
    public int getItemCount() {
        return monthlyreports.size();
    }

    public void setRefresh(ArrayList<GetMonthlyreport> monthlyreports) {
        this.monthlyreports=monthlyreports;
        notifyDataSetChanged();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_opening_balance, tv_month,tv_ccmonthd,tv_grossrevenue,tv_cashbalance,tv_ctrevenue,tv_cashexpense,tv_cashdeposit,
                tv_cctr,tv_totalrevenue,tv_closebalance;
        private LinearLayout ll_view;

        public MyViewHolder(View view) {
            super(view);
            tv_opening_balance = (TextView) view.findViewById(R.id.tv_opening_balance);
            tv_month = (TextView) view.findViewById(R.id.tv_month);
            tv_ccmonthd = (TextView) view.findViewById(R.id.tv_ccmonthd);
            tv_grossrevenue = (TextView) view.findViewById(R.id.tv_grossrevenue);
            tv_cashbalance = (TextView) view.findViewById(R.id.tv_cashbalance);
            tv_ctrevenue = (TextView) view.findViewById(R.id.tv_ctrevenue);
            tv_cashexpense = (TextView) view.findViewById(R.id.tv_cashexpense);
            tv_cashdeposit = (TextView) view.findViewById(R.id.tv_cashdeposit);
            tv_cctr = (TextView) view.findViewById(R.id.tv_cctr);
            tv_totalrevenue = (TextView) view.findViewById(R.id.tv_totalrevenue);
            tv_closebalance = (TextView) view.findViewById(R.id.tv_closebalance);

            ll_view =  view.findViewById(R.id.ll_view);
        }
    }

}
