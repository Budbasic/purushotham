package com.budbasic.posconsole.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.budbasic.posconsole.R;
import com.budbasic.posconsole.driver.background.BackgroundLOcationActivity;
import com.budbasic.posconsole.sales.UserInfoDetails;
import com.google.gson.Gson;
import com.kotlindemo.model.GetEmployeeLogin;
import com.kotlindemo.model.GetStoreOrders;
import com.kotlindemo.model.notesdata;

import java.util.ArrayList;

public class OnLIneOrdersAdapter extends RecyclerView.Adapter<OnLIneOrdersAdapter.MyViewHolder> {


    private Context context;
    ArrayList<GetStoreOrders> orderslist;
    GetEmployeeLogin logindata;

    public OnLIneOrdersAdapter(Context context, ArrayList<GetStoreOrders> notes_data, GetEmployeeLogin logindata) {
        this.context=context;
        this.orderslist=notes_data;
        this.logindata=logindata;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.employee_order_item, parent, false);

        return new MyViewHolder(itemView);
    }
    public void dataChanged(ArrayList<GetStoreOrders> moviesList){
        this.orderslist=moviesList;
        notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        holder.tv_patient_name.setText(""+orderslist.get(position).getPatient_fname()+""+orderslist.get(position).getPatient_lname());
        holder.tv_mobile_no.setText(""+orderslist.get(position).getPatient_mobile_no());
        holder.tv_order_unique_no.setText(""+orderslist.get(position).getInvoice_order_unique_no());

        holder.tv_procced.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String myJson = new Gson().toJson(logindata);
                Intent i=new Intent(context, BackgroundLOcationActivity.class);
                i.putExtra("logindata",myJson);
                context.startActivity(i);
            }
        });
    }

    @Override
    public int getItemCount() {
        return orderslist.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_patient_name,tv_mobile_no,tv_order_unique_no,tv_procced;

        public MyViewHolder(View view) {
            super(view);
            tv_patient_name = itemView.findViewById(R.id.tv_patient_name);
            tv_mobile_no = itemView.findViewById(R.id.tv_mobile_no);
            tv_order_unique_no = itemView.findViewById(R.id.tv_order_unique_no);
            tv_procced = itemView.findViewById(R.id.tv_procced);


        }
    }


}
