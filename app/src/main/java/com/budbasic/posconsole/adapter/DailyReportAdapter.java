package com.budbasic.posconsole.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.budbasic.posconsole.R;
import com.budbasic.posconsole.reception.DailyReportActivity;
import com.budbasic.posconsole.reception.ReceptionSalesPatientActivity;
import com.google.gson.Gson;
import com.kotlindemo.model.GetDailyReport;
import com.kotlindemo.model.GetPatient;

import java.util.ArrayList;
import java.util.List;

public class DailyReportAdapter extends RecyclerView.Adapter<DailyReportAdapter.MyViewHolder>  {


    private Context context;
    ArrayList<GetDailyReport> dialyreports;

    public DailyReportAdapter(Context receptionSalesPatientActivity, ArrayList<GetDailyReport> dialyreports) {
        this.dialyreports = dialyreports;
        this.context = receptionSalesPatientActivity;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.dialyreport, parent, false);

        return new MyViewHolder(itemView);
    }


    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
       holder.tv_text.setText(""+dialyreports.get(position).getJdate());

        holder.ll_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String myJson = new Gson().toJson(dialyreports.get(position));
                Intent i=new Intent(context, DailyReportActivity.class);
                i.putExtra("dailyexp",myJson);
                context.startActivity(i);
            }
        });
    }

    @Override
    public int getItemCount() {
        return dialyreports.size();
    }

    public void setRefresh(ArrayList<GetDailyReport> dialyreports) {
        notifyDataSetChanged();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_text, p_tag;
        private LinearLayout ll_view;

        public MyViewHolder(View view) {
            super(view);
            tv_text = (TextView) view.findViewById(R.id.tv_text);
            p_tag = (TextView) view.findViewById(R.id.tv_texttag);
            ll_view =  view.findViewById(R.id.ll_view);
        }
    }

}
