package com.budbasic.posconsole.adapter

import android.app.Activity
import android.content.Intent
import android.support.v7.app.AlertDialog
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.budbasic.customer.global.Preferences
import com.budbasic.global.CUC
import com.budbasic.posconsole.R
import com.budbasic.posconsole.sales.SalesProductListFragment
import com.budbasic.posconsole.webservice.RequetsInterface
import com.bumptech.glide.Glide
import com.kotlindemo.model.Favourite
import com.kotlindemo.model.GetEmployeeAppSetting
import com.kotlindemo.model.GetEmployeeLogin
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class PatientFavouritProductAdapter(val activity: Activity, val patient_id: String?, val appSettingData: GetEmployeeAppSetting?, val logindata: GetEmployeeLogin?, var data: ArrayList<Favourite>) : RecyclerView.Adapter<PatientFavouritProductAdapter.Holder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
        return Holder(LayoutInflater.from(activity).inflate(R.layout.sales_patient_detail_product_item_view, parent, false))
    }

    override fun getItemCount(): Int {
        return data.size
    }

    override fun onBindViewHolder(holder: Holder, position: Int) {
        holder.productName.text = data[position].packet_name
      //  holder.subCategoryName.text = data[position].product_category_id


        //var date = CUC.getdatefromoneformattoAnother("yyyy-MM-dd HH:mm:ss", "MM-dd-yyyy", "${data[position].invoice_created_date}")
        //if (date.isNotEmpty()) {

        holder.itemView.setOnClickListener {


            val builder = AlertDialog.Builder(activity)
            builder.setMessage("Are you sure want add to cart")

            builder.setPositiveButton("Yes") { dialog, which ->
                dialog.dismiss()
                callEmployeePatientCart(data[position].packet_id, data[position].packet_category_id)
            }

            builder.setNegativeButton("No") { dialog, which -> dialog.dismiss() }
            val alert = builder.create()
            alert.show()


        }
        holder.text_price.text = data[position].packet_price
        Glide.with(activity).load(data[position].packet_image).into(holder.imgProduct)
        //}

    }


    inner class Holder(v: View) : RecyclerView.ViewHolder(v) {
        val productName: TextView = v.findViewById(R.id.productName)
        val subCategoryName: TextView = v.findViewById(R.id.subCategoryName)
        val text_price: TextView = v.findViewById(R.id.text_price)
        val imgProduct: ImageView = v.findViewById(R.id.imgProduct)
    }


    fun callEmployeePatientCart(product_id: String?, category_id: String?) {
        if (CUC.isNetworkAvailablewithPopup(activity)) {
            val mDialog = CUC.createDialog(activity)
            val apiService = RequetsInterface.Factory.create()
            val parameterMap = HashMap<String, String>()
            parameterMap["api_key"] = "${Preferences.getPreference(activity, "API_KEY")}"
            if (logindata != null) {
                parameterMap["store_id"] = logindata.user_store_id
                parameterMap["employee_id"] = logindata.user_id
            }
            parameterMap["patient_id"] = SalesProductListFragment.queue_patient_id
            parameterMap["product_id"] = "$product_id"
            parameterMap["category_id"] = "$category_id"
            parameterMap["qty"] = "1"
            parameterMap["package_id"] = ""
            parameterMap["terminal_id"] = "${appSettingData?.terminal_id}"
            parameterMap["emp_lat"] = "${Preferences.getPreference(activity, "latitude")}"
            parameterMap["emp_log"] = "${Preferences.getPreference(activity, "longitude")}"
            parameterMap["queue_id"] = "${SalesProductListFragment.queue_id}"
            Log.i("Result", "parameterMap : $parameterMap")
            /*{store_id=1, emp_log=72.53353, category_id=1, emp_lat=23.04786, api_key=b599961fda5dcd54dea3b45b74a5943a, patient_id=65, employee_id=2, product_id=Designer Cookies, qty=1, terminal_id=127, queue_id=256}*/
            CompositeDisposable().add(apiService.callEmployeeSalesAddcart(parameterMap)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe({ result ->
                        if (mDialog != null && mDialog.isShowing)
                            mDialog.dismiss()
                        if (result != null) {
                            Log.i("Result", "" + result.toString())
                            if (result.status == "0") {
                                //Preferences.setPreference(this@EmployeeProductDetails, "total_qty", "${result.total_qty}")
                                val i = Intent("android.intent.action.MAINQTY")
                                activity!!.sendBroadcast(i)
                                val sendOrder = Intent("android.intent.action.MAINORD")
                                activity!!.sendBroadcast(sendOrder)

                                CUC.displayToast(activity!!, result.show_status, result.message)
                            } else if (result.status == "10") {

                            } else {
                                CUC.displayToast(activity!!, result.show_status, result.message)
                            }

                        } else {

                        }

                    }, { error ->
                        if (mDialog != null && mDialog.isShowing)
                            mDialog.dismiss()
                        CUC.displayToast(activity!!, "0", activity.getString(R.string.connection_to_database_failed))
                        error.printStackTrace()
                    })
            )
        }
    }

    /*fun callEmployeePatientUpdatecart(product_id: String) {
        if (CUC.isNetworkAvailablewithPopup(activity)) {
            var mDialog = CUC.createDialog(activity)
            val apiService = RequetsInterface.Factory.create()
            val parameterMap = HashMap<String, String>()
            parameterMap["api_key"] = "${Preferences.getPreference(activity, "API_KEY")}"
            //parameterMap["cart_detail_id"] = "$editId"
            if (logindata != null) {
                parameterMap["store_id"] = logindata.user_store_id
                parameterMap["employee_id"] = logindata.user_id
            }
            parameterMap["patient_id"] = patient_id
            parameterMap["product_id"] = "$product_id"
            parameterMap["category_id"] = "$category_id"
            parameterMap["package_id"] = "$package_id"
            parameterMap["qty"] = "$changeQty"
            parameterMap["terminal_id"] = "${mAppBasic.terminal_id}"
            parameterMap["emp_lat"] = "${Preferences.getPreference(this@SalesCategoryActivity, "latitude")}"
            parameterMap["emp_log"] = "${Preferences.getPreference(this@SalesCategoryActivity, "longitude")}"
            parameterMap["queue_id"] = "$queue_id"

            Log.i("Result", "parameterMap : $parameterMap")
            CompositeDisposable().add(apiService.callEmployeeSalesUpdatecart(parameterMap)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe({ result ->
                        if (mDialog != null && mDialog.isShowing)
                            mDialog.dismiss()
                        Log.i("Result", "" + result.toString())
                        if (result != null) {
                            if (result.status == "0") {
                                changeQty = ""
                                editId = ""
                                category_id = ""
                                package_id = ""
                                category_id = ""
//                                if (productQtyDialog != null && productQtyDialog.isShowing) {
//                                    productQtyDialog.dismiss()
//                                }
                                if (dialogProductQty != null && dialogProductQty.isShowing) {
                                    dialogProductQty.dismiss()
                                }
                                callEmployeePatientCart()
                                CUC.displayToast(this@SalesCategoryActivity, result.show_status, result.message)
                            } else if (result.status == "10") {
                                apiClass!!.callApiKey(EMPLOYEEPATIENTUPDATECART)
                            } else {
                                CUC.displayToast(this@SalesCategoryActivity, result.show_status, result.message)
                            }
                        } else {
                            CUC.displayToast(this@SalesCategoryActivity, "0", getString(R.string.connection_to_database_failed))
                        }
                    }, { error ->
                        if (mDialog != null && mDialog.isShowing)
                            mDialog.dismiss()
                        CUC.displayToast(this@SalesCategoryActivity, "0", getString(R.string.connection_to_database_failed))
                        error.printStackTrace()
                    })
            )
        }
    }*/
}