package com.budbasic.posconsole.adapter;

import android.app.Dialog;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.budbasic.posconsole.R;
import com.budbasic.posconsole.reception.ExpensesFragment;
import com.kotlindemo.model.GetEmployeeExpanses;
import com.kotlindemo.model.GetExpensesItems;

import java.util.ArrayList;
import java.util.List;

public class ExpenseCashDrawerAdapter extends RecyclerView.Adapter<ExpenseCashDrawerAdapter.MyViewHolder> {


    private Context context;
    List<GetEmployeeExpanses> getExpensesLists;
    Dialog dialog;


    public ExpenseCashDrawerAdapter(Context context, List<GetEmployeeExpanses> getEmployeeExpansesList) {
        this.context=context;
        this.getExpensesLists=getEmployeeExpansesList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.expenses_cash_drawer, parent, false);

        return new MyViewHolder(itemView);
    }


    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {

        holder.tv_name.setText(getExpensesLists.get(position).getExpensename());
        holder.tv_description.setText(getExpensesLists.get(position).getExpensedescription());
        holder.tv_amount.setText(getExpensesLists.get(position).getAmount());


    }

    @Override
    public int getItemCount() {
        return getExpensesLists.size();
    }

    public void setRefresh(List<GetEmployeeExpanses> getEmployeeExpansesList) {
        this.getExpensesLists=getEmployeeExpansesList;
        notifyDataSetChanged();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_name,tv_description,tv_amount;


        public MyViewHolder(View view) {
            super(view);
            tv_name = itemView.findViewById(R.id.tv_name);
            tv_description = itemView.findViewById(R.id.tv_description);
            tv_amount = itemView.findViewById(R.id.tv_amount);

        }
    }


}
