package com.budbasic.posconsole.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.budbasic.posconsole.R;
import com.budbasic.posconsole.reception.ReceptionSalesPatient;
import com.budbasic.posconsole.reception.ReceptionSalesPatientActivity;
import com.kotlindemo.model.GetPatient;

import java.util.ArrayList;
import java.util.List;

public class PatientAdapter extends RecyclerView.Adapter<PatientAdapter.MyViewHolder> implements Filterable {

    private List<GetPatient> patientlist;
    private List<GetPatient> filterpatientlist;
    private Context context;


    public PatientAdapter(Context receptionSalesPatientActivity, List<GetPatient> moviesList) {
        this.patientlist = moviesList;
        this.filterpatientlist = moviesList;
        this.context = receptionSalesPatientActivity;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.product_search_item, parent, false);

        return new MyViewHolder(itemView);
    }
    public void dataChanged(List<GetPatient> moviesList){
        this.patientlist=moviesList;
        this.filterpatientlist=moviesList;
        notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
       holder.p_name.setText("Patient Name : "+filterpatientlist.get(position).getPatientname());
       holder.p_tag.setText("Patient Name : "+filterpatientlist.get(position).getPatient_licence_no());
       holder.ll_itemamin.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {
               ((ReceptionSalesPatientActivity)context).setSendData(filterpatientlist.get(position));
           }
       });
    }

    @Override
    public int getItemCount() {
        return filterpatientlist.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView p_name, p_tag;
        private LinearLayout ll_itemamin;

        public MyViewHolder(View view) {
            super(view);
            p_name = (TextView) view.findViewById(R.id.tv_text);
            p_tag = (TextView) view.findViewById(R.id.tv_texttag);
            ll_itemamin =  view.findViewById(R.id.ll_itemamin);
        }
    }
    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    filterpatientlist = patientlist;
                } else {
                    List<GetPatient> filteredList = new ArrayList<>();
                    for (GetPatient row : patientlist) {

                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        if (row.getPatient_licence_no().toLowerCase().contains(charString.toLowerCase()) || row.getPatientname().toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(row);
                        }
                    }

                    filterpatientlist = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = filterpatientlist;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                filterpatientlist = (ArrayList<GetPatient>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    public interface ContactsAdapterListener {
        void onContactSelected(GetPatient contact);
    }

}
