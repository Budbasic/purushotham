package com.budbasic.posconsole.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.budbasic.posconsole.R;
import com.budbasic.posconsole.reception.ProductsFragment;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DecodeFormat;
import com.bumptech.glide.request.RequestOptions;
import com.kotlindemo.model.GetData;
import com.kotlindemo.model.GetSalesProduct;

import java.util.ArrayList;
import java.util.List;

public class FinancialAdapter extends RecyclerView.Adapter<FinancialAdapter.MyViewHolder>{
    private Context context;
    ArrayList<String> financialList;

    public FinancialAdapter(Context context, ArrayList<String> financialList) {
        this.context = context;
        this.financialList=financialList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.financial_item, parent, false);

        return new MyViewHolder(itemView);
    }
    public void dataChanged(List<GetData> moviesList){
        notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        holder.tv_title.setText(financialList.get(position));

        holder.tv_title.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
    }

    @Override
    public int getItemCount() {
        return financialList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_title;
        private ImageView iv_icon;

        public MyViewHolder(View view) {
            super(view);
            tv_title = itemView.findViewById(R.id.tv_title);
            iv_icon = itemView.findViewById(R.id.iv_icon);



        }
    }


}
