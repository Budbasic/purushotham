package com.budbasic.posconsole.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.budbasic.posconsole.R;
import com.budbasic.posconsole.reception.AddExpensesActivity;
import com.budbasic.posconsole.reception.ExpensesFragment;
import com.budbasic.posconsole.reception.ProductsFragment;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DecodeFormat;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.kotlindemo.model.GetData;
import com.kotlindemo.model.GetEmpExpenses;
import com.kotlindemo.model.GetEmployeeExpanses;
import com.kotlindemo.model.GetEmployeeLogin;
import com.kotlindemo.model.GetSalesProduct;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class ExpensesAdapter extends RecyclerView.Adapter<ExpensesAdapter.MyViewHolder> {


    private Context context;
    List<GetEmployeeExpanses> getEmployeeExpansesList;
    GetEmployeeLogin login;
    private ExpensesFragment expensesFragment;

    public ExpensesAdapter(Context context, List<GetEmployeeExpanses> getEmpExpenses, GetEmployeeLogin logindata, ExpensesFragment expensesFragment) {
        this.context = context;
        this.getEmployeeExpansesList = getEmpExpenses;
        this.login=logindata;
        this.expensesFragment=expensesFragment;

    }



    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.expensevelist, parent, false);

        return new MyViewHolder(itemView);
    }
    public void dataChanged(List<GetData> moviesList){

        notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        holder.tv_expense_date.setText(getEmployeeExpansesList.get(position).getDates());
        holder.tv_expstore.setText(getEmployeeExpansesList.get(position).getCompany_name());
        holder.tv_exstore.setText(getEmployeeExpansesList.get(position).getStore_name());
        holder.tv_employee.setText(getEmployeeExpansesList.get(position).getEmp_first_name()+" "+getEmployeeExpansesList.get(position).getEmp_last_name());
        holder.tv_exitem.setText(getEmployeeExpansesList.get(position).getExpensename());
        holder.tv_amount.setText(getEmployeeExpansesList.get(position).getAmount());

        String[] spilt=getEmployeeExpansesList.get(position).getDates().split("-");
        Calendar calendar=Calendar.getInstance();
        int day=calendar.get(Calendar.DAY_OF_MONTH);
        int month=calendar.get(Calendar.MONTH);
        int year=calendar.get(Calendar.YEAR);
        if (getEmployeeExpansesList.get(position).getDates().equalsIgnoreCase(day+"-"+(month+1)+"-"+year)){
            holder.layout_currentdate.setVisibility(View.VISIBLE);
        }else {
            holder.layout_currentdate.setVisibility(View.INVISIBLE);
        }

        holder.tv_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                expensesFragment.setEdit(getEmployeeExpansesList.get(position));
                /*String getdata=new Gson().toJson(getEmployeeExpansesList.get(position));
                String logindata=new Gson().toJson(login);
                Intent i=new Intent(context, AddExpensesActivity.class);
                i.putExtra("position",1);
                i.putExtra("expansedata",getdata);
                i.putExtra("logindata",logindata);
                context.startActivity(i);*/
            }
        });
        holder.img_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                expensesFragment.setDelete(getEmployeeExpansesList.get(position));
            }
        });
    }

    @Override
    public int getItemCount() {
        return getEmployeeExpansesList.size();
    }

    public void setRefresh(List<GetEmployeeExpanses> result) {
        this.getEmployeeExpansesList=result;
        notifyDataSetChanged();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_expense_date, tv_expstore,tv_change_table,tv_exstore,tv_employee,tv_exitem,tv_amount,tv_edit;
        private LinearLayout layout_currentdate;
        private ImageView img_delete;

        public MyViewHolder(View view) {
            super(view);
            tv_expense_date = itemView.findViewById(R.id.tv_expense_date);
            tv_expstore = itemView.findViewById(R.id.tv_expstore);
            tv_change_table = itemView.findViewById(R.id.tv_change_table);
            tv_exstore = itemView.findViewById(R.id.tv_exstore);
            tv_employee = itemView.findViewById(R.id.tv_employee);
            tv_exitem = itemView.findViewById(R.id.tv_exitem);
            tv_amount = itemView.findViewById(R.id.tv_amount);
            layout_currentdate = itemView.findViewById(R.id.layout_currentdate);
            tv_edit = itemView.findViewById(R.id.tv_edit);
            img_delete = itemView.findViewById(R.id.img_delete);



        }
    }

}
