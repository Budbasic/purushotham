package com.budbasic.posconsole.webservice

import com.kotlindemo.model.*
import okhttp3.MultipartBody
import okhttp3.OkHttpClient
import okhttp3.RequestBody
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.http.*
import java.util.*
import java.util.concurrent.TimeUnit


/**
 * Created by ww21 on 19-12-2017.
 */


interface RequetsInterface {


    /**
     * Companion object for the factory
     */

    companion object Factory {
        private val REQUEST_TIMEOUT = 60
        var okHttpClient: OkHttpClient? = null
        fun create(): RequetsInterface {
            if (okHttpClient == null)
                initOkHttp()
            val retrofit = retrofit2.Retrofit.Builder()
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(retrofit2.converter.gson.GsonConverterFactory.create())
                    .client(okHttpClient)
                    //live link
                    //.baseUrl("https://pos.budbasic.com/apiemployee/")
                    .baseUrl("https://terminaldev.nonadyne.com/apiemployee/")
                    //demo link
                 //   .baseUrl("http://icpos.nonadyne.com/Apiemployee/")
                    //.baseUrl("http://ic.nonadyne.com/Apiemployee/")
                    // .baseUrl("http://icpos.nonadyne.com/beta/appapi/")
                    //.baseUrl("http://18.218.162.237/appapi/")
                    //.baseUrl("http://dailybills.in/projects/medicalpos/appapi/")
                    .build()

            //.client(okHttpClient)

            return retrofit.create(RequetsInterface::class.java)
        }

        private fun initOkHttp() {
//            val trustManagerFactory = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm())
//            trustManagerFactory.init(null as KeyStore?)
//            val trustManagers = trustManagerFactory.trustManagers
//            if (trustManagers.size != 1 || trustManagers[0] !is X509TrustManager) {
//                throw IllegalStateException("Unexpected default trust managers:" + Arrays.toString(trustManagers))
//            }
//            val trustManager = trustManagers[0] as X509TrustManager
//            val sslcontext = SSLContext.getInstance("TLSv1")
//            sslcontext.init(null, null, null)
//            val NoSSLv3Factory = NoSSLv3SocketFactory(sslcontext.socketFactory)
            val httpClient = OkHttpClient().newBuilder()
                    //.sslSocketFactory(NoSSLv3Factory, trustManager)
                    .connectTimeout(REQUEST_TIMEOUT.toLong(), TimeUnit.SECONDS)
                    .readTimeout(REQUEST_TIMEOUT.toLong(), TimeUnit.SECONDS)
                    .writeTimeout(REQUEST_TIMEOUT.toLong(), TimeUnit.SECONDS)

            val interceptor = HttpLoggingInterceptor()
            interceptor.level = HttpLoggingInterceptor.Level.BODY
            httpClient.addInterceptor(interceptor)
            httpClient.addInterceptor { chain ->
                val original = chain.request()
                val requestBuilder = original.newBuilder()
                        .addHeader("Accept", "application/json")
                        .addHeader("Content-Type", "application/json")
                val request = requestBuilder.build()
                chain.proceed(request)
            }
            okHttpClient = httpClient.build()
        }

        fun createGoogle(): RequetsInterface {

            val retrofit = retrofit2.Retrofit.Builder()
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(retrofit2.converter.gson.GsonConverterFactory.create())
                    .baseUrl("https://maps.googleapis.com/maps/api/directions/")
                    .build()

            return retrofit.create(RequetsInterface::class.java)
        }
    }

    /*
    * Get API KEY
    * */
    @POST("apikey")
    @FormUrlEncoded
    fun callGetApiKey(@FieldMap mJsonObject: HashMap<String, String>): io.reactivex.Observable<GetApiKey>

    /*
    * LOGIN
    * */
    @POST("employee_login")
    @FormUrlEncoded
    fun callGetEmployeeLogin(@FieldMap mJsonObject: HashMap<String, String>): io.reactivex.Observable<GetEmployeeLogin>

    /*
    * SEARCH PATIENT
    * */
    @POST("employee_search_patient")
    @FormUrlEncoded
    fun callGetEmployeeSearchPatient(@FieldMap mJsonObject: HashMap<String, String>): io.reactivex.Observable<GetEmployeeSearchPatient>

    /*
    * PRE CUSTOMER
    * */
    @POST("employee_pre_customer")
    @FormUrlEncoded
    fun callGetEmployeePreCustomer(@FieldMap mJsonObject: HashMap<String, String>): io.reactivex.Observable<GetEmployeePreCustomer>

    /*
    * LOGIN
    * */
    @POST("employee_details")
    @FormUrlEncoded
    fun callEmployeeDetails(@FieldMap mJsonObject: HashMap<String, String>): io.reactivex.Observable<GetEmployeeLogin>


    @POST("employee_create_patient")
    @FormUrlEncoded
    fun callGetEmployeeCreatePatient(@FieldMap mJsonObject: Map<String, String>): io.reactivex.Observable<GetStatus>


    @POST("employee_create_patient")
    @Multipart
    fun callGetEmployeeCreatePatientWithImage(@PartMap mJsonObject: Map<String, @JvmSuppressWildcards RequestBody>, @Part document_file: MultipartBody.Part, @Part("patient_image") document_image: RequestBody): io.reactivex.Observable<GetStatus>

  @POST("employee_photo_add")
    @Multipart
    fun callEmployeeAddphoto(@PartMap mJsonObject: Map<String, @JvmSuppressWildcards RequestBody>, @Part document_file: MultipartBody.Part, @Part("patient_image") document_image: RequestBody): io.reactivex.Observable<GetStatus>


    @POST("employee_orders")
    @FormUrlEncoded
    fun callGetEmployeeOrders(@FieldMap mJsonObject: Map<String, String>): io.reactivex.Observable<GetEmployeeOrders>

    @POST("employee_order_cart")
    @FormUrlEncoded
    fun callEmployeePatientCart(@FieldMap mJsonObject: HashMap<String, String>): io.reactivex.Observable<GetEmployeePatientCart>

    @POST("employee_accept_order")
    @FormUrlEncoded
    fun callGetEmployeeAcceptOrder(@FieldMap mJsonObject: HashMap<String, String>): io.reactivex.Observable<GetEmployeeQenerateInvoice>

    @POST("employee_location_update")
    @FormUrlEncoded
    fun callGetEmployeeLocationUpdate(@FieldMap mJsonObject: HashMap<String, String>): io.reactivex.Observable<GetStatus>

    @POST("employee_driver_order")
    @FormUrlEncoded
    fun callGetEmployeeDriverOrder(@FieldMap mJsonObject: HashMap<String, String>): io.reactivex.Observable<GetEmployeeDriverOrder>

    @POST("employee_driver_orders")
    @FormUrlEncoded
    fun callGetEmployeeDriverOrders(@FieldMap mJsonObject: HashMap<String, String>): io.reactivex.Observable<GetEmployeeDriverOrders>

    @POST("employee_driver_accept_delivery")
    @FormUrlEncoded
    fun callGetEmployeeDriverAcceptDelivery(@FieldMap mJsonObject: HashMap<String, String>): io.reactivex.Observable<GetStatus>

    @POST("employee_forgot_password")
    @FormUrlEncoded
    fun callEmployeeForgotPassword(@FieldMap mJsonObject: HashMap<String, String>): io.reactivex.Observable<GetStatus>

    @POST("employee_driver_cancel_delivery")
    @FormUrlEncoded
    fun callEmployeeDriverCancelDelivery(@FieldMap mJsonObject: HashMap<String, String>): io.reactivex.Observable<GetStatus>

    @POST("employee_driver_return_package")
    @FormUrlEncoded
    fun callEmployeeDriverReturnPackage(@FieldMap mJsonObject: HashMap<String, String>): io.reactivex.Observable<GetStatus>

    @POST("employee_sales_return_order")
    @FormUrlEncoded
    fun callEmployeeSalesReturnOrder(@FieldMap mJsonObject: HashMap<String, String>): io.reactivex.Observable<GetStatus>

    @POST("employee_app_setting")
    @FormUrlEncoded
    fun callEmployeeAppSetting(@FieldMap mJsonObject: HashMap<String, String>): io.reactivex.Observable<GetEmployeeAppSetting>

    @POST("employee_terminal_registration")
    @FormUrlEncoded
    fun callEmployeeTerminalRegistration(@FieldMap mJsonObject: HashMap<String, String>): io.reactivex.Observable<GetStatus>

    @POST("employee_driver_pickup_order")
    @FormUrlEncoded
    fun callEmployeeDriverPickupOrder(@FieldMap mJsonObject: HashMap<String, String>): io.reactivex.Observable<GetEmployeeDriverPickupOrder>

//    @POST("employee_driver_delivered_order")
//    @FormUrlEncoded
//    fun callEmployeeDriverDeliveredOrder(@FieldMap mJsonObject: HashMap<String, String>): io.reactivex.Observable<GetStatus>

    @POST("employee_driver_delivered_order")
    @Multipart
    fun callEmployeeDriverDeliveredOrder(@PartMap partMap: Map<String, @JvmSuppressWildcards RequestBody>, @Part part: MultipartBody.Part, @Part("digital_siganture") requestBody: RequestBody): io.reactivex.Observable<GetStatus>

    @POST("employee_city")
    @FormUrlEncoded
    fun callEmployeeCity(@FieldMap mJsonObject: HashMap<String, String>): io.reactivex.Observable<GetLocationData>

    @POST("employee_check_patient")
    @FormUrlEncoded
    fun callEmployeeCheckPatient(@FieldMap mJsonObject: HashMap<String, String>): io.reactivex.Observable<GetEmployeeCheckPatient>

    @POST("employee_queue_list")
    @FormUrlEncoded
    fun callEmployeeQueueList(@FieldMap mJsonObject: HashMap<String, String>): io.reactivex.Observable<GetEmployeeQueueList>

    @POST("appq_validate_queue")
    @FormUrlEncoded
    fun callAppqValidateQueue(@FieldMap mJsonObject: HashMap<String, String>): io.reactivex.Observable<GetAppqValidateQueue>

    @POST("employee_patient_assign_company")
    @FormUrlEncoded
    fun callEmployeePatientAssignCompany(@FieldMap mJsonObject: HashMap<String, String>): io.reactivex.Observable<GetEmployeeCheckPatient>

    @POST("employee_queue_close")
    @FormUrlEncoded
    fun callEmployeeQueueClose(@FieldMap mJsonObject: HashMap<String, String>): io.reactivex.Observable<GetStatus>

    @POST("appq_validate_queue_push")
    @FormUrlEncoded
    fun callAppqValidateQueuePush(@FieldMap mJsonObject: HashMap<String, String>): io.reactivex.Observable<GetEmployeeSalesCreateQueue>

    @POST("employee_sales_queue_patient")
    @FormUrlEncoded
    fun callEmployeeSalesQueuePatient(@FieldMap mJsonObject: HashMap<String, String>): io.reactivex.Observable<GetEmployeeSalesCreateQueue>

    @POST("appq_verify_dlicence")
    @FormUrlEncoded
    fun callAppqVerifyDlicence(@FieldMap mJsonObject: HashMap<String, String>): io.reactivex.Observable<GetStatus>

    @POST("appq_verify_dlicence")
    @FormUrlEncoded
    fun callGetPatientid(@FieldMap mJsonObject: HashMap<String, String>): io.reactivex.Observable<GetNotesData>


    @POST("appq_employee_tracking")
    @FormUrlEncoded
    fun callAppqEmployeeTracking(@FieldMap mJsonObject: HashMap<String, String>): io.reactivex.Observable<GetStatus>

    @POST("employee_cancel_approve")
    @FormUrlEncoded
    fun callEmployeeCancelApprove(@FieldMap mJsonObject: HashMap<String, String>): io.reactivex.Observable<GetStatus>

    @POST("employee_cancel_order")
    @FormUrlEncoded
    fun callEmployeeCancelOrder(@FieldMap mJsonObject: HashMap<String, String>): io.reactivex.Observable<GetStatus>

    @POST("employee_void_invoice")
    @FormUrlEncoded
    fun callEmployeeVoidOrder(@FieldMap mJsonObject: Map<String, String>): io.reactivex.Observable<GetStatus>

    @POST("employee_driver_reassign")
    @FormUrlEncoded
    fun callEmployeeDriverReassign(@FieldMap mJsonObject: HashMap<String, String>): io.reactivex.Observable<GetStatus>

    @POST("employee_single_void")
    @FormUrlEncoded
    fun CallSingleVoid(@FieldMap mJsonObject: Map<String, String>): io.reactivex.Observable<GetStatus>

    @POST("employee_generate_invoice")
    @FormUrlEncoded
    fun callEmployeeGenerateInvoice(@FieldMap mJsonObject: HashMap<String, String>): io.reactivex.Observable<GetEmployeeQenerateInvoice>

//    @POST("employee_payment_invoice")
//    @FormUrlEncoded
//    fun callEmployeePaymentInvoice(@FieldMap mJsonObject: HashMap<String, String>): io.reactivex.Observable<GetEmployeeQenerateInvoice>

    @POST("employee_payment_invoice")
    @Multipart
    fun callEmployeePaymentInvoice(@PartMap partMap: Map<String, @JvmSuppressWildcards RequestBody>): io.reactivex.Observable<GetEmployeeQenerateInvoice>

    @POST("employee_payment_invoice")
    @Multipart
    fun callEmployeePaymentInvoice(@PartMap partMap: Map<String, @JvmSuppressWildcards RequestBody>, @Part part: MultipartBody.Part,
                                   @Part("digital_siganture") requestBody: RequestBody): io.reactivex.Observable<GetEmployeeQenerateInvoice>

    @POST("employee_invoice_details")
    @FormUrlEncoded
    fun callEmployeeInvoiceDetails(@FieldMap mJsonObject: Map<String, String>): io.reactivex.Observable<GetEmployeeInvoiceDetails>

    @POST("employee_invoice_tracking")
    @FormUrlEncoded
    fun callEmployeeInvoiceTracking(@FieldMap mJsonObject: HashMap<String, String>): io.reactivex.Observable<GetEmployeeInvoiceTracking>

    @GET("json?")
    fun getLoginDetail(@Query("origin") origin: String, @Query("destination") destination: String, @Query("sensor") sensor: String, @Query("mode") mode: String): io.reactivex.Observable<GetDirectionFile>

//    @POST("employee_sales_queues")
//    @FormUrlEncoded
//    fun callEmployeeSalesQueues(@FieldMap mJsonObject: HashMap<String, String>): io.reactivex.Observable<GetEmployeeSalesQueues>

    @POST("employee_sales_queue_select")
    @FormUrlEncoded
    fun callEmployeeSalesQueueSelect(@FieldMap mJsonObject: HashMap<String, String>): io.reactivex.Observable<GetStatus>

    @POST("employee_sales_products")
    @FormUrlEncoded
    fun callEmployeeSalesProducts(@FieldMap mJsonObject: Map<String, String>): io.reactivex.Observable<GetEmployeeSalesProducts>

    @POST("employee_sales_addcart")
    @FormUrlEncoded
    fun callEmployeeSalesAddcart(@FieldMap mJsonObject: Map<String, String>): io.reactivex.Observable<GetEmployeeSalesProducts>

    @POST("employee_sales_delete_item")
    @FormUrlEncoded
    fun callEmployeePatientRemovecart(@FieldMap mJsonObject: Map<String, String>): io.reactivex.Observable<GetStatus>

    @POST("employee_patient_apply_loyalty")
    @FormUrlEncoded
    fun callEmployeePatientApplyLoyalty(@FieldMap mJsonObject: HashMap<String, String>): io.reactivex.Observable<GetStatus>

    @POST("employee_patient_remove_loyalty")
    @FormUrlEncoded
    fun callEmployeePatientRemoveLoyalty(@FieldMap mJsonObject: HashMap<String, String>): io.reactivex.Observable<GetStatus>

    @POST("employee_sales_viewcart")
    @FormUrlEncoded
    fun callEmployeeSalesViewcart(@FieldMap mJsonObject: Map<String, String>): io.reactivex.Observable<GetEmployeePatientCart>

    @POST("employee_sales_updatecart")
    @FormUrlEncoded
    fun callEmployeeSalesUpdatecart(@FieldMap mJsonObject: Map<String, String>): io.reactivex.Observable<GetStatus>

//    @POST("employee_sales_invoice")
//    @FormUrlEncoded
//    fun callEmployeeSalesInvoice(@FieldMap mJsonObject: HashMap<String, String>): io.reactivex.Observable<GetEmployeeQenerateInvoice>

    @POST("employee_sales_invoice")
    @Multipart
    fun callEmployeeSalesInvoice(@PartMap partMap: Map<String, @JvmSuppressWildcards RequestBody>): io.reactivex.Observable<GetEmployeeQenerateInvoice>

    @POST("employee_sales_invoice")
    @Multipart
    fun callEmployeeSalesInvoice(@PartMap partMap: Map<String, @JvmSuppressWildcards RequestBody>, @Part part: MultipartBody.Part,
                                 @Part("digital_siganture") requestBody: RequestBody): io.reactivex.Observable<GetEmployeeQenerateInvoice>

    @POST("employee_sales_cancel_order")
    @FormUrlEncoded
    fun callEmployeeSalesCancelOrder(@FieldMap mJsonObject: Map<String, String>): io.reactivex.Observable<GetEmployeeQenerateInvoice>

    @POST("employee_scan_products")
    @FormUrlEncoded
    fun callEmployeeScanProducts(@FieldMap mJsonObject: Map<String, String>): io.reactivex.Observable<GetScanProducts>

    @POST("employee_search_products")
    @FormUrlEncoded
    fun callEmployeeSearchProducts(@FieldMap mJsonObject: Map<String, String>): io.reactivex.Observable<GetEmployeeSearchProducts>

    @POST("employee_search_caregiver")
    @FormUrlEncoded
    fun callEmployeeSearchCaregiver(@FieldMap mJsonObject: HashMap<String, String>): io.reactivex.Observable<GetEmployeeSearchCaregiver>

    @POST("employee_sales_search_patient")
    @FormUrlEncoded
    fun callEmployeeSalesSearchPatient(@FieldMap mJsonObject: Map<String, String>): io.reactivex.Observable<GetEmployeeSalesSearchPatient>

    @POST("employee_sales_patient_caregiverlist")
    @FormUrlEncoded
    fun callEmployeeSalesPatientCaregiverlist(@FieldMap mJsonObject: HashMap<String, String>): io.reactivex.Observable<GetEmployeeSalesSearchPatient>

    @POST("employee_sales_create_queue")
    @FormUrlEncoded
    fun callEmployeeSalesCreateQueue(@FieldMap mJsonObject: HashMap<String, String>): io.reactivex.Observable<GetEmployeeSalesCreateQueue>

    @POST("employee_sales_product_info")
    @FormUrlEncoded
    fun callEmployeeSalesProductInfo(@FieldMap mJsonObject: HashMap<String, String>): io.reactivex.Observable<GetEmployeeSalesProductInfo>

    @POST("employee_apply_stman_discount")
    @FormUrlEncoded
    fun callEmployeeApplyStmanDiscount(@FieldMap mJsonObject: HashMap<String, String>): io.reactivex.Observable<GetStatus>

    @POST("employee_remove_stman_discount")
    @FormUrlEncoded
    fun callEmployeeRemoveStmanDiscount(@FieldMap mJsonObject: HashMap<String, String>): io.reactivex.Observable<GetStatus>

    @POST("employee_sales_apply_stman_discount")
    @FormUrlEncoded
    fun callEmployeeSalesApplyStmanDiscount(@FieldMap mJsonObject: Map<String, String>): io.reactivex.Observable<GetStatus>

    @POST("employee_sales_remove_stman_discount")
    @FormUrlEncoded
    fun callEmployeeSalesRemoveStmanDiscount(@FieldMap mJsonObject: Map<String, String>): io.reactivex.Observable<GetStatus>

    @POST("employee_units")
    @FormUrlEncoded
    fun callEmployeeUnits(@FieldMap mJsonObject: HashMap<String, String>): io.reactivex.Observable<EmployeeUnits>

    @POST("employee_sales_report")
    @FormUrlEncoded
    fun callEmployeeSalesReport(@FieldMap mJsonObject: Map<String, String>): io.reactivex.Observable<EmployeeSalesReport>

    @POST("employee_search_invoice")
    @FormUrlEncoded
    fun callEmployeeSearchInvoice(@FieldMap mJsonObject: HashMap<String, String>): io.reactivex.Observable<EmployeeSalesReport>

    @POST("employee_patient_promocode_list")
    @FormUrlEncoded
    fun callEmployeePatientPromocodeList(@FieldMap mJsonObject: Map<String, String>): io.reactivex.Observable<GetEmployeePatientPromocodeList>

    @POST("employee_apply_promocode")
    @FormUrlEncoded
    fun callEmployeeApplyPromocode(@FieldMap mJsonObject: HashMap<String, String>): io.reactivex.Observable<GetEmployeePatientApplyPromocode>

    @POST("employee_remove_promocode")
    @FormUrlEncoded
    fun callEmployeeRemovePromocode(@FieldMap mJsonObject: HashMap<String, String>): io.reactivex.Observable<GetStatus>

    @POST("employee_sales_apply_promocode")
    @FormUrlEncoded
    fun callEmployeeSalesApplyPromocode(@FieldMap mJsonObject: HashMap<String, String>): io.reactivex.Observable<GetEmployeePatientApplyPromocode>

    @POST("employee_sales_remove_promocode")
    @FormUrlEncoded
    fun callEmployeeSalesRemovePromocode(@FieldMap mJsonObject: Map<String, String>): io.reactivex.Observable<GetStatus>

    @POST("employee_terminal_sales")
    @FormUrlEncoded
    fun callEmployeeTerminalSales(@FieldMap mJsonObject: HashMap<String, String>): io.reactivex.Observable<GetEmployeeTerminalSales>

    @POST("employee_product_qty")
    @FormUrlEncoded
    fun callEmployeeProductQty(@FieldMap mJsonObject: Map<String, String>): io.reactivex.Observable<GetStatus>

    @POST("employee_terminal_takeout_sales")
    @FormUrlEncoded
    fun callEmployeeTerminalTakeoutSales(@FieldMap mJsonObject: HashMap<String, String>): io.reactivex.Observable<GetStatus>

    @POST("employee_terminal_petty_limit")
    @FormUrlEncoded
    fun callEmployeeTerminalPettyLimit(@FieldMap mJsonObject: HashMap<String, String>): io.reactivex.Observable<GetStatus>

    @POST("employee_pin_change")
    @FormUrlEncoded
    fun callEmployeePinChange(@FieldMap mJsonObject: HashMap<String, String>): io.reactivex.Observable<GetStatus>

    @POST("employee_password_change")
    @FormUrlEncoded
    fun callEmployeePasswordChange(@FieldMap mJsonObject: HashMap<String, String>): io.reactivex.Observable<GetStatus>

    @POST("employee_storem_list")
    @FormUrlEncoded
    fun callEmployeeStoremList(@FieldMap mJsonObject: Map<String, String>): io.reactivex.Observable<GetEmployeeStoremList>

    @POST("employee_storem_requirepin")
    @FormUrlEncoded
    fun callEmployeeRequirepin(@FieldMap mJsonObject: Map<String, String>): io.reactivex.Observable<GetStatus>

    @POST("employee_storem_changepassword")
    @FormUrlEncoded
    fun callEmployeeStoremChangepassword(@FieldMap mJsonObject: HashMap<String, String>): io.reactivex.Observable<GetStatus>

    @POST("employee_storem_changepin")
    @FormUrlEncoded
    fun callEmployeeStoremChangepin(@FieldMap mJsonObject: HashMap<String, String>): io.reactivex.Observable<GetStatus>

    @POST("employee_helpdesk")
    @FormUrlEncoded
    fun callEmployeeHelpdesk(@FieldMap mJsonObject: HashMap<String, String>): io.reactivex.Observable<GetStatus>

    //graph report api call
    @POST("employee_report_metrcs")
    @FormUrlEncoded
    fun callEmployeeReportMetric(@FieldMap mJsonObject: HashMap<String, String>): io.reactivex.Observable<GetEmployeeReportMetric>

    //for printer add 6/12/2018

    //call in splash screen
    //get printer data
    @POST("employee_terminal_printers")
    @FormUrlEncoded
    fun callEmployeeTerminalPrinter(@FieldMap mJsonObject: Map<String, String>): io.reactivex.Observable<GetTerminalPrinter>

    //if no printer then register
    @POST("employee_printer_regis")
    @FormUrlEncoded
    fun callEmployeePrinterRegister(@FieldMap mJsonObject: Map<String, String>): io.reactivex.Observable<GetPrinterRegister>

    @POST("employee_banned_patient")
    @FormUrlEncoded
    fun callEmployeeBannedPatient(@FieldMap mJsonObject: HashMap<String, String>): io.reactivex.Observable<getEmployeeBannedPatient>


    @POST("employee_patient_notes")
    @FormUrlEncoded
    fun callEmployeePatientNote(@FieldMap mJsonObject: Map<String, String>): io.reactivex.Observable<getEmployeeBannedPatient>


    @POST("employee_patient_documents")
    @FormUrlEncoded
    fun callEmployeePatientDocument(@FieldMap mJsonObject: HashMap<String, String>): io.reactivex.Observable<GETEmployeePatientDocument>

    @POST("employee_patient_documents_delete")
    @FormUrlEncoded
    fun callEmployeePatientDeleteDocument(@FieldMap mJsonObject: HashMap<String, String>): io.reactivex.Observable<GETEmployeePatientDocument>

    @POST("employee_patient_documents_add")
    @Multipart
    fun addEmployeePatientDocument(@PartMap mJsonObject: Map<String, @JvmSuppressWildcards RequestBody>, @Part document_file: MultipartBody.Part, @Part("document_image") document_image: RequestBody): io.reactivex.Observable<GETEmployeePatientDocument>

    @POST("employee_patient_photo_add")
    @Multipart
    fun addEmployeeinfoImg(@PartMap mJsonObject: Map<String, @JvmSuppressWildcards RequestBody>, @Part document_file: MultipartBody.Part, @Part("document_image") document_image: RequestBody): io.reactivex.Observable<GetStatus>

    @POST("employee_patient_info")
    @FormUrlEncoded
    fun callEmployeePatientInfo(@FieldMap mJsonObject: Map<String, String>): io.reactivex.Observable<EmployeePatientInfo>

    //store_analytics report api call
    @POST("employee_store_analytics")
    @FormUrlEncoded
    fun callEmployeeStoreAnalytics(@FieldMap mJsonObject: HashMap<String, String>): io.reactivex.Observable<getEmployeeStoreAnalytics>

    @POST("employee_store_analytics_product")
    @FormUrlEncoded
    fun callEmployeeStoreAnalyticsProduct(@FieldMap mJsonObject: HashMap<String, String>): io.reactivex.Observable<getEmployeeStoreAnalyticsProduct>

    @POST("employee_store_analytics_employee")
    @FormUrlEncoded
    fun callEmployeeStoreAnalyticsEmployee(@FieldMap mJsonObject: HashMap<String, String>): io.reactivex.Observable<getEmployeeStoreAnalyticsEmployee>

    @POST("employee_store_analytics_patient")
    @FormUrlEncoded
    fun callEmployeeStoreAnalyticsPatient(@FieldMap mJsonObject: HashMap<String, String>): io.reactivex.Observable<getEmployeeStoreAnalyticsPatient>

     @POST("employee_patient_notes_add")
    @FormUrlEncoded
    fun callEmployeeSendNote(@FieldMap mJsonObject: Map<String, String>): io.reactivex.Observable<GetStatus>

    @POST("employee_patient_notes_delete")
    @FormUrlEncoded
    fun callDdeleteNOte(@FieldMap mJsonObject: Map<String, String>): io.reactivex.Observable<GetStatus>

    @POST("expenses")
    @FormUrlEncoded
    fun callExpenses(@FieldMap mJsonObject: Map<String, String>): io.reactivex.Observable<GetEmpExpenses>

 @POST("addnewexpense")
    @FormUrlEncoded
    fun callAddexpenses(@FieldMap mJsonObject: Map<String, String>): io.reactivex.Observable<GetStatus>

    @POST("expensename")
    @FormUrlEncoded
    fun callExpenseslist(@FieldMap mJsonObject: Map<String, String>): io.reactivex.Observable<GetExpensesList>

    @POST("deleteexpense")
    @FormUrlEncoded
    fun callExpensedelete(@FieldMap mJsonObject: Map<String, String>): io.reactivex.Observable<GetStatus>

 @POST("editexpense")
    @FormUrlEncoded
    fun calleditExpenses(@FieldMap mJsonObject: Map<String, String>): io.reactivex.Observable<GetStatus>

    @POST(" editdeposit")
    @FormUrlEncoded
    fun calleditDeposit(@FieldMap mJsonObject: Map<String, String>): io.reactivex.Observable<GetStatus>

    @POST("deposit")
    @FormUrlEncoded
    fun callDeposit(@FieldMap mJsonObject: Map<String, String>): io.reactivex.Observable<GetDeposits>

    @POST("addnewdeposit")
    @FormUrlEncoded
    fun callAddDeposit(@FieldMap mJsonObject: Map<String, String>): io.reactivex.Observable<GetStatus>

    @POST("depositbank")
    @FormUrlEncoded
    fun callDepositbanks(@FieldMap mJsonObject: Map<String, String>): io.reactivex.Observable<GetBanks>

    @POST("deletedeposit")
    @FormUrlEncoded
    fun callDeleteDeposit(@FieldMap mJsonObject: Map<String, String>): io.reactivex.Observable<GetStatus>

    @POST("financial_analytics")
    @FormUrlEncoded
    fun callFinancialAnalytics(@FieldMap mJsonObject: Map<String, String>): io.reactivex.Observable<GetFinancialReport>

    @POST("employee_terminal_sales")
    @FormUrlEncoded
    fun callEmployeeTerminal(@FieldMap mJsonObject: Map<String, String>): io.reactivex.Observable<GetTerminalEmployee>

    @POST("employee_store_open")
    @FormUrlEncoded
    fun callEmployeeStoreopen(@FieldMap mJsonObject: Map<String, String>): io.reactivex.Observable<GetStoreOpen>

    @POST("employee_till_open")
    @FormUrlEncoded
    fun callTillsOpen(@FieldMap mJsonObject: Map<String, String>): io.reactivex.Observable<GetTillOpen>

    @POST("employee_terminal_takeout_sales")
    @FormUrlEncoded
    fun callemployeecashdrop(@FieldMap mJsonObject: Map<String, String>): io.reactivex.Observable<GetStatus>

    @POST("expenses_terminal")
    @FormUrlEncoded
    fun callExpensesterminal(@FieldMap mJsonObject: Map<String, String>): io.reactivex.Observable<GetEmpExpenses>

    @POST("employee_update_denominations")
    @FormUrlEncoded
    fun calldenominations(@FieldMap mJsonObject: Map<String, String>): io.reactivex.Observable<GetDenominations>

    @POST("employee_till_close")
    @FormUrlEncoded
    fun callTillClose(@FieldMap mJsonObject: Map<String, String>): io.reactivex.Observable<GetTillClose>

    @POST("employee_store_close")
    @FormUrlEncoded
    fun callStoreClose(@FieldMap mJsonObject: Map<String, String>): io.reactivex.Observable<GetStoreClose>

     @POST("employee_notes_patients")
    @FormUrlEncoded
    fun callGetNotes(@FieldMap mJsonObject: Map<String, String>): io.reactivex.Observable<GetNotesDatashow>

    @POST("employee_patient_notes_add")
    @FormUrlEncoded
    fun callAddNote(@FieldMap mJsonObject: Map<String, String>): io.reactivex.Observable<GetStatus>

    @POST("employee_patient_notes_delete")
    @FormUrlEncoded
    fun callDeleteNote(@FieldMap mJsonObject: Map<String, String>): io.reactivex.Observable<GetStatus>


    @POST("employee_patient_documents")
    @FormUrlEncoded
    fun callPatientDocument(@FieldMap mJsonObject: Map<String, String>): io.reactivex.Observable<GetDocumentNotes>


    @POST("employee_patient_documents_add")
    @Multipart
    fun callPatientDocumentAdd(@PartMap mJsonObject: Map<String, @JvmSuppressWildcards RequestBody>, @Part document_file: MultipartBody.Part, @Part("document_image") document_image: RequestBody): io.reactivex.Observable<GetStatus>

    @POST("patient_docs_add")
    @Multipart
    fun calldocumentsadd(@PartMap mJsonObject: Map<String, @JvmSuppressWildcards RequestBody>, @Part document_file: MultipartBody.Part, @Part("document_image") document_image: RequestBody): io.reactivex.Observable<GetStatus>


    @POST("employee_patient_documents_delete")
    @FormUrlEncoded
    fun callDeleteDocument(@FieldMap mJsonObject: Map<String, String>): io.reactivex.Observable<GetStatus>

    @POST("employee_patient_update")
    @FormUrlEncoded
    fun callPatientUpdate(@FieldMap mJsonObject: Map<String, String>): io.reactivex.Observable<GetStatus>

  @POST("employee_cg_patient_add")
    @FormUrlEncoded
    fun calladdcgpatient(@FieldMap mJsonObject: Map<String, String>): io.reactivex.Observable<GetStatus>

    @POST("employee_cg_patients")
    @FormUrlEncoded
    fun callgetCaregivers(@FieldMap mJsonObject: Map<String, String>): io.reactivex.Observable<getcaregivers>

    @POST("employee_cg_patient_delete")
    @FormUrlEncoded
    fun callcgdelete(@FieldMap mJsonObject: Map<String, String>): io.reactivex.Observable<GetStatus>

    @POST("store_open_check")
    @FormUrlEncoded
    fun callCheckStoreOpen(@FieldMap mJsonObject: Map<String, String>): io.reactivex.Observable<GetStatus>

 @POST("till_open_check")
    @FormUrlEncoded
    fun callTillOpenCheck(@FieldMap mJsonObject: Map<String, String>): io.reactivex.Observable<GetStatus>

    @POST("driver_gps_track")
    @FormUrlEncoded
    fun CallLocationsend(@FieldMap mJsonObject: Map<String, String>): io.reactivex.Observable<GetStatus>


}
