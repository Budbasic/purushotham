package com.budbasic.posconsole.dialogs

import android.annotation.SuppressLint
import android.app.DialogFragment
import android.content.Context
import android.os.Bundle
import android.support.v7.widget.CardView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.EditText
import com.budbasic.posconsole.R

@SuppressLint("ValidFragment")
class AddLicenceDialog constructor(val mcontext: Context, val addLicence: AddLicence) : DialogFragment() {

    lateinit var edt_note_message: EditText
    lateinit var cv_submit: CardView
    lateinit var cv_cancel: CardView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(DialogFragment.STYLE_NO_TITLE, R.style.TransparentProgressDialog)
    }


    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View {
        val v = inflater!!.inflate(R.layout.dialog_licence_edit, container, false)

        init(v)

        return v
    }


    fun init(v: View) {
        edt_note_message = v.findViewById(R.id.edt_note_message) as EditText
        cv_submit = v.findViewById(R.id.cv_submit)
        cv_cancel = v.findViewById(R.id.cv_cancel)
        edt_note_message.setImeOptions(EditorInfo.IME_FLAG_NO_EXTRACT_UI);

        cv_submit.setOnClickListener {
            println("aaaaaaaaaaa   loicence "+edt_note_message.text.toString())
           addLicence.addLicence(edt_note_message.text.toString())
            this@AddLicenceDialog.dismiss()
        }
        cv_cancel.setOnClickListener {
            this@AddLicenceDialog.dismiss()
        }
    }

    interface AddLicence {
        fun addLicence(Licencenumber: String)
    }

}
