package com.budbasic.posconsole.dialogs;

import android.app.Activity;
import android.app.Dialog;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.budbasic.customer.global.Preferences;
import com.budbasic.global.CUC;
import com.budbasic.posconsole.R;
import com.budbasic.posconsole.adapter.ProductprintAdapter;
import com.budbasic.posconsole.global.RasterDocument;
import com.budbasic.posconsole.global.StarBitmap;
import com.budbasic.posconsole.printer.sdk.BluetoothService;
import com.budbasic.posconsole.printer.sdk.Command;
import com.budbasic.posconsole.reception.fragment.PrintPageFragment;
import com.budbasic.posconsole.reception.fragment.PrinterConnectionActivity;
import com.budbasic.posconsole.webservice.RequetsInterface;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.Writer;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.oned.Code128Writer;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;
import com.journeyapps.barcodescanner.BarcodeEncoder;
import com.kotlindemo.model.GetEmployeeLogin;
import com.kotlindemo.model.GetEmployeeQenerateInvoice;
import com.kotlindemo.model.GetEmployeeSearchProducts;
import com.kotlindemo.model.GetInvoiceDetails;
import com.kotlindemo.model.GetProducts;
import com.kotlindemo.model.GetTaxData;
import com.kotlindemo.model.getprinterdata;

import org.jetbrains.annotations.NotNull;

import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import kotlin.TypeCastException;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.StringCompanionObject;
import zj.com.customize.sdk.Other;

public class BloothDailog {

   private Context context;
    public Dialog mDialog;
    GetEmployeeLogin logindata;
    private TextView rb_recepprinter,rb_lblprinter;


    public void showDialog(final PrinterConnectionActivity context, final getprinterdata getprinterdata) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_printerregister1);
        rb_recepprinter=dialog.findViewById(R.id.rb_recepprinter);
        rb_lblprinter=dialog.findViewById(R.id.rb_lblprinter);


        rb_recepprinter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                ((PrinterConnectionActivity)context).callEmployeePrinterRegister(getprinterdata.getHardware_manufacturer(),1);
            }
        }); rb_lblprinter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                ((PrinterConnectionActivity)context).callEmployeePrinterRegister(getprinterdata.getHardware_manufacturer(),0);
            }
        });

        dialog.show();
        dialog.setCancelable(true);
    }

    public void showDialog(final Context context, int i, final PrintPageFragment printPageFragment, final int position) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.print_barcode);

        TextView tv_print=dialog.findViewById(R.id.tv_print);
        TextView tv_text=dialog.findViewById(R.id.tv_text);
        TextView tv_cancle=dialog.findViewById(R.id.tv_cancle);
        final ImageView imgbarcode=dialog.findViewById(R.id.imgbarcode);
        final EditText et_entertext=dialog.findViewById(R.id.et_entertext);
        final LinearLayout rlLogos=dialog.findViewById(R.id.rlLogos);
        rlLogos.setDrawingCacheEnabled(true);
        rlLogos.buildDrawingCache();
        int nPaperWidth = 576;
        Bitmap newBitmap  = rlLogos.getDrawingCache();
       /* try {
            Bitmap bitmap = CUC.Companion.encodeAsBitmap$app_debug("helloo",BarcodeFormat.QR_CODE,600,600);
        } catch (WriterException e) {
            e.printStackTrace();
        }*/
        tv_cancle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        tv_print.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String entretext=et_entertext.getText().toString().trim();
                if (entretext.isEmpty()){
                    Toast.makeText(context, "Please Enter Text", Toast.LENGTH_SHORT).show();
                }else {
                    rlLogos.setVisibility(View.VISIBLE);
                    if (position == 0) {

                        try {
                            Bitmap bitmap = encodeAsBitmap(entretext);
                            System.out.println("aaaaaaaaaaaaa   bitmap1111  " + bitmap);
                            imgbarcode.setImageBitmap(bitmap);
                            printPageFragment.print(bitmap, rlLogos, entretext);
                            // dialog.dismiss();
                        } catch (WriterException e) {
                            System.out.println("aaaaaaaaaaa  catch  " + e.getMessage());
                            e.printStackTrace();
                        }
                    } else {
                        try {
                            Hashtable<EncodeHintType, ErrorCorrectionLevel> hintMap = new Hashtable<EncodeHintType, ErrorCorrectionLevel>();
                            hintMap.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.L);
                            Writer codeWriter;
                            codeWriter = new Code128Writer();
                            BitMatrix byteMatrix = codeWriter.encode(entretext, BarcodeFormat.CODE_128, 400, 200, hintMap);
                            int width = byteMatrix.getWidth();
                            int height = byteMatrix.getHeight();
                            Bitmap bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
                            for (int i = 0; i < width; i++) {
                                for (int j = 0; j < height; j++) {
                                    bitmap.setPixel(i, j, byteMatrix.get(i, j) ? Color.BLACK : Color.WHITE);
                                }
                            }
                            imgbarcode.setImageBitmap(bitmap);
                            printPageFragment.print(bitmap, rlLogos, entretext);
                        } catch (Exception e) {
                            System.out.println("aaaaaaaaaaa   " + e.getMessage());
                            Toast.makeText(context, e.getMessage(), Toast.LENGTH_LONG).show();
                        }

                    }
                }

            }
        });
        dialog.show();
        dialog.setCancelable(true);
    }
    Bitmap encodeAsBitmap(String str) throws WriterException {
        BitMatrix result;
        try {
            result = new MultiFormatWriter().encode(str,
                    BarcodeFormat.QR_CODE, 200, 200, null);
        } catch (IllegalArgumentException iae) {
            // Unsupported format
            return null;
        }
        int w = result.getWidth();
        int h = result.getHeight();
        int[] pixels = new int[w * h];
        for (int y = 0; y < h; y++) {
            int offset = y * w;
            for (int x = 0; x < w; x++) {
                pixels[offset + x] = result.get(x, y) ? 0xFF000000 : 0xFFFFFFFF;
            }
        }
        Bitmap bitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
        bitmap.setPixels(pixels, 0, 200, 0, 0, w, h);
        System.out.println("aaaaaaaaaaaaa   bitmap  "+bitmap);
        return bitmap;
    }


}
