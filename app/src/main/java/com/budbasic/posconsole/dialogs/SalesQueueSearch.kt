package com.budbasic.posconsole.dialogs

import android.annotation.SuppressLint
import android.app.Dialog
import android.app.DialogFragment
import android.content.Context
import android.content.Intent
import android.graphics.Point
import android.opengl.Visibility
import android.os.Build
import android.os.Bundle
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.SearchView
import android.util.Log
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.EditText
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.annotation.RequiresApi
import com.budbasic.customer.global.Preferences
import com.budbasic.global.CUC
import com.budbasic.posconsole.R
import com.budbasic.posconsole.adapter.SaleQueueAdapter
import com.budbasic.posconsole.dialogs.ProductDetailsDialog
import com.budbasic.posconsole.global.EmployeeSearchProducts
import com.budbasic.posconsole.reception.SalesActivity
import com.budbasic.posconsole.sales.SalesMenagerFragment
import com.budbasic.posconsole.webservice.RequetsInterface
import com.bumptech.glide.Glide
import com.google.gson.Gson
import com.kotlindemo.model.GetEmployeeAppSetting
import com.kotlindemo.model.GetEmployeeLogin
import com.kotlindemo.model.GetProducts
import com.kotlindemo.model.GetQueueList
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_sales_menager.view.*
import kotlinx.android.synthetic.main.store_searchs.view.*
import java.util.*

@SuppressLint("ValidFragment")
class SalesQueueSearch(val mContext: Context?) : DialogFragment() {
    lateinit var mDialog: Dialog
    lateinit var logindata: GetEmployeeLogin
    lateinit var mAppBasic: GetEmployeeAppSetting
    lateinit var productAdapter: ProductAdapter
    lateinit var saleQueueAdapter: SaleQueueAdapter
    internal var mSearchProductName = ""
    var selectDate = ""

    var list: ArrayList<GetQueueList> = ArrayList()
    var product_id = ""

    companion object {
        var searchdataArrayList = ArrayList<GetProducts>()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(DialogFragment.STYLE_NO_TITLE, R.style.TransparentProgressDialog)
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View {
        return inflater!!.inflate(R.layout.sales_queue, container, false)
    }

    var mView: View? = null
    @RequiresApi(Build.VERSION_CODES.M)
    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mView = view
        mAppBasic = Gson().fromJson(Preferences.getPreference(mContext!!, "AppSetting"), GetEmployeeAppSetting::class.java)
        logindata = Gson().fromJson(Preferences.getPreference(mContext!!, "logindata"), GetEmployeeLogin::class.java)
        setupRecylcer();
        customizeSearchView()
        callEmployeeQueueList()
    }
    @RequiresApi(Build.VERSION_CODES.M)
    private fun setupRecylcer() {
        mView!!.iv_back.visibility = View.GONE
        val searchProduct = mView!!.findViewById<RecyclerView>(R.id.recycle_searchproduct)
        searchProduct.layoutManager = LinearLayoutManager(mContext!!)
        searchProduct.itemAnimator = DefaultItemAnimator()
        saleQueueAdapter= SaleQueueAdapter(context,list,logindata)

        productAdapter = ProductAdapter(list,context,object : onClickListener {
            override fun callClick(data: GetQueueList) {
                println("aaaaaaaaaa  GetQueueList "+data.toString())
                val gson = Gson()
                val myJson = gson.toJson(logindata)
                val QueueData = Gson().toJson(data)
                val goIntent = Intent(activity!!, SalesActivity::class.java)
                goIntent.putExtra("queue_patient_id", "${data.queue_patient_id}")
                goIntent.putExtra("queue_id", "${data.queue_id}")
                goIntent.putExtra("QueueData", "$QueueData")
                goIntent.putExtra("logindata",myJson)
                activity!!.startActivity(goIntent)
            }

        })
        searchProduct.adapter = saleQueueAdapter
    }
    @RequiresApi(Build.VERSION_CODES.M)
    fun callEmployeeQueueList() {
        Log.i("Result", "callEmployeeSalesReport()")
        if (!activity!!.isFinishing) {
            if (mView!!.mSwipeRefreshLayout != null) {
                mView!!.mSwipeRefreshLayout.isRefreshing = false
            }
            mView!!.simpleProgressBar!!.visibility = View.VISIBLE
            val apiService = RequetsInterface.Factory.create()
            val parameterMap = HashMap<String, String>()
            parameterMap["api_key"] = "${Preferences.getPreference(activity!!, "API_KEY")}"
            if (logindata != null) {
                parameterMap["employee_id"] = "${logindata.user_id}"
                parameterMap["store_id"] = "${logindata.user_store_id}"
            }
            selectDate = "${Calendar.getInstance().get(Calendar.YEAR)}-" +
                    "${(if (Calendar.getInstance().get(Calendar.MONTH) + 1 >= 10) Calendar.getInstance().get(Calendar.MONTH) + 1 else "0" + (Calendar.getInstance().get(Calendar.MONTH) + 1))}-" +
                    "${(if (Calendar.getInstance().get(Calendar.DAY_OF_MONTH) >= 10) Calendar.getInstance().get(Calendar.DAY_OF_MONTH) else "0${Calendar.getInstance().get(Calendar.DAY_OF_MONTH)}")}"
            parameterMap["queue_date"] = "$selectDate"
            parameterMap["queue_type"] = "1"
            println("aaaaaaaaaaaa   queuelist "+parameterMap.toString())
            Log.i("Result", "$parameterMap")
            CompositeDisposable().add(apiService.callEmployeeQueueList(parameterMap)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe({ result ->
                        mView!!.simpleProgressBar!!.visibility = View.GONE
                        if (result != null) {
                            println("aaaaaaaaaaaa   queuelist "+result.toString())
                            Log.i("Result", "" + result.toString())
                            if (result.status == "0") {
                                if (result.queue_list != null && result.queue_list.size > 0) {
                                    list.clear()
                                    list.addAll(result.queue_list)
                                    println("aaaaaaaaaaa   list  "+result.queue_list.toString())
                                    saleQueueAdapter.dataChanged(list)
                                    saleQueueAdapter.notifyDataSetChanged()
                                    mView!!.tv_nodata.visibility = View.GONE
                                    mView!!.recycle_searchproduct.visibility = View.VISIBLE
                                } else {
                                    showdialog(result.message)
                                    searchdataArrayList.clear()
                                    dialog.dismiss()
                                   // list.clear()
                                  //  saleQueueAdapter.notifyDataSetChanged()
                                    mView!!.tv_nodata.visibility = View.VISIBLE
                                    mView!!.recycle_searchproduct.visibility = View.GONE
                                }
                                if (!activity!!.isFinishing)
                                    CUC.displayToast(activity!!, result.show_status, result.message)
                            } else if (result.status == "10") {
                               // apiClass!!.callApiKey(EMPLOYEEQUEUELIST)
                            } else {
                                list.clear()
                                saleQueueAdapter.notifyDataSetChanged()
                                mView!!.recycle_searchproduct.visibility = View.GONE
                                showdialog(result.message)
                                searchdataArrayList.clear()
                                dialog.dismiss()
                            }
                        } else {
                            if (!activity!!.isFinishing) {
                                list.clear()
                                productAdapter.notifyDataSetChanged()
                                mView!!.recycle_searchproduct.visibility = View.GONE
                                //  CUC.displayToast(activity!!, "0", getString(R.string.connection_to_database_failed))
                            }
                        }

                    }, { error ->
                        if (!activity!!.isFinishing) {
                            list.clear()
                            productAdapter.notifyDataSetChanged()
                            mView!!.recycle_searchproduct.visibility = View.VISIBLE
                            CUC.displayToast(activity!!, "0", getString(R.string.connection_to_database_failed))
                        }
                        error.printStackTrace()
                    })
            )
        }

    }

    @RequiresApi(Build.VERSION_CODES.M)
    fun showdialog(msg:String) {
        val builder1 = android.app.AlertDialog.Builder(context)
        builder1.setMessage("" + msg)
        builder1.setCancelable(true)

        builder1.setPositiveButton(
                "Ok"
        ) { dialog, id ->
            dialog.cancel()

        }

        val alert11 = builder1.create()
        alert11.show()
    }

    override fun onResume() {
        super.onResume()
        val display = activity!!.windowManager.defaultDisplay
        val size = Point()
        display.getSize(size)
        val width = (size.x / 2) + activity!!.resources.getDimensionPixelSize(R.dimen._100sdp)
        val height = size.y - activity!!.resources.getDimensionPixelSize(R.dimen._20sdp)
        Log.e("proData", "width : $width , height : $height")
        dialog.window.setLayout(width, height)
    }
    private fun customizeSearchView() {
        val search_product = mView!!.findViewById<SearchView>(R.id.search_product)
        search_product.setFocusable(true)
        search_product.setIconified(false)
        search_product.setImeOptions(EditorInfo.IME_FLAG_NO_EXTRACT_UI);
        search_product.setOnSearchClickListener(View.OnClickListener {
            //iv_back.setVisibility(View.GONE);
        })
        /*  val searchMagIcon = search_product.findViewById(android.support.v7.appcompat.R.id.search_button) as ImageView
        //  searchMagIcon.setImageResource(R.drawable.ic_search)
          searchMagIcon.setImageBitmap(CUC.IconicsBitmap(activity, FontAwesome.Icon.faw_search, ContextCompat.getColor(activity, R.color.graylight)))
        */
        val searchBox = search_product.findViewById<EditText>(android.support.v7.appcompat.R.id.search_src_text)
        //searchBox.setBackgroundResource(R.drawable.null_selector)
        searchBox.setTextColor(resources.getColor(R.color.border_gray))
        searchBox.setHintTextColor(resources.getColor(R.color.graylight))
        searchBox.hint = "Search Patient Name"
      //  searchBox.setTextSize(TypedValue.COMPLEX_UNIT_PX, 45f)
        val img_close = mView!!.findViewById<ImageView>(R.id.img_close)
        //  img_close.setImageBitmap(CUC.IconicsBitmap(activity, FontAwesome.Icon.faw_times, ContextCompat.getColor(activity, R.color.graylight)))
        img_close.setOnClickListener {
            searchdataArrayList.clear()
            dialog.dismiss()

        }


        search_product.setOnCloseListener(SearchView.OnCloseListener {
            searchdataArrayList.clear()
            productAdapter!!.notifyDataSetChanged()
            dialog.dismiss()
            //setResult(Activity.RESULT_OK)
            //finish()
            false
        })

        search_product.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String): Boolean {
                return false
            }

            override fun onQueryTextChange(newText: String): Boolean {
                println("aaaaaaaaaaa   search  "+newText)
                saleQueueAdapter.getFilter().filter(newText)
               /* if (productAdapter != null) {
                    if (!newText.isEmpty()) {
                        if (newText.length > 0) {
                            mSearchProductName = newText
                          //  callEmployeeQueueList(mSearchProductName)
                        }

                    } else {
                        mSearchProductName = newText
                       // searchdataArrayList.clear()
                       // productAdapter!!.notifyDataSetChanged()
                        saleQueueAdapter.getFilter().filter(newText)
                        //mProductAdapter.notifyDataSetChanged();

                        //callEmployeeScanProducts("")
                    }
                } else {
                    if (newText.length > 0) {
                        mSearchProductName = newText
                        saleQueueAdapter.getFilter().filter(newText)
                       // callEmployeeScanProducts(mSearchProductName)
                    }
                }*/
                return false
            }
        })
    }


    inner class ProductAdapter(val list: ArrayList<GetQueueList>, val context: Context, val onClick: onClickListener) :
            RecyclerView.Adapter<ProductAdapter.ViewHolder>() {

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            val data = list[position]
            println("aaaaaaaaaaaa  name   ${data.first_name} ${data.last_name}")
            holder.tv_name.text = CUC.getCapsSentences("${data.first_name} ${data.last_name}")

            holder.tv_name.setOnClickListener {
                onClick.callClick(data)
            }
        }

        override fun getItemCount(): Int {
            return list.size
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            val v = LayoutInflater.from(parent!!.context).inflate(R.layout.sales_dailog_queue, parent, false)
            return ViewHolder(v)
        }

        inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
            val tv_name: TextView = itemView.findViewById(R.id.tv_name)

        }
    }

    interface onClickListener {
        fun callClick(data: GetQueueList)
    }

}