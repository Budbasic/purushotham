package com.budbasic.posconsole.dialogs;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ExpandableListAdapter;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.budbasic.customer.global.Preferences;
import com.budbasic.global.CUC;
import com.budbasic.posconsole.R;
import com.budbasic.posconsole.adapter.DepositItemAdapter;
import com.budbasic.posconsole.adapter.ExpensesItemAdapter;
import com.budbasic.posconsole.global.Apppreference;
import com.budbasic.posconsole.reception.AddExpensesActivity;
import com.budbasic.posconsole.reception.DepositFragment;
import com.budbasic.posconsole.reception.ExpensesFragment;
import com.budbasic.posconsole.reception.ProductsFragment;
import com.budbasic.posconsole.reception.SalesActivity;
import com.budbasic.posconsole.sales.UserInfoDetails;
import com.budbasic.posconsole.webservice.RequetsInterface;
import com.kotlindemo.model.Favourite;
import com.kotlindemo.model.GetCartData;
import com.kotlindemo.model.GetDepositItems;
import com.kotlindemo.model.GetDepositbanklist;
import com.kotlindemo.model.GetEmployeeAppSetting;
import com.kotlindemo.model.GetEmployeeLogin;
import com.kotlindemo.model.GetEmployeeSalesProducts;
import com.kotlindemo.model.GetExpensesItems;
import com.kotlindemo.model.GetProductBlock;
import com.kotlindemo.model.GetSalesProduct;
import com.kotlindemo.model.GetSalesProductScan;
import com.kotlindemo.model.GetScanProducts;
import com.kotlindemo.model.GetStatus;
import com.kotlindemo.model.notesdata;
import com.starmicronics.starmgsio.ConnectionInfo;
import com.starmicronics.starmgsio.Scale;
import com.starmicronics.starmgsio.ScaleCallback;
import com.starmicronics.starmgsio.ScaleData;
import com.starmicronics.starmgsio.ScaleSetting;
import com.starmicronics.starmgsio.StarDeviceManager;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

import static java.security.AccessController.getContext;

public class AddProductDialog  {

    private TextView tv_product_name,tv_stock,tv_tottal_qty,tv_tottal_price,tv_short_name,tv_add_cart;
    private EditText tv_first_text,disc_per,disc_amount;
    private LinearLayout ll_add_cart;
    private int k=0,value=0;
    public Dialog mDialog;
    Context context;
    private Scale mScale;
    private String weight;
    Apppreference apppreference;

    Dialog dialog;
    public void showDialog(final Context context, final GetStatus result, final GetSalesProduct getProducts,
                           final ProductsFragment productsFragment,String weigth)
    {
        this.context=context;
         dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.product_qty_dialog_new);
        tv_product_name=dialog.findViewById(R.id.tv_product_name);
        tv_stock=dialog.findViewById(R.id.tv_stock);
        tv_tottal_qty=dialog.findViewById(R.id.tv_tottal_qty);
        tv_tottal_price=dialog.findViewById(R.id.tv_tottal_price);
        tv_first_text=dialog.findViewById(R.id.tv_first_text);
        disc_amount=dialog.findViewById(R.id.disc_amount);
        disc_per=dialog.findViewById(R.id.disc_per);
        ll_add_cart=dialog.findViewById(R.id.ll_add_cart);
        tv_short_name=dialog.findViewById(R.id.tv_short_name);
        tv_add_cart=dialog.findViewById(R.id.tv_add_cart);

        tv_add_cart.setText("Add To Cart");
        tv_first_text.setImeOptions(EditorInfo.IME_FLAG_NO_EXTRACT_UI | EditorInfo.IME_ACTION_DONE);
        disc_per.setImeOptions(EditorInfo.IME_FLAG_NO_EXTRACT_UI | EditorInfo.IME_ACTION_DONE);
        disc_amount.setImeOptions(EditorInfo.IME_FLAG_NO_EXTRACT_UI | EditorInfo.IME_ACTION_DONE);
        tv_product_name.setText(""+getProducts.getPacket_name());
        if (result.getInventory_qty().equals("0")){
            tv_stock.setTextColor(context.getResources().getColor(R.color.red));
            tv_stock.setText("Stack Unavailable");
        }else {
            tv_stock.setText("Stack "+result.getInventory_qty()+" "+getProducts.getUnit_short_name());
        }

        if (!weigth.equalsIgnoreCase("0")){
            tv_first_text.setText(weigth);
        }

        tv_tottal_price.setText(""+getProducts.getPacket_base_price());
        tv_short_name.setText(""+getProducts.getUnit_short_name());
        apppreference=new Apppreference(context);
        /*if(mScale != null) {
            mScale.disconnect();
        }*/
      /*  if(mScale == null) {
            System.out.println("aaaaaaaaaaa  scale not found ");
            String identifier = apppreference.getidentifier();
            ConnectionInfo.InterfaceType interfaceType = ConnectionInfo.InterfaceType.valueOf(apppreference.getinterfaceType());

            StarDeviceManager starDeviceManager = new StarDeviceManager(context);

            ConnectionInfo connectionInfo;

            switch (interfaceType) {
                default:
                case BLE:
                    connectionInfo = new ConnectionInfo.Builder()
                            .setBleInfo(identifier)
                            .build();
                    break;
                case USB:
                    connectionInfo = new ConnectionInfo.Builder()
                            .setUsbInfo(identifier)
                            .setBaudRate(1200)
                            .build();
                    break;
            }

            mScale = starDeviceManager.createScale(connectionInfo);
            mScale.connect(mScaleCallback);
        }*/
        ll_add_cart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String entervalue=tv_first_text.getText().toString().trim();
                String percentagedes=disc_per.getText().toString().trim();
                String amountdes=disc_amount.getText().toString().trim();
                Float persecent = 0.0f; int i=0;
                if (entervalue.isEmpty()){
                    CUC.Companion.displayToast(context, "Please Enter Quantity", result.getMessage());
                    Toast.makeText(context, "Please Enter Quantity", Toast.LENGTH_SHORT).show();
                }else {
                    if (result.getInventory_qty().equals("0")){
                        Toast.makeText(context, "Currrently Product Unavailable", Toast.LENGTH_SHORT).show();
                    }else {
                        if (getProducts.getUnit_short_name().equalsIgnoreCase("ea")){
                            int value=Integer.parseInt(entervalue);
                            if (value<=Float.parseFloat(result.getInventory_qty())){
                                if (!percentagedes.isEmpty()&&!amountdes.isEmpty()){
                                    Toast.makeText(context, "Enter Any One Discount", Toast.LENGTH_SHORT).show();
                                }else {
                                    if (!percentagedes.isEmpty()){
                                        persecent=Float.parseFloat(percentagedes);
                                        i=1;
                                    }
                                    if (!amountdes.isEmpty()){
                                        persecent=Float.parseFloat(amountdes);
                                        i=2;
                                    }

                                    //productsFragment.sendToCartea(getProducts,value,persecent,i);
                                    dialog.cancel();
                                }


                            }else {
                                Toast.makeText(context, "Enter valid Quantity", Toast.LENGTH_SHORT).show();
                                CUC.Companion.displayToast(context, "Store Quantity more than you entered", result.getMessage());
                            }
                        }else {
                            Float value=Float.parseFloat(entervalue);
                            if (value<=Float.parseFloat(result.getInventory_qty())){
                                if (!percentagedes.isEmpty()&&!amountdes.isEmpty()){
                                    Toast.makeText(context, "Enter Any One Discount", Toast.LENGTH_SHORT).show();
                                }else {
                                    if (!percentagedes.isEmpty()){
                                        persecent=Float.parseFloat(percentagedes);
                                        i=1;
                                    }
                                    if (!amountdes.isEmpty()){
                                        persecent=Float.parseFloat(amountdes);
                                        i=2;
                                    }

                                    productsFragment.sendToCart(getProducts,value,persecent,i);
                                    dialog.cancel();
                                }


                            }else {
                                Toast.makeText(context, "Enter valid Quantity", Toast.LENGTH_SHORT).show();
                                CUC.Companion.displayToast(context, "Store Quantity more than you entered", result.getMessage());
                            }
                        }


                    }

                }
            }
        });
        dialog.show();
        dialog.setCancelable(true);
    }

    public void showDialog(final Context context, final GetStatus result, final GetCartData getProducts) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.product_qty_dialog_new);
        tv_product_name=dialog.findViewById(R.id.tv_product_name);
        tv_stock=dialog.findViewById(R.id.tv_stock);
        tv_tottal_qty=dialog.findViewById(R.id.tv_tottal_qty);
        tv_tottal_price=dialog.findViewById(R.id.tv_tottal_price);
        tv_first_text=dialog.findViewById(R.id.tv_first_text);
        disc_amount=dialog.findViewById(R.id.disc_amount);
        disc_per=dialog.findViewById(R.id.disc_per);
        ll_add_cart=dialog.findViewById(R.id.ll_add_cart);
        tv_add_cart=dialog.findViewById(R.id.tv_add_cart);
        tv_add_cart.setText("Update To Cart");
        tv_short_name=dialog.findViewById(R.id.tv_short_name);
        tv_first_text.setImeOptions(EditorInfo.IME_FLAG_NO_EXTRACT_UI | EditorInfo.IME_ACTION_DONE);
        disc_per.setImeOptions(EditorInfo.IME_FLAG_NO_EXTRACT_UI | EditorInfo.IME_ACTION_DONE);
        disc_amount.setImeOptions(EditorInfo.IME_FLAG_NO_EXTRACT_UI | EditorInfo.IME_ACTION_DONE);
        tv_product_name.setText(""+getProducts.getPacket_name());
        if (result.getInventory_qty().equals("0")){
            tv_stock.setTextColor(context.getResources().getColor(R.color.red));
            tv_stock.setText("Stack Unavailable");
        }else {
            Float qty=Float.parseFloat(result.getInventory_qty());
            tv_stock.setText("Stack "+qty+" "+getProducts.getProduct_packets().getPacket_unit());
        }

        tv_tottal_price.setText(""+getProducts.getProduct_packets().getPacket_base_price());
        tv_short_name.setText(""+getProducts.getProduct_packets().getPacket_unit());
        tv_first_text.setText(""+getProducts.getCart_detail_qty());
        tv_first_text.setSelection(tv_first_text.getText().length());

        System.out.println("aaaaaaaaa   discount  "+getProducts.getCart_detail_per_disc()+"   "+getProducts.getCart_detail_fix_disc());

        if (!getProducts.getCart_detail_per_disc().equalsIgnoreCase("0.00")){
            if (getProducts.getCart_detail_per_disc().equalsIgnoreCase("0")){
                disc_per.setHint("0.00");
                disc_per.setText("");
                if (!getProducts.getCart_detail_fix_disc().equalsIgnoreCase("0.00")){
                    if (getProducts.getCart_detail_fix_disc().equalsIgnoreCase("0")){
                        disc_amount.setHint("0.00");
                        disc_amount.setText("");
                    }else {
                        disc_amount.setText(getProducts.getCart_detail_fix_disc());

                    }
                }

            }else {
                disc_per.setText(getProducts.getCart_detail_per_disc());

            }
        }
        else if (!getProducts.getCart_detail_fix_disc().equalsIgnoreCase("0.00")){
            if (getProducts.getCart_detail_fix_disc().equalsIgnoreCase("0")){

                disc_amount.setHint("0.00");
                disc_amount.setText("");
            }else {
                disc_amount.setText(getProducts.getCart_detail_fix_disc());

            }
        }

        ll_add_cart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String entervalue=tv_first_text.getText().toString().trim();
                System.out.println("aaaaaaaaaa  enter value  "+entervalue);
                if (entervalue.isEmpty()){
                    CUC.Companion.displayToast(context, "Please Enter Quantity", result.getMessage());
                    Toast.makeText(context, "Please Enter Quantity", Toast.LENGTH_SHORT).show();
                }else {
                    /*if (result.getInventory_qty().equals("0")){
                        Toast.makeText(context, "Currrently Product Unavailable", Toast.LENGTH_SHORT).show();
                    }else {*/
                        if (getProducts.getProduct_packets().getPacket_unit().equalsIgnoreCase("Each")){
                            int value=0;
                            try{
                                 value=Integer.parseInt(entervalue);
                            }catch (NumberFormatException e){
                                value=Integer.parseInt(getProducts.getCart_detail_qty());
                            }

                            String percentagedes=disc_per.getText().toString().trim();
                            String amountdes=disc_amount.getText().toString().trim();
                            Float persecent = 0.0f; int i=0;
                            if (value<=(Float.parseFloat(result.getInventory_qty()+getProducts.getCart_detail_qty()))){
                                // productsFragment.setClick();
                                if (!percentagedes.isEmpty()&&!amountdes.isEmpty()){
                                    Toast.makeText(context, "Enter only one Discount", Toast.LENGTH_SHORT).show();
                                    CUC.Companion.displayToast(context, "Enter only one", result.getMessage());
                                }else {
                                    if (!percentagedes.isEmpty()){
                                        persecent=Float.parseFloat(percentagedes);
                                        i=1;
                                    }
                                    if (!amountdes.isEmpty()){
                                        persecent=Float.parseFloat(amountdes);
                                        i=2;
                                    }
                                   // ((SalesActivity)context).sendUpdatecartea(getProducts,value,persecent,i);
                                    dialog.cancel();
                                }

                            }else {
                                Toast.makeText(context, "Enter valid Quantity", Toast.LENGTH_SHORT).show();
                                CUC.Companion.displayToast(context, "Store Quantity more than you entered", result.getMessage());
                            }
                        }else {
                            Float value=Float.parseFloat(entervalue);
                            String percentagedes=disc_per.getText().toString().trim();
                            String amountdes=disc_amount.getText().toString().trim();
                            Float persecent = 0.0f; int i=0;
                            if (value<=Float.parseFloat(result.getInventory_qty() + getProducts.getCart_detail_qty())){
                                // productsFragment.setClick();
                                if (!percentagedes.isEmpty()&&!amountdes.isEmpty()){
                                    Toast.makeText(context, "Enter only one Discount", Toast.LENGTH_SHORT).show();
                                    CUC.Companion.displayToast(context, "Enter only one", result.getMessage());
                                }else {
                                    if (!percentagedes.isEmpty()){
                                        persecent=Float.parseFloat(percentagedes);
                                        i=1;
                                    }
                                    if (!amountdes.isEmpty()){
                                        persecent=Float.parseFloat(amountdes);
                                        i=2;
                                    }
                                    ((SalesActivity)context).sendUpdatecart(getProducts,value,persecent,i);
                                    dialog.cancel();
                                }

                            }else {
                                Toast.makeText(context, "Enter valid Quantity", Toast.LENGTH_SHORT).show();
                                CUC.Companion.displayToast(context, "Store Quantity more than you entered", result.getMessage());
                            }
                        }

                 //   }

                }
            }
        });
        dialog.show();
        dialog.setCancelable(true);
    }

    public void showDialog(final Context context, final GetStatus result, final Favourite favourite, int position) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.product_qty_dialog_new);
        tv_product_name=dialog.findViewById(R.id.tv_product_name);
        tv_stock=dialog.findViewById(R.id.tv_stock);
        tv_tottal_qty=dialog.findViewById(R.id.tv_tottal_qty);
        tv_tottal_price=dialog.findViewById(R.id.tv_tottal_price);
        tv_first_text=dialog.findViewById(R.id.tv_first_text);
        disc_amount=dialog.findViewById(R.id.disc_amount);
        disc_per=dialog.findViewById(R.id.disc_per);
        ll_add_cart=dialog.findViewById(R.id.ll_add_cart);
        tv_short_name=dialog.findViewById(R.id.tv_short_name);
        tv_add_cart=dialog.findViewById(R.id.tv_add_cart);
        tv_add_cart.setText("Add To Cart");
        tv_first_text.setImeOptions(EditorInfo.IME_FLAG_NO_EXTRACT_UI | EditorInfo.IME_ACTION_DONE);
        disc_per.setImeOptions(EditorInfo.IME_FLAG_NO_EXTRACT_UI | EditorInfo.IME_ACTION_DONE);
        disc_amount.setImeOptions(EditorInfo.IME_FLAG_NO_EXTRACT_UI | EditorInfo.IME_ACTION_DONE);
        tv_product_name.setText(""+favourite.getPacket_name());
        if (result.getInventory_qty().equals("0")){
            tv_stock.setTextColor(context.getResources().getColor(R.color.red));
            tv_stock.setText("Stack Unavailable");
        }else {
            tv_stock.setText("Stack "+favourite.getInventory_qty());
        }
        tv_tottal_price.setText(""+favourite.getPacket_price());
        tv_short_name.setText(""+favourite.getProduct_packets().getPacket_unit());

        ll_add_cart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String entervalue=tv_first_text.getText().toString().trim();
                if (entervalue.isEmpty()){
                    CUC.Companion.displayToast(context, "Please Enter Quantity", result.getMessage());
                    Toast.makeText(context, "Please Enter Quantity", Toast.LENGTH_SHORT).show();
                }else {
                    if (result.getInventory_qty().equals("0")){
                        Toast.makeText(context, "Currrently Product Unavailable", Toast.LENGTH_SHORT).show();
                    }else {
                        Float value=Float.parseFloat(entervalue);

                        if (value<=Float.parseFloat(result.getInventory_qty())){
                            // productsFragment.setClick();
                            ((UserInfoDetails)context).setAddCart(favourite,value);
                            dialog.cancel();
                        }else {
                            Toast.makeText(context, "Enter valid Quantity", Toast.LENGTH_SHORT).show();
                            CUC.Companion.displayToast(context, "Store Quantity more than you entered", result.getMessage());
                        }
                    }

                }
            }
        });
        dialog.show();
        dialog.setCancelable(true);
    }

    public void showDialog(final Context context, final int check) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.totaldiscountdialog);
        final RadioButton radio1=dialog.findViewById(R.id.radio1);
        final RadioButton radio2=dialog.findViewById(R.id.radio2);
        final TextView tv_discount=dialog.findViewById(R.id.tv_discount);
        final EditText et_discount=dialog.findViewById(R.id.et_discount);
        LinearLayout cv_generate_invoice=dialog.findViewById(R.id.cv_generate_invoice);
        LinearLayout ll_submit=dialog.findViewById(R.id.ll_submit);
        final LinearLayout ll_pincode=dialog.findViewById(R.id.ll_pincode);
        final LinearLayout ll_discount=dialog.findViewById(R.id.ll_discount);
        final LinearLayout ll_cancle=dialog.findViewById(R.id.ll_cancle);
        final EditText et_pin=dialog.findViewById(R.id.et_pin);

        et_pin.setImeOptions(EditorInfo.IME_FLAG_NO_EXTRACT_UI | EditorInfo.IME_ACTION_DONE);
        et_discount.setImeOptions(EditorInfo.IME_FLAG_NO_EXTRACT_UI | EditorInfo.IME_ACTION_DONE);


        if (check==0){
            ll_discount.setVisibility(View.VISIBLE);
            ll_pincode.setVisibility(View.GONE);
        }else {
            ll_discount.setVisibility(View.GONE);
            ll_pincode.setVisibility(View.VISIBLE);
        }
        et_discount.setHint("Please Enter Fix Discount Amount");
        radio1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                radio2.setChecked(false);
                radio1.setChecked(true);
                tv_discount.setText("Enter Fix Discount");
                et_discount.setHint("Please Enter Fix Discount Amount");
                k =0;
            }
        });
        radio2.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                radio1.setChecked(false);
                radio2.setChecked(true);
                tv_discount.setText("Enter Percentage Discount");
                et_discount.setHint("Please Enter Discount Percentage");
                k =1;
            }
        });
        cv_generate_invoice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String entertxt=et_discount.getText().toString().trim();
                if (entertxt.isEmpty()){
                    Toast.makeText(context, "Please "+tv_discount.getText().toString(), Toast.LENGTH_SHORT).show();
                }else {
                    if (Float.parseFloat(entertxt)<=0){
                        Toast.makeText(context, "Please "+tv_discount.getText().toString()+"  greater than 0", Toast.LENGTH_SHORT).show();
                    }else {
                        ll_discount.setVisibility(View.GONE);
                        ll_pincode.setVisibility(View.VISIBLE);
                    }
                }
            }
        });
        ll_cancle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        ll_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String pin=et_pin.getText().toString().trim();
                if (pin.isEmpty()){
                    Toast.makeText(context, "Please enter Your pIN", Toast.LENGTH_SHORT).show();
                }else {
                    dialog.dismiss();
                    if (check==0){
                        ((SalesActivity)context).setDiscount(et_discount.getText().toString().trim(),k,pin);
                    }else {
                        ((SalesActivity)context).setRemovediscount(pin);
                    }

                }
            }
        });
        dialog.show();
        dialog.setCancelable(true);
    }


    public void showDialog(final Context context,String check) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.addnote_dialog);
       final EditText edt_note_name=dialog.findViewById(R.id.edt_note_name);
       final EditText edt_note_message=dialog.findViewById(R.id.edt_note_message);
        final LinearLayout alertcheck=dialog.findViewById(R.id.alertcheck);
        final LinearLayout alertscreen=dialog.findViewById(R.id.alertscreen);
        LinearLayout cv_submit=dialog.findViewById(R.id.cv_submit);
        final TextView tv_alertcheck=dialog.findViewById(R.id.tv_alertcheck);
        final TextView alertscreen1=dialog.findViewById(R.id.alertscreen1);
        final TextView delete_note=dialog.findViewById(R.id.delete_note);

        delete_note.setVisibility(View.GONE);

        edt_note_name.setImeOptions(EditorInfo.IME_FLAG_NO_EXTRACT_UI | EditorInfo.IME_ACTION_DONE);
        edt_note_message.setImeOptions(EditorInfo.IME_FLAG_NO_EXTRACT_UI | EditorInfo.IME_ACTION_DONE);

        alertcheck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                value=0;
                alertcheck.setBackground(context.getResources().getDrawable(R.drawable.blue_rectangle));
                alertscreen.setBackground(context.getResources().getDrawable(R.drawable.grey_border));
                alertscreen1.setTextColor(context.getResources().getColor(R.color.blue));
                tv_alertcheck.setTextColor(context.getResources().getColor(R.color.white));
            }
        });alertscreen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                value=1;
                alertcheck.setBackground(context.getResources().getDrawable(R.drawable.grey_border));
                alertscreen.setBackground(context.getResources().getDrawable(R.drawable.blue_rectangle));
                alertscreen1.setTextColor(context.getResources().getColor(R.color.white));
                tv_alertcheck.setTextColor(context.getResources().getColor(R.color.black));
            }
        });
        cv_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String notename=edt_note_name.getText().toString().trim();
                String notemsg=edt_note_message.getText().toString().trim();
                if (notename.isEmpty()){
                    Toast.makeText(context, "Please Enter Name", Toast.LENGTH_SHORT).show();
                }else if (notemsg.isEmpty()){
                    Toast.makeText(context, "Please Enter Message", Toast.LENGTH_SHORT).show();
                }else {
                    ((UserInfoDetails)context).sendnote(notename,notemsg,value);
                    dialog.dismiss();
                }
            }
        });


        dialog.show();
        dialog.setCancelable(true);
    }

    public void showDialog(final Context context, final notesdata notesdata) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.addnote_dialog);
        final EditText edt_note_name=dialog.findViewById(R.id.edt_note_name);
        final EditText edt_note_message=dialog.findViewById(R.id.edt_note_message);
        final LinearLayout alertcheck=dialog.findViewById(R.id.alertcheck);
        final LinearLayout alertscreen=dialog.findViewById(R.id.alertscreen);
        LinearLayout cv_submit=dialog.findViewById(R.id.cv_submit);
        final TextView tv_alertcheck=dialog.findViewById(R.id.tv_alertcheck);
        final TextView alertscreen1=dialog.findViewById(R.id.alertscreen1);
        final TextView delete_note=dialog.findViewById(R.id.delete_note);

        delete_note.setVisibility(View.VISIBLE);


        edt_note_name.setImeOptions(EditorInfo.IME_FLAG_NO_EXTRACT_UI | EditorInfo.IME_ACTION_DONE);
        edt_note_message.setImeOptions(EditorInfo.IME_FLAG_NO_EXTRACT_UI | EditorInfo.IME_ACTION_DONE);

        edt_note_name.setText(notesdata.getPnote_title());
        edt_note_message.setText(notesdata.getPnote_notes());
        edt_note_name.setSelection(edt_note_name.getText().length());
        edt_note_message.setSelection(edt_note_message.getText().length());

        if (notesdata.getPnote_store().equalsIgnoreCase("0")){
            value=0;
            alertcheck.setBackground(context.getResources().getDrawable(R.drawable.blue_rectangle));
            alertscreen.setBackground(context.getResources().getDrawable(R.drawable.grey_border));
            alertscreen1.setTextColor(context.getResources().getColor(R.color.blue));
            tv_alertcheck.setTextColor(context.getResources().getColor(R.color.white));
        }else {
            value=1;
            alertcheck.setBackground(context.getResources().getDrawable(R.drawable.grey_border));
            alertscreen.setBackground(context.getResources().getDrawable(R.drawable.blue_rectangle));
            alertscreen1.setTextColor(context.getResources().getColor(R.color.white));
            tv_alertcheck.setTextColor(context.getResources().getColor(R.color.black));
        }

        alertcheck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                value=0;
                alertcheck.setBackground(context.getResources().getDrawable(R.drawable.blue_rectangle));
                alertscreen.setBackground(context.getResources().getDrawable(R.drawable.grey_border));
                alertscreen1.setTextColor(context.getResources().getColor(R.color.blue));
                tv_alertcheck.setTextColor(context.getResources().getColor(R.color.white));
            }
        });alertscreen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                value=1;
                alertcheck.setBackground(context.getResources().getDrawable(R.drawable.grey_border));
                alertscreen.setBackground(context.getResources().getDrawable(R.drawable.blue_rectangle));
                alertscreen1.setTextColor(context.getResources().getColor(R.color.white));
                tv_alertcheck.setTextColor(context.getResources().getColor(R.color.black));
            }
        });
        cv_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String notename=edt_note_name.getText().toString().trim();
                String notemsg=edt_note_message.getText().toString().trim();
                if (notename.isEmpty()){
                    Toast.makeText(context, "Please Enter Name", Toast.LENGTH_SHORT).show();
                }else if (notemsg.isEmpty()){
                    Toast.makeText(context, "Please Enter Message", Toast.LENGTH_SHORT).show();
                }else {
                    ((UserInfoDetails)context).sendnote(notename,notemsg,value);
                    dialog.dismiss();
                }
            }
        });
        delete_note.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((UserInfoDetails)context).deletenote(notesdata);
                dialog.dismiss();
            }
        });

        dialog.show();
        dialog.setCancelable(true);
    }


    public void showDialog(Context context, ArrayList<GetExpensesItems> getExpensesLists, ExpensesFragment expensesFragment) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.product_qty_dialog_new);
        RelativeLayout layout_list=dialog.findViewById(R.id.layout_list);
        RelativeLayout layout_dialog_cart=dialog.findViewById(R.id.layout_dialog_cart);
        RecyclerView recycle_data=dialog.findViewById(R.id.recycle_data);

        layout_dialog_cart.setVisibility(View.GONE);
        layout_list.setVisibility(View.VISIBLE);

        recycle_data.setLayoutManager(new GridLayoutManager(context,1));

        ExpensesItemAdapter expensesItemAdapter=new ExpensesItemAdapter(context,getExpensesLists,dialog,expensesFragment);
        recycle_data.setAdapter(expensesItemAdapter);
        dialog.show();
        dialog.setCancelable(true);

    }
    public void showDialog(Context context, ArrayList<GetDepositbanklist> getDepositbanklists, DepositFragment expensesFragment) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.product_qty_dialog_new);
        RelativeLayout layout_list=dialog.findViewById(R.id.layout_list);
        RelativeLayout layout_dialog_cart=dialog.findViewById(R.id.layout_dialog_cart);
        RecyclerView recycle_data=dialog.findViewById(R.id.recycle_data);

        layout_dialog_cart.setVisibility(View.GONE);
        layout_list.setVisibility(View.VISIBLE);

        recycle_data.setLayoutManager(new GridLayoutManager(context,1));

        DepositItemAdapter expensesItemAdapter=new DepositItemAdapter(context,getDepositbanklists,dialog,expensesFragment);
        recycle_data.setAdapter(expensesItemAdapter);
        dialog.show();
        dialog.setCancelable(true);

    }



    public void showDialog(final SalesActivity context, final GetStatus result, final GetSalesProductScan getProducts, final GetEmployeeLogin logindata,
                           final GetEmployeeAppSetting mBAsis, final String queue_patient_id, final String queue_id) {

        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.product_qty_dialog_new);
        tv_product_name=dialog.findViewById(R.id.tv_product_name);
        tv_stock=dialog.findViewById(R.id.tv_stock);
        tv_tottal_qty=dialog.findViewById(R.id.tv_tottal_qty);
        tv_tottal_price=dialog.findViewById(R.id.tv_tottal_price);
        tv_first_text=dialog.findViewById(R.id.tv_first_text);
        disc_amount=dialog.findViewById(R.id.disc_amount);
        disc_per=dialog.findViewById(R.id.disc_per);
        ll_add_cart=dialog.findViewById(R.id.ll_add_cart);
        tv_short_name=dialog.findViewById(R.id.tv_short_name);
        tv_add_cart=dialog.findViewById(R.id.tv_add_cart);

        tv_add_cart.setText("Add To Cart");
        tv_first_text.setImeOptions(EditorInfo.IME_FLAG_NO_EXTRACT_UI | EditorInfo.IME_ACTION_DONE);
        disc_per.setImeOptions(EditorInfo.IME_FLAG_NO_EXTRACT_UI | EditorInfo.IME_ACTION_DONE);
        disc_amount.setImeOptions(EditorInfo.IME_FLAG_NO_EXTRACT_UI | EditorInfo.IME_ACTION_DONE);
        tv_product_name.setText(""+getProducts.getPacket_name());
        if (result.getInventory_qty().equals("0")){
            tv_stock.setTextColor(context.getResources().getColor(R.color.red));
            tv_stock.setText("Stack Unavailable");
        }else {
            tv_stock.setText("Stack "+result.getInventory_qty()+" "+getProducts.getUnit_short_name());
        }
        tv_tottal_price.setText(""+getProducts.getPacket_base_price());
        tv_short_name.setText(""+getProducts.getUnit_short_name());

        ll_add_cart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String entervalue=tv_first_text.getText().toString().trim();
                String percentagedes=disc_per.getText().toString().trim();
                String amountdes=disc_amount.getText().toString().trim();
                Float persecent = 0.0f; int i=0;
                if (entervalue.isEmpty()){
                    CUC.Companion.displayToast(context, "Please Enter Quantity", result.getMessage());
                    Toast.makeText(context, "Please Enter Quantity", Toast.LENGTH_SHORT).show();
                }else {
                    if (result.getInventory_qty().equals("0")){
                        Toast.makeText(context, "Currrently Product Unavailable", Toast.LENGTH_SHORT).show();
                    }else {
                        if (getProducts.getUnit_short_name().equalsIgnoreCase("ea")){
                            int value=Integer.parseInt(entervalue);
                            if (value<=Float.parseFloat(result.getInventory_qty())){
                                if (!percentagedes.isEmpty()&&!amountdes.isEmpty()){
                                    Toast.makeText(context, "Enter Any One Discount", Toast.LENGTH_SHORT).show();
                                }else {
                                    if (!percentagedes.isEmpty()){
                                        persecent=Float.parseFloat(percentagedes);
                                        i=1;
                                    }
                                    if (!amountdes.isEmpty()){
                                        persecent=Float.parseFloat(amountdes);
                                        i=2;
                                    }

                                    sendToCartea(context,getProducts,value,persecent,i,mBAsis,logindata,queue_id,queue_patient_id);
                                    dialog.cancel();
                                }


                            }else {
                                Toast.makeText(context, "Enter valid Quantity", Toast.LENGTH_SHORT).show();
                                CUC.Companion.displayToast(context, "Store Quantity more than you entered", result.getMessage());
                            }
                        }else {
                            Float value=Float.parseFloat(entervalue);
                            if (value<=Float.parseFloat(result.getInventory_qty())){
                                if (!percentagedes.isEmpty()&&!amountdes.isEmpty()){
                                    Toast.makeText(context, "Enter Any One Discount", Toast.LENGTH_SHORT).show();
                                }else {
                                    if (!percentagedes.isEmpty()){
                                        persecent=Float.parseFloat(percentagedes);
                                        i=1;
                                    }
                                    if (!amountdes.isEmpty()){
                                        persecent=Float.parseFloat(amountdes);
                                        i=2;
                                    }

                                   sendToCart(context,getProducts,value,persecent,i,mBAsis,logindata,queue_id,queue_patient_id);
                                    dialog.cancel();
                                }


                            }else {
                                Toast.makeText(context, "Enter valid Quantity", Toast.LENGTH_SHORT).show();
                                CUC.Companion.displayToast(context, "Store Quantity more than you entered", result.getMessage());
                            }
                        }


                    }

                }
            }
        });
        dialog.show();
        dialog.setCancelable(true);
    }

    public void sendToCart(final Context context, final GetSalesProductScan getProducts, Float value, Float discount, int i, GetEmployeeAppSetting mBAsis, GetEmployeeLogin logindata, String queue_id, String queue_patient_id){

        mDialog = CUC.Companion.createDialog((Activity) context);
        RequetsInterface apiService = RequetsInterface.Factory.create();
        Map parameterMap =new HashMap<String, String>();
        parameterMap.put("api_key", String.valueOf(Preferences.INSTANCE.getPreference((context), "API_KEY")));

        if (logindata != null) {
            parameterMap.put("store_id", logindata.getUser_store_id());
            parameterMap.put("employee_id", logindata.getUser_id());
        }
        parameterMap.put("package_id",getProducts.getPacket_id());

        parameterMap.put("patient_id",queue_patient_id);

        if (i==1){
            parameterMap.put("percentage_discount", discount);
        }else if (i==2){
            parameterMap.put("fixed_discount", discount);
        }

        parameterMap.put("product_id", getProducts.getProduct_id());
        parameterMap.put("category_id", getProducts.getProduct_cat_id());
        parameterMap.put("package_id", getProducts.getPacket_id());
        parameterMap.put("qty", ""+value);
        parameterMap.put("terminal_id", mBAsis.getTerminal_id());
        parameterMap.put("emp_lat", Preferences.INSTANCE.getPreference(context, "latitude"));
        parameterMap.put("emp_log", Preferences.INSTANCE.getPreference(context, "longitude"));
        parameterMap.put("queue_id", queue_id);

        System.out.println("aaaaaaaaaaa  parameterMap add cart "+parameterMap);
        Log.i("aaaaaaaa   Result", "parameterMap : " + parameterMap);
        (new CompositeDisposable()).add(apiService.callEmployeeSalesAddcart(parameterMap).
                observeOn(AndroidSchedulers.mainThread()).
                subscribeOn(Schedulers.io()).subscribe((Consumer)(new Consumer() {
            // $FF: synthetic method
            // $FF: bridge method
            public void accept(Object var1) {
                this.accept((GetEmployeeSalesProducts)var1);
            }

            public final void accept(GetEmployeeSalesProducts result) {
                mDialog.cancel();
                if (result != null) {
                    mDialog.cancel();

                    System.out.println("aaaaaaaaa  result  "+result.toString());
                    Log.i("aaaaaaa   Result", "" + result.toString());
                    if (result.getStatus().equalsIgnoreCase("0")) {
                        System.out.println("aaaaaaaaaa  addcart sucess");

                        try{
                            if (result.getStatus().equals("0") && result.getGrade_status().equals("0")){
                                showdialog(context,result.getGrade_message());
                            }
                        }catch (NullPointerException e){

                        }try{
                            if (result.getStatus().equals("0") && result.getGrade_warning_status().equals("0")){
                                showdialog(context,result.getGrade_warning());
                            }
                        }catch (NullPointerException e){

                        }
                        ((SalesActivity)context).onCart();
                        CUC.Companion.displayToast(context, result.getShow_status(), result.getMessage());
                    } else {
                        //  Toast.makeText(getContext(), ""+result.getShow_status(), Toast.LENGTH_SHORT).show();
                        CUC.Companion.displayToast(context, result.getShow_status(), result.getMessage());
                    }
                } else {
                    mDialog.cancel();
                }
            }
        }), (Consumer)(new Consumer() {
            // $FF: synthetic method
            // $FF: bridge method
            public void accept(Object var1) {
                this.accept((Throwable)var1);
            }

            public final void accept(Throwable error) {
                mDialog.cancel();
                Toast.makeText(context, context.getResources().getString(R.string.connection_to_database_failed), Toast.LENGTH_SHORT).show();
                error.printStackTrace();
                System.out.println("aaaaaaaa  error "+error.getMessage());
            }
        })));
    }


    public void sendToCartea(final Context context, final GetSalesProductScan getProducts, int value, Float discount, int i, GetEmployeeAppSetting mBAsis, GetEmployeeLogin logindata, String queue_id, String queue_patient_id){

        mDialog = CUC.Companion.createDialog((Activity) context);
        RequetsInterface apiService = RequetsInterface.Factory.create();
        Map parameterMap =new HashMap<String, String>();
        parameterMap.put("api_key", String.valueOf(Preferences.INSTANCE.getPreference((context), "API_KEY")));

        if (logindata != null) {
            parameterMap.put("store_id", logindata.getUser_store_id());
            parameterMap.put("employee_id", logindata.getUser_id());
        }
        parameterMap.put("package_id",getProducts.getPacket_id());

        parameterMap.put("patient_id",queue_patient_id);

        if (i==1){
            parameterMap.put("percentage_discount", discount);
        }else if (i==2){
            parameterMap.put("fixed_discount", discount);
        }

        parameterMap.put("product_id", getProducts.getProduct_id());
        parameterMap.put("category_id", getProducts.getProduct_cat_id());
        parameterMap.put("package_id", getProducts.getPacket_id());
        parameterMap.put("qty", ""+value);
        parameterMap.put("terminal_id", mBAsis.getTerminal_id());
        parameterMap.put("emp_lat", Preferences.INSTANCE.getPreference(context, "latitude"));
        parameterMap.put("emp_log", Preferences.INSTANCE.getPreference(context, "longitude"));
        parameterMap.put("queue_id", queue_id);

        System.out.println("aaaaaaaaaaa  parameterMap add cart "+parameterMap);
        Log.i("aaaaaaaa   Result", "parameterMap : " + parameterMap);
        (new CompositeDisposable()).add(apiService.callEmployeeSalesAddcart(parameterMap).
                observeOn(AndroidSchedulers.mainThread()).
                subscribeOn(Schedulers.io()).subscribe((Consumer)(new Consumer() {
            // $FF: synthetic method
            // $FF: bridge method
            public void accept(Object var1) {
                this.accept((GetEmployeeSalesProducts)var1);
            }

            public final void accept(GetEmployeeSalesProducts result) {
                mDialog.cancel();
                if (result != null) {
                    mDialog.cancel();

                    System.out.println("aaaaaaaaa  result  "+result.toString());
                    Log.i("aaaaaaa   Result", "" + result.toString());
                    if (result.getStatus().equalsIgnoreCase("0")) {
                        System.out.println("aaaaaaaaaa  addcart sucess");

                        try{
                            if (result.getStatus().equals("0") && result.getGrade_status().equals("0")){
                                showdialog(context,result.getGrade_message());
                            }
                        }catch (NullPointerException e){

                        }try{
                            if (result.getStatus().equals("0") && result.getGrade_warning_status().equals("0")){
                                showdialog(context,result.getGrade_warning());
                            }
                        }catch (NullPointerException e){

                        }
                        ((SalesActivity)context).onCart();
                        CUC.Companion.displayToast(context, result.getShow_status(), result.getMessage());
                    } else if (result.getStatus().equalsIgnoreCase("10")) {

                    } else {
                        //  Toast.makeText(getContext(), ""+result.getShow_status(), Toast.LENGTH_SHORT).show();
                        CUC.Companion.displayToast(context, result.getShow_status(), result.getMessage());
                    }
                } else {
                    mDialog.cancel();
                }
            }
        }), (Consumer)(new Consumer() {
            // $FF: synthetic method
            // $FF: bridge method
            public void accept(Object var1) {
                this.accept((Throwable)var1);
            }

            public final void accept(Throwable error) {
                mDialog.cancel();
                Toast.makeText(context, context.getResources().getString(R.string.connection_to_database_failed), Toast.LENGTH_SHORT).show();
                error.printStackTrace();
                System.out.println("aaaaaaaa  error "+error.getMessage());
            }
        })));
    }



    public void showdialog(Context context,String msg){
        AlertDialog.Builder builder1 = new AlertDialog.Builder(context);
        builder1.setMessage(""+msg);
        builder1.setCancelable(true);

        builder1.setPositiveButton(
                "Ok",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        AlertDialog alert11 = builder1.create();
        alert11.show();
    }


    private final ScaleCallback mScaleCallback = new ScaleCallback() {
        @Override
        public void onConnect(Scale scale, int status) {
            boolean connectSuccess = false;
            System.out.println("aaaaaaaaaaa  scale status "+status);
            switch (status) {
                case Scale.CONNECT_SUCCESS:
                    connectSuccess = true;
                    Toast.makeText(context, "Connect success.", Toast.LENGTH_SHORT).show();
                    break;

                case Scale.CONNECT_NOT_AVAILABLE:
                    Toast.makeText(context, "Failed to connect. (Not available)", Toast.LENGTH_SHORT).show();
                    break;

                case Scale.CONNECT_ALREADY_CONNECTED:
                    Toast.makeText(context, "Failed to connect. (Already connected)", Toast.LENGTH_SHORT).show();
                    break;

                case Scale.CONNECT_TIMEOUT:
                    Toast.makeText(context, "Failed to connect. (Timeout)", Toast.LENGTH_SHORT).show();
                    break;

                case Scale.CONNECT_READ_WRITE_ERROR:
                    Toast.makeText(context, "Failed to connect. (Read Write error)", Toast.LENGTH_SHORT).show();
                    break;

                case Scale.CONNECT_NOT_SUPPORTED:
                    Toast.makeText(context, "Failed to connect. (Not supported device)", Toast.LENGTH_SHORT).show();
                    break;

                case Scale.CONNECT_NOT_GRANTED_PERMISSION:
                    Toast.makeText(context, "Failed to connect. (Not granted permission)", Toast.LENGTH_SHORT).show();
                    break;

                default:
                case Scale.CONNECT_UNEXPECTED_ERROR:
                    Toast.makeText(context, "Failed to connect. (Unexpected error)", Toast.LENGTH_SHORT).show();
                    break;
            }

            if(!connectSuccess) {
                mScale = null;
                // finish();
            }
        }

        @Override
        public void onDisconnect(Scale scale, int status) {
            mScale = null;
            System.out.println("aaaaaaaaaaa  scale status onDisconnect "+status);
            switch(status) {
                case Scale.DISCONNECT_SUCCESS:
                    Toast.makeText(context, "Disconnect success.", Toast.LENGTH_SHORT).show();
                    break;

                case Scale.DISCONNECT_NOT_CONNECTED:
                    Toast.makeText(context, "Failed to disconnect. (Not connected)", Toast.LENGTH_SHORT).show();
                    //finish();
                    break;

                case Scale.DISCONNECT_TIMEOUT:
                    Toast.makeText(context, "Failed to disconnect. (Timeout)", Toast.LENGTH_SHORT).show();
                    // finish();
                    break;

                case Scale.DISCONNECT_READ_WRITE_ERROR:
                    Toast.makeText(context, "Failed to disconnect. (Read Write error)", Toast.LENGTH_SHORT).show();
                    // finish();
                    break;

                case Scale.DISCONNECT_UNEXPECTED_ERROR:
                    Toast.makeText(context, "Failed to disconnect. (Unexpected error)", Toast.LENGTH_SHORT).show();
                    // finish();
                    break;

                default:
                case Scale.DISCONNECT_UNEXPECTED_DISCONNECTION:
                    Toast.makeText(context, "Unexpected disconnection.", Toast.LENGTH_SHORT).show();
                    // finish();
                    break;
            }
        }

        @Override
        public void onReadScaleData(Scale scale, ScaleData scaleData) {
            if(scaleData.getStatus() == ScaleData.Status.ERROR) { // Error
                System.out.println("aaaaaaaaa   invalid weight");
            } else {
                String weight1 = String.format(Locale.US,"%."+ scaleData.getNumberOfDecimalPlaces() +"f", scaleData.getWeight());
                String unit = scaleData.getUnit().toString();
                weight=weight1;
                tv_first_text.setText(""+weight);
                tv_first_text.setSelection(tv_first_text.getText().toString().length());
                String weightStr = weight1 + " [" + unit + "]";

                //  mWeightTextView.setText(weightStr);

            }
        }

        @Override
        public void onUpdateSetting(Scale scale, ScaleSetting scaleSetting, int status) {
            if (scaleSetting == ScaleSetting.ZeroPointAdjustment) {
                switch(status) {
                    case Scale.UPDATE_SETTING_SUCCESS:
                        Toast.makeText(context, "Succeeded.", Toast.LENGTH_SHORT).show();
                        break;

                    case Scale.UPDATE_SETTING_NOT_CONNECTED:
                        Toast.makeText(context, "Failed. (Not connected)", Toast.LENGTH_SHORT).show();
                        break;

                    case Scale.UPDATE_SETTING_REQUEST_REJECTED:
                        Toast.makeText(context, "Failed. (Request rejected)", Toast.LENGTH_SHORT).show();
                        break;

                    case Scale.UPDATE_SETTING_TIMEOUT:
                        Toast.makeText(context, "Failed. (Timeout)", Toast.LENGTH_SHORT).show();
                        break;

                    case Scale.UPDATE_SETTING_ALREADY_EXECUTING:
                        Toast.makeText(context, "Failed. (Already executing)", Toast.LENGTH_SHORT).show();
                        break;

                    default:
                    case Scale.UPDATE_SETTING_UNEXPECTED_ERROR:
                        Toast.makeText(context, "Failed. (Unexpected error)", Toast.LENGTH_SHORT).show();
                        break;
                }
            }
        }
    };


}
