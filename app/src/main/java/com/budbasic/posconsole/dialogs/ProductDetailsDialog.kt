package com.budbasic.posconsole.dialogs

import android.annotation.SuppressLint
import android.app.Dialog
import android.app.DialogFragment
import android.content.Context
import android.graphics.Point
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v4.view.PagerAdapter
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.text.SpannableString
import android.text.SpannableStringBuilder
import android.text.style.ForegroundColorSpan
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView
import com.budbasic.customer.global.Preferences
import com.budbasic.posconsole.R
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DecodeFormat
import com.bumptech.glide.request.RequestOptions
import com.google.gson.Gson
import com.kotlindemo.model.*
import kotlinx.android.synthetic.main.product_details_dialog.*
import kotlinx.android.synthetic.main.product_details_dialog.view.*


@SuppressLint("ValidFragment")
class ProductDetailsDialog(internal var mContext: Context) : DialogFragment() {
    private var mAdapter: ViewPagerAdapter? = null
    var productData: GetEmployeeSalesProductInfo? = null

    lateinit var mDialog: Dialog

    lateinit var logindata: GetEmployeeLogin
    lateinit var mAppBasic: GetEmployeeAppSetting

    var productQytValue = 1f
    var package_id = ""
    var Currency_value = ""
    var qty = ""

    var queue_patient_id = ""
    var queue_id = ""

    var imagePath = ""
    lateinit var mPacketAdapter: PacketAdapter
    var packetList = ArrayList<GetProductPackets>()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(DialogFragment.STYLE_NO_TITLE, R.style.TransparentProgressDialog)
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View {
        return inflater!!.inflate(R.layout.product_details_dialog, container, false)
    }

    override fun onResume() {
        super.onResume()
        val display = activity!!.windowManager.defaultDisplay
        val size = Point()
        display.getSize(size)
        val width = (size.x / 2) + activity!!.resources.getDimensionPixelSize(R.dimen._100sdp)
        val height = size.y - activity!!.resources.getDimensionPixelSize(R.dimen._20sdp)
        Log.e("proData", "width : $width , height : $height")
        dialog.window.setLayout(width, height)
    }

    var mView: View? = null
    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        mView = view
        mAppBasic = Gson().fromJson(Preferences.getPreference(mContext!!, "AppSetting"), GetEmployeeAppSetting::class.java)
        logindata = Gson().fromJson(Preferences.getPreference(mContext!!, "logindata"), GetEmployeeLogin::class.java)
        val mData = arguments
        val proData = mData.getString("data")
        Log.i("proData", "$proData")
        productData = Gson().fromJson(proData, GetEmployeeSalesProductInfo::class.java)
        init()
    }

    fun init() {
        Currency_value = "${mAppBasic.Currency_value}"
        Log.i("ProductDetails", "product_id : $productData")
//        if (intent != null && intent.hasExtra("queue_patient_id"))
//            queue_patient_id = intent.getStringExtra("queue_patient_id")
//        if (intent != null && intent.hasExtra("queue_id"))
//            queue_id = intent.getStringExtra("queue_id")

        mView!!.iv_back.setOnClickListener {
            dialog.dismiss()
        }
        mView!!.img_close.setOnClickListener{
            dialog.dismiss()
        }
        val listPro = ArrayList<GetProductSubImage>()
        if (productData != null) {
            imagePath = productData!!.image_path
            listPro.add(GetProductSubImage("", "", productData!!.product_data.product_image, "", "", "", ""))
           // listPro.addAll(productData!!.products_images)
          //  mAdapter = ViewPagerAdapter(mContext, listPro!!)
           // mView!!.photos_viewpager.adapter = mAdapter
          //  mView!!.tab_indicator.setupWithViewPager(mView!!.photos_viewpager)
     //       Log.d("TITLENAME", "" + productData!!.product_data.product_name)
          //  mView!!.tv_pro_title.text = productData!!.product_data.product_name
            mView!!.tv_pro_name.text = productData!!.product_data.product_name
            mView!!.tv_category.text = "${productData!!.category_data.category_name}"
            mView!!.tv_category_type.text = "${productData!!.category_type_data.category_type_name}"

            println("aaaaaaaaaaaa   product image  "+productData!!.products_images)
            Glide.with(mContext)
                    .applyDefaultRequestOptions(RequestOptions().format(DecodeFormat.PREFER_ARGB_8888).error(R.drawable.no_image_available))
                    .asBitmap()
                    .load("${productData!!.products_images}")
                    .into(mView!!.img_product)

            if (productData!!.product_data.p_map_base_price != null && productData!!.product_data.p_map_base_price != "0" && productData!!.product_data.p_map_base_price != "0.0" && productData!!.product_data.p_map_base_price != "0.00") {
                mView!!.tv_pro_price.setText("$Currency_value${productData!!.product_data.p_map_base_price}")
            } else {
                mView!!.tv_pro_price.setText("$Currency_value${productData!!.product_data.product_price}")
            }
            if (productData!!.product_data.product_indica != null && productData!!.product_data.product_indica != "" && productData!!.product_data.product_indica != "0")
                mView!!.tv_indica.text = "${productData!!.product_data.product_indica}%"
            else
                mView!!.tv_indica.text = "0%"
            if (productData!!.product_data.product_sativa != null && productData!!.product_data.product_sativa != "" && productData!!.product_data.product_sativa != "0")
                mView!!.tv_sativa.text = "${productData!!.product_data.product_sativa}%"
            else
                mView!!.tv_sativa.text = "0%"
            //var lab_test_str = ""
            if (productData!!.product_data.product_thc != null && productData!!.product_data.product_thc != "")
                mView!!.tv_thc.text = "${productData!!.product_data.product_thc}%"
            else
                mView!!.tv_thc.text = "0%"

            if (productData!!.product_data.product_thca != null && productData!!.product_data.product_thca != "" && productData!!.product_data.product_thca != "0")
                mView!!.tv_thca.text = "${productData!!.product_data.product_thca}%"
            else
                mView!!.tv_thca.text = "0%"

            if (productData!!.product_data.product_cbd != null && productData!!.product_data.product_cbd != "" && productData!!.product_data.product_cbd != "0")
                mView!!.tv_cbd.text = "${productData!!.product_data.product_cbd}%"
            else
                mView!!.tv_cbd.text = "0%"

            if (productData!!.product_data.product_cbda != null && productData!!.product_data.product_cbda != "" && productData!!.product_data.product_cbda != "0")
                mView!!.tv_cbda.text = "${productData!!.product_data.product_cbda}%"
            else
                mView!!.tv_cbda.text = "0%"

            if (productData!!.product_data.product_cbn != null && productData!!.product_data.product_cbn != "" && productData!!.product_data.product_cbn != "0")
                mView!!.tv_cbn.text = "${productData!!.product_data.product_cbn}%"
            else
                mView!!.tv_cbn.text = "0%"
            val mColor1 = ContextCompat.getColor(mContext, R.color.blue)
            val mColor2 = ContextCompat.getColor(mContext, R.color.blue)
//            mView!!.tv_who_tested_medicin.setText(xxx("Who tested this medicine?\n", " -${productData!!.product_data.product_who_tested_medicine}", mColor1, mColor2), TextView.BufferType.SPANNABLE)
//            mView!!.tv_genetics.setText(xxx("Genetics (ex. Blueberry X Haze)\n", " -${productData!!.product_data.products_genetics}", mColor1, mColor2), TextView.BufferType.SPANNABLE)
//            mView!!.tv_sclabs_report_id.setText(xxx("SCLabs Report ID\n", " -${productData!!.product_data.product_sclabs_report_id}", mColor1, mColor2), TextView.BufferType.SPANNABLE)
//            mView!!.tv_lab_batch_number.setText(xxx("Lab Batch Number\n", " -${productData!!.product_data.product_lab_batch_number}", mColor1, mColor2), TextView.BufferType.SPANNABLE)
//            mView!!.tv_lab_licenceno.setText(xxx("Lab Licence Number\n", " -${productData!!.product_data.product_lab_licenceno}", mColor1, mColor2), TextView.BufferType.SPANNABLE)
//            mView!!.tv_medicine_amount.setText(xxx("Medicine Amount\n", " -${productData!!.product_data.product_medicine_amount}", mColor1, mColor2), TextView.BufferType.SPANNABLE)
//            mView!!.tv_medicine_measurement.setText(xxx("Medicine Measurement\n", " -${productData!!.product_data.product_medicine_measurement}", mColor1, mColor2), TextView.BufferType.SPANNABLE)
//            mView!!.tv_ingredients.setText(xxx("Ingredients\n", " -${productData!!.product_data.product_ingredients}", mColor1, mColor2), TextView.BufferType.SPANNABLE)
            mView!!.tv_who_tested_medicin.setText("${productData!!.product_data.product_who_tested_medicine}")
            mView!!.tv_genetics.setText("${productData!!.product_data.products_genetics}")
            mView!!.tv_sclabs_report_id.setText("${productData!!.product_data.product_sclabs_report_id}")
            mView!!.tv_lab_batch_number.setText("${productData!!.product_data.product_lab_batch_number}")
            mView!!.tv_lab_licenceno.setText("${productData!!.product_data.product_lab_licenceno}")
            mView!!.tv_medicine_amount.setText("${productData!!.product_data.product_medicine_amount}")
            mView!!.tv_medicine_measurement.setText("${productData!!.product_data.product_medicine_measurement}")
            mView!!.tv_ingredients.setText("${productData!!.product_data.product_ingredients}")
            if (productData!!.inventory_qty == "" || productData!!.inventory_qty == null || productData!!.inventory_qty == "0") {
                mView!!.tv_tottal_qty.text = "Unavailable"
                mView!!.tv_tottal_qty.setTextColor(ContextCompat.getColor(mContext, R.color.red))
              //  mView!!.ll_add_to_cart.visibility = View.GONE
            } else {
                mView!!.tv_tottal_qty.text = "${productData!!.inventory_qty} ${productData!!.unit_data.unit_short_name}"
            }
            val productTaxes = productData!!.tax_data
            if (productTaxes.size!=0){
                rv_product_taxes.visibility=View.VISIBLE
                layout_default.visibility=View.GONE
                val poductTaxesAdapter = ProductTaxesAdapter(mContext, productTaxes)
                mView!!.rv_product_taxes.layoutManager = LinearLayoutManager(mContext)
                mView!!.rv_product_taxes.adapter = poductTaxesAdapter
            }else{
                rv_product_taxes.visibility=View.GONE
                layout_default.visibility=View.VISIBLE
            }


            val mClickListner = object : ClickListner {
                override fun onClick(getProductPackets: GetProductPackets) {
                }
            }
            packetList.clear()

           // packetList = productData!!.packet_data
            if (packetList != null && packetList.size > 0) {
            //    mView!!.rv_packets!!.layoutManager = LinearLayoutManager(activity!!, LinearLayoutManager.HORIZONTAL, false)
               // mPacketAdapter = PacketAdapter(activity!!, packetList, productData!!.unit_data.unit_short_name, Currency_value, mClickListner)
              //  mView!!.rv_packets.adapter = mPacketAdapter
             //   mView!!.tvnopackets.visibility = View.GONE
            } else {
              //  mView!!.tvnopackets.visibility = View.VISIBLE
            }
        } else {
            //finish()
        }
      /*  mView!!.ll_add_to_cart.setOnClickListener {
            //DisplayProductQtyDialog()
        }*/
    }

    inner class ViewPagerAdapter(private val mContext: Context, private val imgData: ArrayList<GetProductSubImage>) : PagerAdapter() {

        override fun getCount(): Int {
            return imgData.size
        }

        override fun isViewFromObject(view: View, `object`: Any): Boolean {
            return view === `object` as LinearLayout
        }

        override fun instantiateItem(container: ViewGroup, position: Int): Any {
            val itemView = LayoutInflater.from(mContext).inflate(R.layout.pager_item, container, false)

            val imageView = itemView.findViewById(R.id.img_pager_item) as ImageView

            Glide.with(mContext)
                    .applyDefaultRequestOptions(RequestOptions().format(DecodeFormat.PREFER_ARGB_8888).error(R.drawable.no_image_available))
                    .asBitmap()
                    .load("$imagePath${imgData!![position].product_sub_image}")
                    .into(imageView)
            container.addView(itemView)

            imageView.setOnClickListener {
                //zoomDialog("$imagePath${imgData!![position].product_sub_image}")
            }

            return itemView
        }

        override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
            container.removeView(`object` as LinearLayout)
        }
    }

    class ProductTaxesAdapter(val mContext: Context, val productTaxes: ArrayList<GetProTaxData>) : RecyclerView.Adapter<ProductTaxesAdapter.ProductTaxesHolder>() {
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductTaxesHolder {
            val mView = LayoutInflater.from(parent.context).inflate(R.layout.product_taxes_items, parent, false)
            return ProductTaxesHolder(mView)
        }

        override fun getItemCount(): Int {
            return productTaxes.size
        }

        override fun onBindViewHolder(holder: ProductTaxesHolder, position: Int) {
            holder.bindVliew(mContext, productTaxes[position])
        }

        class ProductTaxesHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
            val tv_taxeslbl = itemView.findViewById<TextView>(R.id.tv_taxeslbl)
            val tv_taxesvalue = itemView.findViewById<TextView>(R.id.tv_taxesvalue)
            fun bindVliew(mContext: Context, productTaxes: GetProTaxData) {
                println("aaaaaaaaaaaa   taxes  "+productTaxes.tax_rate+"   name  "+productTaxes.tax_name)
                Log.d("AAAAAA",""+productTaxes.tax_name)
                Log.d("AAAAAA11",""+productTaxes.tax_rate)

                if (productTaxes.tax_name!=null){
                    tv_taxeslbl.text = productTaxes.tax_name
                    tv_taxesvalue.text = productTaxes.tax_rate
                }else{
                    tv_taxeslbl.text = "Detroit 5%"
                    tv_taxesvalue.text = "Michigan 6%"
                }

            }
        }
    }

    class PacketAdapter(val mContext: Context, val packetList: ArrayList<GetProductPackets>, val unit_name: String, val Currency_value: String, val mClickListner: ClickListner) : RecyclerView.Adapter<PacketAdapter.PacketHolder>() {
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PacketHolder {
            val mView = LayoutInflater.from(parent.context).inflate(R.layout.packet_item, parent, false)
            return PacketHolder(mView)
        }

        override fun getItemCount(): Int {
            return packetList.size
        }

        override fun onBindViewHolder(holder: PacketHolder, position: Int) {
            holder.bindVliew(mContext, packetList[position], unit_name, Currency_value, mClickListner)
        }

        class PacketHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
            val tv_packet_qty = itemView.findViewById<TextView>(R.id.tv_packet_qty)
            val tv_packet_price = itemView.findViewById<TextView>(R.id.tv_packet_price)
            val rl_packet_item = itemView.findViewById<RelativeLayout>(R.id.rl_packet_item)

            @SuppressLint("SetTextI18n")
            fun bindVliew(mContext: Context, getProductPackets: GetProductPackets, unit_name: String, Currency_value: String, mClickListner: ClickListner) {
                tv_packet_qty.text = "${getProductPackets.packet_qty}\n$unit_name"
                tv_packet_price.text = "$Currency_value${getProductPackets.packet_total_price}"
                if (getProductPackets.isSelected) {
                    tv_packet_qty.setTextColor(ContextCompat.getColor(mContext, R.color.white))
                    rl_packet_item.setBackgroundResource(R.drawable.packet_select_ring)
                } else {
                    tv_packet_qty.setTextColor(ContextCompat.getColor(mContext, R.color.blue))
                    rl_packet_item.setBackgroundResource(R.drawable.packet_ring)
                }
                rl_packet_item.setOnClickListener {
                    mClickListner.onClick(getProductPackets)

                }
            }
        }
    }

    interface ClickListner {
        fun onClick(getProductPackets: GetProductPackets)
    }

    fun xxx(textlbl: String, textvalue: String, textlblColor: Int, textvalueColor: Int): SpannableStringBuilder {
        val builder = SpannableStringBuilder()
        val str1 = SpannableString(textlbl)
        //str1.setSpan( RelativeSizeSpan(2f), 0,5, 0);
        str1.setSpan(ForegroundColorSpan(textlblColor), 0, str1.length, 0)
        builder.append(str1)
        val str2 = SpannableString(textvalue)
        str2.setSpan(ForegroundColorSpan(textvalueColor), 0, str2.length, 0)
        builder.append(str2)
        return builder
    }

}
