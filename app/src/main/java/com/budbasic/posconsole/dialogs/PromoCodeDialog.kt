package com.budbasic.posconsole.dialogs

import android.annotation.SuppressLint
import android.app.Dialog
import android.app.DialogFragment
import android.content.Context
import android.graphics.Point
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RelativeLayout
import android.widget.TextView
import com.budbasic.customer.global.Preferences
import com.budbasic.global.CUC
import com.budbasic.posconsole.R
import com.budbasic.posconsole.webservice.RequetsInterface
import com.google.gson.Gson
import com.kotlindemo.model.GetEmployeeAppSetting
import com.kotlindemo.model.GetEmployeeLogin
import com.kotlindemo.model.GetPromosales
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.patient_promocode_list1.*
import java.util.*

@SuppressLint("ValidFragment")
class PromoCodeDialog(internal var mContext: Context, val callListner: onCallListner) : DialogFragment() {
    var mDialog: Dialog? = null
    var logindata: GetEmployeeLogin? = null
    var mAppBasic: GetEmployeeAppSetting? = null

    //  var row_index = -1

    companion object {
        var list = ArrayList<GetPromosales>()
        var templist = ArrayList<GetPromosales>()
        var discountBarcode = ""

        var promocodeAdapter: PromoCodeAdapter? = null
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(DialogFragment.STYLE_NO_TITLE, R.style.TransparentProgressDialog)
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View {
        return inflater!!.inflate(R.layout.patient_promocode_list1, container, false)
    }

    override fun onResume() {
        super.onResume()
        val display = activity!!.windowManager.defaultDisplay
        val size = Point()
        display.getSize(size)
        val width = (size.x / 2) + activity!!.resources.getDimensionPixelSize(R.dimen._100sdp)
        val height = size.y - activity!!.resources.getDimensionPixelSize(R.dimen._20sdp)
        Log.e("proData", "width : $width , height : $height")
        dialog.window.setLayout(width, height)
    }

    var mView: View? = null
    var myBundle: Bundle? = null
    var cart_id = ""
    var patient_id = ""
    var app_status = "1"
    var queue_id = ""
    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mView = view
        myBundle = arguments
        if (myBundle != null) {
            cart_id = myBundle!!.getString("cart_id")
            patient_id = myBundle!!.getString("patient_id")
            app_status = myBundle!!.getString("app_status")
            if (app_status == "1") {
                queue_id = myBundle!!.getString("queue_id")
            }
        }
        mAppBasic = Gson().fromJson(Preferences.getPreference(mContext!!, "AppSetting"), GetEmployeeAppSetting::class.java)
        logindata = Gson().fromJson(Preferences.getPreference(mContext!!, "logindata"), GetEmployeeLogin::class.java)
        init()
    }

    private fun init() {
        list.clear()
        discountBarcode = ""
        //   edtPromoCodeName.setText("")
        rv_promo_code_list.layoutManager = LinearLayoutManager(mContext!!)
        promocodeAdapter = PromoCodeAdapter(mContext!!, object : PromoCodeListner {
            override fun onPromoCode(promo_code: GetPromosales) {
                if (app_status == "2") {
                    if (promo_code.discount_barcode == discountBarcode) {
                        callEmployeeRemovePromocode()
                    } else {
                        discountBarcode = promo_code.discount_barcode
                        //edtPromoCodeName.setText("${promo_code.discount_barcode}")
                        callEmployeeApplyPromocode()
                    }
                } else {
                    if (promo_code.discount_barcode == discountBarcode) {
                        callEmployeeSalesRemovePromocode()
                    } else {
                        discountBarcode = promo_code.discount_barcode
                        //edtPromoCodeName.setText("${promo_code.discount_barcode}")
                        callEmployeeSalesApplyPromocode()
                    }
                }

            }
        })
        edtPromoCodeName.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(editable: Editable?) {
                Log.d("ASASA", "ASASASA")


            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(char: CharSequence?, p1: Int, p2: Int, p3: Int) {

                if (promocodeAdapter != null) {
                    //  val answerString = edtPromoCodeName.getText().toString()
                    promocodeAdapter!!.filter(char.toString())
                }
            }
        })
        iv_add_category.setOnClickListener {
            dismiss()
        }
        rl_applyCode.setOnClickListener {
            if (app_status == "2") {
                // discountBarcode = promo_code.discount_barcode
                callEmployeeApplyPromocode()

            } else {

                //  discountBarcode = promo_code.discount_barcode
                //edtPromoCodeName.setText("${promo_code.discount_barcode}")
                callEmployeeSalesApplyPromocode()

            }

        }
        tvApply.setOnClickListener {
            if (app_status == "2") {
                if (tvApply.text == "Remove") {
                    callEmployeeRemovePromocode()
                } else {
                    if (edtPromoCodeName.text.toString() == "") {
                        CUC.displayToast(mContext!!, "0", "Enter Promo code")
                    } else {
                        discountBarcode = edtPromoCodeName.text.toString()
                        callEmployeeApplyPromocode()
                    }
                }
            } else {
                if (tvApply.text == "Remove") {
                    callEmployeeSalesRemovePromocode()
                } else {
                    if (edtPromoCodeName.text.toString() == "") {
                        CUC.displayToast(mContext!!, "0", "Enter Promo code")
                    } else {
                        discountBarcode = edtPromoCodeName.text.toString()
                        callEmployeeSalesApplyPromocode()
                    }
                }
            }
        }

        iv_back.setOnClickListener {

        }

        rv_promo_code_list.adapter = promocodeAdapter
        callEmployeePatientPromocodeList()
        //  templist = list
    }

    class PromoCodeAdapter(val mContext: Context, val promoCodeListner: PromoCodeListner) : RecyclerView.Adapter<PromoCodeAdapter.PromoViewHolder>() {

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PromoViewHolder {
            val itemView = LayoutInflater.from(mContext).inflate(R.layout.promo_code_item1, null)
            return PromoViewHolder(itemView)
        }

        override fun getItemCount(): Int {
            return list.size
        }

        override fun onBindViewHolder(holder: PromoViewHolder, position: Int) {
/*
            if (row_index == position) {
                holder.rl_promocode.setBackgroundDrawable(mContext.resources.getDrawable(R.drawable.gardient_radius))
            } else {
                holder.rl_promocode.setBackgroundDrawable(mContext.resources.getDrawable(R.drawable.gray_border_radius))
            }*/

            holder.bindView(mContext, list.get(position), promoCodeListner, position)
        }

        class PromoViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
            val tvPromoCodeName = itemView.findViewById<TextView>(R.id.tvPromoCodeName)
            val tvPromoCodeMsg = itemView.findViewById<TextView>(R.id.tvPromoCodeMsg)
            val tvPromoCodeApply = itemView.findViewById<TextView>(R.id.tvPromoCodeApply)
            val rl_promocode = itemView.findViewById<RelativeLayout>(R.id.rl_promocode)
            val tv_endDate = itemView.findViewById<TextView>(R.id.tv_endDate)
            val tv_endDateTitle = itemView.findViewById<TextView>(R.id.tv_endDateTitle)


            fun bindView(mContext: Context, promo_code: GetPromosales, promoCodeListner: PromoCodeListner, position: Int) {
                tvPromoCodeName.text = "${promo_code.discount_name} (${promo_code.discount_barcode})"
                tvPromoCodeMsg.text = promo_code.discount_notification_msg
                tv_endDate.text = promo_code.discount_end_date

                if (promo_code.discount_barcode == discountBarcode) {
                    tvPromoCodeApply.isEnabled = false
                    tvPromoCodeApply.setTextColor(ContextCompat.getColor(mContext, R.color.white))
                    tvPromoCodeApply.setBackgroundResource(R.drawable.blue_color_full)
                    rl_promocode.setBackgroundDrawable(mContext.resources.getDrawable(R.drawable.gray_border_gradient))
                    tvPromoCodeName.setTextColor(mContext.resources.getColor(R.color.white))
                    tvPromoCodeMsg.setTextColor(mContext.resources.getColor(R.color.white))
                    tv_endDate.setTextColor(mContext.resources.getColor(R.color.white))
                    tv_endDateTitle.setTextColor(mContext.resources.getColor(R.color.white))
                    //  Log.d("VVVV11","VVVV11")
                } else {
                    tvPromoCodeApply.isEnabled = true
                    tvPromoCodeApply.setTextColor(ContextCompat.getColor(mContext, R.color.blue))
                    tvPromoCodeApply.setBackgroundResource(R.drawable.blue_border_white)
                    rl_promocode.setBackgroundDrawable(mContext.resources.getDrawable(R.drawable.gray_border_radius))
                    tvPromoCodeName.setTextColor(mContext.resources.getColor(R.color.graylight))
                    tvPromoCodeMsg.setTextColor(mContext.resources.getColor(R.color.graylight))
                    tv_endDate.setTextColor(mContext.resources.getColor(R.color.blue))
                    tv_endDateTitle.setTextColor(mContext.resources.getColor(R.color.graylight))
                    //   Log.d("VVVV22","VVVV22")
                }
                /* if (row_index == position) {
                     rl_promocode.setBackgroundDrawable(mContext.resources.getDrawable(R.drawable.gardient_radius))
                     tvPromoCodeName.setTextColor(mContext.resources.getColor(R.color.white))
                     tvPromoCodeMsg.setTextColor(mContext.resources.getColor(R.color.white))
                     tv_endDate.setTextColor(mContext.resources.getColor(R.color.white))
                     tv_endDateTitle.setTextColor(mContext.resources.getColor(R.color.white))
                 } else {
                     rl_promocode.setBackgroundDrawable(mContext.resources.getDrawable(R.drawable.gray_border_radius))
                     tvPromoCodeName.setTextColor(mContext.resources.getColor(R.color.border_gray))
                     tvPromoCodeMsg.setTextColor(mContext.resources.getColor(R.color.border_gray))
                     tv_endDate.setTextColor(mContext.resources.getColor(R.color.border_gray))
                     tv_endDateTitle.setTextColor(mContext.resources.getColor(R.color.border_gray))

                 }*/
                rl_promocode.setOnClickListener {
                    //  row_index = position
                    discountBarcode = promo_code.discount_barcode
                    rl_promocode.setBackgroundDrawable(mContext.resources.getDrawable(R.drawable.gray_border_gradient))
                    tvPromoCodeName.setTextColor(mContext.resources.getColor(R.color.white))
                    tvPromoCodeMsg.setTextColor(mContext.resources.getColor(R.color.white))
                    tv_endDate.setTextColor(mContext.resources.getColor(R.color.white))
                    tv_endDateTitle.setTextColor(mContext.resources.getColor(R.color.white))
                    promocodeAdapter!!.notifyDataSetChanged()
                }
                tvPromoCodeApply.setOnClickListener {
                    promoCodeListner.onPromoCode(promo_code)

                }

            }
        }

        fun filter(charText: String) {

            //this.searchstring = charText
            //  searchstring = charText.toLowerCase(Locale.getDefault())
            //lists!!.clear()
            // lists!!.addAll(list)
            //  lists.clear()
            if (charText.length == 0) {
                list.clear()
                list.addAll(templist)
            } else {
                var tempList = ArrayList<GetPromosales>()
                for (wp in templist) {
                    if (wp.discount_name.toLowerCase(Locale.getDefault()).contains(charText.toLowerCase(Locale.getDefault()))) {
                        tempList.add(wp)
                    }
                }
                list.clear()
                list = tempList
            }
            promocodeAdapter!!.notifyDataSetChanged()
        }
    }

    interface PromoCodeListner {
        fun onPromoCode(promo_code: GetPromosales)
    }

    fun callEmployeePatientPromocodeList() {
        if (CUC.isNetworkAvailablewithPopup(mContext!!)) {
            mDialog = CUC.createDialog(activity!!)
            val apiService = RequetsInterface.create()
            val parameterMap = HashMap<String, String>()
            parameterMap["api_key"] = Preferences.getPreference(mContext!!, "API_KEY")
            parameterMap["cart_id"] = cart_id
            parameterMap["patient_id"] = patient_id
            parameterMap["app_status"] = app_status
            Log.i("Result", "parameterMap :  $parameterMap")
            CompositeDisposable().add(apiService.callEmployeePatientPromocodeList(parameterMap)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe({ result ->
                        if (mDialog != null && mDialog!!.isShowing)
                            mDialog!!.dismiss()
                        if (result != null) {
                            Log.i("Result", "" + result.toString())
                            if (result.status == "0") {
                                list.clear()
                                list.addAll(result.promosales)
                                templist.clear()
                                templist = result.promosales
                                promocodeAdapter!!.notifyDataSetChanged()
                                CUC.displayToast(mContext!!, result.show_status, result.message)
                            } else if (result.status == "10") {
                                //callApiKey()
                            } else {
                                list.clear()
                                promocodeAdapter!!.notifyDataSetChanged()
                                CUC.displayToast(mContext!!, result.show_status, result.message)
                            }
                        } else {
                            //  CUC.displayToast(mContext!!, "0", getString(R.string.connection_to_database_failed))
                        }
                    }, { error ->
                        if (mDialog != null && mDialog!!.isShowing)
                            mDialog!!.dismiss()
                        CUC.displayToast(mContext!!, "0", getString(R.string.connection_to_database_failed))
                        error.printStackTrace()
                    })
            )
        }
    }

    fun callEmployeeApplyPromocode() {
        if (CUC.isNetworkAvailablewithPopup(mContext!!)) {
            mDialog = CUC.createDialog(activity!!)
            val apiService = RequetsInterface.create()
            val parameterMap = HashMap<String, String>()
            parameterMap["api_key"] = Preferences.getPreference(mContext!!, "API_KEY")
            parameterMap["invoice_id"] = cart_id
            if (logindata != null) {
                parameterMap["employee_id"] = logindata!!.user_id
            }
            if (mAppBasic != null) {
                parameterMap["terminal_id"] = "${mAppBasic!!.terminal_id}"
            }
            parameterMap["employee_lat"] = "${Preferences.getPreference(mContext!!, "latitude")}"
            parameterMap["employee_log"] = "${Preferences.getPreference(mContext!!, "longitude")}"
            parameterMap["promo_code"] = discountBarcode
            Log.i("Result", "parameterMap : $parameterMap")
            CompositeDisposable().add(apiService.callEmployeeApplyPromocode(parameterMap)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe({ result ->
                        if (mDialog != null && mDialog!!.isShowing)
                            mDialog!!.dismiss()
                        if (result != null) {
                            Log.i("Result", "" + result.toString())
                            if (result.status == "0") {
                                discountBarcode = result.discount.discount_barcode
                                //   edtPromoCodeName.setText("${result.discount.discount_barcode}")
                                edtPromoCodeName.isEnabled = false
                                tvApply.text = "Remove"
                                promocodeAdapter!!.notifyDataSetChanged()
                                callListner.onRefresh()
                            } else if (result.status == "10") {
                                //callApiKey()
                            } else {
                                discountBarcode = ""
                                edtPromoCodeName.isEnabled = true
                                //      edtPromoCodeName.setText("")
                                tvApply.text = "Apply"
                                promocodeAdapter!!.notifyDataSetChanged()
                                callListner.onRefresh()
                                CUC.displayToast(mContext!!, result.show_status, result.message)
                            }
                        } else {
                            // CUC.displayToast(mContext!!, "0", getString(R.string.connection_to_database_failed))
                        }
                    }, { error ->
                        if (mDialog != null && mDialog!!.isShowing)
                            mDialog!!.dismiss()
                        CUC.displayToast(mContext!!, "0", getString(R.string.connection_to_database_failed))
                        error.printStackTrace()
                    })
            )
        }
    }

    fun callEmployeeRemovePromocode() {
        if (CUC.isNetworkAvailablewithPopup(mContext!!)) {
            mDialog = CUC.createDialog(activity!!)
            val apiService = RequetsInterface.create()
            val parameterMap = HashMap<String, String>()
            parameterMap["api_key"] = Preferences.getPreference(mContext!!, "API_KEY")
            parameterMap["invoice_id"] = cart_id
            if (logindata != null) {
                parameterMap["employee_id"] = logindata!!.user_id
            }
            if (mAppBasic != null) {
                parameterMap["terminal_id"] = "${mAppBasic!!.terminal_id}"
            }
            parameterMap["employee_lat"] = "${Preferences.getPreference(mContext!!, "latitude")}"
            parameterMap["employee_log"] = "${Preferences.getPreference(mContext!!, "longitude")}"
            Log.i("Result", "parameterMap : $parameterMap")
            CompositeDisposable().add(apiService.callEmployeeRemovePromocode(parameterMap)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe({ result ->
                        if (mDialog != null && mDialog!!.isShowing)
                            mDialog!!.dismiss()
                        if (result != null) {
                            Log.i("Result", "" + result.toString())
                            if (result.status == "0") {
                                discountBarcode = ""
                                edtPromoCodeName.isEnabled = true
                                //   edtPromoCodeName.setText("")
                                tvApply.text = "Apply"
                                promocodeAdapter!!.notifyDataSetChanged()
                                callListner.onRefresh()
                            } else if (result.status == "10") {
                                //callApiKey()
                            } else {
                                CUC.displayToast(mContext!!, result.show_status, result.message)
                            }
                        } else {
                            //   CUC.displayToast(mContext!!, "0", getString(R.string.connection_to_database_failed))
                        }
                    }, { error ->
                        if (mDialog != null && mDialog!!.isShowing)
                            mDialog!!.dismiss()
                        CUC.displayToast(mContext!!, "0", getString(R.string.connection_to_database_failed))
                        error.printStackTrace()
                    })
            )
        }
    }

    fun callEmployeeSalesApplyPromocode() {
        if (CUC.isNetworkAvailablewithPopup(mContext!!)) {
            mDialog = CUC.createDialog(activity!!)
            val apiService = RequetsInterface.create()
            val parameterMap = HashMap<String, String>()
            parameterMap["api_key"] = Preferences.getPreference(mContext!!, "API_KEY")
            if (logindata != null) {
                parameterMap["employee_id"] = logindata!!.user_id
                parameterMap["store_id"] = logindata!!.user_store_id
            }
            if (mAppBasic != null) {
                parameterMap["terminal_id"] = "${mAppBasic!!.terminal_id}"
            }
            parameterMap["employee_lat"] = "${Preferences.getPreference(mContext!!, "latitude")}"
            parameterMap["employee_log"] = "${Preferences.getPreference(mContext!!, "longitude")}"
            parameterMap["queue_id"] = queue_id
            parameterMap["patient_id"] = patient_id
            parameterMap["promocode"] = discountBarcode
            Log.i("Result", "parameterMap : $parameterMap")
            CompositeDisposable().add(apiService.callEmployeeSalesApplyPromocode(parameterMap)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe({ result ->
                        if (mDialog != null && mDialog!!.isShowing)
                            mDialog!!.dismiss()
                        if (result != null) {
                            Log.i("Result", "" + result.toString())
                            if (result.status == "0") {
                                discountBarcode = result.discount.discount_barcode
                                //   edtPromoCodeName.setText("${result.discount.discount_barcode}")
                                edtPromoCodeName.isEnabled = false
                                tvApply.text = "Remove"
                                promocodeAdapter!!.notifyDataSetChanged()
                                callListner.onRefresh()
                            } else if (result.status == "10") {
                                //callApiKey()
                            } else {
                                discountBarcode = ""
                                edtPromoCodeName.isEnabled = true
                                //  edtPromoCodeName.setText("")
                                tvApply.text = "Apply"
                                promocodeAdapter!!.notifyDataSetChanged()
                                callListner.onRefresh()
                                CUC.displayToast(mContext!!, result.show_status, result.message)
                            }
                        } else {

                            //  CUC.displayToast(mContext!!, "0", getString(R.string.connection_to_database_failed))
                        }
                    }, { error ->
                        if (mDialog != null && mDialog!!.isShowing)
                            mDialog!!.dismiss()
                        CUC.displayToast(mContext!!, "0", getString(R.string.connection_to_database_failed))
                        error.printStackTrace()
                    })
            )
        }
    }

    fun callEmployeeSalesRemovePromocode() {
        if (CUC.isNetworkAvailablewithPopup(mContext!!)) {
            mDialog = CUC.createDialog(activity!!)
            val apiService = RequetsInterface.create()
            val parameterMap = HashMap<String, String>()
            parameterMap["api_key"] = Preferences.getPreference(mContext!!, "API_KEY")
            if (logindata != null) {
                parameterMap["employee_id"] = logindata!!.user_id
                parameterMap["store_id"] = logindata!!.user_store_id
            }
            if (mAppBasic != null) {
                parameterMap["terminal_id"] = "${mAppBasic!!.terminal_id}"
            }
            parameterMap["employee_lat"] = "${Preferences.getPreference(mContext!!, "latitude")}"
            parameterMap["employee_log"] = "${Preferences.getPreference(mContext!!, "longitude")}"
            parameterMap["queue_id"] = queue_id
            parameterMap["patient_id"] = patient_id
            Log.i("Result", "parameterMap : $parameterMap")
            CompositeDisposable().add(apiService.callEmployeeSalesRemovePromocode(parameterMap)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe({ result ->
                        if (mDialog != null && mDialog!!.isShowing)
                            mDialog!!.dismiss()
                        if (result != null) {
                            Log.i("Result", "" + result.toString())
                            if (result.status == "0") {
                                discountBarcode = ""
                                edtPromoCodeName.isEnabled = true
                                // edtPromoCodeName.setText("")
                                tvApply.text = "Apply"
                                promocodeAdapter!!.notifyDataSetChanged()
                                callListner.onRefresh()
                            } else if (result.status == "10") {
                                //callApiKey()
                            } else {
                                CUC.displayToast(mContext!!, result.show_status, result.message)
                            }
                        } else {
                            //  CUC.displayToast(mContext!!, "0", getString(R.string.connection_to_database_failed))
                        }
                    }, { error ->
                        if (mDialog != null && mDialog!!.isShowing)
                            mDialog!!.dismiss()
                        CUC.displayToast(mContext!!, "0", getString(R.string.connection_to_database_failed))
                        error.printStackTrace()
                    })
            )
        }
    }

    interface onCallListner {
        fun onRefresh()
    }

}
