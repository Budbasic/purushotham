package com.budbasic.posconsole.dialogs

import android.annotation.SuppressLint
import android.app.Dialog
import android.app.DialogFragment
import android.content.Context
import android.os.Bundle
import android.support.v7.widget.CardView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import com.budbasic.customer.global.Preferences
import com.budbasic.global.CUC
import com.budbasic.posconsole.R
import com.budbasic.posconsole.webservice.RequetsInterface
import com.kotlindemo.model.Patient_data
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

@SuppressLint("ValidFragment")
class AddEditPatientNoteDialog constructor(val mcontext: Context, val employee_id: String, var data: Patient_data, val isnoteupdate: IsNoteUpdata) : DialogFragment() {

    lateinit var edt_note_message: EditText
    lateinit var cv_submit: CardView
    lateinit var cv_cancel: CardView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(DialogFragment.STYLE_NO_TITLE, R.style.TransparentProgressDialog)
    }


    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View {
        val v = inflater!!.inflate(R.layout.dialog_patient_note_edit, container, false)

        init(v)

        return v
    }


    fun init(v: View) {
        edt_note_message = v.findViewById(R.id.edt_note_message) as EditText
        cv_submit = v.findViewById(R.id.cv_submit)
        cv_cancel = v.findViewById(R.id.cv_cancel)

        if (data.patient_notes.isNotEmpty() && !data.patient_notes.equals("null", true)) {
            edt_note_message.setText(data.patient_notes)
            edt_note_message.setSelection(edt_note_message.text.toString().length)
        }

        cv_submit.setOnClickListener {
            apiCallForEditNote()
        }
        cv_cancel.setOnClickListener {
            this@AddEditPatientNoteDialog.dismiss()
        }
    }

    private fun apiCallForEditNote() {
        var mDialog: Dialog = Dialog(mcontext)

        try {
            if (!activity!!.isFinishing) {
                mDialog = CUC.createDialog(activity!!)
                mDialog.show()
                val apiService = RequetsInterface.Factory.create()
                val parameterMap = HashMap<String, String>()


                parameterMap["api_key"] = "${Preferences.getPreference(mcontext!!, "API_KEY")}"
                parameterMap["employee_id"] = employee_id
                parameterMap["patient_id"] = data!!.patient_id
                parameterMap["patient_notes"] = edt_note_message.text.toString()


                CompositeDisposable().add(apiService.callEmployeePatientNote(parameterMap)
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.io())
                        .subscribe({ result ->
                            if (mDialog != null && mDialog.isShowing)
                                mDialog.dismiss()
                            if (result != null && result!!.status.equals("0")) {
                                isnoteupdate.onResponseChange(edt_note_message.text.toString())
                                this@AddEditPatientNoteDialog.dismiss()
                            }

                        }, { error ->
                            if (mDialog != null && mDialog.isShowing)
                                mDialog.dismiss()
                            //onChange.isSelected = (!isBanned)
                            CUC.displayToast(mcontext!!, "0", getString(R.string.connection_to_database_failed))
                            error.printStackTrace()
                        })
                )
            }
        } catch (e: Exception) {
            if (mDialog != null && mDialog.isShowing) {
                mDialog.dismiss()
            }
            e.printStackTrace()
        }

    }

    interface IsNoteUpdata {
        fun onResponseChange(updatedNote: String)
    }

}
