package com.budbasic.posconsole.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.budbasic.global.CUC;
import com.budbasic.posconsole.R;
import com.budbasic.posconsole.global.Apppreference;
import com.budbasic.posconsole.reception.ProductsFragment;
import com.kotlindemo.model.GetSalesProduct;
import com.kotlindemo.model.GetStatus;

public class ProductquantityDialog extends Dialog {

    private TextView tv_product_name,tv_stock,tv_tottal_qty,tv_tottal_price,tv_short_name,tv_add_cart;
    private EditText tv_first_text,disc_per,disc_amount;
    private LinearLayout ll_add_cart;
    GetSalesProduct getProducts;
    private Context context;
    GetStatus result;
    ProductsFragment productsFragment1; String weigth;
    private Apppreference apppreference;
    public ProductquantityDialog(Context context, GetStatus result, GetSalesProduct getProducts, ProductsFragment productsFragment1, String s) {
        super(context);
        this.context=context;
        this.getProducts=getProducts;
        this.result=result;
        this.productsFragment1=productsFragment1;
        this.weigth=s;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.product_qty_dialog_new);

        tv_product_name=findViewById(R.id.tv_product_name);
        tv_stock=findViewById(R.id.tv_stock);
        tv_tottal_qty=findViewById(R.id.tv_tottal_qty);
        tv_tottal_price=findViewById(R.id.tv_tottal_price);
        tv_first_text=findViewById(R.id.tv_first_text);
        disc_amount=findViewById(R.id.disc_amount);
        disc_per=findViewById(R.id.disc_per);
        ll_add_cart=findViewById(R.id.ll_add_cart);
        tv_short_name=findViewById(R.id.tv_short_name);
        tv_add_cart=findViewById(R.id.tv_add_cart);

        tv_add_cart.setText("Add To Cart");
        tv_first_text.setImeOptions(EditorInfo.IME_FLAG_NO_EXTRACT_UI | EditorInfo.IME_ACTION_DONE);
        disc_per.setImeOptions(EditorInfo.IME_FLAG_NO_EXTRACT_UI | EditorInfo.IME_ACTION_DONE);
        disc_amount.setImeOptions(EditorInfo.IME_FLAG_NO_EXTRACT_UI | EditorInfo.IME_ACTION_DONE);
        tv_product_name.setText(""+getProducts.getPacket_name());
        if (result.getInventory_qty().equals("0")){
            tv_stock.setTextColor(context.getResources().getColor(R.color.red));
            tv_stock.setText("Stack Unavailable");
        }else {
            tv_stock.setText("Stack "+result.getInventory_qty()+" "+getProducts.getUnit_short_name());
        }

        if (!weigth.equalsIgnoreCase("0")){
            tv_first_text.setText(weigth);
            tv_first_text.setSelection(tv_first_text.getText().toString().length());
        }

        tv_tottal_price.setText(""+getProducts.getPacket_base_price());
        tv_short_name.setText(""+getProducts.getUnit_short_name());
        apppreference=new Apppreference(context);
        /*if(mScale != null) {
            mScale.disconnect();
        }*/
      /*  if(mScale == null) {
            System.out.println("aaaaaaaaaaa  scale not found ");
            String identifier = apppreference.getidentifier();
            ConnectionInfo.InterfaceType interfaceType = ConnectionInfo.InterfaceType.valueOf(apppreference.getinterfaceType());

            StarDeviceManager starDeviceManager = new StarDeviceManager(context);

            ConnectionInfo connectionInfo;

            switch (interfaceType) {
                default:
                case BLE:
                    connectionInfo = new ConnectionInfo.Builder()
                            .setBleInfo(identifier)
                            .build();
                    break;
                case USB:
                    connectionInfo = new ConnectionInfo.Builder()
                            .setUsbInfo(identifier)
                            .setBaudRate(1200)
                            .build();
                    break;
            }

            mScale = starDeviceManager.createScale(connectionInfo);
            mScale.connect(mScaleCallback);
        }*/
        ll_add_cart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String entervalue=tv_first_text.getText().toString().trim();
                String percentagedes=disc_per.getText().toString().trim();
                String amountdes=disc_amount.getText().toString().trim();
                Float persecent = 0.0f; int i=0;
                if (entervalue.isEmpty()){
                    CUC.Companion.displayToast(context, "Please Enter Quantity", result.getMessage());
                    Toast.makeText(context, "Please Enter Quantity", Toast.LENGTH_SHORT).show();
                }else {
                    if (result.getInventory_qty().equals("0")){
                        Toast.makeText(context, "Currrently Product Unavailable", Toast.LENGTH_SHORT).show();
                    }else {
                        if (getProducts.getUnit_short_name().equalsIgnoreCase("ea")){
                            Double value=Double.parseDouble(entervalue);
                            if (value<=Float.parseFloat(result.getInventory_qty())){
                                if (!percentagedes.isEmpty()&&!amountdes.isEmpty()){
                                    Toast.makeText(context, "Enter Any One Discount", Toast.LENGTH_SHORT).show();
                                }else {
                                    if (!percentagedes.isEmpty()){
                                        persecent=Float.parseFloat(percentagedes);
                                        i=1;
                                    }
                                    if (!amountdes.isEmpty()){
                                        persecent=Float.parseFloat(amountdes);
                                        i=2;
                                    }

                                    productsFragment1.sendToCartea(getProducts,value,persecent,i);
                                    dismiss();

                                }


                            }else {
                                Toast.makeText(context, "Enter valid Quantity", Toast.LENGTH_SHORT).show();
                                CUC.Companion.displayToast(context, "Store Quantity more than you entered", result.getMessage());
                            }
                        }else {
                            Float value=Float.parseFloat(entervalue);
                            if (value<=Float.parseFloat(result.getInventory_qty())){
                                if (!percentagedes.isEmpty()&&!amountdes.isEmpty()){
                                    Toast.makeText(context, "Enter Any One Discount", Toast.LENGTH_SHORT).show();
                                }else {
                                    if (!percentagedes.isEmpty()){
                                        persecent=Float.parseFloat(percentagedes);
                                        i=1;
                                    }
                                    if (!amountdes.isEmpty()){
                                        persecent=Float.parseFloat(amountdes);
                                        i=2;
                                    }

                                    productsFragment1.sendToCart(getProducts,value,persecent,i);
                                    dismiss();
                                }


                            }else {
                                Toast.makeText(context, "Enter valid Quantity", Toast.LENGTH_SHORT).show();
                                CUC.Companion.displayToast(context, "Store Quantity more than you entered", result.getMessage());
                            }
                        }


                    }

                }
            }
        });
    }
}
