package com.budbasic.posconsole.dialogs;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.budbasic.customer.global.Preferences;
import com.budbasic.global.CUC;
import com.budbasic.posconsole.R;
import com.budbasic.posconsole.adapter.ExpenseCashDrawerAdapter;
import com.budbasic.posconsole.reception.CashDrawerFragment;
import com.budbasic.posconsole.webservice.RequetsInterface;
import com.google.gson.Gson;
import com.kotlindemo.model.GetDenominations;
import com.kotlindemo.model.GetDenominationsdata;
import com.kotlindemo.model.GetEmpExpenses;
import com.kotlindemo.model.GetEmployeeAppSetting;
import com.kotlindemo.model.GetEmployeeExpanses;
import com.kotlindemo.model.GetEmployeeLogin;
import com.kotlindemo.model.GetTillClose;
import com.kotlindemo.model.GetsalesTerminalsdata;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

public class AddCashDrop {

    private TextView tv_expenses,tv_denomination,tv_closedrawer,tv_submit,tv_save,tv_cancel;
    private LinearLayout layout_expense,layout_denomination,layout_cashdrop,layoutadd_one,layout_add_two;
    private RecyclerView recycle_expense;
    private List<GetEmployeeExpanses> getEmployeeExpansesList;
    private List<GetDenominationsdata> getDenominationsdata;
    public Dialog mDialog;
    private GetEmployeeLogin logindata;
    private int s_day,s_month,s_year;
    private ExpenseCashDrawerAdapter expensesAdapter;
    GetsalesTerminalsdata getsalesTerminalsdata;
    CashDrawerFragment cashDrawerFragment;
    private EditText tv_pennies,tv_nickels,tv_dimes,tv_quartes,tv_ones,tv_fives,tv_tens,tv_twenties,
            tv_fifties,tv_hundreds,tv_date,tv_begingbal,tv_cashsales,tv_debitsales,tv_expensess,tv_expectedcount,tv_total_sales,
    tv_cash_draweramt,tv_actual_draweramt,tv_varience;
    private Calendar calendar;
    int day,month,year;
    GetEmployeeAppSetting mBAsis;

    public void showDialog(final Context context, GetsalesTerminalsdata getsalesTerminalsdata, GetEmployeeLogin logindata,
                           CashDrawerFragment cashDrawerFragment) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.close_cash_drawer);
        tv_expenses=dialog.findViewById(R.id.tv_expenses);
        tv_denomination=dialog.findViewById(R.id.tv_denomination);
        tv_closedrawer=dialog.findViewById(R.id.tv_closedrawer);
        layout_expense=dialog.findViewById(R.id.layout_expense);
        recycle_expense=dialog.findViewById(R.id.recycle_expense);
        layout_denomination=dialog.findViewById(R.id.layout_denomination);
        layout_cashdrop=dialog.findViewById(R.id.layout_cashdrop);
        tv_pennies=dialog.findViewById(R.id.tv_pennies);
        tv_nickels=dialog.findViewById(R.id.tv_nickels);
        tv_dimes=dialog.findViewById(R.id.tv_dimes);
        tv_quartes=dialog.findViewById(R.id.tv_quartes);
        tv_ones=dialog.findViewById(R.id.tv_ones);
        tv_fives=dialog.findViewById(R.id.tv_fives);
        tv_tens=dialog.findViewById(R.id.tv_tens);
        tv_twenties=dialog.findViewById(R.id.tv_twenties);
        tv_fifties=dialog.findViewById(R.id.tv_fifties);
        tv_hundreds=dialog.findViewById(R.id.tv_hundreds);
        tv_submit=dialog.findViewById(R.id.tv_submit);
        tv_date=dialog.findViewById(R.id.tv_date);
        tv_begingbal=dialog.findViewById(R.id.tv_begingbal);
        tv_cashsales=dialog.findViewById(R.id.tv_cashsales);
        tv_debitsales=dialog.findViewById(R.id.tv_debitsales);
        tv_expensess=dialog.findViewById(R.id.tv_expensess);
        tv_expectedcount=dialog.findViewById(R.id.tv_expectedcount);
        tv_total_sales=dialog.findViewById(R.id.tv_total_sales);
        tv_cash_draweramt=dialog.findViewById(R.id.tv_cash_draweramt);
        tv_actual_draweramt=dialog.findViewById(R.id.tv_actual_draweramt);
        tv_varience=dialog.findViewById(R.id.tv_varience);
        tv_save=dialog.findViewById(R.id.tv_save);
        tv_cancel=dialog.findViewById(R.id.tv_cancel);
        layoutadd_one=dialog.findViewById(R.id.layoutadd_one);
        layout_add_two=dialog.findViewById(R.id.layout_add_two);

        this.cashDrawerFragment=cashDrawerFragment;
        layoutadd_one.setVisibility(View.GONE);
        layout_add_two.setVisibility(View.GONE);
        mBAsis = new Gson().fromJson(Preferences.INSTANCE.getPreference(context, "AppSetting"), GetEmployeeAppSetting.class);
        tv_pennies.setImeOptions(EditorInfo.IME_FLAG_NO_EXTRACT_UI | EditorInfo.IME_ACTION_DONE);
        tv_nickels.setImeOptions(EditorInfo.IME_FLAG_NO_EXTRACT_UI | EditorInfo.IME_ACTION_DONE);
        tv_dimes.setImeOptions(EditorInfo.IME_FLAG_NO_EXTRACT_UI | EditorInfo.IME_ACTION_DONE);
        tv_quartes.setImeOptions(EditorInfo.IME_FLAG_NO_EXTRACT_UI | EditorInfo.IME_ACTION_DONE);
        tv_ones.setImeOptions(EditorInfo.IME_FLAG_NO_EXTRACT_UI | EditorInfo.IME_ACTION_DONE);
        tv_fives.setImeOptions(EditorInfo.IME_FLAG_NO_EXTRACT_UI | EditorInfo.IME_ACTION_DONE);
        tv_tens.setImeOptions(EditorInfo.IME_FLAG_NO_EXTRACT_UI | EditorInfo.IME_ACTION_DONE);
        tv_twenties.setImeOptions(EditorInfo.IME_FLAG_NO_EXTRACT_UI | EditorInfo.IME_ACTION_DONE);
        tv_fifties.setImeOptions(EditorInfo.IME_FLAG_NO_EXTRACT_UI | EditorInfo.IME_ACTION_DONE);
        tv_hundreds.setImeOptions(EditorInfo.IME_FLAG_NO_EXTRACT_UI | EditorInfo.IME_ACTION_DONE);

        editable(tv_begingbal);
        editable(tv_cashsales);
        editable(tv_debitsales);
        editable(tv_expensess);
        editable(tv_expectedcount);
        editable(tv_total_sales);
        editable(tv_cash_draweramt);
        editable(tv_actual_draweramt);
        editable(tv_varience);

       /* tv_begingbal.setImeOptions(EditorInfo.IME_FLAG_NO_EXTRACT_UI | EditorInfo.IME_ACTION_DONE);
        tv_cashsales.setImeOptions(EditorInfo.IME_FLAG_NO_EXTRACT_UI | EditorInfo.IME_ACTION_DONE);
        tv_debitsales.setImeOptions(EditorInfo.IME_FLAG_NO_EXTRACT_UI | EditorInfo.IME_ACTION_DONE);
        tv_expensess.setImeOptions(EditorInfo.IME_FLAG_NO_EXTRACT_UI | EditorInfo.IME_ACTION_DONE);
        tv_expectedcount.setImeOptions(EditorInfo.IME_FLAG_NO_EXTRACT_UI | EditorInfo.IME_ACTION_DONE);
        tv_total_sales.setImeOptions(EditorInfo.IME_FLAG_NO_EXTRACT_UI | EditorInfo.IME_ACTION_DONE);
        tv_cash_draweramt.setImeOptions(EditorInfo.IME_FLAG_NO_EXTRACT_UI | EditorInfo.IME_ACTION_DONE);
        tv_actual_draweramt.setImeOptions(EditorInfo.IME_FLAG_NO_EXTRACT_UI | EditorInfo.IME_ACTION_DONE);
        tv_varience.setImeOptions(EditorInfo.IME_FLAG_NO_EXTRACT_UI | EditorInfo.IME_ACTION_DONE);
*/
        defaultset(tv_pennies);
        defaultset(tv_nickels);
        defaultset(tv_dimes);
        defaultset(tv_quartes);
        defaultset(tv_ones);
        defaultset(tv_fives);
        defaultset(tv_tens);
        defaultset(tv_twenties);
        defaultset(tv_hundreds);
        defaultset(tv_fifties);

        tv_pennies.setSelection(tv_pennies.getText().toString().length());
        tv_nickels.setSelection(tv_nickels.getText().toString().length());
        tv_dimes.setSelection(tv_dimes.getText().toString().length());
        tv_quartes.setSelection(tv_quartes.getText().toString().length());
        tv_ones.setSelection(tv_ones.getText().toString().length());
        tv_fives.setSelection(tv_fives.getText().toString().length());
        tv_tens.setSelection(tv_tens.getText().toString().length());
        tv_twenties.setSelection(tv_twenties.getText().toString().length());
        tv_hundreds.setSelection(tv_hundreds.getText().toString().length());
        tv_fifties.setSelection(tv_fifties.getText().toString().length());

        calendar=Calendar.getInstance();
        day=calendar.get(Calendar.DAY_OF_MONTH);
        month=calendar.get(Calendar.MONTH);
        year=calendar.get(Calendar.YEAR);
        int hr=calendar.get(Calendar.HOUR);
        int mn=calendar.get(Calendar.MINUTE);
        int sec=calendar.get(Calendar.SECOND);


        tv_date.setText(""+day+"-"+(month+1)+"-"+year+" "+hr+":"+mn+":"+sec);
        tv_date.setFocusable(false);
        tv_date.setFocusableInTouchMode(false);
        tv_date.setClickable(false);

        this.getsalesTerminalsdata=getsalesTerminalsdata;
        this.logindata=logindata;
        recycle_expense.setLayoutManager(new GridLayoutManager(context,1));
        getEmployeeExpansesList=new ArrayList<>();
        getDenominationsdata=new ArrayList<>();
        Calendar calendar=Calendar.getInstance();
        s_day=calendar.get(Calendar.DAY_OF_MONTH);
        s_month=calendar.get(Calendar.MONTH);
        s_year=calendar.get(Calendar.YEAR);
        expensesAdapter=new ExpenseCashDrawerAdapter(context,getEmployeeExpansesList);
        recycle_expense.setAdapter(expensesAdapter);

        tv_expenses.setBackgroundColor(context.getResources().getColor(R.color.light_blue_500));
        tv_denomination.setBackgroundColor(context.getResources().getColor(R.color.graylight));
        tv_closedrawer.setBackgroundColor(context.getResources().getColor(R.color.graylight));
        layout_expense.setVisibility(View.VISIBLE);
        layout_denomination.setVisibility(View.GONE);
        layout_cashdrop.setVisibility(View.GONE);
        getExpensesList(context);

        tv_expenses.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tv_expenses.setBackgroundColor(context.getResources().getColor(R.color.light_blue_500));
                tv_denomination.setBackgroundColor(context.getResources().getColor(R.color.graylight));
                tv_closedrawer.setBackgroundColor(context.getResources().getColor(R.color.graylight));
                layout_expense.setVisibility(View.VISIBLE);
                layout_denomination.setVisibility(View.GONE);
                layout_cashdrop.setVisibility(View.GONE);
                getExpensesList(context);
            }
        });
        tv_denomination.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tv_denomination.setBackgroundColor(context.getResources().getColor(R.color.light_blue_500));
                tv_expenses.setBackgroundColor(context.getResources().getColor(R.color.graylight));
                tv_closedrawer.setBackgroundColor(context.getResources().getColor(R.color.graylight));
                layout_expense.setVisibility(View.GONE);
                layout_cashdrop.setVisibility(View.GONE);
                layout_denomination.setVisibility(View.VISIBLE);
                getDenominations(context);
            }
        });

        tv_closedrawer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tv_closedrawer.setBackgroundColor(context.getResources().getColor(R.color.light_blue_500));
                tv_denomination.setBackgroundColor(context.getResources().getColor(R.color.graylight));
                tv_expenses.setBackgroundColor(context.getResources().getColor(R.color.graylight));
                layout_expense.setVisibility(View.GONE);
                layout_denomination.setVisibility(View.GONE);
                layout_cashdrop.setVisibility(View.VISIBLE);
                getClosetill(context,0,dialog);

            }
        });

        tv_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendDenominations(context);
            }
        });
        tv_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getClosetill(context,1,dialog);
                Toast.makeText(context, "close till save", Toast.LENGTH_SHORT).show();
            }
        });
        tv_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });


        dialog.show();
        dialog.setCancelable(true);

    }
    public void defaultset(EditText editText){
        editText.setText("0");
    }

    public void editable(EditText editText){
        editText.setFocusable(false);
        editText.setFocusableInTouchMode(false);
        editText.setClickable(false);

    }

    public void getExpensesList(final Context context){
        getEmployeeExpansesList.clear();
        mDialog = CUC.Companion.createDialog((Activity) context);
        RequetsInterface apiService = RequetsInterface.Factory.create();
        Map parameterMap =new HashMap<String, String>();
        parameterMap.put("api_key", String.valueOf(Preferences.INSTANCE.getPreference((context), "API_KEY")));
        if (logindata != null) {
            parameterMap.put("employee_id", logindata.getUser_id());
           // parameterMap.put("store_id", logindata.getUser_store_id());
        }

        parameterMap.put("terminal_id",getsalesTerminalsdata.getTerminal_id());
        parameterMap.put("from_date",s_year+"-"+(s_month+1)+"-"+s_day+" 00:00:00");
        parameterMap.put("to_date",s_year+"-"+(s_month+1)+"-"+s_day+" 23:59:59");

        System.out.println("aaaaaaaaaaa  parameterMap expenses  "+parameterMap);
        Log.i("aaaaaaaa   Result", "parameterMap : " + parameterMap);
        (new CompositeDisposable()).add(apiService.callExpensesterminal(parameterMap).
                observeOn(AndroidSchedulers.mainThread()).
                subscribeOn(Schedulers.io()).subscribe((Consumer)(new Consumer() {
            // $FF: synthetic method
            // $FF: bridge method
            public void accept(Object var1) {
                this.accept((GetEmpExpenses)var1);
            }

            public final void accept(GetEmpExpenses result) {
                System.out.println("aaaaaaaaa  result  "+result.toString());
                mDialog.cancel();
                if (result != null) {
                    mDialog.cancel();
                    System.out.println("aaaaaaaaa  result expenses  "+result.toString());
                    Log.i("aaaaaaa   Result", "" + result.toString());
                    if (result.getExpense_data().size()>0){
                        getEmployeeExpansesList.addAll(result.getExpense_data());
                        expensesAdapter.setRefresh(getEmployeeExpansesList);
                        CUC.Companion.displayToast((Context)context, "Sucess", "Sucess");
                    }else {
                        CUC.Companion.displayToast((Context)context, "No Data", "No Data");
                    }
                } else {
                    mDialog.cancel();
                }
            }
        }), (Consumer)(new Consumer() {
            // $FF: synthetic method
            // $FF: bridge method
            public void accept(Object var1) {
                this.accept((Throwable)var1);
            }

            public final void accept(Throwable error) {
                mDialog.cancel();
                Toast.makeText(context, context.getResources().getString(R.string.connection_to_database_failed), Toast.LENGTH_SHORT).show();
                error.printStackTrace();
                System.out.println("aaaaaaaa  error update "+error.getMessage());
            }
        })));
    }


    public void getDenominations(final Context context){
        getDenominationsdata.clear();
        mDialog = CUC.Companion.createDialog((Activity) context);
        RequetsInterface apiService = RequetsInterface.Factory.create();
        Map parameterMap =new HashMap<String, String>();
        parameterMap.put("api_key", String.valueOf(Preferences.INSTANCE.getPreference((context), "API_KEY")));
        if (logindata != null) {
            parameterMap.put("emp_id", logindata.getUser_id());
            parameterMap.put("store_id", logindata.getUser_store_id());
        }

        parameterMap.put("terminal_id",getsalesTerminalsdata.getTerminal_id());
        parameterMap.put("update","0");

        System.out.println("aaaaaaaaaaa  parameterMap denominations "+parameterMap);
        Log.i("aaaaaaaa   Result", "parameterMap : " + parameterMap);
        (new CompositeDisposable()).add(apiService.calldenominations(parameterMap).
                observeOn(AndroidSchedulers.mainThread()).
                subscribeOn(Schedulers.io()).subscribe((Consumer)(new Consumer() {
            // $FF: synthetic method
            // $FF: bridge method
            public void accept(Object var1) {
                this.accept((GetDenominations)var1);
            }

            public final void accept(GetDenominations result) {
                System.out.println("aaaaaaaaa  result  "+result.toString());
                mDialog.cancel();
                if (result != null) {
                    mDialog.cancel();
                    System.out.println("aaaaaaaaa  result denomination  "+result.toString());
                    Log.i("aaaaaaa   Result", "" + result.toString());
                    if (result.getStatus().equalsIgnoreCase("2")){
                        if (result.getNew_data().size()>0){
                            getDenominationsdata.addAll(result.getNew_data());
                            tv_pennies.setText(result.getNew_data().get(0).getTl_pennies());
                            tv_nickels.setText(result.getNew_data().get(0).getTl_nickels());
                            tv_dimes.setText(result.getNew_data().get(0).getTl_dimes());
                            tv_quartes.setText(result.getNew_data().get(0).getTl_quarters());
                            tv_ones.setText(result.getNew_data().get(0).getTl_ones());
                            tv_fives.setText(result.getNew_data().get(0).getTl_fives());
                            tv_tens.setText(result.getNew_data().get(0).getTl_tens());
                            tv_twenties.setText(result.getNew_data().get(0).getTl_twenties());
                            tv_fifties.setText(result.getNew_data().get(0).getTl_fifties());
                            tv_hundreds.setText(result.getNew_data().get(0).getTl_hundreds());

                            CUC.Companion.displayToast((Context)context, "Sucess", "Sucess");
                        }else {
                            CUC.Companion.displayToast((Context)context, "No Data", "No Data");

                        }
                    }else {
                        cashDrawerFragment.showAlert(result.getMessage());
                    }

                } else {
                    mDialog.cancel();
                }
            }
        }), (Consumer)(new Consumer() {
            // $FF: synthetic method
            // $FF: bridge method
            public void accept(Object var1) {
                this.accept((Throwable)var1);
            }

            public final void accept(Throwable error) {
                mDialog.cancel();
                Toast.makeText(context, context.getResources().getString(R.string.connection_to_database_failed), Toast.LENGTH_SHORT).show();
                error.printStackTrace();
                System.out.println("aaaaaaaa  error update "+error.getMessage());
            }
        })));
    }

    public void sendDenominations(final Context context){
        getEmployeeExpansesList.clear();
        mDialog = CUC.Companion.createDialog((Activity) context);
        RequetsInterface apiService = RequetsInterface.Factory.create();
        Map parameterMap =new HashMap<String, String>();
        parameterMap.put("api_key", String.valueOf(Preferences.INSTANCE.getPreference((context), "API_KEY")));
        if (logindata != null) {
            parameterMap.put("emp_id", logindata.getUser_id());
            parameterMap.put("store_id", logindata.getUser_store_id());
        }

        parameterMap.put("terminal_id",getsalesTerminalsdata.getTerminal_id());
        parameterMap.put("update","1");
        parameterMap.put("pennies",tv_pennies.getText().toString());
        parameterMap.put("nickels",tv_nickels.getText().toString());
        parameterMap.put("dimes",tv_dimes.getText().toString());
        parameterMap.put("ones",tv_ones.getText().toString());
        parameterMap.put("tens",tv_tens.getText().toString());
        parameterMap.put("fifties",tv_fifties.getText().toString());
        parameterMap.put("quarters",tv_quartes.getText().toString());
        parameterMap.put("fives",tv_fives.getText().toString());
        parameterMap.put("twenties",tv_twenties.getText().toString());
        parameterMap.put("hundreds",tv_hundreds.getText().toString());

        System.out.println("aaaaaaaaaaa  parameterMap send denominations "+parameterMap);
        Log.i("aaaaaaaa   Result", "parameterMap : " + parameterMap);
        (new CompositeDisposable()).add(apiService.calldenominations(parameterMap).
                observeOn(AndroidSchedulers.mainThread()).
                subscribeOn(Schedulers.io()).subscribe((Consumer)(new Consumer() {
            // $FF: synthetic method
            // $FF: bridge method
            public void accept(Object var1) {
                this.accept((GetDenominations)var1);
            }

            public final void accept(GetDenominations result) {
                System.out.println("aaaaaaaaa  result  "+result.toString());
                mDialog.cancel();
                if (result != null) {
                    mDialog.cancel();
                    System.out.println("aaaaaaaaa  result send denamiantions  "+result.toString());
                    Log.i("aaaaaaa   Result", "" + result.toString());
                    if (result.getStatus().equalsIgnoreCase("0")){
                        getDenominations(context);
                        /*if (result.getNew_data().size()>0){
                            getDenominationsdata.addAll(result.getNew_data());

                            CUC.Companion.displayToast((Context)context, "Sucess", "Sucess");
                        }else {
                            CUC.Companion.displayToast((Context)context, "No Data", "No Data");

                        }*/
                    }else {

                    }

                } else {
                    mDialog.cancel();
                }
            }
        }), (Consumer)(new Consumer() {
            // $FF: synthetic method
            // $FF: bridge method
            public void accept(Object var1) {
                this.accept((Throwable)var1);
            }

            public final void accept(Throwable error) {
                mDialog.cancel();
                Toast.makeText(context, context.getResources().getString(R.string.connection_to_database_failed), Toast.LENGTH_SHORT).show();
                error.printStackTrace();
                System.out.println("aaaaaaaa  error update "+error.getMessage());
            }
        })));
    }

    public void getClosetill(final Context context, int i, final Dialog dialog){
        getEmployeeExpansesList.clear();
        mDialog = CUC.Companion.createDialog((Activity) context);
        RequetsInterface apiService = RequetsInterface.Factory.create();
        Map parameterMap =new HashMap<String, String>();
        parameterMap.put("api_key", String.valueOf(Preferences.INSTANCE.getPreference((context), "API_KEY")));
        if (logindata != null) {
            parameterMap.put("emp_id", logindata.getUser_id());
            parameterMap.put("store_id", logindata.getUser_store_id());
        }

        parameterMap.put("terminal_id",getsalesTerminalsdata.getTerminal_id());
        parameterMap.put("till_close",""+i);

        System.out.println("aaaaaaaaaaa  parameterMap close tilll "+parameterMap);
        Log.i("aaaaaaaa   Result", "parameterMap : " + parameterMap);
        (new CompositeDisposable()).add(apiService.callTillClose(parameterMap).
                observeOn(AndroidSchedulers.mainThread()).
                subscribeOn(Schedulers.io()).subscribe((Consumer)(new Consumer() {
            // $FF: synthetic method
            // $FF: bridge method
            public void accept(Object var1) {
                this.accept((GetTillClose)var1);
            }

            public final void accept(GetTillClose result) {
                System.out.println("aaaaaaaaa  result  "+result.toString());
                mDialog.cancel();
                if (result != null) {
                    mDialog.cancel();
                    System.out.println("aaaaaaaaa  result close till  "+result.toString());
                    Log.i("aaaaaaa   Result", "" + result.toString());
                    if (result.getStatus().equalsIgnoreCase("1")){
                        tv_begingbal.setText("$ "+result.getBalance());
                        tv_cashsales.setText("$ "+result.getCash());
                        tv_debitsales.setText("$ "+result.getNet());
                        tv_expensess.setText("$ "+result.getExpenses());
                        tv_expectedcount.setText("$ "+result.getExpected_cash());
                        tv_total_sales.setText("$ "+result.getTotal_sale());
                        tv_cash_draweramt.setText("$ "+result.getBalance());
                        tv_actual_draweramt.setText("$ "+result.getActual_till());
                        tv_varience.setText("$ "+result.getVariance());

                        if (result.getClosetillCheck().equalsIgnoreCase("0")){
                            cashDrawerFragment.showAlert(result.getMessage());
                            dialog.dismiss();
                        }
                    }else {
                        tv_save.setClickable(false);
                        tv_save.setEnabled(false);
                        cashDrawerFragment.showAlert(result.getMessage());
                    }

                } else {
                    mDialog.cancel();
                }
            }
        }), (Consumer)(new Consumer() {
            // $FF: synthetic method
            // $FF: bridge method
            public void accept(Object var1) {
                this.accept((Throwable)var1);
            }

            public final void accept(Throwable error) {
                mDialog.cancel();
                Toast.makeText(context, context.getResources().getString(R.string.connection_to_database_failed), Toast.LENGTH_SHORT).show();
                error.printStackTrace();
                System.out.println("aaaaaaaa  error update "+error.getMessage());
            }
        })));
    }



}
