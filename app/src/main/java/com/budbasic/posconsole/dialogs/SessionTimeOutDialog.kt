package com.budbasic.posconsole.dialogs

import android.app.Activity
import android.util.Log
import com.github.omadahealth.lollipin.lib.managers.AppLockActivity


class SessionTimeOutDialog : AppLockActivity() {
    override fun onPinSuccess(attempts: Int) {
        Log.i("SessionTimeOutDialog", "onPinSuccess($attempts)")
        setResult(Activity.RESULT_OK)
    }

    override fun onPinFailure(attempts: Int) {
    }

    override fun showForgotDialog() {
    }


}