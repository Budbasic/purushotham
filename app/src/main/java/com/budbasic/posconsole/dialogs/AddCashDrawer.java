package com.budbasic.posconsole.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.budbasic.global.CUC;
import com.budbasic.posconsole.R;
import com.budbasic.posconsole.adapter.DepositItemAdapter;
import com.budbasic.posconsole.adapter.ExpensesItemAdapter;
import com.budbasic.posconsole.reception.CashDrawerFragment;
import com.budbasic.posconsole.reception.DepositFragment;
import com.budbasic.posconsole.reception.ExpensesFragment;
import com.budbasic.posconsole.reception.SalesActivity;
import com.budbasic.posconsole.sales.UserInfoDetails;
import com.kotlindemo.model.Favourite;
import com.kotlindemo.model.GetCartData;
import com.kotlindemo.model.GetDepositbanklist;
import com.kotlindemo.model.GetExpensesItems;
import com.kotlindemo.model.GetSalesProduct;
import com.kotlindemo.model.GetStatus;
import com.kotlindemo.model.GetStoreDatas;
import com.kotlindemo.model.GetStoreOpen;
import com.kotlindemo.model.GetTillOpen;
import com.kotlindemo.model.GetsalesTerminalsdata;
import com.kotlindemo.model.notesdata;

import java.util.ArrayList;

public class AddCashDrawer {

    private EditText et_creditcash,et_cashdispered;
    private TextView tv_cancel,tv_save,et_activedrawers,et_noofdrawers,et_prevdayamount;
    LinearLayout layout_storeopen,layout_tillopen;

    public void showDialog(Context context, GetStoreOpen result, final CashDrawerFragment cashDrawerFragment, GetStoreDatas getStoreDatas, final int storeopen) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.store_open_dailog);
        et_prevdayamount=dialog.findViewById(R.id.et_prevdayamount);
        et_creditcash=dialog.findViewById(R.id.et_creditcash);
        et_noofdrawers=dialog.findViewById(R.id.et_noofdrawers);
        et_activedrawers=dialog.findViewById(R.id.et_activedrawers);
        et_cashdispered=dialog.findViewById(R.id.et_cashdispered);
        tv_cancel=dialog.findViewById(R.id.tv_cancel);
        tv_save=dialog.findViewById(R.id.tv_save);
        layout_storeopen=dialog.findViewById(R.id.layout_storeopen);
        layout_tillopen=dialog.findViewById(R.id.layout_tillopen);
        layout_storeopen.setVisibility(View.VISIBLE);
        layout_tillopen.setVisibility(View.GONE);

        et_prevdayamount.setText(result.getPrevious_day_cash_balance());
        et_creditcash.setText(result.getCredit_bal());
        et_noofdrawers.setText(result.getAll_tills());
        et_activedrawers.setText(result.getActive_tills());
        et_cashdispered.setText(result.getTill_disperssed());

        et_creditcash.setImeOptions(EditorInfo.IME_FLAG_NO_EXTRACT_UI | EditorInfo.IME_ACTION_DONE);
        et_cashdispered.setImeOptions(EditorInfo.IME_FLAG_NO_EXTRACT_UI | EditorInfo.IME_ACTION_DONE);

        if (storeopen==0){
            tv_save.setTextColor(context.getResources().getColor(R.color.white));
        }else {
            tv_save.setTextColor(context.getResources().getColor(R.color.graylight));
        }
        if (result.getOpen_check().equalsIgnoreCase("1")){
            et_creditcash.setFocusable(false);
            et_creditcash.setFocusableInTouchMode(false);
            et_creditcash.setClickable(false);
            et_cashdispered.setFocusable(false);
            et_cashdispered.setFocusableInTouchMode(false);
            et_cashdispered.setClickable(false);

        }

        tv_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        tv_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (storeopen==0){
                    String creitcash=et_creditcash.getText().toString().trim();
                    String cashdispered=et_cashdispered.getText().toString().trim();

                    cashDrawerFragment.getStoreopen(1,creitcash,cashdispered);
                    dialog.dismiss();
                }

            }
        });


        dialog.show();
        dialog.setCancelable(true);
    }

    public void showDialog(final Context context, GetTillOpen result, final CashDrawerFragment cashDrawerFragment, final GetsalesTerminalsdata getsalesTerminalsdata) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.store_open_dailog);
        layout_storeopen=dialog.findViewById(R.id.layout_storeopen);
        layout_tillopen=dialog.findViewById(R.id.layout_tillopen);
        tv_cancel=dialog.findViewById(R.id.tv_cancel);
        tv_save=dialog.findViewById(R.id.tv_save);
        TextView tv_date=dialog.findViewById(R.id.tv_date);
        TextView tv_beginingbal=dialog.findViewById(R.id.tv_beginingbal);
        final EditText et_cashdropalert=dialog.findViewById(R.id.et_cashdropalert);

        layout_tillopen.setVisibility(View.VISIBLE);
        layout_storeopen.setVisibility(View.GONE);

        tv_date.setText(result.getDate());
        tv_beginingbal.setText(result.getBalance());
        et_cashdropalert.setImeOptions(EditorInfo.IME_FLAG_NO_EXTRACT_UI | EditorInfo.IME_ACTION_DONE);

        tv_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        tv_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String st_dropalert=et_cashdropalert.getText().toString().trim();
                if (st_dropalert.isEmpty()){
                    Toast.makeText(context, "Enter Cash Drop Alert", Toast.LENGTH_SHORT).show();
                }else {
                    cashDrawerFragment.setTillOpen(getsalesTerminalsdata,0,st_dropalert);
                }
                dialog.dismiss();
            }
        });

        dialog.show();
        dialog.setCancelable(true);

    }
}
