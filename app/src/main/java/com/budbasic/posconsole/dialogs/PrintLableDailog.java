package com.budbasic.posconsole.dialogs;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.budbasic.customer.global.Preferences;
import com.budbasic.global.CUC;
import com.budbasic.posconsole.R;
import com.budbasic.posconsole.adapter.ExpenseCashDrawerAdapter;
import com.budbasic.posconsole.reception.CashDrawerFragment;
import com.budbasic.posconsole.webservice.RequetsInterface;
import com.google.gson.Gson;
import com.kotlindemo.model.GetDenominations;
import com.kotlindemo.model.GetDenominationsdata;
import com.kotlindemo.model.GetEmpExpenses;
import com.kotlindemo.model.GetEmployeeAppSetting;
import com.kotlindemo.model.GetEmployeeExpanses;
import com.kotlindemo.model.GetEmployeeLogin;
import com.kotlindemo.model.GetTillClose;
import com.kotlindemo.model.GetsalesTerminalsdata;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

public class PrintLableDailog {

    private TextView tv_expenses,tv_denomination,tv_closedrawer,tv_submit,tv_save,tv_cancel;
    private LinearLayout layout_expense,layout_denomination,layout_cashdrop,layoutadd_one,layout_add_two;
    private RecyclerView recycle_expense;
    private List<GetEmployeeExpanses> getEmployeeExpansesList;
    private List<GetDenominationsdata> getDenominationsdata;
    public Dialog mDialog;
    private GetEmployeeLogin logindata;
    private int s_day,s_month,s_year;
    private ExpenseCashDrawerAdapter expensesAdapter;
    GetsalesTerminalsdata getsalesTerminalsdata;
    CashDrawerFragment cashDrawerFragment;
    private EditText tv_pennies,tv_nickels,tv_dimes,tv_quartes,tv_ones,tv_fives,tv_tens,tv_twenties,
            tv_fifties,tv_hundreds,tv_date,tv_begingbal,tv_cashsales,tv_debitsales,tv_expensess,tv_expectedcount,tv_total_sales,
    tv_cash_draweramt,tv_actual_draweramt,tv_varience;
    private Calendar calendar;
    int day,month,year;
    GetEmployeeAppSetting mBAsis;

    public void showDialog(final Context context, GetsalesTerminalsdata getsalesTerminalsdata, GetEmployeeLogin logindata,
                           CashDrawerFragment cashDrawerFragment) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.close_cash_drawer);
        tv_expenses=dialog.findViewById(R.id.tv_expenses);
        tv_denomination=dialog.findViewById(R.id.tv_denomination);


        dialog.show();
        dialog.setCancelable(true);

    }



}
