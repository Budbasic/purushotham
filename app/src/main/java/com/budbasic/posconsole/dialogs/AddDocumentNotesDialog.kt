package com.budbasic.posconsole.dialogs

import android.Manifest
import android.annotation.SuppressLint
import android.annotation.TargetApi
import android.app.Activity
import android.app.Dialog
import android.app.DialogFragment
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.os.StrictMode
import android.provider.MediaStore
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v4.content.FileProvider
import android.support.v7.widget.CardView
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.*
import androidx.annotation.RequiresApi
import com.budbasic.customer.global.Preferences
import com.budbasic.global.CUC
import com.budbasic.posconsole.R
import com.budbasic.posconsole.adapter.DocumentAdapter
import com.budbasic.posconsole.webservice.RequetsInterface
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.SimpleTarget
import com.kotlindemo.model.GetEmployeeAppSetting
import com.kotlindemo.model.PatientDocumentModel
import com.kotlindemo.model.Patient_data
import com.squareup.picasso.Picasso
import com.tbruyelle.rxpermissions2.RxPermissions
import io.card.payment.CardIOActivity
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.customer_item1.view.*
import kotlinx.android.synthetic.main.patient_manager_viewedit_dialog.view.*
import kotlinx.android.synthetic.main.patient_manager_viewedit_dialog_new.view.*
import kotlinx.android.synthetic.main.patient_manager_viewedit_dialog_new.view.ll_document
import kotlinx.android.synthetic.main.patient_manager_viewedit_dialog_new.view.ll_phone
import kotlinx.android.synthetic.main.patient_manager_viewedit_dialog_new.view.rl_Notes
import kotlinx.android.synthetic.main.patient_manager_viewedit_dialog_new.view.tvDocuments
import kotlinx.android.synthetic.main.patient_manager_viewedit_dialog_new.view.tvNotes
import kotlinx.android.synthetic.main.patient_manager_viewedit_dialog_new.view.tvPhone
import kotlinx.android.synthetic.main.patient_manager_viewedit_dialog_new.view.tv_phoneNumber
import kotlinx.android.synthetic.main.patient_manager_viewedit_dialog_new.view.view
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.File
import java.io.FileOutputStream
import java.io.OutputStream
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

@SuppressLint("ValidFragment")
class AddDocumentNotesDialog(val mcontext: Context, val appSettingData: GetEmployeeAppSetting?, val employee_id: String, val data: Patient_data,
                             val imagePath:String?, val isnoteupdate: IsNoteUpdata) : DialogFragment() {

    lateinit var recycle_documents: RecyclerView
    lateinit var rl_documents: RelativeLayout
    lateinit var img_doucument_data: ImageView
    lateinit var img_infoImage: ImageView
    lateinit var cv_add_document: CardView
    lateinit var cv_savephoto: CardView
    lateinit var cv_upload_image: CardView
    lateinit var alertcheck: CardView
    lateinit var alertscreen: CardView
    lateinit var tv_alertcheck: TextView
    lateinit var tv_alertscreen: TextView
    internal var bitmap: Bitmap? = null
    var imageCompanyFile: File? = null
    var uri: Uri? = null
    var isImageSelected = false
    var rxPermissions: RxPermissions? = null
    lateinit var edt_note_message: EditText
    lateinit var cv_submit: CardView

    internal var check: Int? = 0
    internal var int_alertcheck: Int? = 0

    internal var documentimglist =  ArrayList<PatientDocumentModel>()

    private val MY_SCAN_REQUEST_CODE = 3
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(DialogFragment.STYLE_NO_TITLE, R.style.TransparentProgressDialog)

    }


    @TargetApi(Build.VERSION_CODES.M)
    @RequiresApi(Build.VERSION_CODES.M)
    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View {
        val v = inflater!!.inflate(R.layout.patient_manager_viewedit_dialog_new, container, false)
        //    val scanbot=Scan

        initView(v)
        AllViewOnClick(v)
        return v
    }

    private fun AllViewOnClick(view: View) {
        view.tvPhone.setOnClickListener {
            view.rl_Notes.visibility = View.GONE
            view.ll_document.visibility = View.GONE
            view.ll_phone.visibility = View.VISIBLE
            view.ll_patient.visibility = View.GONE

            view.tvNotes.setBackgroundColor(mcontext.resources.getColor(R.color.graylight))
            view.tvDocuments.setBackgroundColor(mcontext.resources.getColor(R.color.graylight))
            view.tvPhone.setBackgroundDrawable(mcontext.resources.getDrawable(R.drawable.gradient_color))
            view.tvpatients.setBackgroundColor(mcontext.resources.getColor(R.color.graylight))

//            view.tvNotes.setTextColor(mcontext.resources.getColor(R.color.border_gray))
//            view.tvDocuments.setTextColor(mcontext.resources.getColor(R.color.border_gray))
//            view.tvPhone.setTextColor(mcontext.resources.getColor(R.color.white))
        }
        view.tvDocuments.setOnClickListener {
            view.rl_Notes.visibility = View.GONE
            view.ll_document.visibility = View.VISIBLE
            view.ll_phone.visibility = View.GONE
            view.ll_patient.visibility = View.GONE

            view.tvNotes.setBackgroundColor(mcontext.resources.getColor(R.color.graylight))
            view.tvDocuments.setBackgroundDrawable(mcontext.resources.getDrawable(R.drawable.gradient_color))
            view.tvPhone.setBackgroundColor(mcontext.resources.getColor(R.color.graylight))
            view.tvpatients.setBackgroundColor(mcontext.resources.getColor(R.color.graylight))

//            view.tvNotes.setTextColor(mcontext.resources.getColor(R.color.border_gray))
//            view.tvDocuments.setTextColor(mcontext.resources.getColor(R.color.white))
//            view.tvPhone.setTextColor(mcontext.resources.getColor(R.color.border_gray))

        }

            view.tvNotes.setOnClickListener {
                view.rl_Notes.visibility = View.VISIBLE
                view.ll_document.visibility = View.GONE
                view.ll_phone.visibility = View.GONE
                view.ll_patient.visibility = View.GONE

                view.tvNotes.setBackgroundDrawable(mcontext.resources.getDrawable(R.drawable.gradient_color))
                //  view.tvNotes.setBackgroundColor(mcontext.resources.getColor(R.color.blue))
                view.tvDocuments.setBackgroundColor(mcontext.resources.getColor(R.color.graylight))
                view.tvPhone.setBackgroundColor(mcontext.resources.getColor(R.color.graylight))
                view.tvpatients.setBackgroundColor(mcontext.resources.getColor(R.color.graylight))

//            view.tvNotes.setTextColor(mcontext.resources.getColor(R.color.white))
//            view.tvDocuments.setTextColor(mcontext.resources.getColor(R.color.border_gray))
//            view.tvPhone.setTextColor(mcontext.resources.getColor(R.color.border_gray))

            }


        view.tvpatients.setOnClickListener {
            view.rl_Notes.visibility = View.GONE
            view.ll_document.visibility = View.GONE
            view.ll_phone.visibility = View.GONE
            view.ll_patient.visibility = View.VISIBLE

            view.tvNotes.setBackgroundColor(mcontext.resources.getColor(R.color.graylight))
            view.tvDocuments.setBackgroundColor(mcontext.resources.getColor(R.color.graylight))
            view.tvPhone.setBackgroundColor(mcontext.resources.getColor(R.color.graylight))
            view.tvpatients.setBackgroundDrawable(mcontext.resources.getDrawable(R.drawable.gradient_color))

//            view.tvNotes.setTextColor(mcontext.resources.getColor(R.color.border_gray))
//            view.tvDocuments.setTextColor(mcontext.resources.getColor(R.color.border_gray))
//            view.tvPhone.setTextColor(mcontext.resources.getColor(R.color.white))
        }



    }


    @RequiresApi(Build.VERSION_CODES.M)
    private fun initView(v: View) {
        edt_note_message = v.findViewById(R.id.edt_note_message) as EditText
        cv_submit = v.findViewById(R.id.cv_submit)

        myDir = File(Environment.getExternalStorageDirectory().toString() + "/TerminalVaultAdmin/")
        if (!myDir!!.exists()) {
            myDir!!.mkdirs()
        }
        recycle_documents = v.findViewById(R.id.recycle_documents)
     //   recycle_documents.layoutManager = LinearLayoutManager(mcontext, LinearLayoutManager.VERTICAL, false)
        recycle_documents.layoutManager = GridLayoutManager(mcontext, 2)
        rl_documents = v.findViewById(R.id.rl_documents)
        img_doucument_data = v.findViewById(R.id.img_doucument_data)
        img_infoImage = v.findViewById(R.id.img_infoImage)
        cv_add_document = v.findViewById(R.id.cv_add_document)
        alertscreen = v.findViewById(R.id.alertscreen)
        alertcheck = v.findViewById(R.id.alertcheck)
        tv_alertscreen = v.findViewById(R.id.tv_alertscreen)
        tv_alertcheck = v.findViewById(R.id.tv_alertcheck)
        cv_savephoto = v.findViewById(R.id.cv_savephoto)
        cv_upload_image = v.findViewById(R.id.cv_upload_image)

        rl_documents.visibility = View.VISIBLE

        img_doucument_data.setOnClickListener {
            check =1
            pickImage()

        }
        cv_savephoto.setOnClickListener {
           addInfoImg(v)

        }

        v.img_infoImage.setOnClickListener {
            check=0
            pickImage()

        }
        v.rl_Notes.visibility = View.VISIBLE
        v.ll_document.visibility = View.GONE
        v.ll_phone.visibility = View.GONE
        v.ll_patient.visibility = View.GONE

        v.tvNotes.setBackgroundDrawable(mcontext.resources.getDrawable(R.drawable.gradient_color))
        //  view.tvNotes.setBackgroundColor(mcontext.resources.getColor(R.color.blue))
        v.tvDocuments.setBackgroundColor(mcontext.resources.getColor(R.color.graylight))
        v.tvPhone.setBackgroundColor(mcontext.resources.getColor(R.color.graylight))
        v.tvpatients.setBackgroundColor(mcontext.resources.getColor(R.color.graylight))


        if (data.patient_notes.isNotEmpty() && !data.patient_notes.equals("null", true)) {
            edt_note_message.setText(data.patient_notes)
            edt_note_message.setSelection(edt_note_message.text.toString().length)

        }
      //  Log.d("PhoneNumber", " = " + data.patient_phone_no)
        v.tv_patientName.setText(data.patient_fname+" "+data.patient_lname)
        v.tv_phoneNumber.setText(data.patient_mobile_no)
        v.tv_mmpNumber.setText("MMP: "+data.patient_mmpp_licence_no)
        v.tv_email.setText(""+data.patient_email_id)

        Log.d("PatientImage"," = "+data.patient_image)
        println("aaaaaaaaaaaaaaaaa   "+data.patient_image)
        try {
            Picasso.with(activity)
                    .load(imagePath+data.patient_image)
                    .placeholder(R.drawable.no_image_available)
                    .error(R.drawable.no_image_available)
                    .into(v.img_infoImage)
        }catch (e:Exception){

        }

       
        alertcheck.setOnClickListener {
            int_alertcheck=0;
            alertcheck.setBackgroundColor(context.resources.getColor(R.color.blue))
            alertscreen.setBackgroundColor(context.resources.getColor(R.color.white))
            tv_alertcheck.setTextColor(context.resources.getColor(R.color.white))
            tv_alertscreen.setTextColor(context.resources.getColor(R.color.black))
        }

        alertcheck.setOnClickListener {
            int_alertcheck=1;
            alertcheck.setBackgroundColor(context.resources.getColor(R.color.white))
            alertscreen.setBackgroundColor(context.resources.getColor(R.color.blue))
            tv_alertcheck.setTextColor(context.resources.getColor(R.color.black))
            tv_alertscreen.setTextColor(context.resources.getColor(R.color.white))
        }


        v.findViewById<LinearLayout>(R.id.ll_cancel).setOnClickListener {
            this@AddDocumentNotesDialog.dismiss()
        }
        apiCallForEmployeeDocument()
        cv_upload_image.setOnClickListener {
            addDocument(v)

        }
        cv_add_document.setOnClickListener {
            addDocument(v)

        }
        cv_submit.setOnClickListener {
            apiCallForEditNote()
        }
    }

    interface IsNoteUpdata {
        fun onResponseChange(updatedNote: String)
    }

    private fun apiCallForEditNote() {
        var mDialog: Dialog = Dialog(mcontext)

        try {
            if (!activity!!.isFinishing) {
                mDialog = CUC.createDialog(activity!!)
                mDialog.show()
                val apiService = RequetsInterface.Factory.create()
                val parameterMap = HashMap<String, String>()


                parameterMap["api_key"] = "${Preferences.getPreference(mcontext!!, "API_KEY")}"
                parameterMap["employee_id"] = employee_id
                parameterMap["patient_id"] = data!!.patient_id
                parameterMap["patient_notes"] = edt_note_message.text.toString()


                CompositeDisposable().add(apiService.callEmployeePatientNote(parameterMap)
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.io())
                        .subscribe({ result ->
                            if (mDialog != null && mDialog.isShowing)
                                mDialog.dismiss()
                            if (result != null && result!!.status.equals("0")) {
                                isnoteupdate.onResponseChange(edt_note_message.text.toString())
                                this@AddDocumentNotesDialog.dismiss()
                            }

                        }, { error ->
                            if (mDialog != null && mDialog.isShowing)
                                mDialog.dismiss()
                            //onChange.isSelected = (!isBanned)
                            CUC.displayToast(mcontext!!, "0", getString(R.string.connection_to_database_failed))
                            error.printStackTrace()
                        })
                )
            }
        } catch (e: Exception) {
            if (mDialog != null && mDialog.isShowing) {
                mDialog.dismiss()
            }
            e.printStackTrace()
        }

    }

    @SuppressLint("CheckResult")
    fun getPermissions() {

        rxPermissions = RxPermissions(activity)
        rxPermissions!!.request(android.Manifest.permission.READ_EXTERNAL_STORAGE, android.Manifest.permission.WRITE_EXTERNAL_STORAGE).subscribe { grant ->
            if (grant) {
                pickImage()
            } else {
                CUC.showToast(activity, "Permission Denied", 100)
            }
        }
    }

    fun pickImage() {

        if (ActivityCompat.checkSelfPermission(activity, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            getPermissions()
        } else {

            /* val pickPhoto = Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
             startActivityForResult(pickPhoto, 1)*/


            var dialog = Dialog(mcontext)
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
            val policy = StrictMode.ThreadPolicy.Builder().permitAll().build()
            StrictMode.setThreadPolicy(policy)
            dialog.setContentView(R.layout.pic_image_dialog)
            dialog.show()
            dialog.findViewById<TextView>(R.id.tv_cancelDialog).setOnClickListener {
                dialog.dismiss()
            }
            dialog.findViewById<TextView>(R.id.tv_openGallery).setOnClickListener {
                val pickPhoto = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
                startActivityForResult(pickPhoto, 1)
                dialog.dismiss()
            }
            dialog.findViewById<TextView>(R.id.tv_OpenCamera).setOnClickListener {
                openCamera()
                //    onScanPress()
                dialog.dismiss()
            }

        }
    }

    fun onScanPress() {

        val scanIntent = Intent(activity!!, CardIOActivity::class.java)
        scanIntent.putExtra(CardIOActivity.EXTRA_SUPPRESS_SCAN, true) // supmit cuando termine de reconocer el documento
        scanIntent.putExtra(CardIOActivity.EXTRA_SUPPRESS_MANUAL_ENTRY, true) // esconder teclado
        scanIntent.putExtra(CardIOActivity.EXTRA_USE_CARDIO_LOGO, true) // cambiar logo de paypal por el de card.io
        scanIntent.putExtra(CardIOActivity.EXTRA_RETURN_CARD_IMAGE, true) // capture img
        scanIntent.putExtra(CardIOActivity.EXTRA_CAPTURED_CARD_IMAGE, true) // capturar img

        // laszar activity
        startActivityForResult(scanIntent, MY_SCAN_REQUEST_CODE)
    }


    fun addDocument(v: View) {
        if (v.findViewById<EditText>(R.id.edt_document_name).text.toString().isEmpty()) {
            v.findViewById<EditText>(R.id.edt_document_name).error = mcontext.getString(R.string.error_document_name)
            //   CUC.displayToast(mcontext!!, mcontext.getString(R.string.error_document_name), getString(R.string.connection_to_database_failed))
            return
        }

        if (!isImageSelected) {
            //    CUC.displayToast(mcontext!!, "Please Select image to upload document", getString(R.string.connection_to_database_failed))
            return
        }


        var mDialog: Dialog? = null
        try {
            if (!activity!!.isFinishing) {
                mDialog = CUC.createDialog(activity!!)
                mDialog.show()
                val apiService = RequetsInterface.Factory.create()
                val parameterMap = HashMap<String, RequestBody>()

                parameterMap.put("api_key", RequestBody.create(MediaType.parse("text/plain"), Preferences.getPreference(mcontext!!, "API_KEY")))
                parameterMap.put("employee_id", RequestBody.create(MediaType.parse("text/plain"), employee_id))
                parameterMap.put("patient_id", RequestBody.create(MediaType.parse("text/plain"), data!!.patient_id))
                parameterMap.put("document_title", RequestBody.create(MediaType.parse("text/plain"), v.findViewById<EditText>(R.id.edt_document_name).text.toString()))

                /* parameterMap["api_key"] = ""
                 parameterMap["employee_id"] = employee_id
                 parameterMap["patient_id"] = data!!.patient_id
                 parameterMap["document_title"] = v.findViewById<EditText>(R.id.edt_document_name).text.toString()*/


                //val file = File(uri!!.path)
                val requestFile = RequestBody.create(MediaType.parse("image/jpeg"), imageCompanyFile)
                val body = MultipartBody.Part.createFormData("document_image", imageCompanyFile!!.name, requestFile)


                CompositeDisposable().add(apiService.addEmployeePatientDocument(parameterMap, body, RequestBody.create(MediaType.parse("text/plain"), imageCompanyFile!!.name))
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.io())
                        .subscribe({ result ->
                            if (mDialog != null && mDialog.isShowing)
                                mDialog.dismiss()

                            img_doucument_data.setImageDrawable(null)
                            isImageSelected = false
                            v.findViewById<EditText>(R.id.edt_document_name).setText("")
                            val game: PatientDocumentModel = PatientDocumentModel("", "", "Driver Licence Front","","",
                                    "","","","")
                            val game1: PatientDocumentModel = PatientDocumentModel("", "", "Driver Licence Back","","",
                                    "","","","")
                            val game2: PatientDocumentModel = PatientDocumentModel("", "", "MMMP Front","","",
                                    "","","","")
                            val game3: PatientDocumentModel = PatientDocumentModel("", "", "MMMP Back","","",
                                    "","","","")
                            documentimglist.add(0,game)
                            documentimglist.add(1,game1)
                            documentimglist.add(2,game2)
                            documentimglist.add(3,game3)


                           // if (result?.documents?.size!! > 0) {
                                recycle_documents.adapter = DocumentAdapter(activity!!, appSettingData!!, data!!.patient_id, employee_id, mcontext,documentimglist)
                                rl_documents.visibility = View.VISIBLE
                           // } else {
                               // rl_documents.visibility = View.GONE
                           // }

                        }, { error ->
                            if (mDialog != null && mDialog.isShowing)
                                mDialog.dismiss()
                            //  CUC.displayToast(mcontext!!, "0", getString(R.string.connection_to_database_failed))
                            error.printStackTrace()
                        })
                )
            }
        } catch (e: Exception) {
            if (mDialog != null && mDialog.isShowing) {
                mDialog.dismiss()
            }
            e.printStackTrace()
        }
    }

    fun addInfoImg(v: View) {

        if (!isImageSelected) {
            //    CUC.displayToast(mcontext!!, "Please Select image to upload document", getString(R.string.connection_to_database_failed))
            return
        }


        var mDialog: Dialog? = null
        try {
            if (!activity!!.isFinishing) {
                mDialog = CUC.createDialog(activity!!)
                mDialog.show()
                val apiService = RequetsInterface.Factory.create()
                val parameterMap = HashMap<String, RequestBody>()

                parameterMap.put("api_key", RequestBody.create(MediaType.parse("text/plain"), Preferences.getPreference(mcontext!!, "API_KEY")))
                parameterMap.put("employee_id", RequestBody.create(MediaType.parse("text/plain"), employee_id))
                parameterMap.put("patient_id", RequestBody.create(MediaType.parse("text/plain"), data!!.patient_id))
               // parameterMap.put("document_title", RequestBody.create(MediaType.parse("text/plain"), v.findViewById<EditText>(R.id.edt_document_name).text.toString()))


                //val file = File(uri!!.path)
                val requestFile = RequestBody.create(MediaType.parse("image/jpeg"), imageCompanyFile)
                val body = MultipartBody.Part.createFormData("document_image", imageCompanyFile!!.name, requestFile)

                println("aaaaaaaaaaaa   addinfo img parameters  "+parameterMap)
                CompositeDisposable().add(apiService.addEmployeeinfoImg(parameterMap, body, RequestBody.create(MediaType.parse("text/plain"), imageCompanyFile!!.name))
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.io())
                        .subscribe({ result ->
                            if (mDialog != null && mDialog.isShowing)
                                mDialog.dismiss()
                            println("aaaaaaaaaa  result  "+result.toString())
                            if (result.status.equals("0")) {
                                CUC.displayToast(mcontext!!, "0", result.message)
                            }

                        }, { error ->
                            if (mDialog != null && mDialog.isShowing)
                                mDialog.dismiss()
                            //  CUC.displayToast(mcontext!!, "0", getString(R.string.connection_to_database_failed))
                            error.printStackTrace()
                        })
                )
            }
        } catch (e: Exception) {
            if (mDialog != null && mDialog.isShowing) {
                mDialog.dismiss()
            }
            e.printStackTrace()
        }
    }

    private fun apiCallForEmployeeDocument() {
        var mDialog: Dialog? = null
        try {
            if (!activity!!.isFinishing) {
                mDialog = CUC.createDialog(activity!!)
                mDialog.show()
                val apiService = RequetsInterface.Factory.create()
                val parameterMap = HashMap<String, String>()

                parameterMap["api_key"] = "${Preferences.getPreference(mcontext!!, "API_KEY")}"
                parameterMap["employee_id"] = employee_id
                parameterMap["patient_id"] = data!!.patient_id

                CompositeDisposable().add(apiService.callEmployeePatientDocument(parameterMap)
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.io())
                        .subscribe({ result ->
                            if (mDialog != null && mDialog.isShowing)
                                mDialog.dismiss()
                            if (result?.documents?.size!! > 0) {

                                recycle_documents.adapter = DocumentAdapter(activity!!, appSettingData!!, data!!.patient_id, employee_id, mcontext, result.documents)
                                rl_documents.visibility = View.VISIBLE
                            } else {
                                rl_documents.visibility = View.GONE
                            }

                        }, { error ->
                            if (mDialog != null && mDialog.isShowing)
                                mDialog.dismiss()
                            CUC.displayToast(mcontext!!, "0", getString(R.string.connection_to_database_failed))
                            error.printStackTrace()
                        })
                )
            }
        } catch (e: Exception) {
            if (mDialog != null && mDialog.isShowing) {
                mDialog.dismiss()
            }
            e.printStackTrace()
        }

    }

    /* override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
         super.onActivityResult(requestCode, resultCode, data)
     }*/

    override fun onActivityResult(requestCode: Int, resultCode: Int, imageReturnedIntent: Intent?) {
        super.onActivityResult(requestCode, resultCode, imageReturnedIntent)
        if (requestCode == MY_SCAN_REQUEST_CODE || requestCode == CardIOActivity.RESULT_SCAN_SUPPRESSED) {
            // RESULT_SCAN_SUPPRESSED
            // se lanza cuando se reconoce la img pero no se procesan datos.

            // tomar img
            // la img no queda almacenada en el dispositivo
            try {
                bitmap = CardIOActivity.getCapturedCardImage(imageReturnedIntent)
                if (check==0){
                    img_infoImage.setImageBitmap(bitmap)
                }else{
                    img_doucument_data.setImageBitmap(bitmap)
                }


                Glide.with(this).asBitmap().load(bitmap).apply(RequestOptions()).into(object : SimpleTarget<Bitmap>() {
                    override fun onResourceReady(resource: Bitmap, transition: com.bumptech.glide.request.transition.Transition<in Bitmap>?) {
                        if (resource != null) {
                            val timeStamp = SimpleDateFormat("yyyy.MM.dd.HH.mm.ss", Locale.US).format(Date())
                            imageCompanyFile = persistImage(resource, timeStamp)
                            isImageSelected = true
                        }
                    }
                })
            } catch (e: Exception) {

            }


        }
        when (requestCode) {

            1 -> if (resultCode == Activity.RESULT_OK && imageReturnedIntent!!.data != null) {
                var selectedImageUri: Uri? = null
                var filePath: String? = null
                selectedImageUri = imageReturnedIntent.data
                val card = CardIOActivity.getCapturedCardImage(imageReturnedIntent)
                //   Log.d("ASSASA"," = "+card)
                val selectedImage = imageReturnedIntent.data
                uri = imageReturnedIntent.data
                if (check==0){
                    img_infoImage.setImageURI(selectedImage)
                }else{
                    img_doucument_data.setImageURI(selectedImage)
                }


                decodeFile(uri!!.path)
                if (selectedImageUri != null) {
                    try {
                        // OI FILE Manager
                        val filemanagerstring = selectedImageUri.path

                        // MEDIA GALLERY
                        val selectedImagePath = getPath(selectedImageUri)

                        if (selectedImagePath != null) {
                            filePath = selectedImagePath
                        } else if (filemanagerstring != null) {
                            filePath = filemanagerstring
                        } else {

                        }

                        if (filePath != null) {
                            decodeFile(filePath)
                        } else {
                            bitmap = null

                        }
                    } catch (e: Exception) {
                        Log.e(e.javaClass.name, e.message, e)
                    }

                }


            }
            2, CAMERA_CAPTURE_IMAGE_REQUEST_CODE -> if (resultCode == Activity.RESULT_OK) {
                isImageSelected = true
                val selectedImage = imageReturnedIntent?.data
                uri = imageReturnedIntent?.data
                if (check==0){
                    img_infoImage.setImageURI(selectedImage)
                }else{
                    img_doucument_data.setImageURI(selectedImage)
                }


                var f = File(myDir.toString())
                for (temp in f.listFiles()!!) {
                    if (temp.name == "$timeStamp.jpg") {
                        f = temp
                        break
                    }
                }
                try {
                    decodeFile(f.absolutePath)
                    //val file = File(myDir, f.name)

                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

        }
    }


    fun getPath(uri: Uri): String? {
        val projection = arrayOf(MediaStore.Images.Media.DATA)
        val cursor = activity!!.managedQuery(uri, projection, null, null, null)
        if (cursor != null) {
            // HERE YOU WILL GET A NULLPOINTER IF CURSOR IS NULL
            // THIS CAN BE, IF YOU USED OI FILE MANAGER FOR PICKING THE MEDIA
            val column_index = cursor!!
                    .getColumnIndexOrThrow(MediaStore.Images.Media.DATA)
            cursor!!.moveToFirst()
            return cursor!!.getString(column_index)
        } else
            return null
    }

    fun decodeFile(filePath: String?) {
        // Decode image size
        val o = BitmapFactory.Options()
        o.inJustDecodeBounds = true
        BitmapFactory.decodeFile(filePath, o)

        // The new size we want to scale to
        val REQUIRED_SIZE = 512

        // Find the correct scale value. It should be the power of 2.
        var width_tmp = o.outWidth
        var height_tmp = o.outHeight
        var scale = 1
        while (true) {
            if (width_tmp < REQUIRED_SIZE && height_tmp < REQUIRED_SIZE)
                break
            width_tmp /= 2
            height_tmp /= 2
            scale *= 2
        }

        // Decode with inSampleSize
        val o2 = BitmapFactory.Options()
        o2.inSampleSize = scale
        bitmap = BitmapFactory.decodeFile(filePath, o2)
        if (check==0){
            img_infoImage.setImageBitmap(bitmap)
        }else{
            img_doucument_data.setImageBitmap(bitmap)
        }


        Glide.with(this).asBitmap().load(bitmap).apply(RequestOptions()).into(object : SimpleTarget<Bitmap>() {
            override fun onResourceReady(resource: Bitmap, transition: com.bumptech.glide.request.transition.Transition<in Bitmap>?) {
                if (resource != null) {
                    val timeStamp = SimpleDateFormat("yyyy.MM.dd.HH.mm.ss", Locale.US).format(Date())
                    imageCompanyFile = persistImage(resource, timeStamp)
                    isImageSelected = true
                }
            }
        })


    }

    /* fun getResizedBitmap(image: Bitmap, maxSize: Int): Bitmap {
         var width = image.width
         var height = image.height

         val bitmapRatio = width.toFloat() / height.toFloat()
         if (bitmapRatio > 1) {
             width = maxSize
             height = (width / bitmapRatio).toInt()
         } else {
             height = maxSize
             width = (height * bitmapRatio).toInt()
         }
         return Bitmap.createScaledBitmap(image, width, height, true)
     }*/

    fun persistImage(bitmap: Bitmap, name: String): File {
        val filesDir: File = mcontext.filesDir
        val imageFile: File = File(filesDir, name + ".jpeg");
        var os: OutputStream
        try {
            os = FileOutputStream(imageFile);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, os);
            os.flush();
            os.close();
        } catch (e: Exception) {
            Log.e(javaClass.getSimpleName(), "Error writing bitmap", e);
        }

        return imageFile
    }

    internal var timeStamp = ""
    private val CAMERA_REQUEST = 1002
    val CAMERA_CAPTURE_IMAGE_REQUEST_CODE = 5263
    private val EXTERNAL_CAMERA = 1003
    var myDir: File? = null
    fun openCamera() {
        val version = Build.VERSION.SDK_INT
        if (version >= 23) {
            if (ContextCompat.checkSelfPermission(activity, android.Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {

                // Should we show an explanation?
                if (ActivityCompat.shouldShowRequestPermissionRationale(activity, android.Manifest.permission.CAMERA)) {

                    //This is called if user has denied the permission before
                    //In this case I am just asking the permission again
                    ActivityCompat.requestPermissions(activity, arrayOf(android.Manifest.permission.CAMERA), CAMERA_REQUEST)

                } else {

                    ActivityCompat.requestPermissions(activity, arrayOf(android.Manifest.permission.CAMERA), CAMERA_REQUEST)
                }
            } else {

                if (ContextCompat.checkSelfPermission(activity, android.Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {

                    // Should we show an explanation?
                    if (ActivityCompat.shouldShowRequestPermissionRationale(activity, android.Manifest.permission.WRITE_EXTERNAL_STORAGE)) {

                        //This is called if user has denied the permission before
                        //In this case I am just asking the permission again
                        ActivityCompat.requestPermissions(activity, arrayOf(android.Manifest.permission.WRITE_EXTERNAL_STORAGE), EXTERNAL_CAMERA)

                    } else {

                        ActivityCompat.requestPermissions(activity, arrayOf(android.Manifest.permission.WRITE_EXTERNAL_STORAGE), EXTERNAL_CAMERA)
                    }
                } else {
                    timeStamp = SimpleDateFormat("yyyy.MM.dd.HH.mm.ss", Locale.US).format(Date())
                    val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                    var f = File(myDir, timeStamp + ".jpg")
                    var f1 = FileProvider.getUriForFile(activity, "com.budbasic.posconsole.fileprovider", f)
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, f1)
                    intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                    startActivityForResult(intent, CAMERA_CAPTURE_IMAGE_REQUEST_CODE)
                }
            }

        } else {
            timeStamp = SimpleDateFormat("yyyy.MM.dd.HH.mm.ss", Locale.US).format(Date())
            val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            val f = File(myDir, timeStamp + ".jpg")
            intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f))
            startActivityForResult(intent, CAMERA_CAPTURE_IMAGE_REQUEST_CODE)
        }

    }

}