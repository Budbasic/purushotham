package com.budbasic.posconsole.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import com.budbasic.customer.global.Preferences;
import com.budbasic.global.CUC;
import com.budbasic.posconsole.R;
import com.budbasic.posconsole.adapter.PromocodeAdapter;
import com.budbasic.posconsole.reception.ProductsFragment;
import com.budbasic.posconsole.reception.SalesActivity;
import com.budbasic.posconsole.sales.UserInfoDetails;
import com.budbasic.posconsole.webservice.RequetsInterface;
import com.kotlindemo.model.Favourite;
import com.kotlindemo.model.GetCartData;
import com.kotlindemo.model.GetEmployeePatientCart;
import com.kotlindemo.model.GetEmployeeQenerateInvoice;
import com.kotlindemo.model.GetPromosales;
import com.kotlindemo.model.GetSalesProduct;
import com.kotlindemo.model.GetStatus;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import kotlin.jvm.internal.Intrinsics;

public class AddPromocardDialog {

    private RecyclerView rv_promo_code_list;
    private RelativeLayout rl_applyCode;
    private ImageView iv_add_category;
    private PromocodeAdapter promocodeAdapter;
    private SearchView search_product;

    public void showDialog(final Context context, ArrayList<GetPromosales> peomocedelist) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.promocode_layout);
        iv_add_category=dialog.findViewById(R.id.iv_add_category);
        rl_applyCode=dialog.findViewById(R.id.rl_applyCode);
        rv_promo_code_list=dialog.findViewById(R.id.rv_promo_code_list);
        search_product=dialog.findViewById(R.id.search_product);

        rv_promo_code_list.setLayoutManager(new GridLayoutManager(context,1));

        promocodeAdapter=new PromocodeAdapter(context,peomocedelist);
        rv_promo_code_list.setAdapter(promocodeAdapter);
        rl_applyCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((SalesActivity)context).setPromocde();
            }
        });
        iv_add_category.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.cancel();
            }
        });
        search_product.setImeOptions(EditorInfo.IME_FLAG_NO_EXTRACT_UI);
        search_product.setFocusable(false);
        search_product.setIconified(false);
        search_product.onActionViewExpanded();
        search_product.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                promocodeAdapter.getFilter().filter(s.toString());
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                return false;
            }
        });
        dialog.show();
        dialog.setCancelable(true);
    }
}
