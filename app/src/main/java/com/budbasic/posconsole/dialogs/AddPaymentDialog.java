package com.budbasic.posconsole.dialogs;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.DialogFragment;
import android.support.v4.view.ViewPager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.budbasic.posconsole.R;
import com.budbasic.posconsole.reception.SalesActivity;
import com.budbasic.posconsole.sales.PayViewPagerAdapter;
import com.kotlindemo.model.GetEmployeePatientCart;
import com.budbasic.global.CUC;
import java.util.ArrayList;

@SuppressLint("ValidFragment")
public class AddPaymentDialog extends DialogFragment {

    private TabLayout tab_layout;
    private ViewPager mViewPager;
    private Context context;
    GetEmployeePatientCart viewcard;
    private TextView tv_total_amount,t9_key_0,t9_key_1,t9_key_2,t9_key_3,t9_key_4,t9_key_5,t9_key_6,t9_key_7,t9_key_8,t9_key_9,
            t9_key_dote,t9_key_backspace,tv_cancel,cv_generate_invoice;
    private EditText text_amount;
    SparseArray<String> keyValues;
    String value="";

    @SuppressLint("ValidFragment")
    public AddPaymentDialog(Context salesActivity, GetEmployeePatientCart payment_types) {
        this.context=salesActivity;
        this.viewcard=payment_types;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable final ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v=inflater.inflate(R.layout.paymentdialog,container,false);
        tab_layout=v.findViewById(R.id.tab_layout);
        mViewPager=v.findViewById(R.id.mViewPager);
        tv_total_amount=v.findViewById(R.id.tv_total_amount);
        text_amount=v.findViewById(R.id.text_amount);
        t9_key_0=v.findViewById(R.id.t9_key_0);
        t9_key_1=v.findViewById(R.id.t9_key_1);
        t9_key_2=v.findViewById(R.id.t9_key_2);
        t9_key_3=v.findViewById(R.id.t9_key_3);
        t9_key_4=v.findViewById(R.id.t9_key_4);
        t9_key_5=v.findViewById(R.id.t9_key_5);
        t9_key_6=v.findViewById(R.id.t9_key_6);
        t9_key_7=v.findViewById(R.id.t9_key_7);
        t9_key_8=v.findViewById(R.id.t9_key_8);
        t9_key_9=v.findViewById(R.id.t9_key_9);
        t9_key_dote=v.findViewById(R.id.t9_key_dote);
        t9_key_backspace=v.findViewById(R.id.t9_key_backspace);
        tv_cancel=v.findViewById(R.id.tv_cancel);
        cv_generate_invoice=v.findViewById(R.id.cv_generate_invoice);

        PayViewPagerAdapter adapter = new PayViewPagerAdapter(getChildFragmentManager());
        for (int i=0;i<viewcard.getPayment_types().size();i++){
            tab_layout.addTab(tab_layout.newTab().setText(viewcard.getPayment_types().get(i).getPayment_detail()));
            //adapter.addFragment(viewcard.getPayment_types().get(i).getPayment_detail(), PaymentFragment.newInstance(i,viewcard.getPayment_types().get(i)));
        }

        //tab_layout.setTabsFromPagerAdapter(adapter);
      //  mViewPager.setAdapter(adapter);
        //Make tabs scrollable
        tab_layout.setTabMode(TabLayout.MODE_SCROLLABLE);
        //tab_layout.setupWithViewPager(mViewPager);

        tv_total_amount.setText("$ "+viewcard.getTotal_payble());

        t9_key_0.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                check("0");
            }
        });
        t9_key_1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                check("1");
            }
        });
        t9_key_2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                check("2");
            }
        });
        t9_key_3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                check("3");
            }
        });
        t9_key_4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                check("4");
            }
        });
        t9_key_5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                check("5");
            }
        });
        t9_key_6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                check("6");
            }
        });
        t9_key_7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                check("7");
            }
        });
        t9_key_8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                check("8");
            }
        });
        t9_key_9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                check("9");
            }
        });
        t9_key_dote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                check(".");
            }
        });
        tv_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });t9_key_backspace.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                value="";
                text_amount.setText("");
                text_amount.setHint("0.00");
            }
        });
        cv_generate_invoice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (text_amount.getText().toString().isEmpty()){
                    CUC.Companion.displayToast(context, "0", "Please Enter Paid Amount");
                }else {
                  Float enteramt=Float.parseFloat(text_amount.getText().toString());
                  Float payamt=Float.parseFloat(viewcard.getTotal_payble());
                    System.out.println("aaaaaaaaaaaaa   float amt "+enteramt+"   "+payamt);
                  if (enteramt>=payamt){
                     // if (enteramt.equals(payamt)){
                          ((SalesActivity)context).sendPayment(enteramt,tab_layout.getSelectedTabPosition());
                          dismiss();
                     /* }else {
                          CUC.Companion.displayToast(context, "0", "Please pay complete amount");
                      }*/
                  }else {
                      CUC.Companion.displayToast(context, "0", "Please pay full amount");
                  }
                }
            }
        });


        return v;
    }
    public void check(String value){
        if (this.value.equalsIgnoreCase("")){
            this.value=value;
        }else {
            String total=this.value+value;
            this.value=total;
        }
        text_amount.setText(this.value);
        System.out.println("aaaaaaaaaaa  value  "+this.value);
    }

    /*@Override
    public void onClick(View view) {
        if (view.getId() == R.id.t9_key_backspace) {
            text_amount.setText("0.00");
            *//*if(text_amount.getText().toString().length() > 0){
                String input = text_amount.getText().toString();
                input = input.substring(0, input.length()-1);
                text_amount.setText(input);
            }*//*
        } else if (view.getId()==R.id.tv_cancel){
            text_amount.setText("0.00");
        }
        else {
            String value1=value+keyValues.get(view.getId());
            text_amount.setText(value1);
        }


    }*/

}
