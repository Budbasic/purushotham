package com.budbasic.posconsole.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.budbasic.global.CUC;
import com.budbasic.posconsole.R;
import com.budbasic.posconsole.global.Apppreference;
import com.budbasic.posconsole.reception.ProductsFragment;
import com.budbasic.posconsole.reception.SalesActivity;
import com.kotlindemo.model.GetCartData;
import com.kotlindemo.model.GetSalesProduct;
import com.kotlindemo.model.GetStatus;

public class ProductquantityupdateDialog extends Dialog {

    private TextView tv_product_name,tv_stock,tv_tottal_qty,tv_tottal_price,tv_short_name,tv_add_cart;
    private EditText tv_first_text,disc_per,disc_amount;
    private LinearLayout ll_add_cart;
    GetCartData getProducts;
    private Context context;
    GetStatus result;
    String weigth;
    private Apppreference apppreference;
    public ProductquantityupdateDialog(Context context, GetStatus result, GetCartData getProducts, String s) {
        super(context);
        this.context=context;
        this.getProducts=getProducts;
        this.result=result;
        this.weigth=s;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.product_qty_dialog_new);

        tv_product_name=findViewById(R.id.tv_product_name);
        tv_stock=findViewById(R.id.tv_stock);
        tv_tottal_qty=findViewById(R.id.tv_tottal_qty);
        tv_tottal_price=findViewById(R.id.tv_tottal_price);
        tv_first_text=findViewById(R.id.tv_first_text);
        disc_amount=findViewById(R.id.disc_amount);
        disc_per=findViewById(R.id.disc_per);
        ll_add_cart=findViewById(R.id.ll_add_cart);
        tv_add_cart=findViewById(R.id.tv_add_cart);
        tv_add_cart.setText("Update To Cart");
        tv_short_name=findViewById(R.id.tv_short_name);
        tv_first_text.setImeOptions(EditorInfo.IME_FLAG_NO_EXTRACT_UI | EditorInfo.IME_ACTION_DONE);
        disc_per.setImeOptions(EditorInfo.IME_FLAG_NO_EXTRACT_UI | EditorInfo.IME_ACTION_DONE);
        disc_amount.setImeOptions(EditorInfo.IME_FLAG_NO_EXTRACT_UI | EditorInfo.IME_ACTION_DONE);
        tv_product_name.setText(""+getProducts.getPacket_name());
        if (result.getInventory_qty().equals("0")){
            tv_stock.setTextColor(context.getResources().getColor(R.color.red));
            tv_stock.setText("Stack Unavailable");
        }else {
            Float qty=Float.parseFloat(result.getInventory_qty());
            tv_stock.setText("Stack "+qty+" "+getProducts.getProduct_packets().getPacket_unit());
        }

        tv_tottal_price.setText(""+getProducts.getProduct_packets().getPacket_base_price());
        tv_short_name.setText(""+getProducts.getProduct_packets().getPacket_unit());
        tv_first_text.setText(""+getProducts.getCart_detail_qty());
        tv_first_text.setSelection(tv_first_text.getText().length());

        System.out.println("aaaaaaaaa   discount  "+getProducts.getCart_detail_per_disc()+"   "+getProducts.getCart_detail_fix_disc());

        if (!getProducts.getCart_detail_per_disc().equalsIgnoreCase("0.00")){
            if (getProducts.getCart_detail_per_disc().equalsIgnoreCase("0")){
                disc_per.setHint("0.00");
                disc_per.setText("");
                if (!getProducts.getCart_detail_fix_disc().equalsIgnoreCase("0.00")){
                    if (getProducts.getCart_detail_fix_disc().equalsIgnoreCase("0")){
                        disc_amount.setHint("0.00");
                        disc_amount.setText("");
                    }else {
                        disc_amount.setText(getProducts.getCart_detail_fix_disc());

                    }
                }

            }else {
                disc_per.setText(getProducts.getCart_detail_per_disc());

            }
        }
        else if (!getProducts.getCart_detail_fix_disc().equalsIgnoreCase("0.00")){
            if (getProducts.getCart_detail_fix_disc().equalsIgnoreCase("0")){

                disc_amount.setHint("0.00");
                disc_amount.setText("");
            }else {
                disc_amount.setText(getProducts.getCart_detail_fix_disc());

            }
        }

        ll_add_cart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String entervalue=tv_first_text.getText().toString().trim();
                System.out.println("aaaaaaaaaa  enter value  "+entervalue);
                if (entervalue.isEmpty()){
                    CUC.Companion.displayToast(context, "Please Enter Quantity", result.getMessage());
                    Toast.makeText(context, "Please Enter Quantity", Toast.LENGTH_SHORT).show();
                }else {
                    if (result.getInventory_qty().equals("0")) {
                        Toast.makeText(context, "Currrently Product Unavailable", Toast.LENGTH_SHORT).show();
                    } else {
                        if (getProducts.getProduct_packets().getPacket_unit().equalsIgnoreCase("Each")) {
                            Double  value = Double.parseDouble(entervalue);
                           // try {

                           // } catch (NumberFormatException e) {
                               // value = Double.parseDouble(getProducts.getCart_detail_qty());
                           // }

                            String percentagedes = disc_per.getText().toString().trim();
                            String amountdes = disc_amount.getText().toString().trim();
                            Float persecent = 0.0f;
                            int i = 0;
                            if (value <= (Float.parseFloat(result.getInventory_qty() + getProducts.getCart_detail_qty()))) {
                                // productsFragment.setClick();
                                if (!percentagedes.isEmpty() && !amountdes.isEmpty()) {
                                    Toast.makeText(context, "Enter only one Discount", Toast.LENGTH_SHORT).show();
                                    CUC.Companion.displayToast(context, "Enter only one", result.getMessage());
                                } else {
                                    if (!percentagedes.isEmpty()) {
                                        persecent = Float.parseFloat(percentagedes);
                                        i = 1;
                                    }
                                    if (!amountdes.isEmpty()) {
                                        persecent = Float.parseFloat(amountdes);
                                        i = 2;
                                    }
                                    ((SalesActivity) context).sendUpdatecartea(getProducts, value, persecent, i);
                                    dismiss();
                                }

                            } else {
                                Toast.makeText(context, "Enter valid Quantity", Toast.LENGTH_SHORT).show();
                                CUC.Companion.displayToast(context, "Store Quantity more than you entered", result.getMessage());
                            }
                        } else {
                            Float value = Float.parseFloat(entervalue);
                            String percentagedes = disc_per.getText().toString().trim();
                            String amountdes = disc_amount.getText().toString().trim();
                            Float persecent = 0.0f;
                            int i = 0;
                            if (value <= Float.parseFloat(result.getInventory_qty() + getProducts.getCart_detail_qty())) {
                                // productsFragment.setClick();
                                if (!percentagedes.isEmpty() && !amountdes.isEmpty()) {
                                    Toast.makeText(context, "Enter only one Discount", Toast.LENGTH_SHORT).show();
                                    CUC.Companion.displayToast(context, "Enter only one", result.getMessage());
                                } else {
                                    if (!percentagedes.isEmpty()) {
                                        persecent = Float.parseFloat(percentagedes);
                                        i = 1;
                                    }
                                    if (!amountdes.isEmpty()) {
                                        persecent = Float.parseFloat(amountdes);
                                        i = 2;
                                    }
                                    ((SalesActivity) context).sendUpdatecart(getProducts, value, persecent, i);
                                    dismiss();
                                }

                            } else {
                                Toast.makeText(context, "Enter valid Quantity", Toast.LENGTH_SHORT).show();
                                CUC.Companion.displayToast(context, "Store Quantity more than you entered", result.getMessage());
                            }
                        }


                    }
                }


            }
        });
    }
}
