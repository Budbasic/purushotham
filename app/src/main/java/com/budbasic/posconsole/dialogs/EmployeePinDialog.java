package com.budbasic.posconsole.dialogs;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.InputFilter;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.budbasic.customer.global.Preferences;
import com.budbasic.global.CUC;
import com.budbasic.posconsole.R;
import com.budbasic.posconsole.adapter.ExpenseCashDrawerAdapter;
import com.budbasic.posconsole.reception.CashDrawerFragment;
import com.budbasic.posconsole.reception.fragment.EmployeeManagerFragment;
import com.budbasic.posconsole.webservice.RequetsInterface;
import com.google.gson.Gson;
import com.kotlindemo.model.GetDenominations;
import com.kotlindemo.model.GetDenominationsdata;
import com.kotlindemo.model.GetDrivers;
import com.kotlindemo.model.GetEmpExpenses;
import com.kotlindemo.model.GetEmployeeAppSetting;
import com.kotlindemo.model.GetEmployeeExpanses;
import com.kotlindemo.model.GetEmployeeLogin;
import com.kotlindemo.model.GetTillClose;
import com.kotlindemo.model.GetsalesTerminalsdata;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

public class EmployeePinDialog {

    private EditText edt_current_pin,edt_new_pin,edt_confirm_pin;
    private TextView textView5,tv_submit,tv_cureent_lbl,tv_new_lbl,tv_confirm,tv_title;

    public void showDialog(final Context context, final GetDrivers getDrivers, final int i, final EmployeeManagerFragment employeeManagerFragment) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.change_pin_dialog);
        edt_current_pin=dialog.findViewById(R.id.edt_current_pin);
        edt_new_pin=dialog.findViewById(R.id.edt_new_pin);
        textView5=dialog.findViewById(R.id.textView5);
        tv_submit=dialog.findViewById(R.id.tv_submit);
        tv_cureent_lbl=dialog.findViewById(R.id.tv_cureent_lbl);
        tv_new_lbl=dialog.findViewById(R.id.tv_new_lbl);
        tv_confirm=dialog.findViewById(R.id.tv_confirm);
        edt_confirm_pin=dialog.findViewById(R.id.edt_confirm_pin);
        tv_title=dialog.findViewById(R.id.tv_title);

        edt_current_pin.setImeOptions(EditorInfo.IME_FLAG_NO_EXTRACT_UI | EditorInfo.IME_ACTION_DONE);
        edt_new_pin.setImeOptions(EditorInfo.IME_FLAG_NO_EXTRACT_UI | EditorInfo.IME_ACTION_DONE);
        edt_confirm_pin.setImeOptions(EditorInfo.IME_FLAG_NO_EXTRACT_UI | EditorInfo.IME_ACTION_DONE);

        if (i==1){
            tv_cureent_lbl.setText("Old PIN");
            edt_current_pin.setHint("Old PIN");
            tv_new_lbl.setText("New PIN");
            edt_new_pin.setHint("New PIN");
            edt_confirm_pin.setHint("Confirm PIN");
            tv_confirm.setText("Confirm PIN");
            tv_title.setText("Change PIN");

            InputFilter[] filterArray = new InputFilter[1];
            filterArray[0] = new InputFilter.LengthFilter(4);


            edt_current_pin.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_VARIATION_PASSWORD);
            edt_current_pin.setFilters(filterArray);

            edt_new_pin.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_VARIATION_PASSWORD);
            edt_new_pin.setFilters(filterArray);

            edt_confirm_pin.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_VARIATION_PASSWORD);
            edt_confirm_pin.setFilters(filterArray);


        }else {
            tv_cureent_lbl.setText("Old Password");
            edt_current_pin.setHint("Old Password");
            tv_new_lbl.setText("New Password");
            edt_new_pin.setHint("New Password");
            tv_confirm.setText("Confirm Password");
            edt_confirm_pin.setHint("Confirm Password");
            tv_title.setText( "Change Password");

            edt_current_pin.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
            edt_new_pin.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
            edt_confirm_pin.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);

        }

        tv_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (i == 1) {
                    if (edt_new_pin.getText().toString().isEmpty()) {
                        CUC.Companion.displayToast(context, "0", "Enter New PIN");
                    } else if (edt_confirm_pin.getText().toString().isEmpty()) {
                        CUC.Companion.displayToast(context, "0", context.getResources().getString(R.string.enter_confirm_pin));
                    } else if (!edt_new_pin.getText().toString().equals(edt_confirm_pin.getText().toString())) {
                        CUC.Companion.displayToast(context, "0", context.getResources().getString(R.string.pin_do_not_match));
                    } else {
                        employeeManagerFragment.setPinChange(getDrivers.getEmp_id(),edt_new_pin.getText().toString());
                    }
                } else {
                    if (edt_new_pin.getText().toString().isEmpty()) {
                        CUC.Companion.displayToast(context, "0", "Enter New Password");
                    } else if (edt_confirm_pin.getText().toString().isEmpty()) {
                        CUC.Companion.displayToast(context, "0",context.getResources().getString(R.string.enter_confirm_password));
                    } else if (edt_new_pin.getText().toString().equals(edt_confirm_pin.getText().toString())) {
                        CUC.Companion.displayToast(context, "0", context.getResources().getString(R.string.password_do_not_match));
                    } else {
                        employeeManagerFragment.setpasswordchange(getDrivers.getEmp_id(),edt_new_pin.getText().toString());

                    }
                }

            }
        });

        textView5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        dialog.show();
        dialog.setCancelable(true);

    }



}
