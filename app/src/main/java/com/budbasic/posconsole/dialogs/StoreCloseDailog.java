package com.budbasic.posconsole.dialogs;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.budbasic.customer.global.Preferences;
import com.budbasic.global.CUC;
import com.budbasic.posconsole.R;
import com.budbasic.posconsole.adapter.ExpenseCashDrawerAdapter;
import com.budbasic.posconsole.reception.CashDrawerFragment;
import com.budbasic.posconsole.reception.SalesActivity;
import com.budbasic.posconsole.webservice.RequetsInterface;
import com.google.gson.Gson;
import com.kotlindemo.model.GetDenominations;
import com.kotlindemo.model.GetDenominationsdata;
import com.kotlindemo.model.GetEmpExpenses;
import com.kotlindemo.model.GetEmployeeAppSetting;
import com.kotlindemo.model.GetEmployeeExpanses;
import com.kotlindemo.model.GetEmployeeLogin;
import com.kotlindemo.model.GetStoreClose;
import com.kotlindemo.model.GetTillClose;
import com.kotlindemo.model.GetsalesTerminalsdata;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

public class StoreCloseDailog {

    private TextView tv_expenses,tv_closedrawer,tv_save,tv_cancel,tv_prevday,tv_cashdrawer,tv_cashdispered;
    private LinearLayout layout_expense,layout_cashdrop,layoutadd_one,layout_add_two;
    private RecyclerView recycle_expense;
    private List<GetEmployeeExpanses> getEmployeeExpansesList;
    private List<GetDenominationsdata> getDenominationsdata;
    public Dialog mDialog;
    private GetEmployeeLogin logindata;
    private int s_day,s_month,s_year;
    private ExpenseCashDrawerAdapter expensesAdapter;
    ArrayList<GetsalesTerminalsdata> getsalesTerminalsdata;
    CashDrawerFragment cashDrawerFragment;
    private EditText tv_date,tv_prev_bal,tv_credit_cash,tv_cashdrawersregisterd,tv_drawers_active,tv_drawers_dispered,tv_cashsales,
            tv_debitsales,tv_expensess,tv_expectedamt,tv_total_sales,tv_tills_ramt,tv_actual_draweramt,tv_varience;

    private Calendar calendar;
    int day,month,year;
    GetEmployeeAppSetting mBAsis;

    public void showDialog(final Context context, ArrayList<GetsalesTerminalsdata> getsalesTerminalsdata, GetEmployeeLogin logindata,
                           CashDrawerFragment cashDrawerFragment) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.close_store);
        tv_expenses=dialog.findViewById(R.id.tv_expenses);
        tv_closedrawer=dialog.findViewById(R.id.tv_closedrawer);
        layout_expense=dialog.findViewById(R.id.layout_expense);
        recycle_expense=dialog.findViewById(R.id.recycle_expense);
        layout_cashdrop=dialog.findViewById(R.id.layout_cashdrop);
        tv_date=dialog.findViewById(R.id.tv_date);
        tv_prev_bal=dialog.findViewById(R.id.tv_prev_bal);
        tv_credit_cash=dialog.findViewById(R.id.tv_credit_cash);
        tv_cashdrawersregisterd=dialog.findViewById(R.id.tv_cashdrawersregisterd);
        tv_drawers_active=dialog.findViewById(R.id.tv_drawers_active);
        tv_drawers_dispered=dialog.findViewById(R.id.tv_drawers_dispered);
        tv_cashsales=dialog.findViewById(R.id.tv_cashsales);
        tv_debitsales=dialog.findViewById(R.id.tv_debitsales);
        tv_expensess=dialog.findViewById(R.id.tv_expensess);
        tv_expectedamt=dialog.findViewById(R.id.tv_expectedamt);
        tv_total_sales=dialog.findViewById(R.id.tv_total_sales);
        tv_tills_ramt=dialog.findViewById(R.id.tv_tills_ramt);
        tv_actual_draweramt=dialog.findViewById(R.id.tv_actual_draweramt);
        tv_varience=dialog.findViewById(R.id.tv_varience);

        tv_save=dialog.findViewById(R.id.tv_save);
        tv_cancel=dialog.findViewById(R.id.tv_cancel);

        layoutadd_one=dialog.findViewById(R.id.layoutadd_one);
        layout_add_two=dialog.findViewById(R.id.layout_add_two);
        tv_prevday=dialog.findViewById(R.id.tv_prevday);
        tv_cashdrawer=dialog.findViewById(R.id.tv_cashdrawer);
        tv_cashdispered=dialog.findViewById(R.id.tv_cashdispered);

        layoutadd_one.setVisibility(View.VISIBLE);
        layout_add_two.setVisibility(View.VISIBLE);
        this.cashDrawerFragment=cashDrawerFragment;


        mBAsis = new Gson().fromJson(Preferences.INSTANCE.getPreference(context, "AppSetting"), GetEmployeeAppSetting.class);


        editable(tv_date);
        editable(tv_prev_bal);
        editable(tv_credit_cash);
        editable(tv_cashdrawersregisterd);
        editable(tv_drawers_active);
        editable(tv_drawers_dispered);
        editable(tv_cashsales);
        editable(tv_debitsales);
        editable(tv_expensess);
        editable(tv_expectedamt);
        editable(tv_total_sales);
        editable(tv_tills_ramt);
        editable(tv_actual_draweramt);
        editable(tv_varience);


        calendar=Calendar.getInstance();
        day=calendar.get(Calendar.DAY_OF_MONTH);
        month=calendar.get(Calendar.MONTH);
        year=calendar.get(Calendar.YEAR);
        int hr=calendar.get(Calendar.HOUR);
        int mn=calendar.get(Calendar.MINUTE);
        int sec=calendar.get(Calendar.SECOND);

        tv_date.setText(""+day+"-"+(month+1)+"-"+year+" "+hr+":"+mn+":"+sec);
        tv_date.setFocusable(false);
        tv_date.setFocusableInTouchMode(false);
        tv_date.setClickable(false);

        this.getsalesTerminalsdata=getsalesTerminalsdata;
        this.logindata=logindata;
        recycle_expense.setLayoutManager(new GridLayoutManager(context,1));
        getEmployeeExpansesList=new ArrayList<>();
        getDenominationsdata=new ArrayList<>();
        Calendar calendar=Calendar.getInstance();
        s_day=calendar.get(Calendar.DAY_OF_MONTH);
        s_month=calendar.get(Calendar.MONTH);
        s_year=calendar.get(Calendar.YEAR);
        expensesAdapter=new ExpenseCashDrawerAdapter(context,getEmployeeExpansesList);
        recycle_expense.setAdapter(expensesAdapter);

        tv_expenses.setBackgroundColor(context.getResources().getColor(R.color.light_blue_500));
        tv_closedrawer.setBackgroundColor(context.getResources().getColor(R.color.graylight));
        layout_expense.setVisibility(View.VISIBLE);
        layout_cashdrop.setVisibility(View.GONE);
        getExpensesList(context);

        tv_expenses.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tv_expenses.setBackgroundColor(context.getResources().getColor(R.color.light_blue_500));
                tv_closedrawer.setBackgroundColor(context.getResources().getColor(R.color.graylight));
                layout_expense.setVisibility(View.VISIBLE);
                layout_cashdrop.setVisibility(View.GONE);
                getExpensesList(context);
            }
        });


        tv_closedrawer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tv_closedrawer.setBackgroundColor(context.getResources().getColor(R.color.light_blue_500));
                tv_expenses.setBackgroundColor(context.getResources().getColor(R.color.graylight));
                layout_expense.setVisibility(View.GONE);
                layout_cashdrop.setVisibility(View.VISIBLE);
                getClosetill(context,0);

            }
        });


        tv_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                saveStore(context,1);
                //Toast.makeText(context, "close till save", Toast.LENGTH_SHORT).show();
            }
        });
        tv_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });


        dialog.show();
        dialog.setCancelable(true);

    }
    public void editable(EditText editText){
        editText.setFocusable(false);
        editText.setFocusableInTouchMode(false);
        editText.setClickable(false);

    }

    public void getExpensesList(final Context context){
        getEmployeeExpansesList.clear();
        mDialog = CUC.Companion.createDialog((Activity) context);
        RequetsInterface apiService = RequetsInterface.Factory.create();
        Map parameterMap =new HashMap<String, String>();
        parameterMap.put("api_key", String.valueOf(Preferences.INSTANCE.getPreference((context), "API_KEY")));
        if (logindata != null) {
            parameterMap.put("employee_id", logindata.getUser_id());
           // parameterMap.put("store_id", logindata.getUser_store_id());
        }

        parameterMap.put("terminal_id",mBAsis.getTerminal_id());
        parameterMap.put("from_date",s_year+"-"+(s_month+1)+"-"+s_day+" 00:00:00");
        parameterMap.put("to_date",s_year+"-"+(s_month+1)+"-"+s_day+" 23:59:59");

        System.out.println("aaaaaaaaaaa  parameterMap expenses  "+parameterMap);
        Log.i("aaaaaaaa   Result", "parameterMap : " + parameterMap);
        (new CompositeDisposable()).add(apiService.callExpensesterminal(parameterMap).
                observeOn(AndroidSchedulers.mainThread()).
                subscribeOn(Schedulers.io()).subscribe((Consumer)(new Consumer() {
            // $FF: synthetic method
            // $FF: bridge method
            public void accept(Object var1) {
                this.accept((GetEmpExpenses)var1);
            }

            public final void accept(GetEmpExpenses result) {
                System.out.println("aaaaaaaaa  result  "+result.toString());
                mDialog.cancel();
                if (result != null) {
                    mDialog.cancel();
                    System.out.println("aaaaaaaaa  result expenses  "+result.toString());
                    Log.i("aaaaaaa   Result", "" + result.toString());
                    if (result.getExpense_data().size()>0){
                        getEmployeeExpansesList.addAll(result.getExpense_data());
                        expensesAdapter.setRefresh(getEmployeeExpansesList);
                        CUC.Companion.displayToast((Context)context, "Sucess", "Sucess");
                    }else {
                        CUC.Companion.displayToast((Context)context, "No Data", "No Data");
                    }
                } else {
                    mDialog.cancel();
                }
            }
        }), (Consumer)(new Consumer() {
            // $FF: synthetic method
            // $FF: bridge method
            public void accept(Object var1) {
                this.accept((Throwable)var1);
            }

            public final void accept(Throwable error) {
                mDialog.cancel();
                Toast.makeText(context, context.getResources().getString(R.string.connection_to_database_failed), Toast.LENGTH_SHORT).show();
                error.printStackTrace();
                System.out.println("aaaaaaaa  error update "+error.getMessage());
            }
        })));
    }

    public void showdialog(Context context,String msg){
        AlertDialog.Builder builder1 = new AlertDialog.Builder(context);
        builder1.setMessage(""+msg);
        builder1.setCancelable(true);

        builder1.setPositiveButton(
                "Ok",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        AlertDialog alert11 = builder1.create();
        alert11.show();
    }
    public void getClosetill(final Context context,int i){
        getEmployeeExpansesList.clear();
        mDialog = CUC.Companion.createDialog((Activity) context);
        RequetsInterface apiService = RequetsInterface.Factory.create();
        Map parameterMap =new HashMap<String, String>();
        parameterMap.put("api_key", String.valueOf(Preferences.INSTANCE.getPreference((context), "API_KEY")));
        if (logindata != null) {
            parameterMap.put("emp_id", logindata.getUser_id());
            parameterMap.put("store_id", logindata.getUser_store_id());
        }

      //  parameterMap.put("terminal_id",mBAsis.getTerminal_id());
       // parameterMap.put("till_close",""+i);

        System.out.println("aaaaaaaaaaa  parameterMap close tilll "+parameterMap);
        Log.i("aaaaaaaa   Result", "parameterMap : " + parameterMap);
        (new CompositeDisposable()).add(apiService.callStoreClose(parameterMap).
                observeOn(AndroidSchedulers.mainThread()).
                subscribeOn(Schedulers.io()).subscribe((Consumer)(new Consumer() {
            // $FF: synthetic method
            // $FF: bridge method
            public void accept(Object var1) {
                this.accept((GetStoreClose)var1);
            }

            public final void accept(GetStoreClose result) {
                System.out.println("aaaaaaaaa  result storeclose "+result.toString());
                mDialog.cancel();
                if (result != null) {
                    mDialog.cancel();
                    System.out.println("aaaaaaaaa  result close till  "+result.toString());
                    Log.i("aaaaaaa   Result", "" + result.toString());
                    if (result.getStatus().equalsIgnoreCase("1")){
                        //showdialog(context,result.getMessage());

                        tv_prev_bal.setText("$ "+result.getPrevious_day_cash_balance());
                        tv_credit_cash.setText("$ "+result.getCredit_cash());
                        tv_cashdrawersregisterd.setText("$ "+result.getTills_registered());
                        tv_drawers_active.setText("$ "+result.getTills_active());
                        tv_drawers_dispered.setText("$ "+result.getCash_per_till_dispersed());
                        tv_cashsales.setText("$ "+result.getCash());
                        tv_debitsales.setText("$ "+result.getNet());
                        tv_expensess.setText("$ "+result.getExpenses());
                        tv_expectedamt.setText("$ "+result.getExpected_count());
                        tv_total_sales.setText("$ "+result.getTotal_storesale());
                        tv_tills_ramt.setText("$ "+result.getTotal_storecash());
                        tv_actual_draweramt.setText("$ "+result.getActual_tillsamount());
                        tv_varience.setText("$ "+result.getVariance());

                        if (!result.getTills_active().equalsIgnoreCase("0")){
                            tv_save.setClickable(false);
                            tv_save.setEnabled(false);
                            tv_save.setTextColor(context.getResources().getColor(R.color.graylight));
                            cashDrawerFragment.showAlert(result.getMessage());
                        }
                    }else {
                        tv_save.setClickable(false);
                        tv_save.setEnabled(false);
                        cashDrawerFragment.showAlert(result.getMessage());
                    }

                } else {
                    mDialog.cancel();
                }
            }
        }), (Consumer)(new Consumer() {
            // $FF: synthetic method
            // $FF: bridge method
            public void accept(Object var1) {
                this.accept((Throwable)var1);
            }

            public final void accept(Throwable error) {
                mDialog.cancel();
                Toast.makeText(context, context.getResources().getString(R.string.connection_to_database_failed), Toast.LENGTH_SHORT).show();
                error.printStackTrace();
                System.out.println("aaaaaaaa  error update "+error.getMessage());
            }
        })));
    }

    public void saveStore(final Context context,int i){
        getEmployeeExpansesList.clear();
        mDialog = CUC.Companion.createDialog((Activity) context);
        RequetsInterface apiService = RequetsInterface.Factory.create();
        Map parameterMap =new HashMap<String, String>();
        parameterMap.put("api_key", String.valueOf(Preferences.INSTANCE.getPreference((context), "API_KEY")));
        if (logindata != null) {
            parameterMap.put("emp_id", logindata.getUser_id());
            parameterMap.put("store_id", logindata.getUser_store_id());
        }

          parameterMap.put("till_close","1");
        // parameterMap.put("till_close",""+i);

        System.out.println("aaaaaaaaaaa  parameterMap close tilll "+parameterMap);
        Log.i("aaaaaaaa   Result", "parameterMap : " + parameterMap);
        (new CompositeDisposable()).add(apiService.callStoreClose(parameterMap).
                observeOn(AndroidSchedulers.mainThread()).
                subscribeOn(Schedulers.io()).subscribe((Consumer)(new Consumer() {
            // $FF: synthetic method
            // $FF: bridge method
            public void accept(Object var1) {
                this.accept((GetStoreClose)var1);
            }

            public final void accept(GetStoreClose result) {
                System.out.println("aaaaaaaaa  result storeclose111 "+result.toString());
                mDialog.cancel();
                if (result != null) {
                    mDialog.cancel();
                    System.out.println("aaaaaaaaa  result close till  "+result.toString());
                    Log.i("aaaaaaa   Result", "" + result.toString());
                    showdialog(context,result.getMessage());

                } else {
                    mDialog.cancel();
                }
            }
        }), (Consumer)(new Consumer() {
            // $FF: synthetic method
            // $FF: bridge method
            public void accept(Object var1) {
                this.accept((Throwable)var1);
            }

            public final void accept(Throwable error) {
                mDialog.cancel();
                Toast.makeText(context, context.getResources().getString(R.string.connection_to_database_failed), Toast.LENGTH_SHORT).show();
                error.printStackTrace();
                System.out.println("aaaaaaaa  error update "+error.getMessage());
            }
        })));
    }

}
