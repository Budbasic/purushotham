package com.budbasic.posconsole.dialogs;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.budbasic.customer.global.Preferences;
import com.budbasic.global.CUC;
import com.budbasic.posconsole.R;
import com.budbasic.posconsole.adapter.ExpenseCashDrawerAdapter;
import com.budbasic.posconsole.adapter.ProductprintAdapter;
import com.budbasic.posconsole.reception.CashDrawerFragment;
import com.budbasic.posconsole.reception.fragment.PrintPageFragment;
import com.budbasic.posconsole.webservice.RequetsInterface;
import com.google.gson.Gson;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.Writer;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.oned.Code128Writer;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;
import com.kotlindemo.model.GetDenominations;
import com.kotlindemo.model.GetDenominationsdata;
import com.kotlindemo.model.GetEmpExpenses;
import com.kotlindemo.model.GetEmployeeAppSetting;
import com.kotlindemo.model.GetEmployeeExpanses;
import com.kotlindemo.model.GetEmployeeLogin;
import com.kotlindemo.model.GetEmployeeSearchProducts;
import com.kotlindemo.model.GetProducts;
import com.kotlindemo.model.GetTillClose;
import com.kotlindemo.model.GetsalesTerminalsdata;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

public class PrintpageDailogs {

   private Context context;
    public Dialog mDialog;
    GetEmployeeLogin logindata;
    ArrayList<GetProducts> productlist;
    private ProductprintAdapter productprintAdapter;
    private ImageView img_close;
    private SearchView search_product;
    ProgressBar simpleProgressBar;
    private RecyclerView recycle_searchproduct;
    PrintPageFragment printPageFragment;
    private int number_print=1;
    private Bitmap barcodebitmap;

    public void showDialog(Context context, PrintPageFragment printPageFragment, GetEmployeeLogin logindata) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.store_searchs);
        img_close=dialog.findViewById(R.id.img_close);
        search_product=dialog.findViewById(R.id.search_product);
        simpleProgressBar=dialog.findViewById(R.id.simpleProgressBar);
        recycle_searchproduct=dialog.findViewById(R.id.recycle_searchproduct);


        this.logindata=logindata;
        this.printPageFragment=printPageFragment;
        productlist=new ArrayList<>();
        recycle_searchproduct.setLayoutManager(new GridLayoutManager(context,1));
        productprintAdapter=new ProductprintAdapter(context,productlist,printPageFragment,dialog);
        recycle_searchproduct.setAdapter(productprintAdapter);
        getExpensesList(context);
        img_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        dialog.show();
        dialog.setCancelable(true);

    }

    public void getExpensesList(final Context context){
        mDialog = CUC.Companion.createDialog((Activity) context);
        RequetsInterface apiService = RequetsInterface.Factory.create();
        Map parameterMap =new HashMap<String, String>();
        parameterMap.put("api_key", String.valueOf(Preferences.INSTANCE.getPreference((context), "API_KEY")));
        if (logindata != null) {
            parameterMap.put("employee_id", logindata.getUser_id());
            parameterMap.put("store_id", logindata.getUser_store_id());
        }

        parameterMap.put("product","");


        System.out.println("aaaaaaaaaaa  parameterMap expenses  "+parameterMap);
        Log.i("aaaaaaaa   Result", "parameterMap : " + parameterMap);
        (new CompositeDisposable()).add(apiService.callEmployeeSearchProducts(parameterMap).
                observeOn(AndroidSchedulers.mainThread()).
                subscribeOn(Schedulers.io()).subscribe((Consumer)(new Consumer() {
            // $FF: synthetic method
            // $FF: bridge method
            public void accept(Object var1) {
                this.accept((GetEmployeeSearchProducts)var1);
            }

            public final void accept(GetEmployeeSearchProducts result) {
                System.out.println("aaaaaaaaa  result  "+result.toString());
                mDialog.cancel();
                if (result != null) {
                    mDialog.cancel();
                    System.out.println("aaaaaaaaa  result expenses  "+result.toString());
                    Log.i("aaaaaaa   Result", "" + result.toString());
                    if (result.getProducts().size()>0){
                        productlist.addAll(result.getProducts());
                        productprintAdapter.setRefresh(productlist);
                        CUC.Companion.displayToast((Context)context, "Sucess", "Sucess");
                    }else {
                        CUC.Companion.displayToast((Context)context, "No Data", "No Data");
                    }
                } else {
                    mDialog.cancel();
                }
            }
        }), (Consumer)(new Consumer() {
            // $FF: synthetic method
            // $FF: bridge method
            public void accept(Object var1) {
                this.accept((Throwable)var1);
            }

            public final void accept(Throwable error) {
                mDialog.cancel();
                Toast.makeText(context, context.getResources().getString(R.string.connection_to_database_failed), Toast.LENGTH_SHORT).show();
                error.printStackTrace();
                System.out.println("aaaaaaaa  error update "+error.getMessage());
            }
        })));
    }


    public void showDialog(final Context context, final PrintPageFragment printPageFragment, GetEmployeeLogin logindata, final GetProducts getProducts, final Dialog dialog1) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.lable_print);
        ImageView img_lable=dialog.findViewById(R.id.img_lable);
        ImageView img_close=dialog.findViewById(R.id.img_close);
        TextView tv_minus=dialog.findViewById(R.id.tv_minus);
        final TextView tv_text=dialog.findViewById(R.id.tv_text);
        TextView tv_plus=dialog.findViewById(R.id.tv_plus);
        TextView tv_cancle=dialog.findViewById(R.id.tv_cancle);
        TextView tv_print=dialog.findViewById(R.id.tv_submit);
        TextView tv_product_name=dialog.findViewById(R.id.tv_product_name);
        TextView text_barcode=dialog.findViewById(R.id.text_barcode);
        final LinearLayout layout_print=dialog.findViewById(R.id.layout_print);

        tv_product_name.setText(""+getProducts.getPacket_name());

        try {
            Hashtable<EncodeHintType, ErrorCorrectionLevel> hintMap = new Hashtable<EncodeHintType, ErrorCorrectionLevel>();
            hintMap.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.L);
            Writer codeWriter;
            codeWriter = new Code128Writer();
            BitMatrix byteMatrix = codeWriter.encode(getProducts.getPacket_barcode(), BarcodeFormat.CODE_128,400, 200, hintMap);
            int width = byteMatrix.getWidth();
            int height = byteMatrix.getHeight();
            barcodebitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
            for (int i = 0; i < width; i++) {
                for (int j = 0; j < height; j++) {
                    barcodebitmap.setPixel(i, j, byteMatrix.get(i, j) ? Color.BLACK : Color.WHITE);
                }
            }
            text_barcode.setText(""+getProducts.getPacket_barcode());
            img_lable.setImageBitmap(barcodebitmap);
        }
        catch (Exception e) {
            Toast.makeText(context, e.getMessage(), Toast.LENGTH_LONG).show();
        }
        tv_text.setText(""+number_print);
        tv_minus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (number_print>1){
                    System.out.println("aaaaaaaa number_print iffff "+number_print);
                    number_print =number_print-1;
                    tv_text.setText(""+number_print);
                }else {
                    System.out.println("aaaaaaaa number_print elseee "+number_print);
                }
            }
        });
        tv_plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                number_print=number_print+1;
                tv_text.setText(""+number_print);
            }
        });
        img_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        tv_print.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                System.out.println("aaaaaaaaa  number  "+number_print);
                dialog.dismiss();
                dialog1.dismiss();
                for (int i=1;i<=number_print;i++){
                    printPageFragment.print(barcodebitmap, layout_print, ""+getProducts.getPacket_name());
                }
            }
        });
        tv_cancle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        dialog.show();
        dialog.setCancelable(true);
    }
}
