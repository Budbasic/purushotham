package com.budbasic.posconsole.dialogs;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.budbasic.customer.global.Preferences;
import com.budbasic.global.CUC;
import com.budbasic.posconsole.R;
import com.budbasic.posconsole.adapter.NotesDialogAdaper;
import com.budbasic.posconsole.reception.fragment.CustomerSearchFragment;
import com.budbasic.posconsole.webservice.RequetsInterface;
import com.kotlindemo.model.GetEmployeeAppSetting;
import com.kotlindemo.model.GetEmployeeLogin;
import com.kotlindemo.model.GetNotes;
import com.kotlindemo.model.GetNotesDatashow;
import com.kotlindemo.model.GetStatus;
import com.kotlindemo.model.GetsalesTerminalsdata;
import com.kotlindemo.model.Patient_data;
import com.tbruyelle.rxpermissions2.RxPermissions;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Ref;

public class AddDocumentDailog  {

    private TextView tvPhone,tvDocuments,tvNotes,tvpatients,tv_patientName,tv_mmpNumber,tv_alertcheck,tv_alertscreen;
    private LinearLayout ll_phone,ll_document,ll_patient;
    private RelativeLayout rl_Notes;
    private ImageView img_infoImage,iv_scan_id;
    private CardView cv_savephoto,cv_saveinfo,cv_delete,alertcheck,alertscreen,cv_submit;
    public static  EditText tv_phoneNumber,tv_email,edt_note_name,edt_note_message;
    private RecyclerView recycle_notes;
    private ArrayList<GetNotes> noteslist;
    public Dialog mDialog;
    private GetEmployeeLogin logindata;
    private Patient_data patient_data;
    private NotesDialogAdaper notesDialogAdaper;
    GetsalesTerminalsdata getsalesTerminalsdata;
    GetEmployeeAppSetting appSettingData;
    String patientImagePath;
    private int notespriority;
    public static String noteid="";
    private CustomerSearchFragment customerSearchFragment;
    private Context context;
    private RxPermissions rxPermissions;
    private String timeStamp;
    private  int CAMERA_REQUEST;
    private  int CAMERA_CAPTURE_IMAGE_REQUEST_CODE;
    private  int EXTERNAL_CAMERA;
    private File myDir;
    private  int MY_SCAN_REQUEST_CODE = 3;
    private Bitmap bitmap;
    private int check;
    private Uri uri;
    private boolean isImageSelected;
    private File imageCompanyFile;

    public void showDialog(final Context context, GetEmployeeAppSetting appSettingData, GetEmployeeLogin logindata, Patient_data data,
                           String patientImagePath, final CustomerSearchFragment customerSearchFragment) {

        this.context=context;
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.patient_manager_viewedit_dialog_new);
        tvPhone=dialog.findViewById(R.id.tvPhone);
        tvDocuments=dialog.findViewById(R.id.tvDocuments);
        tvNotes=dialog.findViewById(R.id.tvNotes);
        tvpatients=dialog.findViewById(R.id.tvpatients);
        ll_phone=dialog.findViewById(R.id.ll_phone);
        ll_document=dialog.findViewById(R.id.ll_document);
        rl_Notes=dialog.findViewById(R.id.rl_Notes);
        ll_patient=dialog.findViewById(R.id.ll_patient);
        img_infoImage=dialog.findViewById(R.id.img_infoImage);
        cv_savephoto=dialog.findViewById(R.id.cv_savephoto);
        tv_patientName=dialog.findViewById(R.id.tv_patientName);
        iv_scan_id=dialog.findViewById(R.id.iv_scan_id);
        tv_mmpNumber=dialog.findViewById(R.id.tv_mmpNumber);
        tv_phoneNumber=dialog.findViewById(R.id.tv_phoneNumber);
        tv_email=dialog.findViewById(R.id.tv_email);
        cv_saveinfo=dialog.findViewById(R.id.cv_saveinfo);
        edt_note_name=dialog.findViewById(R.id.edt_note_name);
        cv_delete=dialog.findViewById(R.id.cv_delete);
        edt_note_message=dialog.findViewById(R.id.edt_note_message);
        alertcheck=dialog.findViewById(R.id.alertcheck);
        tv_alertcheck=dialog.findViewById(R.id.tv_alertcheck);
        alertscreen=dialog.findViewById(R.id.alertscreen);
        tv_alertscreen=dialog.findViewById(R.id.tv_alertscreen);
        cv_submit=dialog.findViewById(R.id.cv_submit);
        recycle_notes=dialog.findViewById(R.id.recycle_notes);
        this.getsalesTerminalsdata=getsalesTerminalsdata;
        this.logindata=logindata;
        this.appSettingData=appSettingData;
        this.patient_data=data;
        this.patientImagePath=patientImagePath;
        this.customerSearchFragment=customerSearchFragment;

        recycle_notes.setLayoutManager(new GridLayoutManager(context,2));
        noteslist=new ArrayList<GetNotes>();

        AddDocumentDailog addDocumentDailog=new AddDocumentDailog();
       // notesDialogAdaper=new NotesDialogAdaper(context,noteslist,dialog,addDocumentDailog,customerSearchFragment);
        recycle_notes.setAdapter(notesDialogAdaper);
        getNoteslist(context);

        alertcheck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                notespriority=0;
                tv_alertcheck.setBackgroundColor(context.getResources().getColor(R.color.blue));
                tv_alertscreen.setBackgroundColor(context.getResources().getColor(R.color.graylight));
                tv_alertcheck.setBackgroundColor(context.getResources().getColor(R.color.white));
                tv_alertscreen.setBackgroundColor(context.getResources().getColor(R.color.black));

            }
        });
        alertscreen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                notespriority=1;
                tv_alertcheck.setBackgroundColor(context.getResources().getColor(R.color.graylight));
                tv_alertscreen.setBackgroundColor(context.getResources().getColor(R.color.blue));
                tv_alertcheck.setBackgroundColor(context.getResources().getColor(R.color.black));
                tv_alertscreen.setBackgroundColor(context.getResources().getColor(R.color.white));

            }
        });
        tvpatients.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tvPhone.setBackgroundColor(context.getResources().getColor(R.color.graylight));
                tvNotes.setBackgroundColor(context.getResources().getColor(R.color.graylight));
                tvDocuments.setBackgroundColor(context.getResources().getColor(R.color.graylight));
                tvpatients.setBackgroundColor(context.getResources().getColor(R.color.blue));

                ll_patient.setVisibility(View.VISIBLE);
                rl_Notes.setVisibility(View.GONE);
                ll_document.setVisibility(View.GONE);
                ll_phone.setVisibility(View.GONE);
            }
        });
        tvPhone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tvPhone.setBackgroundColor(context.getResources().getColor(R.color.blue));
                tvNotes.setBackgroundColor(context.getResources().getColor(R.color.graylight));
                tvDocuments.setBackgroundColor(context.getResources().getColor(R.color.graylight));
                tvpatients.setBackgroundColor(context.getResources().getColor(R.color.graylight));

                ll_patient.setVisibility(View.GONE);
                rl_Notes.setVisibility(View.GONE);
                ll_document.setVisibility(View.GONE);
                ll_phone.setVisibility(View.VISIBLE);

            }
        });
        tvNotes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tvPhone.setBackgroundColor(context.getResources().getColor(R.color.graylight));
                tvNotes.setBackgroundColor(context.getResources().getColor(R.color.blue));
                tvDocuments.setBackgroundColor(context.getResources().getColor(R.color.graylight));
                tvpatients.setBackgroundColor(context.getResources().getColor(R.color.graylight));

                ll_patient.setVisibility(View.GONE);
                rl_Notes.setVisibility(View.VISIBLE);
                ll_document.setVisibility(View.GONE);
                ll_phone.setVisibility(View.GONE);

            }
        });
        tvDocuments.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tvPhone.setBackgroundColor(context.getResources().getColor(R.color.graylight));
                tvNotes.setBackgroundColor(context.getResources().getColor(R.color.graylight));
                tvDocuments.setBackgroundColor(context.getResources().getColor(R.color.blue));
                tvpatients.setBackgroundColor(context.getResources().getColor(R.color.graylight));

                ll_patient.setVisibility(View.GONE);
                rl_Notes.setVisibility(View.GONE);
                ll_document.setVisibility(View.VISIBLE);
                ll_phone.setVisibility(View.GONE);

            }
        });
        cv_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String title=edt_note_name.getText().toString().trim();
                String msg=edt_note_message.getText().toString().trim();
                if (title.isEmpty()){
                    Toast.makeText(context, "Please Enter Title", Toast.LENGTH_SHORT).show();
                }else {
                    if (msg.isEmpty()){
                        Toast.makeText(context, "Please Enter Dscription", Toast.LENGTH_SHORT).show();
                    }else {
                        AddNote(context);
                    }
                }
            }
        });cv_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String title=edt_note_name.getText().toString().trim();
                System.out.println("aaaaaaaa   noteid  "+noteid);
                if (noteid.isEmpty() || noteid.equalsIgnoreCase("")){
                    Toast.makeText(context, "Not a Valid note", Toast.LENGTH_SHORT).show();
                }else {
                    noteDelete(context);
                }
            }
        });
        img_infoImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
              //  customerSearchFragment.pickImage();

               // pickImage();
            }
        });

        dialog.show();
        dialog.setCancelable(true);

    }
    public void pickImage() {
        if (ActivityCompat.checkSelfPermission(context, "android.permission.READ_EXTERNAL_STORAGE") != 0) {
            this.getPermissions();
        } else {
            final Ref.ObjectRef dialog = new Ref.ObjectRef();
            dialog.element = new Dialog(this.context);
            ((Dialog)dialog.element).requestWindowFeature(1);
            StrictMode.ThreadPolicy policy = (new StrictMode.ThreadPolicy.Builder()).permitAll().build();
            StrictMode.setThreadPolicy(policy);
            ((Dialog)dialog.element).setContentView(R.layout.pic_image_dialog);
            ((Dialog)dialog.element).show();
            ((TextView)((Dialog)dialog.element).findViewById(R.id.tv_cancelDialog)).setOnClickListener((View.OnClickListener)(new View.OnClickListener() {
                public final void onClick(View it) {
                    ((Dialog)dialog.element).dismiss();
                }
            }));
            ((TextView)((Dialog)dialog.element).findViewById(R.id.tv_openGallery)).setOnClickListener((View.OnClickListener)(new View.OnClickListener() {
                public final void onClick(View it) {
                    Intent pickPhoto = new Intent("android.intent.action.PICK", MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    ((Activity) context).startActivityForResult(pickPhoto, 1);
                    ((Dialog)dialog.element).dismiss();
                }
            }));
            ((TextView)((Dialog)dialog.element).findViewById(R.id.tv_OpenCamera)).setOnClickListener((View.OnClickListener)(new View.OnClickListener() {
                public final void onClick(View it) {
                   // customerSearchFragment.openCamera();
                  //  takePicture();
                    ((Dialog)dialog.element).dismiss();
                }
            }));
        }
    }
    public final void getPermissions() {
        this.rxPermissions = new RxPermissions((Activity) context);
        RxPermissions var10000 = this.rxPermissions;
        if (var10000 == null) {
            Intrinsics.throwNpe();
        }

        var10000.request(new String[]{"android.permission.READ_EXTERNAL_STORAGE", "android.permission.WRITE_EXTERNAL_STORAGE"}).subscribe((Consumer)(new Consumer() {
            // $FF: synthetic method
            // $FF: bridge method
            public void accept(Object var1) {
                this.accept((Boolean)var1);
            }

            public final void accept(Boolean grant) {
                Intrinsics.checkExpressionValueIsNotNull(grant, "grant");
                if (grant) {
                    AddDocumentDailog.this.pickImage();
                } else {
                    Toast.makeText(context, "Permission Denied", Toast.LENGTH_SHORT).show();
                }

            }
        }));
    }
    public void takePicture() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        uri = Uri.fromFile(getOutputMediaFile());
        intent.putExtra(MediaStore.EXTRA_OUTPUT, imageCompanyFile);

        ((Activity) context).startActivityForResult(intent, 100);
    }
    private static File getOutputMediaFile(){
        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES), "CameraDemo");

        if (!mediaStorageDir.exists()){
            if (!mediaStorageDir.mkdirs()){
                return null;
            }
        }

        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        return new File(mediaStorageDir.getPath() + File.separator +
                "IMG_"+ timeStamp + ".jpg");
    }

   /* @SuppressLint("RestrictedApi")
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        this.onActivityResult(requestCode, resultCode, data);
        System.out.println("aaaaaaaaaaaa  data  "+data.toString());
        if (requestCode == 100) {
            if (resultCode == RESULT_OK) {

                img_infoImage.setImageURI(uri);
            }
        }else {
            System.out.println("aaaaa
            aaaaaa data  "+data.toString());
        }
    }*/

    public void getNoteslist(final Context context){
        noteslist.clear();
        mDialog = CUC.Companion.createDialog((Activity) context);
        RequetsInterface apiService = RequetsInterface.Factory.create();
        Map parameterMap =new HashMap<String, String>();
        parameterMap.put("api_key", String.valueOf(Preferences.INSTANCE.getPreference((context), "API_KEY")));
        if (logindata != null) {
            parameterMap.put("employee_id", logindata.getUser_id());
            // parameterMap.put("store_id", logindata.getUser_store_id());
        }

        parameterMap.put("patient_id",patient_data.getPatient_id());

        System.out.println("aaaaaaaaaaa  parameterMap expenses  "+parameterMap);
        Log.i("aaaaaaaa   Result", "parameterMap : " + parameterMap);
        (new CompositeDisposable()).add(apiService.callGetNotes(parameterMap).
                observeOn(AndroidSchedulers.mainThread()).
                subscribeOn(Schedulers.io()).subscribe((Consumer)(new Consumer() {
            // $FF: synthetic method
            // $FF: bridge method
            public void accept(Object var1) {
                this.accept((GetNotesDatashow)var1);
            }

            public final void accept(GetNotesDatashow result) {
                System.out.println("aaaaaaaaa  result  "+result.toString());
                mDialog.cancel();
                if (result != null) {
                    mDialog.cancel();
                    System.out.println("aaaaaaaaa  result expenses  "+result.toString());
                    Log.i("aaaaaaa   Result", "" + result.toString());
                    if (result.getNotes().size()>0){
                        noteslist.addAll(result.getNotes());
                        notesDialogAdaper.setRefresh(noteslist);
                        CUC.Companion.displayToast((Context)context, "Sucess", "Sucess");
                    }else {
                        CUC.Companion.displayToast((Context)context, "No Data", "No Data");
                    }
                } else {
                    mDialog.cancel();
                }
            }
        }), (Consumer)(new Consumer() {
            // $FF: synthetic method
            // $FF: bridge method
            public void accept(Object var1) {
                this.accept((Throwable)var1);
            }

            public final void accept(Throwable error) {
                mDialog.cancel();
                Toast.makeText(context, context.getResources().getString(R.string.connection_to_database_failed), Toast.LENGTH_SHORT).show();
                error.printStackTrace();
                System.out.println("aaaaaaaa  error update "+error.getMessage());
            }
        })));
    }

    public void AddNote(final Context context){
        noteslist.clear();
        mDialog = CUC.Companion.createDialog((Activity) context);
        RequetsInterface apiService = RequetsInterface.Factory.create();
        Map parameterMap =new HashMap<String, String>();
        parameterMap.put("api_key", String.valueOf(Preferences.INSTANCE.getPreference((context), "API_KEY")));
        if (logindata != null) {
            parameterMap.put("employee_id", logindata.getUser_id());
            // parameterMap.put("store_id", logindata.getUser_store_id());
        }

        parameterMap.put("patient_id",patient_data.getPatient_id());
        parameterMap.put("notes_title",edt_note_name.getText().toString().trim());
        parameterMap.put("notes",edt_note_message.getText().toString().trim());
        parameterMap.put("notes_priority",""+notespriority);

        System.out.println("aaaaaaaaaaa  parameterMap expenses  "+parameterMap);
        Log.i("aaaaaaaa   Result", "parameterMap : " + parameterMap);
        (new CompositeDisposable()).add(apiService.callAddNote(parameterMap).
                observeOn(AndroidSchedulers.mainThread()).
                subscribeOn(Schedulers.io()).subscribe((Consumer)(new Consumer() {
            // $FF: synthetic method
            // $FF: bridge method
            public void accept(Object var1) {
                this.accept((GetStatus)var1);
            }

            public final void accept(GetStatus result) {
                System.out.println("aaaaaaaaa  result  "+result.toString());
                mDialog.cancel();
                if (result != null) {
                    mDialog.cancel();
                    System.out.println("aaaaaaaaa  result expenses  "+result.toString());
                    Log.i("aaaaaaa   Result", "" + result.toString());
                    if (result.getStatus().equalsIgnoreCase("0")){
                        edt_note_name.setText("");
                        edt_note_message.setText("");
                       getNoteslist(context);
                        CUC.Companion.displayToast((Context)context, result.getMessage(), result.getMessage());
                    }else {
                        CUC.Companion.displayToast((Context)context, ""+result.getMessage(), ""+result.getMessage());
                    }
                } else {
                    mDialog.cancel();
                }
            }
        }), (Consumer)(new Consumer() {
            // $FF: synthetic method
            // $FF: bridge method
            public void accept(Object var1) {
                this.accept((Throwable)var1);
            }

            public final void accept(Throwable error) {
                mDialog.cancel();
                Toast.makeText(context, context.getResources().getString(R.string.connection_to_database_failed), Toast.LENGTH_SHORT).show();
                error.printStackTrace();
                System.out.println("aaaaaaaa  error update "+error.getMessage());
            }
        })));
    }

    public void noteDelete(final Context context){
        noteslist.clear();
        mDialog = CUC.Companion.createDialog((Activity) context);
        RequetsInterface apiService = RequetsInterface.Factory.create();
        Map parameterMap =new HashMap<String, String>();
        parameterMap.put("api_key", String.valueOf(Preferences.INSTANCE.getPreference((context), "API_KEY")));
        if (logindata != null) {
            parameterMap.put("employee_id", logindata.getUser_id());
            // parameterMap.put("store_id", logindata.getUser_store_id());
        }

        parameterMap.put("patient_id",patient_data.getPatient_id());
        parameterMap.put("notes_id",noteid);

        System.out.println("aaaaaaaaaaa  parameterMap expenses  "+parameterMap);
        Log.i("aaaaaaaa   Result", "parameterMap : " + parameterMap);
        (new CompositeDisposable()).add(apiService.callDeleteNote(parameterMap).
                observeOn(AndroidSchedulers.mainThread()).
                subscribeOn(Schedulers.io()).subscribe((Consumer)(new Consumer() {
            // $FF: synthetic method
            // $FF: bridge method
            public void accept(Object var1) {
                this.accept((GetStatus)var1);
            }

            public final void accept(GetStatus result) {
                System.out.println("aaaaaaaaa  result  "+result.toString());
                mDialog.cancel();
                if (result != null) {
                    mDialog.cancel();
                    System.out.println("aaaaaaaaa  result expenses  "+result.toString());
                    Log.i("aaaaaaa   Result", "" + result.toString());
                    if (result.getStatus().equalsIgnoreCase("0")){
                        edt_note_name.setText("");
                        edt_note_message.setText("");
                        getNoteslist(context);
                        CUC.Companion.displayToast((Context)context, result.getMessage(), result.getMessage());
                    }else {
                        CUC.Companion.displayToast((Context)context, ""+result.getMessage(), ""+result.getMessage());
                    }
                } else {
                    mDialog.cancel();
                }
            }
        }), (Consumer)(new Consumer() {
            // $FF: synthetic method
            // $FF: bridge method
            public void accept(Object var1) {
                this.accept((Throwable)var1);
            }

            public final void accept(Throwable error) {
                mDialog.cancel();
                Toast.makeText(context, context.getResources().getString(R.string.connection_to_database_failed), Toast.LENGTH_SHORT).show();
                error.printStackTrace();
                System.out.println("aaaaaaaa  error update "+error.getMessage());
            }
        })));
    }

    public void onClickNote(GetNotes getNotes) {
        this.noteid=getNotes.getPnote_id();
        System.out.println("aaaaaaaaaa  "+getNotes.getPnote_title());
        edt_note_name.setText(getNotes.getPnote_title());
        edt_note_message.setText(getNotes.getPnote_notes());
    }

    public void setOnActivityforresults( Uri uri) {
        System.out.println("aaaaaaaaaa  imageCompanyFile "+ this.uri);
        img_infoImage.setImageURI(uri);
    }

    public void setBitmap(@Nullable Bitmap bitmap) {
        img_infoImage.setImageBitmap(bitmap);
    }

    public void saveFile( File persistImage) {
        System.out.println("aaaaaaaaaaa  filee "+persistImage.toString());
    }


  /*  public final void openCamera() {
        int version = Build.VERSION.SDK_INT;
        String var10001;
        Intent intent;
        File f;
        if (version >= 23) {
            if (ContextCompat.checkSelfPermission(context, "android.permission.CAMERA") != 0) {
                if (ActivityCompat.shouldShowRequestPermissionRationale((Activity) context, "android.permission.CAMERA")) {
                    ActivityCompat.requestPermissions( ((Activity) context), new String[]{"android.permission.CAMERA"}, this.CAMERA_REQUEST);
                } else {
                    ActivityCompat.requestPermissions( ((Activity) context), new String[]{"android.permission.CAMERA"}, this.CAMERA_REQUEST);
                }
            } else if (ContextCompat.checkSelfPermission(context, "android.permission.WRITE_EXTERNAL_STORAGE") != 0) {
                if (ActivityCompat.shouldShowRequestPermissionRationale((Activity) context, "android.permission.WRITE_EXTERNAL_STORAGE")) {
                    ActivityCompat.requestPermissions((Activity) context, new String[]{"android.permission.WRITE_EXTERNAL_STORAGE"}, this.EXTERNAL_CAMERA);
                } else {
                    ActivityCompat.requestPermissions((Activity) context, new String[]{"android.permission.WRITE_EXTERNAL_STORAGE"}, this.EXTERNAL_CAMERA);
                }
            } else {
                var10001 = (new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss", Locale.US)).format(new Date());
                Intrinsics.checkExpressionValueIsNotNull(var10001, "SimpleDateFormat(\"yyyy.M…Locale.US).format(Date())");
                this.timeStamp = var10001;
                intent = new Intent("android.media.action.IMAGE_CAPTURE");
                f = new File(this.myDir, this.timeStamp + ".jpg");
                Uri f1 = FileProvider.getUriForFile((Context)(Activity) context, "com.budbasic.posconsole.fileprovider", f);
                intent.putExtra("output", (Parcelable)f1);
                intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                ((Activity) context).startActivityForResult(intent, this.CAMERA_CAPTURE_IMAGE_REQUEST_CODE);
                // startActivityForResult(intent, this.CAMERA_CAPTURE_IMAGE_REQUEST_CODE);
            }
        } else {
            var10001 = (new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss", Locale.US)).format(new Date());
            Intrinsics.checkExpressionValueIsNotNull(var10001, "SimpleDateFormat(\"yyyy.M…Locale.US).format(Date())");
            this.timeStamp = var10001;
            intent = new Intent("android.media.action.IMAGE_CAPTURE");
            f = new File(this.myDir, this.timeStamp + ".jpg");
            intent.putExtra("output", (Parcelable)Uri.fromFile(f));
            ((Activity) context).startActivityForResult(intent, this.CAMERA_CAPTURE_IMAGE_REQUEST_CODE);
        }

    }
    public final void getPermissions() {
        this.rxPermissions = new RxPermissions((Activity) context);
        RxPermissions var10000 = this.rxPermissions;
        if (var10000 == null) {
            Intrinsics.throwNpe();
        }

        var10000.request(new String[]{"android.permission.READ_EXTERNAL_STORAGE", "android.permission.WRITE_EXTERNAL_STORAGE"}).subscribe((Consumer)(new Consumer() {
            // $FF: synthetic method
            // $FF: bridge method
            public void accept(Object var1) {
                this.accept((Boolean)var1);
            }

            public final void accept(Boolean grant) {
                Intrinsics.checkExpressionValueIsNotNull(grant, "grant");
                if (grant) {
                    AddDocumentDailog.this.pickImage();
                } else {
                    Toast.makeText(context, "Permission Denied", Toast.LENGTH_SHORT).show();
                }

            }
        }));
    }
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent imageReturnedIntent) {
        this.onActivityResult(requestCode, resultCode, imageReturnedIntent);
        // super.onActivityResult(requestCode, resultCode, imageReturnedIntent);
        Integer var10000;
        ImageView var19;
        if (requestCode == this.MY_SCAN_REQUEST_CODE || requestCode == CardIOActivity.RESULT_SCAN_SUPPRESSED) {
            try {
                label146: {
                    bitmap = CardIOActivity.getCapturedCardImage(imageReturnedIntent);
                    var10000 = check;
                    if (var10000 != null) {
                        if (var10000 == 0) {
                            var19 = this.img_infoImage;
                            if (var19 == null) {
                                Intrinsics.throwUninitializedPropertyAccessException("img_infoImage");
                            }

                            var19.setImageBitmap(this.bitmap);
                            break label146;
                        }
                    }

                   *//* var19 = this.img_doucument_data;
                    if (var19 == null) {
                        Intrinsics.throwUninitializedPropertyAccessException("img_doucument_data");
                    }

                    var19.setImageBitmap(this.bitmap);*//*
                }

                Glide.with((context)).asBitmap().load(this.bitmap).apply(new RequestOptions()).into((Target)(new SimpleTarget() {
                    public void onResourceReady(@NotNull Bitmap resource, @Nullable Transition transition) {
                        Intrinsics.checkParameterIsNotNull(resource, "resource");
                        String timeStamp = (new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss", Locale.US)).format(new Date());
                        AddDocumentDailog var10000 = AddDocumentDailog.this;
                        AddDocumentDailog var10001 = AddDocumentDailog.this;
                        Intrinsics.checkExpressionValueIsNotNull(timeStamp, "timeStamp");
                        var10000.setImageCompanyFile(var10001.persistImage(resource, timeStamp));
                        AddDocumentDailog.this.setImageSelected(true);
                    }

                    // $FF: synthetic method
                    // $FF: bridge method
                    public void onResourceReady(Object var1, Transition var2) {
                        this.onResourceReady((Bitmap)var1, var2);
                    }
                }));
            } catch (Exception var13) {
            }
        }

        Uri selectedImageUri;
        if (requestCode == 1) {
            if (resultCode == -1) {
                if (imageReturnedIntent == null) {
                    Intrinsics.throwNpe();
                }

                if (imageReturnedIntent.getData() != null) {
                    String filePath;
                    label148: {
                        selectedImageUri = (Uri)null;
                        filePath = (String)null;
                        selectedImageUri = imageReturnedIntent.getData();
                        Bitmap card = CardIOActivity.getCapturedCardImage(imageReturnedIntent);
                        Uri selectedImage = imageReturnedIntent.getData();
                        this.uri = imageReturnedIntent.getData();
                        var10000 = this.check;
                        if (var10000 != null) {
                            if (var10000 == 0) {
                                var19 = this.img_infoImage;
                                if (var19 == null) {
                                    Intrinsics.throwUninitializedPropertyAccessException("img_infoImage");
                                }

                                var19.setImageURI(selectedImage);
                                break label148;
                            }
                        }

                       *//* var19 = this.img_doucument_data;
                        if (var19 == null) {
                            Intrinsics.throwUninitializedPropertyAccessException("img_doucument_data");
                        }

                        var19.setImageURI(selectedImage);*//*
                    }

                    Uri var10001 = this.uri;
                    if (var10001 == null) {
                        Intrinsics.throwNpe();
                    }

                    this.decodeFile(var10001.getPath());
                    if (selectedImageUri != null) {
                        try {
                            String filemanagerstring = selectedImageUri.getPath();
                            String selectedImagePath = this.getPath(selectedImageUri);
                            if (selectedImagePath != null) {
                                filePath = selectedImagePath;
                            } else if (filemanagerstring != null) {
                                filePath = filemanagerstring;
                            }

                            if (filePath != null) {
                                this.decodeFile(filePath);
                            } else {
                                this.bitmap = (Bitmap)null;
                            }
                        } catch (Exception var12) {
                            Log.e(var12.getClass().getName(), var12.getMessage(), (Throwable)var12);
                        }
                    }
                }
            }
        } else if ((requestCode == 2 || requestCode == this.CAMERA_CAPTURE_IMAGE_REQUEST_CODE) && resultCode == -1) {
            label150: {
                this.isImageSelected = true;
                selectedImageUri = imageReturnedIntent != null ? imageReturnedIntent.getData() : null;
                this.uri = imageReturnedIntent != null ? imageReturnedIntent.getData() : null;
                var10000 = this.check;
                if (var10000 != null) {
                    if (var10000 == 0) {
                        var19 = this.img_infoImage;
                        if (var19 == null) {
                            Intrinsics.throwUninitializedPropertyAccessException("img_infoImage");
                        }

                        var19.setImageURI(selectedImageUri);
                        break label150;
                    }
                }

               *//* var19 = this.img_doucument_data;
                if (var19 == null) {
                    Intrinsics.throwUninitializedPropertyAccessException("img_doucument_data");
                }

                var19.setImageURI(selectedImageUri);*//*
            }

            File f = new File(String.valueOf(this.myDir));
            File[] var20 = f.listFiles();
            if (var20 == null) {
                Intrinsics.throwNpe();
            }

            File[] var17 = var20;
            int var18 = var17.length;

            for(int var16 = 0; var16 < var18; ++var16) {
                File temp = var17[var16];
                Intrinsics.checkExpressionValueIsNotNull(temp, "temp");
                if (Intrinsics.areEqual(temp.getName(), this.timeStamp + ".jpg")) {
                    f = temp;
                    break;
                }
            }

            try {
                this.decodeFile(f.getAbsolutePath());
            } catch (Exception var11) {
                var11.printStackTrace();
            }
        }

    }
    public final String getPath(@NotNull Uri uri) {
        Intrinsics.checkParameterIsNotNull(uri, "uri");
        String[] projection = new String[]{"_data"};
        Activity var10000 = (Activity) context;
        if (var10000 == null) {
            Intrinsics.throwNpe();
        }

        Cursor cursor = var10000.managedQuery(uri, projection, (String)null, (String[])null, (String)null);
        if (cursor != null) {
            int column_index = cursor.getColumnIndexOrThrow("_data");
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } else {
            return null;
        }
    }
    public final void decodeFile(@Nullable String filePath) {
        BitmapFactory.Options o = new BitmapFactory.Options();
        o.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(filePath, o);
        int REQUIRED_SIZE = 512;
        int width_tmp = o.outWidth;
        int height_tmp = o.outHeight;

        int scale;
        for(scale = 1; width_tmp >= REQUIRED_SIZE || height_tmp >= REQUIRED_SIZE; scale *= 2) {
            width_tmp /= 2;
            height_tmp /= 2;
        }

        label35: {
            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize = scale;
            this.bitmap = BitmapFactory.decodeFile(filePath, o2);
            Integer var10000 = this.check;
            ImageView var8;
            if (var10000 != null) {
                if (var10000 == 0) {
                    var8 = this.img_infoImage;
                    if (var8 == null) {
                        Intrinsics.throwUninitializedPropertyAccessException("img_infoImage");
                    }

                    var8.setImageBitmap(this.bitmap);
                    break label35;
                }
            }

           *//* var8 = this.img_doucument_data;
            if (var8 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("img_doucument_data");
            }

            var8.setImageBitmap(this.bitmap);*//*
        }

        Glide.with((context)).asBitmap().load(this.bitmap).apply(new RequestOptions()).into((Target)(new SimpleTarget() {
            public void onResourceReady(@NotNull Bitmap resource, @Nullable Transition transition) {
                Intrinsics.checkParameterIsNotNull(resource, "resource");
                String timeStamp = (new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss", Locale.US)).format(new Date());
                AddDocumentDailog var10000 = AddDocumentDailog.this;
                AddDocumentDailog var10001 = AddDocumentDailog.this;
                Intrinsics.checkExpressionValueIsNotNull(timeStamp, "timeStamp");
                var10000.setImageCompanyFile(var10001.persistImage(resource, timeStamp));
                AddDocumentDailog.this.setImageSelected(true);
            }

            // $FF: synthetic method
            // $FF: bridge method
            public void onResourceReady(Object var1, Transition var2) {
                this.onResourceReady((Bitmap)var1, var2);
            }
        }));
    }
    public final void setImageCompanyFile(@Nullable File var1) {
        this.imageCompanyFile = var1;
    }

    public final void setImageSelected(boolean var1) {
        this.isImageSelected = var1;
    }
    public final File persistImage(@NotNull Bitmap bitmap, @NotNull String name) {
        Intrinsics.checkParameterIsNotNull(bitmap, "bitmap");
        Intrinsics.checkParameterIsNotNull(name, "name");
        File var10000 = this.context.getFilesDir();
        Intrinsics.checkExpressionValueIsNotNull(var10000, "mcontext.filesDir");
        File filesDir = var10000;
        File imageFile = new File(filesDir, name + ".jpeg");
        OutputStream os = null;

        try {
            os = (OutputStream)(new FileOutputStream(imageFile));
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, os);
            os.flush();
            os.close();
        } catch (Exception var7) {
            Log.e(this.getClass().getSimpleName(), "Error writing bitmap", (Throwable)var7);
        }

        return imageFile;
    }*/
}
