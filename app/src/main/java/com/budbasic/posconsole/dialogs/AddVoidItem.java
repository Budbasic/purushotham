package com.budbasic.posconsole.dialogs;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.budbasic.customer.global.Preferences;
import com.budbasic.global.CUC;
import com.budbasic.posconsole.R;
import com.budbasic.posconsole.adapter.ExpenseCashDrawerAdapter;
import com.budbasic.posconsole.reception.CashDrawerFragment;
import com.budbasic.posconsole.reception.EmployeeInvoiceDetails;
import com.budbasic.posconsole.webservice.RequetsInterface;
import com.google.gson.Gson;
import com.kotlindemo.model.GetDenominations;
import com.kotlindemo.model.GetDenominationsdata;
import com.kotlindemo.model.GetEmpExpenses;
import com.kotlindemo.model.GetEmployeeAppSetting;
import com.kotlindemo.model.GetEmployeeExpanses;
import com.kotlindemo.model.GetEmployeeLogin;
import com.kotlindemo.model.GetInvoiceDetail;
import com.kotlindemo.model.GetInvoiceOrders;
import com.kotlindemo.model.GetTillClose;
import com.kotlindemo.model.GetsalesTerminalsdata;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

public class AddVoidItem {

   private TextView tv_customer_name,tv_order_type;
   private EditText tv_cancel_reson;
   CardView cv_cancel,cv_submit;

    public void showDialog(final Context context, GetInvoiceOrders invoiceOrders) {

        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.voiddialog);
        tv_customer_name=dialog.findViewById(R.id.tv_customer_name);
        tv_order_type=dialog.findViewById(R.id.tv_order_type);
        tv_cancel_reson=dialog.findViewById(R.id.tv_cancel_reson);
        cv_cancel=dialog.findViewById(R.id.cv_cancel);
        cv_submit=dialog.findViewById(R.id.cv_submit);

        tv_cancel_reson.setImeOptions(EditorInfo.IME_FLAG_NO_EXTRACT_UI | EditorInfo.IME_ACTION_DONE);
        tv_customer_name.setText(invoiceOrders.getPatient_fname()+" "+invoiceOrders.getPatient_lname());
        if (invoiceOrders.getInvoice_serve_type().equalsIgnoreCase("2")){
            tv_order_type.setText("Delivery");
        }else {
            tv_order_type.setText("Pickup");
        }


        cv_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });  cv_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String reason=tv_cancel_reson.getText().toString().trim();
                if (reason.isEmpty()){
                    Toast.makeText(context, "enter Reason", Toast.LENGTH_SHORT).show();
                }else {
                    dialog.dismiss();
                    ((EmployeeInvoiceDetails)context).sendVoidItem(reason);
                }

            }
        });

        dialog.show();
        dialog.setCancelable(true);

    }


    public void showDialog(final Context context, GetInvoiceOrders invoiceOrders, final GetInvoiceDetail getInvoiceDetail,String user_info_page) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.voiddialog);
        tv_customer_name=dialog.findViewById(R.id.tv_customer_name);
        tv_order_type=dialog.findViewById(R.id.tv_order_type);
        tv_cancel_reson=dialog.findViewById(R.id.tv_cancel_reson);
        cv_cancel=dialog.findViewById(R.id.cv_cancel);
        cv_submit=dialog.findViewById(R.id.cv_submit);

        tv_cancel_reson.setImeOptions(EditorInfo.IME_FLAG_NO_EXTRACT_UI | EditorInfo.IME_ACTION_DONE);

        tv_customer_name.setText(user_info_page);
        if (invoiceOrders.getInvoice_serve_type().equalsIgnoreCase("2")){
            tv_order_type.setText("Delivery");
        }else {
            tv_order_type.setText("Pickup");
        }


        cv_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });  cv_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String reason=tv_cancel_reson.getText().toString().trim();
                if (reason.isEmpty()){
                    Toast.makeText(context, "enter Reason", Toast.LENGTH_SHORT).show();
                }else {
                    dialog.dismiss();
                    ((EmployeeInvoiceDetails)context).sendSinglevoid(reason,getInvoiceDetail);
                }

            }
        });

        dialog.show();
        dialog.setCancelable(true);
    }
}
