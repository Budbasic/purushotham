package com.budbasic.posconsole.global

import android.annotation.TargetApi
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.support.annotation.StringRes
import android.support.design.widget.Snackbar
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.TextView
import android.widget.Toast
import com.budbasic.customer.global.Preferences
import com.budbasic.posconsole.LoginActivity
import com.budbasic.posconsole.R
import com.google.gson.Gson
import com.kotlindemo.model.GetEmployeeLogin


/**
 * Created by janisharali on 27/01/17.
 */

abstract class BaseActivity : AppCompatActivity(), MvpView, BaseFragment.Callback {

    lateinit var logindata: GetEmployeeLogin

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        logindata = Gson().fromJson(Preferences.getPreference(applicationContext, "logindata"), GetEmployeeLogin::class.java)
    }

    override fun attachBaseContext(newBase: Context) {
        super.attachBaseContext(newBase)
    }

    override fun onStart() {
        super.onStart()
        startService(Intent(this, SessionsTimeOut::class.java))
        /*if (!isFinishing) {
            logindata = Gson().fromJson(Preferences.getPreference(applicationContext, "logindata"), GetEmployeeLogin::class.java)
            val lockManager = LockManager.getInstance()
            lockManager.appLock.disable()
        }*/
    }

    override fun onUserInteraction() {
        super.onUserInteraction()
        Log.i("SessionsTAG", "onUserInteraction(), ")
        //SessionsTimeOut.restart()
//        if (logindata != null) {
//            SessionsTimeOut.timeout_limit = "${logindata!!.timeout_limit}".toLong()
//            SessionsTimeOut.final_out = SessionsTimeOut.timeout_limit * 60 * 1000
//
//            if (SessionsTimeOut.timer!= null){
//                SessionsTimeOut.timer!!.cancel()
//                SessionsTimeOut.timer!!.start()
//            }
//        } else {
//            SessionsTimeOut.timeout_limit = "15".toLong()
//            SessionsTimeOut.final_out = SessionsTimeOut.timeout_limit * 60 * 1000
//            if (SessionsTimeOut.timer!= null){
//                SessionsTimeOut.timer!!.cancel()
//                SessionsTimeOut.timer!!.start()
//            }
//        }
    }

    override fun onStop() {
        super.onStop()
        try {
            val startHoverIntent = Intent(applicationContext, SingleSectionNotificationHoverMenuService::class.java)
            applicationContext.stopService(startHoverIntent)
            applicationContext.stopService(Intent(this, SessionsTimeOut::class.java))
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    @TargetApi(Build.VERSION_CODES.M)
    fun requestPermissionsSafely(permissions: Array<String>, requestCode: Int) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(permissions, requestCode)
        }
    }

    @TargetApi(Build.VERSION_CODES.M)
    fun hasPermission(permission: String): Boolean {
        return Build.VERSION.SDK_INT < Build.VERSION_CODES.M || checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED
    }

    override fun showLoading() {
        hideLoading()
    }

    override fun hideLoading() {}

    private fun showSnackBar(message: String) {
        val snackbar = Snackbar.make(findViewById(android.R.id.content),
                message, Snackbar.LENGTH_SHORT)
        val sbView = snackbar.view
        val textView = sbView
                .findViewById<View>(android.support.design.R.id.snackbar_text) as TextView
        textView.setTextColor(ContextCompat.getColor(this, R.color.white))
        snackbar.show()
    }

    override fun onError(message: String?) {
        if (message != null) {
            showSnackBar(message)
        } else {
            showSnackBar("some_error")
        }
    }

    override fun onError(@StringRes resId: Int) {
        onError(getString(resId))
    }

    override fun showMessage(message: String?) {
        if (message != null) {
            Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
        } else {
            Toast.makeText(this, "some_error", Toast.LENGTH_SHORT).show()
        }
    }

    override fun showMessage(@StringRes resId: Int) {
        showMessage(getString(resId))
    }

    override fun onFragmentAttached() {

    }

    override fun onFragmentDetached(tag: String) {

    }

    override fun hideKeyboard() {
        val view = this.currentFocus
        if (view != null) {
            val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(view.windowToken, 0)
        }
    }

    override fun openActivityOnTokenExpire() {
        LoginActivity.logout(this)
        finish()
    }

    override fun onDestroy() {

        super.onDestroy()
    }


    protected abstract fun setUp()
    abstract fun showImage()

}
