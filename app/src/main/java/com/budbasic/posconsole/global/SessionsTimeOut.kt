package com.budbasic.posconsole.global

import android.app.Service
import android.content.Intent
import android.os.CountDownTimer
import android.os.IBinder
import android.util.Log
import com.budbasic.customer.global.Preferences
import com.budbasic.global.CUC
import com.budbasic.posconsole.R
import com.budbasic.posconsole.model.APIClass
import com.budbasic.posconsole.webservice.RequetsInterface
import com.github.omadahealth.lollipin.lib.managers.AppLockActivity
import com.github.omadahealth.lollipin.lib.managers.LockManager
import com.google.gson.Gson
import com.kotlindemo.model.GetEmployeeAppSetting
import com.kotlindemo.model.GetEmployeeLogin
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import android.os.Build
import com.budbasic.posconsole.dialogs.SessionTimeOutDialog


class SessionsTimeOut : Service(), APIClass.onCallListner {
    override fun onFailed(requestCode: Int) {
    }

    override fun onSuccess(requestCode: Int) {
        callEmployeeDetails()
    }


    var logindata: GetEmployeeLogin? = null

    var appSettingData: GetEmployeeAppSetting? = null
    lateinit var apiClass: APIClass
    var update = false


    companion object {
        var reset = false
        var timeout_limit = "15".toLong()
        var final_out = timeout_limit * 60 * 1000
        var timer: CountDownTimer? = null
        fun restart() {
            reset = true
        }
    }

    override fun onCreate() {
        super.onCreate()
        createTimer()
    }

    fun createTimer() {
        reset = false
        apiClass = APIClass(applicationContext, this)
        logindata = Gson().fromJson(Preferences.getPreference(applicationContext, "logindata"), GetEmployeeLogin::class.java)
        if (logindata != null) {
            timeout_limit = "${logindata!!.timeout_limit}".toLong()
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            applicationContext.startForegroundService(Intent(applicationContext, SingleSectionNotificationHoverMenuService::class.java))
        } else {
            applicationContext.startService(Intent(applicationContext, SingleSectionNotificationHoverMenuService::class.java))
        }
        appSettingData = Gson().fromJson(Preferences.getPreference(applicationContext, "AppSetting"), GetEmployeeAppSetting::class.java)
        update = true
        final_out = timeout_limit * 60 * 1000
        Log.i("SessionsTAG", "final_out $final_out")
        timer = object : CountDownTimer(final_out, 10000) {
            override fun onTick(millisUntilFinished: Long) {
                //Some code
                if (reset) {
                    timer!!.cancel()
                    createTimer()
                } else {
                    if (update) {
                        update = false
                        callEmployeeDetails()
                    }
                }
                Log.i("SessionsTAG", "Service Started $millisUntilFinished")
            }

            override fun onFinish() {
                Log.i("SessionsTAG", "Call Logout by Service")
                // Code for Logout
                if (logindata!!.user_role == "2") {
                    if (!reset) {
                        logindata = Gson().fromJson(Preferences.getPreference(applicationContext, "logindata"), GetEmployeeLogin::class.java)
                        if (logindata != null && "${logindata!!.timeout_lock}" == "1") {
                            try {
                                val startHoverIntent = Intent(applicationContext, SingleSectionNotificationHoverMenuService::class.java)
                                applicationContext.stopService(startHoverIntent)
                            } catch (e: Exception) {
                                e.printStackTrace()
                            }

                            val lockManager = LockManager.getInstance()
                            Log.i("SessionsTAG", "Call Logout by Service")
                            if (lockManager.isAppLockEnabled) {
                                lockManager.enableAppLock(applicationContext, AppLockActivity::class.java)
                                lockManager.appLock.setPasscode("${logindata!!.user_pin}")
                                val intent = Intent(applicationContext, SessionTimeOutDialog::class.java)
                                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                                applicationContext.startActivity(intent)
                            }
                        }
                    }
                } else {
                    createTimer()
                }

            }
        }
        timer!!.start()

    }

    override fun onDestroy() {
        super.onDestroy()
        try {
            timer!!.cancel()
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    override fun onBind(intent: Intent): IBinder? {
        return null
    }

    fun callEmployeeDetails() {
        val apiService = RequetsInterface.create()
        val parameterMap = HashMap<String, String>()
        parameterMap["api_key"] = "${Preferences.getPreference(applicationContext, "API_KEY")}"
        if (logindata != null)
            parameterMap["employee_id"] = "${logindata!!.user_id}"
        if (appSettingData != null) {
            parameterMap["terminal_id"] = "${appSettingData!!.terminal_id}"
        }

        Log.i("Result", "parameterMap : $parameterMap")
        val mDisposable = apiService.callEmployeeDetails(parameterMap)
        CompositeDisposable().add(mDisposable
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe({ result ->
                    if (result != null) {
                        update = true
                        Log.i("Result", "" + result.toString())
                        if (result.status == "0") {
                            Preferences.setPreference(applicationContext, "logindata", Gson().toJson(result))
                            CUC.displayToast(applicationContext, result.show_status, result.message)
                        } else if (result.status == "10") {
                            apiClass.callApiKey(1)
                        } else if (result.status == "11") {
                            CUC.Logout(applicationContext)
                        } else {
                            CUC.displayToast(applicationContext, result.show_status, result.message)
                        }
                    } else {
                        update = true
                      //  CUC.displayToast(applicationContext, "0", getString(R.string.connection_to_database_failed))
                    }
                }, { error ->
                    update = true
                  //  CUC.displayToast(applicationContext, "0", getString(R.string.connection_to_database_failed))
                    error.printStackTrace()
                })
        )
    }
}