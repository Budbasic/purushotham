package com.budbasic.posconsole.global;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.View;

import com.budbasic.posconsole.reception.StoreManagerActivity;
import com.kotlindemo.model.GetNotes;

public abstract class BaseFragment extends Fragment {

    private StoreManagerActivity mActivity;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setUp(view);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof StoreManagerActivity) {
            StoreManagerActivity activity = (StoreManagerActivity) context;
            this.mActivity = activity;
            //activity.onFragmentAttached();
        }
    }


    @Override
    public void onDetach() {
        mActivity = null;
        super.onDetach();
    }


    public StoreManagerActivity getBaseActivity() {
        return mActivity;
    }

    protected abstract void setUp(View view);

    @Override
    public void onDestroy() {
        super.onDestroy();
    }



    public interface Callback {
        void onFragmentAttached();

        void onFragmentDetached(String tag);
    }
}