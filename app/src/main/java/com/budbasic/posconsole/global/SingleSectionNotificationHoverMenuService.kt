/*
 * Copyright 2016 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.budbasic.posconsole.global

import android.app.Notification
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.support.v4.app.NotificationCompat
import android.util.Log
import android.view.Gravity
import android.view.View
import android.widget.EditText
import android.widget.LinearLayout
import com.budbasic.customer.global.Preferences
import com.budbasic.global.CUC
import com.budbasic.posconsole.R
import com.budbasic.posconsole.webservice.RequetsInterface
import com.google.gson.Gson
import com.kotlindemo.model.GetEmployeeLogin
import io.mattcarroll.hover.Content
import io.mattcarroll.hover.HoverMenu
import io.mattcarroll.hover.HoverView
import io.mattcarroll.hover.window.HoverMenuService
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

/**
 * Extend [HoverMenuService] to get a Hover menu that displays the tabs and content
 * in your custom [HoverMenu].
 *
 *
 * This menu presents the simplest possible Hover Menu, a menu with a single Section.  This menu
 * also runs as a foreground Service by providing a notification.
 */
class SingleSectionNotificationHoverMenuService : HoverMenuService() {

    override fun getForegroundNotificationId(): Int {
        return 1000
    }

    override fun getForegroundNotification(): Notification? {
        return NotificationCompat.Builder(this, "My_Hover")
                .setSmallIcon(R.drawable.tab_background)
                .setContentTitle("Help")
                .setContentText("Help Service.")
                .build()
    }

    override fun onHoverMenuLaunched(intent: Intent, hoverView: HoverView) {
        logindata = Gson().fromJson(Preferences.getPreference(applicationContext, "logindata"), GetEmployeeLogin::class.java)
        mHoverView = hoverView
        mHoverView!!.setMenu(createHoverMenu())
        mHoverView!!.collapse()
    }

    private fun createHoverMenu(): HoverMenu {
        return SingleSectionHoverMenu(applicationContext)
    }

    private class SingleSectionHoverMenu(private val mContext: Context) : HoverMenu() {
        private val mSection: HoverMenu.Section

        var help_message = ""
        var edt_help_message: EditText? = null

        init {
            mSection = HoverMenu.Section(
                    HoverMenu.SectionId("1"),
                    createTabView(),
                    createScreen()
            )
            val params = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT)
            params.gravity = Gravity.RIGHT
            mSection.content.view.layoutParams = params
            mSection.tabView.rootView.background = null
            mSection.tabView.rootView.setBackgroundColor(mContext.resources.getColor(android.R.color.transparent))
            mSection.content.view.rootView.background = null
            mSection.content.view.rootView.setBackgroundColor(mContext.resources.getColor(android.R.color.transparent))

            val cv_submit = mSection.content.view.findViewById<LinearLayout>(R.id.cv_submit)
            val cv_cancel = mSection.content.view.findViewById<LinearLayout>(R.id.cv_cancel)
            edt_help_message = mSection.content.view.findViewById(R.id.edt_help_message)

            cv_submit.setOnClickListener {
                if (edt_help_message!!.text.toString() == "") {
                    CUC.displayToast(mContext!!, "0", "Enter Query Message")
                } else {
                    help_message = edt_help_message!!.text.toString()
                    if (mHoverView != null)
                        mHoverView!!.collapse()
                    callEmployeeHelpdesk()
                }

            }
            cv_cancel.setOnClickListener {
                help_message = ""
                edt_help_message!!.setText("")
                if (mHoverView != null)
                    mHoverView!!.collapse()
            }


        }

        private fun createTabView(): View {
            //            ImageView imageView = new ImageView(mContext);
            //            imageView.setImageResource(R.drawable.ic_orange_circle);
            //            imageView.setScaleType(ImageView.ScaleType.CENTER_INSIDE);

            val resources = mContext.resources

            val view = DemoTabView(
                    mContext,
                    resources.getDrawable(R.drawable.tab_background),
                    resources.getDrawable(R.drawable.ic_comment)
            )
            view.setTabBackgroundColor(Color.parseColor("#2ecbc6"))
            view.setTabForegroundColor(null)
            return view
        }

        private fun createScreen(): Content {
            return HoverMenuScreen(mContext, "")
        }

        override fun getId(): String {
            return "singlesectionmenu_foreground"
        }

        override fun getSectionCount(): Int {
            return 1
        }

        override fun getSection(index: Int): HoverMenu.Section? {
            return if (0 == index) {
                mSection
            } else {
                null
            }
        }

        override fun getSection(sectionId: HoverMenu.SectionId): HoverMenu.Section? {
            return if (sectionId == mSection.id) {
                mSection
            } else {
                null
            }
        }

        override fun getSections(): List<HoverMenu.Section> {
            return listOf(mSection)
        }

        fun callEmployeeHelpdesk() {
            //mDialog = CUC.createDialog(mContext!!)
            val apiService = RequetsInterface.create()
            val parameterMap = HashMap<String, String>()
            parameterMap["api_key"] = "${Preferences.getPreference(mContext!!, "API_KEY")}"
            parameterMap["employee_id"] = "${logindata!!.user_id}"
            parameterMap["query_msg"] = help_message
            Log.i("Result", "parameterMap : $parameterMap")
            CompositeDisposable().add(apiService.callEmployeeHelpdesk(parameterMap)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe({ result ->
                        //                        if (mDialog != null && mDialog!!.isShowing)
//                            mDialog!!.dismiss()
                        if (result != null) {
                            Log.i("Result", "" + result.toString())
                            if (result.status == "0") {
                                help_message = ""
                                edt_help_message!!.setText("")
                                CUC.displayToast(mContext!!, result.show_status, result.message)

                            } else if (result.status == "10") {
                                //apiClass!!.callApiKey(2)
                            } else {
                                CUC.displayToast(mContext!!, result.show_status, result.message)
                            }
                        } else {
                            // CUC.displayToast(mContext!!, "0", mContext.getString(R.string.connection_to_database_failed))
                        }
                    }, { error ->
                        //                        if (mDialog != null && mDialog!!.isShowing)
//                            mDialog!!.dismiss()
                        //   CUC.displayToast(mContext!!, "0", mContext.getString(R.string.connection_to_database_failed))

                        error.printStackTrace()
                    })
            )
        }
    }

    companion object {

        private val TAG = "SingleSectionNotificationHoverMenuService"

        var mHoverView: HoverView? = null
        var logindata: GetEmployeeLogin? = null
    }



}
