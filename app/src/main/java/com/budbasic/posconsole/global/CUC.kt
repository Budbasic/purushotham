package com.budbasic.global

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.ContextWrapper
import android.content.Intent
import android.database.Cursor
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.drawable.Drawable
import android.media.MediaScannerConnection
import android.net.ConnectivityManager
import android.net.Uri
import android.os.Environment
import android.os.Handler
import android.os.SystemClock
import android.provider.MediaStore
import android.support.v4.content.ContextCompat
import android.support.v7.app.AlertDialog
import android.text.*
import android.text.method.LinkMovementMethod
import android.text.style.ForegroundColorSpan
import android.util.Base64
import android.util.DisplayMetrics
import android.util.Log
import android.view.Gravity
import android.view.View
import android.view.ViewTreeObserver
import android.view.Window
import android.view.inputmethod.InputMethodManager
import android.widget.ImageView
import android.widget.TextView
import android.widget.TextView.BufferType
import android.widget.Toast
import com.google.android.gms.maps.model.LatLng
import com.budbasic.posconsole.R
import com.budbasic.customer.global.ExifUtils
import com.budbasic.customer.global.MySpannable
import com.budbasic.customer.global.Preferences
import com.budbasic.posconsole.LoginActivity
import com.budbasic.posconsole.global.FilterData
import com.google.zxing.BarcodeFormat
import com.google.zxing.EncodeHintType
import com.google.zxing.MultiFormatWriter
import com.google.zxing.WriterException
import com.google.zxing.common.BitMatrix
import com.mikepenz.iconics.typeface.IIcon
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.text.*
import java.util.*
import java.util.regex.Pattern
import kotlin.collections.ArrayList

/**
 * Created by Dev009 on 5/29/2017 29.
 */

class CUC {


    fun getFromSdcard() {
        val f = ArrayList<String>()// list of file paths
        val listFile: Array<File>
        val file = File(android.os.Environment.getExternalStorageDirectory(), IMAGE_DIRECTORY)
        if (file.isDirectory) {
            listFile = file.listFiles()
            for (i in listFile.indices) {
                f.add(listFile[i].absolutePath)
            }
        }
    }

    companion object {
        private var mLastClickTime: Long = 0
        private val timeBetweenClick = 300 //in ns
        private val EXTENSION_SEPARATOR = '.'
        private val DIRECTORY_SEPARATOR = '/'
        private val IMAGE_DIRECTORY = "/itacho_cashier/Images"
        //var mfilterdata: ArrayList<FilterData?> = ArrayList()


        /*fun setFilterdata(mfilterdata: FilterData, position: Int) {
            this.mfilterdata.add(position, mfilterdata!!)
        }*/

        fun hideSoftKeyboard(activity: Activity) {
            val inputMethodManager = activity.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
            inputMethodManager!!.hideSoftInputFromWindow(
                    activity.currentFocus!!.windowToken, 0)
        }

        fun IconicsBitmap(context: Context, icon: IIcon, color: Int): Bitmap {
            return com.mikepenz.iconics.IconicsDrawable(context, icon).sizeDpX(context.resources.getDimensionPixelSize(R.dimen._48sdp)).sizeDpY(context.resources.getDimensionPixelSize(R.dimen._48sdp)).color(color).toBitmap()
        }

        fun IconicsDrawable(context: Context, icon: IIcon, color: Int): Drawable {
            return com.mikepenz.iconics.IconicsDrawable(context, icon).sizeDpX(context.resources.getDimensionPixelSize(R.dimen._48sdp)).sizeDpY(context.resources.getDimensionPixelSize(R.dimen._48sdp)).color(color)
        }

        fun isClickEnable(): Boolean {
            if (SystemClock.elapsedRealtime() - mLastClickTime < timeBetweenClick)
                return false
            else {
                mLastClickTime = SystemClock.elapsedRealtime()
                return true
            }
        }

        fun xxx(textlbl: String, textvalue: String, textlblColor: Int, textvalueColor: Int): SpannableStringBuilder {
            val builder = SpannableStringBuilder()
            val str1 = SpannableString(textlbl)
            //str1.setSpan( RelativeSizeSpan(2f), 0,5, 0);
            str1.setSpan(ForegroundColorSpan(textlblColor), 0, str1.length, 0)
            builder.append(str1)
            val str2 = SpannableString(textvalue)
            str2.setSpan(ForegroundColorSpan(textvalueColor), 0, str2.length, 0)
            builder.append(str2)
            return builder
        }
        fun isContainOneAlphabet(text: String): Boolean {

            return (Pattern.compile("(?=.*[a-zA-Z].{2}).{0,100}")).matcher(text).matches()
            /*val PASSWORD_PATTERN: String = "((?=.*[A-Za-z]))";
             val pattern: Pattern = Pattern.compile(PASSWORD_PATTERN)
             val matcher = pattern.matcher(text);
             return matcher.matches()*/

        }
        fun getWidhtHeight(activity: Activity) {

        }

        /**
         * Returns the unique identifier for the device
         *
         * @return unique identifier for the device
         */

        fun createDialog(activity: Activity): Dialog {
            val dialog = Dialog(activity, R.style.TransparentProgressDialog)
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
            dialog.setContentView(R.layout.customprogressbar)


            val iv_bg_color = dialog.findViewById(R.id.iv_bg_color) as ImageView
            iv_bg_color.tag = 0
            iv_bg_color.postDelayed({ }, 100)
            val mTimer1: Timer?
            val mTt1: TimerTask
            val mTimerHandler = Handler()

//            mTimer1 = Timer()
//            mTt1 = object : TimerTask() {
//                override fun run() {
//                    mTimerHandler.post {
//                        if (iv_bg_color.tag as Int == 0) {
//                            iv_bg_color.tag = 1
//                            iv_bg_color.setBackgroundResource(R.drawable.progress1_radius)
//                        } else {
//                            iv_bg_color.tag = 0
//                            iv_bg_color.setBackgroundResource(R.drawable.progress_radius)
//                        }
//                    }
//                }
//            }
//
//            mTimer1.schedule(mTt1, 1, 500)
//            dialog.setOnDismissListener {
//                if (mTimer1 != null) {
//                    mTimer1.cancel()
//                    mTimer1.purge()
//                }
//            }
            if (!activity.isFinishing) {
                dialog.show()
            }
            dialog.window!!.setGravity(Gravity.CENTER)
            dialog.setCancelable(false)
            dialog.setCanceledOnTouchOutside(false)
            return dialog
        }
        fun getCapsSentences(tagName: String): String {
            val splits = tagName.toLowerCase().split(" ".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
            val sb = StringBuilder()
            for (i in splits.indices) {
                val eachWord = splits[i]
                if (i > 0 && eachWord.length > 0) {
                    sb.append(" ")
                }
                val cap = eachWord.substring(0, 1).toUpperCase() + eachWord.substring(1)
                sb.append(cap)
            }
            return sb.toString()
        }
        fun isNetworkAvailablewithPopup(mContext: Context): Boolean {
            /* getting systems Service connectivity manager */
            val cm = mContext.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val networkInfo = cm.activeNetworkInfo
            if (networkInfo == null) {
                val builder = AlertDialog.Builder(mContext)
                builder.setMessage(mContext.resources.getString(R.string.nointernetconnect)).setTitle(mContext.resources.getString(R.string.nointernettitle)).setCancelable(false).setPositiveButton(mContext.resources.getString(R.string.ok)) { dialog, id ->
                    dialog.cancel()
                }
                val alert = builder.create()
                alert.show()
                return false
            } else {
                return true
            }
        }

        fun formatDecimal(value: String): String {
            return NumberFormat.getNumberInstance(Locale.getDefault()).format(java.lang.Double.valueOf(value))
        }

        fun displayToast(context: Context, show_message: String, message: String) {
            if (show_message.equals("0", ignoreCase = true))
                showToast(context, message, 8000)
        }

        fun showToast(cnt: Context, msg: String, duration: Int) {
            val toast = Toast.makeText(cnt, msg, Toast.LENGTH_LONG)
            toast.setGravity(Gravity.TOP or Gravity.CENTER, 0, cnt.resources.getDimensionPixelSize(R.dimen._50sdp))


            val toastView = toast.view
            toastView.setBackgroundResource(R.drawable.toast_border)


            toastView.setPadding(cnt.resources.getDimension(R.dimen._9sdp).toInt(), cnt.resources.getDimension(R.dimen._6sdp).toInt(),
                    cnt.resources.getDimension(R.dimen._9sdp).toInt(), cnt.resources.getDimension(R.dimen._6sdp).toInt())

            val v = toast.view.findViewById<View>(android.R.id.message) as TextView
            v.setTextColor(ContextCompat.getColor(cnt, R.color.white))
            toast.show()

        }

        fun getStringtoNumberWithDecimal(target: String): String {
            return target.replace("[^\\d.]".toRegex(), "")
        }

        fun getStatusBarHeight(c: Context): Int {
            var result = 0
            val resourceId = c.resources.getIdentifier("status_bar_height", "dimen", "android")
            if (resourceId > 0) {
                result = c.resources.getDimensionPixelSize(resourceId)
            }
            return result
        }

        fun fromHtml(html: String): Spanned {
            val result: Spanned
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                result = Html.fromHtml(html, Html.FROM_HTML_MODE_LEGACY)
            } else {
                result = Html.fromHtml(html)
            }
            return result
        }

        fun isValidEmail(target: CharSequence): Boolean {
            return !TextUtils.isEmpty(target) && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches()
        }

        fun saveToInternalStorage(context: Context, image_name: String, bitmapImage: Bitmap): String {
            val cw = ContextWrapper(context)
            // path to /data/data/yourapp/app_data/imageDir
            Log.i("PostAnAdProductActivity", "saveToInternalStorage(), called.. image_name : $image_name")
            val directory = cw.getDir("imageDir", Context.MODE_PRIVATE)
            // Create imageDir
            val mypath = File(directory, image_name)

            var fos: FileOutputStream? = null
            try {
                fos = FileOutputStream(mypath)
                // Use the compress method on the BitMap object to write image to the OutputStream
                bitmapImage.compress(Bitmap.CompressFormat.PNG, 100, fos)
            } catch (e: Exception) {
                e.printStackTrace()
            } finally {
                try {
                    fos!!.close()
                } catch (e: IOException) {
                    e.printStackTrace()
                }

            }
            return mypath.absolutePath
        }

        fun getRealPathFromUri(context: Context, contentUri: Uri): String {
            var cursor: Cursor? = null
            try {
                val proj = arrayOf(MediaStore.Images.Media.DATA)
                cursor = context.contentResolver.query(contentUri, proj, null, null, null)
                val column_index = cursor!!.getColumnIndexOrThrow(MediaStore.Images.Media.DATA)
                cursor.moveToFirst()
                return cursor.getString(column_index)
            } finally {
                if (cursor != null) {
                    cursor.close()
                }
            }
        }

        fun saveImage(context: Context, myBitmap: Bitmap): String {
            val bytes = ByteArrayOutputStream()
            myBitmap.compress(Bitmap.CompressFormat.JPEG, 100, bytes)
            val wallpaperDirectory = File(
                    Environment.getExternalStorageDirectory().toString() + IMAGE_DIRECTORY)
            // have the object build the directory structure, if needed.
            if (!wallpaperDirectory.exists()) {
                wallpaperDirectory.mkdirs()
            }
            try {
                val f = File(wallpaperDirectory, Calendar.getInstance().timeInMillis.toString() + ".jpg")
                f.createNewFile()
                val fo = FileOutputStream(f)
                fo.write(bytes.toByteArray())
                MediaScannerConnection.scanFile(context,
                        arrayOf(f.path),
                        arrayOf("image/jpeg"), null)
                fo.close()
                Log.d("TAG", "File Saved::--->" + f.absolutePath)

                return f.absolutePath
            } catch (e1: IOException) {
                e1.printStackTrace()
            }

            return ""
        }

        fun getFile(context: Context, image_name: String): File {
            val cw = ContextWrapper(context)
            // path to /data/data/yourapp/app_data/imageDir
            val directory = cw.getDir("imageDir", Context.MODE_PRIVATE)
            // Create imageDir
            return File(directory, image_name)
        }

        fun getDelete(context: Context): Boolean {
            val cw = ContextWrapper(context)
            // path to /data/data/yourapp/app_data/imageDir
            val directory = cw.getDir("imageDir", Context.MODE_PRIVATE)
            if (directory.exists())
                directory.delete()
            // Create imageDir
            return true
        }


        fun decodeFile(filePath: String): Bitmap {

            // Decode image size
            val o = BitmapFactory.Options()
            o.inJustDecodeBounds = true
            BitmapFactory.decodeFile(filePath, o)

            // The new size we want to scale to
            val REQUIRED_SIZE = 1024

            // Find the correct scale value. It should be the power of 2.
            var width_tmp = o.outWidth
            var height_tmp = o.outHeight
            var scale = 1
            while (true) {
                if (width_tmp < REQUIRED_SIZE && height_tmp < REQUIRED_SIZE)
                    break
                width_tmp /= 2
                height_tmp /= 2
                scale *= 2
            }

            // Decode with inSampleSize
            val o2 = BitmapFactory.Options()
            o2.inSampleSize = scale
            val b1 = BitmapFactory.decodeFile(filePath, o2)
            return ExifUtils.rotateBitmap(filePath, b1)
            // image.setImageBitmap(bitmap);
        }


        fun getEncoded64ImageStringFromBitmap(bitmap: Bitmap): String {
            val stream = ByteArrayOutputStream()
            bitmap.compress(Bitmap.CompressFormat.PNG, 70, stream)
            val byteFormat = stream.toByteArray()
            // get the base 64 string

            return Base64.encodeToString(byteFormat, Base64.NO_WRAP)
        }

        fun getReadableFileSize(size: Long): String {
            if (size <= 0) {
                return "0"
            }
            val units = arrayOf("B", "KB", "MB", "GB", "TB")
            val digitGroups = (Math.log10(size.toDouble()) / Math.log10(1024.0)).toInt()
            return DecimalFormat("#,##0.#").format(size / Math.pow(1024.0, digitGroups.toDouble())) + " " + units[digitGroups]
        }

        fun getdatefromoneformattoAnother(currentformat: String, expectedformat: String, datevalue: String): String {
            val originalFormat = SimpleDateFormat(currentformat, Locale.ENGLISH)
            val targetFormat = SimpleDateFormat(expectedformat)
            var date: Date? = null
            try {
                date = originalFormat.parse(datevalue)
            } catch (e: ParseException) {
                e.printStackTrace()
            }

            return targetFormat.format(date)
        }


        fun makeTextViewResizable(tv: TextView, maxLine: Int, expandText: String, dots: String, viewMore: Boolean) {

            if (tv.tag == null) {
                tv.tag = tv.text
            }
            val vto = tv.viewTreeObserver
            vto.addOnGlobalLayoutListener(object : ViewTreeObserver.OnGlobalLayoutListener {

                override fun onGlobalLayout() {

                    val obs = tv.viewTreeObserver
                    obs.removeGlobalOnLayoutListener(this)
                    if (maxLine == 0) {
                        val lineEndIndex = tv.layout.getLineEnd(0)
                        val text = tv.text.subSequence(0, lineEndIndex - expandText.length + 1).toString() + dots + expandText
                        tv.text = text
                        tv.movementMethod = LinkMovementMethod.getInstance()
                        tv.setText(
                                addClickablePartTextViewResizable(Html.fromHtml(tv.text.toString()), tv, maxLine, expandText,
                                        viewMore), BufferType.SPANNABLE)
                    } else if (maxLine > 0 && tv.lineCount >= maxLine) {
                        val lineEndIndex = tv.layout.getLineEnd(maxLine - 1)
                        val text = tv.text.subSequence(0, lineEndIndex - expandText.length + 1).toString() + dots + expandText
                        tv.text = text
                        tv.movementMethod = LinkMovementMethod.getInstance()
                        tv.setText(addClickablePartTextViewResizable(Html.fromHtml(tv.text.toString()), tv, maxLine, expandText, viewMore), BufferType.SPANNABLE)
                    } else {
                        if (maxLine == -1) {
                            val lineEndIndex = tv.layout.getLineEnd(tv.layout.lineCount - 1)
                            val text = tv.text.subSequence(0, lineEndIndex).toString() + dots + expandText
                            tv.text = text
                            tv.movementMethod = LinkMovementMethod.getInstance()
                            tv.setText(addClickablePartTextViewResizable(Html.fromHtml(tv.text.toString()), tv, lineEndIndex, expandText, viewMore), BufferType.SPANNABLE)
                        }

                    }
                }
            })

        }

        private fun addClickablePartTextViewResizable(strSpanned: Spanned, tv: TextView,
                                                      maxLine: Int, spanableText: String, viewMore: Boolean): SpannableStringBuilder {
            val str = strSpanned.toString()
            val ssb = SpannableStringBuilder(strSpanned)

            if (str.contains(spanableText)) {


                ssb.setSpan(object : MySpannable(true) {
                    override fun onClick(widget: View) {
                        if (viewMore) {
                            tv.layoutParams = tv.layoutParams
                            tv.setText(tv.tag.toString(), BufferType.SPANNABLE)
                            tv.invalidate()
                            makeTextViewResizable(tv, -1, "VIEW LESS", " ", false)
                        } else {
                            tv.layoutParams = tv.layoutParams
                            tv.setText(tv.tag.toString() + "", BufferType.SPANNABLE)
                            tv.invalidate()
                            makeTextViewResizable(tv, 3, "VIEW MORE", "...", true)
                        }
                    }
                }, str.indexOf(spanableText), str.indexOf(spanableText) + spanableText.length, 0)

            }
            return ssb
        }

        fun isPassword(password: String): Boolean {
            //Pattern capital = Pattern.compile("[A-z]");
            val letter = Pattern.compile("[a-z]")
            val digit = Pattern.compile("[0-9]")
            //Pattern special = Pattern.compile("[!@#$%&*()_+=|<>?{}\\[\\]~-]");
            val eight = Pattern.compile(".{8,20}")

            //Matcher hasCapital = capital.matcher(password);
            val hasLetter = letter.matcher(password)
            val hasDigit = digit.matcher(password)
            //Matcher hasSpecial = special.matcher(password);
            val hasEight = eight.matcher(password)
            return hasLetter.find() && hasDigit.find() && hasEight.matches()
        }

        fun decodePoly(encoded: String): List<LatLng> {

            val poly = ArrayList<LatLng>()
            var index = 0
            val len = encoded.length
            var lat = 0
            var lng = 0

            while (index < len) {
                var b: Int
                var shift = 0
                var result = 0
                do {
                    b = encoded[index++].toInt() - 63
                    result = result or (b and 0x1f shl shift)
                    shift += 5
                } while (b >= 0x20)
                val dlat = if (result and 1 != 0) (result shr 1).inv() else result shr 1
                lat += dlat

                shift = 0
                result = 0
                do {
                    b = encoded[index++].toInt() - 63
                    result = result or (b and 0x1f shl shift)
                    shift += 5
                } while (b >= 0x20)
                val dlng = if (result and 1 != 0) (result shr 1).inv() else result shr 1
                lng += dlng

                val p = LatLng(lat.toDouble() / 1E5,
                        lng.toDouble() / 1E5)
                poly.add(p)
            }
            return poly
        }


        @Throws(WriterException::class)
        internal fun encodeAsBitmap(contents: String?, format: BarcodeFormat, img_width: Int, img_height: Int): Bitmap? {
            if (contents == null) {
                return null
            }
            var hints: MutableMap<EncodeHintType, Any>? = null
            val encoding = guessAppropriateEncoding(contents)
            if (encoding != null) {
                hints = EnumMap(EncodeHintType::class.java)
                hints[EncodeHintType.CHARACTER_SET] = encoding
            }
            val writer = MultiFormatWriter()
            val result: BitMatrix
            try {
                result = writer.encode(contents, format, img_width, img_height, hints)
            } catch (iae: IllegalArgumentException) {
                // Unsupported format
                return null
            }

            val width = result.width
            val height = result.height
            val pixels = IntArray(width * height)
            for (y in 0 until height) {
                val offset = y * width
                for (x in 0 until width) {
                    pixels[offset + x] = if (result.get(x, y)) BLACK else WHITE
                }
            }

            val bitmap = Bitmap.createBitmap(width, height,
                    Bitmap.Config.ARGB_8888)
            bitmap.setPixels(pixels, 0, width, 0, 0, width, height)
            return bitmap
        }

        /**************************************************************
         * getting from com.google.zxing.client.android.encode.QRCodeEncoder
         *
         * See the sites below
         * http://code.google.com/p/zxing/
         * http://code.google.com/p/zxing/source/browse/trunk/android/src/com/google/zxing/client/android/encode/EncodeActivity.java
         * http://code.google.com/p/zxing/source/browse/trunk/android/src/com/google/zxing/client/android/encode/QRCodeEncoder.java
         */

        private val WHITE = -0x1
        private val BLACK = -0x1000000

        private fun guessAppropriateEncoding(contents: CharSequence): String? {
            // Very crude at the moment
            for (i in 0 until contents.length) {
                if (contents[i].toInt() > 0xFF) {
                    return "UTF-8"
                }
            }
            return null
        }

        fun Logout(context: Context) {
            val Logout_intent = Intent(context, LoginActivity::class.java)
            Logout_intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            Preferences.removePreference(context, "logindata")
            context.startActivity(Logout_intent)
        }

    }


}