package com.budbasic.posconsole.global

import android.content.Context
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.Path
import android.graphics.RectF
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.View
import android.widget.LinearLayout

internal class SignatureView : View {

    private val paint = Paint()
    private val path = Path()

    private var lastTouchX: Float = 0.toFloat()
    private var lastTouchY: Float = 0.toFloat()
    private val dirtyRect = RectF()

    constructor(context: Context) : super(context) {
    }

    constructor(arg0: Context, arg1: AttributeSet) : super(arg0, arg1) {
    }

    constructor(arg0: Context, arg1: AttributeSet, arg2: Int) : super(arg0, arg1, arg2) {
    }

    /**
     * Get signature
     *
     * @return
     */
    // set the signature bitmap
    // important for saving signature
    val signature: Bitmap
        get() {

            var signatureBitmap: Bitmap? = null
            if (signatureBitmap == null) {
                signatureBitmap = Bitmap.createBitmap(this.width, this.height, Bitmap.Config.RGB_565)
            }
            val canvas = Canvas(signatureBitmap!!)
            this.draw(canvas)

            return signatureBitmap
        }

    init {

        paint.isAntiAlias = true
        paint.color = Color.BLACK
        paint.style = Paint.Style.STROKE
        paint.strokeJoin = Paint.Join.ROUND
        paint.strokeWidth = STROKE_WIDTH

        // set the bg color as white
        this.setBackgroundColor(Color.WHITE)

        // width and height should cover the screen
        this.layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT)

    }

    /**
     * clear signature canvas
     */
    fun clearSignature() {
        path.reset()
        this.invalidate()
    }
    fun GetPaint() : Boolean{

      return path.isEmpty
    }

    // all touch events during the drawing
    override fun onDraw(canvas: Canvas) {
        canvas.drawPath(this.path, this.paint)
    }

    override fun onTouchEvent(event: MotionEvent): Boolean {
        val eventX = event.x
        val eventY = event.y

        when (event.action) {
            MotionEvent.ACTION_DOWN -> {

                path.moveTo(eventX, eventY)

                lastTouchX = eventX
                lastTouchY = eventY
                return true
            }

            MotionEvent.ACTION_MOVE,

            MotionEvent.ACTION_UP -> {

                resetDirtyRect(eventX, eventY)
                val historySize = event.historySize
                for (i in 0 until historySize) {
                    val historicalX = event.getHistoricalX(i)
                    val historicalY = event.getHistoricalY(i)

                    expandDirtyRect(historicalX, historicalY)
                    path.lineTo(historicalX, historicalY)
                }
                path.lineTo(eventX, eventY)
            }

            else ->

                return false
        }

        invalidate((dirtyRect.left - HALF_STROKE_WIDTH).toInt(),
                (dirtyRect.top - HALF_STROKE_WIDTH).toInt(),
                (dirtyRect.right + HALF_STROKE_WIDTH).toInt(),
                (dirtyRect.bottom + HALF_STROKE_WIDTH).toInt())

        lastTouchX = eventX
        lastTouchY = eventY

        return true
    }

    private fun expandDirtyRect(historicalX: Float, historicalY: Float) {
        if (historicalX < dirtyRect.left) {
            dirtyRect.left = historicalX
        } else if (historicalX > dirtyRect.right) {
            dirtyRect.right = historicalX
        }

        if (historicalY < dirtyRect.top) {
            dirtyRect.top = historicalY
        } else if (historicalY > dirtyRect.bottom) {
            dirtyRect.bottom = historicalY
        }
    }

    private fun resetDirtyRect(eventX: Float, eventY: Float) {
        dirtyRect.left = Math.min(lastTouchX, eventX)
        dirtyRect.right = Math.max(lastTouchX, eventX)
        dirtyRect.top = Math.min(lastTouchY, eventY)
        dirtyRect.bottom = Math.max(lastTouchY, eventY)
    }

    companion object {

        // set the stroke width
        private val STROKE_WIDTH = 5f
        private val HALF_STROKE_WIDTH = STROKE_WIDTH / 2
    }
}
