package com.budbasic.posconsole.global;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;

public class Apppreference {

    Context context;
    private static final String APP_SHARED_PREFS = "com.budbasic.posconsole";
    private SharedPreferences appSharedPrefs;
    private SharedPreferences.Editor prefsEditor;

    public Apppreference(Context context) {
        this.context = context;
        this.appSharedPrefs = context.getSharedPreferences(APP_SHARED_PREFS, AppCompatActivity.MODE_PRIVATE);
        this.prefsEditor = appSharedPrefs.edit();
    }

    public void savePrintdetails(String printer_name) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("PrinterDetails", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        System.out.println("aaaaaaaaaaaa  printer_name  "+printer_name);
        editor.putString("printername", printer_name);
        editor.commit();
    }

    public String getPrinterName() {
        SharedPreferences sharedPreferences = context.getSharedPreferences("PrinterDetails", Context.MODE_PRIVATE);
        return sharedPreferences.getString("printername", "");
    }

    public void saveidentifier(String identifier,String interfaceType) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("saveidentifier", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("identifier", identifier);
        editor.putString("interfaceType", interfaceType);
        editor.commit();
    }
    public String getidentifier() {
        SharedPreferences sharedPreferences = context.getSharedPreferences("saveidentifier", Context.MODE_PRIVATE);
        return sharedPreferences.getString("identifier", "");
    } public String getinterfaceType() {
        SharedPreferences sharedPreferences = context.getSharedPreferences("saveidentifier", Context.MODE_PRIVATE);
        return sharedPreferences.getString("interfaceType", "");
    }

    public void saveData(String key,String value) {
        prefsEditor .putString(key, value);
        prefsEditor.commit();
    }

    public String getData(String key) {
        return appSharedPrefs.getString(key, "");
    }


}
