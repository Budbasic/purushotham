package com.budbasic.posconsole.global

import android.content.Context
import android.support.v4.content.ContextCompat
import android.support.v7.widget.CardView
import android.view.*
import android.view.View.OnTouchListener
import android.widget.PopupWindow
import android.widget.TextView
import com.budbasic.customer.global.Preferences
import com.budbasic.posconsole.R
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.google.gson.Gson
import com.kotlindemo.model.GetEmployeeAppSetting
import com.kotlindemo.model.GetEmployeeLogin
import de.hdodenhof.circleimageview.CircleImageView

open class UPDialog(private val triggerView: View, val onClick: onClickListener) : OnTouchListener {
    private val window: PopupWindow = PopupWindow(triggerView.context)
    private val windowManager: WindowManager
    var logindata: GetEmployeeLogin = Gson().fromJson(Preferences.getPreference(triggerView.context, "logindata"), GetEmployeeLogin::class.java)
    var employeeAppSetting:GetEmployeeAppSetting=Gson().fromJson(Preferences.getPreference(triggerView.context, "AppSetting"), GetEmployeeAppSetting::class.java)

    init {
        window.isTouchable = true
        window.setTouchInterceptor(this)
        windowManager = triggerView.context.getSystemService(Context.WINDOW_SERVICE) as WindowManager
        val inflate = LayoutInflater.from(triggerView.context).inflate(R.layout.profile_layout, null)

        val iv_user_profile = inflate.findViewById<CircleImageView>(R.id.iv_user_profile)
        val cv_logout = inflate.findViewById<CardView>(R.id.cv_logout)
        val cv_change_pin = inflate.findViewById<CardView>(R.id.cv_change_pin)
        val cv_change_password = inflate.findViewById<CardView>(R.id.cv_change_password)
        val tv_username = inflate.findViewById<TextView>(R.id.tv_username)
        val tv_useremail = inflate.findViewById<TextView>(R.id.tv_useremail)
        val tv_userroles = inflate.findViewById<TextView>(R.id.tv_userroles)
        val tv_useraddress = inflate.findViewById<TextView>(R.id.tv_useraddress)
        val tv_usernametill = inflate.findViewById<TextView>(R.id.tv_usernametill)

        tv_username.text = logindata.user_show_name
        tv_useremail.text = logindata.user_email
        tv_userroles.text = logindata.user_role_name
        tv_useraddress.text = logindata.user_store_address
        tv_usernametill.text = logindata.user_show_name

        println("aaaaaaaaaa  storename  "+employeeAppSetting.emp_image)

        if(employeeAppSetting!=null){
            if(employeeAppSetting.emp_image!=null && employeeAppSetting.emp_image.isNotEmpty()){
                Glide.with(triggerView.context) // safer!
                        .asBitmap()
                        .load(employeeAppSetting.emp_image)
                        .into(iv_user_profile)
            }

        }


        iv_user_profile.setOnClickListener {
            window.dismiss()
            onClick.callchangeprofile()
        }

        cv_logout.setOnClickListener {
            window.dismiss()
            onClick.callLogout()
        }
        cv_change_pin.setOnClickListener {
            window.dismiss()
            onClick.callChangePin()
        }
        cv_change_password.setOnClickListener {
            window.dismiss()
            onClick.callChangePassword()
        }
        window.contentView = inflate
        window.setBackgroundDrawable(ContextCompat.getDrawable(triggerView.context, R.drawable.null_selector))
        window.width = WindowManager.LayoutParams.WRAP_CONTENT
        window.height = WindowManager.LayoutParams.WRAP_CONTENT
        window.isTouchable = true
        window.isFocusable = true
        window.isOutsideTouchable = true
    }

    override fun onTouch(v: View, event: MotionEvent): Boolean {
        if (MotionEvent.ACTION_OUTSIDE == event.action) {
            this.window.dismiss()
            return true
        }
        return false
    }

    fun show() {
        val location = IntArray(2)
        triggerView.getLocationOnScreen(location)
        window.showAtLocation(triggerView, Gravity.NO_GRAVITY, location[0] - 30, location[1] + triggerView.height)
    }

    interface onClickListener {
        fun callLogout()
        fun callChangePin()
        fun callChangePassword()
        fun callchangeprofile()
    }
}
