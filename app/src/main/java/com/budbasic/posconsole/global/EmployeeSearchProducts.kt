package com.budbasic.posconsole.global

import android.annotation.SuppressLint
import android.app.Dialog
import android.app.DialogFragment
import android.content.Context
import android.graphics.Point
import android.opengl.Visibility
import android.os.Bundle
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.SearchView
import android.util.Log
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.EditText
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.budbasic.customer.global.Preferences
import com.budbasic.global.CUC
import com.budbasic.posconsole.R
import com.budbasic.posconsole.dialogs.ProductDetailsDialog
import com.budbasic.posconsole.webservice.RequetsInterface
import com.google.gson.Gson
import com.kotlindemo.model.GetEmployeeAppSetting
import com.kotlindemo.model.GetEmployeeLogin
import com.kotlindemo.model.GetProducts
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.store_searchs.view.*
import java.util.*

@SuppressLint("ValidFragment")
class EmployeeSearchProducts(val mContext: Context?) : DialogFragment() {
    lateinit var mDialog: Dialog
    lateinit var logindata: GetEmployeeLogin
    lateinit var mAppBasic: GetEmployeeAppSetting

    internal var mSearchProductName = ""

    lateinit var adapter: ReceiptTaxItemAdapter

    var product_id = ""

    companion object {
        var searchdataArrayList = ArrayList<GetProducts>()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(DialogFragment.STYLE_NO_TITLE, R.style.TransparentProgressDialog)
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View {
        return inflater!!.inflate(R.layout.store_searchs, container, false)
    }

    var mView: View? = null
    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mView = view
        mAppBasic = Gson().fromJson(Preferences.getPreference(mContext!!, "AppSetting"), GetEmployeeAppSetting::class.java)
        logindata = Gson().fromJson(Preferences.getPreference(mContext!!, "logindata"), GetEmployeeLogin::class.java)
        setupRecylcer()
        customizeSearchView()
        callEmployeeScanProducts("")
    }

    override fun onResume() {
        super.onResume()
        val display = activity!!.windowManager.defaultDisplay
        val size = Point()
        display.getSize(size)
        val width = (size.x / 2) + activity!!.resources.getDimensionPixelSize(R.dimen._100sdp)
        val height = size.y - activity!!.resources.getDimensionPixelSize(R.dimen._20sdp)
        Log.e("proData", "width : $width , height : $height")
        dialog.window.setLayout(width, height)
    }

    private fun setupRecylcer() {
        mView!!.iv_back.visibility = View.GONE
        val searchProduct = mView!!.findViewById<RecyclerView>(R.id.recycle_searchproduct)
        searchProduct.layoutManager = LinearLayoutManager(mContext!!)
        searchProduct.itemAnimator = DefaultItemAnimator()
        adapter = ReceiptTaxItemAdapter(object : onClickListener {
            override fun callClick(data: GetProducts) {
                Log.i("mProduct", "${data.toString()}")
                product_id = "${data.packet_id}"
                callEmployeeSalesProductInfo()
            }
        })
        searchProduct.adapter = adapter
    }

    private fun customizeSearchView() {
        val search_product = mView!!.findViewById<SearchView>(R.id.search_product)
        search_product.setFocusable(true)
        search_product.setIconified(false)
        search_product.setImeOptions(EditorInfo.IME_FLAG_NO_EXTRACT_UI);
        search_product.setOnSearchClickListener(View.OnClickListener {
            //iv_back.setVisibility(View.GONE);
        })
        /*  val searchMagIcon = search_product.findViewById(android.support.v7.appcompat.R.id.search_button) as ImageView
        //  searchMagIcon.setImageResource(R.drawable.ic_search)
          searchMagIcon.setImageBitmap(CUC.IconicsBitmap(activity, FontAwesome.Icon.faw_search, ContextCompat.getColor(activity, R.color.graylight)))
        */
        val searchBox = search_product.findViewById<EditText>(android.support.v7.appcompat.R.id.search_src_text)
        //searchBox.setBackgroundResource(R.drawable.null_selector)
        searchBox.setTextColor(resources.getColor(R.color.border_gray))
        searchBox.setHintTextColor(resources.getColor(R.color.graylight))
        searchBox.hint = "Product Name"
      //  searchBox.setTextSize(TypedValue.COMPLEX_UNIT_PX, 45f)
        val img_close = mView!!.findViewById<ImageView>(R.id.img_close)
        //  img_close.setImageBitmap(CUC.IconicsBitmap(activity, FontAwesome.Icon.faw_times, ContextCompat.getColor(activity, R.color.graylight)))
        img_close.setOnClickListener {
            searchdataArrayList.clear()
            dialog.dismiss()

        }


        search_product.setOnCloseListener(SearchView.OnCloseListener {
            searchdataArrayList.clear()
            adapter!!.notifyDataSetChanged()
            dialog.dismiss()
            //setResult(Activity.RESULT_OK)
            //finish()
            false
        })

        search_product.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String): Boolean {
                return false
            }

            override fun onQueryTextChange(newText: String): Boolean {
                if (adapter != null) {
                    if (!newText.isEmpty()) {
                        if (newText.length > 0) {
                            mSearchProductName = newText
                            callEmployeeScanProducts(mSearchProductName)
                        }

                    } else {
                        mSearchProductName = newText
                        searchdataArrayList.clear()
                        adapter!!.notifyDataSetChanged()
                        //mProductAdapter.notifyDataSetChanged();

                        callEmployeeScanProducts("")
                    }
                } else {
                    if (newText.length > 0) {
                        mSearchProductName = newText
                        callEmployeeScanProducts(mSearchProductName)
                    }
                }
                return false
            }
        })
    }

    class ReceiptTaxItemAdapter(val listner: onClickListener) : RecyclerView.Adapter<ReceiptTaxItemAdapter.ViewHolder>() {

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            holder!!.bindItems(searchdataArrayList[position], listner)
        }

        override fun getItemCount(): Int {
            return searchdataArrayList.size
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            val v = LayoutInflater.from(parent!!.context).inflate(R.layout.product_search_items, parent, false)
            return ViewHolder(v)
        }

        class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
            val tv_text: TextView = itemView.findViewById(R.id.tv_text)
            val tv_texttag: TextView = itemView.findViewById(R.id.tv_texttag)
            val ll_itemamin: LinearLayout = itemView.findViewById(R.id.ll_itemamin)
            val img_cat: ImageView=itemView.findViewById(R.id.img_cat);

            fun bindItems(data: GetProducts, listner: onClickListener) {
                img_cat.visibility=View.GONE;
                tv_texttag.visibility=View.VISIBLE;

                tv_text.text = "${data.packet_name}"
                tv_texttag.text = "SKU:${data.packet_barcode}"

                ll_itemamin.setOnClickListener {
                    listner.callClick(data)
                }
            }
        }
    }

    interface onClickListener {
        fun callClick(data: GetProducts)
    }

    fun callEmployeeScanProducts(name: String) {
        if (CUC.isNetworkAvailablewithPopup(mContext!!)) {
            //mDialog = CUC.createDialog(activity!!)
            mView!!.simpleProgressBar!!.visibility = View.VISIBLE
            val apiService = RequetsInterface.Factory.create()
            val parameterMap = HashMap<String, String>()
            parameterMap["api_key"] = "${Preferences.getPreference(mContext, "API_KEY")}"
            //if (::logindata.isInitialized)
            if (logindata != null) {
                parameterMap["store_id"] = logindata.user_store_id
                parameterMap["employee_id"] = logindata.user_id
            }
            parameterMap["product"] = "$name"

            Log.i("Result", "parameterMap : $parameterMap")
            CompositeDisposable().add(apiService.callEmployeeSearchProducts(parameterMap)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe({ result ->
                        //                        if (mDialog != null && mDialog.isShowing)
//                            mDialog.dismiss()
                        if (result != null) {
                            Log.i("Result", "" + result.toString())
                            if (result.status == "0") {
                                mView!!.simpleProgressBar!!.visibility = View.GONE
                                if (result.products != null && result.products.size > 0) {
                                    searchdataArrayList.clear()
                                    searchdataArrayList.addAll(result.products)
                                    adapter!!.notifyDataSetChanged()
                                    mView!!.tv_nodata.visibility = View.GONE
                                    mView!!.recycle_searchproduct.visibility = View.VISIBLE
                                } else {
                                    searchdataArrayList.clear()
                                    adapter!!.notifyDataSetChanged()
                                    mView!!.tv_nodata.visibility = View.VISIBLE
                                    mView!!.recycle_searchproduct.visibility = View.GONE
                                }
                                CUC.displayToast(mContext!!, result.show_status, result.message)
                            } else if (result.status == "10") {
                                mView!!.simpleProgressBar!!.visibility = View.GONE
                                //callApiKey()
                            } else {
                                mView!!.simpleProgressBar!!.visibility = View.GONE
                                CUC.displayToast(mContext!!, result.show_status, result.message)
                            }

                        } else {
                            mView!!.simpleProgressBar!!.visibility = View.GONE
                           // CUC.displayToast(mContext!!, "0", activity!!.getString(R.string.connection_to_database_failed))
                        }

                    }, { error ->
                        mView!!.simpleProgressBar!!.visibility = View.GONE
//                        if (mDialog != null && mDialog.isShowing)
//                            mDialog.dismiss()
                       // CUC.displayToast(mContext!!, "0", activity!!.getString(R.string.connection_to_database_failed))
                        error.printStackTrace()
                    })
            )
        }
    }

    fun callEmployeeSalesProductInfo() {
        if (CUC.isNetworkAvailablewithPopup(mContext!!)) {
            mDialog = CUC.createDialog(activity!!)
            val apiService = RequetsInterface.Factory.create()
            val parameterMap = HashMap<String, String>()
            parameterMap["api_key"] = "${Preferences.getPreference(mContext, "API_KEY")}"
            if (logindata != null) {
                //parameterMap["store_id"] = logindata.user_store_id
                parameterMap["employee_id"] = logindata.user_id
            }
            parameterMap["packet_id"] = "$product_id"
            Log.i("Result", "parameterMap : $parameterMap")
            CompositeDisposable().add(apiService.callEmployeeSalesProductInfo(parameterMap)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe({ result ->
                        if (mDialog != null && mDialog.isShowing)
                            mDialog.dismiss()
                        if (result != null) {
                            println("aaaaaaaaaaaaa    product info result  "+result.toString())
                            Log.i("Result", "" + result.toString())
                            if (result.status == "0") {
                                CUC.displayToast(mContext!!, result.show_status, result.message)
                                val data = Gson().toJson(result)
                                val mDialog = ProductDetailsDialog(mContext!!)
                                val mData = Bundle()
                                mData.putString("data", "$data")
                                mDialog.arguments = mData
                                mDialog.show(activity!!.fragmentManager, "productdetails")
                                //dialog.dismiss()
                            } else if (result.status == "10") {
                                //callApiKey()
                            } else {
                                CUC.displayToast(mContext!!, result.show_status, result.message)
                            }

                        } else {
                            //CUC.displayToast(mContext!!, "0", activity!!.getString(R.string.connection_to_database_failed))
                        }

                    }, { error ->
                        if (mDialog != null && mDialog.isShowing)
                            mDialog.dismiss()
                        println("aaaaaaaaaaaaaa error  "+error.message);
                        CUC.displayToast(mContext!!, "0", activity!!.getString(R.string.connection_to_database_failed))
                        error.printStackTrace()
                    })
            )
        }
    }

}