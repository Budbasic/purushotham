package com.budbasic.customer.global

import android.content.Context
import android.content.SharedPreferences
import java.util.*

object Preferences {

    /**
     * @param context - pass context
     * @return SharedPreferences
     *
     */
    fun getSharedPreferences(context: Context): SharedPreferences {
        return context.getSharedPreferences("prf_terminalvaultadmin", Context.MODE_PRIVATE)
    }

    /**
     * @param context - context
     * @param key     - Constant key, will be used for accessing the stored value
     * @param val     - String value to be stored
     */
    fun setPreference(context: Context, key: String, `val`: String) {
        val settings = Preferences.getSharedPreferences(context)
        val editor = settings.edit()
        editor.putString(key, `val`)
        editor.commit()
    }

    /**
     * @param context - context
     * @param key     - Constant key, will be used for accessing the stored value
     * @param val     - String value to be stored
     */
    fun setPreference_float(context: Context, key: String, `val`: Float) {
        val settings = Preferences.getSharedPreferences(context)
        val editor = settings.edit()
        editor.putFloat(key, `val`)
        editor.commit()
    }

    /**
     * @param context
     * @param key
     * @param val
     */
    fun setPreference(context: Context, key: String, `val`: Boolean) {
        val settings = Preferences.getSharedPreferences(context)
        val editor = settings.edit()
        editor.putBoolean(key, `val`)
        editor.commit()
    }

    /**
     * @param context
     * @param key
     * @param val
     */
    fun setPreference_int(context: Context, key: String, `val`: Int) {
        val settings = Preferences.getSharedPreferences(context)
        val editor = settings.edit()
        editor.putInt(key, `val`)
        editor.commit()
    }


    /**
     * Add preferences
     *
     * @param context - context
     * @param key     - Constant key, will be used for accessing the stored value
     * @param val     - long value to be stored, mostly used to store FB Session value
     */
    fun setPreference_long(context: Context, key: String, `val`: Long) {
        val settings = Preferences.getSharedPreferences(context)
        val editor = settings.edit()
        editor.putLong(key, `val`)
        editor.commit()
    }


    /**
     * Add preferences
     *
     * @param context           - context
     * @param key               - Constant key, will be used for accessing the stored value
     * @param ArrayList<String> val    - ArrayList<String> value to be stored, mostly used to store FB Session value
    </String></String> */

    fun setPreferenceArray(mContext: Context, key: String, array: ArrayList<String>): Boolean {
        val prefs = Preferences.getSharedPreferences(mContext)
        val editor = prefs.edit()
        editor.putInt(key + "_size", array.size)
        for (i in array.indices)
            editor.putString(key + "_" + i, array[i])
        return editor.commit()
    }


    fun clearPreferenceArray(c: Context, key: String) {
        val settings = Preferences.getSharedPreferences(c)

        if (getPreferenceArray(c, key) != null && getPreferenceArray(c, key).size > 0) {
            for (element in getPreferenceArray(c, key)) {
                if (findPrefrenceKey(c, element) != null && settings.contains(findPrefrenceKey(c, element))) {
                    val editor = settings.edit()
                    editor.remove(findPrefrenceKey(c, element))
                    editor.commit()
                }
            }
        }
    }


    fun findPrefrenceKey(con: Context, value: String): String? {
        val settings = Preferences.getSharedPreferences(con)

        val editor = settings.all

        for ((key, value1) in editor) {
            if (value == value1) {
                return key
            }
        }
        return null // not found
    }


    /**
     * Remove preference key
     *
     * @param context - context
     * @param key     - the key which you stored before
     */
    fun removePreference(context: Context, key: String) {
        val settings = Preferences.getSharedPreferences(context)
        val editor = settings.edit()
        editor.remove(key)
        editor.commit()
    }

    /**
     * Get preference value by passing related key
     *
     * @param context - context
     * @param key     - key value used when adding preference
     * @return - String value
     */
    fun getPreference(context: Context, key: String): String {
        val prefs = Preferences.getSharedPreferences(context)
        return prefs.getString(key, "")
    }

    /**
     * Get preference ArrayList<String> value by passing related key
     *
     * @param context - context
     * @param key     - key value used when adding preference
     * @return - ArrayList<String> value
    </String></String> */

    fun getPreferenceArray(mContext: Context, key: String): ArrayList<String> {
        val prefs = Preferences.getSharedPreferences(mContext)
        val size = prefs.getInt(key + "_size", 0)
        val array = ArrayList<String>(size)
        for (i in 0 until size)
            array.add(prefs.getString(key + "_" + i, null))
        return array
    }


    /**
     * Get preference value by passing related key
     *
     * @param context - context
     * @param key     - key value used when adding preference
     * @return - long value
     */
    fun getPreference_long(context: Context, key: String): Long {
        val prefs = Preferences.getSharedPreferences(context)
        return prefs.getLong(key, 0)

    }


    fun getPreference_boolean(context: Context, key: String): Boolean {
        val prefs = Preferences.getSharedPreferences(context)
        return prefs.getBoolean(key, false)

    }

    fun getPreference_int(context: Context, key: String): Int {
        val prefs = Preferences.getSharedPreferences(context)
        return prefs.getInt(key, 0)

    }


    /**
     * Clear all stored  preferences
     *
     * @param context - context
     */
    fun removeAllPreference(context: Context) {
        val settings = Preferences.getSharedPreferences(context)
        val editor = settings.edit()
        editor.clear()
        editor.commit()
    }

    /**
     * Clear all stored  preferences
     *
     * @param context - context
     */
    fun getAllPreference(context: Context): String {
        val settings = Preferences.getSharedPreferences(context)
        val editor = settings.all
        var text = ""

        try {
            for ((key, value) in editor) {
// do stuff
                text += "\t$key = $value\t"
            }
        } catch (e: Exception) {
            // TODO: handle exception
            e.printStackTrace()
        }

        return text

    }

}
