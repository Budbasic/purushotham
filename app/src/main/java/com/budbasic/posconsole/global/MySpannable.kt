package com.budbasic.customer.global

import android.graphics.Color
import android.text.TextPaint
import android.text.style.ClickableSpan
import android.view.View

/**
 * Created by Dev009 on 9/1/2017 1.
 */

open class MySpannable
/**
 * Constructor
 */
(isUnderline: Boolean) : ClickableSpan() {

    private var isUnderline = false

    init {
        this.isUnderline = isUnderline
    }

    override fun updateDrawState(ds: TextPaint) {

        ds.isUnderlineText = isUnderline
        ds.color = Color.parseColor("#c9c195")

    }

    override fun onClick(widget: View) {

    }
}
