package com.budbasic.posconsole.global;

public interface CallBackListner {
    void onSave(String path);
    void onCancel();
}
