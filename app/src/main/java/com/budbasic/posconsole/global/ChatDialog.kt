package com.budbasic.posconsole.global

import android.content.Context
import android.support.v4.content.ContextCompat
import android.support.v7.widget.CardView
import android.util.DisplayMetrics
import android.util.Log
import android.view.*
import android.view.View.OnTouchListener
import android.widget.Button
import android.widget.EditText
import android.widget.PopupWindow
import android.widget.TextView
import com.budbasic.customer.global.Preferences
import com.budbasic.posconsole.R
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.google.gson.Gson
import com.kotlindemo.model.GetEmployeeAppSetting
import com.kotlindemo.model.GetEmployeeLogin
import de.hdodenhof.circleimageview.CircleImageView
import kotlinx.android.synthetic.main.chatdialog.view.*
import android.view.Gravity
import com.budbasic.global.CUC
import com.budbasic.posconsole.webservice.RequetsInterface
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers


open class ChatDialog(private val triggerView: View, val onClick: onClickListener) : OnTouchListener {
    private val window: PopupWindow = PopupWindow(triggerView.context)
    private val windowManager: WindowManager
    var logindata: GetEmployeeLogin = Gson().fromJson(Preferences.getPreference(triggerView.context, "logindata"), GetEmployeeLogin::class.java)
    var employeeAppSetting:GetEmployeeAppSetting=Gson().fromJson(Preferences.getPreference(triggerView.context, "AppSetting"), GetEmployeeAppSetting::class.java)
    var help_message = ""
    var edt_help_message: EditText? = null
    init {
        window.isTouchable = true
        window.setTouchInterceptor(this)
        windowManager = triggerView.context.getSystemService(Context.WINDOW_SERVICE) as WindowManager
        val inflate = LayoutInflater.from(triggerView.context).inflate(R.layout.chatdialog, null)

        val message_text = inflate.findViewById<TextView>(R.id.messagetext)
        val cancel_button = inflate.findViewById<TextView>(R.id.btncancel)
        val submit_button = inflate.findViewById<TextView>(R.id.btnsubmit)
        edt_help_message = inflate.findViewById<EditText>(R.id.textArea_information)


        cancel_button.setOnClickListener {
            window.dismiss()
           // onClick.callLogout()
        }


        submit_button.setOnClickListener {
            if (edt_help_message!!.text.toString() == "") {
                CUC.displayToast(triggerView.context!!, "0", "Enter Query Message")
            } else {
                help_message = edt_help_message!!.text.toString()
                if (SingleSectionNotificationHoverMenuService.mHoverView != null)
                    SingleSectionNotificationHoverMenuService.mHoverView!!.collapse()
                callEmployeeHelpdesk()
            }

        }

        window.contentView = inflate
        window.setBackgroundDrawable(ContextCompat.getDrawable(triggerView.context, R.drawable.null_selector))
        window.width = WindowManager.LayoutParams.WRAP_CONTENT
        window.height = WindowManager.LayoutParams.WRAP_CONTENT
        window.isTouchable = true
        window.isFocusable = true
        window.isOutsideTouchable = true
    }

    override fun onTouch(v: View, event: MotionEvent): Boolean {
        if (MotionEvent.ACTION_OUTSIDE == event.action) {
            this.window.dismiss()
            return true
        }
        return false
    }



    fun callEmployeeHelpdesk() {
        //mDialog = CUC.createDialog(mContext!!)
        val apiService = RequetsInterface.create()
        val parameterMap = HashMap<String, String>()
        parameterMap["api_key"] = "${Preferences.getPreference(triggerView.context, "API_KEY")}"
        parameterMap["employee_id"] = "${logindata.user_id}"
        parameterMap["query_msg"] = help_message
        Log.i("Result", "parameterMap : $parameterMap")
        CompositeDisposable().add(apiService.callEmployeeHelpdesk(parameterMap)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe({ result ->
                    //                        if (mDialog != null && mDialog!!.isShowing)
//                            mDialog!!.dismiss()
                    if (result != null) {
                        Log.i("Result", "" + result.toString())
                        if (result.status == "0") {
                            help_message = ""
                            edt_help_message!!.setText("")
                            CUC.displayToast(triggerView.context!!, result.show_status, result.message)
                            window.dismiss()
                        } else if (result.status == "10") {
                            //apiClass!!.callApiKey(2)
                        } else {
                            CUC.displayToast(triggerView.context!!, result.show_status, result.message)
                        }
                    } else {
                        // CUC.displayToast(mContext!!, "0", mContext.getString(R.string.connection_to_database_failed))
                    }
                }, { error ->
                    //                        if (mDialog != null && mDialog!!.isShowing)
//                            mDialog!!.dismiss()
                    //   CUC.displayToast(mContext!!, "0", mContext.getString(R.string.connection_to_database_failed))

                    error.printStackTrace()
                })
        )
    }

    fun show() {
        window.showAtLocation(triggerView, Gravity.CENTER, 0, 0);
       // val location = IntArray(2)
       // triggerView.getLocationOnScreen(location)
       // window.showAtLocation(triggerView, Gravity.CENTER, location[0] - 0, location[1] + triggerView.height)
    }

    interface onClickListener {
        fun callLogout()
        fun callChangePin()
        fun callChangePassword()
    }
}
