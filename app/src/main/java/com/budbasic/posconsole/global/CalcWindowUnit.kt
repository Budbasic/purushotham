package com.budbasic.posconsole.global

import android.content.Context
import android.content.DialogInterface
import android.graphics.Point
import android.support.v4.content.ContextCompat
import android.support.v7.app.AlertDialog
import android.text.Editable
import android.text.InputType
import android.text.TextWatcher
import android.util.Log
import android.view.*
import android.view.View.OnTouchListener
import android.widget.*
import com.budbasic.customer.global.Preferences
import com.budbasic.global.CUC
import com.budbasic.posconsole.R
import com.budbasic.posconsole.model.APIClass
import com.budbasic.posconsole.webservice.RequetsInterface
import com.kotlindemo.model.unit
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import net.objecthunter.exp4j.ExpressionBuilder
import java.math.BigDecimal


open class CalcWindowUnit(private val triggerView: View) : View.OnClickListener, OnTouchListener, APIClass.onCallListner {

    override fun onFailed(requestCode: Int) {
    }

    override fun onSuccess(requestCode: Int) {
        callEmployeeUnits()
    }

    private var tvselectunit: TextView? = null

    var apiClass: APIClass? = null
    var mUnitMap = HashMap<String, unit>()

    private val window: PopupWindow = PopupWindow(triggerView.context)
    private val windowManager: WindowManager
    var mView: View? = null

    private var inputtext: EditText? = null
    //private var resulttext: TextView? = null
    private var but0: Button? = null
    private var but1: Button? = null
    private var but2: Button? = null
    private var but3: Button? = null
    private var but4: Button? = null
    private var but5: Button? = null
    private var but6: Button? = null
    private var but7: Button? = null
    private var but8: Button? = null
    private var but9: Button? = null
    private var butc: Button? = null
    private var butvir: Button? = null

//    private var butadd: Button? = null
//    private var butmin: Button? = null
//    private var butmulti: Button? = null
//    private var butdivi: Button? = null
//    private var butdelet: Button? = null
//    private var butbra: Button? = null
//    private var but100: Button? = null
//    private var butequl: Button? = null
//    private var butsing: Button? = null
//

    private var stateError: Boolean = false
    private var isNumber: Boolean = false
    private var lastDot: Boolean = false
    private var tv_title: TextView? = null
    private var iv_close: ImageView? = null
    private var draganddrop: View? = null
    private var tvConvertUnit: TextView? = null

    init {
        //window.setTouchInterceptor(this)
        windowManager = triggerView.context.getSystemService(Context.WINDOW_SERVICE) as WindowManager
        mView = LayoutInflater.from(triggerView.context).inflate(R.layout.calc_window_unit, null)
        window.contentView = mView
        window.setBackgroundDrawable(ContextCompat.getDrawable(triggerView.context, R.drawable.null_selector))
        val display = windowManager.defaultDisplay
        val size = Point()
        display.getSize(size)
        val width = (size.x / 3) + triggerView.context!!.resources.getDimensionPixelSize(R.dimen._15sdp)
        val height = size.y - triggerView.context!!.resources.getDimensionPixelSize(R.dimen._20sdp)
        Log.e("proData", "width : $width , height : $height")
        //dialog.window.setLayout(width, height)
        window.width = width
        window.height = height
        window.isTouchable = true
        window.isFocusable = true
        window.isOutsideTouchable = false
        window.setTouchInterceptor(this)
        inisializeButtons()
        setOnclick()
        // Hiding and disable keyboard
        inputtext!!.setRawInputType(InputType.TYPE_NULL)
        inputtext!!.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                //calcule(false)
                if ("${inputtext!!.text}".isNotEmpty() && "${inputtext!!.text}" != "") {
                    if ("${inputtext!!.text}".toDouble() > 0) {
                        val bdec = BigDecimal("${inputtext!!.text}")
                        val bdecRes = bdec.multiply(BigDecimal("${tvselectunit!!.tag}"))
                        println("Multiply:${bdecRes.toPlainString()}")
                        tvConvertUnit!!.text = "${bdecRes.toPlainString()}"
                    } else {
                        tvConvertUnit!!.text = ""
                    }
                } else {
                    tvConvertUnit!!.text = ""
                }
            }

            override fun afterTextChanged(s: Editable) {

            }
        })

        draganddrop!!.setOnTouchListener(object : OnTouchListener {
            internal var orgX: Int = 0
            internal var orgY: Int = 0
            internal var offsetX: Int = 0
            internal var offsetY: Int = 0

            override fun onTouch(v: View, event: MotionEvent): Boolean {
                when (event.action) {
                    MotionEvent.ACTION_DOWN -> {
                        draganddrop!!.setBackgroundColor(ContextCompat.getColor(mView!!.context, R.color.move))
                        orgX = event.x.toInt()
                        orgY = event.y.toInt()
                    }
                    MotionEvent.ACTION_UP -> {
                        draganddrop!!.setBackgroundColor(ContextCompat.getColor(mView!!.context, R.color.colorPrimaryDark))
                    }
                    MotionEvent.ACTION_MOVE -> {
                        offsetX = event.rawX.toInt() - orgX
                        offsetY = event.rawY.toInt() - orgY
                        window.update(offsetX, offsetY, -1, -1, true)
                    }
                }
                return true
            }
        })
    }

    override fun onTouch(v: View, event: MotionEvent): Boolean {
        if (MotionEvent.ACTION_OUTSIDE == event.action) {
            //this.window.dismiss()
            return false
        }
        return false
    }

    fun show() {
        val location = IntArray(2)
        triggerView.getLocationOnScreen(location)
        window.showAtLocation(triggerView, Gravity.NO_GRAVITY, location[0] - 30, location[1] + triggerView.height)
    }

    private fun inisializeButtons() {
        this.inputtext = mView!!.findViewById(R.id.edtConvertUnitValue)
        //this.resulttext = mView!!.findViewById(R.id.result)
        this.but0 = mView!!.findViewById(R.id.but0)
        this.but1 = mView!!.findViewById(R.id.but1)
        this.but2 = mView!!.findViewById(R.id.but2)
        this.but3 = mView!!.findViewById(R.id.but3)
        this.but4 = mView!!.findViewById(R.id.but4)
        this.but5 = mView!!.findViewById(R.id.but5)
        this.but6 = mView!!.findViewById(R.id.but6)
        this.but7 = mView!!.findViewById(R.id.but7)
        this.but8 = mView!!.findViewById(R.id.but8)
        this.but9 = mView!!.findViewById(R.id.but9)
        this.butc = mView!!.findViewById(R.id.butc)
        this.butc = mView!!.findViewById(R.id.butc)

        this.butvir = mView!!.findViewById(R.id.butv)
        this.tv_title = mView!!.findViewById(R.id.tv_title)
        this.draganddrop = mView!!.findViewById(R.id.draganddrop)
        this.iv_close = mView!!.findViewById(R.id.iv_close)
        this.tvselectunit = mView!!.findViewById(R.id.tvselectunit)
        this.tvConvertUnit = mView!!.findViewById(R.id.tvConvertUnit)

//        this.but100 = mView!!.findViewById(R.id.but100)
//        this.butadd = mView!!.findViewById(R.id.butplus)
//        this.butmin = mView!!.findViewById(R.id.butmin)
//        this.butmulti = mView!!.findViewById(R.id.butmult)
//        this.butdivi = mView!!.findViewById(R.id.butdivi)
//        this.butdelet = mView!!.findViewById(R.id.butdelet)
//        this.butbra = mView!!.findViewById(R.id.butbra)
//        this.butsing = mView!!.findViewById(R.id.butsin)
//        this.butequl = mView!!.findViewById(R.id.butequl)
        callEmployeeUnits()
    }

    private fun setOnclick() {
        this.but0!!.setOnClickListener(this)
        this.but1!!.setOnClickListener(this)
        this.but2!!.setOnClickListener(this)
        this.but3!!.setOnClickListener(this)
        this.but4!!.setOnClickListener(this)
        this.but5!!.setOnClickListener(this)
        this.but6!!.setOnClickListener(this)
        this.but7!!.setOnClickListener(this)
        this.but8!!.setOnClickListener(this)
        this.but9!!.setOnClickListener(this)
        this.butvir!!.setOnClickListener(this)
        this.butc!!.setOnClickListener(this)
        this.iv_close!!.setOnClickListener(this)
        this.tvselectunit!!.setOnClickListener(this)

//        this.but100!!.setOnClickListener(this)
//        this.butadd!!.setOnClickListener(this)
//        this.butmulti!!.setOnClickListener(this)
//        this.butmin!!.setOnClickListener(this)
//        this.butdelet!!.setOnClickListener(this)
//        this.butdivi!!.setOnClickListener(this)
//        this.butequl!!.setOnClickListener(this)
//        this.butdelet!!.setOnClickListener(this)
//        this.butsing!!.setOnClickListener(this)
//        this.butbra!!.setOnClickListener(this)
    }

    var levelDialog: AlertDialog? = null

    var selectItem = 0

    override fun onClick(v: View) {
        val Id = v.id
        when (Id) {
            R.id.iv_close -> {
                this.window.dismiss()
            }
            R.id.tvselectunit -> {
                val builder = AlertDialog.Builder(triggerView.context)
                builder.setTitle("Units")
                builder.setPositiveButton("OK") { dialogInterface, i ->
                    for (unitData in mUnitMap) {
                        if (unitData.key == mUnitMap.keys.toTypedArray()[selectItem]) {
                            tvselectunit!!.text = "${unitData.value.unit_name}"
                            tvselectunit!!.tag = "${unitData.value.unit_in_gram}"

                            inputtext!!.setText("${unitData.value.unit_base_value}")

                            Log.i(CalcWindowUnit::class.java.simpleName, "${unitData.value.unit_in_gram}")
                            //Toast.makeText(triggerView.context, "${unitData.value.unit_in_gram}", Toast.LENGTH_LONG).show()
                        }
                    }
                }
                builder.setNegativeButton("CANCEL") { dialogInterface, i ->
                }
                builder.setSingleChoiceItems(mUnitMap.keys.toTypedArray(), selectItem, object : DialogInterface.OnClickListener {
                    override fun onClick(p0: DialogInterface?, p1: Int) {
                        selectItem = p1
                    }

                })
                levelDialog = builder.create()
                levelDialog!!.show()
            }
            R.id.but0 -> {
                append("0")
                isNumber = true
            }
            R.id.but1 -> {
                append("1")
                isNumber = true
            }
            R.id.but2 -> {
                append("2")
                isNumber = true
            }
            R.id.but3 -> {
                append("3")
                isNumber = true
            }
            R.id.but4 -> {
                append("4")
                isNumber = true
            }
            R.id.but5 -> {
                append("5")
                isNumber = true
            }
            R.id.but6 -> {
                append("6")
                isNumber = true
            }
            R.id.but7 -> {
                append("7")
                isNumber = true
            }
            R.id.but8 -> {
                append("8")
                isNumber = true
            }
            R.id.but9 -> {
                append("9")
                isNumber = true
            }
            R.id.but100 -> if (!isEmpty() && isNumber)
                append("%")
            R.id.butplus -> {
                if (!isEmpty())
                    if (endsWithOperatore())
                        replace("+")
                    else
                        append("+")
                isNumber = false
                lastDot = false
            }
            R.id.butmin -> {
                if (endsWithOperatore())
                    replace("-")
                else
                    append("-")
                isNumber = false
                lastDot = false
            }
            R.id.butmult -> {
                if (!isEmpty())
                    if (endsWithOperatore())
                        replace("x")
                    else
                        append("x")
                isNumber = false
                lastDot = false
            }
            R.id.butdivi -> {
                if (!isEmpty())
                    if (endsWithOperatore())
                        replace("÷")
                    else
                        append("÷")
                isNumber = false
                lastDot = false
            }
            R.id.butv -> if (isNumber && !stateError && !lastDot) {
                append(".")
                isNumber = false
                lastDot = true
            } else if (isEmpty()) {
                append("0.")
                isNumber = false
                lastDot = true
            }
            R.id.butdelet -> delete()
            R.id.butc -> clear()
            R.id.butbra -> bracket()
            R.id.butequl -> calcule(true)
            R.id.butsin -> setSing()
            else -> {
            }
        }

    }

    private fun setSing() {
        if (isEmpty()) {
            append("(-")
        } else if (isNumber && !endsWithOperatore()) {
            val index1: Int
            val index2: Int
            val index3: Int
            val index4: Int
            var lastone = 0
            index1 = getinput().lastIndexOf("x") + 1
            index2 = getinput().lastIndexOf("+") + 1
            index3 = getinput().lastIndexOf("-") + 1
            index4 = getinput().lastIndexOf("/") + 1
            if (index1 > index2 && index1 > index3 && index1 > index4)
                lastone = index1
            else if (index2 > index1 && index2 > index3 && index2 > index4)
                lastone = index2
            else if (index3 > index2 && index3 > index1 && index3 > index4)
                lastone = index1
            else if (index4 > index1 && index4 > index3 && index4 > index2)
                lastone = index1
            val ch = getinput()[lastone]
            appendsing("(-" + ch.toString(), lastone)

        }
    }

    private fun appendsing(str: String, index: Int) {

        inputtext!!.getText().replace(index, index + 1, str)
    }


    private fun bracket() {
        if (!stateError && !isEmpty() && !endsWithbra() && isNumber || isclosed()) {
            append("x(")
        } else if (isEmpty() || endsWithOperatore() || endsWithbra()) {
            append("(")
        } else if (!isEmpty() && !endsWithbra()) {
            append(")")
        }
    }

    private fun endsWithbra(): Boolean {
        return getinput().endsWith("(")
    }

    private fun isclosed(): Boolean {
        return getinput().endsWith(")")
    }

    private fun endsWithOperatore(): Boolean {
        return getinput().endsWith("+") || getinput().endsWith("-") || getinput().endsWith("/") || getinput().endsWith("x") || getinput().endsWith("÷")
    }

    private fun replace(str: String) {
        inputtext!!.getText().replace(getinput().length - 1, getinput().length, str)
    }

    private fun clear() {
        lastDot = false
        isNumber = false
        stateError = false
        inputtext!!.getText().clear()
    }

    private fun append(str: String) {
        this.inputtext!!.getText().append(str)
    }

    private fun delete() {
        if (!isEmpty()) {
            this.inputtext!!.getText().delete(getinput().length - 1, getinput().length)

        } else
            clear()
    }

    private fun getinput(): String {
        return this.inputtext!!.text.toString()
    }

    private fun isEmpty(): Boolean {
        return getinput().isEmpty()
    }

    private fun calcule(isequlclick: Boolean) {

        var input = getinput()
        try {
            if (!isEmpty() && !endsWithOperatore()) {
                if (input.contains("x")) {
                    input = input.replace("x".toRegex(), "*")
                }

                if (input.contains("÷")) {
                    input = input.replace("÷".toRegex(), "/")
                }
                val expression = ExpressionBuilder(input).build()
                val result = expression.evaluate()
                if (isequlclick) {
                    inputtext!!.setText(result.toString())
                    //resulttext!!.setText("")
                } else {
                    //resulttext!!.setText(result.toString())
                }
            } else {
                //resulttext!!.setText("")
            }
        } catch (e: Exception) {
            stateError = true
            isNumber = false
        }
    }

    fun callEmployeeUnits() {
        if (CUC.isNetworkAvailablewithPopup(triggerView.context)) {
            val apiService = RequetsInterface.create()
            val parameterMap = HashMap<String, String>()
            parameterMap["api_key"] = "${Preferences.getPreference(triggerView.context, "API_KEY")}"
            Log.i("Result", "parameterMap : $parameterMap")
            CompositeDisposable().add(apiService.callEmployeeUnits(parameterMap)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe({ result ->
                        Log.i("Result", "" + result.toString())
                        if (result != null) {
                            if (result.status == "0") {
                                //dialogChangePin.dismiss()
                                for (mUnit in result.Unit) {
                                    if (tvselectunit!!.tag == "") {
                                        tvselectunit!!.text = "${mUnit.unit_name}"
                                        tvselectunit!!.tag = "${mUnit.unit_in_gram}"
                                        inputtext!!.setText("${mUnit.unit_base_value}")
                                    }
                                    mUnitMap.put("${mUnit.unit_name}", mUnit)
                                }
                            } else if (result.status == "10") {
                                apiClass!!.callApiKey(99)
                            } else {
                                CUC.displayToast(triggerView.context, result.show_status, result.message)
                            }
                        } else {
                            CUC.displayToast(triggerView.context, "0", triggerView.context.getString(R.string.connection_to_database_failed))
                        }
                    }, { error ->
                        CUC.displayToast(triggerView.context, "0", triggerView.context.getString(R.string.connection_to_database_failed))
                        error.printStackTrace()
                    })
            )
        }
    }

}
