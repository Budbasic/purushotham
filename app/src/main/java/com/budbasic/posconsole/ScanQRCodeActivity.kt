package com.budbasic.posconsole

import android.app.Activity
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.View
import android.view.Window
import com.google.zxing.Result
import kotlinx.android.synthetic.main.simple_scan_layout.*
import me.dm7.barcodescanner.zxing.ZXingScannerView


/**
 * Created by Dev009 on 9/15/2017 15.
 */

class ScanQRCodeActivity : AppCompatActivity(), ZXingScannerView.ResultHandler {


    public override fun onCreate(state: Bundle?) {
        super.onCreate(state)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.simple_scan_layout)
        initToolbar()
        init()
    }

    private fun initToolbar() {
        iv_scanner_toolbar_back!!.visibility = View.VISIBLE
        iv_scanner_toolbar_back!!.setOnClickListener { finish() }
        setSupportActionBar(tb_scanner_actionbar)
        if (supportActionBar != null) {
            supportActionBar!!.setHomeButtonEnabled(false)
            supportActionBar!!.setDisplayHomeAsUpEnabled(false)
            supportActionBar!!.setDisplayShowTitleEnabled(false)
        }
        tb_scanner_actionbar!!.setPadding(0, 0, 0, 0)//for tab otherwise give space in tab
        tb_scanner_actionbar!!.setContentInsetsAbsolute(0, 0)
        //mToolbar.setNavigationIcon(R.drawable.btn_back);
        tb_scanner_actionbar!!.setNavigationOnClickListener { onBackPressed() }
        tv_scanner__toolbar_title!!.text = "Scan QRCode"
        tb_scanner_actionbar!!.visibility = View.GONE
    }

    private fun init() {}

    public override fun onResume() {
        super.onResume()
        zxing_scanner!!.setResultHandler(this) // Register ourselves as a handler for scan results.
        zxing_scanner!!.startCamera()          // Start camera on resume
    }

    public override fun onPause() {
        super.onPause()
        // Stop camera on pause
        zxing_scanner!!.stopCamera()
    }

    override fun onBackPressed() {
        zxing_scanner!!.stopCamera()
        setResult(Activity.RESULT_CANCELED)
        super.onBackPressed()
    }

    override fun handleResult(rawResult: Result) {
        // Do something with the result here

        Log.i("ScanQRCodexxx", rawResult.text) // Prints scan results
        Log.i("ScanQRCodexxx123", rawResult.barcodeFormat.toString()) // Prints the scan format (qrcode, pdf417 etc.)
        intent.putExtra("qrcode", rawResult.text)
        zxing_scanner!!.stopCamera()
        this.setResult(Activity.RESULT_OK, intent)
        this.finish()
        // If you would like to resume scanning, call this method below:
        //zxing_scanner!!.resumeCameraPreview(this)
    }


}
