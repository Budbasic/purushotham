package com.budbasic.posconsole

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.Settings
import android.support.design.widget.Snackbar
import android.support.v4.app.ActivityCompat
import android.support.v7.app.AppCompatActivity
import android.telephony.TelephonyManager
import android.util.Log
import android.view.View
import android.view.Window
import android.view.WindowManager
import com.google.firebase.iid.FirebaseInstanceId
import com.google.gson.Gson
import com.kotlindemo.model.GetEmployeeAppSetting
import com.budbasic.posconsole.webservice.RequetsInterface
import com.tbruyelle.rxpermissions2.RxPermissions
import com.budbasic.customer.global.Preferences
import com.budbasic.global.CUC
import com.budbasic.posconsole.model.APIClass
import com.google.android.gms.tasks.OnSuccessListener
import com.google.firebase.iid.InstanceIdResult
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.functions.Consumer
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.registration_terminal_layout.*
import java.util.*


/**
 * Created by waytoweb on 20-04-2017.
 */

class RegistrationTerminalActivity : AppCompatActivity(), APIClass.onCallListner {
    override fun onFailed(requestCode: Int) {
        if (requestCode == 1) {
           // CUC.displayToast(this@RegistrationTerminalActivity, "0", getString(R.string.connection_to_database_failed))
        }
    }

    override fun onSuccess(requestCode: Int) {
        if (requestCode == 1) {
            callEmployeeTerminalRegistration()
        }
    }

    lateinit var mDialog: Dialog
    lateinit var logindata: GetEmployeeAppSetting

    var terminal_model = ""
    var rxPermissions: RxPermissions? = null
    var apiClass: APIClass? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN)
        setContentView(R.layout.registration_terminal_layout)
        apiClass = APIClass(this@RegistrationTerminalActivity, this)
        rxPermissions = RxPermissions(this)
        init()
    }

    private fun init() {
        logindata = Gson().fromJson(Preferences.getPreference(this@RegistrationTerminalActivity, "AppSetting"), GetEmployeeAppSetting::class.java)
        terminal_model = "${Build.MANUFACTURER} ${Build.MODEL}"
        AllViewClick()
    }

    private fun AllViewClick() {
        ll_forgot_back.setOnClickListener {
            finish()
            //overridePendingTransition(R.anim.fadein, R.anim.fadeout);
        }


        cv_submit.setOnClickListener {

            rxPermissions!!.request(
                    android.Manifest.permission.READ_PHONE_STATE)
                    .subscribe(object : Consumer<Boolean> {
                        override fun accept(granted: Boolean?) {
                            if (granted!!) { // Always true pre-M
                                // I can control the camera now
                                getIMEI()
                                /* deviceUniqueIdentifier= Settings.Secure.getString(contentResolver,
                                         Settings.Secure.ANDROID_ID)
 */
                                //registerPass()
                            } else {
                                // Oups permission denied
                                Snackbar.make(findViewById(R.id.terminal_registermain), resources.getString(R.string.permissionenable),
                                        Snackbar.LENGTH_LONG)
                                        .setActionTextColor(Color.WHITE)
                                        .setAction(resources.getString(R.string.ok), object : View.OnClickListener {
                                            override fun onClick(p0: View?) {
                                                val intent = Intent()
                                                intent.action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
                                                val uri = Uri.fromParts("package", packageName, null)
                                                intent.data = uri
                                                startActivity(intent)
                                            }
                                        }).show()
                            }
                        }

                    })

        }
    }

    var uniqueID = ""
    var deviceUniqueIdentifier = ""

    @SuppressLint("HardwareIds")
    fun getIMEI() {
        try {
            if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED) {
                val telephonyManager = getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager

                if (telephonyManager != null) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                        deviceUniqueIdentifier = telephonyManager.meid
                    } else if (android.os.Build.VERSION.SDK_INT >= 26) {
                        deviceUniqueIdentifier = telephonyManager.imei
                    } else {
                        deviceUniqueIdentifier = telephonyManager.deviceId
                        /*   deviceUniqueIdentifier= Settings.Secure.getString(contentResolver,
                                   Settings.Secure.ANDROID_ID)*/

                    }
                }

                Log.i("deviceUniqueIdentifier", "uniqueID : $deviceUniqueIdentifier")
                Log.i("deviceUniqueIdentifier", "uniqueID : ${UUID.randomUUID()}")
            }
            uniqueID = UUID.randomUUID().toString()
            Log.i("deviceUniqueIdentifier", "uniqueID : $uniqueID")
            Log.i("deviceUniqueIdentifier", "$deviceUniqueIdentifier")

        } catch (e: Exception) {
            e.printStackTrace()
        } finally {
            registerPass()
        }

    }

    fun registerPass() {
        if (edt_full_name.text.toString() == "") {
            //til_full_name.error = "Enter Name"
            CUC.displayToast(this@RegistrationTerminalActivity, "0", "Enter Name")
            edt_full_name.requestFocus()
        } else if (edt_email.text.toString() == "") {
            //til_email.error = "Enter Registered Email"
            CUC.displayToast(this@RegistrationTerminalActivity, "0", "Enter Email")
            edt_email.requestFocus()
        } else if (!CUC.isValidEmail(edt_email.text.toString())) {
            //til_email.error = "Enter valid Email"
            CUC.displayToast(this@RegistrationTerminalActivity, "0", "Enter valid Email")
            edt_email.requestFocus()
        } else {
            callEmployeeTerminalRegistration()
        }
    }

    fun callEmployeeTerminalRegistration() {
        FirebaseInstanceId.getInstance().instanceId.addOnSuccessListener(this@RegistrationTerminalActivity, object : OnSuccessListener<InstanceIdResult> {
            override fun onSuccess(p0: InstanceIdResult?) {
                val mToken = p0!!.token
                Log.e("Token", mToken)
                if (CUC.isNetworkAvailablewithPopup(this@RegistrationTerminalActivity)) {
                    mDialog = CUC.createDialog(this@RegistrationTerminalActivity)
                    val apiService = RequetsInterface.create()
                    val parameterMap = HashMap<String, String>()
                    parameterMap["api_key"] = "${Preferences.getPreference(this@RegistrationTerminalActivity, "API_KEY")}"
                    parameterMap["terminal_device_id"] = "${logindata.terminal_device_id}"
                    parameterMap["terminal_employee"] = "${edt_email.text.trim()}"
                    parameterMap["terminal_device_type"] = "Android"
                    parameterMap["terminal_device_token"] = "$mToken"
                    parameterMap["terminal_model"] = "$terminal_model"
                    parameterMap["terminal_name"] = "${edt_full_name.text.trim()}"
                    parameterMap["terminal_registration_date"] = "${logindata.terminal_registration}"
                    parameterMap["terminal_expiry_date"] = "${logindata.terminal_expiry}"
                    parameterMap["terminal_imei"] = "$deviceUniqueIdentifier"
                    parameterMap["terminal_udid"] = "$uniqueID"
                    Log.i("Result", "parameterMap : $parameterMap")
                    CompositeDisposable().add(apiService.callEmployeeTerminalRegistration(parameterMap)
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribeOn(Schedulers.io())
                            .subscribe({ result ->
                                if (mDialog != null && mDialog.isShowing)
                                    mDialog.dismiss()
                                Log.i("Result", "" + result.toString())
                                if (result != null) {
                                    if (result.status == "0") {
                                        CUC.displayToast(this@RegistrationTerminalActivity, result.show_status, result.message)
                                        val goIntent = Intent(this@RegistrationTerminalActivity, RegistrationApproved::class.java)
                                        goIntent.putExtra("message", "${result.message}")
                                        startActivity(goIntent)
                                        finish()
                                    } else if (result.status == "10") {
                                        apiClass!!.callApiKey(1)
                                    } else {
                                        CUC.displayToast(this@RegistrationTerminalActivity, result.show_status, result.message)
                                    }
                                } else {
                                  //  CUC.displayToast(this@RegistrationTerminalActivity, "0", getString(R.string.connection_to_database_failed))
                                }
                            }, { error ->
                                if (mDialog != null && mDialog.isShowing)
                                    mDialog.dismiss()
                                CUC.displayToast(this@RegistrationTerminalActivity, "0", getString(R.string.connection_to_database_failed))
                                error.printStackTrace()
                            })
                    )
                }

            }
        })
//        val refreshedToken = FirebaseInstanceId.getInstance().token
//        if (refreshedToken != "") {
//
//        } else {
//            callEmployeeTerminalRegistration()
//        }

    }
}
