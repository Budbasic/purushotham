package com.budbasic.posconsole

import android.app.Dialog
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.Window
import android.view.WindowManager
import com.budbasic.posconsole.webservice.RequetsInterface
import com.budbasic.customer.global.Preferences
import com.budbasic.global.CUC
import com.budbasic.posconsole.model.APIClass
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.forgotpass_layout.*


/**
 * Created by waytoweb on 20-04-2017.
 */

class ForgotPasswordActivity : AppCompatActivity(), APIClass.onCallListner {
    override fun onFailed(requestCode: Int) {
        if (requestCode == 1) {
           // CUC.displayToast(this@ForgotPasswordActivity, "0", getString(R.string.connection_to_database_failed))
        }
    }

    override fun onSuccess(requestCode: Int) {
        if (requestCode == 1) {
            callEmployeeForgotPassword()
        }
    }

    lateinit var mDialog: Dialog
    lateinit var apiClass: APIClass
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN)
        setContentView(R.layout.forgotpass_layout)
        apiClass = APIClass(this@ForgotPasswordActivity, this)
        init()
    }

    private fun init() {
        AllViewClick()
    }

    private fun AllViewClick() {
        ll_forgot_back.setOnClickListener {
            finish()
            //overridePendingTransition(R.anim.fadein, R.anim.fadeout);
        }

        cv_submit.setOnClickListener {
            if (edt_full_name.getText().toString().equals("")) {
                //til_full_name.error = "Enter Registered Email"
                CUC.displayToast(this@ForgotPasswordActivity, "0", "Enter Registered Email")
                edt_full_name.requestFocus()
            } else if (!CUC.isValidEmail(edt_full_name.getText().toString())) {
                //til_full_name.error = "Enter valid Email"
                CUC.displayToast(this@ForgotPasswordActivity, "0", "Enter valid Email")
                edt_full_name.requestFocus()
            } else {
                callEmployeeForgotPassword()
            }
        }
    }

    fun callEmployeeForgotPassword() {
        if (CUC.isNetworkAvailablewithPopup(this@ForgotPasswordActivity)) {
            mDialog = CUC.createDialog(this@ForgotPasswordActivity)
            val apiService = RequetsInterface.Factory.create()
            val parameterMap = HashMap<String, String>()
            parameterMap["api_key"] = "${Preferences.getPreference(this@ForgotPasswordActivity, "API_KEY")}"
            parameterMap["employee_email"] = "${edt_full_name.text.toString().trim()}"
            Log.i("Result", "parameterMap : $parameterMap")
            CompositeDisposable().add(apiService.callEmployeeForgotPassword(parameterMap)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe({ result ->
                        if (mDialog != null && mDialog.isShowing)
                            mDialog.dismiss()
                        Log.i("Result", "" + result.toString())
                        if (result != null) {
                            if (result.status == "0") {
                                CUC.displayToast(this@ForgotPasswordActivity, "0", result.message)
                                finish()
                            } else if (result.status == "10") {
                                apiClass.callApiKey(1)
                            } else {
                                CUC.displayToast(this@ForgotPasswordActivity, result.show_status, result.message)
                            }
                        } else {
                           // CUC.displayToast(this@ForgotPasswordActivity, "0", getString(R.string.connection_to_database_failed))
                        }
                    }, { error ->
                        if (mDialog != null && mDialog.isShowing)
                            mDialog.dismiss()
                        CUC.displayToast(this@ForgotPasswordActivity, "0", getString(R.string.connection_to_database_failed))
                        error.printStackTrace()
                    })
            )
        }
    }
}
