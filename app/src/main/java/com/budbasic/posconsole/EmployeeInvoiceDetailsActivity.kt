package com.budbasic.posconsole

import android.annotation.SuppressLint
import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.StrictMode
import android.provider.Settings
import android.support.design.widget.Snackbar
import android.support.v4.content.ContextCompat
import android.support.v7.app.AlertDialog
import android.support.v7.widget.CardView
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.*
import android.widget.*
import com.budbasic.customer.global.Preferences
import com.budbasic.global.BSV
import com.budbasic.global.CUC
import com.budbasic.posconsole.driver.SignatureActivity
import com.budbasic.posconsole.global.BaseActivity
import com.budbasic.posconsole.dialogs.PromoCodeDialog
import com.budbasic.posconsole.global.DigitsInputFilter
import com.budbasic.posconsole.model.APIClass
import com.budbasic.posconsole.printer.fragment.BluetoothScan
import com.budbasic.posconsole.reception.BackRoomReceiptPrintActivity
import com.budbasic.posconsole.reception.TimelineViewActivity
import com.budbasic.posconsole.webservice.RequetsInterface
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DecodeFormat
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.signature.ObjectKey
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.kotlindemo.model.*
import com.tbruyelle.rxpermissions2.RxPermissions
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.functions.Consumer
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.employee_cart_details_layout.*
import kotlinx.android.synthetic.main.raw_payment_method.view.*
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.File
import java.lang.NullPointerException
import java.util.*
import kotlin.collections.HashMap
import kotlin.collections.set


class EmployeeInvoiceDetailsActivity : BaseActivity(), APIClass.onCallListner {
    override fun setUp() {
    }

    override fun isNetworkConnected(): Boolean {
        return false
    }

    override fun onFailed(requestCode: Int) {
    }

    override fun showImage() {

    }

    override fun onSuccess(requestCode: Int) {
        if (requestCode == EMPLOYEEPATIENTCART) {
            callEmployeePatientCart()
        } else if (requestCode == EMPLOYEEGENERATEINVOICE) {
            callEmployeeGenerateInvoice()
        } else if (requestCode == EMPLOYEECANCELAPPROVE) {
            callEmployeeCancelApprove()
        } else if (requestCode == EMPLOYEECANCELORDER) {
            callEmployeeCancelOrder()
        } else if (requestCode == EMPLOYEEPAYMENTINVOICE) {
            callEmployeePaymentInvoice()
        } else if (requestCode == EMPLOYEEDRIVERREASSIGN) {
            callEmployeeDriverReassign()
        } else if (requestCode == EMPLOYEEVOIDORDER) {
            callEmployeeVoidOrder()
        } else if (requestCode == EMPLOYEEAPPLYSTMANDISCOUNT) {
            callEmployeeApplyStmanDiscount()
        } else if (requestCode == EMPLOYEEREMOVESTMANDISCOUNT) {
            callEmployeeRemoveStmanDiscount()
        }
    }

    lateinit var mDialog: Dialog

    val EMPLOYEEPATIENTCART = 1
    val EMPLOYEEGENERATEINVOICE = 2
    val EMPLOYEECANCELAPPROVE = 3
    val EMPLOYEECANCELORDER = 4
    val EMPLOYEEPAYMENTINVOICE = 5
    val EMPLOYEEDRIVERREASSIGN = 6
    val EMPLOYEEVOIDORDER = 7
    val EMPLOYEEAPPLYSTMANDISCOUNT = 8
    val EMPLOYEEREMOVESTMANDISCOUNT = 9

    var driverList = ArrayList<GetDrivers>()
    var paymentTypeList = ArrayList<GetPaymentTypes>()

    var list: ArrayList<GetInvoiceDetail> = ArrayList()
    lateinit var productAdapter: ProductAdapter

    lateinit var taxItemAdapter: TaxItemAdapter

    lateinit var appSettingData: GetEmployeeAppSetting
    var driver_id = ""
    var paymentId = ""
    var payment_amount = ""
    var change_amount = ""

    var Currency_value = ""
    var payment_path = ""
    var store_id = ""
    var patient_id = ""
    var Store_sign_required = ""

    var mainCartId = ""
    var invoice_taxes = ""
    var invoice_total_qty = ""
    var invoice_sub_total = ""
    var invoice_total_payble = ""
    var loyalty_amount = "0.00"
    var order_type = ""
    var all_data = ""
    var invoiceData: GetStoreOrders? = null

    var invoiceOrders: GetInvoiceOrders? = null

    var invoice_histroy: InvoiceHistory? = null
    lateinit var customDialog: Dialog

    var cancelReson = ""
    var driverAdapter: DriverAdapter? = null
    var paymentAdapter: PaymentAdapter? = null

    var driver_reaggine = "0"
    var status_driver_reaggine = false
    var rxPermissions: RxPermissions? = null
    lateinit var apiClass: APIClass

    companion object {
        var taxList: ArrayList<GetTaxData> = ArrayList()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
        setContentView(R.layout.employee_cart_details_layout)
        if (intent != null) {
            if (intent!!.hasExtra("status")) {
                if (intent.hasExtra("order_type"))
                    order_type = "${intent.getStringExtra("order_type")}"
                if (intent.hasExtra("all_data")) {
                    all_data = intent.getStringExtra("all_data")
                    if (all_data.isNotEmpty())
                        invoiceOrders = Gson().fromJson(all_data, GetInvoiceOrders::class.java)
                }
            } else if (intent.hasExtra("user_info_page_data")) {
                all_data = intent.getStringExtra("user_info_page_data")
                invoice_histroy = Gson().fromJson(all_data, InvoiceHistory::class.java)
            } else {
                if (intent.hasExtra("order_type"))
                    order_type = "${intent.getStringExtra("order_type")}"
                if (intent.hasExtra("all_data")) {
                    all_data = intent.getStringExtra("all_data")
                    if (all_data.isNotEmpty())
                        invoiceData = Gson().fromJson(all_data, GetStoreOrders::class.java)
                }
            }
        }
        apiClass = APIClass(this@EmployeeInvoiceDetailsActivity, this)
        rxPermissions = RxPermissions(this)
        appSettingData = Gson().fromJson(Preferences.getPreference(this@EmployeeInvoiceDetailsActivity, "AppSetting"), GetEmployeeAppSetting::class.java)
        logindata = Gson().fromJson(Preferences.getPreference(this@EmployeeInvoiceDetailsActivity, "logindata"), GetEmployeeLogin::class.java)
        Currency_value = "${appSettingData.Currency_value}"
        // Get local Bluetooth adapter
        init()
    }

    override fun onStart() {
        super.onStart()
    }

    private fun init() {
        if (intent!!.hasExtra("status")) {
            store_id = "${invoiceOrders!!.invoice_store_id!!}"
            patient_id = "${invoiceOrders!!.invoice_patient_id!!}"
            mainCartId = "${invoiceOrders!!.invoice_id!!}"
            invoice_taxes = "${invoiceOrders!!.invoice_taxes!!}"
            invoice_total_qty = "${invoiceOrders!!.invoice_total_qty!!}"
            invoice_sub_total = "${invoiceOrders!!.invoice_sub_total!!}"
            try {
                loyalty_amount = "${invoiceOrders!!.invoice_loyalty_redeem!!}"
                println("aaaaaaaaaaa invoiceOrders  ${invoiceOrders!!.invoice_loyalty_redeem!!}")
            }catch (e : NullPointerException){

            }
            invoice_total_payble = "${invoiceOrders!!.invoice_total_payble!!}"
            tv_pro_title.text = "${invoiceOrders!!.invoice_order_unique_no!!}"
        } else if (intent.hasExtra("user_info_page_data")) {
            if (invoice_histroy != null) {
                store_id = "${invoice_histroy!!.invoice_store_id!!}"
                patient_id = "${invoice_histroy!!.invoice_patient_id!!}"
                mainCartId = "${invoice_histroy!!.invoice_id!!}"
                invoice_taxes = "${invoice_histroy!!.invoice_taxes!!}"
                invoice_total_qty = "${invoice_histroy!!.invoice_total_qty!!}"
                invoice_sub_total = "${invoice_histroy!!.invoice_sub_total!!}"
                loyalty_amount = "${invoice_histroy!!.invoice_loyalty_redeem!!}"
                invoice_total_payble = "${invoice_histroy!!.invoice_total_payble!!}"
                tv_pro_title.text = "${invoice_histroy!!.invoice_order_unique_no!!}"
                tv_generate_bill.visibility = View.GONE
            }
        } else {
            if (invoiceData != null) {
                store_id = "${invoiceData!!.invoice_store_id!!}"
                patient_id = "${invoiceData!!.invoice_patient_id!!}"
                mainCartId = "${invoiceData!!.invoice_id!!}"
                invoice_taxes = "${invoiceData!!.invoice_taxes!!}"
                invoice_total_qty = "${invoiceData!!.invoice_total_qty!!}"
                invoice_sub_total = "${invoiceData!!.invoice_sub_total!!}"
                loyalty_amount = "${invoiceData!!.invoice_loyalty_redeem!!}"
                invoice_total_payble = "${invoiceData!!.invoice_total_payble!!}"
                tv_pro_title.text = "${invoiceData!!.invoice_order_unique_no!!}"
            }
        }
        if (order_type == "1") {
            tv_generate_bill.visibility = View.VISIBLE
            // tv_generate_bill.text = "Proceed"
            tv_generate_bill.text = "Generate Bill"
            tv_generate_bill.setBackgroundResource(R.color.yellowlight)
            iv_tracking.visibility = View.VISIBLE
            Log.d("AAAA11", "AAAA11")
            ll_promoCode.visibility = View.VISIBLE
            ll_redeem_option.visibility = View.GONE

            //iv_printer.visibility = View.VISIBLE
        } else if (order_type == "2") {
            Log.d("AAAA22", "AAAA22")
            tv_generate_bill.visibility = View.VISIBLE
            // tv_generate_bill.text = "Proceed"
            tv_generate_bill.text = resources.getString(R.string.make_payment)
            iv_tracking.visibility = View.VISIBLE
            ll_redeem_option.visibility = View.VISIBLE

            //iv_printer.visibility = View.VISIBLE
            if (invoiceData != null) {
                if (invoiceData!!.invoice_serve_type!! == "2") {
                    if (invoiceData!!.invoice_delivery_driver!! == "0") {
                        iv_driver.visibility = View.VISIBLE
                    }
                }
            }

        } else if (order_type == "3") {
            Log.d("AAAA33", "AAAA33")
            tv_generate_bill.visibility = View.VISIBLE
            tv_generate_bill.text = "Void Order"
            iv_tracking.visibility = View.VISIBLE
            ll_redeem_option.visibility = View.GONE
            tv_generate_bill.setBackgroundResource(R.color.close_red)

            //iv_printer.visibility = View.VISIBLE
        } else if (order_type == "4") {
            Log.d("AAAA44", "AAAA44")
            tv_generate_bill.visibility = View.VISIBLE
            tv_generate_bill.text = "Void Order"
            iv_tracking.visibility = View.VISIBLE
            tv_generate_bill.setBackgroundResource(R.color.close_red)
            //iv_printer.visibility = View.VISIBLE
        }

        iv_printer.setOnClickListener {
            val goIntent = Intent(this@EmployeeInvoiceDetailsActivity, BluetoothScan::class.java)
            startActivityForResult(goIntent, 2525)
        }

        iv_driver.setOnClickListener {
            driver_reaggine = "0"
            callDriverReaggine()
        }

        iv_back.setOnClickListener {
            onBackPressed()
        }

        iv_tracking.setOnClickListener {
            val passIntent = Intent(this@EmployeeInvoiceDetailsActivity, TimelineViewActivity::class.java)
            if (order_type == "4") {
                passIntent.putExtra("invoice_id", "$mainCartId")
                passIntent.putExtra("order_no", "${tv_pro_title.text}")
                passIntent.putExtra("invoice_no", "${invoiceOrders!!.invoice_no!!}")
            } else {
                passIntent.putExtra("invoice_id", "$mainCartId")
                passIntent.putExtra("order_no", "${tv_pro_title.text}")
                if (invoiceData != null) {
                    passIntent.putExtra("invoice_no", "${invoiceData!!.invoice_no!!}")
                }
            }
            startActivity(passIntent)
        }

        rv_tax_list!!.layoutManager = LinearLayoutManager(this@EmployeeInvoiceDetailsActivity)
        taxItemAdapter = TaxItemAdapter(this@EmployeeInvoiceDetailsActivity, Currency_value)
        rv_tax_list!!.adapter = taxItemAdapter
        rv_cart_list!!.layoutManager = LinearLayoutManager(this@EmployeeInvoiceDetailsActivity)
        productAdapter = ProductAdapter(list, this@EmployeeInvoiceDetailsActivity, Currency_value, object : onClickListener {
            override fun voidclick(data: GetInvoiceDetail) {
                println("aaaaaaaaaaaa  GetInvoiceDetail  "+data.toString())
                callPaymentReceived()
            }

            override fun deleteClick(data: GetCartData) {

            }

        })
        rv_cart_list!!.adapter = productAdapter


        ll_send_order.setOnClickListener {
        }

        tv_generate_bill.setOnClickListener {
            if (order_type == "1") {
                callReceivedAndInProcess()
            } else if (order_type == "2") {
                if (invoiceData != null) {
                    if (invoiceData!!.invoice_serve_type!! == "2") {
                        if (invoiceData!!.invoice_delivery_driver!! == "0") {
                            driver_reaggine = "1"
                            callDriverReaggine()
                        } else {
                            callInvoiceGenerated()
                        }
                    } else {
                        callInvoiceGenerated()

                    }
                }

            } else if (order_type == "3") {
                callPaymentReceived()
            } else if (order_type == "4") {
                callPaymentReceived()
            }

        }
        ll_redeem_option.setOnClickListener {
            if (tv_applyremove.text == "Remove Discount") {
                DisplayRemoveStmanDiscount()
            } else {
                DisplayRedeemPoints()
            }
        }
        callEmployeePatientCart()
    }

    @SuppressLint("ClickableViewAccessibility")
    private fun callReceivedAndInProcess() {
        driver_id = ""
        customDialog = Dialog(this@EmployeeInvoiceDetailsActivity, R.style.TransparentProgressDialog)
        val policy = StrictMode.ThreadPolicy.Builder().permitAll().build()
        StrictMode.setThreadPolicy(policy)
        customDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        customDialog.setContentView(R.layout.order_received_inprocess)
        customDialog.window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)
        customDialog.show()
        val tv_cancel_reson = customDialog.findViewById<EditText>(R.id.tv_cancel_reson)
        val tv_customer_name = customDialog.findViewById<TextView>(R.id.tv_customer_name)
        val tv_order_type = customDialog.findViewById<TextView>(R.id.tv_order_type)
        val bs_drivers = customDialog.findViewById<BSV>(R.id.bs_drivers)

        val textView5 = customDialog.findViewById<TextView>(R.id.textView5)
        val cv_approve_request = customDialog.findViewById<LinearLayout>(R.id.cv_approve_request)
        val cv_generate_invoice = customDialog.findViewById<LinearLayout>(R.id.cv_generate_invoice)
        val cv_cancel = customDialog.findViewById<LinearLayout>(R.id.cv_cancel)
        val ll_cancel_order = customDialog.findViewById<LinearLayout>(R.id.ll_cancel_order)
        val ll_customername = customDialog.findViewById<LinearLayout>(R.id.ll_customername)
        val ll_drivers_list = customDialog.findViewById<LinearLayout>(R.id.ll_drivers_list)
        val ll_order_type = customDialog.findViewById<LinearLayout>(R.id.ll_order_type)
        val onClickView = object : onClickDriver {
            override fun callClick(data: GetDrivers) {
                driver_id = "${data.emp_id}"
                bs_drivers.setText("${data.emp_first_name} ${data.emp_last_name}")
                bs_drivers.dismissDropDown()
                bs_drivers.clearFocus()
            }
        }
        if (invoiceData != null) {
            if (invoiceData!!.invoice_serve_type!! == "2") {
                tv_order_type.text = "Delivery"
                ll_drivers_list.visibility = View.VISIBLE
                driverAdapter = DriverAdapter(this@EmployeeInvoiceDetailsActivity, R.layout.driver_item_layout, driverList, onClickView)
                bs_drivers.setAdapter(driverAdapter)

            } else {
                tv_order_type.text = "Pickup"
                ll_drivers_list.visibility = View.GONE
            }

            if (invoiceData!!.invoice_cancel_request!! == "1") {
                ll_cancel_order.visibility = View.VISIBLE
                tv_cancel_reson.keyListener = null
                cv_generate_invoice.setBackgroundColor(ContextCompat.getColor(this@EmployeeInvoiceDetailsActivity, R.color.gray))
                tv_cancel_reson.setText("${invoiceData!!.invoice_cancel_reason!!}")
                textView5.text = "Approve Request"
                ll_drivers_list.visibility = View.GONE
                tv_cancel_reson.setOnTouchListener(object : View.OnTouchListener {
                    override fun onTouch(view: View?, event: MotionEvent?): Boolean {
                        if (view!!.id == R.id.tv_cancel_reson) {
                            view.parent.requestDisallowInterceptTouchEvent(true)
                            when (event!!.action and MotionEvent.ACTION_MASK) {
                                MotionEvent.ACTION_UP -> {
                                    view.parent.requestDisallowInterceptTouchEvent(false)
                                }
                            }
                        }
                        return false
                    }
                })
            } else {
                cv_generate_invoice.setBackgroundColor(ContextCompat.getColor(this@EmployeeInvoiceDetailsActivity, R.color.colorPrimaryDark))
                ll_cancel_order.visibility = View.GONE
                textView5.text = "Cancel Order"
            }

            tv_customer_name.text = "${invoiceData!!.patient_fname!!} ${invoiceData!!.patient_lname!!}"
        }


        cv_cancel.setOnClickListener {
            if (tv_cancel_reson.text.isEmpty()) {
                CUC.displayToast(this@EmployeeInvoiceDetailsActivity, "0", "Please enter cancel reason")
            } else {
                cancelReson = "${tv_cancel_reson.text}"
                callEmployeeCancelOrder()
            }
        }

        cv_approve_request.setOnClickListener {
            if (invoiceData != null) {
                if (invoiceData!!.invoice_cancel_request!! == "1") {
                    callEmployeeCancelApprove()
                } else {
                    ll_customername.visibility = View.GONE
                    ll_cancel_order.visibility = View.VISIBLE
                    cv_approve_request.visibility = View.GONE
                    cv_generate_invoice.visibility = View.GONE
                    ll_drivers_list.visibility = View.GONE
                    ll_order_type.visibility = View.GONE
                    cv_cancel.visibility = View.VISIBLE
                }
            }

        }

        cv_generate_invoice.setOnClickListener {
            if (invoiceData != null) {
                if (invoiceData!!.invoice_cancel_request!! != "1") {
                    if (invoiceData!!.invoice_serve_type!! == "2") {
                        if (driver_id == "") {
                            CUC.displayToast(this@EmployeeInvoiceDetailsActivity, "0", "Please select driver")
                        } else {
                            callEmployeeGenerateInvoice()
                        }

                    } else {
                        driver_id = ""
                        callEmployeeGenerateInvoice()

                    }
                } else {
                    CUC.displayToast(this@EmployeeInvoiceDetailsActivity, "0", "Customer has been applied for order cancellation !")
                }
            }

        }
    }

    private fun callDriverReaggine() {
        driver_id = ""
        customDialog = Dialog(this@EmployeeInvoiceDetailsActivity, R.style.TransparentProgressDialog)
        val policy = StrictMode.ThreadPolicy.Builder().permitAll().build()
        StrictMode.setThreadPolicy(policy)
        customDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        customDialog.setContentView(R.layout.order_driver_reaggine)
        customDialog.window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)
        customDialog.show()
        val bs_drivers = customDialog.findViewById<BSV>(R.id.bs_drivers)
        val cv_generate_invoice = customDialog.findViewById<LinearLayout>(R.id.cv_generate_invoice)
        val onClickView = object : onClickDriver {
            override fun callClick(data: GetDrivers) {
                driver_id = "${data.emp_id}"
                bs_drivers.setText("${data.emp_first_name} ${data.emp_last_name}")
                bs_drivers.dismissDropDown()
                bs_drivers.clearFocus()
            }
        }

        driverAdapter = DriverAdapter(this@EmployeeInvoiceDetailsActivity, R.layout.driver_item_layout, driverList, onClickView)
        bs_drivers.setAdapter(driverAdapter)

        cv_generate_invoice.setOnClickListener {
            if (invoiceData != null) {
                if (invoiceData!!.invoice_cancel_request!! != "1") {
                    if (driver_id == "") {
                        CUC.displayToast(this@EmployeeInvoiceDetailsActivity, "0", "Please select driver")
                    } else {
                        callEmployeeDriverReassign()
                    }
                } else {
                    CUC.displayToast(this@EmployeeInvoiceDetailsActivity, "0", "Customer has been applied for order cancellation !")
                }
            }

        }
    }

    var cart_manual_discount_type = "1"
    lateinit var dialogProductQty: Dialog

    var discount_type: String = "1"
    var discount_value: String = "0"

    var pin_value: String = ""

    private fun DisplayRedeemPoints() {
        discount_type = "1"
        dialogProductQty = Dialog(this@EmployeeInvoiceDetailsActivity, R.style.TransparentProgressDialog)
        val policy = StrictMode.ThreadPolicy.Builder().permitAll().build()
        StrictMode.setThreadPolicy(policy)
        //dialog_changepass.window.setLayout(resources.getDimensionPixelSize(R.dimen._10sdp) ,ViewGroup.LayoutParams.WRAP_CONTENT)
        dialogProductQty.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialogProductQty.setContentView(R.layout.redeem_point_dialog)
        dialogProductQty.window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)
        dialogProductQty.show()

        val cv_approve_request = dialogProductQty.findViewById<LinearLayout>(R.id.cv_approve_request)
        val cv_generate_invoice = dialogProductQty.findViewById<LinearLayout>(R.id.cv_generate_invoice)
        val rgdiscounttype = dialogProductQty.findViewById<RadioGroup>(R.id.rgdiscounttype)
        val edt_value = dialogProductQty.findViewById<EditText>(R.id.edt_value)
        val tv_discounttitle = dialogProductQty.findViewById<TextView>(R.id.tv_discounttitle)
        val edt_pin = dialogProductQty.findViewById<EditText>(R.id.edt_pin)
        val ll_discount_type = dialogProductQty.findViewById<LinearLayout>(R.id.ll_discount_type)
        val ll_fix_discount = dialogProductQty.findViewById<LinearLayout>(R.id.ll_fix_discount)
        ll_discount_type.visibility = View.VISIBLE
        ll_fix_discount.visibility = View.VISIBLE

        rgdiscounttype.setOnCheckedChangeListener(object : RadioGroup.OnCheckedChangeListener {
            override fun onCheckedChanged(p0: RadioGroup?, p1: Int) {
                when (p1) {
                    R.id.rbFixDiscount -> {
                        discount_type = "1"
                        edt_value.hint = "Enter Fix Discount"
                        tv_discounttitle.text = "Enter Fix Discount"
                    }
                    R.id.rbPercentageDiscount -> {
                        discount_type = "2"
                        edt_value.hint = "Enter Percentage Discount"
                        tv_discounttitle.text = "Enter Percentage Discount"
                    }
                }
            }
        })
        cv_approve_request.setOnClickListener {
            dialogProductQty.dismiss()
        }

        cv_generate_invoice.setOnClickListener {
            if (edt_value.text.toString() == "") {
                CUC.displayToast(this@EmployeeInvoiceDetailsActivity, "0", "Please ${tv_discounttitle.text}")
            } else if (edt_value.text.toString().toFloat() <= 0) {
                CUC.displayToast(this@EmployeeInvoiceDetailsActivity, "0", "Please ${tv_discounttitle.text} greater than 0")
            } else if (edt_pin.text.toString() == "") {
                CUC.displayToast(this@EmployeeInvoiceDetailsActivity, "0", "Please Enter Your PIN")
            } else {
                discount_value = "${edt_value.text}"
                pin_value = "${edt_pin.text}"
                callEmployeeApplyStmanDiscount()
            }
        }
    }

    private fun DisplayRemoveStmanDiscount() {
        dialogProductQty = Dialog(this@EmployeeInvoiceDetailsActivity, R.style.TransparentProgressDialog)
        val policy = StrictMode.ThreadPolicy.Builder().permitAll().build()
        StrictMode.setThreadPolicy(policy)
        dialogProductQty.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialogProductQty.setContentView(R.layout.redeem_point_dialog)
        dialogProductQty.window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)
        dialogProductQty.show()

        val cv_approve_request = dialogProductQty.findViewById<LinearLayout>(R.id.cv_approve_request)
        val cv_generate_invoice = dialogProductQty.findViewById<LinearLayout>(R.id.cv_generate_invoice)
        val edt_value = dialogProductQty.findViewById<EditText>(R.id.edt_value)
        val tv_discounttitle = dialogProductQty.findViewById<TextView>(R.id.tv_discounttitle)
        val edt_pin = dialogProductQty.findViewById<EditText>(R.id.edt_pin)
        val ll_discount_type = dialogProductQty.findViewById<LinearLayout>(R.id.ll_discount_type)
        val ll_fix_discount = dialogProductQty.findViewById<LinearLayout>(R.id.ll_fix_discount)
        val tv_submit = dialogProductQty.findViewById<TextView>(R.id.tv_submit)

        ll_discount_type.visibility = View.GONE
        ll_fix_discount.visibility = View.GONE
        tv_submit.text = "Remove"

        cv_approve_request.setOnClickListener {
            dialogProductQty.dismiss()
        }

        cv_generate_invoice.setOnClickListener {
            if (edt_pin.text.toString() == "") {
                CUC.displayToast(this@EmployeeInvoiceDetailsActivity, "0", "Please Enter Your PIN")
            } else {
                pin_value = "${edt_pin.text}"
                callEmployeeRemoveStmanDiscount()
            }
        }
    }

    lateinit var iv_siganture: ImageView


//    private fun callInvoiceGenerated()
//    {
//        paymentId = ""
//        customDialog = Dialog(this@EmployeeInvoiceDetailsActivity, R.style.TransparentProgressDialog)
//        val policy = StrictMode.ThreadPolicy.Builder().permitAll().build()
//        StrictMode.setThreadPolicy(policy)
//        customDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
//        customDialog.setContentView(R.layout.order_invoice_generated)
//        customDialog.window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)
//        customDialog.show()
//
//        val tv_cancel_reson = customDialog.findViewById<EditText>(R.id.tv_cancel_reson)
//        val tv_customer_name = customDialog.findViewById<TextView>(R.id.tv_customer_name)
//        val bs_payment = customDialog.findViewById<BSV>(R.id.bs_payment)
//        val tv_order_type = customDialog.findViewById<TextView>(R.id.tv_order_type)
//
//        val tv_total_amount = customDialog.findViewById<TextView>(R.id.tv_total_amount)
//
//        val edt_total_amt = customDialog.findViewById<EditText>(R.id.edt_total_amt)
//        val textView5 = customDialog.findViewById<TextView>(R.id.textView5)
//        val cv_approve_request = customDialog.findViewById<CardView>(R.id.cv_approve_request)
//        val cv_generate_invoice = customDialog.findViewById<CardView>(R.id.cv_generate_invoice)
//        val cv_cancel = customDialog.findViewById<CardView>(R.id.cv_cancel)
//        val ll_cancel_order = customDialog.findViewById<LinearLayout>(R.id.ll_cancel_order)
//        val ll_customername = customDialog.findViewById<LinearLayout>(R.id.ll_customername)
//        val ll_payment_type = customDialog.findViewById<LinearLayout>(R.id.ll_payment_type)
//        val ll_order_type = customDialog.findViewById<LinearLayout>(R.id.ll_order_type)
//        val ll_digital_siganture = customDialog.findViewById<LinearLayout>(R.id.ll_digital_siganture)
//        val tv_digital_siganture: TextView = customDialog.findViewById(R.id.tv_digital_siganture)
//
//        val ll_totalamount = customDialog.findViewById<LinearLayout>(R.id.ll_totalamount)
//        val ll_paidamount = customDialog.findViewById<LinearLayout>(R.id.ll_paidamount)
//
//        iv_siganture = customDialog.findViewById(R.id.iv_siganture)
//        if (invoiceData != null) {
//            if (invoiceData!!.invoice_serve_type!! == "2") {
//                tv_order_type.text = "Delivery"
//
//            } else {
//                tv_order_type.text = "Pickup"
//            }
//            tv_total_amount.text = "$ " + "${invoiceData!!.invoice_total_payble!!}"
//        }
//
//
//        val onClickPayment = object : onClickPayment {
//            override fun callClick(data: GetPaymentTypes) {
//                paymentId = "${data.payment_id}"
//                bs_payment.setText("${data.payment_method}")
//                bs_payment.dismissDropDown()
//                bs_payment.clearFocus()
//            }
//        }
//
//        if (Store_sign_required != "" && Store_sign_required == "1") {
//            ll_digital_siganture.visibility = View.VISIBLE
//        } else {
//            ll_digital_siganture.visibility = View.GONE
//        }
//        tv_digital_siganture.setOnClickListener {
//            rxPermissions!!.request(
//                    android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
//                    .subscribe(object : Consumer<Boolean> {
//                        override fun accept(granted: Boolean?) {
//                            if (granted!!) { // Always true pre-M
//                                // I can control the camera now
//                                val siantureIntent = Intent(this@EmployeeInvoiceDetailsActivity, SignatureActivity::class.java)
//                                startActivityForResult(siantureIntent, 8989)
//                            } else {
//                                // Oups permission denied
//                                Snackbar.make(coordinator_backroom, resources.getString(R.string.permissionenable),
//                                        Snackbar.LENGTH_LONG)
//                                        .setActionTextColor(Color.WHITE)
//                                        .setAction(resources.getString(R.string.ok), object : View.OnClickListener {
//                                            override fun onClick(p0: View?) {
//                                                val intent = Intent()
//                                                intent.action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
//                                                val uri = Uri.fromParts("package", this@EmployeeInvoiceDetailsActivity.packageName, null)
//                                                intent.data = uri
//                                                startActivity(intent)
//                                            }
//                                        }).show()
//                            }
//                        }
//                    })
//        }
//        paymentAdapter = PaymentAdapter(this@EmployeeInvoiceDetailsActivity, R.layout.payment_type_item, paymentTypeList, payment_path, onClickPayment)
//        bs_payment.setAdapter(paymentAdapter)
//
//        try {
//            if (paymentTypeList != null && paymentTypeList.size > 0) {
//                //if (paymentTypeList.size == 1) {
//                paymentId = "${paymentTypeList[0].payment_id}"
//                bs_payment.setText("${paymentTypeList[0].payment_method}")
//                bs_payment.dismissDropDown()
//                bs_payment.clearFocus()
//                //}
//            }
//        } catch (e: Exception) {
//            e.printStackTrace()
//        }
//        if (invoiceData != null)
//        {
//            if (invoiceData!!.invoice_cancel_request!! == "1") {
//                ll_cancel_order.visibility = View.VISIBLE
//                tv_cancel_reson.keyListener = null
//                cv_generate_invoice.setCardBackgroundColor(ContextCompat.getColor(this@EmployeeInvoiceDetailsActivity, R.color.gray))
//                tv_cancel_reson.setText("${invoiceData!!.invoice_cancel_reason!!}")
//                textView5.text = "Approve Request"
//                ll_payment_type.visibility = View.GONE
//                tv_cancel_reson.setOnTouchListener(object : View.OnTouchListener {
//                    override fun onTouch(view: View?, event: MotionEvent?): Boolean {
//                        if (view!!.id == R.id.tv_cancel_reson) {
//                            view.parent.requestDisallowInterceptTouchEvent(true)
//                            when (event!!.action and MotionEvent.ACTION_MASK) {
//                                MotionEvent.ACTION_UP -> {
//                                    view.parent.requestDisallowInterceptTouchEvent(false)
//                                }
//                            }
//                        }
//                        return false
//                    }
//                })
//            } else {
//                cv_generate_invoice.setCardBackgroundColor(ContextCompat.getColor(this@EmployeeInvoiceDetailsActivity, R.color.colorPrimaryDark))
//                ll_cancel_order.visibility = View.GONE
//                textView5.text = "Cancel Order"
//
//                if (invoiceData!!.invoice_serve_type!! == "2") {
//                    ll_payment_type.visibility = View.GONE
//                } else {
//                    ll_payment_type.visibility = View.VISIBLE
//                }
//            }
//
//            tv_customer_name.text = "${invoiceData!!.patient_fname!!} ${invoiceData!!.patient_lname!!}"
//
//        }
//
//
//        cv_cancel.setOnClickListener {
//            if (tv_cancel_reson.text.isEmpty()) {
//                CUC.displayToast(this@EmployeeInvoiceDetailsActivity, "0", "Please enter cancel reason")
//            } else {
//                cancelReson = "${tv_cancel_reson.text}"
//                callEmployeeCancelOrder()
//            }
//        }
//
//        cv_approve_request.setOnClickListener {
//            if (invoiceData != null) {
//                if (invoiceData!!.invoice_cancel_request!! == "1") {
//                    callEmployeeCancelApprove()
//                } else {
//                    ll_customername.visibility = View.GONE
//                    ll_cancel_order.visibility = View.VISIBLE
//                    cv_approve_request.visibility = View.GONE
//                    cv_generate_invoice.visibility = View.GONE
//                    ll_order_type.visibility = View.GONE
//                    ll_payment_type.visibility = View.GONE
//                    ll_digital_siganture.visibility = View.GONE
//                    ll_totalamount.visibility = View.GONE
//                    ll_paidamount.visibility = View.GONE
//                    cv_cancel.visibility = View.VISIBLE
//                }
//            }
//
//        }
//
//        cv_generate_invoice.setOnClickListener {
//            if (invoiceData != null) {
//                if (invoiceData!!.invoice_cancel_request!! != "1") {
//                    if (invoiceData!!.invoice_serve_type!! == "2") {
//                        paymentId = ""
//                        if (edt_total_amt.text.toString() != "") {
//
//                            //val strValue = edt_total_amt.text.toString().replace("[^0-9?!]\\.]", "", true)
//                            if (edt_total_amt.text.toString().toFloat() >= CUC.getStringtoNumberWithDecimal("${tv_total_amount.text}").toFloat()) {
//                                if (Store_sign_required == "1") {
//                                    if (siganture_path != "") {
//                                        payment_amount = CUC.getStringtoNumberWithDecimal("${tv_total_amount.text}")
//                                        change_amount = "${edt_total_amt.text}"
//                                        callEmployeePaymentInvoice()
//                                    } else {
//                                        CUC.displayToast(this@EmployeeInvoiceDetailsActivity, "0", "Patient signature required")
//                                    }
//                                } else {
//                                    siganture_path = ""
//                                    payment_amount = CUC.getStringtoNumberWithDecimal("${tv_total_amount.text}")
//                                    change_amount = "${edt_total_amt.text}"
//                                    callEmployeePaymentInvoice()
//                                }
//                            } else {
//                                Toast.makeText(this@EmployeeInvoiceDetailsActivity, "Please pay complete amount", Toast.LENGTH_LONG).show()
//                            }
//                        } else {
//                            Toast.makeText(this@EmployeeInvoiceDetailsActivity, "Please Enter Paid Amount", Toast.LENGTH_LONG).show()
//                        }
//                    } else {
//                        if (paymentId == "") {
//                            CUC.displayToast(this@EmployeeInvoiceDetailsActivity, "0", "Please select payment")
//                        } else {
//                            if (edt_total_amt.text.toString() != "") {
//                                if ("${edt_total_amt.text}".toFloat() >= "${tv_total_amount.text}".toFloat()) {
//                                    if (Store_sign_required == "1") {
//                                        if (siganture_path != "") {
//                                            payment_amount = CUC.getStringtoNumberWithDecimal("${tv_total_amount.text}")
//                                            change_amount = "${edt_total_amt.text}"
//                                            callEmployeePaymentInvoice()
//                                        } else {
//                                            CUC.displayToast(this@EmployeeInvoiceDetailsActivity, "0", "Patient signature required")
//                                        }
//                                    } else {
//                                        siganture_path = ""
//                                        payment_amount = CUC.getStringtoNumberWithDecimal("${tv_total_amount.text}")
//                                        change_amount = "${edt_total_amt.text}"
//                                        callEmployeePaymentInvoice()
//                                    }
//                                } else {
//                                    Toast.makeText(this@EmployeeInvoiceDetailsActivity, "Please pay complete amount", Toast.LENGTH_LONG).show()
//                                }
//                            } else {
//                                Toast.makeText(this@EmployeeInvoiceDetailsActivity, "Please Enter Paid Amount", Toast.LENGTH_LONG).show()
//                            }
//                        }
//                    }
//
//                } else {
//                    CUC.displayToast(this@EmployeeInvoiceDetailsActivity, "0", "Customer has been applied for order cancellation !")
//                }
//            }
//
//        }
//    }

    fun callInvoiceGenerated() {
        paymentId = ""
        customDialog = Dialog(this@EmployeeInvoiceDetailsActivity, R.style.TransparentProgressDialog)
        val policy = StrictMode.ThreadPolicy.Builder().permitAll().build()
        StrictMode.setThreadPolicy(policy)
        customDialog.window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE)
        customDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        customDialog.setContentView(R.layout.sales_invoice_dialog_new)
        customDialog.window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)
        customDialog.show()

        val amount_field = customDialog.findViewById<EditText>(R.id.text_amount)
        //amount_field.setText("$total_payble")
        amount_field.setSelection(amount_field.text.toString().length)


        val onClickListner = View.OnClickListener {
            val editable = amount_field.getText().toString()
            val valueString = editable + (it.tag as String)

            if (valueString.contains(".")) {
                val f = java.lang.Float.valueOf(valueString)
                amount_field.setText(String.format("%.2f", f))
            } else {
                amount_field.setText(valueString)
            }
        }

        val tv_total_amount = customDialog.findViewById<TextView>(R.id.tv_total_amount)
        val edt_total_amt = customDialog.findViewById<EditText>(R.id.edt_total_amt)
        val tv_customer_name = customDialog.findViewById<TextView>(R.id.tv_customer_name)
        val bs_payment = customDialog.findViewById<BSV>(R.id.bs_payment)
        val cv_generate_invoice = customDialog.findViewById<TextView>(R.id.cv_generate_invoice)
        val ll_digital_siganture = customDialog.findViewById<LinearLayout>(R.id.ll_digital_siganture)
        val tv_digital_siganture: TextView = customDialog.findViewById(R.id.tv_digital_siganture)
        val tv_cancel = customDialog.findViewById<TextView>(R.id.tv_cancel)
        val recy_payment = customDialog.findViewById<RecyclerView>(R.id.recy_payment)
        val llPaymentType = customDialog.findViewById<LinearLayout>(R.id.llPaymentType)

        val t9_key_1 = customDialog.findViewById<TextView>(R.id.t9_key_1)
        val t9_key_2 = customDialog.findViewById<TextView>(R.id.t9_key_2)
        val t9_key_3 = customDialog.findViewById<TextView>(R.id.t9_key_3)
        val t9_key_4 = customDialog.findViewById<TextView>(R.id.t9_key_4)
        val t9_key_5 = customDialog.findViewById<TextView>(R.id.t9_key_5)
        val t9_key_6 = customDialog.findViewById<TextView>(R.id.t9_key_6)
        val t9_key_7 = customDialog.findViewById<TextView>(R.id.t9_key_7)
        val t9_key_8 = customDialog.findViewById<TextView>(R.id.t9_key_8)
        val t9_key_9 = customDialog.findViewById<TextView>(R.id.t9_key_9)
        val t9_key_0 = customDialog.findViewById<TextView>(R.id.t9_key_0)
        val t9_key_dote = customDialog.findViewById<TextView>(R.id.t9_key_dote)


        t9_key_1.setOnClickListener(onClickListner)
        t9_key_2.setOnClickListener(onClickListner)
        t9_key_3.setOnClickListener(onClickListner)
        t9_key_4.setOnClickListener(onClickListner)
        t9_key_5.setOnClickListener(onClickListner)
        t9_key_6.setOnClickListener(onClickListner)
        t9_key_7.setOnClickListener(onClickListner)
        t9_key_8.setOnClickListener(onClickListner)
        t9_key_9.setOnClickListener(onClickListner)
        t9_key_0.setOnClickListener(onClickListner)
        t9_key_dote.setOnClickListener { amount_field.append(".") }
        var width = 0
        var height = 0
        val vto = llPaymentType.viewTreeObserver
        vto.addOnGlobalLayoutListener(object : ViewTreeObserver.OnGlobalLayoutListener {
            @Override
            override fun onGlobalLayout() {
                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                    llPaymentType.getViewTreeObserver().removeGlobalOnLayoutListener(this)
                } else {
                    llPaymentType.getViewTreeObserver().removeOnGlobalLayoutListener(this)
                }
                if (paymentTypeList != null && paymentTypeList.size > 0) {
                    if (paymentTypeList.size == 1) {
                        width = llPaymentType.measuredWidth
                        height = llPaymentType.measuredHeight
                        recy_payment.adapter = PaymentMethodAdapter(this@EmployeeInvoiceDetailsActivity, width)
                        //  Log.d("Width111"," = "+width)
                    } else if (paymentTypeList.size == 2) {
                        width = llPaymentType.measuredWidth / 2
                        height = llPaymentType.measuredHeight
                        recy_payment.adapter = PaymentMethodAdapter(this@EmployeeInvoiceDetailsActivity, width)
                        //   Log.d("Width122"," = "+width)
                    } else {
                        width = llPaymentType.measuredWidth / 3
                        height = llPaymentType.measuredHeight
                        recy_payment.adapter = PaymentMethodAdapter(this@EmployeeInvoiceDetailsActivity, width)
                        // Log.d("Width33"," = "+width)
                    }
                }

            }
        })
        recy_payment.layoutManager = LinearLayoutManager(this, LinearLayout.HORIZONTAL, false)

        amount_field.setOnClickListener {
            CUC.hideSoftKeyboard(this@EmployeeInvoiceDetailsActivity)
        }
        /* amount_field.addTextChangedListener(object : TextWatcher {
             override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

             }

             override fun onTextChanged(text: CharSequence?, p1: Int, p2: Int, p3: Int) {

                 *//*if (amount_field.text.toString().equals("")) {

                }*//*
            }

            override fun afterTextChanged(textdata: Editable?) {

            }

        })*/


        customDialog.findViewById<TextView>(R.id.t9_key_backspace).setOnClickListener {
            amount_field.setText("")
        }


        tv_cancel.setOnClickListener {
            customDialog.dismiss()
        }
        iv_siganture = customDialog.findViewById(R.id.iv_siganture)

        val onClickPayment = object : onClickPayment {
            override fun callClick(data: GetPaymentTypes) {
                paymentId = "${data.payment_id}"
                bs_payment.setText("${data.payment_method}")
                bs_payment.dismissDropDown()
                bs_payment.clearFocus()
            }
        }


//        if (Store_sign_required != "" && Store_sign_required == "1") {
//            ll_digital_siganture.visibility = View.VISIBLE
//        } else {
//            ll_digital_siganture.visibility = View.GONE
//        }
//
//        iv_siganture.setOnClickListener {
//            openSignatureActivity()
//        }
//        tv_digital_siganture.setOnClickListener {
//            openSignatureActivity()
//        }


        //paymentAdapter = PaymentAdapter(this@SalesCategoryActivity, R.layout.payment_type_item, paymentTypeList, payment_path, onClickPayment)
        //bs_payment.setAdapter(paymentAdapter)
        //tv_customer_name.text = "${selecteQueue.first_name} ${selecteQueue.last_name}"
        tv_total_amount.text = "$ " + "${invoiceData!!.invoice_total_payble!!}"

        try {
            if (paymentTypeList != null && paymentTypeList.size > 0) {
                //if (paymentTypeList.size == 1) {
                paymentId = "${paymentTypeList[0].payment_id}"
                bs_payment.setText("${paymentTypeList[0].payment_method}")
                bs_payment.dismissDropDown()
                bs_payment.clearFocus()
                //}
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
        cv_generate_invoice.setOnClickListener {
          //  paymentId = "1"
       //     Log.d("PaymentId545454"," = "+paymentId)
            if (paymentId == "") {
                CUC.displayToast(this@EmployeeInvoiceDetailsActivity, "0", "Please select payment")
            } else {
                if (amount_field.text.toString() != "") {
                    if (amount_field.text.toString().toFloat() >= "${invoiceData!!.invoice_total_payble!!}".toFloat()) {
                        val newstring =
                                if ("${amount_field.text}".toFloat() >= "${amount_field!!.text}".toFloat()) {
                                    if (Store_sign_required == "1") {
                                        payment_amount = "${amount_field.text}"
                                        change_amount = "${amount_field.text}"
                                        openSignatureActivity()
//                                        if (siganture_path != "") {
//                                            payment_amount = "${amount_field.text}"
//                                            change_amount = "${amount_field.text}"
//                                            callEmployeePaymentInvoice()
//                                        } else {
//                                            CUC.displayToast(this@EmployeeInvoiceDetailsActivity, "0", "Patient signature required")
//                                        }
                                    } else {
                                        siganture_path = ""
                                        payment_amount = "${amount_field.text}"
                                        change_amount = "${amount_field.text}"
                                        callEmployeePaymentInvoice()
                                    }
                                } else {
                                    CUC.displayToast(this@EmployeeInvoiceDetailsActivity, "0", "Please pay complete amount")
                                }
                    } else {
                        CUC.displayToast(this@EmployeeInvoiceDetailsActivity, "0", "Please pay full amount")
                    }
                } else {
                    CUC.displayToast(this@EmployeeInvoiceDetailsActivity, "0", "Please Enter Paid Amount")
                }
            }
        }
    }

    inner class PaymentMethodAdapter(context: Context, widths: Int) : RecyclerView.Adapter<PaymentMethodAdapter.PaymentMethodAdapter>() {
        var row_index = 0
        var width = 0

        init {
            this.width = widths
        }


        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PaymentMethodAdapter {
            val v = LayoutInflater.from(parent.context).inflate(R.layout.raw_payment_method, parent, false)

            return PaymentMethodAdapter(v)

        }

        override fun getItemCount(): Int {
            return paymentTypeList.size
        }


        override fun onBindViewHolder(holder: PaymentMethodAdapter, position: Int) {

            holder.tv_paymetMethod.text = paymentTypeList.get(position).payment_method

            holder.tv_paymetMethod.setOnClickListener {
                paymentId = paymentTypeList.get(position).payment_id
                row_index = position
                notifyDataSetChanged()
            }
            val linearLayout = LinearLayout.LayoutParams(width, LinearLayout.LayoutParams.WRAP_CONTENT)

            //   Log.d("",""+width)
            //  Log.d("SASASASASS",""+width)
            holder.ll_linear.layoutParams = linearLayout
            //   holder.tv_paymetMethod.height=height
            //  holder.tv_paymetMethod.setWidth(100)
            if (row_index == position) {
                holder.tv_paymetMethod.setBackgroundColor(resources.getColor(R.color.blue))
            } else {
                holder.tv_paymetMethod.setBackgroundColor(resources.getColor(R.color.lightgray))
            }
        }

        inner class PaymentMethodAdapter(itemView: View) : RecyclerView.ViewHolder(itemView) {
            val tv_paymetMethod = itemView.tv_paymetMethod
            val ll_linear = itemView.ll_linear


        }


    }

    @SuppressLint("CheckResult")
    private fun openSignatureActivity() {
        rxPermissions!!.request(
                android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .subscribe(object : Consumer<Boolean> {
                    override fun accept(granted: Boolean?) {
                        if (granted!!) { // Always true pre-M
                            // I can control the camera now
                            val siantureIntent = Intent(this@EmployeeInvoiceDetailsActivity, SignatureActivity::class.java)
                            startActivityForResult(siantureIntent, 8989)
                        } else {
                            // Oups permission denied
                            Snackbar.make(coordinator_backroom, resources.getString(R.string.permissionenable),
                                    Snackbar.LENGTH_LONG)
                                    .setActionTextColor(Color.WHITE)
                                    .setAction(resources.getString(R.string.ok), object : View.OnClickListener {
                                        override fun onClick(p0: View?) {
                                            val intent = Intent()
                                            intent.action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
                                            val uri = Uri.fromParts("package", this@EmployeeInvoiceDetailsActivity.packageName, null)
                                            intent.data = uri
                                            startActivity(intent)
                                        }
                                    }).show()
                        }
                    }
                })
    }

    private fun callPaymentReceived() {
        customDialog = Dialog(this@EmployeeInvoiceDetailsActivity, R.style.TransparentProgressDialog)
        val policy = StrictMode.ThreadPolicy.Builder().permitAll().build()
        StrictMode.setThreadPolicy(policy)
        customDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        customDialog.setContentView(R.layout.order_payment_received)
        customDialog.window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)
        customDialog.show()

        val tv_cancel_reson = customDialog.findViewById<EditText>(R.id.tv_cancel_reson)
        val tv_customer_name = customDialog.findViewById<TextView>(R.id.tv_customer_name)
        val tv_order_type = customDialog.findViewById<TextView>(R.id.tv_order_type)

        val textView5 = customDialog.findViewById<TextView>(R.id.textView5)
        val cv_approve_request = customDialog.findViewById<LinearLayout>(R.id.cv_approve_request)
        val cv_cancel = customDialog.findViewById<CardView>(R.id.cv_cancel)
        val ll_cancel_order = customDialog.findViewById<LinearLayout>(R.id.ll_cancel_order)
        val ll_customername = customDialog.findViewById<LinearLayout>(R.id.ll_customername)
        val ll_order_type = customDialog.findViewById<LinearLayout>(R.id.ll_order_type)
        val tv_voidlbl = customDialog.findViewById<TextView>(R.id.tv_voidlbl)
        val tv_voidcancel = customDialog.findViewById<TextView>(R.id.tv_voidcancel)
        if (order_type == "4") {

            if (invoiceOrders != null) {
                if (invoiceOrders!!.invoice_serve_type!! == "2") {
                    tv_order_type.text = "Delivery"

                } else {
                    tv_order_type.text = "Pickup"
                }
                if (invoiceOrders!!.invoice_void!!.isNotEmpty()) {
                    ll_cancel_order.visibility = View.VISIBLE
                    tv_cancel_reson.keyListener = null
                    tv_voidlbl.text = "Order Void Reason"
                    tv_cancel_reson.setText("${invoiceOrders!!.invoice_cancel_reason!!}")
                    cv_approve_request.visibility = View.GONE
                    textView5.text = "Approve Request"
                    tv_cancel_reson.setOnTouchListener(object : View.OnTouchListener {
                        override fun onTouch(view: View?, event: MotionEvent?): Boolean {
                            if (view!!.id == R.id.tv_cancel_reson) {
                                view.parent.requestDisallowInterceptTouchEvent(true)
                                when (event!!.action and MotionEvent.ACTION_MASK) {
                                    MotionEvent.ACTION_UP -> {
                                        view.parent.requestDisallowInterceptTouchEvent(false)
                                    }
                                }
                            }
                            return false
                        }
                    })
                } else {
                    if (invoiceOrders!!.invoice_cancel_request!! == "1") {
                        ll_cancel_order.visibility = View.VISIBLE
                        tv_cancel_reson.keyListener = null
                        tv_voidlbl.text = "Order Void Reason"
                        tv_cancel_reson.setText("${invoiceOrders!!.invoice_cancel_reason!!}")
                        cv_approve_request.visibility = View.GONE
                        textView5.text = "Approve Request"
                        tv_cancel_reson.setOnTouchListener(object : View.OnTouchListener {
                            override fun onTouch(view: View?, event: MotionEvent?): Boolean {
                                if (view!!.id == R.id.tv_cancel_reson) {
                                    view.parent.requestDisallowInterceptTouchEvent(true)
                                    when (event!!.action and MotionEvent.ACTION_MASK) {
                                        MotionEvent.ACTION_UP -> {
                                            view.parent.requestDisallowInterceptTouchEvent(false)
                                        }
                                    }
                                }
                                return false
                            }
                        })
                    } else {
                        ll_cancel_order.visibility = View.GONE
                        textView5.text = "Void Order"
                    }
                }
                tv_customer_name.text = "${invoiceOrders!!.patient_fname!!} ${invoiceOrders!!.patient_lname!!}"
            }
        } else {
            if (invoiceData != null) {
                if (invoiceData!!.invoice_serve_type!! == "2") {
                    tv_order_type.text = "Delivery"

                } else {
                    tv_order_type.text = "Pickup"
                }
                if (invoiceData!!.invoice_void!!.isNotEmpty()) {
                    ll_cancel_order.visibility = View.VISIBLE
                    tv_cancel_reson.keyListener = null
                    tv_voidlbl.text = "Order Void Reason"
                    tv_cancel_reson.setText("${invoiceData!!.invoice_cancel_reason!!}")
                    cv_approve_request.visibility = View.GONE
                    textView5.text = "Approve Request"
                    tv_cancel_reson.setOnTouchListener(object : View.OnTouchListener {
                        override fun onTouch(view: View?, event: MotionEvent?): Boolean {
                            if (view!!.id == R.id.tv_cancel_reson) {
                                view.parent.requestDisallowInterceptTouchEvent(true)
                                when (event!!.action and MotionEvent.ACTION_MASK) {
                                    MotionEvent.ACTION_UP -> {
                                        view.parent.requestDisallowInterceptTouchEvent(false)
                                    }
                                }
                            }
                            return false
                        }
                    })
                } else {
                    if (invoiceData!!.invoice_cancel_request!! == "1") {
                        ll_cancel_order.visibility = View.VISIBLE
                        tv_cancel_reson.keyListener = null
                        tv_voidlbl.text = "Order Void Reason"
                        tv_cancel_reson.setText("${invoiceData!!.invoice_cancel_reason!!}")
                        cv_approve_request.visibility = View.GONE
                        textView5.text = "Approve Request"
                        tv_cancel_reson.setOnTouchListener(object : View.OnTouchListener {
                            override fun onTouch(view: View?, event: MotionEvent?): Boolean {
                                if (view!!.id == R.id.tv_cancel_reson) {
                                    view.parent.requestDisallowInterceptTouchEvent(true)
                                    when (event!!.action and MotionEvent.ACTION_MASK) {
                                        MotionEvent.ACTION_UP -> {
                                            view.parent.requestDisallowInterceptTouchEvent(false)
                                        }
                                    }
                                }
                                return false
                            }
                        })
                    } else {
                        ll_cancel_order.visibility = View.GONE
                        textView5.text = "Void Order"
                    }
                }
                tv_customer_name.text = "${invoiceData!!.patient_fname!!} ${invoiceData!!.patient_lname!!}"
            }
        }


        cv_cancel.setOnClickListener {
            if (tv_cancel_reson.text.isEmpty()) {
                CUC.displayToast(this@EmployeeInvoiceDetailsActivity, "0", "Please enter cancel reason")
            } else {
                cancelReson = "${tv_cancel_reson.text}"
                callEmployeeVoidOrder()
            }
        }

        cv_approve_request.setOnClickListener {
            if (order_type == "4") {
                if (invoiceOrders != null) {
                    if (invoiceOrders!!.invoice_cancel_request!! == "1") {
                    } else {
                        tv_voidlbl.text = "Order Void Reason"
                        tv_voidcancel.text = "Submit"
                        ll_customername.visibility = View.GONE
                        ll_cancel_order.visibility = View.VISIBLE
                        cv_approve_request.visibility = View.GONE
                        ll_order_type.visibility = View.GONE
                        cv_cancel.visibility = View.VISIBLE
                    }
                }
            } else {
                if (invoiceData != null) {
                    if (invoiceData!!.invoice_cancel_request!! == "1") {
                    } else {
                        tv_voidlbl.text = "Order Void Reason"
                        tv_voidcancel.text = "Submit"
                        ll_customername.visibility = View.GONE
                        ll_cancel_order.visibility = View.VISIBLE
                        cv_approve_request.visibility = View.GONE
                        ll_order_type.visibility = View.GONE
                        cv_cancel.visibility = View.VISIBLE
                    }
                }
            }

        }
    }

    private fun callEmployeeApplyStmanDiscount() {
        if (CUC.isNetworkAvailablewithPopup(this@EmployeeInvoiceDetailsActivity)) {
            mDialog = CUC.createDialog(this@EmployeeInvoiceDetailsActivity)
            val apiService = RequetsInterface.create()

            val parameterMap = HashMap<String, String>()
            parameterMap["api_key"] = "${Preferences.getPreference(this@EmployeeInvoiceDetailsActivity, "API_KEY")}"
            if (logindata != null) {
                // parameterMap["store_id"] = "${logindata.user_store_id}"
                parameterMap["employee_id"] = "${logindata.user_id}"
            }
            parameterMap["invoice_id"] = "$mainCartId"
            parameterMap["terminal_id"] = "${appSettingData.terminal_id}"
            parameterMap["discount_type"] = "$discount_type"
            parameterMap["discount_value"] = "$discount_value"
            parameterMap["employee_pin"] = "$pin_value"
            parameterMap["emp_lat"] = "${Preferences.getPreference(this@EmployeeInvoiceDetailsActivity, "latitude")}"
            parameterMap["emp_log"] = "${Preferences.getPreference(this@EmployeeInvoiceDetailsActivity, "longitude")}"
            Log.i("Result", "parameterMap : $parameterMap")
            CompositeDisposable().add(apiService.callEmployeeApplyStmanDiscount(parameterMap)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe({ result ->
                        if (mDialog != null && mDialog.isShowing)
                            mDialog.dismiss()
                        Log.i("Result", "" + result.toString())
                        if (result != null) {
                            if (result.status == "0") {
                                dialogProductQty.dismiss()
                                callEmployeePatientCart()
                                CUC.displayToast(this@EmployeeInvoiceDetailsActivity, result.show_status, result.message)
                            } else if (result.status == "10") {
                                apiClass.callApiKey(EMPLOYEEAPPLYSTMANDISCOUNT)
                            } else {
                                CUC.displayToast(this@EmployeeInvoiceDetailsActivity, result.show_status, result.message)
                            }
                        } else {
                            // CUC.displayToast(this@EmployeeInvoiceDetailsActivity, "0", getString(R.string.connection_to_database_failed))
                        }
                    }, { error ->
                        if (mDialog != null && mDialog.isShowing)
                            mDialog.dismiss()
                        CUC.displayToast(this@EmployeeInvoiceDetailsActivity, "0", getString(R.string.connection_to_database_failed))
                        error.printStackTrace()
                    })
            )
        }
    }

    private fun callEmployeeRemoveStmanDiscount() {
        if (CUC.isNetworkAvailablewithPopup(this@EmployeeInvoiceDetailsActivity)) {
            mDialog = CUC.createDialog(this@EmployeeInvoiceDetailsActivity)
            val apiService = RequetsInterface.create()
            val parameterMap = HashMap<String, String>()
            parameterMap["api_key"] = "${Preferences.getPreference(this@EmployeeInvoiceDetailsActivity, "API_KEY")}"
            if (logindata != null) {
                parameterMap["employee_id"] = "${logindata.user_id}"
            }
            parameterMap["invoice_id"] = "$mainCartId"
            parameterMap["terminal_id"] = "${appSettingData.terminal_id}"
            parameterMap["emp_lat"] = "${Preferences.getPreference(this@EmployeeInvoiceDetailsActivity, "latitude")}"
            parameterMap["emp_log"] = "${Preferences.getPreference(this@EmployeeInvoiceDetailsActivity, "longitude")}"
            parameterMap["employee_pin"] = "$pin_value"
            Log.i("Result", "parameterMap : $parameterMap")
            CompositeDisposable().add(apiService.callEmployeeRemoveStmanDiscount(parameterMap)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe({ result ->
                        if (mDialog != null && mDialog.isShowing)
                            mDialog.dismiss()
                        Log.i("Result", "" + result.toString())
                        if (result != null) {
                            if (result.status == "0") {
                                //dialogChangePin.dismiss()
                                discount_value = "0"
                                pin_value = ""
                                dialogProductQty.dismiss()
                                callEmployeePatientCart()
                                CUC.displayToast(this@EmployeeInvoiceDetailsActivity, result.show_status, result.message)
                            } else if (result.status == "10") {
                                apiClass.callApiKey(EMPLOYEEREMOVESTMANDISCOUNT)
                            } else {
                                CUC.displayToast(this@EmployeeInvoiceDetailsActivity, result.show_status, result.message)
                            }
                        } else {
                            //  CUC.displayToast(this@EmployeeInvoiceDetailsActivity, "0", getString(R.string.connection_to_database_failed))
                        }
                    }, { error ->
                        if (mDialog != null && mDialog.isShowing)
                            mDialog.dismiss()
                        CUC.displayToast(this@EmployeeInvoiceDetailsActivity, "0", getString(R.string.connection_to_database_failed))
                        error.printStackTrace()
                    })
            )
        }
    }

    private fun callEmployeePatientCart() {
        if (CUC.isNetworkAvailablewithPopup(this@EmployeeInvoiceDetailsActivity)) {
            mDialog = CUC.createDialog(this@EmployeeInvoiceDetailsActivity)
            val apiService = RequetsInterface.Factory.create()
            val parameterMap = HashMap<String, String>()
            parameterMap["api_key"] = "${Preferences.getPreference(this@EmployeeInvoiceDetailsActivity, "API_KEY")}"
            parameterMap["invoice_id"] = "$mainCartId"
            Log.i("Result", "parameterMap : $parameterMap")
            CompositeDisposable().add(apiService.callEmployeeInvoiceDetails(parameterMap)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe({ result ->
                        if (mDialog != null && mDialog.isShowing)
                            mDialog.dismiss()
                        if (result != null) {
                            Log.i("Result", "" + result.toString())
                            Log.d("AAAAAA", "" + result.invoice.invoice_manual_discount_type)
                            if (result.status == "0") {
                                payment_path = result.payment_path
                                Store_sign_required = "${result.Store_sign_required}"
                                //cart_serve_type = "${result.main_cart.cart_serve_type}"
                                if (result.invoice != null) {
                                    discount_value = result.invoice.invoice_manual_discount
                                    cart_manual_discount_type = result.invoice.invoice_manual_discount_type
                                }
                                /*  if (!intent!!.hasExtra("status")) {
                                      Log.d("AAAAAA11",""+result.invoice.invoice_manual_discount_type)
                                  } else*/// {
                                Log.d("AAAAAA22", "" + result.invoice.invoice_manual_discount_type)
                                if (order_type == "1") {
                                    Log.d("AAAAAA33", "" + result.invoice.invoice_manual_discount_type)
                                    if (logindata != null) {
                                        if (logindata.user_role == "2") {
                                            ll_redeem_option.visibility = View.GONE
                                        } else {
                                            ll_redeem_option.visibility = View.VISIBLE
                                        }
                                    } else {
                                        ll_redeem_option.visibility = View.GONE
                                    }

                                    ll_promoCode.visibility = View.VISIBLE
                                    if (result.invoice.invoice_promo_code != "") {
                                        ll_promoCode.setBackgroundResource(R.color.blue)
                                        tv_promoCode.text = "Remove Promo Code"
                                    } else {
                                        ll_promoCode.setBackgroundResource(R.color.blue)
                                        tv_promoCode.text = "Apply Promo Code"
                                    }

                                    tv_promoCode.setOnClickListener {
                                        if (tv_promoCode.text == "Remove Promo Code") {
                                            val builder = AlertDialog.Builder(this@EmployeeInvoiceDetailsActivity)
                                            builder.setMessage(getString(R.string.are_you_sure_you_want_to_remove_promo_code))

                                            builder.setPositiveButton("Yes") { dialog, which ->
                                                dialog.dismiss()
                                                callEmployeeRemovePromocode()
                                            }
                                            builder.setNegativeButton("No") { dialog, which -> dialog.dismiss() }
                                            val alert = builder.create()
                                            alert.show()
                                        } else {
                                            val mDialog = PromoCodeDialog(this@EmployeeInvoiceDetailsActivity, object : PromoCodeDialog.onCallListner {
                                                override fun onRefresh() {
                                                    callEmployeePatientCart()
                                                }
                                            })
                                            val myBundle = Bundle()
                                            myBundle.putString("cart_id", mainCartId)
                                            myBundle.putString("patient_id", patient_id)
                                            myBundle.putString("app_status", "2")

                                            mDialog.arguments = myBundle
                                            mDialog.show(fragmentManager, "promo_code")
                                        }
                                    }
                                    Log.d("CartDiscountTypeAAAA", "AASAAAAA")
                                    Log.d("CartDiscountType", " = " + cart_manual_discount_type)
                                    if (cart_manual_discount_type == "1" || cart_manual_discount_type == "2") {

                                        tv_applyremove.text = "Remove Discount"
                                        Log.d("Remove", " = ")
                                        Log.d("RemoveDis", " = ")
                                    } else {
                                        tv_applyremove.text = "Apply Discount"
                                        Log.d("Apply", " = ")
                                    }
                                }
                                //    }

                                if (order_type == "2") {
                                    if (cart_manual_discount_type == "1" || cart_manual_discount_type == "2") {

                                        tv_applyremove.text = "Remove Discount"
                                        Log.d("Remove", " = ")
                                        Log.d("RemoveDis", " = ")
                                    } else {
                                        tv_applyremove.text = "Apply Discount"
                                        Log.d("Apply", " = ")
                                    }
                                }
                                taxList.clear()
                                tv_total_qty.text = "$invoice_total_qty"
                                tv_subtotal.text = "$Currency_value$invoice_sub_total"
                                if (result.invoice.invoice_taxes != null && result.invoice.invoice_taxes.isNotEmpty()) {
                                    taxList = Gson().fromJson(result.invoice.invoice_taxes,
                                            object : TypeToken<ArrayList<GetTaxData>>() {

                                            }.type) as ArrayList<GetTaxData>
                                }
                                try {
                                    if (loyalty_amount != null && loyalty_amount != "") {
                                        if ("$loyalty_amount".toFloat() > 0) {
                                            taxList.add(GetTaxData("989", "Loyalty Amount", loyalty_amount))
                                        }
                                    }
                                } catch (e: Exception) {
                                    e.printStackTrace()
                                }
                                try {
                                    if (discount_value != null && discount_value != "") {
                                        if ("$discount_value".toFloat() > 0) {
                                            if (cart_manual_discount_type == "2") {
                                                taxList.add(GetTaxData("2", "Discount", discount_value, "2"))
                                            } else {
                                                taxList.add(GetTaxData("2", "Discount", discount_value, "1"))
                                            }
                                        }
                                    }
                                } catch (e: Exception) {
                                    e.printStackTrace()
                                }
                                if (result.invoice.invoice_promo_code != "") {
                                    taxList.add(GetTaxData("101", "Promo Code (${result.invoice.invoice_promo_code})", result.invoice.invoice_promo_discount))
                                }
                                taxList.add(GetTaxData("102", "Grand Total", result.invoice.invoice_total_payble, "1"))
                                if (order_type == "3" || order_type == "4") {
                                    taxList.add(GetTaxData("103", "Paid Amount", result.invoice.invoice_paid_amount))
                                    taxList.add(GetTaxData("104", "Tender Cash", result.invoice.invoice_change))
                                    if (cart_manual_discount_type == "1" || cart_manual_discount_type == "2") {

                                        tv_applyremove.text = "Remove Discount"
                                        Log.d("Remove", " = ")
                                        Log.d("RemoveDis", " = ")
                                    } else {
                                        tv_applyremove.text = "Apply Discount"
                                        Log.d("Apply", " = ")
                                    }
                                }
                                taxItemAdapter.notifyDataSetChanged()
                                if (result.invoice_detail != null && result.invoice_detail.size > 0) {
                                    list.clear()
                                    list.addAll(result.invoice_detail)
                                    productAdapter.notifyDataSetChanged()

                                    driverList.clear()
                                    driverList.addAll(result.drivers)

                                    paymentTypeList.clear()
                                    paymentTypeList.addAll(result.payment_types)
                                    Log.i(EmployeeInvoiceDetailsActivity::class.java.simpleName, "paymentTypeList : $paymentTypeList")
                                    Log.i("Result", "$driverList")
                                } else {
                                    list.clear()
                                    productAdapter.notifyDataSetChanged()
                                    taxList.clear()
                                    taxItemAdapter.notifyDataSetChanged()
                                }
                                //Preferences.setPreference(this@EmployeeInvoiceDetailsActivity, "total_qty", result.total_qty)
                                CUC.displayToast(this@EmployeeInvoiceDetailsActivity, result.show_status, result.message)
                            } else if (result.status == "10") {
                                apiClass.callApiKey(EMPLOYEEPATIENTCART)
                            } else {
                                list.clear()
                                productAdapter.notifyDataSetChanged()
                                taxList.clear()
                                taxItemAdapter.notifyDataSetChanged()
                                CUC.displayToast(this@EmployeeInvoiceDetailsActivity, result.show_status, result.message)
                            }

                        } else {
                            //   CUC.displayToast(this@EmployeeInvoiceDetailsActivity, "0", getString(R.string.connection_to_database_failed))
                        }

                    }, { error ->
                        if (mDialog != null && mDialog.isShowing)
                            mDialog.dismiss()
                        CUC.displayToast(this@EmployeeInvoiceDetailsActivity, "0", getString(R.string.connection_to_database_failed))
                        error.printStackTrace()
                    })
            )
        }
    }

    private fun callEmployeeGenerateInvoice() {
        mDialog = CUC.createDialog(this@EmployeeInvoiceDetailsActivity)
        val apiService = RequetsInterface.Factory.create()
        val parameterMap = HashMap<String, String>()
        parameterMap["api_key"] = "${Preferences.getPreference(this@EmployeeInvoiceDetailsActivity, "API_KEY")}"
        parameterMap["invoice_id"] = "$mainCartId"
        //parameterMap["cart_payment_id"] = "$paymentId"
        if (logindata != null) {
            parameterMap["employee_id"] = "${logindata.user_id}"
        }
        parameterMap["driver_id"] = "$driver_id"
        parameterMap["terminal_id"] = "${appSettingData.terminal_id}"
        parameterMap["emp_lat"] = "${Preferences.getPreference(this@EmployeeInvoiceDetailsActivity, "latitude")}"
        parameterMap["emp_log"] = "${Preferences.getPreference(this@EmployeeInvoiceDetailsActivity, "longitude")}"
        Log.i("Result", "parameterMap : $parameterMap")
        CompositeDisposable().add(apiService.callEmployeeGenerateInvoice(parameterMap)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe({ result ->
                    if (mDialog != null && mDialog.isShowing)
                        mDialog.dismiss()
                    if (result != null) {
                        Log.i("Result", "result : $result")
                        if (result.status == "0") {
                            if (customDialog!!.isShowing) {
                                customDialog!!.dismiss()
                            }
                            CUC.displayToast(this@EmployeeInvoiceDetailsActivity, result.show_status, result.message)
                            val invoiceStr = Gson().toJson(result)
                            val myIntent = Intent(this@EmployeeInvoiceDetailsActivity, BackRoomReceiptPrintActivity::class.java)
                            myIntent.putExtra("all_data", "$all_data")
                            myIntent.putExtra("invoiceStr", "$invoiceStr")
                            myIntent.putExtra("order_type", "$order_type")
                            startActivityForResult(myIntent, 7070)
                            setResult(Activity.RESULT_FIRST_USER)
                            finish()
                        } else if (result.status == "10") {
                            apiClass.callApiKey(EMPLOYEEGENERATEINVOICE)
                        } else {
                            CUC.displayToast(this@EmployeeInvoiceDetailsActivity, result.show_status, result.message)
                        }
                    } else {
                        // CUC.displayToast(this@EmployeeInvoiceDetailsActivity, "0", getString(R.string.connection_to_database_failed))
                    }
                }, { error ->
                    if (mDialog != null && mDialog.isShowing)
                        mDialog.dismiss()
                    CUC.displayToast(this@EmployeeInvoiceDetailsActivity, "0", getString(R.string.connection_to_database_failed))
                    error.printStackTrace()
                })
        )
    }


    fun callEmployeeRemovePromocode() {
        if (CUC.isNetworkAvailablewithPopup(this@EmployeeInvoiceDetailsActivity)) {
            mDialog = CUC.createDialog(this@EmployeeInvoiceDetailsActivity)
            val apiService = RequetsInterface.create()
            val parameterMap = HashMap<String, String>()
            parameterMap["api_key"] = Preferences.getPreference(this@EmployeeInvoiceDetailsActivity, "API_KEY")
            parameterMap["invoice_id"] = mainCartId
            if (logindata != null) {
                parameterMap["employee_id"] = logindata!!.user_id
            }
            if (appSettingData != null) {
                parameterMap["terminal_id"] = "${appSettingData!!.terminal_id}"
            }
            parameterMap["employee_lat"] = "${Preferences.getPreference(this@EmployeeInvoiceDetailsActivity, "latitude")}"
            parameterMap["employee_log"] = "${Preferences.getPreference(this@EmployeeInvoiceDetailsActivity, "longitude")}"
            Log.i("Result", "parameterMap : $parameterMap")
            CompositeDisposable().add(apiService.callEmployeeRemovePromocode(parameterMap)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe({ result ->
                        if (mDialog != null && mDialog!!.isShowing)
                            mDialog!!.dismiss()
                        if (result != null) {
                            Log.i("Result", "" + result.toString())
                            if (result.status == "0") {
                                ll_promoCode.setBackgroundResource(R.color.colorPrimary)
                                tv_promoCode.text = "Have A Promo Code?"
                                callEmployeePatientCart()
                            } else if (result.status == "10") {
                                //callApiKey()
                            } else {
                                CUC.displayToast(this@EmployeeInvoiceDetailsActivity, result.show_status, result.message)
                            }
                        } else {
                            //  CUC.displayToast(this@EmployeeInvoiceDetailsActivity, "0", getString(R.string.connection_to_database_failed))
                        }
                    }, { error ->
                        if (mDialog != null && mDialog!!.isShowing)
                            mDialog!!.dismiss()
                        CUC.displayToast(this@EmployeeInvoiceDetailsActivity, "0", getString(R.string.connection_to_database_failed))
                        error.printStackTrace()
                    })
            )
        }
    }


    private fun callEmployeeDriverReassign() {
        mDialog = CUC.createDialog(this@EmployeeInvoiceDetailsActivity)
        val apiService = RequetsInterface.Factory.create()
        val parameterMap = HashMap<String, String>()
        parameterMap["api_key"] = "${Preferences.getPreference(this@EmployeeInvoiceDetailsActivity, "API_KEY")}"
        parameterMap["invoice_id"] = "$mainCartId"
        if (logindata != null) {
            parameterMap["employee_id"] = "${logindata.user_id}"
        }
        parameterMap["driver_id"] = "$driver_id"
        parameterMap["terminal_id"] = "${appSettingData.terminal_id}"
        parameterMap["emp_lat"] = "${Preferences.getPreference(this@EmployeeInvoiceDetailsActivity, "latitude")}"
        parameterMap["emp_log"] = "${Preferences.getPreference(this@EmployeeInvoiceDetailsActivity, "longitude")}"
        Log.i("Result", "parameterMap : $parameterMap")
        CompositeDisposable().add(apiService.callEmployeeDriverReassign(parameterMap)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe({ result ->
                    if (mDialog != null && mDialog.isShowing)
                        mDialog.dismiss()
                    if (result != null) {
                        Log.i("Result", "result : $result")
                        if (result.status == "0") {
                            if (customDialog!!.isShowing) {
                                customDialog!!.dismiss()
                            }
                            if (invoiceData != null) {
                                invoiceData!!.invoice_delivery_driver = "$driver_id"
                            }

                            iv_driver.visibility = View.GONE
                            status_driver_reaggine = true
                            if (driver_reaggine == "1") {
                                callInvoiceGenerated()
                            }
                            CUC.displayToast(this@EmployeeInvoiceDetailsActivity, result.show_status, result.message)
                        } else if (result.status == "10") {
                            apiClass.callApiKey(EMPLOYEEDRIVERREASSIGN)
                        } else {
                            CUC.displayToast(this@EmployeeInvoiceDetailsActivity, result.show_status, result.message)
                        }
                    } else {
                        // CUC.displayToast(this@EmployeeInvoiceDetailsActivity, "0", getString(R.string.connection_to_database_failed))
                    }
                }, { error ->
                    if (mDialog != null && mDialog.isShowing)
                        mDialog.dismiss()
                    CUC.displayToast(this@EmployeeInvoiceDetailsActivity, "0", getString(R.string.connection_to_database_failed))
                    error.printStackTrace()
                })
        )
    }

    private fun callEmployeePaymentInvoice() {
        mDialog = CUC.createDialog(this@EmployeeInvoiceDetailsActivity)
        val apiService = RequetsInterface.Factory.create()
        val parameterMap = java.util.HashMap<String, RequestBody>()
        parameterMap["api_key"] = RequestBody.create(MediaType.parse("text/plain"), "${Preferences.getPreference(this@EmployeeInvoiceDetailsActivity, "API_KEY")}")
        parameterMap["invoice_id"] = RequestBody.create(MediaType.parse("text/plain"), "$mainCartId")
        if (logindata != null) {
            parameterMap["employee_id"] = RequestBody.create(MediaType.parse("text/plain"), "${logindata.user_id}")
        }
        parameterMap["payment_id"] = RequestBody.create(MediaType.parse("text/plain"), "$paymentId")
        parameterMap["driver_id"] = RequestBody.create(MediaType.parse("text/plain"), "$driver_id")
        parameterMap["terminal_id"] = RequestBody.create(MediaType.parse("text/plain"), "${appSettingData.terminal_id}")
        parameterMap["emp_lat"] = RequestBody.create(MediaType.parse("text/plain"), "${Preferences.getPreference(this@EmployeeInvoiceDetailsActivity, "latitude")}")
        parameterMap["emp_log"] = RequestBody.create(MediaType.parse("text/plain"), "${Preferences.getPreference(this@EmployeeInvoiceDetailsActivity, "longitude")}")
        parameterMap["payment_amount"] = RequestBody.create(MediaType.parse("text/plain"), "$change_amount")
        val changeAmount = change_amount.toFloat() - payment_amount.toFloat()
        parameterMap["change_amount"] = RequestBody.create(MediaType.parse("text/plain"), "$changeAmount")
        var disposable: Observable<GetEmployeeQenerateInvoice>
        if (Store_sign_required == "1") {
            val sigantureFile = File(siganture_path)
            disposable = apiService.callEmployeePaymentInvoice(parameterMap,
                    MultipartBody.Part.createFormData("digital_siganture", "${sigantureFile.name}",
                            RequestBody.create(MediaType.parse("image*//*"), sigantureFile)),
                    RequestBody.create(MediaType.parse("text/plain"), "${sigantureFile.name}"))
        } else {
            disposable = apiService.callEmployeePaymentInvoice(parameterMap)
        }
        Log.i("Result", "parameterMap : $parameterMap")




        CompositeDisposable().add(disposable
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe({ result ->
                    if (mDialog != null && mDialog.isShowing)
                        mDialog.dismiss()
                    if (result != null) {
                        Log.i("Result", "result : $result")
                        if (result.status == "0") {
                            if (customDialog!!.isShowing) {
                                customDialog!!.dismiss()
                            }
                            CUC.displayToast(this@EmployeeInvoiceDetailsActivity, result.show_status, result.message)
                            val invoiceStr = Gson().toJson(result)
                            val myIntent = Intent(this@EmployeeInvoiceDetailsActivity, BackRoomReceiptPrintActivity::class.java)
                            myIntent.putExtra("all_data", "$all_data")
                            myIntent.putExtra("invoiceStr", "$invoiceStr")
                            myIntent.putExtra("order_type", "$order_type")
                            startActivityForResult(myIntent, 7070)
                            setResult(Activity.RESULT_FIRST_USER)
                            finish()
                        } else if (result.status == "10") {
                            apiClass.callApiKey(EMPLOYEEPAYMENTINVOICE)
                        } else {
                            CUC.displayToast(this@EmployeeInvoiceDetailsActivity, result.show_status, result.message)
                        }
                    } else {
                        // CUC.displayToast(this@EmployeeInvoiceDetailsActivity, "0", getString(R.string.connection_to_database_failed))
                    }
                }, { error ->
                    if (mDialog != null && mDialog.isShowing)
                        mDialog.dismiss()
                    CUC.displayToast(this@EmployeeInvoiceDetailsActivity, "0", getString(R.string.connection_to_database_failed))
                    error.printStackTrace()
                })
        )
    }

    private fun callEmployeeCancelApprove() {
        mDialog = CUC.createDialog(this@EmployeeInvoiceDetailsActivity)
        val apiService = RequetsInterface.create()
        val parameterMap = HashMap<String, String>()
        parameterMap["api_key"] = "${Preferences.getPreference(this@EmployeeInvoiceDetailsActivity, "API_KEY")}"
        parameterMap["invoice_id"] = "$mainCartId"
        if (logindata != null)
            parameterMap["employee_id"] = "${logindata.user_id}"
        parameterMap["terminal_id"] = "${appSettingData.terminal_id}"
        parameterMap["employee_lat"] = "${Preferences.getPreference(this@EmployeeInvoiceDetailsActivity, "latitude")}"
        parameterMap["employee_log"] = "${Preferences.getPreference(this@EmployeeInvoiceDetailsActivity, "longitude")}"
        Log.i("Result", "parameterMap : $parameterMap")
        CompositeDisposable().add(apiService.callEmployeeCancelApprove(parameterMap)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe({ result ->
                    if (mDialog != null && mDialog.isShowing)
                        mDialog.dismiss()
                    if (result != null) {
                        Log.i("Result", "result : $result")
                        if (result.status == "0") {
                            if (customDialog != null && customDialog.isShowing)
                                customDialog.dismiss()
                            setResult(Activity.RESULT_OK)
                            finish()
                        } else if (result.status == "10") {
                            apiClass.callApiKey(EMPLOYEECANCELAPPROVE)
                        } else {
                            CUC.displayToast(this@EmployeeInvoiceDetailsActivity, result.show_status, result.message)
                        }
                    } else {
                        // CUC.displayToast(this@EmployeeInvoiceDetailsActivity, "0", getString(R.string.connection_to_database_failed))
                    }
                }, { error ->
                    if (mDialog != null && mDialog.isShowing)
                        mDialog.dismiss()
                    CUC.displayToast(this@EmployeeInvoiceDetailsActivity, "0", getString(R.string.connection_to_database_failed))
                    error.printStackTrace()
                })
        )
    }

    private fun callEmployeeVoidOrder() {
        mDialog = CUC.createDialog(this@EmployeeInvoiceDetailsActivity)
        val apiService = RequetsInterface.Factory.create()
        val parameterMap = HashMap<String, String>()
        parameterMap["api_key"] = "${Preferences.getPreference(this@EmployeeInvoiceDetailsActivity, "API_KEY")}"
        parameterMap["invoice_id"] = "$mainCartId"
        if (logindata != null)
            parameterMap["employee_id"] = "${logindata.user_id}"
        parameterMap["terminal_id"] = "${appSettingData.terminal_id}"
        parameterMap["employee_lat"] = "${Preferences.getPreference(this@EmployeeInvoiceDetailsActivity, "latitude")}"
        parameterMap["employee_log"] = "${Preferences.getPreference(this@EmployeeInvoiceDetailsActivity, "longitude")}"
        parameterMap["cancel_reason"] = "$cancelReson"

        Log.i("Result", "parameterMap : $parameterMap")
        CompositeDisposable().add(apiService.callEmployeeVoidOrder(parameterMap)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe({ result ->
                    if (mDialog != null && mDialog.isShowing)
                        mDialog.dismiss()
                    if (result != null) {
                        Log.i("Result", "result : $result")
                        if (result.status == "0") {
                            if (customDialog != null && customDialog.isShowing)
                                customDialog.dismiss()
                            val changeI = Intent()
                            changeI.putExtra("message", "${result.message}")
                            setResult(Activity.RESULT_FIRST_USER, changeI)
                            finish()
                        } else if (result.status == "10") {
                            apiClass.callApiKey(EMPLOYEEVOIDORDER)
                        } else {
                            CUC.displayToast(this@EmployeeInvoiceDetailsActivity, result.show_status, result.message)
                        }
                    } else {
                        //  CUC.displayToast(this@EmployeeInvoiceDetailsActivity, "0", getString(R.string.connection_to_database_failed))
                    }
                }, { error ->
                    if (mDialog != null && mDialog.isShowing)
                        mDialog.dismiss()
                    CUC.displayToast(this@EmployeeInvoiceDetailsActivity, "0", getString(R.string.connection_to_database_failed))
                    error.printStackTrace()
                })
        )
    }

    private fun callEmployeeCancelOrder() {
        mDialog = CUC.createDialog(this@EmployeeInvoiceDetailsActivity)
        val apiService = RequetsInterface.Factory.create()
        val parameterMap = HashMap<String, String>()
        parameterMap["api_key"] = "${Preferences.getPreference(this@EmployeeInvoiceDetailsActivity, "API_KEY")}"
        parameterMap["invoice_id"] = "$mainCartId"
        if (logindata != null)
            parameterMap["employee_id"] = "${logindata.user_id}"
        parameterMap["terminal_id"] = "${appSettingData.terminal_id}"

        parameterMap["employee_lat"] = "${Preferences.getPreference(this@EmployeeInvoiceDetailsActivity, "latitude")}"
        parameterMap["employee_log"] = "${Preferences.getPreference(this@EmployeeInvoiceDetailsActivity, "longitude")}"

        parameterMap["cancel_reason"] = "$cancelReson"

        Log.i("Result", "parameterMap : $parameterMap")
        CompositeDisposable().add(apiService.callEmployeeCancelOrder(parameterMap)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe({ result ->
                    if (mDialog != null && mDialog.isShowing)
                        mDialog.dismiss()
                    if (result != null) {
                        Log.i("Result", "result : $result")
                        if (result.status == "0") {
                            if (customDialog != null && customDialog.isShowing)
                                customDialog.dismiss()
                            setResult(Activity.RESULT_OK)
                            finish()
                        } else if (result.status == "10") {
                            apiClass.callApiKey(EMPLOYEECANCELORDER)
                        } else {
                            CUC.displayToast(this@EmployeeInvoiceDetailsActivity, result.show_status, result.message)
                        }
                    } else {
                        //CUC.displayToast(this@EmployeeInvoiceDetailsActivity, "0", getString(R.string.connection_to_database_failed))
                    }
                }, { error ->
                    if (mDialog != null && mDialog.isShowing)
                        mDialog.dismiss()
                    CUC.displayToast(this@EmployeeInvoiceDetailsActivity, "0", getString(R.string.connection_to_database_failed))
                    error.printStackTrace()
                })
        )
    }

    class PaymentTypeAdapter(val list: ArrayList<GetPaymentTypes>, val context: Context, val onClick: onPaymentClickListener, val payment_path: String) : RecyclerView.Adapter<PaymentTypeAdapter.ViewHolder>() {

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            holder!!.bindItems(list[position], context, onClick, payment_path)
        }

        override fun getItemCount(): Int {
            return list.size
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            val v = LayoutInflater.from(parent!!.context).inflate(R.layout.payment_type_item, parent, false)
            return ViewHolder(v)
        }

        class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
            val tv_payment: TextView = itemView.findViewById(R.id.tv_payment)
            val iv_payment: ImageView = itemView.findViewById(R.id.iv_payment)

            fun bindItems(data: GetPaymentTypes, context: Context, onClick: onPaymentClickListener, payment_path: String) {
                tv_payment.text = "${data.payment_method}"

                // set zoomable tag on views that is to be zoomed
                Glide.with(context!!)
                        .applyDefaultRequestOptions(RequestOptions().format(DecodeFormat.PREFER_ARGB_8888).error(R.drawable.no_image_available))
                        .load("$payment_path${data.payment_logo}")
                        .into(iv_payment)
                iv_payment.setOnClickListener {
                    onClick.onPaymentClick(data)
                }
            }
        }
    }


    interface onPaymentClickListener {
        fun onPaymentClick(data: GetPaymentTypes)
    }

    class ProductAdapter(val list: ArrayList<GetInvoiceDetail>, val context: Context, val Currency_value: String, val onClick: onClickListener) : RecyclerView.Adapter<ProductAdapter.ViewHolder>() {

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            holder!!.bindItems(list[position], context, Currency_value, onClick)
        }

        override fun getItemCount(): Int {
            return list.size
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            val v = LayoutInflater.from(parent!!.context).inflate(R.layout.employee_cart_item, parent, false)
            return ViewHolder(v)
        }

        class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
            val tv_name: TextView = itemView.findViewById(R.id.tv_name)
            val tv_mobilenumber: TextView = itemView.findViewById(R.id.tv_mobilenumber)
            val tv_phonenumber: TextView = itemView.findViewById(R.id.tv_phonenumber)
            val layout_void: LinearLayout = itemView.findViewById(R.id.layout_void)

            fun bindItems(data: GetInvoiceDetail, context: Context, Currency_value: String, onClick: onClickListener) {
                tv_name.text = "${data.product_name}"
                tv_mobilenumber.text = "${data.invoice_detail_qty} ${data.unit_short_name}"
                tv_phonenumber.text = "$Currency_value${data.invoice_detail_total_price}"
                //tv_patienttype.text = "${data.type_name}"
                layout_void.setOnClickListener {
                    onClick.voidclick(data)
                }

            }

        }
    }


    interface onClickListener {
        fun deleteClick(data: GetCartData)
        fun voidclick(data: GetInvoiceDetail)
    }

    class TaxItemAdapter(val context: Context, val Currency_value: String) : RecyclerView.Adapter<TaxItemAdapter.ViewHolder>() {

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            holder!!.bindItems(taxList[position], context, Currency_value)
        }

        override fun getItemCount(): Int {
            return taxList.size
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            val v = LayoutInflater.from(parent!!.context).inflate(R.layout.tax_data_item, parent, false)
            return ViewHolder(v)
        }

        class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
            val tv_tax_title: TextView = itemView.findViewById(R.id.tv_tax_title)
            val tv_tax_amount: TextView = itemView.findViewById(R.id.tv_tax_amount)

            fun bindItems(data: GetTaxData, context: Context, Currency_value: String) {
                tv_tax_title.text = "${data.tax_name}"
                //v_tax_amount.text = "$Currency_value${data.tax_total}"
                if (data.tax_status == "2") {
                    tv_tax_amount.text = "-${data.tax_total}%"
                } else {
                    if ("${data.tax_name}" == "Loyalty Amount") {
                        tv_tax_amount.text = "-$Currency_value${data.tax_total}"
                    } else if ("${data.tax_name}" == "Discount") {
                        tv_tax_amount.text = "-$Currency_value${data.tax_total}"
                    } else {
                        tv_tax_amount.text = "$Currency_value${data.tax_total}"
                    }
                }
            }
        }
    }

    class DriverAdapter(context: Context?, val resource: Int, val companyList: ArrayList<GetDrivers>, val clickListner: onClickDriver) : ArrayAdapter<GetDrivers>(context, resource, companyList) {

        override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
            var rootView = convertView
            if (rootView == null)
                rootView = LayoutInflater.from(context!!).inflate(resource, null, false)

            val compnayText = rootView!!.findViewById<TextView>(R.id.textView)

            val dataCompany = companyList[position]
            compnayText.text = "${dataCompany.emp_first_name} ${dataCompany.emp_last_name}"
            compnayText.setOnClickListener {
                clickListner.callClick(dataCompany)
            }
            return rootView!!
        }
    }

    interface onClickDriver {
        fun callClick(data: GetDrivers)
    }

    class PaymentAdapter(context: Context?, val resource: Int, val companyList: ArrayList<GetPaymentTypes>, val payment_path: String, val clickListner: onClickPayment) : ArrayAdapter<GetPaymentTypes>(context, resource, companyList) {

        override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
            var rootView = convertView
            if (rootView == null)
                rootView = LayoutInflater.from(context!!).inflate(resource, null, false)

            val ll_ = rootView!!.findViewById<RelativeLayout>(R.id.ll_)
            val tv_payment = rootView!!.findViewById<TextView>(R.id.tv_payment)
            val iv_payment = rootView!!.findViewById<ImageView>(R.id.iv_payment)
            val data = companyList[position]
            Glide.with(context!!)
                    .applyDefaultRequestOptions(RequestOptions().format(DecodeFormat.PREFER_ARGB_8888).error(R.drawable.no_image_available))
                    .load("$payment_path${data.payment_logo}")
                    .into(iv_payment)
            ll_.isLongClickable = false
            tv_payment.text = "${data.payment_method}"
            ll_.setOnClickListener {
                clickListner.callClick(data)
            }
            return rootView!!
        }
    }

    interface onClickPayment {
        fun callClick(data: GetPaymentTypes)
    }

    override fun onBackPressed() {
        if (status_driver_reaggine) {
            setResult(Activity.RESULT_OK)
            finish()
        } else {
            super.onBackPressed()
        }
    }

    var siganture_path = ""

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            8989 -> if (resultCode === Activity.RESULT_OK) {
                if (data != null && data.hasExtra("siganture_path")) {
                    siganture_path = "${data.getStringExtra("siganture_path")}"
                    if (siganture_path != null && siganture_path.isNotEmpty()) {
                        callEmployeePaymentInvoice()
                    }
//                    if (::iv_siganture.isInitialized) {
//                        val newPhotoKey = ObjectKey(System.currentTimeMillis().toString())
//                        Glide.with(this@EmployeeInvoiceDetailsActivity)
//                                .load("$siganture_path")
//                                .apply(RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.NONE)
//                                        .dontTransform().dontAnimate()
//                                        .signature(newPhotoKey))
//                                .into(iv_siganture)
//                    }
                }
            }
        }
    }
}