package com.budbasic.posconsole.queue

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import com.budbasic.customer.global.Preferences
import com.budbasic.global.CUC
import com.google.gson.Gson
import com.google.gson.JsonObject
import com.kotlindemo.model.GetEmployeeAppSetting
import com.mylibrary.*
import org.json.JSONObject

class BarCodeScan : AppCompatActivity(), BarcodeView.ResultHandler {
    override fun handleResult(rawResult: ScanResult?) {
        println("aaaaaaaaaaa   scanresult  "+rawResult.toString())
        try {
            when (rawResult!!.barcodeFormat) {
                DevyBarcodeFormat.PDF_417 -> {
                    val rawR = rawResult.text.split("\n".toRegex())
                    println("aaaaaaaaaaa   rawR  "+rawR.toString())
                    val resultOBJ = JSONObject()
                    for (rData in rawR) {
                       // Log.i("ScanQRCodexxx123", "rData : $rData")
                        if (rData.length >= 3) {
                            resultOBJ.put("${rData.substring(0, 3)}", "${rData.substring(3, rData.length)}")
                        }
                    }
                 //   Log.d("ANSI"," == "+rawR.get(1).split(","))
                    println("aaaaaaaaaaa   resultOBJ  "+resultOBJ.toString())

                    driving_license = resultOBJ.optString("DAQ")
                    Timestamp = resultOBJ.optString("DBA")
                    dob = resultOBJ.optString("DBD")
                    Address = "${resultOBJ.optString("DAG")},${resultOBJ.optString("DAI")},${resultOBJ.optString("DAJ")},${resultOBJ.optString("DAK")}"
                    City = resultOBJ.optString("DAI")
                    Name = resultOBJ.optString("DCT")
                    date=resultOBJ.optString("DBB")
                    date="${CUC.getdatefromoneformattoAnother("ddMMyyyy", "dd/MM/yyyy", date)}"
                    if(Name.equals("")){
                        val raeAnsi=rawR.get(1).split(",")
                        if(raeAnsi.size>=2){
                            Name=raeAnsi.get(1).toString()
                        }
                    }
                    Postal = resultOBJ.optString("DAK")
                    lName = resultOBJ.optString("DCS")
                    if(lName.equals("")){
                        val raeAnsi=rawR.get(1).split(",")
                        if(raeAnsi.size>=2){
                            val index=raeAnsi.get(0).indexOf("DAA")
                            lName=raeAnsi.get(0).substring(index+3,raeAnsi.get(0).length)
                        }


                    }
                    state_code = resultOBJ.optString("DAJ")
                    if (Address != "") {
                        val splitAddress = Address.split(",")
                        Postal = "${splitAddress[splitAddress.size - 1]}"
                        state_code = "${splitAddress[splitAddress.size - 2]}"
                        City = "${splitAddress[splitAddress.size - 3]}"
                        Log.i("combinedResult", "${splitAddress[splitAddress.size - 1]}")
                        Log.i("combinedResult", "${splitAddress[splitAddress.size - 2]}")
                        Log.i("combinedResult", "${splitAddress[splitAddress.size - 3]}")
                    }

                    if (Timestamp != "") {
                        //08252018
                        Timestamp = "${CUC.getdatefromoneformattoAnother("MMddyyyy", "yyyy-MM-dd", Timestamp)}"
                    }
                    if (dob != "") {
                        //08252018
                        dob = "${CUC.getdatefromoneformattoAnother("MMddyyyy", "yyyy-MM-dd", dob)}"
                    }

                    val mainData = JsonObject()
                    val licence_data = JsonObject()
                    licence_data.addProperty("driving_license", "${driving_license.trim()}")
                    licence_data.addProperty("Timestamp", "${Timestamp.trim()}")
                    licence_data.addProperty("patient_dob", "${dob.trim()}")
                    licence_data.addProperty("Address", "${Address.trim()}")
                    licence_data.addProperty("Name", "${Name.trim()}")
                    licence_data.addProperty("lName", "${lName.trim()}")
                    licence_data.addProperty("Postal", "${Postal.trim()}")
                    licence_data.addProperty("state_code", "${state_code.trim()}")
                    licence_data.addProperty("City", "${City.trim()}")
                    mainData.add("licence_data", licence_data)

                    val returnIntent = Intent()
                  //  val data = Gson().toJson(licence_data)
                    val data1 = Gson().toJson(mainData)
                    println("aaaaaaaaa  dataresult  barcode  "+data1.toString())

                    returnIntent.putExtra("data", data1)
                    returnIntent.putExtra("driving_license", driving_license.trim())
                    returnIntent.putExtra("patient_dob", dob.trim())
                    returnIntent.putExtra("Name", Name.trim())
                    returnIntent.putExtra("lName", lName.trim())
                    returnIntent.putExtra("Postal", Postal.trim())
                    returnIntent.putExtra("state_code", state_code.trim())
                    returnIntent.putExtra("City", City.trim())
                    returnIntent.putExtra("Timestamp", Timestamp.trim())

                    setResult(Activity.RESULT_OK, returnIntent)
                    finish()


                }
                else ->{
                    println("aaaaaaaaa  else ")
                    finish()
                }

            }
        } catch (ee: Exception) {
            ee.printStackTrace()
        }
    }

    internal var TAG = "MenuActivity"
    var CALL_FUN = 0x11
    val MY_BLINK_ID_CUSTOMER = 0x12
    val MY_BLINK_ID_EMPLOYEE = 0x13

    var driving_license = ""
    var Timestamp = ""
    var Address = ""
    var date=""
    var City = ""
    var Name = ""
    var lName = ""
    var Postal = ""
    var state_code = ""
    var dob = ""
    lateinit var appSettingData: GetEmployeeAppSetting

    //var beepManager: BeepManager? = null
    var scannerView: BarcodeView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        scannerView = BarcodeView(this)
        setContentView(scannerView)
        //requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE
        appSettingData = Gson().fromJson(Preferences.getPreference(this@BarCodeScan, "AppSetting"), GetEmployeeAppSetting::class.java)
        //beepManager = BeepManager(this)
        if (intent != null && intent.hasExtra("status")) {
            if (intent.getStringExtra("status") == "0") {
                CALL_FUN = MY_BLINK_ID_EMPLOYEE
                //val passScan = Intent(this@MenuActivity, DDJActivity::class.java)
                //startActivityForResult(passScan, CALL_FUN)
            } else {
                CALL_FUN = MY_BLINK_ID_CUSTOMER
                //val passScan = Intent(this@MenuActivity, DDJActivity::class.java)
                //startActivityForResult(passScan, CALL_FUN)
            }
        }
//        val cv_queue = findViewById<CardView>(R.id.cv_queue)
//        val cv_attendance = findViewById<CardView>(R.id.cv_attendance)
//
//        cv_queue.setOnClickListener({ view ->
//            CALL_FUN = MY_BLINK_ID_CUSTOMER
//            //scanID()
//            val passScan = Intent(this@MenuActivity, DDJActivity::class.java)
//            startActivityForResult(passScan, CALL_FUN)
//        })
//
//        cv_attendance.setOnClickListener({ view ->
//            CALL_FUN = MY_BLINK_ID_EMPLOYEE
//            val passScan = Intent(this@MenuActivity, DDJActivity::class.java)
//            startActivityForResult(passScan, CALL_FUN)
//            //scanID()
//        })
    }


    public override fun onResume() {
        super.onResume()
//        if (beepManager == null) {
//            beepManager = BeepManager(this@MenuActivity)
//        }
        scannerView!!.setFormats(arrayListOf(DevyBarcodeFormat.PDF_417))
        scannerView!!.setResultHandler(this@BarCodeScan) // Register ourselves as a handler for scan results.
        scannerView!!.startCamera()          // Start camera on resume
    }

    public override fun onPause() {
        super.onPause()
//        if (beepManager != null) {
//            beepManager!!.close()
//            beepManager = null
//        }
        scannerView!!.stopCamera()
        // Stop camera on pause
    }

    override fun onBackPressed() {
        setResult(Activity.RESULT_CANCELED)
        super.onBackPressed()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        println("aaaaaaaaaa   data  "+data.toString())
        when (requestCode) {
            444 -> {
                println("aaaaaaaaaa   data  "+data.toString())
                val returnIntent = Intent()
                //val data = Gson().toJson(data)

                returnIntent.putExtra("data", data)
                setResult(Activity.RESULT_OK, returnIntent)
                finish()
               // setResult(Activity.RESULT_OK)
               // finish()
            }
            CALL_FUN -> if (resultCode === Activity.RESULT_CANCELED) {
                setResult(Activity.RESULT_CANCELED)
                finish()
            } else if (resultCode === Activity.RESULT_OK) {
                if (resultCode == Activity.RESULT_OK && data != null) {
                    if (data != null && data.hasExtra("scannerResult")) {
                        if (data.getStringExtra("scannerResult").isNotEmpty()) {
                            val resultOBJ = JSONObject(data.getStringExtra("scannerResult"))
                            Log.i("scannerResult", "scannerResult : $resultOBJ")
                            driving_license = resultOBJ.optString("DAQ")
                            Timestamp = resultOBJ.optString("DBA")
                            dob = resultOBJ.optString("DBD")
                            Address = "${resultOBJ.optString("DAG")},${resultOBJ.optString("DAG")},${resultOBJ.optString("DAJ")},${resultOBJ.optString("DAK")}"
                            City = resultOBJ.optString("DAI")
                            Name = resultOBJ.optString("DCT")
                            Postal = resultOBJ.optString("DAK")
                            lName = resultOBJ.optString("DCS")
                            date=resultOBJ.optString("DBB")
                            date="${CUC.getdatefromoneformattoAnother("ddMMyyyy", "dd/MM/yyyy", date)}"

                            if (Address != "") {
                                val splitAddress = Address.split(",")
                                Postal = "${splitAddress[splitAddress.size - 1]}"
                                state_code = "${splitAddress[splitAddress.size - 2]}"
                                City = "${splitAddress[splitAddress.size - 3]}"
                                Log.i("combinedResult", "${splitAddress[splitAddress.size - 1]}")
                                Log.i("combinedResult", "${splitAddress[splitAddress.size - 2]}")
                                Log.i("combinedResult", "${splitAddress[splitAddress.size - 3]}")
                            }

                            if (Timestamp != "") {
                                //08252018
                                Timestamp = "${CUC.getdatefromoneformattoAnother("MMddyyyy", "yyyy-MM-dd", Timestamp)}"
                            }
                            if (dob != "") {
                                //08252018
                                dob = "${CUC.getdatefromoneformattoAnother("MMddyyyy", "yyyy-MM-dd", dob)}"
                            }

                            val mainData = JsonObject()
                            val licence_data = JsonObject()
                            licence_data.addProperty("driving_license", "${driving_license.trim()}")
                            licence_data.addProperty("Timestamp", "${Timestamp.trim()}")
                            licence_data.addProperty("patient_dob", "${dob.trim()}")
                            licence_data.addProperty("Address", "${Address.trim()}")
                            licence_data.addProperty("Name", "${Name.trim()}")
                            licence_data.addProperty("lName", "${lName.trim()}")
                            licence_data.addProperty("Postal", "${Postal.trim()}")
                            licence_data.addProperty("state_code", "${state_code.trim()}")
                            licence_data.addProperty("City", "${City.trim()}")
                            mainData.add("licence_data", licence_data)

                            if (CALL_FUN == MY_BLINK_ID_CUSTOMER) {
                                val mIntent = Intent(this@BarCodeScan, MessageActivity::class.java)
                                mIntent.putExtra("isFrom", "Customer")
                                mIntent.putExtra("licenceNo", driving_license)
                                mIntent.putExtra("FirstName", Name)
                                mIntent.putExtra("LastName", lName)
                                mIntent.putExtra("ExpiryDate", Timestamp)
                                mIntent.putExtra("City", City)
                                mIntent.putExtra("MeberSinceDate",date)
                                mIntent.putExtra("licence_data", mainData.toString())
                                startActivityForResult(mIntent, 444)
                            } else if (CALL_FUN == MY_BLINK_ID_EMPLOYEE) {
                                val mIntent = Intent(this@BarCodeScan, MessageActivity::class.java)
                                mIntent.putExtra("isFrom", "Employee")
                                mIntent.putExtra("licenceNo", driving_license)
                                mIntent.putExtra("FirstName", Name)
                                mIntent.putExtra("LastName", lName)
                                mIntent.putExtra("MeberSinceDate",date)
                                mIntent.putExtra("ExpiryDate", Timestamp)
                                mIntent.putExtra("City", City)
                                startActivityForResult(mIntent, 444)
                            } else {

                            }
                        }
                    }
                } else {
                    // if BlinkID activity did not return result, user has probably
                    // pressed Back button and cancelled scanning
                    driving_license = ""
                    Timestamp = ""
                    dob = ""
                    Address = ""
                    City = ""
                    Name = ""
                    Postal = ""
                    lName = ""
                    date=""
                    CUC.displayToast(this@BarCodeScan, "0", "please scan valid license.")
                }
            }
        }
    }
}
