package com.budbasic.posconsole.queue

import android.animation.ArgbEvaluator
import android.animation.ObjectAnimator
import android.animation.ValueAnimator
import android.app.Activity
import android.app.AlertDialog
import android.app.Dialog
import android.content.DialogInterface
import android.content.pm.ActivityInfo
import android.os.Bundle
import android.os.Handler
import android.os.StrictMode
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.Window
import android.view.WindowManager
import android.widget.CheckBox
import android.widget.EditText
import android.widget.TextView
import com.budbasic.customer.global.Preferences
import com.budbasic.global.CUC
import com.budbasic.posconsole.R
import com.budbasic.posconsole.model.APIClass
import com.budbasic.posconsole.webservice.RequetsInterface
import com.google.gson.Gson
import com.kotlindemo.model.GetEmployeeLogin
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.msg_layout.*

class MessageCopyActivity : AppCompatActivity(), APIClass.onCallListner {
    override fun onFailed(requestCode: Int) {
    }

    override fun onSuccess(requestCode: Int) {
        if (requestCode == APPQEMPLOYEETRACKING) {
            callAppqEmployeeTracking()
        } else if (requestCode == APPQVERIFYDLICENCE) {
            callAppqVerifyDlicence()
        }
    }

    lateinit var mDialog: Dialog
    val APPQEMPLOYEETRACKING = 1
    val APPQVERIFYDLICENCE = 2

    lateinit var logindata: GetEmployeeLogin

    lateinit var textColorAnim: ObjectAnimator

    var driving_license = ""
    var Timestamp = ""
    var Address = ""
    var City = ""
    var Name = ""
    var lName = ""
    var Postal = ""
    var isFrom = ""
    var licence_data = ""
    var meberSinceDate=""

    var mHandler = Handler()

    var mRunable: Runnable? = null
    var apiClass: APIClass? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE
        setContentView(R.layout.msg_layout)
        apiClass = APIClass(this@MessageCopyActivity, this)
        logindata = Gson().fromJson(Preferences.getPreference(this@MessageCopyActivity, "logindata"), GetEmployeeLogin::class.java)

        textColorAnim = ObjectAnimator.ofInt(tv_msg, "textColor", ContextCompat.getColor(this@MessageCopyActivity,
                R.color.black), ContextCompat.getColor(this@MessageCopyActivity, R.color.border_gray))
        textColorAnim.duration = 700
        textColorAnim.setEvaluator(ArgbEvaluator())
        textColorAnim.repeatCount = ValueAnimator.INFINITE
        textColorAnim.repeatMode = ValueAnimator.REVERSE

        mRunable = Runnable {
            setResult(Activity.RESULT_OK)
            finish()
        }

        if (intent != null) {
            if (intent.hasExtra("isFrom")) {
                isFrom = intent.getStringExtra("isFrom")
                driving_license = intent.getStringExtra("licenceNo")
                Name = intent.getStringExtra("FirstName")
                lName = intent.getStringExtra("LastName")
                Timestamp = intent.getStringExtra("ExpiryDate")
                meberSinceDate=intent.getStringExtra("MeberSinceDate")
            //    Log.d("TimeStamp"," = "+Timestamp)
                City = intent.getStringExtra("City")
                if (intent.hasExtra("licence_data"))
                    licence_data = intent.getStringExtra("licence_data")

                if (isFrom == "Customer") {
                    callInvoiceGenerated()
                } else if (isFrom == "Employee") {
                    callAppqEmployeeTracking()

                }
            }
        }
    }

    override fun onBackPressed() {
        //super.onBackPressed()
        setResult(Activity.RESULT_OK)
        finish()
    }

    lateinit var customDialog: Dialog
    var descrition = ""
    var queue_mmmp = "0"
    fun callInvoiceGenerated() {
        customDialog = Dialog(this@MessageCopyActivity, R.style.TransparentProgressDialog)
        val policy = StrictMode.ThreadPolicy.Builder().permitAll().build()
        StrictMode.setThreadPolicy(policy)
        customDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        customDialog.setContentView(R.layout.queue_patient_dialog_new)
        customDialog.window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)
        customDialog.setCanceledOnTouchOutside(false)
        customDialog.show()
        val cv_generate_invoice = customDialog.findViewById<TextView>(R.id.cv_generate_invoice)
        val cv_approve_request = customDialog.findViewById<TextView>(R.id.cv_approve_request)
        val tv_mmmp = customDialog.findViewById<TextView>(R.id.tv_mmmp)
        val tv_meber_since = customDialog.findViewById<TextView>(R.id.tv_meber_since)
    //    val ck_mmmp = customDialog.findViewById<CheckBox>(R.id.ck_mmmp)
        val tv_cancel_reson = customDialog.findViewById<EditText>(R.id.tv_cancel_reson)
        val tv_customer_name = customDialog.findViewById<TextView>(R.id.tv_customer_name)


        cv_approve_request.setOnClickListener {
            finish()
        }
        tv_customer_name.text = "$Name $lName"
        tv_mmmp.text="MMMP: "+driving_license+""
        tv_meber_since.text="Meber since: "+meberSinceDate+""
        cv_generate_invoice.setOnClickListener {
            descrition = "${tv_cancel_reson.text}"
//            if (descrition == "") {
//                CUC.displayToast(this@MessageActivity, "0", "Enter Description")
//            } else {
//                if (ck_mmmp.isChecked) {
//                    queue_mmmp = "1"
//                } else {
//                    queue_mmmp = "0"
//                }
                queue_mmmp = "1"
                callAppqVerifyDlicence()
          //  }
        }
    }

    fun callAppqVerifyDlicence() {
        mDialog = CUC.createDialog(this@MessageCopyActivity)
        val apiService = RequetsInterface.create()
        val parameterMap = HashMap<String, String>()
        parameterMap["api_key"] = "${Preferences.getPreference(this@MessageCopyActivity, "API_KEY")}"
        parameterMap["licence_no"] = "$driving_license"
        parameterMap["login_user"] = "${logindata.user_id}"
        parameterMap["store_id"] = "${logindata.user_store_id}"
        parameterMap["company_id"] = "0"
        parameterMap["first_name"] = "$Name"
        parameterMap["licence_city"] = "$City"
        parameterMap["last_name"] = "$lName"
        parameterMap["exp_date"] = "$Timestamp"
        parameterMap["licence_data"] = "$licence_data"
        parameterMap["queue_desc"] = "$descrition"
        parameterMap["queue_mmmp"] = "$queue_mmmp"

        Log.i("Result", "parameterMap : $parameterMap")
        CompositeDisposable().add(apiService.callAppqVerifyDlicence(parameterMap)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe({ result ->
                    if (mDialog != null && mDialog.isShowing)
                        mDialog.dismiss()
                    if (customDialog != null && customDialog.isShowing)
                        customDialog.dismiss()
                    if (result != null) {
                      //  Log.d("AAAAA", "AAAA")
                        Log.i("Result", "" + result.toString())
                        if (result.status == "0") {
                            if (result.image != null && result.image != "") {
//                                Glide.with(this@MessageActivity)
//                                        .applyDefaultRequestOptions(RequestOptions().format(DecodeFormat.PREFER_ARGB_8888).error(R.drawable.no_image_available))
//                                        .load("${result.image}")
//                                        .into(img_msg)
                            }
                            setResult(Activity.RESULT_OK)
                            finish()
//                            tv_msg.text = result.message
//                            mHandler.postDelayed(mRunable, 10000)
//                            startBlinkAnim()
                        } else if (result.status == "10") {
                            apiClass!!.callApiKey(APPQVERIFYDLICENCE)
                        } else {
                            if (result.image != null && result.image != "") {
//                                Glide.with(this@MessageActivity)
//                                        .applyDefaultRequestOptions(RequestOptions().format(DecodeFormat.PREFER_ARGB_8888).error(R.drawable.no_image_available))
//                                        .load("${result.image}")
//                                        .into(img_msg)
                            }
                            setResult(Activity.RESULT_OK)
                            finish()
//                            tv_msg.text = result.message
//                            mHandler.postDelayed(mRunable, 10000)
//                            startBlinkAnim()
                            //CommonUtils.displayToast(this@MessageActivity, result.show_status, result.message)
                        }
                    } else {
                        //  CUC.displayToast(this@MessageActivity, "0", getString(R.string.connection_to_database_failed))
                    }
                }, { error ->
                    if (mDialog != null && mDialog.isShowing)
                        mDialog.dismiss()
                    CUC.displayToast(this@MessageCopyActivity, "0", getString(R.string.connection_to_database_failed))

                    error.printStackTrace()
                })
        )
    }

    fun callAppqEmployeeTracking() {
        mDialog = CUC.createDialog(this@MessageCopyActivity)
        val apiService = RequetsInterface.create()
        val parameterMap = HashMap<String, String>()
        parameterMap["api_key"] = "${Preferences.getPreference(this@MessageCopyActivity, "API_KEY")}"
        parameterMap["licence_no"] = "$driving_license"
        parameterMap["login_user"] = "${logindata.user_id}"
        Log.i("Result", "parameterMap : $parameterMap")
        CompositeDisposable().add(apiService.callAppqEmployeeTracking(parameterMap)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe({ result ->
                    if (mDialog != null && mDialog.isShowing)
                        mDialog.dismiss()
                    if (result != null) {
                       // Log.d("AAAAA111", "AAAA111")
                        Log.i("Result", "" + result.toString())
                        if (result.status == "0") {
                            if (result.image != null && result.image != "") {
                                /*  Glide.with(this@MessageActivity)
                                          .applyDefaultRequestOptions(RequestOptions().format(DecodeFormat.PREFER_ARGB_8888).error(R.drawable.no_image_available))
                                          .load("${result.image}")
                                          .into(img_msg)*/
                            }
//                            tv_msg.text = result.message
//                            mHandler.postDelayed(mRunable, 10000)
//                            startBlinkAnim()
                            setResult(Activity.RESULT_OK)
                            finish()
                        } else if (result.status == "10") {
                            apiClass!!.callApiKey(APPQEMPLOYEETRACKING)
                        } else {
                            if (result.image != null && result.image != "") {
                                /*Glide.with(this@MessageActivity)
                                        .applyDefaultRequestOptions(RequestOptions().format(DecodeFormat.PREFER_ARGB_8888).error(R.drawable.no_image_available))
                                        .load("${result.image}")
                                        .into(img_msg)*/
                            }
//                            tv_msg.text = result.message
//                            mHandler.postDelayed(mRunable, 10000)
//                            startBlinkAnim()
                            showdialog(result.message)

                            // CommonUtils.displayToast(this@MessageActivity, result.show_status, result.message)
                        }
                    } else {
                        //   CUC.displayToast(this@MessageActivity, "0", getString(R.string.connection_to_database_failed))
                    }
                }, { error ->
                    if (mDialog != null && mDialog.isShowing)
                        mDialog.dismiss()
                    CUC.displayToast(this@MessageCopyActivity, "0", getString(R.string.connection_to_database_failed))

                    error.printStackTrace()
                })
        )
    }
    fun showdialog(msg:String) {
        val builder1 = AlertDialog.Builder(this@MessageCopyActivity)
        builder1.setMessage("" + msg)
        builder1.setCancelable(true)

        builder1.setPositiveButton(
                "Ok"
        ) { dialog, id ->
            dialog.cancel()
            setResult(Activity.RESULT_OK)
            finish()
        }

        val alert11 = builder1.create()
        alert11.show()
    }
    override fun onDestroy() {
        super.onDestroy()
        stopBlinkAnim()
        try {
            if (mRunable != null)
                mHandler.removeCallbacks(mRunable)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }


    private fun startBlinkAnim() {
        textColorAnim.start()
    }

    private fun stopBlinkAnim() {
        try {
            if (textColorAnim != null) {
                textColorAnim.cancel()
                tv_msg.setTextColor(ContextCompat.getColor(this@MessageCopyActivity, R.color.black))
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }
}