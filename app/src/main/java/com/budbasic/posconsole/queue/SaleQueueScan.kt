package com.budbasic.posconsole.queue

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import com.budbasic.customer.global.Preferences
import com.budbasic.global.CUC
import com.google.gson.Gson
import com.google.gson.JsonObject
import com.kotlindemo.model.GetEmployeeAppSetting
import com.mylibrary.*
import org.json.JSONObject

class SaleQueueScan : AppCompatActivity(), BarcodeView.ResultHandler {
    override fun handleResult(rawResult: ScanResult?) {
        println("aaaaaaaaaaa   scanresult  "+rawResult.toString())
        try {
            val returnIntent = Intent()

            returnIntent.putExtra("data", rawResult.toString())

            setResult(Activity.RESULT_OK, returnIntent)
            finish()

        } catch (ee: Exception) {
            ee.printStackTrace()
        }
    }

    internal var TAG = "MenuActivity"
    var CALL_FUN = 0x11
    val MY_BLINK_ID_CUSTOMER = 0x12
    val MY_BLINK_ID_EMPLOYEE = 0x13

    var driving_license = ""
    var Timestamp = ""
    var Address = ""
    var date=""
    var City = ""
    var Name = ""
    var lName = ""
    var Postal = ""
    var state_code = ""
    var dob = ""
    lateinit var appSettingData: GetEmployeeAppSetting

    //var beepManager: BeepManager? = null
    var scannerView: BarcodeView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        scannerView = BarcodeView(this)
        setContentView(scannerView)
        //requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE
        appSettingData = Gson().fromJson(Preferences.getPreference(this@SaleQueueScan, "AppSetting"), GetEmployeeAppSetting::class.java)
        //beepManager = BeepManager(this)
        if (intent != null && intent.hasExtra("status")) {
            if (intent.getStringExtra("status") == "0") {
                CALL_FUN = MY_BLINK_ID_EMPLOYEE
                //val passScan = Intent(this@MenuActivity, DDJActivity::class.java)
                //startActivityForResult(passScan, CALL_FUN)
            } else {
                CALL_FUN = MY_BLINK_ID_CUSTOMER
                //val passScan = Intent(this@MenuActivity, DDJActivity::class.java)
                //startActivityForResult(passScan, CALL_FUN)
            }
        }
//        val cv_queue = findViewById<CardView>(R.id.cv_queue)
//        val cv_attendance = findViewById<CardView>(R.id.cv_attendance)
//
//        cv_queue.setOnClickListener({ view ->
//            CALL_FUN = MY_BLINK_ID_CUSTOMER
//            //scanID()
//            val passScan = Intent(this@MenuActivity, DDJActivity::class.java)
//            startActivityForResult(passScan, CALL_FUN)
//        })
//
//        cv_attendance.setOnClickListener({ view ->
//            CALL_FUN = MY_BLINK_ID_EMPLOYEE
//            val passScan = Intent(this@MenuActivity, DDJActivity::class.java)
//            startActivityForResult(passScan, CALL_FUN)
//            //scanID()
//        })
    }


    public override fun onResume() {
        super.onResume()
//        if (beepManager == null) {
//            beepManager = BeepManager(this@MenuActivity)
//        }
        scannerView!!.setFormats(arrayListOf(DevyBarcodeFormat.PDF_417))
        scannerView!!.setResultHandler(this@SaleQueueScan) // Register ourselves as a handler for scan results.
        scannerView!!.startCamera()          // Start camera on resume
    }

    public override fun onPause() {
        super.onPause()
//        if (beepManager != null) {
//            beepManager!!.close()
//            beepManager = null
//        }
        scannerView!!.stopCamera()
        // Stop camera on pause
    }

    override fun onBackPressed() {
        setResult(Activity.RESULT_CANCELED)
        super.onBackPressed()
    }


}
