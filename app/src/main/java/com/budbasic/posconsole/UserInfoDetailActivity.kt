package com.budbasic.posconsole

import android.app.DatePickerDialog
import android.app.Dialog
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import android.view.WindowManager
import com.budbasic.customer.global.Preferences
import com.budbasic.global.CUC
import com.budbasic.posconsole.adapter.DocumentShowAdapter
import com.budbasic.posconsole.adapter.PatientFavouritProductAdapter
import com.budbasic.posconsole.adapter.PatientPurchaseHistroryAdapter
import com.budbasic.posconsole.model.APIClass
import com.budbasic.posconsole.webservice.RequetsInterface
import com.bumptech.glide.Glide
import com.google.gson.Gson
import com.kotlindemo.model.EmployeePatientInfo
import com.kotlindemo.model.GetEmployeeAppSetting
import com.kotlindemo.model.GetEmployeeLogin
import com.kotlindemo.model.InvoiceHistory
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.sales_patient_pos.*
import java.util.*
import kotlin.collections.ArrayList

class UserInfoDetailActivity : AppCompatActivity(), APIClass.onCallListner {
    override fun onFailed(requestCode: Int) {

    }

    override fun onSuccess(requestCode: Int) {

    }

    var apiClass: APIClass? = null
    lateinit var logindata: GetEmployeeLogin
    var patient_id: String? = null
    var appSettingData: GetEmployeeAppSetting? = null
    var patientpurchaseInfo: PatientPurchaseHistroryAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN)
        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN or WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN)
        setContentView(R.layout.sales_patient_pos)
        init()
    }

    private fun init() {
        logindata = Gson().fromJson(Preferences.getPreference(this@UserInfoDetailActivity, "logindata"), GetEmployeeLogin::class.java)
        appSettingData = Gson().fromJson(Preferences.getPreference(this@UserInfoDetailActivity, "AppSetting"), GetEmployeeAppSetting::class.java)
        if (intent != null && intent.extras != null && intent.extras.containsKey("patient_id")) {
            patient_id = intent.getStringExtra("patient_id")
            apiCallForEmployeeDocument()
        }

        closeActivity.setOnClickListener {
            this.finish()
        }

        tvSelectDate.setOnClickListener {
            val datePickerDialog = DatePickerDialog(this@UserInfoDetailActivity, DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                val selectDate = "" + (if (monthOfYear + 1 >= 10) monthOfYear + 1 else "0" + (monthOfYear + 1)) + "-" + (if (dayOfMonth >= 10) dayOfMonth else "0$dayOfMonth") + "-" + year
                tvSelectDate.text = selectDate
                tvSelectDate.tag = "${CUC.getdatefromoneformattoAnother("MM-dd-yyyy", "yyyy-MM-dd", selectDate)}"

                if (patientpurchaseInfo != null) {
                    patientpurchaseInfo!!.setFilterValue(selectDate)
                }
            }, Calendar.getInstance().get(Calendar.YEAR), Calendar.getInstance().get(Calendar.MONTH), Calendar.getInstance().get(Calendar.DAY_OF_MONTH))
            datePickerDialog.datePicker.maxDate = System.currentTimeMillis()
            datePickerDialog.show()
        }

        textDocument.setOnClickListener {
            rc_document.visibility = View.VISIBLE
            scrollNotes.visibility = View.GONE
            textDocument.background = resources.getDrawable(R.drawable.gradiant_line_background_toside)
            textNotes.background = resources.getDrawable(R.drawable.gray_line_background)
            textNotes.setTextColor(resources.getColor(R.color.black))
            textDocument.setTextColor(resources.getColor(R.color.white))
        }

        textNotes.setOnClickListener {
            rc_document.visibility = View.GONE
            scrollNotes.visibility = View.VISIBLE
            textDocument.background = resources.getDrawable(R.drawable.gray_line_background_toside)
            textNotes.background = resources.getDrawable(R.drawable.gradiant_line_background)
            textNotes.setTextColor(resources.getColor(R.color.white))
            textDocument.setTextColor(resources.getColor(R.color.black))
        }
    }

    private fun apiCallForEmployeeDocument() {
        var mDialog = Dialog(this)
        try {

            if (!this!!.isFinishing) {
                mDialog = CUC.createDialog(this)
                mDialog.show()
                val apiService = RequetsInterface.Factory.create()
                val parameterMap = HashMap<String, String>()

                parameterMap["api_key"] = "${Preferences.getPreference(this!!, "API_KEY")}"
                parameterMap["employee_id"] = "${logindata.user_id}"
                parameterMap["patient_id"] = "${patient_id}"
                parameterMap["store_id"] = "${logindata.user_store_id}"

                CompositeDisposable().add(apiService.callEmployeePatientInfo(parameterMap)
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.io())
                        .subscribe({ result ->
                            if (mDialog != null && mDialog.isShowing)
                                mDialog.dismiss()
                            if (result != null) {
                                setData(result!!)
                            }

                        }, { error ->
                            if (mDialog != null && mDialog.isShowing)
                                mDialog.dismiss()
                            CUC.displayToast(this!!, "0", this.getString(R.string.connection_to_database_failed))
                            error.printStackTrace()
                        })
                )
            }
        } catch (e: Exception) {
            if (mDialog != null && mDialog.isShowing) {
                mDialog.dismiss()
            }
            e.printStackTrace()
        }

    }

    private fun setData(data: EmployeePatientInfo) {

        tv_name.text = String.format("%s %s ", data.patient!!.patient_fname, data.patient!!.patient_lname)
        tv_licence.text = String.format("%s", data.patient!!.patient_licence_no)
        tv_dob.text = String.format(" D.O.B %s", data.patient!!.patient_dob)
        tv_avgSpeedData.text = String.format("%s", data.average_spent)
        tv_spentLimit.text = String.format("%s", data.Limit)
        patientNote.text = String.format("%s", data.patient!!.patient_notes)

        val registrationDate = CUC.getdatefromoneformattoAnother("yyyy-MM-dd", "MM/dd/yyyy", data.patient!!.patient_registration_date)
        if (registrationDate.isNotEmpty() && !data.registered_since.isNullOrEmpty()) {
            tv_member_since.text = String.format("Member since: %s %s", registrationDate, data.registered_since)
        }
        if (appSettingData != null) {
            Glide.with(this).load(appSettingData!!.patient_image_path + data.patient!!.patient_image).into(imgUserProfile)
        } else {
            Glide.with(this).load(data.patient!!.patient_image).into(imgUserProfile)
        }

        if (data.invoice_history != null && data.invoice_history.size > 0) {
            txtNodataFound.visibility = View.GONE
            rc_purchase_history.visibility = View.VISIBLE
            val patientName = String.format("%s %s ", data.patient!!.patient_fname, data.patient!!.patient_lname)
            patientpurchaseInfo = PatientPurchaseHistroryAdapter(this, patientName, data.invoice_history)
            rc_purchase_history.layoutManager = LinearLayoutManager(this@UserInfoDetailActivity, LinearLayoutManager.VERTICAL, false)
            rc_purchase_history.adapter = patientpurchaseInfo
        } else {
            txtNodataFound.visibility = View.VISIBLE
            rc_purchase_history.visibility = View.GONE
        }

        if (data.favourites!!.size > 0) {
            txtNoDataFoundFavourite.visibility = View.GONE
            recycle_favourite.visibility = View.VISIBLE
            val favouriteAdapter = PatientFavouritProductAdapter(this, patient_id, appSettingData, logindata, data.favourites!!)
            recycle_favourite.layoutManager = LinearLayoutManager(this@UserInfoDetailActivity, LinearLayoutManager.HORIZONTAL, false)
            recycle_favourite.adapter = favouriteAdapter
        } else {
            recycle_favourite.visibility = View.GONE
            txtNoDataFoundFavourite.visibility = View.VISIBLE
        }

        if (data.recomended!!.size > 0) {
            recycler_recommended.visibility = View.VISIBLE
            txtNoDataFoundRecommended.visibility = View.GONE
            val favouriteAdapter1 = PatientFavouritProductAdapter(this, patient_id, appSettingData, logindata, data.recomended!!)
            recycler_recommended.layoutManager = LinearLayoutManager(this@UserInfoDetailActivity, LinearLayoutManager.HORIZONTAL, false)
            recycler_recommended.adapter = favouriteAdapter1
        } else {
            recycler_recommended.visibility = View.GONE
            txtNoDataFoundRecommended.visibility = View.VISIBLE
        }


        if (data.documents.size > 0) {
            val favouriteAdapter1 = DocumentShowAdapter(this, data.document_path!!, this, data.documents)
            rc_document.layoutManager = LinearLayoutManager(this@UserInfoDetailActivity, LinearLayoutManager.HORIZONTAL, false)
            rc_document.adapter = favouriteAdapter1
        }


    }
}