package com.budbasic.posconsole

import android.content.Intent
import android.os.Bundle
import android.provider.Settings
import android.support.v7.app.AppCompatActivity
import android.util.Base64
import android.util.Log
import android.view.Window
import android.view.WindowManager
import com.budbasic.customer.global.Preferences
import com.budbasic.global.CUC
import com.budbasic.posconsole.model.APIClass
import com.budbasic.posconsole.reception.StoreManagerActivity
import com.budbasic.posconsole.webservice.RequetsInterface
import com.google.android.gms.common.GooglePlayServicesNotAvailableException
import com.google.android.gms.common.GooglePlayServicesRepairableException
import com.google.android.gms.security.ProviderInstaller
import com.google.firebase.iid.FirebaseInstanceId
import com.google.gson.Gson
import com.kotlindemo.model.GetEmployeeAppSetting
import com.kotlindemo.model.GetEmployeeLogin
import com.kotlindemo.model.GetTerminalPrinter
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import java.security.KeyManagementException
import java.security.NoSuchAlgorithmException
import java.util.*
import javax.net.ssl.SSLContext


class SplashActivity : AppCompatActivity(), APIClass.onCallListner {
    override fun onFailed(requestCode: Int) {
        if (requestCode == 1) {
            // CUC.displayToast(this@SplashActivity, "0", getString(R.string.connection_to_database_failed))
        }
    }

    override fun onSuccess(requestCode: Int) {
        if (requestCode == 1) {
            callEmployeeAppSetting()
        }
    }

    var device_id = ""
    var apiClass: APIClass? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN)
        setContentView(R.layout.activity_splash)
        apiClass = APIClass(this@SplashActivity, this)
        val charset = Charsets.UTF_8
        val data = "9696".toByteArray(charset)
        val base64 = Base64.encodeToString(data, Base64.DEFAULT)
        Log.i(SplashActivity::class.java.simpleName, "base64 : $base64")
        val data2 = Base64.decode(base64, Base64.DEFAULT)
        val text = String(data2, charset)
        Log.i(SplashActivity::class.java.simpleName, "text : $text")
        try {
            ProviderInstaller.installIfNeeded(applicationContext)
            val sslContext: SSLContext
            sslContext = SSLContext.getInstance("TLSv1.2")
            sslContext.init(null, null, null)
            sslContext.createSSLEngine()
        } catch (e: GooglePlayServicesRepairableException) {
            e.printStackTrace()
        } catch (e: GooglePlayServicesNotAvailableException) {
            e.printStackTrace()
        } catch (e: NoSuchAlgorithmException) {
            e.printStackTrace()
        } catch (e: KeyManagementException) {
            e.printStackTrace()
        }

        init()
    }

    override fun onStart() {
        super.onStart()
    }

    fun init() {
        FirebaseInstanceId.getInstance().token
        getDeviceId()
        //   callEmployeeTerminalPrinter()
    }

    fun getDeviceId() {
        device_id = Settings.Secure.getString(this@SplashActivity.contentResolver, Settings.Secure.ANDROID_ID)
        if (device_id != "") {
            callEmployeeAppSetting()
        } else {
            getDeviceId()
        }
    }

    fun callEmployeeTerminalPrinter() {
        var printerdata: GetTerminalPrinter? = null
        val apiService = RequetsInterface.create()
        val parameterMap = HashMap<String, String>()

        if (logindata != null) {
            parameterMap["api_key"] = "${Preferences.getPreference(this@SplashActivity, "API_KEY")}"
            parameterMap["store_id"] = logindata!!.user_store_id
        }

        if (appSettingData != null) {
            //  parameterMap["store_id"]="${}"
            parameterMap["terminal_id"] = "${appSettingData!!.terminal_id}"
        }
        Log.i("Result", "parameterMapprinter : $parameterMap")
        CompositeDisposable().add(apiService.callEmployeeTerminalPrinter(parameterMap)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe({ result ->
                    Log.i("Resultprinter", "" + result.toString())

                    printerdata = Gson().fromJson(Preferences.getPreference(applicationContext, "printerdata"), GetTerminalPrinter::class.java)
                    //   CUC.displayToast(this,"printetres",result.toString())

                }, { error ->
                    error.printStackTrace()
                })
        )

    }

    fun callEmployeeAppSetting() {

        val apiService = RequetsInterface.create()
        val parameterMap = HashMap<String, String>()
        parameterMap["api_key"] = "${Preferences.getPreference(this@SplashActivity, "API_KEY")}"
        parameterMap["terminal_device_id"] = "$device_id"
        Log.i("Result", "parameterMap : $parameterMap")
        CompositeDisposable().add(apiService.callEmployeeAppSetting(parameterMap)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe({ result ->
                    if (result != null) {
                        Log.i("Result", "" + result.toString())
                        if (result.status == "0") {
                            Preferences.setPreference(this@SplashActivity, "AppSetting", Gson().toJson(result))
                            if (Preferences.getPreference(this@SplashActivity, "logindata").length > 0) {
                                callEmployeeDetails()
                                callEmployeeTerminalPrinter()
                            } else {
                                //callEmployeeDetails()
                                if (result!!.terminal_status == "2") {
                                 /*   val goIntent = Intent(this, RegistrationTerminalActivity::class.java)
                                    startActivity(goIntent)
                                    finish()*/
                                    overridePendingTransition(R.anim.indicator_animator_reverse, R.anim.indicator_animator_reverse)
                                    val goIntent = Intent(this, LoginActivity::class.java)
                                    startActivity(goIntent)
                                    finish()
                                } else if (result!!.terminal_status == "3") {
                                    val goIntent = Intent(this, RegistrationApproved::class.java)
                                    goIntent.putExtra("status", "${result!!.terminal_status}")
                                    goIntent.putExtra("message", "${result.message}")
                                    startActivity(goIntent)
                                    finish()
                                } else if (result!!.terminal_status == "4") {
                                    val goIntent = Intent(this, RegistrationApproved::class.java)
                                    goIntent.putExtra("status", "${result!!.terminal_status}")
                                    goIntent.putExtra("message", "${result.message}")
                                    startActivity(goIntent)
                                    finish()
                                } else if (result!!.terminal_status == "1") {
                                    overridePendingTransition(R.anim.indicator_animator_reverse, R.anim.indicator_animator_reverse)
                                    val goIntent = Intent(this, LoginActivity::class.java)
                                    startActivity(goIntent)
                                    finish()
                                } else {
                                }
                            }

                            CUC.displayToast(this@SplashActivity, result.show_status, result.message)
                        } else if (result.status == "10") {
                            apiClass!!.callApiKey(1)
                        } else {
                            CUC.displayToast(this@SplashActivity, result.show_status, result.message)
                        }
                    } else {
                        //  CUC.displayToast(this@SplashActivity, "0", getString(R.string.connection_to_database_failed))
                    }
                }, { error ->
                    CUC.displayToast(this@SplashActivity, "0", getString(R.string.connection_to_database_failed))
                    error.printStackTrace()
                })
        )
    }

    var logindata: GetEmployeeLogin? = null
    var appSettingData: GetEmployeeAppSetting? = null
    fun callEmployeeDetails() {
        val apiService = RequetsInterface.create()
        val parameterMap = HashMap<String, String>()
        parameterMap["api_key"] = "${Preferences.getPreference(applicationContext, "API_KEY")}"
        logindata = Gson().fromJson(Preferences.getPreference(applicationContext, "logindata"), GetEmployeeLogin::class.java)
        if (logindata != null && logindata!!.user_id.isNotEmpty())
            parameterMap["employee_id"] = "${logindata!!.user_id}"
        appSettingData = Gson().fromJson(Preferences.getPreference(this@SplashActivity, "AppSetting"), GetEmployeeAppSetting::class.java)
        if (appSettingData != null) {
            parameterMap["terminal_id"] = "${appSettingData!!.terminal_id}"
        }

        Log.i("Result344", "parameterMap : $parameterMap")
        val mDisposable = apiService.callEmployeeDetails(parameterMap)
        CompositeDisposable().add(mDisposable
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe({ result ->
                    if (result != null) {
                        Log.i("Result", "" + result.toString())
                        if (result.status == "0") {
                            Preferences.setPreference(applicationContext, "logindata", Gson().toJson(result))
                            val logindata =
                                    Gson().fromJson(Preferences.getPreference(applicationContext, "logindata"), GetEmployeeLogin::class.java)
                            if (logindata != null && "${logindata!!.timeout_lock}" == "1") {
                                overridePendingTransition(R.anim.slide_in_left, R.anim.slide_in_right)
                                val goIntent = Intent(this, StoreManagerActivity::class.java)
                                goIntent.putExtra("to", "splash")
                                startActivity(goIntent)
                                finish()
                            }
                            CUC.displayToast(applicationContext, result.show_status, result.message)
                        } else if (result.status == "10") {
                            apiClass!!.callApiKey(4)
                        } else {
                            CUC.displayToast(applicationContext, result.show_status, result.message)
                        }
                    } else {
                        // CUC.displayToast(applicationContext, "0", getString(R.string.connection_to_database_failed))
                    }
                }, { error ->
                    CUC.displayToast(applicationContext, "0", getString(R.string.connection_to_database_failed))
                    error.printStackTrace()
                })
        )
    }
}
