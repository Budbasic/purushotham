package com.budbasic.posconsole

import android.content.Context
import android.support.multidex.MultiDex
import android.support.multidex.MultiDexApplication
import com.crashlytics.android.Crashlytics
import com.github.omadahealth.lollipin.lib.managers.AppLock
import com.github.omadahealth.lollipin.lib.managers.AppLockActivity
import com.github.omadahealth.lollipin.lib.managers.LockManager
import io.fabric.sdk.android.Fabric

/**
 * Created by deep jethva on 5/6/15.
 */

class TerminalApplication : MultiDexApplication() {

    override fun attachBaseContext(base: Context) {
        super.attachBaseContext(base)
        MultiDex.install(this)
    }

    override fun onCreate() {
        super.onCreate()
        Fabric.with(this, Crashlytics())
        val lockManager = LockManager.getInstance()
        lockManager.enableAppLock(this, AppLockActivity::class.java)
        lockManager.appLock.logoId = AppLock.LOGO_ID_NONE
        lockManager.appLock.isFingerprintAuthEnabled = false
        lockManager.appLock.setShouldShowForgot(false)
    }
}