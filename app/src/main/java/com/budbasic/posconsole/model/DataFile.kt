package com.kotlindemo.model

import com.google.gson.JsonObject

/**
 * Created by ww21 on 19-12-2017.
 */
data class GetApiKey(var api_key: String)

data class GetStatus(var inventory_qty: String = "", var status: String, var show_status: String, var message: String,var msg: String
                     , var grade_message: String = "" ,var patient_id: String = "" , var image: String = "", var grade_status: String = "", var grade_warning: String = "",
                     var grade_warning_status: String = "", var total_qty: String,var till_check: String,var store_chk: String,var date: String)

data class GetUpdateCart(var grade_message: String = "", var grade_status: String, var grade_warning: String,
                         var grade_warning_status: String, var total_qty: String = "", var status: String = "", var show_status: String = "", var message: String = "")
data class getCartadd(var grade_message: String = "", var grade_status: String, var grade_warning: String, var grade_warning_status: String,
                      var total_qty: String = "",var status: String,var show_status: String,var message: String)
data class GetEmployeeLogin(var timeout_lock: String, var timeout_limit: String, var user_pin:
String, var user_store: String, var user_store_address: String, var user_store_id: String, var user_role_name: String, var user_email: String, var user_role: String,
                            var user_id: String, var user_show_name: String, var status: String, var show_status: String, var message: String)


data class GetEmployeeAppSetting(var Scanner_key_employee_android: String, var Currency_value: String, var Explaination: String, var terminal_id: String,
                                 var terminal_device_id: String, var terminal_registration: String, var terminal_expiry: String, var terminal_status: String,
                                 var App_driver_sign_note: String, var company_image: String, var status: String, var show_status: String, var message: String,
                                 var receiptlogo: String,var emp_image: String, var changeamount_lable: String, var amountpaid_lable: String, val patient_image_path: String, val patient_documents_path: String?)

/*
* Employee SearchPatient
* [START]
* */
data class GetEmployeeSearchPatient(var status: String, var show_status: String, var message: String, var offset: String, var patient_data: ArrayList<Patient_data>)

data class Patient_data(var patient_id: String,var patient_metrcid: String,var patient_metrcdelete: String, var patient_fname: String,
                        var patient_lname: String, var patient_reffered: String,var patient_dl_front: String,var patient_dl_back: String,
                        var patient_mmmp_front: String, var patient_mmmp_back: String,var patient_mmpp1: String,var patient_mmpp2: String,
                        var patient_mmpp3: String,var patient_mmpp4: String,var patient_mmpp5: String,var patient_dob: String,
                        var patient_default_store: String,
                        var patient_mobile_no: String, var patient_phone_no: String, var patient_licence_no: String, var patient_licence_exp_date: String,
                        var patient_mmpp_licence_no: String, var patient_mmpp_licence_exp_date: String, var patient_address: String, var patient_city: String,
                        var patient_state: String, var patient_country: String, var patient_zipcode: String, var patient_mobile_app: String,
                        var patient_type: String, var patient_loyalty_group: String, var patient_email_id: String, var patient_status: String,
                        var patient_regester_at: String, var patient_last_updated: String, var patient_updated_by: String, var store_name: String,
                        var type_name: String,
                        var patient_image: String, var loyalty_name: String, var patient_notes: String, var patient_documents: String,
                        var patient_profilepic: String)



/*
* Employee SearchPatient
* [END]
* */

/*
* Employee PreCustomer
* [START]
* */


data class GetEmployeePreCustomer(var status: String, var show_status: String, var message: String, var offset: String,
                                  var patient_type_data: ArrayList<Getpatient_type_data>, var loyalty_data: ArrayList<GetLoyalty_data>,
                                  var state_data: ArrayList<GetAreaData>)

data class Getpatient_type_data(var patient_type_id: String, var type_name: String, var type_allowed_quantity: String, var type_updated_by: String,
                                var type_last_updated: String, var type_status: String)

data class GetLoyalty_data(var discount_id: String, var discount_company_id: String, var discount_store_id: String, var discount_type_id: String,
                           var discount_name: String, var discount_classification: String, var discount_price: String, var discount_applied_on: String,
                           var discount_category_id: String, var discount_product_id: String, var discount_image: String, var discount_notification_msg: String,
                           var discount_limit_set: String, var discount_barcode: String, var discount_start_date: String, var discount_end_date: String,
                           var discount_last_updated: String, var discount_updated_by: String, var discount_status: String)

data class GetLocationData(var status: String, var show_status: String, var message: String, var state_id: String, var area_data: ArrayList<GetAreaData>)

data class GetAreaData(var state: String, var state_id: String, var state_code: String, var city_id: String, var city: String)
/*
* Employee PreCustomer
* [END]
* */

/*
* Employee Orders
* [START]
* */
data class GetEmployeeOrders(var assigned_order: GetStoreOrders, var status: String, var show_status: String, var message: String, var offset: String,
                             var count_cart_orders: String, var count_accepted_orders: String, var count_invoiced_orders: String, var count_payment_orders: String,
                             var store_orders: ArrayList<GetStoreOrders>, var invoice_orders: ArrayList<GetStoreOrders>, var enroute: ArrayList<GetStoreOrders>)



data class GetStoreOrders(var cart_id: String, var cart_order_unique_no: String, var cart_store_id: String, var cart_patient_id: String,
                          var cart_status: String, var cart_main_extra_status: String, var cart_last_updated: String, var cart_updated_by: String,
                          var patient_id: String, var patient_fname: String, var patient_lname: String, var patient_reffered: String,
                          var patient_mobile_no: String, var patient_phone_no: String, var patient_licence_no: String, var patient_licence_exp_date: String,
                          var patient_mmpp_licence_no: String, var patient_mmpp_licence_exp_date: String, var patient_address: String, var patient_city: String,
                          var patient_state: String, var patient_country: String, var patient_zipcode: String, var patient_mobile_app: String,
                          var patient_status: String, var patient_regester_at: String, var patient_last_updated: String, var patient_updated_by: String,
                          var invoice_id: String, var invoice_no: String, var invoice_order_unique_no: String, var invoice_store_id: String,
                          var invoice_patient_id: String, var invoice_sub_total: String, var invoice_total_qty: String, var invoice_taxes: String,
                          var invoice_grand_total: String, var invoice_payment_id: String, var invoice_status: String, var invoice_serve_type: String,
                          var invoice_company_id: String, var invoice_total_payble: String, var invoice_void: String, var invoice_cancel_request: String,
                          var invoice_promo_id: String, var invoice_promo_discount: String, var invoice_promo_code: String,
                          var invoice_cancel_reason: String, var invoice_delivery_address: String, var invoice_delivery_lat: String, var invoice_delivery_log: String,
                          var invoice_delivery_states: String, var invoice_delivery_driver: String, var invoice_delivery_signature: String, var invoice_terminal_id: String,
                          var invoice_manual_discount_type: String, var invoice_manual_discount: String, var patient_caregiver: String, var patient_caregiver_id: String,
                          var invoice_loyalty_redeem: String, var invoice_created_date: String, var invoice_created_by: String)

/*
* Employee Orders
* [END]
* */
/*
* Employee Patient Cart
* [START]
* */
data class GetEmployeePatientCart(var cart_id: String, var total_qty: String, var subtotal: String, var status: String, var show_status: String,
                                  var patient_point: String, var loyalty_amount: String, var total_payble: String, var main_cart: GetMainCart,
                                  var promosales_amount: String, var promocode_name: String, var promosales_id: String,var discount_code: String,
                                  var payment_types: ArrayList<GetPaymentTypes>, var payment_path: String, var Store_sign_required: String,
                                  var message: String,var manager_discount: String, var cart_data: ArrayList<GetCartData>, var tax_data: ArrayList<GetTaxData>,val Limit:String?,val spent_limit:String?)

data class GetMainCart(var cart_id: String, var cart_order_unique_no: String, var cart_store_id: String, var cart_patient_id: String,
                       var cart_status: String, var cart_main_extra_status: String, var cart_last_updated: String, var cart_serve_type: String,
                       var cart_delivery_address: String, var cart_delivery_lat: String, var cart_delivery_log: String,
        //var cart_loyalty_points: String,
                       var cart_updated_by: String, var cart_manual_discount_type: String, var cart_manual_discount: String,
                       var cart_ref_queue_id: String, var cart_caregiver_id: String)

data class GetCartData(var cart_detail_id: String, var cart_detail_main_id: String, var cart_detail_product: String,var cart_detail_fix_disc: String,
                       var cart_detail_qty: String,var cart_detail_per_disc: String,var packet_id: String,var packet_name: String,
                       var cart_detail_price: String, var cart_detail_status: String, var cart_detail_date: String, var cart_updated_by: String,
                       var cart_detail_packet_id: String, var cart_detail_category_id: String, var cart_detail_unit_gram: String, var cart_detail_total_price: String,
                       var cart_detail_tax_id: String, var cart_detail_total_tax: String, var packet_barcode: String,
                       var product_packets: GetProductPackets,
                       var product_name: String, var product_sku: String, var product_mapping: GetProduct)

data class GetProduct(var p_map_id: String, var p_map_qty: String, var p_map_product_id: String, var p_map_store_id: String,
                      var p_map_qty_gram: String, var p_map_store_name: String, var p_map_store_category_id: String,
                      var p_map_category_type: String, var p_map_company_id: String, var product_company_id: String, var product_state_unit: String,
                      var p_map_status: String, var product_id: String, var product_name: String, var product_sku: String,
                      var product_cat_id: String, var product_price: String, var product_status: String,
                      var product_last_updated: String, var product_type: String, var product_qty: String, var product_default_qty: String,
                      var product_location: String, var product_tax: String, var product_alert_unit: String, var product_image: String,
                      var p_map_base_price: String, var p_map_tax_status: String, var p_map_tax_available: String,
                      var product_unit: String, var product_unit_name: String, var product_tag: String, var product_indica: String,
                      var product_sativa: String, var product_thc: String, var product_thca: String, var product_cbd: String,
                      var product_cbda: String, var product_cbn: String, var product_who_tested_medicine: String,
                      var products_genetics: String, var product_sclabs_report_id: String, var product_lab_batch_number: String,
                      var product_lab_licenceno: String, var product_medicine_amount: String, var product_medicine_measurement: String, var product_ingredients: String,
                      var unit_short_name: String)

data class GetTaxData(var tax_type_id: String, var tax_name: String, var tax_total: String, var tax_status: String = "0")
/*
* Employee Patient Cart
* [END]
* */
/*
* Employee Driver Orders
* [START]
* */

data class GetEmployeeDriverOrder(var status: String, var show_status: String, var message: String, var payment_path: String, var assigned_store: String,
                                  var payment_types: ArrayList<GetPaymentTypes>, var pickups: ArrayList<GetPickups>, var order_data: ArrayList<GetOrderData>)

data class GetPickups(var assigned_invoice_id: String, var invoice_order_no: String, var invoice_no: String, var delivery_address: String,
                      var patient_name: String, var patient_mobile_no: String, var patient_lat: String, var patient_log: String,
                      var invoice_total_payble: String,
                      var patient_licence: String, var patient_mmmp_licence: String)

data class GetEmployeeDriverOrders(var assigned_store: String, var store_name: String, var store_address: String, var store_contact: String,
                                   var delivery_address: String, var patient_name: String, var patient_mobile_no: String, var payment_path: String,
                                   var patient_lat: String, var patient_log: String, var patient_licence: String, var patient_mmmp_licence: String,
                                   var assigned_invoice_id: String, var store_lat: String, var store_log: String, var payment_types: ArrayList<GetPaymentTypes>,
                                   var status: String, var show_status: String, var message: String, var order_data: ArrayList<GetOrderData>)

data class GetOrderData(var invoice_id: String, var invoice_no: String, var invoice_order_unique_no: String, var invoice_store_id: String,
                        var invoice_patient_id: String, var invoice_sub_total: String, var invoice_total_qty: String, var invoice_taxes: String,
                        var invoice_grand_total: String, var invoice_payment_id: String, var invoice_status: String, var invoice_serve_type: String,
                        var invoice_delivery_address: String, var invoice_serve_status: String, var invoice_created_date: String, var invoice_created_by: String,
                        var store_name: String, var store_lat: String, var store_log: String, var patient_fname: String,
                        var patient_lname: String, var patient_address: String, var patient_zipcode: String, var patient_mobile_no: String,
                        var distance: String)
/*
* Employee Driver Orders
* [END]
* */

/*
* Employee Generate Invoice
* [START]
* */
data class GetEmployeeQenerateInvoice(var total_qty: String, var subtotal: String, var status: String, var show_status: String, var message: String,
                                      var invoice_data: GetInvoiceData, var patient_data: GetPatientData, var store_data: GetStoreData,
                                      var payment_data: GetPaymentTypes,
                                      var invoice_details: ArrayList<GetInvoiceDetails>, var invoice_tax: ArrayList<GetInvoiceTax>)

data class GetInvoiceData(var invoice_id: String, var invoice_no: String, var invoice_order_unique_no: String, var invoice_store_id: String,
                          var invoice_patient_id: String, var invoice_sub_total: String, var invoice_total_qty: String, var invoice_taxes: String,
                          var invoice_grand_total: String, var invoice_payment_id: String, var invoice_status: String, var invoice_serve_type: String,
                          var invoice_delivery_address: String, var invoice_delivery_states: String, var invoice_delivery_driver: String,
                          var invoice_manual_discount_type: String, var invoice_manual_discount: String, var invoice_total_payble: String,
                          var invoice_promo_id: String, var invoice_promo_discount: String, var invoice_promo_code: String, var invoice_loyalty_redeem: String,
                          var invoice_paid_amount: String,var invoice_void: String, var invoice_change: String, var invoice_created_date: String, var invoice_created_by: String)

data class GetPatientData(var patient_id: String, var patient_fname: String, var patient_lname: String, var patient_reffered: String,
                          var patient_mobile_no: String, var patient_phone_no: String, var patient_licence_no: String, var patient_licence_exp_date: String,
                          var patient_mmpp_licence_no: String, var patient_mmpp_licence_exp_date: String, var patient_address: String, var patient_city: String,
                          var patient_state: String, var patient_country: String, var patient_zipcode: String, var patient_mobile_app: String,
                          var patient_type: String, var patient_loyalty_group: String, var patient_email_id: String, var patient_status: String,
                          var patient_regester_at: String, var patient_last_updated: String, var patient_updated_by: String)

data class GetStoreData(var store_id: String, var store_company_id: String, var store_name: String, var store_email: String,
                        var store_address: String, var store_contact: String, var store_uniq_code: String, var store_username: String,
                        var store_password: String, var store_status: String, var per_day_buy_quantity: String, var store_last_updated: String, var store_lat: String,
                        var store_log: String, var store_last_location: String, var store_updated_by: String)


data class GetInvoiceDetails(var invoice_detail_id: String, var invoice_detail_main_id: String, var invoice_detail_product: String, var invoice_detail_qty: String,
                             var invoice_detail_price: String, var invoice_detail_unit_gram: String, var invoice_detail_created_date: String, var product_name: String,
                             var invoice_detail_packet_id: String, var invoice_detail_status: String, var invoice_detail_total_price: String, var invoice_detail_tax_data: String,
                             var product_sku: String, var product_unit: String, var unit_name: String, var packet_barcode: String, var unit_short_name: String,
                             var Subtotal: String)

data class GetInvoiceTax(var tax_name: String, var tax_value: String, var tax_total: String)
/*
* Employee Generate Invoice
* [END]
* */


/*
* Driver Pickup Order
* [START]
* */
data class GetEmployeeDriverPickupOrder(var status: String, var show_status: String, var message: String, var order_details: GetOrderDetails)

data class GetOrderDetails(var delivery_address: String, var patient_name: String, var patient_mobile_no: String, var patient_lat: String, var patient_log: String)
/*
* Driver Pickup Order
* [END]
* */


/*
* Employee SearchPatient
* [START]
* */
data class GetEmployeeInvoiceDetails(var status: String, var show_status: String, var message: String, var offset: String, var payment_path: String, var invoice: GetInvoiceData,
                                     var Store_sign_required: String, var invoice_detail: ArrayList<GetInvoiceDetail>, var drivers: ArrayList<GetDrivers>, var payment_types: ArrayList<GetPaymentTypes>)

data class GetInvoiceDetail(var invoice_detail_id: String, var invoice_detail_main_id: String, var invoice_detail_product: String, var invoice_detail_packet_id: String,
                            var invoice_detail_qty: String, var invoice_detail_price: String, var invoice_detail_total_price: String, var invoice_detail_tax_data: String,
                            var invoice_detail_status: String, var invoice_detail_created_date: String, var product_name: String, var product_sku: String,
                            var unit_short_name: String,var invoice_detail_void: String,
                            var packet_barcode: String, var Subtotal: String, var invoice_detail_unit_gram: String, var product_unit: String, var unit_name: String)

data class GetDrivers(var emp_id: String, var emp_role_id: String, var emp_store_id: String, var emp_first_name: String,
                      var emp_last_name: String, var emp_mobile_number: String, var emp_driver_licence: String, var emp_licence_expire_date: String,
                      var emp_address_one: String, var emp_address_two: String, var emp_city: String, var emp_state: String,
                      var emp_country: String, var emp_zipcode: String, var emp_joining_date: String, var emp_status: String,
                      var emp_email: String, var emp_password: String, var emp_current_lat: String, var emp_current_log: String,
                      var emp_last_location_update: String, var emp_link: String, var emp_link_datetime: String, var emp_creted_by: String,
                      var emp_updated_by: String, var emp_requirepin: String, var emp_last_updated_date: String)

data class GetPaymentTypes(var p_map_id: String, var p_map_pay_id: String, var p_map_store_id: String, var p_map_status: String,
                           var payment_id: String, var payment_method: String, var payment_detail: String, var payment_logo: String,
                           var payment_status: String, var payment_last_updated: String, var payment_updated_by: String)

/*
* Employee SearchPatient
* [END]
* */
/*
* Direction File
* [START]
* */
data class GetDirectionFile(var status: String, var geocoded_waypoints: ArrayList<GetGeocodedWaypoints>, var routes: ArrayList<GetRoutes>)

data class GetGeocodedWaypoints(var geocoder_status: String, var place_id: String, var types: ArrayList<String>)

data class GetRoutes(var bounds: GetBounds, var copyrights: String, var summary: String, var overview_polyline: GetOverview_polyline,
                     var warnings: ArrayList<String>, var waypoint_order: ArrayList<String>, var legs: ArrayList<GetLegs>)

data class GetBounds(var northeast: GetNortheast, var southwest: GetSouthwest)
data class GetNortheast(var lat: String, var lng: String)
data class GetSouthwest(var lat: String, var lng: String)
data class GetLegs(var distance: GetDistance, var duration: GetDuration, var end_address: String, var end_location: GetEnd_location, var start_address: String,
                   var start_location: GetStart_location, var steps: ArrayList<GetSteps>, var traffic_speed_entry: ArrayList<String>, var via_waypoint: ArrayList<String>)

data class GetDistance(var text: String, var value: Int)
data class GetDuration(var text: String, var value: Int)
data class GetEnd_location(var lat: Double, var lng: Double)
data class GetStart_location(var lat: Double, var lng: Double)
data class GetSteps(var distance: GetDistance, var duration: GetDuration, var end_location: GetEnd_location, var start_location: GetStart_location,
                    var html_instructions: String, var travel_mode: String, var polyline: GetPolyline)

data class GetPolyline(var points: String)
data class GetOverview_polyline(var points: String)
/*
* Direction File
* [END]
* */

data class GetEmployeeCheckPatient(var show_popup_aquire: String, var patient_id: String, var status: String, var show_status: String, var message: String)

data class GetSettingList(var id: Int, var imageID: Int, var itemName: String)

data class GetAppqValidateQueue(var status: String, var show_status: String, var message: String, var patient_status: String,
                                var patient_id: String, var patient_status_info: String, var patient_licence: String)

data class LicenceData(var driving_license: String, var patient_dob: String, var Timestamp: String, var Address: String, var Name: String,
                       var lName: String, var Postal: String, var state_code: String, var City: String)

data class GetEmployeeQueueList(var status: String, var show_status: String, var message: String, var queue_list: ArrayList<GetQueueList>)

data class GetQueueList(var queue_id: String, var queue_company_id: String, var queue_store_id: String, var queue_patient_id: String, var queue_number: String,
                        var patient_licence_number: String, var licence_city: String, var first_name: String, var last_name: String, var licence_exp_date: String,
                        var patient_mmmp_licence: String, var queue_desc: String, var queue_start_time: String, var queue_sales_start_time: String, var queue_status: String,
                        var queue_pushed_accept: String, var queue_pushed_by: String, var queue_minutes: String, var is_closed: String, var queue_created_by: String, var queue_created_date: String, var queue_updated_by: String, var queue_updated_date: String,
                        var queue_licence_data: String, var patient_image: String?)

data class queue_licence_data(var licence_data: LicenceData)

data class GetEmployeeInvoiceTracking(var status: String, var show_status: String, var message: String, var track_details: ArrayList<GetTrackDetails>)

data class GetTrackDetails(var i_track_id: String, var i_invoice_id: String, var i_track_emp_id: String, var i_track_desc: String,
                           var i_track_lat: String, var i_track_log: String, var i_track_datetime: String, var i_track_terminal: String,
                           var terminal_name: String, var terminal_device_id: String, var emp_first_name: String, var emp_last_name: String,
                           var emp_email: String)
//
//data class GetEmployeeSalesQueues(var status: String, var show_status: String, var message: String, var get_sales_queue: ArrayList<GetGetSalesQueue>,
//                                  var queue_acceoted: String, var Queue: GetGetSalesQueue)
//
//data class GetGetSalesQueue(var queue_id: String, var queue_company_id: String, var queue_store_id: String, var queue_patient_id: String, var queue_number: String,
//                            var patient_licence_number: String, var licence_city: String, var first_name: String, var last_name: String, var licence_exp_date: String,
//                            var patient_mmmp_licence: String, var queue_desc: String, var queue_start_time: String, var queue_sales_start_time: String, var queue_status: String,
//                            var queue_pushed_accept: String, var queue_pushed_by: String, var queue_minutes: String, var is_closed: String, var queue_created_by: String, var queue_created_date: String, var queue_updated_by: String, var queue_updated_date: String,
//                            var queue_licence_data: String)

//Employee Patient Products
data class GetEmployeeSalesProducts(var grade_message: String,var grade_status: String,var grade_warning: String,var grade_warning_status: String,
                                    var status: String,var Limit: String,var Orderd_qty: String, var show_status: String, var message: String,
                                    var image_path: String, var pro_cat: GetProCat)

data class GetProCat(var data: ArrayList<GetData>)
data class GetData(var category: GetCategory)
data class GetCategory(var c_map_id: String, var c_map_cat_id: String, var c_map_store_id: String, var c_map_status: String,
                       var category_id: String, var category_name: String,var p_map_category_type:String?,var category_parent_id: String,
                       var category_status: String,
                       var category_last_updated: String, var category_updated_by: String, var product: ArrayList<GetSalesProduct>?)

data class GetSalesProduct(var p_map_id: String, var p_map_qty: String, var p_map_product_id: String, var p_map_store_id: String,
                           var p_map_qty_gram: String, var p_map_store_name: String, var p_map_store_category_id: String,
                           var p_map_category_type: String, var p_map_company_id: String, var product_company_id: String, var product_state_unit: String,
                           var p_map_status: String, var product_id: String, var product_name: String, var product_sku: String,
                           var product_cat_id: String, var product_price: String, var product_status: String,
                           var product_last_updated: String, var product_type: String, var product_qty: String, var product_default_qty: String,
                           var product_location: String, var product_tax: String, var product_alert_unit: String, var product_images: String,
                           var p_map_base_price: String, var p_map_tax_status: String, var p_map_tax_available: String,
                           var product_unit: String, var product_unit_name: String, var product_tag: String, var product_indica: String,
                           var product_sativa: String, var product_thc: String, var product_thca: String, var product_cbd: String,
                           var product_cbda: String, var product_cbn: String, var product_who_tested_medicine: String,
                           var products_genetics: String, var product_sclabs_report_id: String, var product_lab_batch_number: String, var unit_short_name: String,
                           var product_lab_licenceno: String, var product_medicine_amount: String, var product_medicine_measurement: String, var product_ingredients: String,
                           var product_packets: GetProductPackets, var product_sub_image: ArrayList<GetProductSubImage>,
                           var product_taxes: ArrayList<GetProductTaxes>,var category_name:String?,var packet_base_price: String,
                           var packet_name: String,var packet_qty: String,var packet_id: String)

data class GetSalesProductScan(var p_map_id: String, var p_map_qty: String, var p_map_product_id: String, var p_map_store_id: String,
                           var p_map_qty_gram: String, var p_map_store_name: String, var p_map_store_category_id: String,
                           var p_map_category_type: String, var p_map_company_id: String, var product_company_id: String, var product_state_unit: String,
                           var p_map_status: String, var product_id: String, var product_name: String, var product_sku: String,
                           var product_cat_id: String, var product_price: String, var product_status: String,
                           var product_last_updated: String, var product_type: String, var product_qty: String, var product_default_qty: String,
                           var product_location: String, var product_tax: String, var product_alert_unit: String, var product_images: String,
                           var p_map_base_price: String, var p_map_tax_status: String, var p_map_tax_available: String,
                           var product_unit: String, var product_unit_name: String, var product_tag: String, var product_indica: String,
                           var product_sativa: String, var product_thc: String, var product_thca: String, var product_cbd: String,
                           var product_cbda: String, var product_cbn: String, var product_who_tested_medicine: String,
                           var products_genetics: String, var product_sclabs_report_id: String, var product_lab_batch_number: String, var unit_short_name: String,
                           var product_lab_licenceno: String, var product_medicine_amount: String, var product_medicine_measurement: String, var product_ingredients: String,
                           var product_packets: ArrayList<GetProductPackets>, var product_sub_image: ArrayList<GetProductSubImage>,
                           var product_taxes: ArrayList<GetProductTaxes>,var category_name:String?,var packet_base_price: String,
                           var packet_name: String,var packet_qty: String,var packet_id: String)

data class GetProductPackets(var packet_id: String, var packet_store_id: String, var packet_product_id: String, var packet_barcode: String,
                             var packet_qty: String, var packet_base_price: String, var packet_total_price: String,
                             var packet_status: String,var packet_unit: String, var packet_last_updated: String, var isSelected: Boolean = false)

data class GetProductSubImage(var product_image_id: String, var product_id: String, var product_sub_image: String, var is_default: String,
                              var product_image_status: String, var product_image_last_updated: String, var product_image_updated_by: String)

data class GetProductTaxes(var tax_name: String, var tax_rate: String)

data class GetScanProducts(var status: String, var show_status: String, var message: String, var image_path: String, var product_block: GetProductBlock)
data class GetProductBlock(var data: GetScanData)
data class GetScanData(var category: GetScanCategory)
data class GetScanCategory(
        var p_map_store_category_id: String, var p_map_category_type: String, var category_id: String, var category_company_id: String,
        var category_name: String, var category_parent_id: String, var category_status: String, var category_base_unit: String,
        var category_last_updated: String, var category_updated_by: String, var category_type_id: String, var category_type_name: String,
        var category_type_status: String, var category_type_created_by: String, var category_type_created_date: String, var category_type_updated_by: String,
        var category_type_updated_date: String, var product: GetSalesProductScan)

data class GetEmployeeSearchProducts(var status: String, var show_status: String, var message: String, var products: ArrayList<GetProducts>)
data class GetProducts(var packet_id: String, var packet_name: String, var packet_barcode: String, var product_tag: String)


data class GetEmployeeSearchCaregiver(var status: String, var show_status: String, var message: String, var image_path: String, var caregiver_data: ArrayList<GetCaregiverData>)
data class GetCaregiverData(var caregiver_id: String, var caregiver_name: String, var caregiver_licence_no: String)

data class GetEmployeeSalesSearchPatient(var assign_msg: String, var status: String, var show_status: String, var message: String, var patient: ArrayList<GetPatient>, var caregiver: ArrayList<GetPatient>)
data class GetPatient(var patient_id: String, var Patientname: String, var patient_mobile_no: String, var patient_licence_no: String,
                      var patient_licence_exp_date: String, var patient_mmpp_licence_no: String, var patient_mmpp_licence_exp_date: String,
                      var patient_caregiver: String, var patient_caregiver_id: String, var patient_company: String, var haveCompany: String)

data class GetEmployeeSalesCreateQueue(var status: String, var show_status: String, var message: String, var queue: GetQueueList, var patient: Patient_data)

data class GetEmployeeSalesProductInfo(var status: String, var show_status: String, var message: String, var image_path: String = "", var inventory_qty: String,
                                       var product_data: GetProductData, var category_data: GetCategoryData, var category_type_data: GetCategoryTypeData, var unit_data: GetUnitData,
                                       var products_images: String, var packet_data: GetProductPackets, var tax_data: ArrayList<GetProTaxData>)


/*
data class GetProductData(var product_id: String,
                          var metrc_product: String, var product_suggested_id: String,var product_metrc_product_id: String,var product_company_id: String,var product_store_id: String,
        var product_metrcupd: String,var product_metrcdelete: String,var product_company_type: String,var product_name: String,
                var product_strainid: String,var product_strainname: String,var product_brand: String,var product_grade: String,
                        var product_sku: String,var product_cat_id: String,var product_metrc_ref_id: String,var product_cat_type_id: String, var product_cat_mapping_id: String,
                          var product_price: String, var custom_name1: String, var custom_name2: String, var product_price_decimal_digit: String, var product_status: String, var product_last_updated: String, var product_type: String,
                          var product_default_qty: String, var product_qty: String, var product_location: String, var product_tax: String, var product_tax_available: String,
                          var product_unit: String, var product_state_unit: String, var product_alert_unit: String, var product_image: String, var product_tag: String,
                          var product_indica: String, var product_sativa: String, var product_thc: String, var product_thca: String, var product_cbd: String, var product_cbda: String,
                          var product_cbn: String, var product_who_tested_medicine: String, var products_genetics: String, var product_sclabs_report_id: String, var product_lab_batch_number: String, var product_lab_licenceno: String,
                          var product_medicine_amount: String, var product_medicine_measurement: String, var product_ingredients: String, var productlab_testing: String, var ApprovalStatus: String,
                          var product_admin_method: String, var product_thcn: String, var product_uvolume: String, var product_uvolmname: String, var product_uweight: String, var product_uweightmname: String, var product_serving: String, var product_supplydur: String,
                          var product_unitqty: String, var product_unitqty_mname: String, var product_used: String, var product_taxavailable: String, var product_lab_testing: String,var Approval_Status: String)
*/


data class GetProductData(var p_map_id: String, var p_map_qty: String, var p_map_product_id: String, var p_map_store_id: String,
                          var p_map_qty_gram: String, var p_map_store_name: String, var p_map_store_category_id: String,
                          var p_map_category_type: String, var p_map_company_id: String, var product_company_id: String, var product_state_unit: String,
                          var p_map_status: String, var product_id: String, var product_name: String, var product_sku: String,
                          var product_cat_id: String, var product_price: String, var product_status: String,
                          var product_last_updated: String, var product_type: String, var product_qty: String, var product_default_qty: String,
                          var product_location: String, var product_tax: String, var product_alert_unit: String, var product_image: String,
                          var p_map_base_price: String, var p_map_tax_status: String, var p_map_tax_available: String,
                          var product_unit: String, var product_unit_name: String, var product_tag: String, var product_indica: String,
                          var product_sativa: String, var product_thc: String, var product_thca: String, var product_cbd: String,
                          var product_cbda: String, var product_cbn: String, var product_who_tested_medicine: String,
                          var products_genetics: String, var product_sclabs_report_id: String, var product_lab_batch_number: String, var unit_short_name: String,
                          var product_lab_licenceno: String, var product_medicine_amount: String, var product_medicine_measurement: String, var product_ingredients: String)

data class GetCategoryData(var category_name: String)
data class GetCategoryTypeData(var category_type_name: String)
data class GetUnitData(var unit_name: String, var unit_short_name: String)
data class GetProTaxData(var tax_name: String, var tax_rate: String)

data class GetEmployeeApplyStmanDiscount(var status: String, var show_status: String, var message: String, var invoice_data: GetInvoiceData)


data class EmployeeUnits(var status: String, var show_status: String, var message: String, val Unit: ArrayList<unit>)
data class unit(var unit_id: String, var unit_name: String, var unit_short_name: String, var unit_base_value: String,
                var unit_in_gram: String, var unit_status: String, var unit_last_updated: String, var unit_updated_by: String)

data class EmployeeSalesReport(var offset: String, var status: String, var show_status: String, var message: String, val invoice_orders: ArrayList<GetInvoiceOrders>)

data class GetInvoiceOrders(var void_color: String,var invoice_id: String, var invoice_no: String, var invoice_order_unique_no: String, var invoice_company_id: String,
                            var invoice_store_id: String, var invoice_patient_id: String, var invoice_caregiver_id: String, var invoice_sub_total: String,
                            var invoice_total_qty: String, var invoice_taxes: String, var invoice_grand_total: String, var invoice_total_payble: String,
                            var invoice_paid_amount: String, var invoice_change: String, var invoice_payment_id: String, var invoice_status: String,
                            var invoice_void: String, var invoice_cancel_request: String, var invoice_cancel_reason: String, var invoice_serve_type: String,
                            var invoice_delivery_address: String, var invoice_delivery_lat: String, var invoice_delivery_log: String, var invoice_delivery_states: String,
                            var invoice_delivery_return_reason: String, var invoice_delivery_driver: String, var invoice_delivery_signature: String, var invoice_signature: String,
                            var invoice_terminal_id: String, var invoice_loyalty_redeem: String, var invoice_loyalty_group: String, var invoice_manual_discount_type: String,
                            var invoice_manual_discount: String, var invoice_created_date: String, var invoice_created_by: String, var invoice_picked_by: String,
                            var invoice_ref_queue_id: String, var patient_id: String, var patient_fname: String, var patient_lname: String,
                            var patient_reffered: String, var patient_mobile_no: String, var patient_phone_no: String, var patient_licence_no: String,
                            var patient_licence_exp_date: String, var patient_mmpp_licence_no: String, var patient_mmpp_licence_exp_date: String, var patient_dob: String,
                            var patient_address: String, var patient_city: String, var patient_state: String, var patient_country: String,
                            var patient_zipcode: String, var patient_mobile_app: String, var patient_type: String, var patient_classification: String,
                            var patient_loyalty_group: String, var patient_points: String, var patient_email_id: String, var patient_password: String,
                            var patient_status: String, var patient_link: String, var patient_link_datetime: String, var patient_regester_at: String,
                            var patient_default_store: String, var patient_company: String, var patient_terminal_id: String, var patient_reg_lat: String,
                            var patient_reg_log: String, var patient_last_updated: String, var patient_updated_by: String, var patient_device_id: String,
                            var patient_device_token: String, var patient_device_type: String, var patient_caregiver: String, var patient_caregiver_id: String,
                            var store_name: String, var payment_method: String)

data class GetEmployeePatientPromocodeList(var status: String, var show_status: String, var message: String, var promosales: ArrayList<GetPromosales>)
data class GetPromosales(var discount_id: String, var discount_company_id: String, var discount_store_id: String, var discount_type_id: String,
                         var discount_name: String, var discount_classification: String, var discount_price: String, var discount_applied_on: String,
                         var discount_category_id: String, var discount_product_id: String, var discount_image: String, var discount_notification_msg: String,
                         var discount_limit_set: String, var discount_barcode: String, var discount_start_date: String, var discount_end_date: String,
                         var discount_last_updated: String, var discount_updated_by: String, var discount_status: String)

data class GetEmployeePatientApplyPromocode(var status: String, var show_status: String, var message: String, var discount: GetPromosalesObj)
data class GetPromosalesObj(var discount_id: String, var discount_company_id: String, var discount_store_id: String, var discount_type_id: String,
                            var discount_name: String, var discount_classification: String, var discount_price: String, var discount_applied_on: String,
                            var discount_category_id: String, var discount_product_id: String, var discount_image: String, var discount_notification_msg: String,
                            var discount_limit_set: String, var discount_barcode: String, var discount_start_date: String, var discount_end_date: String,
                            var discount_last_updated: String, var discount_updated_by: String, var discount_status: String)

data class GetEmployeeTerminalSales(var status: String, var show_status: String, var message: String, var store: GetStoreData, var sales_terminals: ArrayList<GetSalesTerminals>)
data class GetSalesTerminals(var terminal_id: String, var terminal_name: String, var terminal_employee_name: String, var terminal_sales: String,
                             var terminal_pettycash: String, var terminal_pettycash_alert: String, var terminal_status_percentage: String,
                             var terminal_sales_completed: String)

data class GetEmployeeStoremList(var status: String, var show_status: String, var message: String, var store: GetStoreData, var employee: ArrayList<GetDrivers>)

//for graph report screen 13/11/2018
data class GetEmployeeReportMetric(var employee: GetEmployeedata?, var master_block: GetMasterblockdata?)

data class GetEmployeedata(var emp_id: String?, var emp_company_id: String?, var emp_role_id: String?, var emp_store_id: String?, var emp_first_name: String?, var emp_last_name: String?, var emp_mobile_number: String?, var emp_driver_licence: String?, var emp_licence_expire_date: String?, var emp_address_one: String?, var emp_address_two: String?, var emp_city: String?, var emp_state: String?, var emp_country: String?, var emp_zipcode: String?, var emp_joining_date: String?, var emp_status: String?, var emp_email: String?, var emp_password: String?, var emp_current_lat: String?, var emp_current_log: String?, var emp_last_location_update: String?, var emp_link: String?, var employee_pin: String?, var emp_link_datetime: String?, var emp_creted_by: String?, var emp_updated_by: String?, var emp_last_updated_date: String?)
data class GetMasterblockdata(var store: GetStoreMasterData?, var employee: GetEmployeeMasterdata?, var items: ArrayList<items>?, var topinvoices: ArrayList<topinvoices>?)

data class GetStoreMasterData(var total_sales_store: String?, var daily_sales_store: String?, var patient_checked_in: String?, var average_amount_sold: String?)
data class GetEmployeeMasterdata(var total_sales_enployee: String?, var daily_sales_enployee: String?, var average_amount_sold: String?)

data class items(var product_name: String?, var product_image: String?, var unit_name: String?, var unit_short_name: String?, var Total_price: String?, var Total_qty: String?, var Average_sales: String?, var daily_sales: String?, var daily_sales_qty: String?)
data class topinvoices(var invoice_id: String?, var invoice_no: String?, var invoice_total_payble: String?)
// for printer
data class GetTerminalPrinter(var get_printers: ArrayList<getprinterdata>, var status: String?, var show_status: String?, var message: String?)

data class getprinterdata(var hardware_id: String?, var hardware_ip_address: String?, var hardware_last_updated: String?,
                          var hardware_mac_address: String?, var hardware_manufacturer: String?, var hardware_model: String?,
                          var hardware_status: String?, var hardware_store_id: String?,
                          var hardware_terminal_id: String?, var hardware_type_id: String?, var hardware_updated_by: String?)

//for printer register
data class GetPrinterRegister(var printer_data: getprinterdata?, var status: String?, var show_status: String?, var message: String?)

data class getEmployeeBannedPatient(var status: String?, var show_status: String?, var message: String?)


data class getEmployeePatientNoteEdit(var status: String?, var show_status: String?, var message: String?)


data class GETEmployeePatientDocument(var status: String?, var show_status: String?, var message: String?, var document_path: String?, var documents: ArrayList<PatientDocumentModel>)

data class PatientDocumentModel(var document_id: String = "", var document_patient_id: String = "", var document_title: String = "", var document_path: String = "", var document_status: String = "", var document_created_date: String = "", var document_created_by: String = "", var document_deleted_date: String = "", var document_deleted_by: String = "")


/*employee_patient_info*/


data class Favourite(var product_packets: Product_packats,var packet_category_id: String?, var packet_id: String?, var packet_image: String?, var packet_name: String?,
                     var packet_price: String?, var inventory_qty: String?)
data class Product_packats(var packet_id: String?, var packet_store_id: String?, var packet_company_id: String?, var packet_metrc_id: String?,
                     var packet_isactive: String?, var packet_name: String?, var packet_metrc_delete: String?, var packet_metrc_upd: String?, var packet_unitdefaultvalue: String?,
                           var packet_amountreduced: String?, var packet_unit: String?, var packet_roomid: String?, var packet_room: String?, var packet_splitform: String?,
                           var packet_splitqty: String?, var packet_startqty: String?, var packet_type: String?, var packet_label: String?, var packet_custom_name1: String?,
                           var packet_custom_name2: String?, var packet_product_id: String?, var packet_barcode: String?, var packet_qty: String?, var packet_pereach: String?, var packet_prevpackqty: String?, var packet_base_price: String?, var packet_total_price: String?, var packet_buy_price: String?, var packet_grade: String?, var packet_srchrvst: String?, var packet_unitabbr: String?, var packet_patient: String?,
                           var packet_productname: String?, var packet_cat: String?, var packet_date: String?, var packet_labtest: String?, var packet_labstate: String?,
                           var packet_labstatedt: String?, var packet_prodbatch: String?, var packet_batchno: String?, var packet_iststsmpl: String?,
                           var packet_isprcsvldttestsmpl: String?, var packet_reqremidat: String?, var packet_containremidpro: String?,
                           var packet_remeddate: String?, var packet_recmanifest: String?, var packet_recfrom: String?, var packet_recname: String?,
                           var packet_recdtme: String?, var packet_ishold: String?, var packet_marijuana_quantity_ingrams: String?, var packet_archvddt: String?,
                           var packet_finished: String?, var packet_vendor: String?, var packet_strain: String?, var packet_brand: String?, var packet_price_type: String?, var packet_tax: String?,
                           var packet_terpenes: String?, var packet_expdate: String?, var packet_start_of_day_quantity: String?, var packet_featured: String?, var packet_transfer_destination: String?, var packet_label_info: String?,
                           var packet_created_at_date: String?, var packet_created_at_time: String?, var packet_last_audit_date: String?, var packet_last_audit_time: String?,
                           var packet_weight_per_unit_grams: String?, var packet_weight_per_unit_ounces: String?, var packet_weight_unit_prepacked_flower: String?,
                           var packet_joint_weight: String?, var packet_calculated_weight_grams: String?, var packet_source_license: String?, var packet_batchid: String?, var packet_datch_date: String?,
                           var packet_test_date: String?, var packet_tested_by: String?, var packet_test_lotno: String?, var packet_locationdisplayname: String?, var packet_transferdestdisplayname: String?, var packet_form: String?,
                           var packet_description: String?, var packet_status: String?, var packet_used_invoice: String?, var packet_last_updated: String?)
data class InvoiceHistory(var invoice_id: String? = null, var invoice_no: String? = null, var invoice_order_unique_no: String? = null,
                          var invoice_company_id: String? = null, var invoice_store_id: String? = null, var invoice_patient_id: String? = null,
                          var invoice_caregiver_id: String? = null, var invoice_sub_total: String? = null, var invoice_total_qty: String? = null,
                          var invoice_taxes: String? = null, var invoice_tax_total: String? = null, var invoice_grand_total: String? = null,
                          var invoice_total_payble: String? = null, var invoice_paid_amount: String? = null, var invoice_change: String? = null,
                          var invoice_payment_id: String? = null, var invoice_status: String? = null, var invoice_void: String? = null, var invoice_cancel_request: String? = null,
                          var invoice_cancel_reason: String? = null, var invoice_serve_type: String? = null, var invoice_delivery_address: String? = null, var invoice_delivery_lat: String? = null,
                          var invoice_delivery_log: String? = null, var invoice_delivery_states: String? = null, var invoice_delivery_return_reason: String? = null,
                          var invoice_delivery_driver: String? = null, var invoice_delivery_signature: String? = null, var invoice_signature: String? = null,
                          var invoice_terminal_id: String? = null, var invoice_loyalty_redeem: String? = null, var invoice_loyalty_group: String? = null,
                          var invoice_promo_id: String? = null, var invoice_promo_discount: String? = null, var invoice_promo_code: String? = null,
                          var invoice_manual_discount_type: String? = null, var invoice_manual_discount: String? = null, var invoice_created_date: String? = null,
                          var invoice_created_by: String? = null, var invoice_picked_by: String? = null, var invoice_ref_queue_id: String? = null,
                          var store_name: String? = null, var payment_method: String? = null)

data class EmployeePatientInfo(var patient: Patient_data1?, var registered_since: String?, var spent_limit:String?,var Limit: String?, var documents: ArrayList<PatientDocumentModel>,
                               var document_path: String?, val invoice_history: ArrayList<InvoiceHistory>?, var favourites: ArrayList<Favourite>?, var recomended: ArrayList<Favourite>?,
                               var notes_data: ArrayList<notesdata>?,
                               var average_spent: String?, var status: String?, var show_status: String?, var message: String?)

data class notesdata(var pnote_id: String,var pnote_store: String,var pnote_patient: String,var pnote_title: String,var pnote_priority: String,
                     var pnote_notes: String,var pnote_status: String,var pnote_last_updated: String,var pnote_updated_by: String)
data class Patient_data1(var patient_id: String, var patient_fname: String, var patient_lname: String, var patient_reffered: String,
                         var patient_mobile_no: String, var patient_phone_no: String, var patient_licence_no: String, var patient_licence_exp_date: String,
                         var patient_mmpp_licence_no: String, var patient_mmpp_licence_exp_date: String, var patient_address: String, var patient_city: String,
                         var patient_state: String, var patient_country: String, var patient_zipcode: String, var patient_mobile_app: String,
                         var patient_type: String, var patient_loyalty_group: String, var patient_email_id: String, var patient_status: String,
                         var patient_regester_at: String, var patient_last_updated: String, var patient_updated_by: String, var store_name: String, var type_name: String,
                         var patient_image: String, var patient_dob: String?, var loyalty_name: String, var patient_notes: String, var patient_documents: String, var patient_registration_date: String)

data class getEmployeeStoreAnalytics(var patient_image:String?,var product_image:String?,var store_sales: EmployeeStoreSalesData?, var status:String?,var show_status: String?, var message: String?)

data class EmployeeStoreSalesData(var total_sales: String?, var total_tax:String?,var net_total:String?,var total_patient:String?,var new_patient:String?,
                                  var average_spent:String?,var item_sold:String?,val Hour_sales:ArrayList<Hour_sales>?,var Product_tab:ArrayList<ProductTabData>?,
                                  var Employee_tab:ArrayList<EmployeeTabData>?,var Patient_tab:ArrayList<PatintTabData>?)
data class Hour_sales(val time:String?, val price: String?)


data class ProductTabData(var product_id:String?,var product_name:String?,var product_image:String?,var all_sold:String?,var appear_in:String?,
                          var packet_total_sell:String?,var packet_average_qty_sold:String?,var packet_id:String?,var packet_name:String?)



data class  EmployeeTabData(var emp_id:String?,var total_selling:String?,var total_invoice:String?,var average_sell:String?,var emp_first_name:String?,
                            var emp_last_name:String?)

class PatintTabData(var total_selling:String?,var total_invoice:String?,var average_sell:String?,var patient_id:String?,var patient_fname:String?,
                     var patient_lname:String?,var patient_image:String?)


data class getEmployeeStoreAnalyticsProduct(var product_image:String?,var product_name:String?,var product_price:String?,
                                            var product_unit:String?,var product_unit_ab:String?,var product_batch:String?,var product_qty:String?,
                                            var product_qty_gram:String?,var product_solded_qty:String?,var product_solded_qty_gram:String?,var product_solded_amount:String?,
                                            var graph_data:ArrayList<Hour_sales>?,var graph_dates:GraphDates?,
                                            var store_sales: EmployeeStoreSalesData?, var status:String?,var show_status: String?, var message: String?)


data class GraphDates(var first_date:String?,var middle_date:String?,var last_date:String?)

data class getEmployeeStoreAnalyticsPatient(var patient_image:String?,var patient_name:String?,var patient_licence:String?,
                                            var patient_mmmp_licence:String?,var patient_phone:String?,var top3days:String?,
                                            var graph_data:ArrayList<Hour_sales>?,var graph_dates:GraphDates?,
                                            var store_sales: EmployeeStoreSalesData?, var status:String?,var show_status: String?, var message: String?)

data class getEmployeeStoreAnalyticsEmployee(var patient_image:String?,var product_image:String?,var emp_name:String?,var emp_license:String?,
                                            var emp_phone:String?,var top3days:String?,
                                            var graph_data:ArrayList<Hour_sales>?,var graph_dates:GraphDates?,
                                            var store_sales: EmployeeStoreSalesData?, var status:String?,var show_status: String?, var message: String?)

data class GetEmpExpenses(var expense_data:ArrayList<GetEmployeeExpanses>?)

data class GetEmployeeExpanses(var item_id:String?,var company_id:String?,var store_id:String?,var ter_id:String?,var dates:String?,var expensesitem:String?,
                               var expensedescription:String?,var amount:String?,var emp:String?,var status:String?,var created_by:String?,var updated_at:String?,var company_facility_type:String?,var company_fei_no:String?,var company_name:String?,var company_first_name:String?,
                               var company_last_name:String?,var company_description:String?,var company_email:String?,var company_address:String?, var company_address_2:String?,
                               var company_state:String?,var company_zip:String?,var company_fax:String?,var company_city_id:String?,var company_domain_address:String?
                               ,var company_contact:String?,var company_uniq_code:String?,var company_username:String?,var company_password:String?,
                               var company_status:String?,var company_image:String?, var company_image_receipt:String?, var company_last_updated:String?,
                               var company_updated_by:String?, var company_user_key:String?,var company_software_key:String?,var store_license:String?,
                               var store_medical_recreational:String?,var store_api_key:String?,var store_company_id:String?,var store_name:String?,var store_email:String?,
                               var store_fein:String?,var store_address:String?,var store_address_2:String?,var store_state:String?,var store_zip:String?,var store_fax:String?,var store_city_id:String?,
                               var store_state_id:String?,var store_type_id:String?,var store_env:String?,var store_contact:String?,var store_domain_address:String?,var store_uniq_code:String?,
                               var store_username:String?,var store_password:String?,var store_status:String?,var per_day_buy_quantity:String?,var store_last_updated:String?,var store_dispersed:String?,
                               var store_lat:String?,var store_log:String?,var store_queue_prefix:String?,var use_default_smtp:String?,/*var smtp_detail:GetEmpStatus?,*/var emp_id:String?,var emp_company_id:String?,
                               var store_have_sign:String?,var licensepackage:String?,var store_last_location:String?,var store_updated_by :String?,
                               var emp_role_id:String?,var emp_store_id:String?,var emp_first_name:String?,var emp_last_name:String?,var emp_mobile_number:String?,
                               var emp_lic_no:String?,var emp_ssn:String?, var emp_lic_type:String?,var emp_driver_licence:String?,var emp_licence_expire_date:String?,var emp_requirepin:String?,
                               var emp_address_one:String?,var emp_address_two:String?,var emp_city:String?, var emp_state:String?,var emp_country:String?,var emp_zipcode:String?,var emp_joining_date:String?,
                               var emp_status:String?,var emp_email:String?,var emp_password:String?,var emp_image:String?,var emp_current_lat:String?,var emp_current_log:String?,
                               var emp_last_location_update:String?,var emp_link:String?,var employee_pin:String?,var employee_can_backup:String?,var emp_link_datetime:String?,
                               var emp_creted_by:String?,var emp_updated_by:String?,var emp_last_updated_date:String?,var emp_eula:String?, var id:String?,var expensename:String?)
data class GetEmpStatus(var smtp_host:String?,var smtp_port:String?,var smtp_user:String?,var smtp_pass:String?)

data class GetExpensesList(var expensename:ArrayList<GetExpensesItems>?)

data class GetExpensesItems(var id:String?,var expensename:String?,var status:String?,var created_by:String?,var updated_at:String?)


data class GetDeposits(var deposit_data : ArrayList<GetDepositItems>)

data class GetDepositItems(var item_id: String?,var company_id: String?,var store_id: String?,var date: String?,var depositamount: String?,var depositbank: String?,
                           var emp: String?,var status: String?,var create_by: String?,var updated_at: String?,var company_facility_type: String?,var company_fei_no: String?,
                           var company_name: String?,var company_first_name: String?,var company_last_name: String?,var company_description: String?,var company_email: String?,var company_address: String?,
                           var company_address_2: String?,var company_state: String?,var company_zip: String?,var company_fax: String?,var company_city_id: String?,var company_domain_address: String?,
                           var company_contact: String?,var company_uniq_code: String?,var company_username: String?,var company_password: String?,var company_status: String?,var company_image: String?,
                           var company_image_receipt: String?,var company_last_updated: String?,var company_updated_by: String?,var company_user_key: String?,var company_software_key: String?,var store_license: String?,
                           var store_medical_recreational: String?,var store_api_key: String?,var store_company_id: String?,var store_name: String?,var store_email: String?,var store_fein: String?,
                           var store_address: String?,var store_address_2: String?,var store_state: String?,var store_zip: String?,var store_fax: String?,var store_city_id: String?,
                           var store_state_id: String?,var store_type_id: String?,var store_env: String?,var store_contact: String?,var store_domain_address: String?,var store_uniq_code: String?,
                           var store_username: String?,var store_password: String?,var store_status: String?,var per_day_buy_quantity: String?,var store_last_updated: String?,var store_dispersed: String?,var store_lat: String?,
                           var store_log: String?,var store_queue_prefix: String?,var use_default_smtp: String?,var store_have_sign: String?,var licensepackage: String?,var store_last_location: String?,var store_updated_by: String?,
                           var emp_id: String?,var emp_company_id: String?,var emp_role_id: String?,var emp_store_id: String?,var emp_first_name: String?,
                           var emp_last_name: String?,var emp_mobile_number: String?,var emp_lic_no: String?,var emp_ssn: String?,var emp_lic_type: String?,
                           var emp_driver_licence: String?,var emp_licence_expire_date: String?,var emp_requirepin: String?,var emp_address_one: String?,var emp_address_two: String?,
                           var emp_city: String?,var emp_state: String?,var emp_country: String?,var emp_zipcode: String?,var emp_joining_date: String?,var emp_status: String?,
                           var emp_email: String?,var emp_password: String?,var emp_image: String?,var emp_current_lat: String?,var emp_current_log: String?,var emp_last_location_update: String?,
                           var emp_link: String?,var employee_pin: String?,var employee_can_backup: String?,var emp_link_datetime: String?,
                           var emp_creted_by: String?,var emp_updated_by: String?,var emp_last_updated_date: String?,var emp_eula: String?,
                           var id: String?,var deposit: String?,var created_by: String?)

data class GetBanks(var depositebank : ArrayList<GetDepositbanklist>)
data class GetDepositbanklist(var id: String?,var deposit: String?,var status: String?,var created_by: String?,var updated_at: String?)

data class GetFinancialReport(var monthly_report : ArrayList<GetMonthlyreport>,var daily_report : ArrayList<GetDailyReport>)

data class GetMonthlyreport(var month_year: String?,var cmonths: String?,var cyears: String?,var cdates: String?,var closingbalance: String?,
                            var rank: String?,var odates: String?,var openingbalance: String?,var orank: String?,var months: String?,
                            var years: String?,var credit_cash_month_date: String?,var cash_transactions_revenue: String?,var credit_card_txn: String?,
                            var gross_revenue: String?,var expenses: String?,var total_net_sales_revenue: String?,
                            var total_sales_cash_balance: String?,var cash_bank_deposit: String?)


data class GetDailyReport(var end_of_last_day_cash: String?,var value: String?,var rank: String?,var jdate: String?,var selected_date: String?
,var id: String?,var store_id: String?,var date: String?,var credit_cash: String?,var start_of_day_cash: String?,
                          var no_of_tills_reg: String?,var no_of_tills_active: String?,var cash_per_till: String?,var starting_till_cash: String?,
                          var store_cash_after_till: String?,var ending_till_cash: String?,var credit_card_transactions: String?,var gross_revenue: String?,var month_year: String?,
                          var expenses: String?,var end_of_previous_day_cash: String?,var till_disperse_payback: String?,var end_of_day_cash: String?,
                          var cash_transactions_revenue: String?,var total_sales_cash_balance: String?,var total_net_sales_revenue: String?, var total_store_cash: String?, var cash_bank_deposit: String?)

data class GetTerminalEmployee(var cash: String?,var net: String?,var store_start: String?,var store_close: String?,var status: String?,var show_status: String?,var message: String?,
                               var store: GetStoreDatas?,var sales_terminals: ArrayList<GetsalesTerminalsdata>?)

data class GetStoreDatas(var store_id: String?,var store_license: String?,var store_medical_recreational: String?,var store_api_key: String?,var store_company_id: String?,
                        var store_name: String?,var store_email: String?,var store_fein: String?,var store_address: String?,var store_address_2: String?,
                        var store_state: String?,var store_zip: String?,var store_fax: String?,var store_city_id: String?,var store_state_id: String?,
                        var store_type_id: String?,var store_env: String?,var store_contact: String?,var store_domain_address: String?,var store_uniq_code: String?,
                        var store_username: String?,var store_password: String?,var store_status: String?,var per_day_buy_quantity: String?,var store_last_updated: String?,
                        var store_dispersed: String?,var store_lat: String?,var store_log: String?,var store_queue_prefix: String?,var use_default_smtp: String?,
                        var store_have_sign: String?,var licensepackage: String?,var store_last_location: String?,var store_updated_by: String?)

data class GetsalesTerminalsdata(var terminal_id: String?,var till_start: String?,var till_close: String?,var terminal_name: String?,
                             var terminal_employee_name: String?,var terminal_deposit: String?,var terminal_alert: String?,var current_cash: String?,
                             var terminal_variance: String?,var terminal_dropcount: String?,var terminal_totalsale: String?,var terminal_closingcash: String?)

data class GetStoreOpen(var till_disperssed: String?,var all_tills: String?,var active_tills: String?,var previous_day_cash_balance: String?,
                        var credit_bal: String?,var open_check: String?,var message: String?,var status: String?,var show_status: String?)

data class GetTillOpen(var date: String?,var balance: String?,var status: String?,var opentillCheck: String?,
                        var message: String?)

data class GetDenominations(var update: String?,var status: String?,var message: String?,var new_data : ArrayList<GetDenominationsdata>)

data class GetDenominationsdata(var tl_id: String?,var tl_company: String?,var tl_store: String?,var tl_terminal: String?,
                        var tl_store_date: String?,var tl_till_startdate: String?,var tl_till_closedate: String?,var tl_start_balance: String?,var tl_cash_sale: String?,
                            var tl_debit_sale: String?,var tl_expenses: String?,var tl_cash_drops: String?,var tl_expected_cash: String?,var tl_pennies: String?,
                            var tl_nickels: String?,var tl_dimes: String?,var tl_ones: String?,var tl_tens: String?,var tl_fifties: String?,
                            var tl_quarters: String?,var tl_fives: String?,var tl_twenties: String?,var tl_hundreds: String?,var tl_status: String?,
                            var tl_last_updated: String?,var tl_last_updatedby: String?,var tl_created_date: String?)

data class GetTillClose(var expenses: String?,var net: String?,var cash: String?,var message: String?,var status: String?,
                        var closetillCheck: String?,var show_status: String?,
                        var balance: String?,var actual_till: String?,var till_close_datetime: String?,var expected_cash: String?,
                        var total_tillamount: String?,var total_sale: String?,var variance: String?)

data class GetStoreClose(var variance: String?,var net: String?,var actual_tillsamount: String?,var previous_day_cash_balance: String?,var cash: String?,
                         var credit_cash: String?,var cash_per_till_dispersed: String?,var tills_registered: String?,var tills_active: String?,
                         var till_close_check: String?,var status: String?,var show_status: String?,var message: String?,
                         var expenses: String?,var expected_count: String?,var total_storesale: String?,var total_storecash: String?)



data class GetNotesData(var status: String?,var show_status: String?,var image: String?,var message: String?,var notes_data: GetNotes?)

data class GetNotes(var pnote_id: String?,var pnote_store: String?,var pnote_patient: String?,var pnote_title: String?,var pnote_priority: String?,
                    var pnote_notes: String?,var pnote_status: String?,var pnote_last_updated: String?,var pnote_updated_by: String?)


data class GetNotesDatashow(var status: String?,var show_status: String?,var image: String?,var message: String?,var notes : ArrayList<GetNotes>)


data class GetDocumentNotes(var documents : ArrayList<DocumentModel>,var document_path: String?,var license_path: String?,var dl_front: String?,var dl_back: String?,var mmmp_front: String?,
                            var mmmp_back: String?,var status: String?,var show_status: String?,var message: String?,var license_data: GetLicenceData?
                            )


data class GetLicenceData(var patient_id: String?,var patient_metrcid: String?,var patient_metrcdelete: String?,var patient_fname: String?,
                          var patient_lname: String?,var patient_image: String?,var patient_reffered: String?,var patient_mobile_no: String?,
                          var patient_phone_no: String?,var patient_licence_no: String?,var patient_licence_exp_date: String?,var patient_mmpp_licence_no: String?,
                          var patient_mmpp_licence_exp_date: String?,var patient_dl_front: String?,var patient_dl_back: String?,var patient_mmmp_front: String?,
                          var patient_mmmp_back: String?,var patient_mmpp1: String?,var patient_mmpp2: String?,var patient_mmpp3: String?,
                          var patient_mmpp4: String?,var document_id: String?,var patient_mmpp5: String?,var patient_dob: String?,var patient_address: String?,
                          var patient_city: String?,var patient_state: String?,var patient_country: String?,var patient_zipcode: String?,
                          var patient_mobile_app: String?,var patient_type: String?,var patient_classification: String?,var patient_loyalty_group: String?,
                          var patient_points: String?,var patient_email_id: String?,var patient_password: String?,var patient_status: String?,
                          var patient_link: String?,var patient_link_datetime: String?,var patient_regester_at: String?,var patient_default_store: String?,
                          var patient_company: String?,var patient_terminal_id: String?,var patient_reg_lat: String?,var patient_reg_log: String?,var patient_recommendedplants: String?,
                          var patient_last_updated: String?,var patient_updated_by: String?,var patient_device_id: String?,var patient_device_token: String?,
                          var patient_device_type: String?,var patient_caregiver: String?,var patient_caregiver_id: String?,var patient_license_issue_date: String?,
                          var patient_recommendedsmokablequantity: String?,var patient_metrc_status: String?,var patient_metrc_upd: String?,var patient_notes: String?,
                          var patient_other_facilitiescount: String?,var patient_registration_date: String?)

data class DocumentModel(var document_id: String= "", var document_patient_id: String = "", var document_title: String = "",
                         var document_path: String = "", var document_status: String = "", var document_created_date: String = "",
                         var document_created_by: String = "", var document_deleted_date: String = "", var document_deleted_by: String = "")

data class getcaregivers(var patient_1: getCaregiver?,var patient_2: getCaregiver?,var patient_3: getCaregiver,var patient_4: getCaregiver?,
                         var patient_5: getCaregiver?,var status: String?,var show_status: String?,var message: String?)

data class getCaregiver(var patient_id: String?,var patient_metrcid: String?,var patient_metrcdelete: String?,var patient_fname: String?,
                        var patient_lname: String?,var patient_image: String?,var patient_reffered: String?,var patient_mobile_no: String?,
                        var patient_phone_no: String?,var patient_licence_no: String?,var patient_licence_exp_date: String?,var patient_mmpp_licence_no: String?,
                        var patient_mmpp_licence_exp_date: String?,var patient_dl_front: String?,var patient_dl_back: String?,var patient_mmmp_front: String?,
                        var patient_mmmp_back: String?,var patient_mmpp1: String?,var patient_mmpp2: String?,var patient_mmpp3: String?,
                        var patient_mmpp4: String?,var patient_mmpp5: String?,var patient_dob: String?,var patient_address: String?,
                        var patient_city: String?,var patient_state: String?,var patient_country: String?,var patient_zipcode: String?,
                        var patient_mobile_app: String?,var patient_type: String?,var patient_classification: String?,var patient_loyalty_group: String?,
                        var patient_points: String?,var patient_email_id: String?,var patient_password: String?,var patient_status: String?,
                        var patient_link: String?,var patient_link_datetime: String?,var patient_regester_at: String?,var patient_default_store: String?,
                        var patient_company: String?,var patient_terminal_id: String?,var patient_reg_lat: String?,var patient_reg_log: String?,
                        var patient_last_updated: String?,var patient_updated_by: String?,var patient_device_id: String?,var patient_device_token: String?,
                        var patient_device_type: String?,var patient_caregiver: String?,var patient_caregiver_id: String?,var patient_license_issue_date: String?,
                        var patient_recommendedplants: String?,var patient_recommendedsmokablequantity: String?,var patient_metrc_status: String?,var patient_metrc_upd: String?,
                        var patient_notes: String?,var patient_other_facilitiescount: String?,var patient_registration_date: String?)



