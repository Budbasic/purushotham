package com.budbasic.posconsole.model

import android.content.Context
import android.util.Log
import com.budbasic.customer.global.Preferences
import com.budbasic.posconsole.webservice.RequetsInterface
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class APIClass(val mContext: Context, val callBack: onCallListner) {
    private val lock = java.lang.Object()

    fun callApiKey(requestCode: Int) = synchronized(lock) {
        val apiService = RequetsInterface.create()
        val parameterMap = HashMap<String, String>()
        CompositeDisposable().add(apiService.callGetApiKey(parameterMap)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe({ result ->
                    Preferences.setPreference(mContext, "API_KEY", "${result.api_key}")
                    callBack.onSuccess(requestCode)
                    Log.i("Result", "" + result.toString())
                }, { error ->
                    error.printStackTrace()
                    callBack.onFailed(requestCode)
                })
        )
    }

    interface onCallListner {
        fun onFailed(requestCode: Int)
        fun onSuccess(requestCode: Int)
    }

}