package com.budbasic.posconsole;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;

import com.budbasic.customer.global.Preferences;
import com.budbasic.global.CUC;
import com.budbasic.posconsole.queue.MessageActivity;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.kotlindemo.model.GetEmployeeAppSetting;
import com.mylibrary.BarcodeView;
import com.mylibrary.BarcodeView.ResultHandler;
import com.mylibrary.DevyBarcodeFormat;
import com.mylibrary.ScanResult;

import org.jetbrains.annotations.NotNull;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import kotlin.TypeCastException;
import kotlin.collections.CollectionsKt;
import kotlin.jvm.internal.Intrinsics;
import kotlin.text.StringsKt;

public class BarcodeSccan extends AppCompatActivity implements ResultHandler {
    private BarcodeView scannerView;
    public GetEmployeeAppSetting appSettingData;
    private int CALL_FUN = 17;
    private final int MY_BLINK_ID_CUSTOMER = 18;
    private final int MY_BLINK_ID_EMPLOYEE = 19;
    private String driving_license = "";
    private String Timestamp = "";
    private String Address = "";
    private String date = "";
    private String City = "";
    private String Name = "";
    private String lName = "";
    private String Postal = "";
    private String state_code = "";
    private String dob = "";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        this.scannerView = new BarcodeView((Context)this);
        this.setContentView((View)this.scannerView);
       // Object var10001 = (new Gson()).fromJson(Preferences.INSTANCE.getPreference((Context)this, "AppSetting"), GetEmployeeAppSetting.class);
       // Intrinsics.checkExpressionValueIsNotNull(var10001, "Gson().fromJson(Preferen…eeAppSetting::class.java)");
       // this.appSettingData = (GetEmployeeAppSetting)var10001;
        if (this.getIntent() != null && this.getIntent().hasExtra("status")) {
            if (Intrinsics.areEqual(this.getIntent().getStringExtra("status"), "0")) {
                this.CALL_FUN = this.MY_BLINK_ID_EMPLOYEE;
            } else {
                this.CALL_FUN = this.MY_BLINK_ID_CUSTOMER;
            }
        }

    }
    public void onResume() {
        super.onResume();
        BarcodeView var10000 = this.scannerView;
        if (var10000 == null) {
            Intrinsics.throwNpe();
        }

        var10000.setFormats((List) CollectionsKt.arrayListOf(new DevyBarcodeFormat[]{DevyBarcodeFormat.PDF_417}));
        var10000 = this.scannerView;
        if (var10000 == null) {
            Intrinsics.throwNpe();
        }

        var10000.setResultHandler((BarcodeView.ResultHandler)this);
        var10000 = this.scannerView;
        if (var10000 == null) {
            Intrinsics.throwNpe();
        }

        var10000.startCamera();
    }

    public void onPause() {
        super.onPause();
        BarcodeView var10000 = this.scannerView;
        if (var10000 == null) {
            Intrinsics.throwNpe();
        }

        var10000.stopCamera();
    }

    public void onBackPressed() {
        this.setResult(0);
        super.onBackPressed();
    }
    @Override
    public void handleResult(ScanResult var1) {
        System.out.println("aaaaaaaaaaaaaaa  var1    "+var1.toString());

        DevyBarcodeFormat s=var1.getBarcodeFormat();
        System.out.println("aaaaaaaaaaaa  "+s.toString());
        System.out.println("aaaaaaaaaaaa  "+s.getDeclaringClass().getName()+"   "+s.getDeclaringClass().getCanonicalName()+"   "+
                s.getDeclaringClass().getDeclaredFields()+"   "+s.getDeclaringClass().getFields());



    }


    protected void onActivityResult(int requestCode, int resultCode, @org.jetbrains.annotations.Nullable Intent data) {
        String var4 = "aaaaaaaaaa   data  " + String.valueOf(data);
        System.out.println(var4);
        if (requestCode == 444) {
            String var5 = "aaaaaaaaaa   data  " + String.valueOf(data);
            System.out.println(var5);
            Intent returnIntent = new Intent();
            returnIntent.putExtra("data", (Parcelable)data);
            this.setResult(-1, returnIntent);
            this.finish();
        } else if (requestCode == this.CALL_FUN) {
            if (resultCode == 0) {
                this.setResult(0);
                this.finish();
            } else if (resultCode == -1) {
                if (resultCode == -1 && data != null) {
                    if (data.hasExtra("scannerResult")) {
                        String var10000 = data.getStringExtra("scannerResult");
                        Intrinsics.checkExpressionValueIsNotNull(var10000, "data.getStringExtra(\"scannerResult\")");
                        CharSequence var13 = (CharSequence)var10000;
                        if (var13.length() > 0) {
                            JSONObject resultOBJ = null;
                            try {
                                resultOBJ = new JSONObject(data.getStringExtra("scannerResult"));
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            Log.i("scannerResult", "scannerResult : " + resultOBJ);
                            String var10001 = resultOBJ.optString("DAQ");
                            Intrinsics.checkExpressionValueIsNotNull(var10001, "resultOBJ.optString(\"DAQ\")");
                            this.driving_license = var10001;
                            var10001 = resultOBJ.optString("DBA");
                            Intrinsics.checkExpressionValueIsNotNull(var10001, "resultOBJ.optString(\"DBA\")");
                            this.Timestamp = var10001;
                            var10001 = resultOBJ.optString("DBD");
                            Intrinsics.checkExpressionValueIsNotNull(var10001, "resultOBJ.optString(\"DBD\")");
                            this.dob = var10001;
                            this.Address = resultOBJ.optString("DAG") + ',' + resultOBJ.optString("DAG") + ',' + resultOBJ.optString("DAJ") + ',' + resultOBJ.optString("DAK");
                            var10001 = resultOBJ.optString("DAI");
                            Intrinsics.checkExpressionValueIsNotNull(var10001, "resultOBJ.optString(\"DAI\")");
                            this.City = var10001;
                            var10001 = resultOBJ.optString("DCT");
                            Intrinsics.checkExpressionValueIsNotNull(var10001, "resultOBJ.optString(\"DCT\")");
                            this.Name = var10001;
                            var10001 = resultOBJ.optString("DAK");
                            Intrinsics.checkExpressionValueIsNotNull(var10001, "resultOBJ.optString(\"DAK\")");
                            this.Postal = var10001;
                            var10001 = resultOBJ.optString("DCS");
                            Intrinsics.checkExpressionValueIsNotNull(var10001, "resultOBJ.optString(\"DCS\")");
                            this.lName = var10001;
                            var10001 = resultOBJ.optString("DBB");
                            Intrinsics.checkExpressionValueIsNotNull(var10001, "resultOBJ.optString(\"DBB\")");
                            this.date = var10001;
                            this.date = String.valueOf(CUC.Companion.getdatefromoneformattoAnother("ddMMyyyy", "dd/MM/yyyy", this.date));
                            /*if (Intrinsics.areEqual(this.Address, "") ^ true) {
                                List splitAddress = StringsKt.split$default((CharSequence)this.Address, new String[]{","}, false, 0, 6, (Object)null);
                                this.Postal = String.valueOf(splitAddress.get(splitAddress.size() - 1));
                                this.state_code = String.valueOf(splitAddress.get(splitAddress.size() - 2));
                                this.City = String.valueOf(splitAddress.get(splitAddress.size() - 3));
                                Log.i("combinedResult", String.valueOf(splitAddress.get(splitAddress.size() - 1)));
                                Log.i("combinedResult", String.valueOf(splitAddress.get(splitAddress.size() - 2)));
                                Log.i("combinedResult", String.valueOf(splitAddress.get(splitAddress.size() - 3)));
                            }*/

                            if (Intrinsics.areEqual(this.Timestamp, "") ^ true) {
                                this.Timestamp = String.valueOf(CUC.Companion.getdatefromoneformattoAnother("MMddyyyy", "yyyy-MM-dd", this.Timestamp));
                            }

                            if (Intrinsics.areEqual(this.dob, "") ^ true) {
                                this.dob = String.valueOf(CUC.Companion.getdatefromoneformattoAnother("MMddyyyy", "yyyy-MM-dd", this.dob));
                            }

                            JsonObject mainData = new JsonObject();
                            JsonObject licence_data = new JsonObject();
                            String var8 = this.driving_license;
                            String var10 = "driving_license";
                            if (var8 == null) {
                                throw new TypeCastException("null cannot be cast to non-null type kotlin.CharSequence");
                            }

                            String var11 = StringsKt.trim((CharSequence)var8).toString();
                            licence_data.addProperty(var10, String.valueOf(var11));
                            var8 = this.Timestamp;
                            var10 = "Timestamp";
                            if (var8 == null) {
                                throw new TypeCastException("null cannot be cast to non-null type kotlin.CharSequence");
                            }

                            var11 = StringsKt.trim((CharSequence)var8).toString();
                            licence_data.addProperty(var10, String.valueOf(var11));
                            var8 = this.dob;
                            var10 = "patient_dob";
                            if (var8 == null) {
                                throw new TypeCastException("null cannot be cast to non-null type kotlin.CharSequence");
                            }

                            var11 = StringsKt.trim((CharSequence)var8).toString();
                            licence_data.addProperty(var10, String.valueOf(var11));
                            var8 = this.Address;
                            var10 = "Address";
                            if (var8 == null) {
                                throw new TypeCastException("null cannot be cast to non-null type kotlin.CharSequence");
                            }

                            var11 = StringsKt.trim((CharSequence)var8).toString();
                            licence_data.addProperty(var10, String.valueOf(var11));
                            var8 = this.Name;
                            var10 = "Name";
                            if (var8 == null) {
                                throw new TypeCastException("null cannot be cast to non-null type kotlin.CharSequence");
                            }

                            var11 = StringsKt.trim((CharSequence)var8).toString();
                            licence_data.addProperty(var10, String.valueOf(var11));
                            var8 = this.lName;
                            var10 = "lName";
                            if (var8 == null) {
                                throw new TypeCastException("null cannot be cast to non-null type kotlin.CharSequence");
                            }

                            var11 = StringsKt.trim((CharSequence)var8).toString();
                            licence_data.addProperty(var10, String.valueOf(var11));
                            var8 = this.Postal;
                            var10 = "Postal";
                            if (var8 == null) {
                                throw new TypeCastException("null cannot be cast to non-null type kotlin.CharSequence");
                            }

                            var11 = StringsKt.trim((CharSequence)var8).toString();
                            licence_data.addProperty(var10, String.valueOf(var11));
                            var8 = this.state_code;
                            var10 = "state_code";
                            if (var8 == null) {
                                throw new TypeCastException("null cannot be cast to non-null type kotlin.CharSequence");
                            }

                            var11 = StringsKt.trim((CharSequence)var8).toString();
                            licence_data.addProperty(var10, String.valueOf(var11));
                            var8 = this.City;
                            var10 = "City";
                            if (var8 == null) {
                                throw new TypeCastException("null cannot be cast to non-null type kotlin.CharSequence");
                            }

                            var11 = StringsKt.trim((CharSequence)var8).toString();
                            licence_data.addProperty(var10, String.valueOf(var11));
                            mainData.add("licence_data", (JsonElement)licence_data);
                            Intent mIntent;
                            if (this.CALL_FUN == this.MY_BLINK_ID_CUSTOMER) {
                                mIntent = new Intent((Context)this, MessageActivity.class);
                                mIntent.putExtra("isFrom", "Customer");
                                mIntent.putExtra("licenceNo", this.driving_license);
                                mIntent.putExtra("FirstName", this.Name);
                                mIntent.putExtra("LastName", this.lName);
                                mIntent.putExtra("ExpiryDate", this.Timestamp);
                                mIntent.putExtra("City", this.City);
                                mIntent.putExtra("MeberSinceDate", this.date);
                                mIntent.putExtra("licence_data", mainData.toString());
                                this.startActivityForResult(mIntent, 444);
                            } else if (this.CALL_FUN == this.MY_BLINK_ID_EMPLOYEE) {
                                mIntent = new Intent((Context)this, MessageActivity.class);
                                mIntent.putExtra("isFrom", "Employee");
                                mIntent.putExtra("licenceNo", this.driving_license);
                                mIntent.putExtra("FirstName", this.Name);
                                mIntent.putExtra("LastName", this.lName);
                                mIntent.putExtra("MeberSinceDate", this.date);
                                mIntent.putExtra("ExpiryDate", this.Timestamp);
                                mIntent.putExtra("City", this.City);
                                this.startActivityForResult(mIntent, 444);
                            }
                        }
                    }
                } else {
                    this.driving_license = "";
                    this.Timestamp = "";
                    this.dob = "";
                    this.Address = "";
                    this.City = "";
                    this.Name = "";
                    this.Postal = "";
                    this.lName = "";
                    this.date = "";
                    CUC.Companion.displayToast((Context)this, "0", "please scan valid license.");
                }
            }
        }

    }


}
