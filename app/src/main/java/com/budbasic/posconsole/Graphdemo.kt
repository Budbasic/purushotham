package com.budbasic.posconsole

import android.graphics.Color
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import com.github.mikephil.charting.components.Description
import com.github.mikephil.charting.data.*
import com.github.mikephil.charting.formatter.PercentFormatter
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet
import com.github.mikephil.charting.interfaces.datasets.IPieDataSet
import com.jjoe64.graphview.series.BarGraphSeries
import com.jjoe64.graphview.series.DataPoint
import com.jjoe64.graphview.series.LineGraphSeries
import kotlinx.android.synthetic.main.activity_graphdemo.*


class Graphdemo : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_graphdemo)
        //  val array= arrayOf<DataPoint>(DataPoint(0,1), DataPoint(1,5),DataPoint(2,3))
        //line graph

        val series = LineGraphSeries(arrayOf(DataPoint(0.0, 1.0), DataPoint(1.0, 5.0), DataPoint(2.0, 3.0), DataPoint(3.0, 2.0), DataPoint(4.0, 6.0)))

        graphdemo1.addSeries(series)
        //bar graph

        val series1 = BarGraphSeries(arrayOf(DataPoint(0.0, -1.0), DataPoint(1.0, 5.0), DataPoint(2.0, 3.0), DataPoint(3.0, 2.0), DataPoint(4.0, 6.0)))

        bargraph.addSeries(series1)
        series1.setSpacing(50);

// draw values on top
        series1.setDrawValuesOnTop(true);
        series1.setValuesOnTopColor(Color.BLUE);


        //bar chart different librires com.github.mikephil.charting.charts.BarChart

        //getDataSet()
        //val bardata = BarDataSet(getXAxisValuesBarEntry())

        val data = BarData(getDataSet())
        chart.data = data
        chart.animateXY(2000, 2000)
        chart.invalidate()
        chart.description = Description()
        //  chart.descr
        //  data.setValueTextColor(ContextCompat.getColor(application,R.color.colorPrimaryDark))

        //piechart

        val data1 = PieData(getPiedataset())
        val data2 = PieData()
        //piechart.data=data1
        piechart.data = data1


        data1.setValueFormatter(PercentFormatter())
        piechart.setDrawHoleEnabled(true);
        piechart.setTransparentCircleRadius(25f);
        piechart.setHoleRadius(25f);
        data1.setValueTextSize(10f)
        data1.setValueTextColor(ContextCompat.getColor(application, R.color.colorPrimary))
        //   data1.setValueTextColor

        // enable rotation of the chart by touch
        piechart.rotationAngle = 0f
        piechart.setRotationEnabled(false);
        //    piechart.setOnChartValueSelectedListener(new O)

    }

    private fun getPiedataset(): IPieDataSet {
        val pieEntrys = ArrayList<PieEntry>()
        //var yvalues = ArrayList<Entry>()
        pieEntrys.add(PieEntry(83444F, 0f))
        pieEntrys.add(PieEntry(15f, 1f))
        pieEntrys.add(PieEntry(1233333f, 2f))
        pieEntrys.add(PieEntry(25f, 3f))
        pieEntrys.add(PieEntry(123.2f, 4f))
        pieEntrys.add(PieEntry(17f, 5f))
        val dataSet = PieDataSet(pieEntrys, "Election Results")
        //dataSet.setColors(ColorTemplate.LIBERTY_COLORS)
        //custom color

        dataSet.setColors(getColorValues())
        return dataSet
    }

    private fun getDataSet(): ArrayList<IBarDataSet> {
        var dataSets: ArrayList<IBarDataSet>? = null

        val valueSet1 = ArrayList<BarEntry>()
        val v1e1 = BarEntry(110.000f, 0f) // Jan`
        valueSet1.add(v1e1)
        val v1e2 = BarEntry(40.000f, 1f) // Feb
        valueSet1.add(v1e2)
        val v1e3 = BarEntry(60.000f, 2f) // Mar
        valueSet1.add(v1e3)
        val v1e4 = BarEntry(30.000f, 3f) // Apr
        valueSet1.add(v1e4)
        val v1e5 = BarEntry(90.000f, 4f) // May
        valueSet1.add(v1e5)
        val v1e6 = BarEntry(190.070f, 5f) // Jun
        valueSet1.add(v1e6)

        val valueSet2 = ArrayList<BarEntry>()
        val v2e1 = BarEntry(150.000f, 0f) // Jan
        valueSet2.add(v2e1)
        val v2e2 = BarEntry(90.000f, 1f) // Feb
        valueSet2.add(v2e2)
        val v2e3 = BarEntry(120.000f, 2f) // Mar
        valueSet2.add(v2e3)
        val v2e4 = BarEntry(60.000f, 3f) // Apr
        valueSet2.add(v2e4)
        val v2e5 = BarEntry(20.000f, 4f) // May
        valueSet2.add(v2e5)
        val v2e6 = BarEntry(280.000f, 5f) // Jun
        valueSet2.add(v2e6)

        val barDataSet1 = BarDataSet(valueSet1 as List<BarEntry>?, "Brand 1")
        barDataSet1.color = Color.rgb(0, 155, 0)
        val barDataSet2 = BarDataSet(valueSet2, "Brand 2")
        barDataSet2.color = Color.GRAY

        dataSets = ArrayList()
        dataSets.add(barDataSet1)
        // dataSets.add(barDataSet2)
        return dataSets
    }


    private fun getXAxisValues(): ArrayList<String> {
        val xAxis = ArrayList<String>()
        xAxis.add(("JAN"))
        xAxis.add("FEB")
        xAxis.add("MAR")
        xAxis.add("APR")
        xAxis.add("MAY")
        xAxis.add("JUN")
        return xAxis
    }

/*
    private fun getXAxisValuesBarEntry(): ArrayList<BarEntry> {
        val xAxis = ArrayList<BarEntry>()
        xAxis.add(BarEntry("JAN",150.000f))
        xAxis.add("FEB")
        xAxis.add("MAR")
        xAxis.add("APR")
        xAxis.add("MAY")
        xAxis.add("JUN")
        return xAxis
    }*/

    private fun getColorValues(): ArrayList<Int> {
        val colors = ArrayList<Int>()
        colors.add(Color.rgb(233, 150, 122))
        colors.add(Color.rgb(144, 238, 144))
        colors.add(ContextCompat.getColor(application, R.color.border_gray))
        colors.add(ContextCompat.getColor(application, R.color.gray))
        colors.add(ContextCompat.getColor(application, R.color.dark_gray))
        colors.add(ContextCompat.getColor(application, R.color.abc_hint_foreground_material_light))
        return colors
    }

}
