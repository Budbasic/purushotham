package com.budbasic.posconsole.driver.background;

import android.Manifest;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Binder;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.widget.Toast;

import com.budbasic.customer.global.Preferences;
import com.budbasic.global.CUC;
import com.budbasic.posconsole.R;
import com.budbasic.posconsole.global.Apppreference;
import com.budbasic.posconsole.reception.SalesActivity;
import com.budbasic.posconsole.webservice.RequetsInterface;
import com.google.gson.Gson;
import com.kotlindemo.model.GetCartData;
import com.kotlindemo.model.GetEmployeeLogin;
import com.kotlindemo.model.GetStatus;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import kotlin.jvm.internal.Intrinsics;


public class BackgroundService extends Service implements LocationListener {

    private Apppreference apppreference;
    private Handler mHandler = new Handler();
    private Timer mTimer = null;
    public static final int notify = 5000;
    String latitudest, longitudest, CountryName;
    LocationManager locationManager;
    private final IBinder binder = new LocalBinder();

    public BackgroundService() {
    }

    @Override
    public void onLocationChanged(Location location) {
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    public class LocalBinder extends Binder {
        BackgroundService getService() {
            // Return this instance of MyService so clients can call public methods
            return BackgroundService.this;
        }
    }

    @Override
    public void onCreate() {
        super.onCreate();
        System.out.println("aaaaaaaaaa   oncreate service  ");
        apppreference=new Apppreference(BackgroundService.this);
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (mTimer != null) {

            mTimer.cancel();
            System.out.println("aaaaaaaaaa  timer ");
        } else {
            System.out.println("aaaaaaaaaa  timer not created ");
            mTimer = new Timer();
            mTimer.scheduleAtFixedRate(new TimeDisplay(), 0, notify);
        }

        System.out.println("aaaaaaaaaaaa   servece started ");
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        mTimer.cancel();
        // if (mFloatingView != null) mWindowManager.removeView(mFloatingView);
    }

    class TimeDisplay extends TimerTask {
        @Override
        public void run() {
            // run on another thread
            mHandler.post(new Runnable() {
                @Override
                public void run() {

                    if (ActivityCompat.checkSelfPermission(BackgroundService.this, Manifest.permission.ACCESS_FINE_LOCATION)
                            != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(BackgroundService.this,
                            Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                        return;
                    }
                    Location location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);

                    Location location1 = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);

                    Location location2 = locationManager.getLastKnownLocation(LocationManager. PASSIVE_PROVIDER);

                    if (location != null) {
                        double latti = location.getLatitude();
                        double longi = location.getLongitude();

                        latitudest = String.valueOf(latti);
                        longitudest = String.valueOf(longi);
                        if (!apppreference.getData("status").equalsIgnoreCase("1")){
                            sendUpdatecartea(latitudest,longitudest);
                        }
                        System.out.println("aaaaaaaaaaaaa   Latitude:  iff  " + latitudest +" Longitude: " + longitudest);


                    } else  if (location1 != null) {
                        double latti = location1.getLatitude();
                        double longi = location1.getLongitude();

                        latitudest = String.valueOf(latti);
                        longitudest = String.valueOf(longi);
                        if (!apppreference.getData("status").equalsIgnoreCase("1")){
                            sendUpdatecartea(latitudest,longitudest);
                        }
                        System.out.println("aaaaaaaaaaaaa   Latitude:  else if " + latitudest +" Longitude: " + longitudest);

                    } else  if (location2 != null) {
                        double latti = location2.getLatitude();
                        double longi = location2.getLongitude();
                        latitudest = String.valueOf(latti);
                        longitudest = String.valueOf(longi);
                        if (!apppreference.getData("status").equalsIgnoreCase("1")){
                            sendUpdatecartea(latitudest,longitudest);
                        }
                        System.out.println("aaaaaaaaaaaaa   Latitude:  else  " + latitudest +" Longitude: " + longitudest);
                    }
                }
            });

        }
    }
    @Override
    public boolean stopService(Intent name) {
        System.out.println("aaaaaaaaaa   stop service ");
        mTimer.cancel();
        mTimer.purge();
        return super.stopService(name);
    }
    @Override
    public IBinder onBind(Intent intent) {
        return binder;
    }


    public void sendUpdatecartea(String latitudest, String longitudest){


        String loginda=apppreference.getData("logindata");
        GetEmployeeLogin logindata = new Gson().fromJson(loginda, GetEmployeeLogin.class);

        RequetsInterface apiService = RequetsInterface.Factory.create();
        Map parameterMap =new HashMap<String, String>();
        parameterMap.put("api_key", String.valueOf(Preferences.INSTANCE.getPreference((BackgroundService.this), "API_KEY")));

        if (logindata != null) {
            parameterMap.put("employee_id", logindata.getUser_id());
        }
        parameterMap.put("order_id",apppreference.getData("orderid"));


        // parameterMap.put("patient_id",queue_patient_id);
        parameterMap.put("employee_log", longitudest);
        parameterMap.put("employee_lat", latitudest);

        System.out.println("aaaaaaaaaaa  parameterMap location "+parameterMap);
        Log.i("aaaaaaaa   Result", "parameterMap : " + parameterMap);
        (new CompositeDisposable()).add(apiService.CallLocationsend(parameterMap).
                observeOn(AndroidSchedulers.mainThread()).
                subscribeOn(Schedulers.io()).subscribe((Consumer)(new Consumer() {
            // $FF: synthetic method
            // $FF: bridge method
            public void accept(Object var1) {
                this.accept((GetStatus)var1);
            }

            public final void accept(GetStatus result) {
                if (result != null) {

                    System.out.println("aaaaaaaaa  result location  "+result.toString());
                    Log.i("aaaaaaa   Result", "" + result.toString());
                    if (Intrinsics.areEqual(result.getStatus(), "0")) {
                        Toast.makeText(BackgroundService.this, "Location Send", Toast.LENGTH_SHORT).show();
                        System.out.println("aaaaaaaaaa  updatecart sucess");
                    } else if (Intrinsics.areEqual(result.getStatus(), "10")) {

                    } else {
                        // Toast.makeText(SalesActivity.this, ""+result.getShow_status(), Toast.LENGTH_SHORT).show();
                    }
                } else {
                }
            }
        }), (Consumer)(new Consumer() {
            // $FF: synthetic method
            // $FF: bridge method
            public void accept(Object var1) {
                this.accept((Throwable)var1);
            }

            public final void accept(Throwable error) {
                error.printStackTrace();
                System.out.println("aaaaaaaa  error update "+error.getMessage());
            }
        })));
    }

}
