package com.budbasic.posconsole.driver.background;

import android.Manifest;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.Process;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.widget.Toast;

import com.budbasic.customer.global.Preferences;
import com.budbasic.posconsole.global.Apppreference;
import com.budbasic.posconsole.webservice.RequetsInterface;
import com.google.gson.Gson;
import com.kotlindemo.model.GetEmployeeLogin;
import com.kotlindemo.model.GetStatus;

import java.util.HashMap;
import java.util.Map;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import kotlin.jvm.internal.Intrinsics;

public class LocationBackground extends Service {
	  private static final String TAG = "MyService";
	  private boolean isRunning  = false;
	  private Looper looper;
	  private MyServiceHandler myServiceHandler;
	LocationManager locationManager;
	String latitudest, longitudest;
	private Apppreference apppreference;
	  @Override
	  public void onCreate() {
	    HandlerThread handlerthread = new HandlerThread("MyThread", Process.THREAD_PRIORITY_BACKGROUND);
	    handlerthread.start();
	    looper = handlerthread.getLooper();
	    myServiceHandler = new MyServiceHandler(looper);
		  locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        isRunning = true;
		  apppreference=new Apppreference(LocationBackground.this);

	  }
	  @Override
	  public int onStartCommand(Intent intent, int flags, int startId) {
	      Message msg = myServiceHandler.obtainMessage();
	      msg.arg1 = startId;
	      myServiceHandler.sendMessage(msg);

	      Toast.makeText(this, "MyService Started.", Toast.LENGTH_SHORT).show();
	      //If service is killed while starting, it restarts. 
	      return START_STICKY;
	  }
	  @Override
	  public IBinder onBind(Intent intent) {
	      return null;
	  }
	  @Override
	  public void onDestroy() {
	   // isRunning = false;
		  System.out.println("aaaaaaaaaaaaaaaa  on destroyee ");
	    Toast.makeText(this, "MyService Completed or Stopped.", Toast.LENGTH_SHORT).show();
	  }
	  private final class MyServiceHandler extends Handler {
	      public MyServiceHandler(Looper looper) {
	          super(looper);
	      }
	      @Override
	      public void handleMessage(Message msg) {
              synchronized (this) {

            	  for (int i = 0; i < i+1; i++) {
                      try {
                          Log.i(TAG, "MyService running...");
                          Thread.sleep(5000);
                          i++;
                      } catch (Exception e) {
                    	  Log.i(TAG, e.getMessage());
                      }

					  if (ActivityCompat.checkSelfPermission(LocationBackground.this, Manifest.permission.ACCESS_FINE_LOCATION)
							  != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(LocationBackground.this,
							  Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
						  return;
					  }
					  Location location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);

					  Location location1 = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);

					  Location location2 = locationManager.getLastKnownLocation(LocationManager. PASSIVE_PROVIDER);

					  if (location != null) {
						  double latti = location.getLatitude();
						  double longi = location.getLongitude();

						  latitudest = String.valueOf(latti);
						  longitudest = String.valueOf(longi);
						  System.out.println("aaaaaaaaaaaaa   Latitude:  iff  " + latitudest +" Longitude: " + longitudest);
						  sendUpdatecartea(latitudest,longitudest);

					  } else  if (location1 != null) {
						  double latti = location1.getLatitude();
						  double longi = location1.getLongitude();

						  latitudest = String.valueOf(latti);
						  longitudest = String.valueOf(longi);
						  sendUpdatecartea(latitudest,longitudest);
						  System.out.println("aaaaaaaaaaaaa   Latitude:  else if " + latitudest +" Longitude: " + longitudest);

					  } else  if (location2 != null) {
						  double latti = location2.getLatitude();
						  double longi = location2.getLongitude();
						  latitudest = String.valueOf(latti);
						  longitudest = String.valueOf(longi);
						  sendUpdatecartea(latitudest,longitudest);
						  System.out.println("aaaaaaaaaaaaa   Latitude:  else  " + latitudest +" Longitude: " + longitudest);
					  }

                      if(!isRunning){
                    	  break;
                      } 
                  }
              }
              //stops the service for the start id.
	          stopSelfResult(msg.arg1);
	      }
	  }

	public void sendUpdatecartea(String latitudest, String longitudest){

		apppreference=new Apppreference(LocationBackground.this);
		String loginda=apppreference.getData("logindata");
		GetEmployeeLogin logindata = new Gson().fromJson(loginda, GetEmployeeLogin.class);

		RequetsInterface apiService = RequetsInterface.Factory.create();
		Map parameterMap =new HashMap<String, String>();
		parameterMap.put("api_key", String.valueOf(Preferences.INSTANCE.getPreference((LocationBackground.this), "API_KEY")));

		if (logindata != null) {
			parameterMap.put("employee_id", logindata.getUser_id());
		}
		parameterMap.put("order_id",apppreference.getData("orderid"));


		// parameterMap.put("patient_id",queue_patient_id);
		parameterMap.put("employee_log", longitudest);
		parameterMap.put("employee_lat", latitudest);

		System.out.println("aaaaaaaaaaa  parameterMap location "+parameterMap);
		Log.i("aaaaaaaa   Result", "parameterMap : " + parameterMap);
		(new CompositeDisposable()).add(apiService.CallLocationsend(parameterMap).
				observeOn(AndroidSchedulers.mainThread()).
				subscribeOn(Schedulers.io()).subscribe((Consumer)(new Consumer() {
			// $FF: synthetic method
			// $FF: bridge method
			public void accept(Object var1) {
				this.accept((GetStatus)var1);
			}

			public final void accept(GetStatus result) {
				if (result != null) {

					System.out.println("aaaaaaaaa  result location  "+result.toString());
					Log.i("aaaaaaa   Result", "" + result.toString());
					if (Intrinsics.areEqual(result.getStatus(), "0")) {
						Toast.makeText(LocationBackground.this, "Location Send", Toast.LENGTH_SHORT).show();
						System.out.println("aaaaaaaaaa  updatecart sucess");
					} else if (Intrinsics.areEqual(result.getStatus(), "10")) {

					} else {
						// Toast.makeText(SalesActivity.this, ""+result.getShow_status(), Toast.LENGTH_SHORT).show();
					}
				} else {
				}
			}
		}), (Consumer)(new Consumer() {
			// $FF: synthetic method
			// $FF: bridge method
			public void accept(Object var1) {
				this.accept((Throwable)var1);
			}

			public final void accept(Throwable error) {
				error.printStackTrace();
				System.out.println("aaaaaaaa  error update "+error.getMessage());
			}
		})));
	}

	}