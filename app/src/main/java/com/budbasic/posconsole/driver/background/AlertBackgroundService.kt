package com.budbasic.posconsole.driver.background

import android.Manifest
import android.app.Service
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Criteria
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.Binder
import android.os.Bundle
import android.os.IBinder
import android.support.v4.content.ContextCompat
import android.util.Log
import android.widget.Toast
import com.google.gson.Gson
import com.kotlindemo.model.GetEmployeeLogin
import com.budbasic.posconsole.webservice.RequetsInterface
import com.budbasic.posconsole.R
import com.budbasic.posconsole.global.GPSTrackerService
import com.budbasic.customer.global.Preferences
import com.budbasic.global.CUC
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import java.text.DecimalFormat
import java.util.concurrent.TimeUnit

class AlertBackgroundService : Service(), LocationListener {
    var provider: String = ""
    lateinit var locationManager: LocationManager
    lateinit var tracker: GPSTrackerService
    //var alertDialog: AlertDialog? = null

    internal var current_latitude = 0.0
    internal var current_longitude = 0.0

    override fun onStatusChanged(p0: String?, p1: Int, p2: Bundle?) {
    }

    override fun onProviderEnabled(p0: String?) {
    }

    override fun onProviderDisabled(p0: String?) {
    }

    val updateTime: Long = (3 * 60 * 1000)

    override fun onLocationChanged(location: Location?) {
        if (location != null) {
            current_latitude = DecimalFormat("##.#####").format(location.latitude).toDouble()
            current_longitude = DecimalFormat("##.#####").format(location.longitude).toDouble()
            Preferences.setPreference(this@AlertBackgroundService, "latitude", "$current_latitude")
            Preferences.setPreference(this@AlertBackgroundService, "longitude", "$current_longitude")
            //callEmployeeLocationUpdate()
            val i = Intent("android.intent.action.MAINLOCATION")
            this@AlertBackgroundService.sendBroadcast(i)
            Log.i(AlertBackgroundService::class.java.simpleName, "${Preferences.getPreference(this@AlertBackgroundService, "latitude")} :" +
                    " ${Preferences.getPreference(this@AlertBackgroundService, "longitude")}")
        }
    }

    private val TAG = AlertBackgroundService::class.java.simpleName

    //private val handler = Handler()

    @get:Synchronized
    var count = 0

    val compsiteDisposable: CompositeDisposable = CompositeDisposable()
    private val binder = TestServiceBinder()
    lateinit var logindata: GetEmployeeLogin

    inner class TestServiceBinder : Binder() {
        internal val service: AlertBackgroundService
            get() = this@AlertBackgroundService
    }

    var subscription: Disposable? = null

    override fun onCreate() {
        Log.i(TAG, "TestService Create..")
        locationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager
        logindata = Gson().fromJson(Preferences.getPreference(this@AlertBackgroundService, "logindata"), GetEmployeeLogin::class.java)
        callLocation()
        subscription = Observable.interval(30000, 30000, TimeUnit.MILLISECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    synchronized(this@AlertBackgroundService) {
                        if (Preferences.getPreference(this@AlertBackgroundService, "logindata") != null &&
                                Preferences.getPreference(this@AlertBackgroundService, "logindata") != "") {
                            logindata = Gson().fromJson(Preferences.getPreference(this@AlertBackgroundService, "logindata"), GetEmployeeLogin::class.java)
                        }
                        if (logindata != null)
                            callEmployeeLocationUpdate()
                    }
                })
        //handler.postDelayed(timer_runnable, updateTime)
    }

    override fun onBind(intent: Intent): IBinder? {
        return binder
    }

    override fun onDestroy() {
        subscription!!.dispose()
        Log.i(TAG, "TestService Destroy..")
    }

    @Synchronized
    fun clearCount() {
        count = 0
    }

    private fun getCriteria(): Criteria {
        val criteria = Criteria()//??????? Criteria.ACCURACY_COARSE?????Criteria.ACCURACY_FINE?????
        criteria.accuracy = Criteria.ACCURACY_FINE//????????
        criteria.isSpeedRequired = true// ???????????
        criteria.isCostAllowed = false//??????????
        criteria.isBearingRequired = false//??????????
        criteria.isAltitudeRequired = false//????????
        criteria.powerRequirement = Criteria.POWER_LOW
        return criteria
    }

    private fun callLocation() {
        if (ContextCompat.checkSelfPermission(this@AlertBackgroundService, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // Should we show an explanation?
        } else {
            //getCurrentLatLong();
            if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                locationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager
                val criteria = getCriteria()// Creating an empty criteria object
                provider = locationManager.getBestProvider(criteria, true)// Getting the name of the provider that meets the criteria
                Log.i(AlertBackgroundService::class.java.simpleName, "provider : $provider")
                if (provider != null && provider != "") {
                    // Get the location from the given provider
                    val location = locationManager.getLastKnownLocation(provider)
                    locationManager.requestLocationUpdates(provider, 0, 1f, this)
                    if (location != null) {
                        onLocationChanged(location)
                    } else {
                        getCurrentLatLong()
                        Toast.makeText(this@AlertBackgroundService, "Location can't be retrieved", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    //getCurrentLatLong()
                    //Toast.makeText(this@AlertBackgroundService, "No Provider Found", Toast.LENGTH_SHORT).show()
                }
                // mMap.setMyLocationEnabled(true);
            } else {
                //showGPSDisabledAlertToUser()
            }
            //Toast.makeText(this, "" + permission + " is already granted.", Toast.LENGTH_SHORT).show();
        }
    }


    private fun getCurrentLatLong() {
        tracker = GPSTrackerService(this@AlertBackgroundService)
        if (tracker.canGetLocation()) {
            if (tracker.latitude != 0.0 && tracker.longitude != 0.0) {
                onLocationChanged(tracker.location)
                //Toast.makeText(NewAppointmentActivity.this, "Please check mobile network location", Toast.LENGTH_SHORT).show();
            } else {
                getCurrentLatLong()
            }
        } else {
//            if (alertDialog != null && alertDialog!!.isShowing)
//                return
//            val builder = AlertDialog.Builder(this@AlertBackgroundService)
//            // Setting Dialog Title
//            builder.setTitle("GPS is settings")
//            // Setting Dialog Message
//            builder.setMessage("GPS is not enabled. Do you want to go to settings menu?")
//            // On pressing Settings button
//            builder.setPositiveButton("Settings") { dialog, which ->
//                val intent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
//                startActivityForResult(intent, REQUEST_CODE_GPS)
//            }
//
//            // on pressing cancel button
//            builder.setNegativeButton("Cancel") { dialog, which -> dialog.cancel() }
//            alertDialog = builder.create()
//            alertDialog!!.setCancelable(false)
//            alertDialog!!.setCanceledOnTouchOutside(false)
//            alertDialog!!.show()
        }
    }


    fun callEmployeeLocationUpdate() {
        val apiService = RequetsInterface.Factory.create()
        val parameterMap = HashMap<String, String>()
        parameterMap["api_key"] = "${Preferences.getPreference(this@AlertBackgroundService, "API_KEY")}"
        parameterMap["employee_lat"] = "${Preferences.getPreference(this@AlertBackgroundService, "latitude")}"
        parameterMap["employee_log"] = "${Preferences.getPreference(this@AlertBackgroundService, "longitude")}"
        parameterMap["employee_id"] = "${logindata.user_id}"
        Log.i("Result", "parameterMap : $parameterMap")
        compsiteDisposable.add(apiService.callGetEmployeeLocationUpdate(parameterMap)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe({ result ->
                    if (result != null) {
                        Log.i("Result", "" + result.toString())
                        if (result.status == "0") {
                            CUC.displayToast(this@AlertBackgroundService, result.show_status, result.message)
                            //handler.postDelayed(timer_runnable, updateTime)
                        } else if (result.status == "10") {
                            callApiKey()
                        } else {
                            CUC.displayToast(this@AlertBackgroundService, result.show_status, result.message)
                            //handler.postDelayed(timer_runnable, updateTime)
                        }
                    } else {
                     //   CUC.displayToast(this@AlertBackgroundService, "0", getString(R.string.connection_to_database_failed))
                        //handler.postDelayed(timer_runnable, updateTime)
                    }
                }, { error ->
                    CUC.displayToast(this@AlertBackgroundService, "0", getString(R.string.connection_to_database_failed))
                    error.printStackTrace()
                    //handler.postDelayed(timer_runnable, updateTime)
                })
        )
    }

    fun callApiKey() {
        val apiService = RequetsInterface.create()
        val parameterMap = HashMap<String, String>()
        compsiteDisposable.add(apiService.callGetApiKey(parameterMap)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe({ result ->
                    Preferences.setPreference(this@AlertBackgroundService, "API_KEY", "${result.api_key}")
                    callEmployeeLocationUpdate()
                    Log.i("Result", "" + result.toString())
                }, { error ->
                    error.printStackTrace()
                })
        )
    }


}
