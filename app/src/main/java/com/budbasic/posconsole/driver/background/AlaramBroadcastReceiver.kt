package com.budbasic.posconsole.driver.background

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.util.Log

class AlaramBroadcastReceiver : BroadcastReceiver() {

    override fun onReceive(context: Context, intent: Intent) {
        Log.i(TAG, "onReceive(), Called..")
        try {
            val msgIntent = Intent(context, AlertBackgroundService::class.java)
            context.startService(msgIntent)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    companion object {
        private val TAG = "AlaramBroadcastReceiver"
    }
}
