package com.budbasic.posconsole.driver.fragment

import android.app.Activity
import android.app.AlertDialog
import android.app.Dialog
import android.content.ActivityNotFoundException
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.Color
import android.location.Geocoder
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.StrictMode
import android.provider.Settings
import android.support.design.widget.Snackbar
import android.support.v4.app.Fragment
import android.support.v7.widget.CardView
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.DisplayMetrics
import android.util.Log
import android.view.*
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.*
import com.budbasic.customer.global.Preferences
import com.budbasic.global.BSV
import com.budbasic.global.CUC
import com.budbasic.posconsole.R
import com.budbasic.posconsole.ScanQRCodeActivity
import com.budbasic.posconsole.driver.SignatureActivity
import com.budbasic.posconsole.driver.background.BackgroundService
import com.budbasic.posconsole.driver.background.LocationBackground
import com.budbasic.posconsole.global.Apppreference
import com.budbasic.posconsole.model.APIClass
import com.budbasic.posconsole.webservice.RequetsInterface
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DecodeFormat
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.signature.ObjectKey
import com.google.android.gms.maps.model.LatLng
import com.google.gson.Gson
import com.kotlindemo.model.*
import com.mylibrary.DDJActivity
import com.tbruyelle.rxpermissions2.RxPermissions
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.functions.Consumer
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.do_listing_map.view.*
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import org.json.JSONObject
import java.io.File
import java.io.IOException
import java.util.*

class DOListingMapF : Fragment(), APIClass.onCallListner {
    override fun onFailed(requestCode: Int) {
    }

    override fun onSuccess(requestCode: Int) {
        if (requestCode == EMPLOYEEDRIVERORDER) {
            callEmployeeDriverOrder()
        } else if (requestCode == EMPLOYEEDRIVERCANCELDELIVERY) {
            callEmployeeDriverCancelDelivery()
        } else if (requestCode == EMPLOYEEDRIVERACCEPTDELIVERY) {
            callEmployeeDriverAcceptDelivery()
        } else if (requestCode == EMPLOYEEDRIVERRETURNPACKAGE) {
            callEmployeeDriverReturnPackage()
        } else if (requestCode == EMPLOYEEDRIVERDELIVEREDORDER) {
            callEmployeeDriverDeliveredOrder()
        }
    }

    val EMPLOYEEDRIVERORDER = 1
    val EMPLOYEEDRIVERCANCELDELIVERY = 2
    val EMPLOYEEDRIVERACCEPTDELIVERY = 3
    val EMPLOYEEDRIVERRETURNPACKAGE = 4
    val EMPLOYEEDRIVERDELIVEREDORDER = 5

    var mView: View? = null
    lateinit var mDialog: Dialog
    lateinit var logindata: GetEmployeeLogin
    lateinit var appSettingData: GetEmployeeAppSetting
    lateinit var ordersDataAdapter: OrdersDataAdapter
    lateinit var pickupsAdapter: PickupsAdapter

    var rxPermissions: RxPermissions? = null
    var store_code = ""
    var invoice_id = ""
    //var mMap: GoogleMap? = null
    var patient_licence = ""
    lateinit var customDialog: Dialog
    var cancelReson = ""

    internal var check: Boolean = false
    private var apppreference: Apppreference? = null
   lateinit var getpickups: GetPickups
    var patient_id = ""
    var driving_license = ""
    var Timestamp = ""
    var Address = ""
    var City = ""
    var Name = ""
    var lName = ""
    var Postal = ""
    var payment_amount = ""
    var change_amount = ""
    var paymentTypeList = java.util.ArrayList<GetPaymentTypes>()
    var paymentId = ""
    var payment_path = ""
    var total_payble = ""
    lateinit var dispatch_type_dialog: Dialog
    lateinit var edt_licence: TextView
    lateinit var iv_siganture: ImageView
    lateinit var edt_mmmplicence: EditText
    var patient_mmmp_licence = ""
    var siganture_path = ""

    lateinit var paymentAdapter: PaymentAdapter

    companion object {
        var orderData: ArrayList<GetOrderData> = ArrayList()
        var pickups: ArrayList<GetPickups> = ArrayList()

    }

    var apiClass: APIClass? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        if (view != null)
            mView = view!!
        else
            mView = inflater.inflate(R.layout.do_listing_map, container, false)
        initView()
        return mView
    }

    fun initView() {
        apiClass = APIClass(context!!, this)
        rxPermissions = RxPermissions(activity!!)
        logindata = Gson().fromJson(Preferences.getPreference(activity!!, "logindata"), GetEmployeeLogin::class.java)
        appSettingData = Gson().fromJson(Preferences.getPreference(activity!!, "AppSetting"), GetEmployeeAppSetting::class.java)
        mView!!.rv_order_data!!.layoutManager = GridLayoutManager(activity!!, 1)
        mView!!.rv_pickups!!.layoutManager = GridLayoutManager(activity!!, 1)
        apppreference = Apppreference(activity!!)
        ordersDataAdapter = OrdersDataAdapter(activity!!, object : onClickListener {
            override fun callClick(data: GetOrderData) {
                invoice_id = "${data.invoice_id}"
                rxPermissions!!.request(
                        android.Manifest.permission.CAMERA)
                        .subscribe(object : Consumer<Boolean> {
                            override fun accept(granted: Boolean?) {
                                if (granted!!) { // Always true pre-M
                                    // I can control the camera now
                                    var builder: AlertDialog.Builder
                                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                                        builder = AlertDialog.Builder(context, android.R.style.Theme_Material_Dialog_Alert)
                                    } else {
                                        builder = AlertDialog.Builder(context)
                                    }
                                    builder.setTitle("Delivery order")
                                            .setMessage("Are you sure you want to accept this order?")
                                            .setPositiveButton("Accept", object : DialogInterface.OnClickListener {
                                                override fun onClick(p0: DialogInterface?, p1: Int) {
                                                    startActivityForResult(Intent(activity, ScanQRCodeActivity::class.java), 555)
                                                }
                                            })
                                            .setNegativeButton("Deny", object : DialogInterface.OnClickListener {
                                                override fun onClick(p0: DialogInterface?, p1: Int) {
                                                    callEmployeeDriverCancelDelivery()
                                                }
                                            })
                                            .setNeutralButton("Cancel", object : DialogInterface.OnClickListener {
                                                override fun onClick(p0: DialogInterface?, p1: Int) {
                                                }
                                            })
                                            .show()

                                } else {
                                    // Oups permission denied
                                    Snackbar.make(mView!!.ll_do_main, resources.getString(R.string.permissionenable),
                                            Snackbar.LENGTH_LONG)
                                            .setActionTextColor(Color.WHITE)
                                            .setAction(resources.getString(R.string.ok), object : View.OnClickListener {
                                                override fun onClick(p0: View?) {
                                                    val intent = Intent()
                                                    intent.action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
                                                    val uri = Uri.fromParts("package", activity!!.packageName, null)
                                                    intent.data = uri
                                                    startActivity(intent)
                                                }
                                            }).show()
                                }
                            }
                        })
            }
        })

        pickupsAdapter = PickupsAdapter(activity!!, object : onPickupsClickListener {
            override fun callPickupsClick(data: GetPickups) {
                total_payble = "${data.invoice_total_payble}"
                invoice_id = "${data.assigned_invoice_id}"
                patient_licence = "${data.patient_licence}"
                patient_mmmp_licence = "${data.patient_mmmp_licence}"
                callPackageStatus(data)
            }
        })
        mView!!.rv_pickups!!.adapter = pickupsAdapter
        mView!!.rv_order_data!!.adapter = ordersDataAdapter

        mView!!.rgdriverordertabs.setOnCheckedChangeListener(object : RadioGroup.OnCheckedChangeListener {
            override fun onCheckedChanged(p0: RadioGroup?, p1: Int) {
                when (p1) {
                    R.id.rbPickups -> {
                        callEmployeeDriverOrder()
                    }
                    R.id.rbOrders -> {
                        callEmployeeDriverOrder()
                    }
                    R.id.rbMap -> {
                        callEmployeeDriverOrder()
                    }
                }
            }
        })

        mView!!.rlAllMapNavigator.setOnClickListener {
            val user = LatLng("${Preferences.getPreference(activity!!, "latitude")}".toDouble(), "${Preferences.getPreference(activity!!, "longitude")}".toDouble())
            var desLocation = ""
            var currLocation = "saddr=${user.latitude},${user.longitude}"
            for (data in pickups) {
                if (desLocation == "") {
                    desLocation = "&daddr=${data.patient_lat},${data.patient_log}"
                } else {
                    desLocation += "+to:${data.patient_lat},${data.patient_log}"
                }
            }
            showMapFromLocation(currLocation, desLocation)
        }


        mView!!.rv_pickups!!.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView?, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                if (dy > 0 && mView!!.rlAllMapNavigator.isShown) {
                    closeEffect(mView!!.rlAllMapNavigator, R.anim.fadeout)
                    //float_btn_cashout_send.startAnimation(FabMenu_fadIn);
                    //float_btn_cashout_send.setVisibility(View.GONE);
                }
                if (dy < 0 && !mView!!.rlAllMapNavigator.isShown) {
                    openEffect(mView!!.rlAllMapNavigator, R.anim.fadein)
                    //float_btn_cashout_send.startAnimation(FabMenu_fadOut);
                    //float_btn_cashout_send.setVisibility(View.VISIBLE);
                }
            }
        })
        callEmployeeDriverOrder()
    }

    override fun onResume() {
        super.onResume()
        if (check){
            callPackageStatus(getpickups)
            check=false
           // apppreference.saveData("checkpos","1")
        }
    }

    var isOpen = true
    fun openEffect(myView: View, anim: Int) {
        if (isOpen)
            return
        val bottomUp = AnimationUtils.loadAnimation(activity!!, anim)
        myView.startAnimation(bottomUp)
        myView.visibility = View.VISIBLE
        isOpen = true

    }

    fun closeEffect(myView: View, anim: Int) {
        if (!isOpen)
            return

        val bottomUp = AnimationUtils.loadAnimation(activity!!, anim)
        bottomUp.setAnimationListener(object : Animation.AnimationListener {
            override fun onAnimationRepeat(p0: Animation?) {
            }

            override fun onAnimationEnd(p0: Animation?) {
                myView.visibility = View.GONE
            }

            override fun onAnimationStart(p0: Animation?) {
            }
        })
        myView.startAnimation(bottomUp)
        myView.visibility = View.INVISIBLE
        isOpen = false

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            555 -> if (resultCode === Activity.RESULT_OK) {
                println("aaaaaaaaaaaaaa   data  "+data)
                if (data != null && data.hasExtra("qrcode")) {
                    store_code = data.getStringExtra("qrcode")
                    println("aaaaaaaaaaaaaa   data  "+store_code)
                    Log.i("qrcode", "${data.getStringExtra("qrcode")}")
                    callEmployeeDriverAcceptDelivery()
                }
            }
            8989 -> if (resultCode === Activity.RESULT_OK) {
                if (data != null && data.hasExtra("siganture_path")) {
                    siganture_path = "${data.getStringExtra("siganture_path")}"
                    val newPhotoKey = ObjectKey(System.currentTimeMillis().toString())
                    Glide.with(context!!)
                            .load("$siganture_path")
                            .apply(RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.NONE)
                                    .dontTransform().dontAnimate()
                                    .signature(newPhotoKey))
                            .into(iv_siganture)
                    //CommonUtils.displayToast(activity!!, "0", "$siganture_path")
                }
            }
            0x101 -> if (resultCode === Activity.RESULT_OK) {
                if (resultCode == Activity.RESULT_OK && data != null && data.hasExtra("scannerResult")) {
                    if (data.getStringExtra("scannerResult").isNotEmpty()) {
                        val resultOBJ = JSONObject(data.getStringExtra("scannerResult"))
                        Log.i("scannerResult", "scannerResult : $resultOBJ")
                        driving_license = resultOBJ.optString("DAQ")
                        Timestamp = resultOBJ.optString("DBA")
                        //dob = resultOBJ.optString("DBD")
                        Address = "${resultOBJ.optString("DAG")},${resultOBJ.optString("DAI")},${resultOBJ.optString("DAJ")},${resultOBJ.optString("DAK")}"
                        //Address = resultOBJ.optString("DAG")
                        City = resultOBJ.optString("DAI")
                        Name = resultOBJ.optString("DCT")
                        Postal = resultOBJ.optString("DAK")
                        lName = resultOBJ.optString("DCS")

                        edt_licence.text = "$driving_license"
//                            if (Address != "") {
//                                val splitAddress = Address.split(",")
//                                Postal = "${splitAddress[splitAddress.size - 1]}"
//                                state_code = "${splitAddress[splitAddress.size - 2]}"
//                                City = "${splitAddress[splitAddress.size - 3]}"
//                                Log.i("combinedResult", "${splitAddress[splitAddress.size - 1]}")
//                                Log.i("combinedResult", "${splitAddress[splitAddress.size - 2]}")
//                                Log.i("combinedResult", "${splitAddress[splitAddress.size - 3]}")
//                            }

                        if (Timestamp != "") {
                            //08252018
                            Timestamp = "${CUC.getdatefromoneformattoAnother("MMddyyyy", "yyyy-MM-dd", Timestamp)}"
                        }
//                            if (dob != "") {
//                                //08252018
//                                dob = "${CUC.getdatefromoneformattoAnother("MMddyyyy", "yyyy-MM-dd", dob)}"
//                            }
                    }

                } else {
                    edt_licence.text = ""
                    // if BlinkID activity did not return result, user has probably
                    // pressed Back button and cancelled scanning
                    CUC.displayToast(activity!!, "0", "please scan valid license.")
                }
            }
        }
    }

    fun callPackageStatus(data: GetPickups) {
        getpickups=data
        val displayMetrics = DisplayMetrics()
        activity!!.windowManager.defaultDisplay.getMetrics(displayMetrics)
        activity!!.windowManager.defaultDisplay.getMetrics(displayMetrics)
        val PackageDialog = Dialog(activity!!, R.style.TransparentProgressDialog)
        val policy = StrictMode.ThreadPolicy.Builder().permitAll().build()
        StrictMode.setThreadPolicy(policy)
        PackageDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        PackageDialog.setContentView(R.layout.driver_package_dialog)
        PackageDialog.window!!.setLayout((displayMetrics.widthPixels * 75) / 100, ViewGroup.LayoutParams.WRAP_CONTENT)

        PackageDialog.window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)
        PackageDialog.show()
        val llMapNavigation = PackageDialog.findViewById<LinearLayout>(R.id.llMapNavigation)
        val llPackageReturn = PackageDialog.findViewById<LinearLayout>(R.id.llPackageReturn)
        val llPackageDelivered = PackageDialog.findViewById<LinearLayout>(R.id.llPackageDelivered)
        llMapNavigation.setOnClickListener {
            PackageDialog.dismiss()
            check=true
            apppreference!!.saveData("orderid",""+data.invoice_order_no)
            val user = LatLng("${Preferences.getPreference(activity!!, "latitude")}".toDouble(), "${Preferences.getPreference(activity!!, "longitude")}".toDouble())
            var desLocation = "&daddr=${data.patient_lat},${data.patient_log}"
            var currLocation = "saddr=${user.latitude},${user.longitude}"
//            for (data in pickups) {
//                if (desLocation == "") {
//                    desLocation = "&daddr=${data.patient_lat},${data.patient_log}"
//                } else {
//                    desLocation += "+to:${data.patient_lat},${data.patient_log}"
//                }
//            }
            showMapFromLocation(currLocation, desLocation)
        }
        llPackageReturn.setOnClickListener {
            PackageDialog.dismiss()
            callDriverReturnPackage()
        }
        llPackageDelivered.setOnClickListener {
            PackageDialog.dismiss()
            DisplayPaymentTypeDialog()
        }
    }

    fun DisplayPaymentTypeDialog() {
        paymentId = ""
        val displayMetrics = DisplayMetrics()
        activity!!.windowManager.defaultDisplay.getMetrics(displayMetrics)
        dispatch_type_dialog = Dialog(activity!!, R.style.TransparentProgressDialog)
        dispatch_type_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dispatch_type_dialog.setContentView(R.layout.delivery_order_dialog)
        dispatch_type_dialog.window!!.setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        val ll_mmmplicence: LinearLayout = dispatch_type_dialog.findViewById(R.id.ll_mmmplicence)
        val tv_submit: TextView = dispatch_type_dialog.findViewById(R.id.tv_submit)
        val tv_cancel: TextView = dispatch_type_dialog.findViewById(R.id.tv_cancel)
        val iv_scan_id: ImageView = dispatch_type_dialog.findViewById(R.id.iv_scan_id)
        val bs_payment = dispatch_type_dialog.findViewById<BSV>(R.id.bs_payment)
        val tv_digital_siganture: TextView = dispatch_type_dialog.findViewById(R.id.tv_digital_siganture)
        val tv_total_amount = dispatch_type_dialog.findViewById<TextView>(R.id.tv_total_amount)

        val edt_total_amt = dispatch_type_dialog.findViewById<EditText>(R.id.edt_total_amt)
        iv_siganture = dispatch_type_dialog.findViewById(R.id.iv_siganture)
        edt_licence = dispatch_type_dialog.findViewById(R.id.edt_driving_license)
        edt_mmmplicence = dispatch_type_dialog.findViewById(R.id.edt_mmmplicence)
        tv_total_amount.text = "$ " + "$total_payble"
        //edt_licence.text = "123"
        //edt_mmmplicence.setText("M 256 737 829 123")
        tv_digital_siganture.setOnClickListener {
            rxPermissions!!.request(
                    android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    .subscribe(object : Consumer<Boolean> {
                        override fun accept(granted: Boolean?) {
                            if (granted!!) { // Always true pre-M
                                // I can control the camera now
                                val siantureIntent = Intent(activity!!, SignatureActivity::class.java)
                                startActivityForResult(siantureIntent, 8989)
                            } else {
                                // Oups permission denied
                                Snackbar.make(mView!!.ll_do_main, resources.getString(R.string.permissionenable),
                                        Snackbar.LENGTH_LONG)
                                        .setActionTextColor(Color.WHITE)
                                        .setAction(resources.getString(R.string.ok), object : View.OnClickListener {
                                            override fun onClick(p0: View?) {
                                                val intent = Intent()
                                                intent.action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
                                                val uri = Uri.fromParts("package", activity!!.packageName, null)
                                                intent.data = uri
                                                startActivity(intent)
                                            }
                                        }).show()
                            }
                        }
                    })
        }

        val onClickPayment = object : onClickPayment {
            override fun callClick(data: GetPaymentTypes) {
                paymentId = "${data.payment_id}"
                bs_payment.setText("${data.payment_method}")
                bs_payment.dismissDropDown()
                bs_payment.clearFocus()
            }
        }

        paymentAdapter = PaymentAdapter(activity!!, R.layout.payment_type_item, paymentTypeList, payment_path, onClickPayment)
        bs_payment.setAdapter(paymentAdapter)
        try {
            if (paymentTypeList != null && paymentTypeList.size > 0) {
                //if (paymentTypeList.size == 1) {
                paymentId = "${paymentTypeList[0].payment_id}"
                bs_payment.setText("${paymentTypeList[0].payment_method}")
                bs_payment.dismissDropDown()
                bs_payment.clearFocus()
                //}
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
        if (patient_mmmp_licence != "") {
            ll_mmmplicence.visibility = View.VISIBLE
        } else {
            ll_mmmplicence.visibility = View.GONE
        }
        iv_scan_id.setOnClickListener {
            rxPermissions!!.request(
                    android.Manifest.permission.CAMERA)
                    .subscribe(object : Consumer<Boolean> {
                        override fun accept(granted: Boolean?) {
                            if (granted!!) { // Always true pre-M
                                // I can control the camera now
                                val startScanner = Intent(activity, DDJActivity::class.java)
                                startActivityForResult(startScanner, 0x101)
                            } else {
                                // Oups permission denied
                                Snackbar.make(mView!!.ll_do_main, resources.getString(R.string.permissionenable),
                                        Snackbar.LENGTH_LONG)
                                        .setActionTextColor(Color.WHITE)
                                        .setAction(resources.getString(R.string.ok), object : View.OnClickListener {
                                            override fun onClick(p0: View?) {
                                                val intent = Intent()
                                                intent.action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
                                                val uri = Uri.fromParts("package", activity!!.packageName, null)
                                                intent.data = uri
                                                startActivity(intent)
                                            }
                                        }).show()
                            }
                        }
                    })
        }
        tv_submit.setOnClickListener {
            //context!!.stopService(Intent(activity!!, LocationBackground::class.java))
            context!!.stopService(Intent(activity!!, BackgroundService::class.java))
            apppreference!!.saveData("status","1")
            callEmployeeDriverDeliveredOrder()

           /* if (patient_mmmp_licence != "") {
                if (edt_licence.text.toString() == "") {
                    CUC.displayToast(activity!!, "0", "Please scan patient licence number")
                } else if (edt_mmmplicence.text.toString() == "") {
                    CUC.displayToast(activity!!, "0", "Please enter patient MMMP licence number")
                } else {

                    if (edt_total_amt.text.toString() != "" && tv_total_amount.text.toString() != "") {
                        if ("${edt_total_amt.text}".toFloat() >= "${tv_total_amount.text}".toFloat()) {
                            if (paymentId == "") {
                                CUC.displayToast(activity!!, "0", "Please select payment")
                            } else if (siganture_path == "") {
                                CUC.displayToast(activity!!, "0", "Patient signature required")
                            } else {
                                change_amount = "${edt_total_amt.text}"
                                payment_amount = "${tv_total_amount.text}"
                                context!!.stopService(Intent(activity!!, LocationBackground::class.java))
                                callEmployeeDriverDeliveredOrder()
                            }

                        } else {
                            Toast.makeText(activity!!, "Please pay complete amount", Toast.LENGTH_LONG).show()
                        }
                    } else {
                        Toast.makeText(activity!!, "Please Enter Paid Amount", Toast.LENGTH_LONG).show()
                    }

                }
            } else {
                if (edt_licence.text.toString() == "") {
                    CUC.displayToast(activity!!, "0", "Please enter patient licence number")
                } else {
                    context!!.stopService(Intent(activity!!, LocationBackground::class.java))
                    callEmployeeDriverDeliveredOrder()

                    *//*if (edt_total_amt.text.toString() != "") {
                        if ("${edt_total_amt.text}".toFloat() >= "${tv_total_amount.text}".toFloat()) {
                            if (paymentId == "") {
                                CUC.displayToast(activity!!, "0", "Please select payment")
                            } else if (siganture_path == "") {
                                CUC.displayToast(activity!!, "0", "Patient signature required")
                            } else {
                                change_amount = "${edt_total_amt.text}"
                                payment_amount = "${tv_total_amount.text}"
                                callEmployeeDriverDeliveredOrder()
                            }
                        } else {
                            Toast.makeText(activity!!, "Please pay complete amount", Toast.LENGTH_LONG).show()
                        }
                    } else {
                        Toast.makeText(activity!!, "Please Enter Paid Amount", Toast.LENGTH_LONG).show()
                    }*//*
                }
            }*/
        }
        tv_cancel.setOnClickListener {
            dispatch_type_dialog.dismiss()
        }
        dispatch_type_dialog.show()
    }

    fun callDriverReturnPackage() {
        customDialog = Dialog(activity, R.style.TransparentProgressDialog)
        val policy = StrictMode.ThreadPolicy.Builder().permitAll().build()
        StrictMode.setThreadPolicy(policy)
        customDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        customDialog.setContentView(R.layout.package_return)
        customDialog.window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)
        customDialog.show()

        val tv_cancel_reson = customDialog.findViewById<EditText>(R.id.tv_cancel_reson)
        val cv_approve_request = customDialog.findViewById<CardView>(R.id.cv_approve_request)

        cv_approve_request.setOnClickListener {
            if (tv_cancel_reson.text.isEmpty()) {
                CUC.displayToast(activity!!, "0", "Please enter return reason")
            } else {
                cancelReson = "${tv_cancel_reson.text}"
                callEmployeeDriverReturnPackage()
            }
        }
    }

    fun showMapFromLocation(currLocation: String, desLocation: String) {
        val geocoder = Geocoder(activity!!, Locale.getDefault())
        try {
//            val srcAddresses = geocoder.getFromLocationName(src.toString(), 1)
//            if (srcAddresses.size > 0) {
//                val location = srcAddresses[0]
//                srcLat = location.latitude
//                srcLng = location.longitude
//            }
//            val destAddresses = geocoder.getFromLocationName(dest.toString(), 1)
//            if (destAddresses.size > 0) {
//                val location = destAddresses[0]
//                destLat = location.latitude
//                destLng = location.longitude
//            }
            // "d" means driving car, "w" means walking "r" means by bus

            //val bintent = Intent(activity!!, LocationBackground::class.java)
            val bintent = Intent(activity!!, BackgroundService::class.java)
            context!!.startService(bintent)

            val locations = "Jntu Kukatpally,hyderabad"
            val gmmIntentUri = Uri.parse("google.navigation:q=,+$locations")
            val mapIntent = Intent(Intent.ACTION_VIEW, gmmIntentUri)
            mapIntent.setPackage("com.google.android.apps.maps")
            startActivity(mapIntent)

           /* val intent = Intent(android.content.Intent.ACTION_VIEW, Uri.parse("http://maps.google.com/maps?$currLocation&$desLocation"))
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS)
            intent.setClassName("com.google.android.apps.maps", "com.google.android.maps.MapsActivity")
            if (intent.resolveActivity(activity!!.packageManager) != null) {
                startActivity(intent)
            } else {
                val intent = Intent(android.content.Intent.ACTION_VIEW, Uri.parse("http://maps.google.com/maps?$currLocation&$desLocation"))
                startActivity(intent)
            }*/
        } catch (e: IOException) {
            Log.e("error", "Error when showing google map directions, E: $e")
        } catch (e: ActivityNotFoundException) {
            val intent = Intent(android.content.Intent.ACTION_VIEW, Uri.parse("http://maps.google.com/maps?$currLocation&$desLocation"))
            startActivity(intent)
        } catch (e: Exception) {
            Log.e("error", "Error when showing google map directions, E: $e")
        }
    }

    fun callEmployeeDriverOrder() {
        if (CUC.isNetworkAvailablewithPopup(activity!!)) {
            mDialog = CUC.createDialog(activity!!)
            val apiService = RequetsInterface.create()
            val parameterMap = HashMap<String, String>()
            parameterMap["api_key"] = "${Preferences.getPreference(activity!!, "API_KEY")}"
            if (logindata != null) {
                parameterMap["employee_id"] = "${logindata.user_id}"
            }
            parameterMap["employee_lat"] = "${Preferences.getPreference(activity!!, "latitude")}"
            parameterMap["employee_log"] = "${Preferences.getPreference(activity!!, "longitude")}"
            println("aaaaaaaaaaaaaa  driverorders   "+parameterMap)
            Log.i("Result", "parameterMap : $parameterMap")
            CompositeDisposable().add(apiService.callGetEmployeeDriverOrder(parameterMap)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe({ result ->
                        println("aaaaaaaaaaaaaa  driverorders  result   "+result.toString())
                        if (mDialog != null && mDialog.isShowing)
                            mDialog.dismiss()
                        if (result != null) {
                            Log.i("Result", "" + result.toString())
                            if (result.status == "0") {
                                if (mView!!.rbPickups.isChecked) {
                                    mView!!.rlpickups.visibility = View.VISIBLE
                                    mView!!.rv_order_data.visibility = View.GONE
                                    mView!!.rlMap.visibility = View.GONE
                                } else if (mView!!.rbOrders.isChecked) {
                                    mView!!.rlpickups.visibility = View.GONE
                                    mView!!.rv_order_data.visibility = View.VISIBLE
                                    mView!!.rlMap.visibility = View.GONE
                                } else if (mView!!.rbMap.isChecked) {
                                    mView!!.rlpickups.visibility = View.GONE
                                    mView!!.rv_order_data.visibility = View.GONE
                                    mView!!.rlMap.visibility = View.VISIBLE
                                }

                                paymentTypeList.clear()

                                if (result.payment_types != null && result.payment_types.size > 0) {
                                    paymentTypeList.clear()
                                    paymentTypeList.addAll(result.payment_types)
                                } else {
                                    paymentTypeList.clear()
                                }

                                if (result.pickups != null && result.pickups.size > 0) {
                                    pickups.clear()
                                    pickups.addAll(result.pickups)
                                    pickupsAdapter.notifyDataSetChanged()
                                    mView!!.rlAllMapNavigator.visibility = View.GONE
                                } else {
                                    pickups.clear()
                                    pickupsAdapter.notifyDataSetChanged()
                                    mView!!.rlAllMapNavigator.visibility = View.GONE
                                }
                                if (result.order_data != null && result.order_data.size > 0) {
                                    orderData.clear()
                                    orderData.addAll(result.order_data)
                                    ordersDataAdapter.notifyDataSetChanged()
                                } else {
                                    orderData.clear()
                                    ordersDataAdapter.notifyDataSetChanged()
                                }
                                CUC.displayToast(activity!!, result.show_status, result.message)
                            } else if (result.status == "10") {
                                apiClass!!.callApiKey(EMPLOYEEDRIVERORDER)
                            } else {
                                orderData.clear()
                                ordersDataAdapter.notifyDataSetChanged()
                                pickups.clear()
                                pickupsAdapter.notifyDataSetChanged()
                                mView!!.rlAllMapNavigator.visibility = View.GONE
                                CUC.displayToast(activity!!, result.show_status, result.message)
                            }
                        } else {
                          //  CUC.displayToast(activity!!, "0", getString(R.string.connection_to_database_failed))
                        }
                    }, { error ->
                        if (mDialog != null && mDialog.isShowing)
                            mDialog.dismiss()
                        CUC.displayToast(activity!!, "0", getString(R.string.connection_to_database_failed))
                        error.printStackTrace()
                    })
            )
        }
    }

    fun callEmployeeDriverCancelDelivery() {
        mDialog = CUC.createDialog(activity!!)
        val apiService = RequetsInterface.create()
        val parameterMap = HashMap<String, String>()
        parameterMap["api_key"] = "${Preferences.getPreference(activity!!, "API_KEY")}"
        parameterMap["employee_id"] = "${logindata.user_id}"
        parameterMap["invoice_id"] = "$invoice_id"
        parameterMap["terminal_id"] = "${appSettingData.terminal_id}"
        parameterMap["emp_lat"] = "${Preferences.getPreference(activity!!, "latitude")}"
        parameterMap["emp_log"] = "${Preferences.getPreference(activity!!, "longitude")}"
        Log.i("Result", "parameterMap : $parameterMap")
        CompositeDisposable().add(apiService.callEmployeeDriverCancelDelivery(parameterMap)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe({ result ->
                    if (mDialog != null && mDialog.isShowing)
                        mDialog.dismiss()
                    if (result != null) {
                        Log.i("Result", "" + result.toString())
                        if (result.status == "0") {
                            CUC.displayToast(activity!!, result.show_status, result.message)
                            callEmployeeDriverOrder()
                        } else if (result.status == "10") {
                            apiClass!!.callApiKey(EMPLOYEEDRIVERCANCELDELIVERY)
                        } else {
                            CUC.displayToast(activity!!, result.show_status, result.message)
                        }
                    } else {
                      //  CUC.displayToast(activity!!, "0", getString(R.string.connection_to_database_failed))
                    }
                }, { error ->
                    if (mDialog != null && mDialog.isShowing)
                        mDialog.dismiss()
                    CUC.displayToast(activity!!, "0", getString(R.string.connection_to_database_failed))

                    error.printStackTrace()
                })
        )
    }

    fun callEmployeeDriverAcceptDelivery() {
        mDialog = CUC.createDialog(activity!!)
        val apiService = RequetsInterface.create()
        val parameterMap = HashMap<String, String>()
        parameterMap["api_key"] = "${Preferences.getPreference(activity!!, "API_KEY")}"
        parameterMap["employee_id"] = "${logindata.user_id}"
        parameterMap["invoice_id"] = "$invoice_id"
        parameterMap["terminal_id"] = "${appSettingData.terminal_id}"
        parameterMap["emp_lat"] = "${Preferences.getPreference(activity!!, "latitude")}"
        parameterMap["emp_log"] = "${Preferences.getPreference(activity!!, "longitude")}"
        parameterMap["store_code"] = "$store_code"
        println("aaaaaaaaaaaaaa accept  parameterMap  "+parameterMap)
        Log.i("Result", "parameterMap : $parameterMap")
        CompositeDisposable().add(apiService.callGetEmployeeDriverAcceptDelivery(parameterMap)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe({ result ->
                    if (mDialog != null && mDialog.isShowing)
                        mDialog.dismiss()
                    if (result != null) {
                        println("aaaaaaaaaaaaaa accept  result  "+result.toString())
                        Log.i("Result", "" + result.toString())
                        if (result.status == "0") {
                            CUC.displayToast(activity!!, result.show_status, result.message)
                            callEmployeeDriverOrder()
                        } else if (result.status == "10") {
                            apiClass!!.callApiKey(EMPLOYEEDRIVERACCEPTDELIVERY)
                        } else {
                            CUC.displayToast(activity!!, result.show_status, result.message)
                        }
                    } else {
                       // CUC.displayToast(activity!!, "0", getString(R.string.connection_to_database_failed))
                    }
                }, { error ->
                    if (mDialog != null && mDialog.isShowing)
                        mDialog.dismiss()
                    CUC.displayToast(activity!!, "0", getString(R.string.connection_to_database_failed))
                    println("aaaaaaaaaaaaaa accept  result  "+error.message)
                    error.printStackTrace()
                })
        )
    }

    fun callEmployeeDriverReturnPackage() {
        mDialog = CUC.createDialog(activity!!)
        val apiService = RequetsInterface.create()
        val parameterMap = HashMap<String, String>()
        parameterMap["api_key"] = "${Preferences.getPreference(activity!!, "API_KEY")}"
        parameterMap["employee_id"] = "${logindata.user_id}"
        parameterMap["invoice_id"] = "$invoice_id"
        parameterMap["terminal_id"] = "${appSettingData.terminal_id}"
        parameterMap["emp_lat"] = "${Preferences.getPreference(activity!!, "latitude")}"
        parameterMap["emp_log"] = "${Preferences.getPreference(activity!!, "longitude")}"
        parameterMap["return_reason"] = "$cancelReson"
        Log.i("Result", "parameterMap : $parameterMap")
        CompositeDisposable().add(apiService.callEmployeeDriverReturnPackage(parameterMap)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe({ result ->
                    if (mDialog != null && mDialog.isShowing)
                        mDialog.dismiss()
                    if (result != null) {
                        Log.i("Result", "" + result.toString())
                        if (result.status == "0") {
                            if (customDialog != null && customDialog.isShowing)
                                customDialog.dismiss()
                            callEmployeeDriverOrder()
                            CUC.displayToast(activity!!, result.show_status, result.message)
                        } else if (result.status == "10") {
                            apiClass!!.callApiKey(EMPLOYEEDRIVERRETURNPACKAGE)
                        } else {
                            CUC.displayToast(activity!!, result.show_status, result.message)
                        }
                    } else {
                        //CUC.displayToast(activity!!, "0", getString(R.string.connection_to_database_failed))
                    }
                }, { error ->
                    if (mDialog != null && mDialog.isShowing)
                        mDialog.dismiss()
                    CUC.displayToast(activity!!, "0", getString(R.string.connection_to_database_failed))

                    error.printStackTrace()
                })
        )
    }

    fun callEmployeeDriverDeliveredOrder() {
        mDialog = CUC.createDialog(activity!!)
        val apiService = RequetsInterface.create()
        val parameterMap = HashMap<String, RequestBody>()
        parameterMap["api_key"] = RequestBody.create(MediaType.parse("text/plain"), "${Preferences.getPreference(activity!!, "API_KEY")}")
        parameterMap["employee_id"] = RequestBody.create(MediaType.parse("text/plain"), "${logindata.user_id}")
        parameterMap["invoice_id"] = RequestBody.create(MediaType.parse("text/plain"), "$invoice_id")
        parameterMap["patient_licence_no"] = RequestBody.create(MediaType.parse("text/plain"), "$patient_licence")
        parameterMap["patient_mmmp_licence_no"] = RequestBody.create(MediaType.parse("text/plain"), "$patient_mmmp_licence")
        parameterMap["terminal_id"] = RequestBody.create(MediaType.parse("text/plain"), "${appSettingData.terminal_id}")
        parameterMap["emp_lat"] = RequestBody.create(MediaType.parse("text/plain"), "${Preferences.getPreference(activity!!, "latitude")}")
        parameterMap["emp_log"] = RequestBody.create(MediaType.parse("text/plain"), "${Preferences.getPreference(activity!!, "longitude")}")
        parameterMap["payment_id"] = RequestBody.create(MediaType.parse("text/plain"), "$paymentId")
        parameterMap["payment_amount"] = RequestBody.create(MediaType.parse("text/plain"), "$change_amount")
        //val changeAmount = change_amount.toFloat() - payment_amount.toFloat()
        parameterMap["change_amount"] = RequestBody.create(MediaType.parse("text/plain"), "0")

        val sigantureFile = File(siganture_path)
        Log.i("Result", "parameterMap : $parameterMap")


        CompositeDisposable().add(apiService.callEmployeeDriverDeliveredOrder(parameterMap,
                MultipartBody.Part.createFormData("digital_siganture", "${sigantureFile.name}",
                        RequestBody.create(MediaType.parse("image*//*"), sigantureFile)),
                RequestBody.create(MediaType.parse("text/plain"), "${sigantureFile.name}"))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe({ result ->
                    if (mDialog != null && mDialog.isShowing)
                        mDialog.dismiss()
                    if (result != null) {
                        Log.i("Result", "" + result.toString())
                        if (result.status == "0") {
                            if (dispatch_type_dialog != null && dispatch_type_dialog.isShowing)
                                dispatch_type_dialog.dismiss()
                            CUC.displayToast(activity!!, result.show_status, result.message)
                            callEmployeeDriverOrder()
                        } else if (result.status == "10") {
                            apiClass!!.callApiKey(EMPLOYEEDRIVERDELIVEREDORDER)
                        } else {
                            CUC.displayToast(activity!!, result.show_status, result.message)
                        }
                    } else {
                      //  CUC.displayToast(activity!!, "0", getString(R.string.connection_to_database_failed))
                    }
                }, { error ->
                    if (mDialog != null && mDialog.isShowing)
                        mDialog.dismiss()
                    CUC.displayToast(activity!!, "0", getString(R.string.connection_to_database_failed))

                    error.printStackTrace()
                })
        )
    }

    class OrdersDataAdapter(val context: Context, val onClick: onClickListener) : RecyclerView.Adapter<OrdersDataAdapter.ViewHolder>() {

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            holder!!.bindItems(orderData[position], context, onClick)
        }

        override fun getItemCount(): Int {
            return orderData.size
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            val v = LayoutInflater.from(parent!!.context).inflate(R.layout.driver_order_item, parent, false)
            return ViewHolder(v)
        }

        class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
            val ll_employee_order_item: LinearLayout = itemView.findViewById(R.id.ll_employee_order_item)

            val tv_store_name: TextView = itemView.findViewById(R.id.tv_store_name)
            val tv_invoice_unique_no: TextView = itemView.findViewById(R.id.tv_invoice_unique_no)
            val tv_payment_type: TextView = itemView.findViewById(R.id.tv_payment_type)
            val tv_amount: TextView = itemView.findViewById(R.id.tv_amount)
            val tv_invoice_no: TextView = itemView.findViewById(R.id.tv_invoice_no)
            val tv_invoice_date: TextView = itemView.findViewById(R.id.tv_invoice_date)
            val llZipcode: LinearLayout = itemView.findViewById(R.id.llZipcode)
            val rlBottomTab: RelativeLayout = itemView.findViewById(R.id.rlBottomTab)
            fun bindItems(data: GetOrderData, context: Context, onClick: onClickListener) {
                tv_store_name.text = "${data.patient_fname} ${data.patient_lname}"
                tv_invoice_unique_no.text = "${data.invoice_delivery_address}"
                tv_payment_type.text = "${data.patient_zipcode}"
                tv_amount.text = "${data.patient_mobile_no}"
                tv_invoice_no.text = "${data.distance}"
                llZipcode.visibility = View.VISIBLE
                rlBottomTab.visibility = View.VISIBLE
                if (data.invoice_created_date != "") {
                    //"2018-04-28 14:13:56"
                    tv_invoice_date.text = CUC.getdatefromoneformattoAnother("yyyy-MM-dd hh:mm:ss", "MM-dd-yyyy", data.invoice_created_date)
                } else {
                    tv_invoice_date.text = ""
                }
                ll_employee_order_item.setOnClickListener {
                    onClick.callClick(data)
                }
            }
        }
    }

    class PickupsAdapter(val context: Context, val onClick: onPickupsClickListener) : RecyclerView.Adapter<PickupsAdapter.ViewHolder>() {

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            holder!!.bindItems(pickups[position], context, onClick)
        }

        override fun getItemCount(): Int {
            return pickups.size
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            val v = LayoutInflater.from(parent!!.context).inflate(R.layout.driver_order_item, parent, false)
            return ViewHolder(v)
        }

        class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
            val ll_employee_order_item: LinearLayout = itemView.findViewById(R.id.ll_employee_order_item)

            val tv_store_name: TextView = itemView.findViewById(R.id.tv_store_name)
            val tv_invoice_unique_no: TextView = itemView.findViewById(R.id.tv_invoice_unique_no)
            val tv_payment_type: TextView = itemView.findViewById(R.id.tv_payment_type)
            val tv_amount: TextView = itemView.findViewById(R.id.tv_amount)
            val tv_invoice_no: TextView = itemView.findViewById(R.id.tv_invoice_no)
            val tv_invoice_date: TextView = itemView.findViewById(R.id.tv_invoice_date)
            val llZipcode: LinearLayout = itemView.findViewById(R.id.llZipcode)
            val rlBottomTab: RelativeLayout = itemView.findViewById(R.id.rlBottomTab)

            fun bindItems(data: GetPickups, context: Context, onClick: onPickupsClickListener) {
                tv_store_name.text = "${data.patient_name}"
                tv_invoice_unique_no.text = "${data.delivery_address}"
                //tv_payment_type.text = "${data.patient_zipcode}"
                tv_amount.text = "${data.patient_mobile_no}"
                //tv_invoice_no.text = "${data.distance}"
                llZipcode.visibility = View.GONE
                rlBottomTab.visibility = View.GONE
//                if (data.invoice_created_date != "") {
//                    //"2018-04-28 14:13:56"
//                    tv_invoice_date.text = CUC.getdatefromoneformattoAnother("yyyy-MM-dd hh:mm:ss", "MM-dd-yyyy", data.invoice_created_date)
//                } else {
//                    tv_invoice_date.text = ""
//                }
                ll_employee_order_item.setOnClickListener {
                    onClick.callPickupsClick(data)
                }
            }
        }
    }


    class PaymentAdapter(context: Context?, val resource: Int, val companyList: java.util.ArrayList<GetPaymentTypes>,
                         val payment_path: String, val clickListner: onClickPayment) :
            ArrayAdapter<GetPaymentTypes>(context, resource, companyList) {

        override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
            var rootView = convertView
            if (rootView == null)
                rootView = LayoutInflater.from(context!!).inflate(resource, null, false)

            val ll_ = rootView!!.findViewById<RelativeLayout>(R.id.ll_)
            val tv_payment = rootView!!.findViewById<TextView>(R.id.tv_payment)
            val iv_payment = rootView!!.findViewById<ImageView>(R.id.iv_payment)
            val data = companyList[position]
            Glide.with(context!!)
                    .applyDefaultRequestOptions(RequestOptions().format(DecodeFormat.PREFER_ARGB_8888).error(R.drawable.no_image_available))
                    .load("$payment_path${data.payment_logo}")
                    .into(iv_payment)
            tv_payment.text = "${data.payment_method}"
            ll_.setOnClickListener {
                clickListner.callClick(data)
            }
            return rootView!!
        }
    }

    interface onClickPayment {
        fun callClick(data: GetPaymentTypes)
    }

    interface onPickupsClickListener {
        fun callPickupsClick(data: GetPickups)
    }

    interface onClickListener {
        fun callClick(data: GetOrderData)
    }
}