package com.budbasic.posconsole.driver

import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.os.Bundle
import android.os.Environment
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.widget.LinearLayout
import android.widget.TextView
import com.google.gson.Gson
import com.kotlindemo.model.GetEmployeeAppSetting
import com.budbasic.posconsole.R
import com.budbasic.posconsole.global.SignatureView
import com.budbasic.customer.global.Preferences
import com.budbasic.global.CUC
import java.io.File
import java.io.FileOutputStream

class SignatureActivity : AppCompatActivity() {
    lateinit var mAppBasic: GetEmployeeAppSetting

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_signature)
        mAppBasic = Gson().fromJson(Preferences.getPreference(this@SignatureActivity, "AppSetting"), GetEmployeeAppSetting::class.java)
        val signview = findViewById<SignatureView>(R.id.signview)
        val cv_done = findViewById<LinearLayout>(R.id.cv_done)
        val ll_clear = findViewById<LinearLayout>(R.id.ll_clear)
        val ll_back = findViewById<LinearLayout>(R.id.ll_back)
        val tv_note = findViewById<TextView>(R.id.tv_note)
        tv_note.text = "${mAppBasic.App_driver_sign_note}"
        ll_back.setOnClickListener {
            finish()
        }
        ll_clear.setOnClickListener {
            signview.clearSignature()
        }

        cv_done.setOnClickListener {
            if(signview.GetPaint()){
                CUC.displayToast(this, "0", "Patient signature required")
            }else{
                saveImage(signview.signature)
            }
         //   Log.d("PaintPath"," = "+signview.GetPaint())
           // Log.d("SignviewSignature",""+signview.signature)
           // saveImage(signview.signature)
        }
        //
    }

    /**
     * save the signature to an sd card directory
     *
     * @param signature bitmap
     */
    internal fun saveImage(signature: Bitmap) {

        val root = Environment.getExternalStorageDirectory().toString()

        // the directory where the signature will be saved
        val myDir = File("$root/saved_signature")

        // make the directory if it does not exist yet
        if (!myDir.exists()) {
            myDir.mkdirs()
        }

        // set the file name of your choice
        val fname = "signature.png"

        // in our case, we delete the previous file, you can remove this
        val file = File(myDir, fname)
        if (file.exists()) {
            file.delete()
        }

        try {

            // save the signature
            val out = FileOutputStream(file)
            signature.compress(Bitmap.CompressFormat.PNG, 90, out)
            out.flush()
            out.close()
           // Log.d("siganture_path333",""+"${file.absolutePath}")
            val callBackIntent = Intent()
            callBackIntent.putExtra("siganture_path", "${file.absolutePath}")
            setResult(Activity.RESULT_OK, callBackIntent)
            finish()
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

}