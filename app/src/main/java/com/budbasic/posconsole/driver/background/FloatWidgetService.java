package com.budbasic.posconsole.driver.background;

import android.app.Service;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.graphics.PixelFormat;
import android.os.Build;
import android.os.IBinder;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.budbasic.posconsole.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class FloatWidgetService extends Service {
    private WindowManager mWindowManager;
    private View mFloatingWidget;
    private Date date1,date2;
    private ImageView collapsed_iv;
    private RelativeLayout relativeLayoutParent;
    Intent intent;

    public FloatWidgetService() {
    }
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
    @Override
    public void onCreate() {
        super.onCreate();

        mFloatingWidget = LayoutInflater.from(this).inflate(R.layout.layout_floating_widget, null);

        int LAYOUT_FLAG;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            LAYOUT_FLAG = WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY;
        } else {
            LAYOUT_FLAG = WindowManager.LayoutParams.TYPE_PHONE;
        }

        final WindowManager.LayoutParams params = new WindowManager.LayoutParams(
                WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.WRAP_CONTENT,
                LAYOUT_FLAG,
                WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
                PixelFormat.TRANSLUCENT);

        params.gravity = Gravity.CENTER | Gravity.LEFT;
        params.x = 0;
        params.y = 100;

         intent = new Intent(FloatWidgetService.this, BackgroundService.class);
        bindService(intent, serviceConnection, Context.BIND_AUTO_CREATE);

        mWindowManager = (WindowManager) getSystemService(WINDOW_SERVICE);
        mWindowManager.addView(mFloatingWidget, params);

         collapsed_iv=mFloatingWidget.findViewById(R.id.collapsed_iv);
        relativeLayoutParent=mFloatingWidget.findViewById(R.id.relativeLayoutParent);

        collapsed_iv.setVisibility(View.VISIBLE);

        collapsed_iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                collapsed_iv.setVisibility(View.GONE);
                relativeLayoutParent.setVisibility(View.GONE);
                System.out.println("aaaaaaaaa  yes ");
                try {
                    Intent i = new Intent(FloatWidgetService.this, BackgroundLOcationActivity.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    i.putExtra("position",1);
                    startActivity(i);
                    stopSelf();
                    stopService(intent);
                    mWindowManager.removeView(mFloatingWidget);
                }catch (Exception e){

                }
            }
        });

    }
    private ServiceConnection serviceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName className, IBinder service) {
            BackgroundService.LocalBinder binder = (BackgroundService.LocalBinder) service;
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            //  bound = false;

        }
    };


    @Override
    public void onDestroy() {
        super.onDestroy();
        //if (mFloatingWidget != null) mWindowManager.removeView(mFloatingWidget);
    }

}
