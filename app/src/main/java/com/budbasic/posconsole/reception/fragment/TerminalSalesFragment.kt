package com.budbasic.posconsole.reception.fragment

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.os.StrictMode
import android.support.v4.app.Fragment
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.widget.TextView
import com.budbasic.customer.global.Preferences
import com.budbasic.global.CUC
import com.budbasic.posconsole.R
import com.budbasic.posconsole.model.APIClass
import com.budbasic.posconsole.webservice.RequetsInterface
import com.google.gson.Gson
import com.kotlindemo.model.GetEmployeeLogin
import com.kotlindemo.model.GetSalesTerminals
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.terminal_sales_fragment.view.*
import android.support.v7.widget.CardView
import android.view.*
import android.widget.EditText
import android.widget.LinearLayout
import com.kotlindemo.model.GetEmployeeAppSetting


class TerminalSalesFragment : Fragment(), APIClass.onCallListner {
    override fun onFailed(requestCode: Int) {
    }

    override fun onSuccess(requestCode: Int) {
    }

    //terminal_sales_fragment
    var mView: View? = null
    var mDialog: Dialog? = null
    var logindata: GetEmployeeLogin? = null
    var apiClass: APIClass? = null
    var productAdapter: ProductAdapter? = null
    var appSettingData: GetEmployeeAppSetting? = null

    companion object {
        var terminalSalesList = ArrayList<GetSalesTerminals>()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        if (view != null)
            mView = view!!
        else
            mView = inflater.inflate(R.layout.terminal_sales_fragment, container, false)
        apiClass = APIClass(context!!, this)
        appSettingData = Gson().fromJson(Preferences.getPreference(activity!!, "AppSetting"), GetEmployeeAppSetting::class.java)
        logindata = Gson().fromJson(Preferences.getPreference(mView!!.context, "logindata"), GetEmployeeLogin::class.java)
        val layout_manager = GridLayoutManager(mView!!.context, 1)
        mView!!.rv_terminal_sales!!.layoutManager = layout_manager
        productAdapter = ProductAdapter(mView!!.context, object : OnClickCall {
            override fun onPettyCashSetting(getSalesTerminals: GetSalesTerminals) {
                DisplayTerminalPettyLimit(getSalesTerminals)
            }

            override fun onCashTakeOut(getSalesTerminals: GetSalesTerminals) {
                DisplayTerminalTakeoutSales(getSalesTerminals)
            }

        })
        mView!!.rv_terminal_sales!!.adapter = productAdapter
        mView!!.mSwipeRefreshLayout.setOnRefreshListener {
            callEmployeeTerminalSales()
        }
        mView!!.btn_back.setOnClickListener {
            activity!!.onBackPressed()
        }
        callEmployeeTerminalSales()
        return mView
    }

    fun callEmployeeTerminalSales() {
        Log.i("Result", "callEmployeeSalesReport()")
        try {
            if (!activity!!.isFinishing) {
                if (mView!!.mSwipeRefreshLayout != null) {
                    mView!!.mSwipeRefreshLayout.isRefreshing = false
                }
                mDialog = CUC.createDialog(activity!!)
                val apiService = RequetsInterface.Factory.create()
                val parameterMap = HashMap<String, String>()
                parameterMap["api_key"] = "${Preferences.getPreference(activity!!, "API_KEY")}"
                if (logindata != null) {
                    parameterMap["employee_id"] = "${logindata!!.user_id}"
                }
                Log.i("Result", "$parameterMap")
                CompositeDisposable().add(apiService.callEmployeeTerminalSales(parameterMap)
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.io())
                        .subscribe({ result ->
                            if (mDialog != null && mDialog!!.isShowing)
                                mDialog!!.dismiss()
                            if (result != null) {
                                Log.i("Result", "" + result.toString())
                                if (result.status == "0") {
                                    terminalSalesList.clear()
                                    terminalSalesList.addAll(result.sales_terminals)
                                    productAdapter!!.notifyDataSetChanged()
                                } else if (result.status == "10") {
                                    apiClass!!.callApiKey(103)
                                } else {

                                }
                            } else {
                                if (!activity!!.isFinishing) {
                                 //   CUC.displayToast(activity!!, "0", getString(R.string.connection_to_database_failed))
                                }
                            }
                        }, { error ->
                            if (!activity!!.isFinishing) {
                                if (mDialog != null && mDialog!!.isShowing)
                                    mDialog!!.dismiss()
                               // CUC.displayToast(activity!!, "0", getString(R.string.connection_to_database_failed))
                            }
                            error.printStackTrace()
                        })
                )
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    class ProductAdapter(val context: Context, val onClickCall: OnClickCall) : RecyclerView.Adapter<ProductAdapter.ViewHolder>() {

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            holder!!.bindItems(terminalSalesList[position], onClickCall)
        }

        override fun getItemCount(): Int {
            return terminalSalesList.size
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            val v = LayoutInflater.from(parent!!.context).inflate(R.layout.terminal_sales_item, parent, false)
            return ViewHolder(v)
        }

        class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
            val tv_terminal_name = itemView.findViewById<TextView>(R.id.tv_terminal_name)
            val tv_terminal_employee_name = itemView.findViewById<TextView>(R.id.tv_terminal_employee_name)
            val tv_terminal_sales = itemView.findViewById<TextView>(R.id.tv_terminal_sales)
            val tv_percentage = itemView.findViewById<TextView>(R.id.tv_percentage)
            val tv_petty_cash = itemView.findViewById<TextView>(R.id.tv_petty_cash)
            val tv_pettycash_amount = itemView.findViewById<TextView>(R.id.tv_pettycash_amount)
            val cv_petty_cash_setting = itemView.findViewById<CardView>(R.id.cv_petty_cash_setting)
            val cv_cash_takeout = itemView.findViewById<CardView>(R.id.cv_cash_takeout)


            fun bindItems(getSalesTerminals: GetSalesTerminals, onClickCall: OnClickCall) {
                tv_terminal_name.text = getSalesTerminals.terminal_name
                tv_terminal_employee_name.text = getSalesTerminals.terminal_employee_name

                tv_terminal_sales.text = "+"+getSalesTerminals.terminal_sales
                tv_percentage.text = "%${getSalesTerminals.terminal_status_percentage}"
                tv_petty_cash.text = "$"+getSalesTerminals.terminal_pettycash
                tv_pettycash_amount.text = "$"+getSalesTerminals.terminal_pettycash_alert

                cv_petty_cash_setting.setOnClickListener {
                    onClickCall.onPettyCashSetting(getSalesTerminals)
                }

                cv_cash_takeout.setOnClickListener {
                    onClickCall.onCashTakeOut(getSalesTerminals)
                }
            }
        }
    }

    interface OnClickCall {
        fun onPettyCashSetting(getSalesTerminals: GetSalesTerminals)
        fun onCashTakeOut(getSalesTerminals: GetSalesTerminals)
    }

    var dialogProductQty: Dialog? = null
    var pin_value = ""
    var terminal_sales = ""
    var terminal_id = ""
    private fun DisplayTerminalTakeoutSales(salesTerminals: GetSalesTerminals) {
        pin_value = ""
        terminal_id = ""
        dialogProductQty = Dialog(activity!!, R.style.TransparentProgressDialog)
        val policy = StrictMode.ThreadPolicy.Builder().permitAll().build()
        StrictMode.setThreadPolicy(policy)
        dialogProductQty!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialogProductQty!!.setContentView(R.layout.take_cashout_dialog)
        dialogProductQty!!.window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)
        dialogProductQty!!.show()

        val cv_approve_request = dialogProductQty!!.findViewById<LinearLayout>(R.id.cv_approve_request)
        val cv_generate_invoice = dialogProductQty!!.findViewById<LinearLayout>(R.id.cv_generate_invoice)
        val edt_pin = dialogProductQty!!.findViewById<EditText>(R.id.edt_pin)

        cv_approve_request.setOnClickListener {
            dialogProductQty!!.dismiss()
        }

        cv_generate_invoice.setOnClickListener {
            terminal_sales = salesTerminals.terminal_sales
            if (edt_pin.text.toString() == "") {
                CUC.displayToast(activity!!, "0", "Please Enter Your PIN")
            } else {
                pin_value = edt_pin.text.toString()
                terminal_id = salesTerminals.terminal_id
                callEmployeeTerminalTakeoutSales()
            }
        }
    }

    var takeout_cash = ""
    var pettycash_alert = ""

    private fun DisplayTerminalPettyLimit(salesTerminals: GetSalesTerminals) {
        pin_value = ""
        takeout_cash = ""
        terminal_id = ""
        dialogProductQty = Dialog(activity!!, R.style.TransparentProgressDialog)
        val policy = StrictMode.ThreadPolicy.Builder().permitAll().build()
        StrictMode.setThreadPolicy(policy)
        dialogProductQty!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialogProductQty!!.setContentView(R.layout.take_cashout_dialog)
        dialogProductQty!!.window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)
        dialogProductQty!!.show()

        val cv_approve_request = dialogProductQty!!.findViewById<LinearLayout>(R.id.cv_approve_request)
        val cv_generate_invoice = dialogProductQty!!.findViewById<LinearLayout>(R.id.cv_generate_invoice)
        val edt_pin = dialogProductQty!!.findViewById<EditText>(R.id.edt_pin)

        val ll_takeout_cash = dialogProductQty!!.findViewById<LinearLayout>(R.id.ll_takeout_cash)
        val tv_takeout_cash = dialogProductQty!!.findViewById<TextView>(R.id.tv_takeout_cash)
        val edt_takeout_cash = dialogProductQty!!.findViewById<EditText>(R.id.edt_takeout_cash)

        val ll_pettycash_alert = dialogProductQty!!.findViewById<LinearLayout>(R.id.ll_pettycash_alert)
        val tv_pettycash_alert = dialogProductQty!!.findViewById<TextView>(R.id.tv_pettycash_alert)
        val edt_pettycash_alert = dialogProductQty!!.findViewById<EditText>(R.id.edt_pettycash_alert)

        tv_takeout_cash.text = "Terminal Cash"
        edt_takeout_cash.hint = "Terminal Cash"
        ll_takeout_cash.visibility = View.VISIBLE
        ll_pettycash_alert.visibility = View.VISIBLE

        edt_takeout_cash.setText("${salesTerminals.terminal_pettycash}")
        edt_pettycash_alert.setText("${salesTerminals.terminal_pettycash_alert}")

        cv_approve_request.setOnClickListener {
            dialogProductQty!!.dismiss()
        }

        cv_generate_invoice.setOnClickListener {
            terminal_sales = salesTerminals.terminal_sales
            if (edt_takeout_cash.text.toString() == "") {
                CUC.displayToast(activity!!, "0", "Please Enter Terminal Cash")
            } else if (edt_pettycash_alert.text.toString() == "") {
                CUC.displayToast(activity!!, "0", "Please Enter Terminal Cash Alert")
            } else if (edt_pin.text.toString() == "") {
                CUC.displayToast(activity!!, "0", "Please Enter Your PIN")
            } else {
                takeout_cash = edt_takeout_cash.text.toString()
                pettycash_alert = edt_pettycash_alert.text.toString()
                pin_value = edt_pin.text.toString()
                terminal_id = salesTerminals.terminal_id
                callEmployeeTerminalPettyLimit()
            }
        }
    }

    fun callEmployeeTerminalTakeoutSales() {
        if (CUC.isNetworkAvailablewithPopup(activity!!)) {
            mDialog = CUC.createDialog(activity!!)
            val apiService = RequetsInterface.create()
            val parameterMap = HashMap<String, String>()
            parameterMap["api_key"] = Preferences.getPreference(activity!!, "API_KEY")
            if (logindata != null) {
                parameterMap["employee_id"] = logindata!!.user_id
            }
            parameterMap["terminal_id"] = terminal_id
            parameterMap["takeout_cash"] = terminal_sales
            parameterMap["employee_pin"] = pin_value

            Log.i("Result", "parameterMap : $parameterMap")
            CompositeDisposable().add(apiService.callEmployeeTerminalTakeoutSales(parameterMap)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe({ result ->
                        if (mDialog != null && mDialog!!.isShowing)
                            mDialog!!.dismiss()
                        if (result != null) {
                            Log.i("Result", "" + result.toString())
                            if (result.status == "0") {
                                dialogProductQty!!.dismiss()
                                callEmployeeTerminalSales()
                            } else if (result.status == "10") {
                                //callApiKey()
                            } else {
                                CUC.displayToast(activity!!, result.show_status, result.message)
                            }
                        } else {
                           // CUC.displayToast(activity!!, "0", getString(R.string.connection_to_database_failed))
                        }
                    }, { error ->
                        if (mDialog != null && mDialog!!.isShowing)
                            mDialog!!.dismiss()
                      //  CUC.displayToast(activity!!, "0", getString(R.string.connection_to_database_failed))
                        error.printStackTrace()
                    })
            )
        }
    }

    fun callEmployeeTerminalPettyLimit() {
        if (CUC.isNetworkAvailablewithPopup(activity!!)) {
            mDialog = CUC.createDialog(activity!!)
            val apiService = RequetsInterface.create()
            val parameterMap = HashMap<String, String>()
            parameterMap["api_key"] = Preferences.getPreference(activity!!, "API_KEY")
            if (logindata != null) {
                parameterMap["employee_id"] = logindata!!.user_id
                //parameterMap["store_id"] = logindata!!.user_store_id
            }
            parameterMap["terminal_id"] = terminal_id
            parameterMap["employee_pin"] = pin_value

            parameterMap["terminal_pettycash"] = takeout_cash
            parameterMap["terminal_pettycash_alert"] = pettycash_alert

            Log.i("Result", "parameterMap : $parameterMap")
            CompositeDisposable().add(apiService.callEmployeeTerminalPettyLimit(parameterMap)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe({ result ->
                        if (mDialog != null && mDialog!!.isShowing)
                            mDialog!!.dismiss()
                        if (result != null) {
                            Log.i("Result", "" + result.toString())
                            if (result.status == "0") {
                                dialogProductQty!!.dismiss()
                                callEmployeeTerminalSales()
                            } else if (result.status == "10") {
                                //callApiKey()
                            } else {
                                CUC.displayToast(activity!!, result.show_status, result.message)
                            }
                        } else {
                          //  CUC.displayToast(activity!!, "0", getString(R.string.connection_to_database_failed))
                        }
                    }, { error ->
                        if (mDialog != null && mDialog!!.isShowing)
                            mDialog!!.dismiss()
                        CUC.displayToast(activity!!, "0", getString(R.string.connection_to_database_failed))
                        error.printStackTrace()
                    })
            )
        }
    }
}