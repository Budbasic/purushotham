package com.budbasic.posconsole.reception.fragment

import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.net.Uri
import android.os.Bundle
import android.provider.Settings
import android.support.design.widget.Snackbar
import android.support.v4.app.ActivityCompat
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import com.budbasic.customer.global.Preferences
import com.budbasic.posconsole.BarcodeSccan
import com.budbasic.posconsole.ProductSearch
import com.budbasic.posconsole.R
import com.budbasic.posconsole.global.EmployeeSearchProducts
import com.budbasic.posconsole.printer.fragment.BluetoothScan
import com.budbasic.posconsole.queue.BarCodeScan
import com.budbasic.posconsole.reception.Bar_Qr_codeActivity
import com.budbasic.posconsole.webservice.RequetsInterface
import com.google.gson.Gson
import com.kotlindemo.model.GetEmployeeAppSetting
import com.kotlindemo.model.GetEmployeeLogin
import com.kotlindemo.model.GetPrinterRegister
import com.kotlindemo.model.GetSettingList
import com.tbruyelle.rxpermissions2.RxPermissions
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.functions.Consumer
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_setting.view.*


class SettingFragment : Fragment() {
    var root: View? = null
    lateinit var logindata: GetEmployeeLogin
    lateinit var appSettingData: GetEmployeeAppSetting

    var settingListing = ArrayList<GetSettingList>()

    lateinit var mSettingAdapter: SettingAdapter
    var rxPermissions: RxPermissions? = null
    internal val CAMERA = 0x7

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        if (view != null)
            root = view!!
        else
            root = inflater.inflate(R.layout.activity_setting, container, false)
        logindata = Gson().fromJson(Preferences.getPreference(context!!, "logindata"), GetEmployeeLogin::class.java)
        appSettingData = Gson().fromJson(Preferences.getPreference(context!!, "AppSetting"), GetEmployeeAppSetting::class.java)
        // check if BlinkID is supported on the device
//        val supportStatus = RecognizerCompatibility.getRecognizerCompatibilityStatus(activity)
//        if (supportStatus != RecognizerCompatibilityStatus.RECOGNIZER_SUPPORTED) {
//            Toast.makeText(activity, "BlinkID is not supported! Reason: " + supportStatus.name, Toast.LENGTH_LONG).show()
//        }

        init()
        return root
    }


    private fun init() {
        initList()

        root!!.ll_setting_main.setOnClickListener {

        }
        root!!.rv_setting_list.layoutManager = GridLayoutManager(activity, 4)
        mSettingAdapter = SettingAdapter(settingListing, object : itemClickListner {
            override fun onClick(data: GetSettingList) {
                if (data.id == 0) {
                    val goIntent = Intent(activity, BluetoothScan::class.java)
                    startActivity(goIntent)
                } else if (data.id == 1) {
                    askForPermission(android.Manifest.permission.CAMERA, CAMERA)
                } else if (data.id == 2) {

                    val goIntent = Intent(activity, BarCodeScan::class.java)
                   // goIntent.putExtra("status", "1")
                   // goIntent.putExtra("printername","Recep")
                    startActivity(goIntent)
                } else if (data.id == 3) {
                    startActivity(Intent(activity, ProductSearch::class.java))

                    val mDialog = BarCodePrintFragment()
                   // mDialog.show(fragmentManager, "Product Info")
                   // val goIntent = Intent(activity, BarCodePrintFragment::class.java)
                   // goIntent.putExtra("status", "0")
                   // goIntent.putExtra("printername","label")
                   // startActivity(goIntent)
                }

            }

        })
        root!!.rv_setting_list.adapter = mSettingAdapter
    }

    private fun askForPermission(permission: String, requestCode: Int?) {
        if (ContextCompat.checkSelfPermission(activity!!, permission) != PackageManager.PERMISSION_GRANTED) {
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(activity!!, permission)) {
                //This is called if user has denied the permission before
                //In this case I am just asking the permission again
                ActivityCompat.requestPermissions(activity!!, arrayOf(permission), requestCode!!)
            } else {
                ActivityCompat.requestPermissions(activity!!, arrayOf(permission), requestCode!!)
            }
        } else {
            //scanID()
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (ActivityCompat.checkSelfPermission(activity!!, permissions[0]) == PackageManager.PERMISSION_GRANTED) {
            when (requestCode) {
                //Camera
                7 -> {
                }
            }
        } else {
            Toast.makeText(activity!!, "Permission denied", Toast.LENGTH_SHORT).show()
        }

    }

    private fun initList() {
        settingListing.add(GetSettingList(0, R.drawable.ic_labelprinter_connection, "PRINTER CONNECTION"))
       settingListing.add(GetSettingList(1, R.drawable.tv_idcard, "ID Verification"))
        settingListing.add(GetSettingList(2, R.drawable.ic_receipt_connection, "PRINTER BARCODE"))
        settingListing.add(GetSettingList(3, R.drawable.ic_printerqrcode, "PRINT QR CODE"))
    }

    override fun onStart() {
        super.onStart()
        Log.i(SettingFragment::class.java.simpleName, "onStart()")
    }

    override fun onStop() {
        super.onStop()
        Log.i(SettingFragment::class.java.simpleName, "onStop()")
    }


    override fun onDestroy() {
        super.onDestroy()
        Log.i(SettingFragment::class.java.simpleName, "onDestroy()")
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        Log.i("Result", "onActivityResult(), $requestCode : $resultCode")
    }

    class SettingAdapter(val settingListing: ArrayList<GetSettingList>, val clickListner: itemClickListner) : RecyclerView.Adapter<SettingAdapter.SettingHolder>() {
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SettingHolder {
            val view = LayoutInflater.from(parent.context).inflate(R.layout.setting_printer_item, parent, false)
            return SettingHolder(view)
        }

        override fun getItemCount(): Int {
            return settingListing.size
        }

        override fun onBindViewHolder(holder: SettingHolder, position: Int) {
            holder.bindItem(settingListing[position], clickListner)
        }

        class SettingHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
            val ll_setting_item: LinearLayout = itemView.findViewById(R.id.ll_setting_item)
            val imageView: ImageView = itemView.findViewById(R.id.imageView)
            val tv_category_title: TextView = itemView.findViewById(R.id.tv_category_title)
            fun bindItem(data: GetSettingList, clickListner: itemClickListner) {
              //  imageView.setColorFilter(Color.BLACK)
                imageView.setImageResource(data.imageID)
                //Log.d("Itemame222"," = "+ "${data.itemName}")
                tv_category_title.text = "${data.itemName}"
                ll_setting_item.setOnClickListener {
                    clickListner.onClick(data)
                }
            }
        }
    }

    interface itemClickListner {
        fun onClick(data: GetSettingList)
    }


}