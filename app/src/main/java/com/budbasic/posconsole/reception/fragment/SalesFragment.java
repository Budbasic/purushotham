package com.budbasic.posconsole.reception.fragment;


import android.Manifest;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.budbasic.customer.global.Preferences;
import com.budbasic.global.CUC;
import com.budbasic.posconsole.R;
import com.budbasic.posconsole.adapter.ProductCartAdapter;
import com.budbasic.posconsole.adapter.TaxitemAdapter;
import com.budbasic.posconsole.dialogs.AddPaymentDialog;
import com.budbasic.posconsole.dialogs.AddProductDialog;
import com.budbasic.posconsole.dialogs.AddPromocardDialog;
import com.budbasic.posconsole.driver.SignatureActivity;
import com.budbasic.posconsole.reception.SalesActivity;
import com.budbasic.posconsole.reception.ViewPagerAdapter;
import com.budbasic.posconsole.sales.SalesReceiptPrintActivity;
import com.budbasic.posconsole.sales.UserInfoDetails;
import com.budbasic.posconsole.webservice.RequetsInterface;
import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.kotlindemo.model.EmployeePatientInfo;
import com.kotlindemo.model.GetCartData;
import com.kotlindemo.model.GetData;
import com.kotlindemo.model.GetEmployeeAppSetting;
import com.kotlindemo.model.GetEmployeeLogin;
import com.kotlindemo.model.GetEmployeePatientCart;
import com.kotlindemo.model.GetEmployeePatientPromocodeList;
import com.kotlindemo.model.GetEmployeeQenerateInvoice;
import com.kotlindemo.model.GetEmployeeSalesProducts;
import com.kotlindemo.model.GetPaymentTypes;
import com.kotlindemo.model.GetProducts;
import com.kotlindemo.model.GetPromosales;
import com.kotlindemo.model.GetQueueList;
import com.kotlindemo.model.GetStatus;
import com.kotlindemo.model.GetTaxData;
import com.kotlindemo.model.notesdata;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import kotlin.jvm.internal.Intrinsics;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

/**
 * A simple {@link Fragment} subclass.
 */
@SuppressLint("ValidFragment")
public class SalesFragment extends Fragment {


    @SuppressLint("ValidFragment")
    public SalesFragment(String queue_patient_id,String queue_id,GetEmployeeLogin logindata, GetQueueList queueList) {
        // Required empty public constructor
        this.queue_patient_id=queue_patient_id;
        this.queue_id=queue_id;
        this.logindata=logindata;
        this.queueList=queueList;
    }
    private TabLayout tab_layout;
    private ViewPager mViewPager;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    public Dialog mDialog;
    public GetEmployeeLogin logindata;
    private LinearLayout rl_bottom;
    private String queue_patient_id,assign_msg,queue_id,mainCartId,payment_path,Store_sign_required,patient_point,discount_value,
            cart_manual_discount_type,Currency_value,total_payble,image_path;
    private List<GetData> cateList;
    List<GetPaymentTypes> paymentTypeList ;
    private List<GetProducts> searchdataArrayList;
    GetQueueList queueList;
    GetEmployeeAppSetting mBAsis;
    Gson gson;
    private ImageView ivpatientcart;
    private LinearLayout img_exmlator;
    private TextView tvcustomername,tv_limit_value,textLimitUsage,tv_current_order,tv_applyremove,tv_cartisempty,tv_total_qty,
            tv_subtotal,tv_promocode,tvGetPromoCode,tv_grandtotal;
    Float noOfAddedItem = 0f,pieGraphLimite=0f;
    GetEmployeePatientCart viewCard;
    List<GetCartData> list;
    private ProductCartAdapter productCartAdapter;
    private LinearLayout ll_redeem_option,ll_send_order,ll_cancel,ll_promoTab,total_unit;
    private RecyclerView rv_cart_list,rv_tax_list;
    List<GetTaxData> taxList;
    private TaxitemAdapter taxItemAdapter;
    String discount_type;
    private Dialog dialogProductQty;
    private ArrayList<GetPromosales> peomocedelist;
    private String siganture_path;
    private ArrayList permissionsToRequest;
    private ArrayList permissionsRejected = new ArrayList();
    private ArrayList permissions = new ArrayList();
    private final static int ALL_PERMISSIONS_RESULT = 107;
    int request_code = 7070, tabposition;
    Float paymentamt;
    ArrayList<notesdata> notsdata;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_sales, container, false);
        tab_layout=view.findViewById(R.id.tab_layout);
        mViewPager=view.findViewById(R.id.mViewPager);
        mSwipeRefreshLayout=view.findViewById(R.id.mSwipeRefreshLayout);
        ivpatientcart=view.findViewById(R.id.ivpatientcart);
        tvcustomername=view.findViewById(R.id.tvcustomername);
        tv_limit_value=view.findViewById(R.id.tv_limit_value);
        textLimitUsage=view.findViewById(R.id.textLimitUsage);
        img_exmlator=view.findViewById(R.id.img_exmlator);
        tv_current_order=view.findViewById(R.id.tv_current_order);
        ll_send_order=view.findViewById(R.id.ll_send_order);
        ll_redeem_option=view.findViewById(R.id.ll_redeem_option);
        ll_cancel=view.findViewById(R.id.ll_cancel);
        ll_promoTab=view.findViewById(R.id.ll_promoTab);
        total_unit=view.findViewById(R.id.total_unit);
        tv_applyremove=view.findViewById(R.id.tv_applyremove);
        tv_cartisempty=view.findViewById(R.id.tv_cartisempty);
        tv_total_qty=view.findViewById(R.id.tv_total_qty);
        tv_subtotal=view.findViewById(R.id.tv_subtotal);
        rv_cart_list=view.findViewById(R.id.rv_cart_list);
        tv_promocode=view.findViewById(R.id.tv_promocode);
        tvGetPromoCode=view.findViewById(R.id.tvGetPromoCode);
        rv_tax_list=view.findViewById(R.id.rv_tax_list);
        tv_grandtotal=view.findViewById(R.id.tv_grandtotal);

        rl_bottom=view.findViewById(R.id.rl_bottom);
        rl_bottom.setVisibility(View.GONE);
        gson = new Gson();
        mBAsis = gson.fromJson(Preferences.INSTANCE.getPreference(getContext(), "AppSetting"), GetEmployeeAppSetting.class);

        Currency_value=mBAsis.getCurrency_value();
        rv_cart_list.setLayoutManager(new GridLayoutManager(getContext(),1));
        cateList =new  ArrayList<GetData>();
        list=new  ArrayList<GetCartData>();
        taxList=new  ArrayList<GetTaxData>();
        notsdata=new  ArrayList<notesdata>();
        peomocedelist=new ArrayList<GetPromosales>();
        paymentTypeList =new  ArrayList<GetPaymentTypes>();
        permissions.add(Manifest.permission.READ_EXTERNAL_STORAGE);
        permissions.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        permissionsToRequest = findUnAskedPermissions(permissions);
        //get the permissions we have asked for before but are not granted..
        //we will store this in a global list to access later.

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if(permissionsToRequest.size() > 0)
                requestPermissions((String[]) permissionsToRequest.toArray(new String[permissionsToRequest.size()]), ALL_PERMISSIONS_RESULT);
        }
        productCartAdapter=new ProductCartAdapter(getContext(),list,Currency_value,viewCard);
        rv_cart_list.setAdapter(productCartAdapter);
        rv_tax_list.setLayoutManager(new GridLayoutManager(getContext(),1));
        taxItemAdapter = new TaxitemAdapter(getContext(),taxList, Currency_value);
        rv_tax_list.setAdapter(taxItemAdapter);

        try{
            queueList.getFirst_name();
            tvcustomername.setText(queueList.getFirst_name()+" "+queueList.getLast_name());
            tv_current_order.setText(queueList.getFirst_name()+" "+queueList.getLast_name());
            Glide.with(this).load(queueList.getPatient_image()).into(ivpatientcart);
            System.out.println("aaaaaaaa  queue_patient_id  "+queue_patient_id+"   "+queueList.getQueue_patient_id());
        }catch (NullPointerException ex){

        }


        img_exmlator.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String myJson = gson.toJson(logindata);
                Intent i=new Intent(getContext(), UserInfoDetails.class);
                if (!queue_patient_id.equals("0")){
                    i.putExtra("patient_id", queue_patient_id);
                }else {
                    i.putExtra("patient_id", queueList.getQueue_patient_id());
                }
                i.putExtra("queue_id", queue_id);
                i.putExtra("logindata", myJson);
                startActivity(i);
            }
        });
        tvGetPromoCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                System.out.println("aaaaaaaaaaaa   tvGetPromoCode  "+tvGetPromoCode.getText().toString());
                if (tvGetPromoCode.getText().toString().equalsIgnoreCase("Remove")){
                    removePromocart();
                }else {
                    getpromocodes();
                }
            }
        });
        ll_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showdialogback();
            }
        });
        ll_send_order.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentTransaction ft = getFragmentManager().beginTransaction();
                Fragment prev = getFragmentManager().findFragmentByTag("dialog");
                if (prev != null) {
                    ft.remove(prev);
                }
                ft.addToBackStack(null);
                AddPaymentDialog dialogFragment = new AddPaymentDialog(getActivity(),viewCard);
                dialogFragment.show(ft,"dialog");
                //dialogFragment.showDialog(getActivity(),viewCard.getPayment_types());
            }
        });
        tv_applyremove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AddProductDialog addProductDialog=new AddProductDialog();
                if (viewCard.getManager_discount() != null && viewCard.getManager_discount() != "") {
                    addProductDialog.showDialog(getActivity(),1);
                }else {
                    addProductDialog.showDialog(getActivity(),0);
                }
            }
        });
        return  view;
    }

    private ArrayList<String> findUnAskedPermissions(ArrayList<String> wanted) {
        ArrayList<String> result = new ArrayList<String>();

        for (String perm : wanted) {
            if (!hasPermission(perm)) {
                result.add(perm);
            }
        }

        return result;
    }
    private boolean hasPermission(String permission) {
        if (canMakeSmores()) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                return (getContext().checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED);
            }
        }
        return true;
    }
    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(getContext())
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();
    }

    private boolean canMakeSmores() {
        return (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1);
    }
    @TargetApi(Build.VERSION_CODES.M)
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        switch (requestCode) {

            case ALL_PERMISSIONS_RESULT:
                for (Object perms : permissionsToRequest) {
                    if (hasPermission((String) perms)) {

                    } else {

                        permissionsRejected.add(perms);
                    }
                }

                if (permissionsRejected.size() > 0) {


                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (shouldShowRequestPermissionRationale((String) permissionsRejected.get(0))) {
                            showMessageOKCancel("These permissions are mandatory for the application. Please allow access.",
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

                                                //Log.d("API123", "permisionrejected " + permissionsRejected.size());

                                                requestPermissions((String[]) permissionsRejected.toArray(new String[permissionsRejected.size()]), ALL_PERMISSIONS_RESULT);
                                            }
                                        }
                                    });
                            return;
                        }
                    }

                }

                break;
        }

    }

    public void getTAbsProducts(){
        if (CUC.Companion.isNetworkAvailablewithPopup((getActivity()))) {
            if (mSwipeRefreshLayout != null) {
                mSwipeRefreshLayout.setRefreshing(false);
            }
            mDialog = CUC.Companion.createDialog(getActivity());
            RequetsInterface apiService = RequetsInterface.Factory.create();
            Map parameterMap =new HashMap<String, String>();
            parameterMap.put("api_key", String.valueOf(Preferences.INSTANCE.getPreference((getActivity()), "API_KEY")));

            if (logindata != null) {
                parameterMap.put("store_id", logindata.getUser_store_id());
            }

            if (!queue_patient_id.equals("0")){
                parameterMap.put("patient_id", queue_patient_id);
            }else {
                parameterMap.put("patient_id", queueList.getQueue_patient_id());
            }

            Log.i("aaaaaaaa   Result", "parameterMap : " + parameterMap);
            (new CompositeDisposable()).add(apiService.callEmployeeSalesProducts(parameterMap)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe((Consumer)(new Consumer() {
                // $FF: synthetic method
                // $FF: bridge method
                public void accept(Object var1) {
                    this.accept((GetEmployeeSalesProducts)var1);
                }

                public final void accept(GetEmployeeSalesProducts result) {
                    mDialog.cancel();
                    if (result != null) {

                        System.out.println("aaaaaaaaa  result  "+result.toString());
                        Log.i("aaaaaaa   Result", "" + result.toString());
                        if (Intrinsics.areEqual(result.getStatus(), "0")) {
                            getcallEmployeePatientCart();

                            if (result.getPro_cat().getData().size() > 0) {
                                image_path=result.getImage_path();
                                cateList.clear();
                                cateList = result.getPro_cat().getData();
                                Collections.reverse(cateList);
                                populateViewPager(cateList,image_path);
                            }
                            CUC.Companion.displayToast(getActivity(), result.getShow_status(), result.getMessage());
                        } else if (Intrinsics.areEqual(result.getStatus(), "10")) {

                        } else {
                            Toast.makeText(getActivity(), ""+result.getShow_status(), Toast.LENGTH_SHORT).show();
                            CUC.Companion.displayToast((Context)getActivity(), result.getShow_status(), result.getMessage());
                        }
                    } else {
                    }
                }
            }), (Consumer)(new Consumer() {
                // $FF: synthetic method
                // $FF: bridge method
                public void accept(Object var1) {
                    this.accept((Throwable)var1);
                }

                public final void accept(Throwable error) {
                    mDialog.cancel();
                    Toast.makeText(getActivity(), getString(R.string.connection_to_database_failed), Toast.LENGTH_SHORT).show();
                    error.printStackTrace();
                    System.out.println("aaaaaaaa  error "+error.getMessage());
                }
            })));

        }else {
            System.out.println("aaaaaaaaaa   else ");
        }
    }

    private void populateViewPager(final List<GetData> cateList,String image_path) {

        ViewPagerAdapter adapter = new ViewPagerAdapter(getFragmentManager(),cateList,logindata,queue_patient_id,queue_id,list,image_path,queueList);
        mViewPager.setAdapter(adapter);

        //Make tabs scrollable
        tab_layout.setTabMode(TabLayout.MODE_SCROLLABLE);
        tab_layout.setupWithViewPager(mViewPager);
    }
    public void getapiCallForEmployeeDocument(){
        if (CUC.Companion.isNetworkAvailablewithPopup((getContext()))) {
            // mDialog = CUC.Companion.createDialog(getActivity());
            RequetsInterface apiService = RequetsInterface.Factory.create();
            Map parameterMap =new HashMap<String, String>();
            parameterMap.put("api_key", String.valueOf(Preferences.INSTANCE.getPreference((getContext()), "API_KEY")));

            if (logindata != null) {
                parameterMap.put("employee_id", logindata.getUser_store_id());
                //parameterMap.put("patient_id", queue_patient_id);
                parameterMap.put("store_id", logindata.getUser_store_id());
            }

            if (queue_patient_id.equals("0")){
                parameterMap.put("patient_id", queueList.getQueue_patient_id());

            }else {
                parameterMap.put("patient_id", queue_patient_id);
            }
            Log.i("aaaaaaaa   Result", "parameterMap : " + parameterMap);
            (new CompositeDisposable()).add(apiService.callEmployeePatientInfo(parameterMap).observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io()).subscribe((Consumer)(new Consumer() {
                // $FF: synthetic method
                // $FF: bridge method
                public void accept(Object var1) {
                    this.accept((EmployeePatientInfo)var1);
                }

                public final void accept(EmployeePatientInfo result) {
                    // mDialog.cancel();
                    if (result != null) {
                        System.out.println("aaaaaaaaa  result  "+result.toString());
                        Log.i("aaaaaaa   Result", "" + result.toString());
                        if (Intrinsics.areEqual(result.getStatus(), "0")) {
                            if (result.getStatus().equals("0")) {
                                textLimitUsage.setText(""+result.getLimit());
                                // tv_limit_value.setText(""+result.getSpent_limit());
                                noOfAddedItem = Float.parseFloat(result.getSpent_limit());
                                String str = result.getLimit();
                                try{
                                    notsdata=result.getNotes_data();
                                    for (int i=0;i<notsdata.size();i++){
                                        shownotedialog(notsdata.get(i).getPnote_title(),notsdata.get(i).getPnote_notes());
                                    }
                                }catch (Exception e){

                                }

                                pieGraphLimite = Float.parseFloat(result.getLimit());
                                setChartData();
                            }
                            CUC.Companion.displayToast(getActivity(), result.getShow_status(), result.getMessage());
                        }  else {
                            Toast.makeText(getActivity(), ""+result.getShow_status(), Toast.LENGTH_SHORT).show();
                            CUC.Companion.displayToast((Context)getActivity(), result.getShow_status(), result.getMessage());
                        }
                    } else {
                    }
                }
            }), (Consumer)(new Consumer() {
                // $FF: synthetic method
                // $FF: bridge method
                public void accept(Object var1) {
                    this.accept((Throwable)var1);
                }

                public final void accept(Throwable error) {
                    //  mDialog.cancel();
                    Toast.makeText(getActivity(), getString(R.string.connection_to_database_failed), Toast.LENGTH_SHORT).show();
                    error.printStackTrace();
                    System.out.println("aaaaaaaa  error "+error.getMessage());
                }
            })));

        }else {
            System.out.println("aaaaaaaaaa   else ");
        }
    }

    public final void setChartData() {
        if (this.pieGraphLimite >= 0.0F && this.noOfAddedItem >= 0.0F) {
            textLimitUsage.setText(String.format("%.2f G", pieGraphLimite));
            tv_limit_value.setText(""+noOfAddedItem+"G/");

            Float usesPersntage = ((noOfAddedItem * 100) / pieGraphLimite);

        }

    }

    public void getcallEmployeePatientCart(){
        if (CUC.Companion.isNetworkAvailablewithPopup((getActivity()))) {
            if (mSwipeRefreshLayout != null) {
                mSwipeRefreshLayout.setRefreshing(false);
            }
            //  mDialog = CUC.Companion.createDialog(getActivity());
            RequetsInterface apiService = RequetsInterface.Factory.create();
            Map parameterMap =new HashMap<String, String>();
            parameterMap.put("api_key", String.valueOf(Preferences.INSTANCE.getPreference((getActivity()), "API_KEY")));

            if (logindata != null) {
                parameterMap.put("store_id", logindata.getUser_store_id());
                //   parameterMap.put("patient_id", queue_patient_id);
                parameterMap.put("queue_id", queue_id);
            }

            if (queue_patient_id.equals("0")){
                parameterMap.put("patient_id", queueList.getQueue_patient_id());
            }else {
                parameterMap.put("patient_id", queue_patient_id);
            }
            Log.i("aaaaaaaa   Result", "parameterMap : " + parameterMap);
            (new CompositeDisposable()).add(apiService.callEmployeeSalesViewcart(parameterMap).observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io()).subscribe((Consumer)(new Consumer() {
                // $FF: synthetic method
                // $FF: bridge method
                public void accept(Object var1) {
                    this.accept((GetEmployeePatientCart)var1);
                }

                public final void accept(GetEmployeePatientCart result) {
                    // mDialog.cancel();
                    if (result != null) {
                        System.out.println("aaaaaaaaa  result viewcart  "+result.toString());
                        Log.i("aaaaaaa   Result", "" + result.toString());
                        if (Intrinsics.areEqual(result.getStatus(), "0")) {
                            viewCard=result;
                            System.out.println("aaaaaaaaaa  viewCard "+viewCard.getMain_cart().getCart_id());
                            textLimitUsage.setText(""+result.getLimit());
                            // tv_limit_value.setText(""+result.getSpent_limit());
                            System.out.println("aaaaaaaaa status  "+result.getStatus());
                            if (result.getStatus().equals("0")){
                                // mainCartId = result.getMain_cart().getCart_id();
                                payment_path = result.getPayment_path();
                                Store_sign_required = result.getStore_sign_required();
                                paymentTypeList.clear();
                                paymentTypeList.addAll(result.getPayment_types());
                                list.clear();
                                list=result.getCart_data();
                                System.out.println("aaaaaaaaa   "+result.getCart_data());
                                System.out.println("aaaaaaaaa   "+list.size());

                                if (list.size()>0){
                                    rl_bottom.setVisibility(View.VISIBLE);
                                    rv_cart_list.setVisibility(View.VISIBLE);
                                    tv_cartisempty.setVisibility(View.GONE);
                                    productCartAdapter.dataChanged(list,viewCard);
                                    tv_grandtotal.setText(Currency_value+""+result.getTotal_payble());
                                    tv_subtotal.setText(Currency_value+""+result.getSubtotal());
                                    tv_total_qty.setText(result.getTotal_qty());
                                }else {
                                    productCartAdapter.dataChanged(list,viewCard);
                                    rv_cart_list.setVisibility(View.GONE);
                                    rl_bottom.setVisibility(View.GONE);
                                    tv_cartisempty.setVisibility(View.VISIBLE);
                                }
                                taxList.clear();
                                taxList.addAll(result.getTax_data());
                                System.out.println("aaaaaaaaaaaa  taxdata  "+result.getTax_data());
                                if (taxList.size()>=0){
                                    rv_tax_list.setVisibility(View.VISIBLE);
                                }else {
                                    rv_tax_list.setVisibility(View.GONE);
                                }

                                try {
                                    if (discount_value != null && discount_value != "") {
                                        if (Float.parseFloat(discount_value) > 0) {
                                            if (cart_manual_discount_type.equals("2")) {
                                                taxList.add(new GetTaxData("202", getResources().getString(R.string.lbl_discount), discount_value, "2"));
                                            } else {
                                                taxList.add(new GetTaxData("202",getResources().getString(R.string.lbl_discount) , discount_value, "1"));
                                            }
                                        }
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                tv_applyremove.setText("Add Discount");
                                try {
                                    if (result.getManager_discount() != null && result.getManager_discount() != "") {
                                        tv_applyremove.setText("Remove Discount");
                                        taxList.add(new GetTaxData("207", getResources().getString(R.string.lbl_discount), result.getManager_discount(), "2"));
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    tv_applyremove.setText("Add Discount");
                                }
                                if ((result.getPromocode_name() != "") || (result.getPromocode_name()!=null)) {
                                    if (result.getPromosales_amount().equals("0.00")){
                                        ll_promoTab.setVisibility(View.VISIBLE);
                                        tv_promocode.setText("");
                                        tvGetPromoCode.setText("Have A Promo Code?");
                                        tvGetPromoCode.setVisibility(View.VISIBLE);
                                    }else {
                                        ll_promoTab.setVisibility(View.VISIBLE);
                                        tv_promocode.setText(result.getPromocode_name());
                                        taxList.add(new GetTaxData("204", "Promo Code", result.getPromosales_amount(),""));
                                        tvGetPromoCode.setText("REMOVE");
                                        tvGetPromoCode.setVisibility(View.GONE);
                                    }

                                } else {
                                    ll_promoTab.setVisibility(View.VISIBLE);
                                    tv_promocode.setText("");
                                    tvGetPromoCode.setText("Have A Promo Code?");
                                }
                                total_payble = result.getTotal_payble();
                                taxList.add(new GetTaxData("201", "Grand Total", result.getTotal_payble(), "1"));
                                taxItemAdapter.dataChanged(taxList);


                               /*if (result.getCart_data() != null && result.getCart_data().size() > 0) {
                                   list.clear();
                                   patient_point = result.getPatient_point();
                                   discount_value = result.getMain_cart().getCart_manual_discount();
                                   cart_manual_discount_type = result.getMain_cart().getCart_manual_discount_type();
                                   String str = result.getLimit();
                                   noOfAddedItem = Float.parseFloat(result.getSpent_limit());
                                   setChartData();
                                   if (logindata != null) {
                                       if (logindata.getUser_role().equals("2")) {
                                           ll_redeem_option.setVisibility(View.VISIBLE);
                                           ll_send_order.setVisibility(View.VISIBLE);
                                           ll_cancel.setVisibility(View.VISIBLE);
                                           ll_promoTab.setVisibility(View.VISIBLE);
                                           //   rl_bottom.visibility = View.VISIBLE
                                           total_unit.setVisibility(View.VISIBLE);
                                       }else {
                                           ll_redeem_option.setVisibility(View.GONE);
                                           ll_send_order.setVisibility(View.GONE);
                                           ll_cancel.setVisibility(View.GONE);
                                           ll_promoTab.setVisibility(View.GONE);
                                           //   rl_bottom.visibility = View.VISIBLE
                                           total_unit.setVisibility(View.GONE);
                                       }
                                   }else {
                                       ll_redeem_option.setVisibility(View.GONE);
                                       ll_send_order.setVisibility(View.GONE);
                                       ll_cancel.setVisibility(View.GONE);
                                       ll_promoTab.setVisibility(View.GONE);
                                       //   rl_bottom.visibility = View.VISIBLE
                                       total_unit.setVisibility(View.GONE);
                                   }

                                   if (cart_manual_discount_type.equals("1") || cart_manual_discount_type.equals("2")) {
                                       tv_applyremove.setText("Remove Discount");
                                   } else {
                                       tv_applyremove.setText("Discount");
                                   }
                                   tv_cartisempty.setVisibility(View.GONE);
                                   tv_total_qty.setText(result.getTotal_qty());
                                   tv_subtotal.setText(Currency_value+""+result.getSubtotal());
                                   list.addAll(result.getCart_data());
                                   productCartAdapter.dataChanged(list);
                                   taxList.clear();
                                   taxList.addAll(result.getTax_data());

                                   try {
                                       if (result.getLoyalty_amount() != null && result.getLoyalty_amount() != "") {
                                           if (Float.parseFloat(result.getLoyalty_amount()) > 0) {
                                               taxList.add(new GetTaxData("989", "Loyalty Amount", result.getLoyalty_amount(),""));
                                           }
                                       }
                                   } catch (Exception e) {
                                       e.printStackTrace();
                                   }
                                   try {
                                       if (discount_value != null && discount_value != "") {
                                           if (Float.parseFloat(discount_value) > 0) {
                                               if (cart_manual_discount_type.equals("2")) {
                                                   taxList.add(new GetTaxData("202", getResources().getString(R.string.lbl_discount), discount_value, "2"));
                                               } else {
                                                   taxList.add(new GetTaxData("202",getResources().getString(R.string.lbl_discount) , discount_value, "1"));
                                               }
                                           }
                                       }
                                   } catch (Exception e) {
                                       e.printStackTrace();
                                   }

                                   if (result.getPromocode_name() != "") {
                                       ll_promoTab.setVisibility(View.VISIBLE);
                                       tv_promocode.setText(result.getPromocode_name());
                                       taxList.add(new GetTaxData("204", "Promo Code"+result.getPromocode_name(), result.getPromosales_amount(),""));
                                       tvGetPromoCode.setText("REMOVE");
                                   } else {
                                       ll_promoTab.setVisibility(View.VISIBLE);
                                       tv_promocode.setText("");
                                       tvGetPromoCode.setText("Have A Promo Code?");
                                   }
                                   total_payble = result.getTotal_payble();
                                   taxList.add(new GetTaxData("201", "Grand Total", result.getTotal_payble(), "1"));
                                   taxItemAdapter.dataChanged(taxList);

                               }
                               else {
                                   if (result.getPromocode_name().equals("")) {
                                       ll_promoTab.setVisibility(View.GONE);
                                       tv_promocode.setText("");
                                       tvGetPromoCode.setText("Have A Promo Code?");
                                   } else {
                                       ll_promoTab.setVisibility(View.GONE);
                                       tv_promocode.setText(""+result.getPromocode_name());
                                       tvGetPromoCode.setText("REMOVE");
                                   }
                                   tv_total_qty.setText("");
                                   tv_subtotal.setText("");
                                   tv_cartisempty.setVisibility(View.VISIBLE);
                                   total_unit.setVisibility(View.GONE);
                                   list.clear();
                                   productAdapter.notifyDataSetChanged();
                                   taxList.clear();
                                   taxItemAdapter.notifyDataSetChanged();
                                   //finish()
                               }*/
                                Preferences.INSTANCE.setPreference(getActivity(), "total_qty", result.getTotal_qty());
                            }else if (result.getStatus().equals("10")){

                            }else {

                            }
                            CUC.Companion.displayToast(getActivity(), result.getShow_status(), result.getMessage());
                        } else if (Intrinsics.areEqual(result.getStatus(), "10")) {

                        } else {
                            Toast.makeText(getActivity(), ""+result.getShow_status(), Toast.LENGTH_SHORT).show();
                            CUC.Companion.displayToast((Context)getActivity(), result.getShow_status(), result.getMessage());
                        }
                    } else {
                    }
                }
            }), (Consumer)(new Consumer() {
                // $FF: synthetic method
                // $FF: bridge method
                public void accept(Object var1) {
                    this.accept((Throwable)var1);
                }

                public final void accept(Throwable error) {
                    // mDialog.cancel();
                    Toast.makeText(getActivity(), getString(R.string.connection_to_database_failed), Toast.LENGTH_SHORT).show();
                    error.printStackTrace();
                    System.out.println("aaaaaaaa  error "+error.getMessage());
                }
            })));

        }else {
            System.out.println("aaaaaaaaaa   else ");
        }
    }

    public void onCart() {
        getcallEmployeePatientCart();
    }

    public void setUpdateCart(GetCartData getCartData) {
        getStores(getCartData);
    }

    public void getStores(final GetCartData getProducts){

        mDialog = CUC.Companion.createDialog(getActivity());
        RequetsInterface apiService = RequetsInterface.Factory.create();
        Map parameterMap =new HashMap<String, String>();
        parameterMap.put("api_key", String.valueOf(Preferences.INSTANCE.getPreference((getActivity()), "API_KEY")));

        if (logindata != null) {
            parameterMap.put("store_id", logindata.getUser_store_id());
            parameterMap.put("cart_detail_id", getProducts.getCart_detail_id());
            parameterMap.put("product_id", getProducts.getCart_detail_product());
            parameterMap.put("packet_id", getProducts.getCart_detail_packet_id());
        }

        System.out.println("aaaaaaaaaaa  parameterMap "+parameterMap);
        Log.i("aaaaaaaa   Result", "parameterMap : " + parameterMap);
        (new CompositeDisposable()).add(apiService.callEmployeeProductQty(parameterMap).
                observeOn(AndroidSchedulers.mainThread()).
                subscribeOn(Schedulers.io()).subscribe((Consumer)(new Consumer() {
            // $FF: synthetic method
            // $FF: bridge method
            public void accept(Object var1) {
                this.accept((GetStatus)var1);
            }

            public final void accept(GetStatus result) {
                mDialog.cancel();
                if (result != null) {
                    mDialog.cancel();
                    System.out.println("aaaaaaaaa  result update  "+result.toString());
                    Log.i("aaaaaaa   Result", "" + result.toString());
                    if (Intrinsics.areEqual(result.getStatus(), "0")) {
                      /* if (result.getInventory_qty().equals("0")){
                           Toast.makeText(getContext(), "Currently Products Not There", Toast.LENGTH_SHORT).show();
                           CUC.Companion.displayToast(getContext(), "Currently Products Not There", result.getMessage());
                       }else {*/
                        AddProductDialog addProductDialog=new AddProductDialog();
                        addProductDialog.showDialog(getActivity(),result,getProducts);
                        // }
                        CUC.Companion.displayToast(getActivity(), result.getShow_status(), result.getMessage());
                    } else if (Intrinsics.areEqual(result.getStatus(), "10")) {

                    } else {
                        Toast.makeText(getActivity(), ""+result.getShow_status(), Toast.LENGTH_SHORT).show();
                        CUC.Companion.displayToast((Context)getActivity(), result.getShow_status(), result.getMessage());
                    }
                } else {
                    mDialog.cancel();
                }
            }
        }), (Consumer)(new Consumer() {
            // $FF: synthetic method
            // $FF: bridge method
            public void accept(Object var1) {
                this.accept((Throwable)var1);
            }

            public final void accept(Throwable error) {
                mDialog.cancel();
                Toast.makeText(getActivity(), getString(R.string.connection_to_database_failed), Toast.LENGTH_SHORT).show();
                error.printStackTrace();
                System.out.println("aaaaaaaa  error "+error.getMessage());
            }
        })));
    }

    public void sendUpdatecart(final GetCartData getProducts, Float value,Float discount,int i){

        mDialog = CUC.Companion.createDialog(getActivity());
        RequetsInterface apiService = RequetsInterface.Factory.create();
        Map parameterMap =new HashMap<String, String>();
        parameterMap.put("api_key", String.valueOf(Preferences.INSTANCE.getPreference((getActivity()), "API_KEY")));

        if (logindata != null) {
            parameterMap.put("store_id", logindata.getUser_store_id());
            parameterMap.put("employee_id", logindata.getUser_id());
        }
        parameterMap.put("cart_detail_id",getProducts.getCart_detail_id());
        if (!queue_patient_id.equals("0")){
            parameterMap.put("patient_id", queue_patient_id);
        }else {
            parameterMap.put("patient_id", queueList.getQueue_patient_id());
        }
        if (i==1){
            parameterMap.put("percentage_discount", discount);
        }else if (i==2){
            parameterMap.put("fixed_discount", discount);
        }

        // parameterMap.put("patient_id",queue_patient_id);
        parameterMap.put("product_id", getProducts.getCart_detail_product());
        parameterMap.put("category_id", getProducts.getCart_detail_category_id());
        parameterMap.put("package_id", getProducts.getCart_detail_packet_id());
        parameterMap.put("qty", ""+value);
        parameterMap.put("terminal_id", mBAsis.getTerminal_id());
        parameterMap.put("emp_lat", Preferences.INSTANCE.getPreference(getActivity(), "latitude"));
        parameterMap.put("emp_log", Preferences.INSTANCE.getPreference(getActivity(), "longitude"));
        parameterMap.put("queue_id", queue_id);

        System.out.println("aaaaaaaaaaa  parameterMap update cart "+parameterMap);
        Log.i("aaaaaaaa   Result", "parameterMap : " + parameterMap);
        (new CompositeDisposable()).add(apiService.callEmployeeSalesUpdatecart(parameterMap).
                observeOn(AndroidSchedulers.mainThread()).
                subscribeOn(Schedulers.io()).subscribe((Consumer)(new Consumer() {
            // $FF: synthetic method
            // $FF: bridge method
            public void accept(Object var1) {
                this.accept((GetStatus)var1);
            }

            public final void accept(GetStatus result) {
                mDialog.cancel();
                if (result != null) {
                    mDialog.cancel();

                    System.out.println("aaaaaaaaa  result updatecart  "+result.toString());
                    Log.i("aaaaaaa   Result", "" + result.toString());
                    if (Intrinsics.areEqual(result.getStatus(), "0")) {
                        try{
                            if (result.getStatus().equals("0") && result.getGrade_status().equals("0")){
                                showdialog(result.getGrade_message());
                            }
                        }catch (NullPointerException e){

                        }try{
                            if (result.getStatus().equals("0") && result.getGrade_warning_status().equals("0")){
                                showdialog(result.getGrade_warning());
                            }
                        }catch (NullPointerException e){

                        }
                        System.out.println("aaaaaaaaaa  updatecart sucess");
                        ((SalesActivity)getActivity()).onCart();
                        CUC.Companion.displayToast(getActivity(), result.getShow_status(), result.getMessage());
                    } else if (Intrinsics.areEqual(result.getStatus(), "10")) {

                    } else {
                        Toast.makeText(getActivity(), ""+result.getShow_status(), Toast.LENGTH_SHORT).show();
                        CUC.Companion.displayToast((Context)getActivity(), result.getShow_status(), result.getMessage());
                    }
                } else {
                    mDialog.cancel();
                }
            }
        }), (Consumer)(new Consumer() {
            // $FF: synthetic method
            // $FF: bridge method
            public void accept(Object var1) {
                this.accept((Throwable)var1);
            }

            public final void accept(Throwable error) {
                mDialog.cancel();
                Toast.makeText(getActivity(), getString(R.string.connection_to_database_failed), Toast.LENGTH_SHORT).show();
                error.printStackTrace();
                System.out.println("aaaaaaaa  error update "+error.getMessage());
            }
        })));
    }

    public void cartDelete(final GetCartData getProducts){

        mDialog = CUC.Companion.createDialog(getActivity());
        RequetsInterface apiService = RequetsInterface.Factory.create();
        Map parameterMap =new HashMap<String, String>();
        parameterMap.put("api_key", String.valueOf(Preferences.INSTANCE.getPreference((getActivity()), "API_KEY")));

        if (logindata != null) {
            parameterMap.put("employee_id", logindata.getUser_id());
        }
        parameterMap.put("cart_detail_id",getProducts.getCart_detail_id());
        if (!queue_patient_id.equals("0")){
            parameterMap.put("patient_id", queue_patient_id);
        }else {
            parameterMap.put("patient_id", queueList.getQueue_patient_id());
        }
        // parameterMap.put("patient_id", queue_patient_id);
        parameterMap.put("terminal_id", mBAsis.getTerminal_id());
        parameterMap.put("emp_lat", Preferences.INSTANCE.getPreference(getActivity(), "latitude"));
        parameterMap.put("emp_log", Preferences.INSTANCE.getPreference(getActivity(), "longitude"));

        System.out.println("aaaaaaaaaaa  parameterMap delete cart "+parameterMap);
        Log.i("aaaaaaaa   Result", "parameterMap : " + parameterMap);
        (new CompositeDisposable()).add(apiService.callEmployeePatientRemovecart(parameterMap).
                observeOn(AndroidSchedulers.mainThread()).
                subscribeOn(Schedulers.io()).subscribe((Consumer)(new Consumer() {
            // $FF: synthetic method
            // $FF: bridge method
            public void accept(Object var1) {
                this.accept((GetStatus)var1);
            }

            public final void accept(GetStatus result) {
                mDialog.cancel();
                if (result != null) {
                    mDialog.cancel();

                    System.out.println("aaaaaaaaa  result deletecart  "+result.toString());
                    Log.i("aaaaaaa   Result", "" + result.toString());
                    if (Intrinsics.areEqual(result.getShow_status(), "0")) {
                        System.out.println("aaaaaaaaaa  deletecart sucess");
                        ((SalesActivity)getActivity()).onCart();
                        CUC.Companion.displayToast(getActivity(), result.getShow_status(), result.getMessage());
                    } else if (Intrinsics.areEqual(result.getStatus(), "10")) {

                    } else {
                        Toast.makeText(getActivity(), ""+result.getShow_status(), Toast.LENGTH_SHORT).show();
                        CUC.Companion.displayToast((Context)getActivity(), result.getShow_status(), result.getMessage());
                    }
                } else {
                    mDialog.cancel();
                }
            }
        }), (Consumer)(new Consumer() {
            // $FF: synthetic method
            // $FF: bridge method
            public void accept(Object var1) {
                this.accept((Throwable)var1);
            }

            public final void accept(Throwable error) {
                mDialog.cancel();
                Toast.makeText(getActivity(), getString(R.string.connection_to_database_failed), Toast.LENGTH_SHORT).show();
                error.printStackTrace();
                System.out.println("aaaaaaaa  error update "+error.getMessage());
            }
        })));
    }

    public void removePromocart(){

        mDialog = CUC.Companion.createDialog(getActivity());
        RequetsInterface apiService = RequetsInterface.Factory.create();
        Map parameterMap =new HashMap<String, String>();
        parameterMap.put("api_key", String.valueOf(Preferences.INSTANCE.getPreference((getActivity()), "API_KEY")));

        if (logindata != null) {
            parameterMap.put("employee_id", logindata.getUser_id());
            parameterMap.put("store_id", logindata.getUser_store_id());
        }
        if (mBAsis != null) {
            parameterMap.put("cart_detail_id",mBAsis.getTerminal_id());
        }

        parameterMap.put("emp_lat", Preferences.INSTANCE.getPreference(getActivity(), "latitude"));
        parameterMap.put("emp_log", Preferences.INSTANCE.getPreference(getActivity(), "longitude"));
        parameterMap.put("queue_id",queue_id);
        if (!queue_patient_id.equals("0")){
            parameterMap.put("patient_id", queue_patient_id);
        }else {
            parameterMap.put("patient_id", queueList.getQueue_patient_id());
        }
        System.out.println("aaaaaaaaaaa  parameterMap removepromo cart "+parameterMap);
        Log.i("aaaaaaaa   Result", "parameterMap : " + parameterMap);
        (new CompositeDisposable()).add(apiService.callEmployeeSalesRemovePromocode(parameterMap).
                observeOn(AndroidSchedulers.mainThread()).
                subscribeOn(Schedulers.io()).subscribe((Consumer)(new Consumer() {
            // $FF: synthetic method
            // $FF: bridge method
            public void accept(Object var1) {
                this.accept((GetStatus)var1);
            }

            public final void accept(GetStatus result) {
                mDialog.cancel();
                if (result != null) {
                    mDialog.cancel();
                    System.out.println("aaaaaaaaa  result removepromo  "+result.toString());
                    Log.i("aaaaaaa   Result", "" + result.toString());
                    if (Intrinsics.areEqual(result.getShow_status(), "0")) {
                        ((SalesActivity)getActivity()).onCart();
                        CUC.Companion.displayToast(getActivity(), result.getShow_status(), result.getMessage());
                    } else if (Intrinsics.areEqual(result.getStatus(), "10")) {

                    } else {
                        Toast.makeText(getActivity(), ""+result.getShow_status(), Toast.LENGTH_SHORT).show();
                        CUC.Companion.displayToast((Context)getActivity(), result.getShow_status(), result.getMessage());
                    }
                } else {
                    mDialog.cancel();
                }
            }
        }), (Consumer)(new Consumer() {
            // $FF: synthetic method
            // $FF: bridge method
            public void accept(Object var1) {
                this.accept((Throwable)var1);
            }

            public final void accept(Throwable error) {
                mDialog.cancel();
                Toast.makeText(getActivity(), getString(R.string.connection_to_database_failed), Toast.LENGTH_SHORT).show();
                error.printStackTrace();
                System.out.println("aaaaaaaa  error update "+error.getMessage());
            }
        })));
    }

    public void removecart(GetCartData getCartData, GetEmployeePatientCart viewCard){

        System.out.println("aaaaaaaaaaaaa  viewCard "+viewCard.getMain_cart().getCart_id());
        mDialog = CUC.Companion.createDialog(getActivity());
        RequetsInterface apiService = RequetsInterface.Factory.create();
        Map parameterMap =new HashMap<String, String>();
        parameterMap.put("api_key", String.valueOf(Preferences.INSTANCE.getPreference((getActivity()), "API_KEY")));
        parameterMap.put("cart_id",viewCard.getMain_cart().getCart_id());

        System.out.println("aaaaaaaaaaa  parameterMap removepcart "+parameterMap);
        Log.i("aaaaaaaa   Result", "parameterMap : " + parameterMap);
        (new CompositeDisposable()).add(apiService.callEmployeeSalesCancelOrder(parameterMap).
                observeOn(AndroidSchedulers.mainThread()).
                subscribeOn(Schedulers.io()).subscribe((Consumer)(new Consumer() {
            // $FF: synthetic method
            // $FF: bridge method
            public void accept(Object var1) {
                this.accept((GetEmployeeQenerateInvoice)var1);
            }

            public final void accept(GetEmployeeQenerateInvoice result) {
                mDialog.cancel();
                if (result != null) {
                    mDialog.cancel();
                    System.out.println("aaaaaaaaa  result removecart  "+result.toString());
                    Log.i("aaaaaaa   Result", "" + result.toString());
                    if (Intrinsics.areEqual(result.getShow_status(), "0")) {
                        rl_bottom.setVisibility(View.GONE);
                        rv_cart_list.setVisibility(View.GONE);
                        //((SalesActivity)getActivity()).onCart();
                        CUC.Companion.displayToast(getActivity(), result.getShow_status(), result.getMessage());
                    } else if (Intrinsics.areEqual(result.getStatus(), "10")) {

                    } else {
                        Toast.makeText(getActivity(), ""+result.getShow_status(), Toast.LENGTH_SHORT).show();
                        CUC.Companion.displayToast((Context)getActivity(), result.getShow_status(), result.getMessage());
                    }
                } else {
                    mDialog.cancel();
                }
            }
        }), (Consumer)(new Consumer() {
            // $FF: synthetic method
            // $FF: bridge method
            public void accept(Object var1) {
                this.accept((Throwable)var1);
            }

            public final void accept(Throwable error) {
                mDialog.cancel();
                Toast.makeText(getActivity(), getString(R.string.connection_to_database_failed), Toast.LENGTH_SHORT).show();
                error.printStackTrace();
                System.out.println("aaaaaaaa  error update "+error.getMessage());
            }
        })));
    }

    public void getpromocodes(){

        System.out.println("aaaaaaaaaaaaa  viewCard "+viewCard.getMain_cart().getCart_id());
        mDialog = CUC.Companion.createDialog(getActivity());
        RequetsInterface apiService = RequetsInterface.Factory.create();
        Map parameterMap =new HashMap<String, String>();
        parameterMap.put("api_key", String.valueOf(Preferences.INSTANCE.getPreference((getActivity()), "API_KEY")));
        parameterMap.put("cart_id",viewCard.getMain_cart().getCart_id());
        parameterMap.put("patient_id",viewCard.getMain_cart().getCart_patient_id());
        parameterMap.put("app_status","1");

        System.out.println("aaaaaaaaaaa  parameterMap removepcart "+parameterMap);
        Log.i("aaaaaaaa   Result", "parameterMap : " + parameterMap);
        (new CompositeDisposable()).add(apiService.callEmployeePatientPromocodeList(parameterMap).
                observeOn(AndroidSchedulers.mainThread()).
                subscribeOn(Schedulers.io()).subscribe((Consumer)(new Consumer() {
            // $FF: synthetic method
            // $FF: bridge method
            public void accept(Object var1) {
                this.accept((GetEmployeePatientPromocodeList)var1);
            }

            public final void accept(GetEmployeePatientPromocodeList result) {
                mDialog.cancel();
                if (result != null) {
                    mDialog.cancel();
                    System.out.println("aaaaaaaaa  result promocodelist  "+result.toString());
                    Log.i("aaaaaaa   Result", "" + result.toString());
                    if (result.getStatus().equalsIgnoreCase("0")){
                        CUC.Companion.displayToast((Context)getActivity(), result.getShow_status(), result.getMessage());
                    }else {
                        if (result.getShow_status().equals("0")){
                            CUC.Companion.displayToast((Context)getActivity(), result.getShow_status(), result.getMessage());
                        }else {
                            peomocedelist.addAll(result.getPromosales());
                            AddPromocardDialog promoCodeDialog=new AddPromocardDialog();
                            promoCodeDialog.showDialog(getActivity(),peomocedelist);
                            CUC.Companion.displayToast(getActivity(), result.getShow_status(), result.getMessage());
                        }

                    }
                    /*if (Intrinsics.areEqual(result.getShow_status(), "0")) {
                        peomocedelist.addAll(result.getPromosales());
                        AddPromocardDialog promoCodeDialog=new AddPromocardDialog();
                        promoCodeDialog.showDialog(getActivity(),peomocedelist);
                        CUC.Companion.displayToast(getActivity(), result.getShow_status(), result.getMessage());
                    } else if (Intrinsics.areEqual(result.getStatus(), "10")) {

                    } else {
                        Toast.makeText(getActivity(), ""+result.getShow_status(), Toast.LENGTH_SHORT).show();
                        CUC.Companion.displayToast((Context)getActivity(), result.getShow_status(), result.getMessage());
                    }*/
                } else {
                    mDialog.cancel();
                }
            }
        }), (Consumer)(new Consumer() {
            // $FF: synthetic method
            // $FF: bridge method
            public void accept(Object var1) {
                this.accept((Throwable)var1);
            }

            public final void accept(Throwable error) {
                mDialog.cancel();
                Toast.makeText(getActivity(), getString(R.string.connection_to_database_failed), Toast.LENGTH_SHORT).show();
                error.printStackTrace();
                System.out.println("aaaaaaaa  error update "+error.getMessage());
            }
        })));
    }

    public void showdialog(String msg){
        AlertDialog.Builder builder1 = new AlertDialog.Builder(getActivity());
        builder1.setMessage(""+msg);
        builder1.setCancelable(true);

        builder1.setPositiveButton(
                "Ok",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        AlertDialog alert11 = builder1.create();
        alert11.show();
    }
    public void shownotedialog(String title,String message){
        AlertDialog.Builder builder1 = new AlertDialog.Builder(getActivity());
        builder1.setTitle(title);
        builder1.setMessage(""+message);
        builder1.setCancelable(true);

        builder1.setPositiveButton(
                "Ok",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        AlertDialog alert11 = builder1.create();
        alert11.show();
    }

    public void showdialogask(final GetCartData getProducts){
        AlertDialog.Builder builder1 = new AlertDialog.Builder(getActivity());
        builder1.setMessage("Are You Sure You Want To Delete?");
        builder1.setCancelable(true);

        builder1.setPositiveButton(
                "Yes",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        cartDelete(getProducts);
                        dialog.cancel();
                    }
                });

        builder1.setNegativeButton(
                "No",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        AlertDialog alert11 = builder1.create();
        alert11.show();
    }
    public void showdialogback(){
        AlertDialog.Builder builder1 = new AlertDialog.Builder(getActivity());
        builder1.setMessage("Are You Want to leave Sales POS?");
        builder1.setTitle("Confitm");
        builder1.setCancelable(true);

        builder1.setPositiveButton(
                "Yes",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        getActivity().onBackPressed();
                        dialog.cancel();
                    }
                });

        builder1.setNegativeButton(
                "No",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        AlertDialog alert11 = builder1.create();
        alert11.show();
    }

    public void setPromocde() {


    }

    public void setapplyPromocard(GetPromosales getPromosales) {

    }

    public void setDiscount(String entertxt, int k, String pin){

        System.out.println("aaaaaaaaaaaaa  viewCard "+viewCard.getMain_cart().getCart_id());
        mDialog = CUC.Companion.createDialog(getActivity());
        RequetsInterface apiService = RequetsInterface.Factory.create();
        Map parameterMap =new HashMap<String, String>();
        parameterMap.put("api_key", String.valueOf(Preferences.INSTANCE.getPreference((getActivity()), "API_KEY")));
        if (logindata != null) {
            parameterMap.put("employee_id", logindata.getUser_id());
            parameterMap.put("store_id", logindata.getUser_store_id());
        }
        if (mBAsis != null) {
            parameterMap.put("terminal_id",mBAsis.getTerminal_id());
        }
        parameterMap.put("patient_id",queue_patient_id);
        parameterMap.put("queue_id",queue_id);
        parameterMap.put("discount_type",(k+1));
        parameterMap.put("discount_value",entertxt);
        parameterMap.put("employee_pin",pin);
        parameterMap.put("emp_lat", Preferences.INSTANCE.getPreference(getActivity(), "latitude"));
        parameterMap.put("emp_log", Preferences.INSTANCE.getPreference(getActivity(), "longitude"));

        System.out.println("aaaaaaaaaaa  parameterMap adddiscount "+parameterMap);
        Log.i("aaaaaaaa   Result", "parameterMap : " + parameterMap);
        (new CompositeDisposable()).add(apiService.callEmployeeSalesApplyStmanDiscount(parameterMap).
                observeOn(AndroidSchedulers.mainThread()).
                subscribeOn(Schedulers.io()).subscribe((Consumer)(new Consumer() {
            // $FF: synthetic method
            // $FF: bridge method
            public void accept(Object var1) {
                this.accept((GetStatus)var1);
            }

            public final void accept(GetStatus result) {
                mDialog.cancel();
                if (result != null) {
                    mDialog.cancel();
                    System.out.println("aaaaaaaaa  result adddiscount  "+result.toString());
                    Log.i("aaaaaaa   Result", "" + result.toString());
                    if (result.getStatus().equalsIgnoreCase("0")){
                        tv_applyremove.setText("Remove Discount");
                        getcallEmployeePatientCart();
                        CUC.Companion.displayToast((Context)getActivity(), result.getShow_status(), result.getMessage());
                    }else {
                        if (result.getShow_status().equals("0")){
                            tv_applyremove.setText("Remove Discount");
                            getcallEmployeePatientCart();
                            CUC.Companion.displayToast((Context)getActivity(), result.getShow_status(), result.getMessage());
                        }else {

                            CUC.Companion.displayToast(getActivity(), result.getShow_status(), result.getMessage());
                        }

                    }
                } else {
                    mDialog.cancel();
                }
            }
        }), (Consumer)(new Consumer() {
            // $FF: synthetic method
            // $FF: bridge method
            public void accept(Object var1) {
                this.accept((Throwable)var1);
            }

            public final void accept(Throwable error) {
                mDialog.cancel();
                Toast.makeText(getActivity(), getString(R.string.connection_to_database_failed), Toast.LENGTH_SHORT).show();
                error.printStackTrace();
                System.out.println("aaaaaaaa  error update "+error.getMessage());
            }
        })));
    }


    public void setRemovediscount(String pin){

        System.out.println("aaaaaaaaaaaaa  viewCard "+viewCard.getMain_cart().getCart_id());
        mDialog = CUC.Companion.createDialog(getActivity());
        RequetsInterface apiService = RequetsInterface.Factory.create();
        Map parameterMap =new HashMap<String, String>();
        parameterMap.put("api_key", String.valueOf(Preferences.INSTANCE.getPreference((getActivity()), "API_KEY")));
        if (logindata != null) {
            parameterMap.put("employee_id", logindata.getUser_id());
            parameterMap.put("store_id", logindata.getUser_store_id());
        }
        if (mBAsis != null) {
            parameterMap.put("terminal_id",mBAsis.getTerminal_id());
        }
        parameterMap.put("patient_id",queue_patient_id);
        parameterMap.put("queue_id",queue_id);
        parameterMap.put("employee_pin",pin);
        parameterMap.put("emp_lat", Preferences.INSTANCE.getPreference(getActivity(), "latitude"));
        parameterMap.put("emp_log", Preferences.INSTANCE.getPreference(getActivity(), "longitude"));

        System.out.println("aaaaaaaaaaa  parameterMap remove discount "+parameterMap);
        Log.i("aaaaaaaa   Result", "parameterMap : " + parameterMap);
        (new CompositeDisposable()).add(apiService.callEmployeeSalesRemoveStmanDiscount(parameterMap).
                observeOn(AndroidSchedulers.mainThread()).
                subscribeOn(Schedulers.io()).subscribe((Consumer)(new Consumer() {
            // $FF: synthetic method
            // $FF: bridge method
            public void accept(Object var1) {
                this.accept((GetStatus)var1);
            }

            public final void accept(GetStatus result) {
                mDialog.cancel();
                if (result != null) {
                    mDialog.cancel();
                    System.out.println("aaaaaaaaa  result remove discount  "+result.toString());
                    Log.i("aaaaaaa   Result", "" + result.toString());
                    if (result.getStatus().equalsIgnoreCase("0")){
                        tv_applyremove.setText("Add Discount");
                        getcallEmployeePatientCart();
                        CUC.Companion.displayToast((Context)getActivity(), result.getShow_status(), result.getMessage());
                    }else {
                        if (result.getShow_status().equals("0")){
                            tv_applyremove.setText("Add Discount");
                            getcallEmployeePatientCart();
                            CUC.Companion.displayToast((Context)getActivity(), result.getShow_status(), result.getMessage());
                        }else {

                            CUC.Companion.displayToast(getActivity(), result.getShow_status(), result.getMessage());
                        }

                    }
                } else {
                    mDialog.cancel();
                }
            }
        }), (Consumer)(new Consumer() {
            // $FF: synthetic method
            // $FF: bridge method
            public void accept(Object var1) {
                this.accept((Throwable)var1);
            }

            public final void accept(Throwable error) {
                mDialog.cancel();
                Toast.makeText(getActivity(), getString(R.string.connection_to_database_failed), Toast.LENGTH_SHORT).show();
                error.printStackTrace();
                System.out.println("aaaaaaaa  error update "+error.getMessage());
            }
        })));
    }

    public void sendPayment(Float enteramt,int position) {
        this.tabposition=position;
        paymentamt=enteramt;
        String payment =viewCard.getPayment_types().get(tabposition).getPayment_detail();
        System.out.println("aaaaaaaaaa  payment   "+position+"   "+payment+"   enteramt  "+enteramt);
        Intent siantureIntent = new Intent(getActivity(), SignatureActivity.class);
        startActivityForResult(siantureIntent, 8989);

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK){
            if (data != null && data.hasExtra("siganture_path")) {
                siganture_path = data.getStringExtra("siganture_path");
                if (siganture_path != null ) {
                    System.out.println("aaaaaaaaa   siganture_path   "+siganture_path);
                    callEmployeePatientConfirmOrder();
                }

            }
        }
    }

    public void callEmployeePatientConfirmOrder(){

        String payment =viewCard.getPayment_types().get(tabposition).getPayment_id();
        System.out.println("aaaaaaaaaaaaaaa  payment "+tabposition);

        System.out.println("aaaaaaaaaaaaa  viewCard "+viewCard.getMain_cart().getCart_id());
        mDialog = CUC.Companion.createDialog(getActivity());
        RequetsInterface apiService = RequetsInterface.Factory.create();
        Map parameterMap =new HashMap<String, String>();
        parameterMap.put("api_key", RequestBody.create(MediaType.parse("text/plain"), String.valueOf(Preferences.INSTANCE.getPreference((getActivity()), "API_KEY"))));
        if (logindata != null) {
            parameterMap.put("employee_id", RequestBody.create(MediaType.parse("text/plain"), String.valueOf(logindata.getUser_id())));
        }
        if (mBAsis != null) {
            parameterMap.put("terminal_id",RequestBody.create(MediaType.parse("text/plain"), String.valueOf(mBAsis.getTerminal_id())));
        }
        parameterMap.put("discount_price",RequestBody.create(MediaType.parse("text/plain"), String.valueOf(viewCard.getManager_discount())));
        parameterMap.put("patient_id",RequestBody.create(MediaType.parse("text/plain"), String.valueOf(queue_patient_id)));
        parameterMap.put("cart_id",RequestBody.create(MediaType.parse("text/plain"), String.valueOf(this.viewCard.getMain_cart().getCart_id())));
        parameterMap.put("payment_id",RequestBody.create(MediaType.parse("text/plain"), String.valueOf(this.viewCard.getPayment_types().get(tabposition).getPayment_id())));
        parameterMap.put("queue_id",RequestBody.create(MediaType.parse("text/plain"), String.valueOf(queue_id)));
        Float changeamt=paymentamt-Float.parseFloat(viewCard.getTotal_payble());
        parameterMap.put("payment_amount",RequestBody.create(MediaType.parse("text/plain"), String.valueOf(paymentamt)));
        parameterMap.put("change_amount",RequestBody.create(MediaType.parse("text/plain"), String.valueOf(changeamt)));
        parameterMap.put("emp_lat", RequestBody.create(MediaType.parse("text/plain"), String.valueOf(Preferences.INSTANCE.getPreference(getActivity(), "latitude"))));
        parameterMap.put("emp_log", RequestBody.create(MediaType.parse("text/plain"), String.valueOf(Preferences.INSTANCE.getPreference(getActivity(), "longitude"))));
        // Observable disposable;
        File sigantureFile = new File(this.siganture_path);
        //RequestBody reqFile = RequestBody.create(MediaType.parse("image/jpeg"), sigantureFile);


        MultipartBody.Part var8 = MultipartBody.Part.createFormData("digital_siganture", String.valueOf(sigantureFile.getName()),
                RequestBody.create(MediaType.parse("image*//*"), sigantureFile));
        Intrinsics.checkExpressionValueIsNotNull(var8, "MultipartBody.Part.creat…ge*//*\"), sigantureFile))");
        RequestBody var14 = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(sigantureFile.getName()));
        Intrinsics.checkExpressionValueIsNotNull(var14, "RequestBody.create(Media… \"${sigantureFile.name}\")");
        Observable disposable = apiService.callEmployeeSalesInvoice(parameterMap, var8, var14);


        System.out.println("aaaaaaaaaaa  parameterMap remove discount "+disposable.toString()+"   parameters   "+parameterMap );
        Log.i("aaaaaaaa   Result", "parameterMap : " + disposable);
        (new CompositeDisposable()).add(disposable.
                observeOn(AndroidSchedulers.mainThread()).
                subscribeOn(Schedulers.io()).subscribe((Consumer)(new Consumer() {
            // $FF: synthetic method
            // $FF: bridge method
            public void accept(Object var1) {
                this.accept((GetEmployeeQenerateInvoice)var1);
            }

            public final void accept(GetEmployeeQenerateInvoice result) {
                mDialog.cancel();
                if (result != null) {
                    mDialog.cancel();
                    System.out.println("aaaaaaaaa  result orderconfirm  "+result.toString());
                    Log.i("aaaaaaa   Result", "" + result.toString());
                    if (result.getStatus().equalsIgnoreCase("0")){
                        String invoiceStr = (new Gson()).toJson(result);
                        Intent myIntent = new Intent(getActivity(), SalesReceiptPrintActivity.class);
                        //myIntent.putExtra("all_data", "$all_data")
                        myIntent.putExtra("invoiceStr", invoiceStr);
                        startActivityForResult(myIntent, request_code);
                        getActivity().onBackPressed();
                        CUC.Companion.displayToast((Context)getActivity(), result.getShow_status(), result.getMessage());
                    }else {
                        if (result.getShow_status().equals("0")){
                            String invoiceStr = new Gson().toJson(result);
                            Intent myIntent = new Intent(getActivity(), SalesReceiptPrintActivity.class);
                            //myIntent.putExtra("all_data", "$all_data")
                            myIntent.putExtra("invoiceStr", invoiceStr);
                            startActivityForResult(myIntent, request_code);
                           getActivity().onBackPressed();
                            CUC.Companion.displayToast((Context)getActivity(), result.getShow_status(), result.getMessage());
                        }else {

                            CUC.Companion.displayToast(getActivity(), result.getShow_status(), result.getMessage());
                        }

                    }
                } else {
                    mDialog.cancel();
                }
            }
        }), (Consumer)(new Consumer() {
            // $FF: synthetic method
            // $FF: bridge method
            public void accept(Object var1) {
                this.accept((Throwable)var1);
            }

            public final void accept(Throwable error) {
                mDialog.cancel();
                Toast.makeText(getActivity(), getString(R.string.connection_to_database_failed), Toast.LENGTH_SHORT).show();
                error.printStackTrace();
                System.out.println("aaaaaaaa  error update "+error.getMessage());
            }
        })));
    }

}
