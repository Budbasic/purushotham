package com.budbasic.posconsole.reception.fragment

import android.Manifest
import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.os.StrictMode
import android.provider.MediaStore
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v4.content.FileProvider
import android.support.v7.app.AlertDialog
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.view.inputmethod.EditorInfo
import android.widget.*
import com.budbasic.customer.global.Preferences
import com.budbasic.global.CUC
import com.budbasic.global.CUC.Companion.decodeFile
import com.budbasic.posconsole.R
import com.budbasic.posconsole.dialogs.AddDocumentDailog
import com.budbasic.posconsole.dialogs.AddDocumentNotesDialog
import com.budbasic.posconsole.global.BaseFragment
import com.budbasic.posconsole.model.APIClass
import com.budbasic.posconsole.reception.NewPatientActivity
import com.budbasic.posconsole.reception.PatientEdit
import com.budbasic.posconsole.webservice.RequetsInterface
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.SimpleTarget
import com.google.gson.Gson
import com.kotlindemo.model.GetEmployeeAppSetting
import com.kotlindemo.model.GetEmployeeLogin
import com.kotlindemo.model.GetNotes
import com.kotlindemo.model.Patient_data
import com.squareup.picasso.Picasso
import com.tbruyelle.rxpermissions2.RxPermissions
import io.card.payment.CardIOActivity
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.customer_search_fragment.view.*
import kotlinx.android.synthetic.main.customer_search_fragment1.*
import java.io.File
import java.io.FileOutputStream
import java.io.OutputStream
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.HashMap
import kotlin.collections.set


class CustomerSearchFragment : BaseFragment(), APIClass.onCallListner {
    override fun setUp(view: View?) {
    }

    override fun onFailed(requestCode: Int) {
    }

    override fun onSuccess(requestCode: Int) {
        if (requestCode == 1) {
            list.clear()
            callEmployeeSearchPatient()
        }
    }

    lateinit var root: View
    var paging = 0
    var isLoading = false
    var isLastPage = false
    lateinit var productAdapter: ProductAdapter

    lateinit var mDialog: Dialog
    var apiClass: APIClass? = null
    var logindata: GetEmployeeLogin? = null
    var appSettingData: GetEmployeeAppSetting? = null
    var patientImagePath: String? = null
    private val imageCompanyFile: File? = null
    private var uri: Uri? = null
    var rxPermissions: RxPermissions? = null
    internal var bitmap: Bitmap? = null

    companion object {
        var list: ArrayList<Patient_data> = ArrayList()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        root = inflater.inflate(R.layout.customer_search_fragment1, container, false)

        apiClass = APIClass(context!!, this)
        return root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        init()
    }

    private fun init() {
        initList()
        root.btn_back.setOnClickListener {
            activity!!.onBackPressed()
        }
        root.ll_search_main.setOnClickListener {

        }
        val layout_manager = LinearLayoutManager(context)
        root.rv_customer_list!!.layoutManager = layout_manager
        edt_employee_search_patient.setImeOptions(EditorInfo.IME_FLAG_NO_EXTRACT_UI);
        productAdapter = ProductAdapter(this!!.context!!, object : onClickListener {
            override fun callClick() {

            }

        })
        root.rv_customer_list!!.adapter = productAdapter

        root.rv_customer_list!!.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView?, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)

                val lastvisibleitemposition = layout_manager.findLastVisibleItemPosition()

                if (lastvisibleitemposition == productAdapter.itemCount - 1) {
                    if (!isLoading && !isLastPage) {
                        isLoading = true

                        callEmployeeSearchPatient()
                        // Increment the pagecount everytime we scroll to fetch data from the next page
                        // make loading = false once the data is loaded
                        // call mAdapter.notifyDataSetChanged() to refresh the Adapter and Layout
                    }
                }
            }
        })


        appSettingData = Gson().fromJson(Preferences.getPreference(context!!, "AppSetting"), GetEmployeeAppSetting::class.java)
        patientImagePath = appSettingData?.patient_image_path

        logindata = Gson().fromJson(Preferences.getPreference(context!!, "logindata"), GetEmployeeLogin::class.java)


        root.cv_search.setOnClickListener {
            paging = 0
            isLoading = false
            isLastPage = false
            list.clear()
            callEmployeeSearchPatient()
//            if (root.edt_employee_search_patient.text.toString().trim() == "") {
//                CUC.displayToast(context!!, "0", "Enter customer")
//            } else {
//
//            }
        }
        root.cv_create_patient.setOnClickListener {
            val intent = Intent(context!!, NewPatientActivity::class.java)
            startActivityForResult(intent, 122)
        }
        paging = 0
        list.clear()
        callEmployeeSearchPatient()
    }

    override fun onStart() {
        super.onStart()

        //paging = 0
    }

    override fun onStop() {
        super.onStop()
    }

    private fun initList() {
    }


    fun callEmployeeSearchPatient() {
        try {
            if (!activity!!.isFinishing) {
                mDialog = CUC.createDialog(activity!!)
                val apiService = RequetsInterface.Factory.create()
                val parameterMap = HashMap<String, String>()
                parameterMap["api_key"] = "${Preferences.getPreference(context!!, "API_KEY")}"
                parameterMap["search_patient"] = "${root.edt_employee_search_patient.text}"
                parameterMap["paging"] = "$paging"
                if (logindata != null) {
                    parameterMap["store_id"] = "${logindata!!.user_store_id}"
                }
                println("aaaaaaaaaaaa  search patient paramerers "+parameterMap.toString())
                Log.i("Result", "callEmployeeSearchPatient() $parameterMap")
                CompositeDisposable().add(apiService.callGetEmployeeSearchPatient(parameterMap)
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.io())
                        .subscribe({ result ->
                            if (mDialog != null && mDialog.isShowing)
                                mDialog.dismiss()
                            if (result != null) {
                                println("aaaaaaaaaaaa  search patient "+result.toString())
                                Log.i("Result", "" + result.toString())
                                if (result.status == "0") {
                                    var tempList: ArrayList<Patient_data> = result.patient_data
                                    if (tempList.size > 0) {
                                        //rl_empty_view.visibility = View.GONE
                                        isLoading = tempList.size < result.offset.toInt()
                                        paging += result.offset.toInt()
                                        Log.i(CustomerSearchFragment::class.java.simpleName, "$paging")
                                        if (list.size > 0) {
                                            list.addAll(tempList)
                                            productAdapter.notifyDataSetChanged()
                                        } else {
                                            list.addAll(tempList)
                                            productAdapter.notifyDataSetChanged()
                                        }
                                    } else {
                                        isLoading = false
                                        if (list.size > 0) {

                                        } /*else {
                                            // rl_empty_view.visibility = View.VISIBLE
                                            list.clear()
                                            list.addAll(list)
                                            productAdapter.notifyDataSetChanged()
                                        }*/
                                    }
                                    CUC.displayToast(context!!, result.show_status, result.message)
                                } else if (result.status == "10") {
                                    apiClass!!.callApiKey(1)
                                } else {
                                    CUC.displayToast(context!!, result.show_status, result.message)
                                    if (list.size > 0) {

                                    } else {
                                        //rl_empty_view.visibility = View.VISIBLE
                                        list.clear()
                                        list.addAll(list)
                                        productAdapter.notifyDataSetChanged()
                                    }
                                }

                            } else {
                                //  CUC.displayToast(context!!, "0", getString(R.string.connection_to_database_failed))
                                if (mDialog != null && mDialog.isShowing)
                                    mDialog.dismiss()
                            }

                        }, { error ->
                            if (mDialog != null && mDialog.isShowing)
                                mDialog.dismiss()
                            CUC.displayToast(context!!, "0", getString(R.string.connection_to_database_failed))
                            error.printStackTrace()
                        })
                )
            }
        } catch (e: Exception) {
            e.printStackTrace()
            if (mDialog != null && mDialog.isShowing)
                mDialog.dismiss()
        }
    }


    fun callBannnedPaitite(isBanned: Boolean, patient_id: String, onChange: CompoundButton, position: Int) {
        try {
            if (!activity!!.isFinishing) {
                mDialog = CUC.createDialog(activity!!)
                val apiService = RequetsInterface.Factory.create()
                val parameterMap = HashMap<String, String>()


                parameterMap["api_key"] = "${Preferences.getPreference(context!!, "API_KEY")}"
                parameterMap["employee_id"] = logindata?.user_id!!
                parameterMap["patient_id"] = patient_id
                parameterMap["patient_status"] = if (isBanned) {
                    "1"
                } else {
                    "0"
                }
                CompositeDisposable().add(apiService.callEmployeeBannedPatient(parameterMap)
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.io())
                        .subscribe({ result ->
                            if (mDialog != null && mDialog.isShowing)
                                mDialog.dismiss()
                            if (result != null && result!!.status.equals("0")) {
                                if (isBanned) {
                                    list[position].patient_status = "3"
                                } else {
                                    list[position].patient_status = "1"
                                }
                            }

                        }, { error ->
                            if (mDialog != null && mDialog.isShowing)
                                mDialog.dismiss()
                            //onChange.isSelected = (!isBanned)
                            CUC.displayToast(context!!, "0", getString(R.string.connection_to_database_failed))
                            error.printStackTrace()
                        })
                )
            }
        } catch (e: Exception) {
            if (mDialog != null && mDialog.isShowing)
                mDialog.dismiss()
            e.printStackTrace()
        }
    }



    inner class ProductAdapter(val context: Context, val onClick: onClickListener) : RecyclerView.Adapter<ProductAdapter.ViewHolder>() {

        val viewEditString = "View/Edit"
        val addString = "ADD"

        override fun getItemCount(): Int {
            return list.size
        }

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {


            var data = list[position]

            holder.tv_name.text = CUC.getCapsSentences("${data.patient_fname} ${data.patient_lname}")
            //    holder.tv_name.text = "${data.patient_fname.substring(0,1).toUpperCase()+ data.patient_fname.substring(1).toLowerCase()} ${data.patient_lname.substring(0,1).toUpperCase()+data.patient_lname.substring(1).toLowerCase()}"

            holder.tv_mobilenumber.text = "${data.patient_mobile_no}"
            holder.tv_phonenumber.text = "${data.patient_phone_no}"
            holder.tv_patienttype.text = "${data.type_name}"
            //  tv_logroup.text = "${data.loyalty_name}"
            if (data.patient_notes.isNotEmpty() && !data.patient_notes.equals("null", true)) {
                holder.tv_notes.text = viewEditString
            } else {
                holder.tv_notes.text = addString
            }

            if (data.patient_documents.isNotEmpty() && !data.patient_documents.equals("null", true)) {
                if (!data.patient_documents.equals("0", true))
                    holder.tv_document.text = viewEditString
                else {
                    holder.tv_document.text = addString
                }
            }

            if (data.loyalty_name != null && "${data.loyalty_name}".isNotEmpty()) {
                holder.tv_logroup.text = "${data.loyalty_name}"
            } else {
                holder.tv_logroup.text = "-"
            }
//            holder.llpersonal.setOnClickListener {
//
//            }
//            holder.llNotes.setOnClickListener {
//                AddEditPatientNoteDialog(context, logindata?.user_id!!, data, object : AddEditPatientNoteDialog.IsNoteUpdata {
//                    override fun onResponseChange(updatedNote: String) {
//                        data.patient_notes = updatedNote
//                        if (updatedNote.isNotEmpty()) {
//                            holder.tv_notes.text = viewEditString
//                        } else {
//                            holder.tv_notes.text = addString
//                        }
//                    }
//
//                }).show(activity!!.fragmentManager, "AddEditNoteDialog")
//            }

//            holder.llDocuments.setOnClickListener {
//                AddDocumentDialog(context, appSettingData, logindata?.user_id!!, data).show(activity!!.fragmentManager, "AddEditNoteDialog")
//            }

            holder.switch_banned.isChecked = data.patient_status.equals("3", false)

            /*holder.switch_banned.setOnCheckedChangeListener { btn, isChecked ->
                callBannnedPaitite(isChecked, data.patient_id, btn!!, position)
            }*/


            holder.llpersonal.setOnClickListener {
                paging=0
                list.clear()
                callEmployeeSearchPatient();
                println("aaaaaaaaa  data.patient_id  "+data.patient_id)
               // AddDocumentDailog().showDialog(context,appSettingData,logindata,data,patientImagePath,this@CustomerSearchFragment)
                val mydata = Gson().toJson(data)
                val logindata = Gson().toJson(logindata)
                val myIntent = Intent(activity!!, PatientEdit::class.java)
                myIntent.putExtra("patientdata", mydata)
                myIntent.putExtra("logindata", logindata)
                myIntent.putExtra("patientImagePath", patientImagePath)

                activity!!.startActivityForResult(myIntent,4)

              /*  AddDocumentNotesDialog(context, appSettingData, logindata?.user_id!!, data,patientImagePath, object : AddDocumentNotesDialog.IsNoteUpdata {
                    override fun onResponseChange(updatedNote: String) {
                        data.patient_notes = updatedNote
                        if (updatedNote.isNotEmpty()) {
                            holder.tv_notes.text = viewEditString
                        } else {
                            holder.tv_notes.text = addString
                        }
                    }

                }).show(activity!!.fragmentManager, "AddEditNoteDialog")*/
            }

            if (data.patient_status.equals("0")) {
                holder.tv_status.setText("Deactive")
            } else if (data.patient_status.equals("1")) {
                holder.tv_status.setText("Active")
            } else if (data.patient_status.equals("3")) {
                holder.tv_status.setText("Banned")
            }

            holder.switch_banned.setOnClickListener {
                var message = ""
                if (holder.switch_banned.isChecked) {
                    message = "Are you sure want to banned patient?"
                } else {
                    message = "Are you sure want to remove banned patient?"
                }
                val builder = AlertDialog.Builder(activity!!)
                //builder.setTitle("Confirm")
                builder.setMessage(message)

                builder.setPositiveButton("YES") { dialog, which ->
                    // Do nothing but close the dialog
                    callBannnedPaitite(holder.switch_banned.isChecked, data.patient_id, holder.switch_banned!!, position)
                    dialog.dismiss()
                }

                builder.setNegativeButton("NO") { dialog, which ->

                    // Do nothing
                    productAdapter.notifyDataSetChanged()
                    dialog.dismiss()
                }
                val alert = builder.create()
                alert.show()

            }
            println("aaaaaaaaaaaaaaaa  patientImagePath   "+patientImagePath+""+data.patient_image)
          //  Glide.with(context).load(patientImagePath + data.patient_image).into(holder.img_profile)


            Picasso.with(context)
                    .load(data.patient_profilepic)
                    .placeholder(R.drawable.logo_top)
                    .error(R.drawable.logo_top)
                    .into(holder.img_profile)

            /*try {

                Picasso.with(context)
                        .load(patientImagePath + data.patient_image)
                        .placeholder(R.drawable.logo_top)
                        .error(R.drawable.logo_top)
                        .into(holder.img_profile)

            } catch (e: Exception) {

                e.printStackTrace()
            }*/


            holder.tv_name.setOnClickListener {
                onClick.callClick()
            }

        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            val v = LayoutInflater.from(parent!!.context).inflate(R.layout.customer_item1, parent, false)
            return ViewHolder(v)
        }

        inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
            val tv_name: TextView = itemView.findViewById(R.id.tv_name)
            val tv_mobilenumber: TextView = itemView.findViewById(R.id.tv_mobilenumber)
            val tv_phonenumber: TextView = itemView.findViewById(R.id.tv_phonenumber)
            val tv_patienttype: TextView = itemView.findViewById(R.id.tv_patienttype)
            val tv_notes: TextView = itemView.findViewById(R.id.tv_notes)
            val tv_loyaltygroup: TextView = itemView.findViewById(R.id.tv_loyaltygroup)
            val tv_document: TextView = itemView.findViewById(R.id.tv_document)
            val switch_banned: Switch = itemView.findViewById(R.id.switch_banned)
            val tv_logroup: TextView = itemView.findViewById(R.id.tv_logroup)
            val img_profile: ImageView = itemView.findViewById(R.id.img_profile)
            val llNotes: LinearLayout = itemView.findViewById(R.id.llNotes)
            //    val llDocuments: LinearLayout = itemView.findViewById(R.id.llDocuments)
            val llpersonal: LinearLayout = itemView.findViewById(R.id.llpersonal)
            val tv_status: TextView = itemView.findViewById(R.id.tv_status)

        }
    }

    interface onClickListener {
        fun callClick()
    }

    fun setNoteClick(getNotes: GetNotes) {
        AddDocumentDailog().onClickNote(getNotes)
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            122 -> if (resultCode === Activity.RESULT_FIRST_USER) {
                root.edt_employee_search_patient.setText("")
                paging = 0
                list.clear()
                callEmployeeSearchPatient()
            }
            4 -> if (resultCode === Activity.RESULT_FIRST_USER) {
                root.edt_employee_search_patient.setText("")
                paging = 0
                list.clear()
                callEmployeeSearchPatient()
            }
        }
    }



}