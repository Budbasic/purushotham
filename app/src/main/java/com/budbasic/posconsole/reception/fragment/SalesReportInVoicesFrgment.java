package com.budbasic.posconsole.reception.fragment;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.budbasic.customer.global.Preferences;
import com.budbasic.global.CUC;
import com.budbasic.posconsole.R;
import com.budbasic.posconsole.webservice.RequetsInterface;
import com.kotlindemo.model.EmployeeSalesReport;
import com.kotlindemo.model.GetEmpExpenses;
import com.kotlindemo.model.GetEmployeeLogin;
import com.kotlindemo.model.GetInvoiceOrders;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;


public class SalesReportInVoicesFrgment extends Fragment {


    public SalesReportInVoicesFrgment() {
        // Required empty public constructor
    }
    private ImageView btn_back;
    private TextView tv_start_date;
    private AppCompatButton btn_search,btn_Scan;
    private RecyclerView rv_customer_list;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    ArrayList<GetInvoiceOrders> invoiceorderlist;
    int s_day,s_month,s_year,padding=0,offset=0;
    private Calendar calendar;
    public Dialog mDialog;
    private GetEmployeeLogin logindata;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =inflater.inflate(R.layout.fragment_sales_report_in_voices_frgment, container, false);
        btn_back=view.findViewById(R.id.btn_back);
        tv_start_date=view.findViewById(R.id.tv_start_date);
        btn_search=view.findViewById(R.id.btn_search);
        btn_Scan=view.findViewById(R.id.btn_Scan);
        rv_customer_list=view.findViewById(R.id.rv_customer_list);
        mSwipeRefreshLayout=view.findViewById(R.id.mSwipeRefreshLayout);

        rv_customer_list.setLayoutManager(new GridLayoutManager(getContext(),1));
        invoiceorderlist=new ArrayList<>();
        calendar=Calendar.getInstance();
        s_day=calendar.get(Calendar.DAY_OF_MONTH);
        s_month=calendar.get(Calendar.MONTH);
        s_year=calendar.get(Calendar.YEAR);

        tv_start_date.setText(s_day+"-"+(s_month+1)+"-"+s_year);

        getInvoiceList();

        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().onBackPressed();
            }
        });
        tv_start_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(),
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                s_day=dayOfMonth;
                                s_month=monthOfYear;
                                s_year=year;
                                tv_start_date.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);
                                invoiceorderlist.clear();
                                getInvoiceList();
                            }
                        }, s_year, s_month, s_day);
                datePickerDialog.show();
            }
        });

        return view;
    }
    public void getInvoiceList(){
        invoiceorderlist.clear();
        mDialog = CUC.Companion.createDialog(getActivity());
        RequetsInterface apiService = RequetsInterface.Factory.create();
        Map parameterMap =new HashMap<String, String>();
        parameterMap.put("api_key", String.valueOf(Preferences.INSTANCE.getPreference((getContext()), "API_KEY")));
        if (logindata != null) {
            parameterMap.put("employee_id", logindata.getUser_id());
        }

        parameterMap.put("invoice_date",s_year+"-"+(s_month+1)+"-"+s_day);
        parameterMap.put("paging",""+padding);
        parameterMap.put("offset",""+offset);


        System.out.println("aaaaaaaaaaa invoice parameterMap  "+parameterMap);
        Log.i("aaaaaaaa   Result", "parameterMap : " + parameterMap);
        (new CompositeDisposable()).add(apiService.callEmployeeSalesReport(parameterMap).
                observeOn(AndroidSchedulers.mainThread()).
                subscribeOn(Schedulers.io()).subscribe((Consumer)(new Consumer() {
            // $FF: synthetic method
            // $FF: bridge method
            public void accept(Object var1) {
                this.accept((EmployeeSalesReport)var1);
            }

            public final void accept(EmployeeSalesReport result) {
                System.out.println("aaaaaaaaaaa invoice result  "+result.toString());
                mDialog.cancel();
                if (result != null) {
                    mDialog.cancel();
                    System.out.println("aaaaaaaaa  result remove discount  "+result.toString());
                    Log.i("aaaaaaa   Result", "" + result.toString());
                    if (result.getInvoice_orders().size()>0){
                        invoiceorderlist.addAll(result.getInvoice_orders());

                        CUC.Companion.displayToast((Context)getContext(), "Sucess", "Sucess");
                    }else {

                        CUC.Companion.displayToast((Context)getContext(), "No Data", "No Data");

                    }
                } else {
                    mDialog.cancel();
                }
            }
        }), (Consumer)(new Consumer() {
            // $FF: synthetic method
            // $FF: bridge method
            public void accept(Object var1) {
                this.accept((Throwable)var1);
            }

            public final void accept(Throwable error) {
                mDialog.cancel();
                Toast.makeText(getContext(), getString(R.string.connection_to_database_failed), Toast.LENGTH_SHORT).show();
                error.printStackTrace();
                System.out.println("aaaaaaaa  error update "+error.getMessage());
            }
        })));
    }
}
