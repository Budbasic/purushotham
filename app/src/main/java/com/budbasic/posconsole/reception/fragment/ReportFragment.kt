package com.budbasic.posconsole.reception.fragment

import android.app.Activity
import android.content.Intent
import android.graphics.Color
import android.net.Uri
import android.os.Bundle
import android.provider.Settings
import android.support.design.widget.Snackbar
import android.support.v4.app.Fragment
import android.support.v7.widget.CardView
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.budbasic.posconsole.R
import com.budbasic.posconsole.queue.MenuActivity
import com.budbasic.posconsole.reception.StoreManagerActivity
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DecodeFormat
import com.bumptech.glide.request.RequestOptions
import io.reactivex.functions.Consumer
import android.R.attr.spacing
import com.budbasic.posconsole.global.GridSpacingItemDecoration
import kotlinx.android.synthetic.main.report_fragment.*


class ReportFragment : Fragment() {
    lateinit var context: StoreManagerActivity
    var mView: View? = null

    companion object {
        var receptionList = ArrayList<String>()
        var listner: ClickListner? = null
    }

    fun setListner(listner1: ClickListner): ClickListner {
        listner = listner1
        return listner1
    }

    /*   companion object {
           var dashList = ArrayList<String>()
           var listner: ClickListner? = null
       }


      */

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        if (view != null)
            mView = view
        else
            mView = inflater.inflate(R.layout.report_fragment, container, false)
        initView()
        return mView
    }

    fun initView() {
        receptionList.clear()
        receptionList.add("SALES REPORT")
        receptionList.add("GRAPH REPORT")

        val spanCount = 2 // 3 columns
        val spacing = 40 // 50px
        val includeEdge = false
        val rv_report_item = mView!!.findViewById<RecyclerView>(R.id.rv_report_item)
        rv_report_item.layoutManager = GridLayoutManager(activity!!, 2, GridLayoutManager.VERTICAL, false)
        rv_report_item.addItemDecoration(GridSpacingItemDecoration(spanCount, spacing, includeEdge))
        val receptionAdapter = ReceptionAdapter(context)
        rv_report_item.adapter = receptionAdapter
        //   ReceptionFragment.receptionList
    }

    class ReceptionAdapter(context: StoreManagerActivity?) : RecyclerView.Adapter<ReceptionAdapter.ViewHolder>() {
        lateinit var context: StoreManagerActivity
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            val rootView = LayoutInflater.from(parent.context).inflate(R.layout.reception_item, parent, false)
            return ViewHolder(rootView)
        }

        init {
            this.context = context!!
        }

        override fun getItemCount(): Int {
            return receptionList.size
        }

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            if (position == 1) {
                Glide.with(holder.iv_icon.context)
                        .applyDefaultRequestOptions(RequestOptions().format(DecodeFormat.PREFER_ARGB_8888))
                        .load(R.drawable.tv_salesreport)
                        .into(holder.iv_icon)
                holder.tv_title.text = "${receptionList[position]}"
            } else {
                Glide.with(holder.iv_icon.context)
                        .applyDefaultRequestOptions(RequestOptions().format(DecodeFormat.PREFER_ARGB_8888))
                        .load(R.drawable.tv_salesreport)
                        .into(holder.iv_icon)
                holder.tv_title.text = "${receptionList[position]}"
            }
            //on click
            holder.cv_reception.setOnClickListener {
                if (position == 0) {
                    context.fragment = SalesReportFragment()
                    context.addOrReplace(context.fragment!!, "replace")
                }
                if (position == 1) {
                    context.fragment = GraphReportFragment()
                    context.setFragmentWithBack(context.fragment!!,false,null,GraphReportFragment::class.java.simpleName)
                  //  context.addOrReplace(context.fragment!!, "replace")
                }
            }

        }

        class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
            val iv_icon = itemView.findViewById<ImageView>(R.id.iv_icon)
            val tv_title = itemView.findViewById<TextView>(R.id.tv_title)
            val cv_reception = itemView.findViewById<CardView>(R.id.cv_reception)

            fun bindView() {
            }
        }
    }

    interface ClickListner {
        fun onClickReceptionList(position: Int)
    }

    override fun onAttach(activity: Activity?) {
        super.onAttach(activity)
        try {
            context = (activity as StoreManagerActivity?)!!
        } catch (e: ClassCastException) {
            throw ClassCastException(activity!!.toString() + " must implement onViewSelected")
        }

    }

}


