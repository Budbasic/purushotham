package com.budbasic.posconsole.reception

import android.app.Activity
import android.content.Intent
import android.graphics.PorterDuff
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.EditText
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.budbasic.customer.global.Preferences
import com.budbasic.global.CUC
import com.budbasic.posconsole.R
import com.budbasic.posconsole.model.APIClass
import com.budbasic.posconsole.webservice.RequetsInterface
import com.google.gson.Gson
import com.kotlindemo.model.GetEmployeeLogin
import com.kotlindemo.model.GetPatient
import com.mikepenz.fontawesome_typeface_library.FontAwesome
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.quick_sale.*
import kotlinx.android.synthetic.main.store_search.*
import java.util.HashMap
import kotlin.collections.ArrayList
import kotlin.collections.set


class ReceptionSalesPatient : AppCompatActivity(), APIClass.onCallListner {
    override fun onFailed(requestCode: Int) {
    }

    override fun onSuccess(requestCode: Int) {
        if (requestCode == 1) {
            callEmployeeSalesSearchPatient()
        }
    }
    //lateinit var mDialog: Dialog

    lateinit var adapter: ReceiptTaxItemAdapter

    var mSearchProductName = ""
    lateinit var logindata: GetEmployeeLogin

    var assign_msg = "We already having this patient,do you want to aquire this details to your store?"

    companion object {
        var searchdataArrayList = ArrayList<GetPatient>()
    }

    var apiClass: APIClass? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.store_search)
        apiClass = APIClass(this@ReceptionSalesPatient, this)
        logindata = Gson().fromJson(Preferences.getPreference(this@ReceptionSalesPatient, "logindata"), GetEmployeeLogin::class.java)
        initView()
    }

    fun initView() {
        search_product.setImeOptions(EditorInfo.IME_FLAG_NO_EXTRACT_UI);
        iv_back.setOnClickListener {
            searchdataArrayList.clear()
            adapter.notifyDataSetChanged()
            finish()
        }
        setupRecylcer()
        customizeSearchView()
        callEmployeeSalesSearchPatient()
    }

    override fun onBackPressed() {
        //super.onBackPressed()
        searchdataArrayList.clear()
        adapter.notifyDataSetChanged()
        finish()
    }

    private fun setupRecylcer() {
        recycle_searchproduct.setHasFixedSize(true)
        recycle_searchproduct.layoutManager = LinearLayoutManager(this)
        recycle_searchproduct.itemAnimator = DefaultItemAnimator()
        adapter = ReceiptTaxItemAdapter(object : onClickListener {
            override fun callClick(data: GetPatient) {
                CUC.hideSoftKeyboard(this@ReceptionSalesPatient)
                searchdataArrayList.clear()
                adapter.notifyDataSetChanged()
                val returnIntent = Intent()
                val data = Gson().toJson(data)
                returnIntent.putExtra("assign_msg", assign_msg)
                returnIntent.putExtra("data", data)
                setResult(Activity.RESULT_OK, returnIntent)
                finish()
            }

        })
        recycle_searchproduct.adapter = adapter
    }

    private fun customizeSearchView() {
        search_product.isFocusable = true
        search_product.isIconified = false
        search_product.setOnSearchClickListener(object : View.OnClickListener {
            override fun onClick(p0: View?) {
            }

        })
        // val searchButton = search_product.findViewById<ImageView>(android.support.v7.appcompat.R.id.search_button)
        // searchButton.setColorFilter(ContextCompat.getColor(this@ReceptionSalesPatient, R.color.colorPrimary), PorterDuff.Mode.SRC_ATOP)
        /* val searchMagIcon = search_product.findViewById(android.support.v7.appcompat.R.id.search_button) as ImageView
         searchMagIcon.setImageResource(R.drawable.ic_search)*/
        val searchBox = search_product.findViewById(android.support.v7.appcompat.R.id.search_src_text) as EditText
        // searchBox.setBackgroundResource(R.drawable.null_selector)
        searchBox.setTextColor(resources.getColor(R.color.border_gray))
        searchBox.setHintTextColor(resources.getColor(R.color.graylight))
        searchBox.hint = "Search"
        /*   val searchClose= search_product.findViewById<ImageView>(android.support.v7.appcompat.R.id.search_close_btn)
           // searchClose.setImageResource(R.drawable.ic_)
           searchClose.setImageBitmap(CUC.IconicsBitmap(this, FontAwesome.Icon.faw_times, ContextCompat.getColor(this, R.color.graylight)))

   */

        //  val img_close=mView!!.findViewById<ImageView>(R.id.img_close)
        //  img_close.setImageBitmap(CUC.IconicsBitmap(activity, FontAwesome.Icon.faw_times, ContextCompat.getColor(activity, R.color.graylight)))
        img_close.setOnClickListener {
            finish()
        }
        //View v = search_product.findViewById(android.support.v7.appcompat.R.id.search_plate);
        //v.setBackgroundColor(ContextCompat.getColor(CountryCitySearch.this, R.color.search));
        search_product.setOnCloseListener(object : android.support.v7.widget.SearchView.OnCloseListener {
            override fun onClose(): Boolean {
                searchdataArrayList.clear()
                adapter.notifyDataSetChanged()
                setResult(Activity.RESULT_OK)
                finish()
                return false
            }
        })

        search_product.setOnQueryTextListener(object : android.support.v7.widget.SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String): Boolean {
                return false
            }

            override fun onQueryTextChange(newText: String): Boolean {
                if (adapter!! != null) {
                    if (!newText.isEmpty()) {
                        if (newText.length >0) {
                            mSearchProductName = newText
                            callEmployeeSalesSearchPatient()
                        }
                    } else {
                        mSearchProductName = newText
//                            stateList.clear()
//                            locationCityAdapter.notifyDataSetChanged()
//                            tv_nodata.visibility = View.VISIBLE
//                            recycle_searchproduct.visibility = View.GONE
                        callEmployeeSalesSearchPatient()
                    }
                } else {
                    if (newText.length >0) {
                        mSearchProductName = newText
                        callEmployeeSalesSearchPatient()
                    }
                }
                return false
            }
        })
    }

    fun callEmployeeSalesSearchPatient() {
        simpleProgressBar.setVisibility(View.VISIBLE)
        //mDialog = CommonUtils.createDialog(this@CountryCitySearch)
        val apiService = RequetsInterface.Factory.create()
        val parameterMap = HashMap<String, String>()
        parameterMap["api_key"] = "${Preferences.getPreference(this@ReceptionSalesPatient, "API_KEY")}"
        parameterMap["patient_search"] = "$mSearchProductName"
        if (logindata != null) {
            parameterMap["employee_id"] = "${logindata.user_id}"
        }
        parameterMap["store_id"] = "${logindata.user_store_id}"
        Log.i("Result", "parameterMap : $parameterMap")
        CompositeDisposable().add(apiService.callEmployeeSalesSearchPatient(parameterMap)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe({ result ->
                    //                    if (mDialog != null && mDialog.isShowing)
//                        mDialog.dismiss()
                    if (result != null) {
                        Log.i("Result", "" + result.toString())
                        if (result.status == "0") {
                            assign_msg = "$result.assign_msg"
                            if (result.patient != null && result.patient.size > 0) {
                                searchdataArrayList.clear()
                                searchdataArrayList.addAll(result.patient)
                                adapter.notifyDataSetChanged()
                                tv_nodata.visibility = View.GONE
                                recycle_searchproduct.visibility = View.VISIBLE
                                simpleProgressBar.visibility = View.INVISIBLE
                            } else {
                                searchdataArrayList.clear()
                                adapter.notifyDataSetChanged()
                                tv_nodata.visibility = View.VISIBLE
                                recycle_searchproduct.visibility = View.GONE
                                simpleProgressBar.visibility = View.INVISIBLE
                            }
                            CUC.displayToast(this@ReceptionSalesPatient, result.show_status, result.message)
                        } else if (result.status == "10") {
                            apiClass!!.callApiKey(1)
                        } else {
                            simpleProgressBar.setVisibility(View.INVISIBLE);
                            CUC.displayToast(this@ReceptionSalesPatient, result.show_status, result.message)
                        }
                    } else {
                        simpleProgressBar.setVisibility(View.INVISIBLE);
                       // CUC.displayToast(this@ReceptionSalesPatient, "0", getString(R.string.connection_to_database_failed))
                    }


                }, { error ->
                    simpleProgressBar.setVisibility(View.INVISIBLE)
                    //                    if (mDialog != null && mDialog.isShowing)
//                        mDialog.dismiss()
                    CUC.displayToast(this@ReceptionSalesPatient, "0", getString(R.string.connection_to_database_failed))
                    error.printStackTrace()
                })
        )
    }

    class ReceiptTaxItemAdapter(val listner: onClickListener) : RecyclerView.Adapter<ReceiptTaxItemAdapter.ViewHolder>() {

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            holder!!.bindItems(searchdataArrayList[position], listner)
        }

        override fun getItemCount(): Int {
            return searchdataArrayList.size
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            val v = LayoutInflater.from(parent!!.context).inflate(R.layout.product_search_item, parent, false)
            return ViewHolder(v)
        }

        class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

            val ll_itemamin = itemView.findViewById<LinearLayout>(R.id.ll_itemamin)
            val tv_text = itemView.findViewById<TextView>(R.id.tv_text)
            val tv_texttag = itemView.findViewById<TextView>(R.id.tv_texttag)

            fun bindItems(data: GetPatient, listner: onClickListener) {
                tv_text.text = "Patient Name : ${data.Patientname}"
                tv_texttag.text = "Patient licence : ${data.patient_licence_no}"
                ll_itemamin.setOnClickListener {
                    listner.callClick(data)
                }
            }
        }
    }


    interface onClickListener {
        fun callClick(data: GetPatient)
    }
}