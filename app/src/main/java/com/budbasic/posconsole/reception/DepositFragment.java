package com.budbasic.posconsole.reception;


import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.budbasic.customer.global.Preferences;
import com.budbasic.global.CUC;
import com.budbasic.posconsole.R;
import com.budbasic.posconsole.adapter.DepositAdapter;
import com.budbasic.posconsole.adapter.ExpensesAdapter;
import com.budbasic.posconsole.dialogs.AddProductDialog;
import com.budbasic.posconsole.webservice.RequetsInterface;
import com.google.gson.Gson;
import com.kotlindemo.model.GetBanks;
import com.kotlindemo.model.GetDepositItems;
import com.kotlindemo.model.GetDepositbanklist;
import com.kotlindemo.model.GetDeposits;
import com.kotlindemo.model.GetEmpExpenses;
import com.kotlindemo.model.GetEmployeeAppSetting;
import com.kotlindemo.model.GetEmployeeExpanses;
import com.kotlindemo.model.GetEmployeeLogin;
import com.kotlindemo.model.GetExpensesItems;
import com.kotlindemo.model.GetExpensesList;
import com.kotlindemo.model.GetStatus;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

/**
 * A simple {@link Fragment} subclass.
 */
@SuppressLint("ValidFragment")
public class DepositFragment extends Fragment  {

    private  GetEmployeeLogin logindata;
    @SuppressLint("ValidFragment")
    public DepositFragment(GetEmployeeLogin logindata) {
        // Required empty public constructor
        this.logindata = logindata;
    }
    private EditText edt_description,edt_amount;
    private LinearLayout cv_add_expenses1,layout_main,layout_add;
    private TextView tv_date,tv_expenseitem;
    GetEmployeeAppSetting mBAsis;
    private TextView tv_start_date,tv_end_date;
    private RecyclerView rv_customer_list;
    private RelativeLayout cv_create_patient2;
    private Calendar calendar,calendar2;
    public Dialog mDialog;
    private Gson gson;
    private int s_day,s_month,s_year,t_day,t_month,t_year,day,month,year;
    private DepositAdapter depositAdapter;
    private List<GetDepositItems> getDepositlist;
    private ImageView btn_back;
    String st_amount;
    GetDepositbanklist getdepositbankitem;
    AddProductDialog addProductDialog;
    GetDepositItems getDepositItems;
    private ArrayList<GetDepositbanklist> getDepositbanklists;
    private int position;
    boolean check;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v= inflater.inflate(R.layout.fragment_deposit, container, false);
        tv_start_date=v.findViewById(R.id.tv_start_date);
        tv_end_date=v.findViewById(R.id.tv_end_date);
        rv_customer_list=v.findViewById(R.id.rv_customer_list);
        cv_create_patient2=v.findViewById(R.id.cv_create_patient2);
        btn_back=v.findViewById(R.id.btn_back);
        edt_amount=v.findViewById(R.id.edt_amount);
        cv_add_expenses1=v.findViewById(R.id.cv_add_expenses1);
        tv_date=v.findViewById(R.id.tv_date);
        tv_expenseitem=v.findViewById(R.id.tv_expenseitem);
        layout_main=v.findViewById(R.id.layout_main);
        layout_add=v.findViewById(R.id.layout_add);

        getDepositbanklists=new ArrayList<>();
        edt_amount.setImeOptions(EditorInfo.IME_FLAG_NO_EXTRACT_UI);

        layout_add.setVisibility(View.GONE);
        layout_main.setVisibility(View.VISIBLE);
        getDepositlist=new ArrayList<GetDepositItems>();
        calendar=Calendar.getInstance();
        calendar2=Calendar.getInstance();
        day=calendar.get(Calendar.DAY_OF_MONTH);
        month=calendar.get(Calendar.MONTH);
        year=calendar.get(Calendar.YEAR);
        calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMinimum(Calendar.DAY_OF_MONTH));
        s_day=calendar.get(Calendar.DAY_OF_MONTH);
        s_month=calendar.get(Calendar.MONTH);
        s_year=calendar.get(Calendar.YEAR);

        tv_start_date.setText(s_day+"-"+(s_month+1)+"-"+s_year);
        tv_date.setText(s_day+"-"+(s_month+1)+"-"+s_year);
        calendar2.set(Calendar.DAY_OF_MONTH, calendar2.getActualMaximum(Calendar.DAY_OF_MONTH));
        t_day=calendar2.get(Calendar.DAY_OF_MONTH);
        t_month=calendar2.get(Calendar.MONTH);
        t_year=calendar2.get(Calendar.YEAR);
        tv_end_date.setText(t_day+"-"+(t_month+1)+"-"+t_year);
        rv_customer_list.setLayoutManager(new GridLayoutManager(getContext(),1));
        gson=new Gson();
        mBAsis = gson.fromJson(Preferences.INSTANCE.getPreference(getContext(), "AppSetting"), GetEmployeeAppSetting.class);
        depositAdapter=new DepositAdapter(getContext(),getDepositlist,logindata, DepositFragment.this);
        rv_customer_list.setAdapter(depositAdapter);

        getExpensesList();

        cv_create_patient2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                layout_add.setVisibility(View.VISIBLE);
                layout_main.setVisibility(View.GONE);
                edt_amount.setText("");
                tv_expenseitem.setText("");
                check=true;
            }
        });
        tv_start_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(),
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                s_day=dayOfMonth;
                                s_month=monthOfYear;
                                s_year=year;
                                tv_start_date.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);
                                getDepositlist.clear();
                                getExpensesList();
                            }
                        }, s_year, s_month, s_day);
                datePickerDialog.show();
            }
        });
        tv_end_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(),
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                t_day=dayOfMonth;
                                t_month=monthOfYear+1;
                                t_year=year;
                                tv_end_date.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);
                                getDepositlist.clear();
                                getExpensesList();

                            }
                        }, t_year, t_month, t_day);
                datePickerDialog.show();
            }
        });
        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (check){
                    position=0;
                    check=false;
                    layout_add.setVisibility(View.GONE);
                    layout_main.setVisibility(View.VISIBLE);
                }else {
                    getActivity().onBackPressed();
                }

            }
        });
        cv_add_expenses1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                st_amount=edt_amount.getText().toString().trim();
                if (position==1){
                    getEditExpenses(st_amount);
                }else {
                    getAddExpense(st_amount);
                }
            }
        });
        tv_expenseitem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getDepositbanks();

            }
        });
        return  v;
    }
    public void getDepositbanks(){
        getDepositbanklists.clear();
        if (CUC.Companion.isNetworkAvailablewithPopup(getActivity())) {

            mDialog = CUC.Companion.createDialog(getActivity());
            RequetsInterface apiService = RequetsInterface.Factory.create();
            Map parameterMap =new HashMap<String, String>();
            parameterMap.put("api_key", String.valueOf(Preferences.INSTANCE.getPreference(getActivity(), "API_KEY")));
            Log.i("aaaaaaaa   Result", "parameterMap : " + parameterMap);
            (new CompositeDisposable()).add(apiService.callDepositbanks(parameterMap)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io()).subscribe((Consumer)(new Consumer() {
                        // $FF: synthetic method
                        // $FF: bridge method
                        public void accept(Object var1) {
                            this.accept((GetBanks)var1);
                        }

                        public final void accept(GetBanks result) {
                            mDialog.cancel();
                            if (result != null) {
                                System.out.println("aaaaaaaaa  result  "+result.toString());
                                Log.i("aaaaaaa   Result", "" + result.toString());
                                if(result.getDepositebank().size()>0){
                                    getDepositbanklists.addAll(result.getDepositebank());
                                    addProductDialog=new AddProductDialog();
                                    addProductDialog.showDialog(getActivity(),getDepositbanklists, DepositFragment.this);
                                }else {

                                }
                            } else {
                            }
                        }
                    }), (Consumer)(new Consumer() {
                        // $FF: synthetic method
                        // $FF: bridge method
                        public void accept(Object var1) {
                            this.accept((Throwable)var1);
                        }

                        public final void accept(Throwable error) {
                            mDialog.cancel();
                            Toast.makeText(getActivity(), getString(R.string.connection_to_database_failed), Toast.LENGTH_SHORT).show();
                            error.printStackTrace();
                            System.out.println("aaaaaaaa  error "+error.getMessage());
                        }
                    })));

        }else {
            System.out.println("aaaaaaaaaa   else ");
        }
    }
   

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        System.out.println("aaaaaaaaaa  fragment on ressume else");
        if (isVisibleToUser) {
            System.out.println("aaaaaaaaaa  fragment on ressume iff ");
            getDepositlist.clear();
            getExpensesList();
            if (getView() != null) {

                // your code goes here
            }
        }
    }

    public void getExpensesList(){
        getDepositlist.clear();
        System.out.println("aaaaaaaaa   "+s_year+"-"+(s_month+1)+"-"+s_day+"   to  "+t_year+"-"+(t_month+1)+"-"+t_day);
        mDialog = CUC.Companion.createDialog(getActivity());
        RequetsInterface apiService = RequetsInterface.Factory.create();
        Map parameterMap =new HashMap<String, String>();
        parameterMap.put("api_key", String.valueOf(Preferences.INSTANCE.getPreference((getContext()), "API_KEY")));
        if (logindata != null) {
          //  parameterMap.put("employee_id", logindata.getUser_id());
            parameterMap.put("store_id", logindata.getUser_store_id());
        }
        if (mBAsis != null) {
            parameterMap.put("terminal_id",mBAsis.getTerminal_id());
        }
        parameterMap.put("from_date",s_year+"-"+(s_month+1)+"-"+s_day+" 00:00:00");
        parameterMap.put("to_date",t_year+"-"+(t_month+1)+"-"+t_day+" 23:59:59");

        System.out.println("aaaaaaaaaaa  parameterMap remove discount "+parameterMap);
        Log.i("aaaaaaaa   Result", "parameterMap : " + parameterMap);
        (new CompositeDisposable()).add(apiService.callDeposit(parameterMap).
                observeOn(AndroidSchedulers.mainThread()).
                subscribeOn(Schedulers.io()).subscribe((Consumer)(new Consumer() {
            // $FF: synthetic method
            // $FF: bridge method
            public void accept(Object var1) {
                this.accept((GetDeposits)var1);
            }

            public final void accept(GetDeposits result) {
                System.out.println("aaaaaaaaa  result  "+result.toString());
                mDialog.cancel();
                if (result != null) {
                    mDialog.cancel();
                    System.out.println("aaaaaaaaa  result remove discount  "+result.toString());
                    Log.i("aaaaaaa   Result", "" + result.toString());
                    if (result.getDeposit_data().size()>0){
                        getDepositlist.addAll(result.getDeposit_data());
                        depositAdapter.setRefresh(getDepositlist);
                        CUC.Companion.displayToast((Context)getContext(), "Sucess", "Sucess");
                    }else {
                        depositAdapter.setRefresh(getDepositlist);
                        CUC.Companion.displayToast((Context)getContext(), "No Data", "No Data");

                    }
                } else {
                    mDialog.cancel();
                }
            }
        }), (Consumer)(new Consumer() {
            // $FF: synthetic method
            // $FF: bridge method
            public void accept(Object var1) {
                this.accept((Throwable)var1);
            }

            public final void accept(Throwable error) {
                mDialog.cancel();
                Toast.makeText(getContext(), getString(R.string.connection_to_database_failed), Toast.LENGTH_SHORT).show();
                error.printStackTrace();
                System.out.println("aaaaaaaa  error update "+error.getMessage());
            }
        })));
    }

    public void setDelete(GetDepositItems getDepositItems) {
        mDialog = CUC.Companion.createDialog(getActivity());
        RequetsInterface apiService = RequetsInterface.Factory.create();
        Map parameterMap =new HashMap<String, String>();
        parameterMap.put("api_key", String.valueOf(Preferences.INSTANCE.getPreference((getContext()), "API_KEY")));
        if (logindata != null) {
            parameterMap.put("emp ", logindata.getUser_id());
        }
        parameterMap.put("item_id",""+getDepositItems.getItem_id());

        System.out.println("aaaaaaaaaaa  parameterMap remove discount "+parameterMap);
        Log.i("aaaaaaaa   Result", "parameterMap : " + parameterMap);
        (new CompositeDisposable()).add(apiService.callDeleteDeposit(parameterMap).
                observeOn(AndroidSchedulers.mainThread()).
                subscribeOn(Schedulers.io()).subscribe((Consumer)(new Consumer() {
            // $FF: synthetic method
            // $FF: bridge method
            public void accept(Object var1) {
                this.accept((GetStatus)var1);
            }

            public final void accept(GetStatus result) {
                System.out.println("aaaaaaaaa  result  "+result.toString());
                mDialog.cancel();
                if (result != null) {
                    mDialog.cancel();
                    System.out.println("aaaaaaaaa  result remove discount  "+result.toString());
                    Log.i("aaaaaaa   Result", "" + result.toString());
                    if (result.getStatus().equalsIgnoreCase("0")){
                        getExpensesList();
                        CUC.Companion.displayToast((Context)getContext(), "Sucess", "Sucess");
                    }else {
                        CUC.Companion.displayToast((Context)getContext(), "No Data", "No Data");
                    }
                } else {
                    mDialog.cancel();
                }
            }
        }), (Consumer)(new Consumer() {
            // $FF: synthetic method
            // $FF: bridge method
            public void accept(Object var1) {
                this.accept((Throwable)var1);
            }

            public final void accept(Throwable error) {
                mDialog.cancel();
                Toast.makeText(getContext(), getString(R.string.connection_to_database_failed), Toast.LENGTH_SHORT).show();
                error.printStackTrace();
                System.out.println("aaaaaaaa  error update "+error.getMessage());
            }
        })));
    }

    public void setEdit(GetDepositItems getDepositItems) {
        layout_main.setVisibility(View.GONE);
        layout_add.setVisibility(View.VISIBLE);
        this.getDepositItems=getDepositItems;
        edt_amount.setText(getDepositItems.getDepositamount());
        tv_expenseitem.setText(getDepositItems.getDeposit());
        edt_amount.setSelection(edt_amount.getText().length());
        position=1;
        check=true;
    }
    

    public void setexpansesItem(GetDepositbanklist getdepositbankitem) {
        tv_expenseitem.setText(""+getdepositbankitem.getDeposit());
        this.getdepositbankitem=getdepositbankitem;
        check=true;
    }

    public void getAddExpense(String st_amount){
        if (CUC.Companion.isNetworkAvailablewithPopup(getActivity())) {
            mDialog = CUC.Companion.createDialog(getActivity());
            RequetsInterface apiService = RequetsInterface.Factory.create();
            Map parameterMap =new HashMap<String, String>();
            parameterMap.put("api_key", String.valueOf(Preferences.INSTANCE.getPreference(getActivity(), "API_KEY")));
            parameterMap.put("emp",""+logindata.getUser_id());
            parameterMap.put("store_id",""+logindata.getUser_store_id());
            parameterMap.put("date",""+(month+1)+"-"+day+"-"+year);
            parameterMap.put("bank",""+tv_expenseitem.getText().toString());
            parameterMap.put("amount",""+st_amount);

            Log.i("aaaaaaaa   Result", "parameterMap : " + parameterMap);
            (new CompositeDisposable()).add(apiService.callAddDeposit(parameterMap)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io()).subscribe((Consumer)(new Consumer() {
                        // $FF: synthetic method
                        // $FF: bridge method
                        public void accept(Object var1) {
                            this.accept((GetStatus)var1);
                        }

                        public final void accept(GetStatus result) {
                            mDialog.cancel();
                            if (result != null) {
                                System.out.println("aaaaaaaaa  result  "+result.toString());
                                Log.i("aaaaaaa   Result", "" + result.toString());
                                if(result.getStatus().equalsIgnoreCase("0")){
                                    Toast.makeText(getActivity(), ""+result.getMsg(), Toast.LENGTH_SHORT).show();
                                    CUC.Companion.displayToast(getActivity(), result.getMsg(), result.getMsg());
                                    layout_main.setVisibility(View.VISIBLE);
                                    layout_add.setVisibility(View.GONE);
                                    getExpensesList();
                                }else {
                                    Toast.makeText(getActivity(), ""+result.getMsg(), Toast.LENGTH_SHORT).show();
                                    CUC.Companion.displayToast(getActivity(), result.getMsg(), result.getMsg());
                                }



                            } else {
                            }
                        }
                    }), (Consumer)(new Consumer() {
                        // $FF: synthetic method
                        // $FF: bridge method
                        public void accept(Object var1) {
                            this.accept((Throwable)var1);
                        }

                        public final void accept(Throwable error) {
                            mDialog.cancel();
                            Toast.makeText(getActivity(), getString(R.string.connection_to_database_failed), Toast.LENGTH_SHORT).show();
                            error.printStackTrace();
                            System.out.println("aaaaaaaa  error "+error.getMessage());
                        }
                    })));

        }else {
            System.out.println("aaaaaaaaaa   else ");
        }
    }

    public void getEditExpenses(String st_amount){
        if (CUC.Companion.isNetworkAvailablewithPopup(getActivity())) {

            mDialog = CUC.Companion.createDialog(getActivity());
            RequetsInterface apiService = RequetsInterface.Factory.create();
            Map parameterMap =new HashMap<String, String>();
            parameterMap.put("api_key", String.valueOf(Preferences.INSTANCE.getPreference(getActivity(), "API_KEY")));
            parameterMap.put("emp",""+logindata.getUser_id());
            parameterMap.put("store_id",""+logindata.getUser_store_id());
            parameterMap.put("terminal_id",mBAsis.getTerminal_id());
            parameterMap.put("item_id",getDepositItems.getItem_id());
            parameterMap.put("date",""+(month+1)+"-"+day+"-"+year);
            parameterMap.put("bank",tv_expenseitem.getText().toString());
            parameterMap.put("amount",""+st_amount);

            Log.i("aaaaaaaa   Result", "parameterMap : " + parameterMap);
            (new CompositeDisposable()).add(apiService.calleditDeposit(parameterMap)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io()).subscribe((Consumer)(new Consumer() {
                        // $FF: synthetic method
                        // $FF: bridge method
                        public void accept(Object var1) {
                            this.accept((GetStatus)var1);
                        }

                        public final void accept(GetStatus result) {
                            mDialog.cancel();
                            if (result != null) {
                                System.out.println("aaaaaaaaa  result  "+result.toString());
                                Log.i("aaaaaaa   Result", "" + result.toString());
                                if(result.getStatus().equalsIgnoreCase("0")){
                                    Toast.makeText(getActivity(), ""+result.getMessage(), Toast.LENGTH_SHORT).show();
                                    CUC.Companion.displayToast(getActivity(), result.getMessage(), result.getMessage());
                                    layout_main.setVisibility(View.VISIBLE);
                                    layout_add.setVisibility(View.GONE);
                                    getExpensesList();

                                }else {
                                    Toast.makeText(getActivity(), ""+result.getMessage(), Toast.LENGTH_SHORT).show();
                                    CUC.Companion.displayToast(getActivity(), result.getMessage(), result.getMessage());
                                }

                            } else {
                            }
                        }
                    }), (Consumer)(new Consumer() {
                        // $FF: synthetic method
                        // $FF: bridge method
                        public void accept(Object var1) {
                            this.accept((Throwable)var1);
                        }

                        public final void accept(Throwable error) {
                            mDialog.cancel();
                            Toast.makeText(getActivity(), getString(R.string.connection_to_database_failed), Toast.LENGTH_SHORT).show();
                            error.printStackTrace();
                            System.out.println("aaaaaaaa  error "+error.getMessage());
                        }
                    })));

        }else {
            System.out.println("aaaaaaaaaa   else ");
        }
    }
    
}
