package com.budbasic.posconsole.reception.fragment;

import android.app.DialogFragment;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.RequiresApi;

import com.budbasic.customer.global.Preferences;
import com.budbasic.global.CUC;
import com.budbasic.posconsole.R;
import com.budbasic.posconsole.webservice.RequetsInterface;
import com.google.gson.Gson;
import com.kotlindemo.model.GetDeposits;
import com.kotlindemo.model.GetEmployeeAppSetting;
import com.kotlindemo.model.GetEmployeeLogin;
import com.kotlindemo.model.GetEmployeeSearchProducts;

import org.jetbrains.annotations.NotNull;

import java.util.HashMap;
import java.util.Map;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import kotlin.jvm.internal.Intrinsics;

public class BarCodePrintFragment extends DialogFragment {

    GetEmployeeAppSetting mAppBasic;
    GetEmployeeLogin logindata;
    Gson gson;
    private SearchView search_product;
    private RecyclerView recycle_searchproduct;
    ProgressBar progressBar;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view= inflater.inflate(R.layout.store_searchs, container, false);
        setStyle(DialogFragment.STYLE_NO_TITLE, R.style.TransparentProgressDialog);
        search_product=view.findViewById(R.id.search_product);
        recycle_searchproduct=view.findViewById(R.id.recycle_searchproduct);
        progressBar=view.findViewById(R.id.simpleProgressBar);

        recycle_searchproduct.setLayoutManager(new GridLayoutManager(getContext(),1));
        gson=new Gson();
        mAppBasic = gson.fromJson(Preferences.INSTANCE.getPreference(getContext(), "AppSetting"), GetEmployeeAppSetting.class);
        logindata = gson.fromJson(Preferences.INSTANCE.getPreference(getContext(), "logindata"), GetEmployeeLogin.class);

        callEmployeeScanProducts("");

        return view;
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    public final void callEmployeeScanProducts(@NotNull String name) {
        Intrinsics.checkParameterIsNotNull(name, "name");
        com.budbasic.global.CUC.Companion var10000 = CUC.Companion;
        if (var10000.isNetworkAvailablewithPopup(getContext())) {
            progressBar.setVisibility(View.VISIBLE);
            RequetsInterface apiService = RequetsInterface.Factory.create();
            Map parameterMap =new HashMap<String, String>();
            parameterMap.put("api_key", String.valueOf(Preferences.INSTANCE.getPreference((getContext()), "API_KEY")));
            if (logindata != null) {
                  parameterMap.put("employee_id", logindata.getUser_id());
                parameterMap.put("store_id", logindata.getUser_store_id());
            }
            if (mAppBasic != null) {
                parameterMap.put("terminal_id",mAppBasic.getTerminal_id());
            }

            System.out.println("aaaaaaaaaaa  parameterMap search products "+parameterMap);
            Log.i("aaaaaaaa   Result", "parameterMap : " + parameterMap);
            (new CompositeDisposable()).add(apiService.callEmployeeSearchProducts(parameterMap).
                    observeOn(AndroidSchedulers.mainThread()).
                    subscribeOn(Schedulers.io()).subscribe((Consumer)(new Consumer() {
                // $FF: synthetic method
                // $FF: bridge method
                public void accept(Object var1) {
                    this.accept((GetEmployeeSearchProducts)var1);
                }

                public final void accept(GetEmployeeSearchProducts result) {
                    System.out.println("aaaaaaaaa  result  "+result.toString());

                    if (result != null) {

                        System.out.println("aaaaaaaaa  result remove discount  "+result.toString());
                        Log.i("aaaaaaa   Result", "" + result.toString());
                        CUC.Companion.displayToast((Context)getContext(), "Sucess", "Sucess");
                    } else {

                    }
                }
            }), (Consumer)(new Consumer() {
                // $FF: synthetic method
                // $FF: bridge method
                public void accept(Object var1) {
                    this.accept((Throwable)var1);
                }

                public final void accept(Throwable error) {
                    Toast.makeText(getContext(), getString(R.string.connection_to_database_failed), Toast.LENGTH_SHORT).show();
                    error.printStackTrace();
                    System.out.println("aaaaaaaa  error update "+error.getMessage());
                }
            })));


        }
    }


}
