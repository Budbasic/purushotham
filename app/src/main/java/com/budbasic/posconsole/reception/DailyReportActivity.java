package com.budbasic.posconsole.reception;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.budbasic.posconsole.R;
import com.google.gson.Gson;
import com.kotlindemo.model.GetDailyReport;
import com.kotlindemo.model.GetEmployeeLogin;

public class DailyReportActivity extends AppCompatActivity {

    private TextView tv_date,tv_credit_cash,tv_endof_pcash,tv_daystore_cash,tv_noregister,tv_nooftills,tv_cashbalance_till_disperse,
            tv_start_tillcash,tv_storebalance_after_till_deis,tv_ebd_tillcasg,tv_till_disperse_payback,tv_cash_trans_bal,
            tv_credit_trans_revenue,tv_total_gross_revenue,tv_csh_expense,tv_total_sale_balance,tv_net_sale_revenue,tv_store_cash,
            tv_cash_bank_deposit,tv_end_cash;
    private GetDailyReport getDailyReport;
    private ImageView btn_back;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_daily_report);

        tv_date=findViewById(R.id.tv_date);
        tv_credit_cash=findViewById(R.id.tv_credit_cash);
        tv_endof_pcash=findViewById(R.id.tv_endof_pcash);
        tv_daystore_cash=findViewById(R.id.tv_daystore_cash);
        tv_noregister=findViewById(R.id.tv_noregister);
        tv_nooftills=findViewById(R.id.tv_nooftills);
        tv_cashbalance_till_disperse=findViewById(R.id.tv_cashbalance_till_disperse);
        tv_start_tillcash=findViewById(R.id.tv_start_tillcash);
        tv_storebalance_after_till_deis=findViewById(R.id.tv_storebalance_after_till_deis);
        tv_ebd_tillcasg=findViewById(R.id.tv_ebd_tillcasg);
        tv_till_disperse_payback=findViewById(R.id.tv_till_disperse_payback);
        tv_cash_trans_bal=findViewById(R.id.tv_cash_trans_bal);
        tv_credit_trans_revenue=findViewById(R.id.tv_credit_trans_revenue);
        tv_total_gross_revenue=findViewById(R.id.tv_total_gross_revenue);
        tv_csh_expense=findViewById(R.id.tv_csh_expense);
        tv_total_sale_balance=findViewById(R.id.tv_total_sale_balance);
        tv_net_sale_revenue=findViewById(R.id.tv_net_sale_revenue);
        tv_store_cash=findViewById(R.id.tv_store_cash);
        tv_cash_bank_deposit=findViewById(R.id.tv_cash_bank_deposit);
        tv_end_cash=findViewById(R.id.tv_end_cash);
        btn_back=findViewById(R.id.btn_back);

        getDailyReport = new Gson().fromJson(getIntent().getStringExtra("dailyexp"), GetDailyReport.class);

        tv_date.setText(""+getDailyReport.getJdate());
        tv_credit_cash.setText(""+getDailyReport.getCredit_cash());
        tv_endof_pcash.setText(""+getDailyReport.getEnd_of_previous_day_cash());
        tv_daystore_cash.setText(""+getDailyReport.getStart_of_day_cash());
        tv_noregister.setText(""+getDailyReport.getNo_of_tills_reg());
        tv_nooftills.setText(""+getDailyReport.getNo_of_tills_reg());
        tv_cashbalance_till_disperse.setText(""+getDailyReport.getNo_of_tills_active());
        tv_start_tillcash.setText(""+getDailyReport.getStarting_till_cash());
        tv_storebalance_after_till_deis.setText(""+getDailyReport.getStore_cash_after_till());
        tv_ebd_tillcasg.setText(""+getDailyReport.getEnding_till_cash());
        tv_till_disperse_payback.setText(""+getDailyReport.getTill_disperse_payback());
        tv_cash_trans_bal.setText(""+getDailyReport.getCash_transactions_revenue());
        tv_credit_trans_revenue.setText(""+getDailyReport.getCredit_card_transactions());
        tv_total_gross_revenue.setText(""+getDailyReport.getTotal_net_sales_revenue());
        tv_csh_expense.setText(""+getDailyReport.getExpenses());
        tv_total_sale_balance.setText(""+getDailyReport.getTotal_sales_cash_balance());
        tv_net_sale_revenue.setText(""+getDailyReport.getTotal_net_sales_revenue());
        tv_store_cash.setText(""+getDailyReport.getTotal_store_cash());
        tv_cash_bank_deposit.setText(""+getDailyReport.getCash_bank_deposit());
        tv_end_cash.setText(""+getDailyReport.getEnd_of_day_cash());

        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

    }
}
