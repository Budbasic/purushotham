package com.budbasic.posconsole.reception.fragment

import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.os.Bundle
import android.os.StrictMode
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v7.widget.CardView
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.text.InputFilter
import android.text.InputType
import android.text.SpannableStringBuilder
import android.util.Log
import android.view.*
import android.view.animation.OvershootInterpolator
import android.widget.*
import com.budbasic.customer.global.Preferences
import com.budbasic.global.CUC

import com.budbasic.posconsole.R
import com.budbasic.posconsole.model.APIClass
import com.budbasic.posconsole.webservice.RequetsInterface
import com.google.gson.Gson
import com.kotlindemo.model.GetDrivers
import com.kotlindemo.model.GetEmployeeAppSetting
import com.kotlindemo.model.GetEmployeeLogin
import com.mikepenz.fontawesome_typeface_library.FontAwesome
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.recycler_view_fragment.view.*

import net.cachapa.expandablelayout.ExpandableLayout
import java.util.*

class RecyclerViewFragment : Fragment(), APIClass.onCallListner {
    override fun onFailed(requestCode: Int) {
    }

    override fun onSuccess(requestCode: Int) {
    }

    var mDialog: Dialog? = null
    var logindata: GetEmployeeLogin? = null
    var apiClass: APIClass? = null
    var appSettingData: GetEmployeeAppSetting? = null
    var recyclerView: RecyclerView? = null
    var employeeAdapter: EmployeeAdapter? = null
    var employeeList = ArrayList<GetDrivers>()
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val rootView = inflater.inflate(R.layout.recycler_view_fragment, container, false)
        apiClass = APIClass(context!!, this)
        appSettingData = Gson().fromJson(Preferences.getPreference(rootView!!.context, "AppSetting"), GetEmployeeAppSetting::class.java)
        logindata = Gson().fromJson(Preferences.getPreference(rootView!!.context, "logindata"), GetEmployeeLogin::class.java)
        recyclerView = rootView.findViewById(R.id.recycler_view)
        recyclerView!!.layoutManager = LinearLayoutManager(context)
        recyclerView!!.setHasFixedSize(true)
        employeeAdapter = EmployeeAdapter(context!!, recyclerView!!, object : AdapterClickListner {
            override fun onclickresetpin(status: String, empId: String) {
                if (status.equals("0")){
                    val dialogBuilder = AlertDialog.Builder(context)
                    // set message of alert dialog
                    dialogBuilder.setMessage("Are you sure you want to remove Require pin for this Employee ?")
                            // if the dialog is cancelable
                            .setCancelable(false)
                            // positive button text and action
                            .setPositiveButton("YES", DialogInterface.OnClickListener {
                                dialog, id -> dialog.cancel()
                                callEmployeeRequirepin(status,empId)


                            })
                            // negative button text and action
                            .setNegativeButton("NO", DialogInterface.OnClickListener {
                                dialog, id -> dialog.cancel()
                            })

                    // create dialog box
                    val alert = dialogBuilder.create()
                    // set title for alert dialog box
                    alert.setTitle("Confirm")
                    // show alert dialog
                    alert.show()
                }else{
                    val dialogBuilder = AlertDialog.Builder(context)
                    // set message of alert dialog
                    dialogBuilder.setMessage("Are you sure you want Require pin for this Employee ?")
                            // if the dialog is cancelable
                            .setCancelable(false)
                            // positive button text and action
                            .setPositiveButton("YES", DialogInterface.OnClickListener {
                                dialog, id -> dialog.cancel()
                                callEmployeeRequirepin(status,empId)


                            })
                            // negative button text and action
                            .setNegativeButton("NO", DialogInterface.OnClickListener {
                                dialog, id -> dialog.cancel()
                            })

                    // create dialog box
                    val alert = dialogBuilder.create()
                    // set title for alert dialog box
                    alert.setTitle("Confirm")
                    // show alert dialog
                    alert.show()
                }

            }

            override fun onClickListner(status: Int, getDrivers: GetDrivers) {
                if (status == 1) {
                    showDialogChangePin("1", getDrivers)
                } else {
                    showDialogChangePin("0", getDrivers)
                }
            }

        })
        recyclerView!!.adapter = employeeAdapter
        rootView.btn_back.setOnClickListener {
            activity!!.onBackPressed()
        }
        rootView.cv_search.setOnClickListener {
            if(employeeAdapter!=null){
                val search=rootView.edt_employee_search.text.toString()
                employeeAdapter!!.filter(search)

            }
        }
        callEmployeeSalesReport()
        return rootView
    }

    class EmployeeAdapter(var context: Context, private val recyclerView: RecyclerView, val adapterClick: AdapterClickListner)
        : RecyclerView.Adapter<EmployeeAdapter.ViewHolder>() {
        private var selectedItem = UNSELECTED
        var employeeList = ArrayList<GetDrivers>()
        var tempEmployeeList = ArrayList<GetDrivers>()
        var searchstring = ""


        init {
            this.context = context
        }
        fun filter(charText: String) {
            this.searchstring = charText
            searchstring = charText.toLowerCase(Locale.getDefault())
            employeeList.clear()
            if (charText.length == 0) {
                employeeList.addAll(tempEmployeeList)
            } else {
                for (wp in tempEmployeeList) {
                    if ("${wp.emp_first_name} ${wp.emp_last_name}".toLowerCase(Locale.getDefault()).contains(charText.toLowerCase())) {
                        employeeList.add(wp)
                    }
                }
            }
            notifyDataSetChanged()
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            val itemView = LayoutInflater.from(parent.context)
                    .inflate(R.layout.recycler_item, parent, false)
            return ViewHolder(itemView)
        }

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            holder.bind(employeeList[position], adapterClick)
        }

        fun addAll(list: ArrayList<GetDrivers>) {
            employeeList.clear()
            employeeList.addAll(list)
            tempEmployeeList.addAll(list)
        }

        override fun getItemCount(): Int {
            return employeeList.size
        }

        inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener, ExpandableLayout.OnExpansionUpdateListener {
            val expandableLayout: ExpandableLayout = itemView.findViewById(R.id.expandable_layout)
            val tv_name: TextView = itemView.findViewById(R.id.tv_name)
            val tv_mobilenumber: TextView = itemView.findViewById(R.id.tv_mobilenumber)
            val tv_phonenumber: TextView = itemView.findViewById(R.id.tv_phonenumber)
            val tv_patienttype: TextView = itemView.findViewById(R.id.tv_patienttype)
            val tv_mobileapp: TextView = itemView.findViewById(R.id.tv_mobileapp)
            val tv_loyaltygroup: TextView = itemView.findViewById(R.id.tv_loyaltygroup)
            val tv_registerat: TextView = itemView.findViewById(R.id.tv_registerat)
            val tv_status: TextView = itemView.findViewById(R.id.tv_status)
            val ll_employee_item: LinearLayout = itemView.findViewById(R.id.ll_employee_item)
            val tv_content: TextView = itemView.findViewById(R.id.tv_content)
            val cv_change_pin = itemView.findViewById<CardView>(R.id.cv_change_pin)
            val cv_change_password = itemView.findViewById<CardView>(R.id.cv_change_password)
            val expandable_arrow = itemView.findViewById<ImageView>(R.id.expandable_arrow)
            val pin_switch = itemView.findViewById<Switch>(R.id.pin_switch)
            val layout_require = itemView.findViewById<LinearLayout>(R.id.layout_require)

            init {
                expandableLayout.setInterpolator(OvershootInterpolator())
                expandableLayout.setOnExpansionUpdateListener(this)
                ll_employee_item.setOnClickListener(this)
            }

            fun bind(getDrivers: GetDrivers, adapterClick: AdapterClickListner) {
                val position = adapterPosition
                val isSelected = position == selectedItem
                //expandButton.text = position.toString() + ". Tap to expand"
                tv_name.text = "${getDrivers.emp_first_name} ${getDrivers.emp_last_name}"
                tv_mobilenumber.text = getDrivers.emp_email
                tv_patienttype.text = getDrivers.emp_driver_licence
                tv_mobileapp.text = getDrivers.emp_mobile_number
                ll_employee_item.isSelected = isSelected
                val spnable = SpannableStringBuilder()
                spnable.append(CUC.xxx("Driver Licence : ", "${getDrivers.emp_driver_licence}\n", ContextCompat.getColor(itemView.context, R.color.blue), ContextCompat.getColor(itemView.context, R.color.graylight)))
                spnable.append(CUC.xxx("Licence Expire Date : ", "${getDrivers.emp_licence_expire_date}\n", ContextCompat.getColor(itemView.context, R.color.blue), ContextCompat.getColor(itemView.context, R.color.graylight)))
                spnable.append(CUC.xxx("Employee Joining Date : ", "${getDrivers.emp_joining_date}\n", ContextCompat.getColor(itemView.context, R.color.blue), ContextCompat.getColor(itemView.context, R.color.graylight)))
                spnable.append(CUC.xxx("Employee Address : ", "${getDrivers.emp_address_one}\n", ContextCompat.getColor(itemView.context, R.color.blue), ContextCompat.getColor(itemView.context, R.color.graylight)))
                spnable.append(CUC.xxx("Employee City : ", "${getDrivers.emp_city}\n", ContextCompat.getColor(itemView.context, R.color.blue), ContextCompat.getColor(itemView.context, R.color.graylight)))
                spnable.append(CUC.xxx("Employee State : ", "${getDrivers.emp_state}\n", ContextCompat.getColor(itemView.context, R.color.blue), ContextCompat.getColor(itemView.context, R.color.graylight)))
                spnable.append(CUC.xxx("Employee Country : ", "${getDrivers.emp_country}\n", ContextCompat.getColor(itemView.context, R.color.blue), ContextCompat.getColor(itemView.context, R.color.graylight)))
                spnable.append(CUC.xxx("Employee Zipcode : ", "${getDrivers.emp_zipcode}", ContextCompat.getColor(itemView.context, R.color.blue), ContextCompat.getColor(itemView.context, R.color.graylight)))
                tv_content.text = spnable
                expandableLayout.setExpanded(isSelected, false)

                if (getDrivers.emp_requirepin.equals("0")){
                    pin_switch.isChecked=false
                }else{
                    pin_switch.isChecked=true
                }

                cv_change_pin.setOnClickListener {
                    adapterClick.onClickListner(1, getDrivers)
                }
                cv_change_password.setOnClickListener {
                    adapterClick.onClickListner(0, getDrivers)
                }
                layout_require.setOnClickListener {
                    if (getDrivers.emp_requirepin.equals("0")){
                        adapterClick.onclickresetpin("1", getDrivers.emp_id)
                    }else{
                        adapterClick.onclickresetpin("0", getDrivers.emp_id)
                    }
                }
                pin_switch.setClickable(false);

               /* pin_switch.setOnCheckedChangeListener { _, isChecked ->
                    if (isChecked){
                        pin_switch.isChecked=false
                        adapterClick.onclickresetpin(1, getDrivers.emp_id)
                        Toast.makeText(context, "Switch ON", Toast.LENGTH_SHORT).show()
                    } else {
                        pin_switch.isChecked=false
                        adapterClick.onclickresetpin(0,getDrivers.emp_id)
                        Toast.makeText(context, "Switch Off", Toast.LENGTH_SHORT).show()
                    }
                }*/
                expandable_arrow.setImageBitmap(CUC.IconicsBitmap(context, FontAwesome.Icon.faw_angle_down, ContextCompat.getColor(context, R.color.blue)))

            }


            override fun onClick(view: View) {
                val holder = recyclerView.findViewHolderForAdapterPosition(selectedItem) as? ViewHolder
                if (holder != null) {
                    holder.ll_employee_item.isSelected = false
                    holder.expandableLayout.collapse()
                }

                val position = adapterPosition
                if (position == selectedItem) {
                    selectedItem = UNSELECTED
                    expandable_arrow.setImageBitmap(CUC.IconicsBitmap(context, FontAwesome.Icon.faw_angle_down, ContextCompat.getColor(context, R.color.blue)))
                } else {
                    ll_employee_item.isSelected = true
                    expandableLayout.expand()
                    selectedItem = position
                    expandable_arrow.setImageBitmap(CUC.IconicsBitmap(context, FontAwesome.Icon.faw_angle_up, ContextCompat.getColor(context, R.color.blue)))
                }
            }

            override fun onExpansionUpdate(expansionFraction: Float, state: Int) {
                Log.d("ExpandableLayout", "State: $state")
                try {
                    if (state == ExpandableLayout.State.EXPANDING) {
                        recyclerView.smoothScrollToPosition(adapterPosition)
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }

            }
        }

        companion object {
            private val UNSELECTED = -1
        }
    }

    interface AdapterClickListner {
        fun onClickListner(status: Int, getDrivers: GetDrivers)
        fun onclickresetpin(status: String, empId: String)
    }

    lateinit var dialogChangePin: Dialog
    var employee_old_pin = ""
    var employee_new_pin = ""
    fun showDialogChangePin(status: String, getDrivers: GetDrivers) {
        dialogChangePin = Dialog(activity!!, R.style.TransparentProgressDialog)
        val policy = StrictMode.ThreadPolicy.Builder().permitAll().build()
        StrictMode.setThreadPolicy(policy)
        //dialog_changepass.window.setLayout(resources.getDimensionPixelSize(R.dimen._10sdp) ,ViewGroup.LayoutParams.WRAP_CONTENT)
        dialogChangePin.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialogChangePin.setContentView(R.layout.change_pin_dialog)
        dialogChangePin.window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)
        dialogChangePin.show()

        val cv_approve_request = dialogChangePin.findViewById<LinearLayout>(R.id.cv_approve_request)
        val cv_generate_invoice = dialogChangePin.findViewById<LinearLayout>(R.id.cv_generate_invoice)
        val edt_current_pin = dialogChangePin.findViewById<EditText>(R.id.edt_current_pin)
        val edt_new_pin = dialogChangePin.findViewById<EditText>(R.id.edt_new_pin)
        val edt_confirm_pin = dialogChangePin.findViewById<EditText>(R.id.edt_confirm_pin)
        val tv_cureent_lbl = dialogChangePin.findViewById<TextView>(R.id.tv_cureent_lbl)
        val tv_new_lbl = dialogChangePin.findViewById<TextView>(R.id.tv_new_lbl)
        val tv_confirm = dialogChangePin.findViewById<TextView>(R.id.tv_confirm)

        val ll_current = dialogChangePin.findViewById<LinearLayout>(R.id.ll_current)
        val ll_new = dialogChangePin.findViewById<LinearLayout>(R.id.ll_new)
        val ll_confirm = dialogChangePin.findViewById<LinearLayout>(R.id.ll_confirm)
        val tv_title = dialogChangePin.findViewById<TextView>(R.id.tv_title)


        ll_current.visibility = View.GONE
        ll_confirm.visibility = View.VISIBLE

        if (status == "1") {
            tv_cureent_lbl.text = "Old PIN"
            edt_current_pin.hint = "Old PIN"
            tv_new_lbl.text = "New PIN"
            edt_new_pin.hint = "New PIN"
            tv_confirm.text = "Confirm PIN"
            edt_confirm_pin.hint = "Confirm PIN"
            tv_title.text = "Change Pin"

            edt_current_pin.inputType = InputType.TYPE_CLASS_NUMBER or InputType.TYPE_NUMBER_VARIATION_PASSWORD
            edt_current_pin.filters = arrayOf(InputFilter.LengthFilter(4))

            edt_new_pin.inputType = InputType.TYPE_CLASS_NUMBER or InputType.TYPE_NUMBER_VARIATION_PASSWORD
            edt_new_pin.filters = arrayOf(InputFilter.LengthFilter(4))

            edt_confirm_pin.inputType = InputType.TYPE_CLASS_NUMBER or InputType.TYPE_NUMBER_VARIATION_PASSWORD
            edt_confirm_pin.filters = arrayOf(InputFilter.LengthFilter(4))

        } else {
            tv_cureent_lbl.text = "Old Password"
            edt_current_pin.hint = "Old Password"
            tv_new_lbl.text = "New Password"
            edt_new_pin.hint = "New Password"
            tv_confirm.text = "Confirm Password"
            edt_confirm_pin.hint = "Confirm Password"
            tv_title.text = "Change Password"

            edt_current_pin.inputType = InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PASSWORD

            edt_new_pin.inputType = InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PASSWORD

            edt_confirm_pin.inputType = InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PASSWORD
        }

        cv_approve_request.setOnClickListener {
            dialogChangePin.dismiss()
        }

        cv_generate_invoice.setOnClickListener {
            if (status == "1") {
                if (edt_new_pin.text.toString() == "") {
                    CUC.displayToast(activity!!, "0", "Enter New PIN")
                } else if (edt_confirm_pin.text.toString() == "") {
                    CUC.displayToast(activity!!, "0", getString(R.string.enter_confirm_pin))
                } else if (edt_new_pin.text.toString() != edt_confirm_pin.text.toString()) {
                    CUC.displayToast(activity!!, "0", getString(R.string.pin_do_not_match))
                } else {
                    employee_old_pin = getDrivers.emp_id
                    employee_new_pin = "${edt_new_pin.text}"
                    callEmployeePinChange()
                }
            } else {
                if (edt_new_pin.text.toString() == "") {
                    CUC.displayToast(activity!!, "0", "Enter New Password")
                } else if (edt_confirm_pin.text.toString() == "") {
                    CUC.displayToast(activity!!, "0", getString(R.string.enter_confirm_password))
                } else if (edt_new_pin.text.toString() != edt_confirm_pin.text.toString()) {
                    CUC.displayToast(activity!!, "0", getString(R.string.password_do_not_match))
                } else {
                    employee_old_pin = getDrivers.emp_id
                    employee_new_pin = "${edt_new_pin.text}"
                    callEmployeePasswordChange()
                }
            }
        }
    }

    fun callEmployeeSalesReport() {
        Log.i("Result", "callEmployeeSalesReport()")
        try {
            if (!activity!!.isFinishing) {
                mDialog = CUC.createDialog(activity!!)
                val apiService = RequetsInterface.Factory.create()
                val parameterMap = HashMap<String, String>()
                parameterMap["api_key"] = "${Preferences.getPreference(activity!!, "API_KEY")}"
                if (logindata != null) {
                    parameterMap["employee_id"] = "${logindata!!.user_id}"
                    parameterMap["employee_store_id"] = "${logindata!!.user_store_id}"
                }
                Log.i("Result", "$parameterMap")
                CompositeDisposable().add(apiService.callEmployeeStoremList(parameterMap)
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.io())
                        .subscribe({ result ->
                            if (mDialog != null && mDialog!!.isShowing)
                                mDialog!!.dismiss()
                            if (result != null) {
                                Log.i("Result", "" + result.toString())
                                if (result.status == "0") {
                                    if (result.employee != null && result.employee.size > 0) {
                                        employeeList.clear()
                                        employeeList.addAll(result.employee)
                                        employeeAdapter!!.addAll(employeeList)
                                        employeeAdapter!!.notifyDataSetChanged()
                                    } else {
                                    }
                                    if (!activity!!.isFinishing)
                                        CUC.displayToast(activity!!, result.show_status, result.message)
                                } else if (result.status == "10") {
                                    apiClass!!.callApiKey(501)
                                } else {
                                    try {
                                        if (!activity!!.isFinishing) {
                                            CUC.displayToast(activity!!, result.show_status, result.message)
                                        }
                                    } catch (e: Exception) {
                                        e.printStackTrace()
                                    }
                                }
                            } else {
                                if (!activity!!.isFinishing) {
                                    //  CUC.displayToast(activity!!, "0", getString(R.string.connection_to_database_failed))
                                }
                            }
                        }, { error ->
                            if (!activity!!.isFinishing) {
                                if (mDialog != null && mDialog!!.isShowing)
                                    mDialog!!.dismiss()
                                CUC.displayToast(activity!!, "0", getString(R.string.connection_to_database_failed))
                            }
                            error.printStackTrace()
                        })
                )
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun callEmployeeRequirepin(status: String, empId: String) {
        try {
            if (!activity!!.isFinishing) {
                mDialog = CUC.createDialog(activity!!)
                val apiService = RequetsInterface.Factory.create()
                val parameterMap = HashMap<String, String>()
                parameterMap["api_key"] = "${Preferences.getPreference(activity!!, "API_KEY")}"
                if (logindata != null) {
                    parameterMap["manager_id"] = "${logindata!!.user_id}"
                }
                parameterMap["employee_id"] = "${empId}"
                parameterMap["require"] = "${status}"

                println("aaaaaaaaaa  requirepin  "+parameterMap)
                Log.i("Result", "$parameterMap")
                CompositeDisposable().add(apiService.callEmployeeRequirepin(parameterMap)
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.io())
                        .subscribe({ result ->
                            if (mDialog != null && mDialog!!.isShowing)
                                mDialog!!.dismiss()
                            if (result != null) {
                                println("aaaaaaaaaa  requirepin  "+result)
                                Log.i("Result", "" + result.toString())
                                if (result.status == "0") {
                                   callEmployeeSalesReport()
                                    CUC.displayToast(activity!!, result.show_status, result.message)
                                } else if (result.status == "10") {
                                    apiClass!!.callApiKey(501)
                                } else {
                                    try {
                                        if (!activity!!.isFinishing) {
                                            CUC.displayToast(activity!!, result.show_status, result.message)
                                        }
                                    } catch (e: Exception) {
                                        e.printStackTrace()
                                    }
                                }
                            } else {
                                if (!activity!!.isFinishing) {
                                    //  CUC.displayToast(activity!!, "0", getString(R.string.connection_to_database_failed))
                                }
                            }
                        }, { error ->
                            if (!activity!!.isFinishing) {
                                if (mDialog != null && mDialog!!.isShowing)
                                    mDialog!!.dismiss()
                                CUC.displayToast(activity!!, "0", getString(R.string.connection_to_database_failed))
                            }
                            error.printStackTrace()
                        })
                )
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }


    fun callEmployeePinChange() {
        mDialog = CUC.createDialog(activity!!)
        val apiService = RequetsInterface.create()
        val parameterMap = HashMap<String, String>()
        parameterMap["api_key"] = "${Preferences.getPreference(activity!!, "API_KEY")}"
        parameterMap["employee_id"] = employee_old_pin
        parameterMap["manager_id"] = "${logindata!!.user_id}"
        parameterMap["newPin"] = employee_new_pin
        Log.i("Result", "parameterMap : $parameterMap")
        CompositeDisposable().add(apiService.callEmployeeStoremChangepin(parameterMap)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe({ result ->
                    if (mDialog != null && mDialog!!.isShowing)
                        mDialog!!.dismiss()
                    if (result != null) {
                        Log.i("Result", "" + result.toString())
                        if (result.status == "0") {
                            if (::dialogChangePin.isInitialized) {
                                if (dialogChangePin!!.isShowing)
                                    dialogChangePin!!.dismiss()
                            }
                            CUC.displayToast(activity!!, result.show_status, result.message)
                        } else if (result.status == "10") {
                            apiClass!!.callApiKey(2)
                        } else {
                            CUC.displayToast(activity!!, result.show_status, result.message)
                        }
                    } else {
                        // CUC.displayToast(activity!!, "0", getString(R.string.connection_to_database_failed))
                    }
                }, { error ->
                    if (mDialog != null && mDialog!!.isShowing)
                        mDialog!!.dismiss()
                    CUC.displayToast(activity!!, "0", getString(R.string.connection_to_database_failed))

                    error.printStackTrace()
                })
        )
    }

    fun callEmployeePasswordChange() {
        mDialog = CUC.createDialog(activity!!)
        val apiService = RequetsInterface.create()
        val parameterMap = HashMap<String, String>()
        parameterMap["api_key"] = "${Preferences.getPreference(activity!!, "API_KEY")}"
        parameterMap["employee_id"] = employee_old_pin
        parameterMap["manager_id"] = "${logindata!!.user_id}"
        parameterMap["newPassword"] = employee_new_pin
        Log.i("Result", "parameterMap : $parameterMap")
        CompositeDisposable().add(apiService.callEmployeeStoremChangepassword(parameterMap)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe({ result ->
                    if (mDialog != null && mDialog!!.isShowing)
                        mDialog!!.dismiss()
                    if (result != null) {
                        Log.i("Result", "" + result.toString())
                        if (result.status == "0") {
                            if (::dialogChangePin.isInitialized) {
                                if (dialogChangePin.isShowing)
                                    dialogChangePin.dismiss()
                            }
                            CUC.displayToast(activity!!, result.show_status, result.message)
                        } else if (result.status == "10") {
                            apiClass!!.callApiKey(2)
                        } else {
                            CUC.displayToast(activity!!, result.show_status, result.message)
                        }
                    } else {
                        // CUC.displayToast(activity!!, "0", getString(R.string.connection_to_database_failed))
                    }
                }, { error ->
                    if (mDialog != null && mDialog!!.isShowing)
                        mDialog!!.dismiss()
                    CUC.displayToast(activity!!, "0", getString(R.string.connection_to_database_failed))

                    error.printStackTrace()
                })
        )
    }
}