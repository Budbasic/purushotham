package com.budbasic.posconsole.reception

import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Color
import android.graphics.Point
import android.location.LocationManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.os.StrictMode
import android.provider.MediaStore
import android.provider.Settings
import android.support.design.widget.Snackbar
import android.support.v4.app.ActivityCompat
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v4.widget.CircularProgressDrawable
import android.support.v7.app.AlertDialog
import android.text.InputFilter
import android.text.InputType
import android.util.Log
import android.view.*
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.EditText
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.budbasic.customer.global.Preferences
import com.budbasic.global.CUC
import com.budbasic.posconsole.*
import com.budbasic.posconsole.dialogs.SalesQueueSearch
import com.budbasic.posconsole.dialogs.SessionTimeOutDialog
import com.budbasic.posconsole.driver.fragment.DOListingMapF
import com.budbasic.posconsole.global.*
import com.budbasic.posconsole.model.APIClass
import com.budbasic.posconsole.queue.MenuActivity
import com.budbasic.posconsole.reception.fragment.*
import com.budbasic.posconsole.sales.SalesMenagerFragment
import com.budbasic.posconsole.webservice.RequetsInterface
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DecodeFormat
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.SimpleTarget
import com.bumptech.glide.request.transition.Transition
import com.github.omadahealth.lollipin.lib.managers.AppLockActivity
import com.github.omadahealth.lollipin.lib.managers.LockManager
import com.google.firebase.iid.FirebaseInstanceId
import com.google.gson.Gson
import com.kotlindemo.model.GetEmployeeAppSetting
import com.kotlindemo.model.GetEmployeeLogin
import com.tbruyelle.rxpermissions2.RxPermissions
import de.hdodenhof.circleimageview.CircleImageView
import io.mattcarroll.hover.overlay.OverlayPermission
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.functions.Consumer
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_store_manager.*
import kotlinx.android.synthetic.main.activity_store_manager.iv_lock
import kotlinx.android.synthetic.main.activity_store_manager.tv_title
import kotlinx.android.synthetic.main.change_profile.*
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.*
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.HashMap


class StoreManagerActivity : BaseActivity(), DashboardFragment.ClickListner, APIClass.onCallListner {


    override fun isNetworkConnected(): Boolean {
        return false
    }

    override fun setUp() {
    }

    override fun onFailed(requestCode: Int) {
    }

    override fun onSuccess(requestCode: Int) {
        if (requestCode == 1) {
            callEmployeeSalesReturnOrder()
        }
    }

    override fun showImage() {
        println("aaaaaaaaaaaaaa  show image")
        ll_salesQueue.visibility=View.VISIBLE
    }
    override fun onClick(position: String) {
        when (position) {

            "PATIENT CHECK-IN" -> {
                ll_salesQueue.visibility=View.GONE
                fragment = QueueFragment()
                setFragmentWithBack(fragment!!, false, null, QueueFragment::class.java.simpleName)
            }
            "PATIENT MANAGER" -> {
                ll_salesQueue.visibility=View.GONE
                fragment = CustomerSearchFragment()
                setFragmentWithBack(fragment!!, false, null, CustomerSearchFragment::class.java.simpleName)
            }
            "ATTENDANCE" -> {
                rxPermissions!!.request(
                        android.Manifest.permission.CAMERA)
                        .subscribe(object : Consumer<Boolean> {
                            override fun accept(granted: Boolean?) {
                                if (granted!!) { // Always true pre-M
                                    // I can control the camera now
                                    val myIntent = Intent(this@StoreManagerActivity, MenuActivity::class.java)
                                    myIntent.putExtra("status", "0")
                                    startActivityForResult(myIntent, 444)
                                } else {
                                    // Oups permission denied
                                    Snackbar.make(findViewById(R.id.rl_store_main), resources.getString(R.string.permissionenable),
                                            Snackbar.LENGTH_LONG)
                                            .setActionTextColor(Color.WHITE)
                                            .setAction(resources.getString(R.string.ok), object : View.OnClickListener {
                                                override fun onClick(p0: View?) {
                                                    val intent = Intent()
                                                    intent.action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
                                                    val uri = Uri.fromParts("package", packageName, null)
                                                    intent.data = uri
                                                    startActivity(intent)
                                                }
                                            }).show()
                                }
                            }
                        })
            }

            "QUICK SALES" -> {
                ll_salesQueue.visibility=View.GONE
                fragment = ReceptionSales()
                setFragmentWithBack(fragment!!, false, null, ReceptionSales::class.java.simpleName)
            }


            "EXPENSES" -> {
                fragment = QueueFragment()
                setFragmentWithBack(fragment!!, false, null, QueueFragment::class.java.simpleName)
            }
            "DEPOSITS" -> {
                fragment = QueueFragment()
                setFragmentWithBack(fragment!!, false, null, QueueFragment::class.java.simpleName)
            }
            "FINANCIAL ANALYTICS" -> {
                fragment = QueueFragment()
                setFragmentWithBack(fragment!!, false, null, QueueFragment::class.java.simpleName)
            }
            "PRINT" -> {
                tv_title.text = "Settings"
                //fragment = SettingFragment()
                fragment = PrintPageFragment(logindata)
                addOrReplace(fragment!!, "replace")
            }
            "CASH DRAWERS" -> {
                removeFragment()
                tv_title.text = "Terminal Sales"
                //bottomViewShow(ll_menuview)
                fragment = CashDrawerFragment(logindata)
                setFragmentWithBack(fragment!!, false, null, CashDrawerFragment::class.java.simpleName)
                //  addOrReplace(fragment!!, "replace")
            }
            "REPORT" -> {
                ll_salesQueue.visibility=View.GONE
                fragment = ReportFragment()
                addOrReplace(fragment!!, "replace")
                /* removeFragment()
             tv_title.text = "Sales Reports"
             //bottomViewShow(ll_menuview)
             fragment = SalesReportFragment()
             addOrReplace(fragment!!, "replace")*/

            }
            "EMPLOYEE MANAGER" -> {
                tv_title.text = "Employee Manager"
                //bottomViewShow(ll_menuview)

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    applicationContext.startForegroundService(Intent(applicationContext, SingleSectionNotificationHoverMenuService::class.java))
                } else {
                    applicationContext.startService(Intent(applicationContext, SingleSectionNotificationHoverMenuService::class.java))
                }
                logindata = Gson().fromJson(Preferences.getPreference(applicationContext, "logindata"), GetEmployeeLogin::class.java)
                val lockManager = LockManager.getInstance()
                if (lockManager.isAppLockEnabled) {
                    lockManager.enableAppLock(applicationContext, AppLockActivity::class.java)
                    lockManager.appLock.setPasscode("${logindata!!.user_pin}")
                    val intent = Intent(applicationContext, SessionTimeOutDialog::class.java)
                    intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                    startActivityForResult(intent, 7979)
                }

               /* fragment = RecyclerViewFragment()
                setFragmentWithBack(fragment!!, false, null, RecyclerViewFragment::class.java.simpleName)*/
                //  addOrReplace(fragment!!, "replace")
            }
            "CALCULATOR" -> {
                openCalculater()
            }
            /* "SALES ANALYTICS" -> {
                 fragment = GraphReportFragment()
                 addOrReplace(fragment!!, "replace")
             }*/


            "INVOICES" -> {
                fragment = SalesReportFragment()
                setFragmentWithBack(fragment!!, false, null, SalesReportFragment::class.java.simpleName)
                // addOrReplace(fragment!!, "replace")

            }

            "STORE ANALYTICS" -> {
                fragment = GraphReportFragment()
                setFragmentWithBack(fragment!!, false, null, GraphReportFragment::class.java.simpleName)
                // addOrReplace(fragment!!, "replace")
            }

            "PRINT LABEL" ->/* val mDialog = EmployeeSearchProducts(this@StoreManagerActivity)
            mDialog.show(fragmentManager, "product")*/ {
                tv_title.text = "Label Printer"
                rxPermissions!!.request(
                        android.Manifest.permission.CAMERA)
                        .subscribe(object : Consumer<Boolean> {
                            override fun accept(granted: Boolean?) {
                                if (granted!!) { // Always true pre-M
                                    // I can control the camera now
                                    startActivity(Intent(this@StoreManagerActivity, ProductSearch::class.java))
                                } else {
                                    // Oups permission denied
                                    Snackbar.make(findViewById(R.id.rl_store_main), resources.getString(R.string.permissionenable),
                                            Snackbar.LENGTH_LONG)
                                            .setActionTextColor(Color.WHITE)
                                            .setAction(resources.getString(R.string.ok), object : View.OnClickListener {
                                                override fun onClick(p0: View?) {
                                                    val intent = Intent()
                                                    intent.action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
                                                    val uri = Uri.fromParts("package", packageName, null)
                                                    intent.data = uri
                                                    startActivity(intent)
                                                }
                                            }).show()
                                }
                            }
                        })
            }
            "PRODUCT INFO" -> {
                tv_title.text = "Label Printer"
                rxPermissions!!.request(
                        android.Manifest.permission.CAMERA)
                        .subscribe(object : Consumer<Boolean> {
                            override fun accept(granted: Boolean?) {
                                if (granted!!) { // Always true pre-M
                                    // I can control the camera now
                                    startActivity(Intent(this@StoreManagerActivity, ProductSearch::class.java))
                                } else {
                                    // Oups permission denied
                                    Snackbar.make(findViewById(R.id.rl_store_main), resources.getString(R.string.permissionenable),
                                            Snackbar.LENGTH_LONG)
                                            .setActionTextColor(Color.WHITE)
                                            .setAction(resources.getString(R.string.ok), object : View.OnClickListener {
                                                override fun onClick(p0: View?) {
                                                    val intent = Intent()
                                                    intent.action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
                                                    val uri = Uri.fromParts("package", packageName, null)
                                                    intent.data = uri
                                                    startActivity(intent)
                                                }
                                            }).show()
                                }
                            }
                        })
              //  val mDialog = EmployeeSearchProducts(this@StoreManagerActivity)
              //  mDialog.show(fragmentManager, "Product Info")
            }
            "DELIVERY" -> {
                tv_title.text = "Driver Returns"
                rxPermissions!!.request(
                        android.Manifest.permission.CAMERA)
                        .subscribe(object : Consumer<Boolean> {
                            override fun accept(granted: Boolean?) {
                                if (granted!!) { // Always true pre-M
                                    // I can control the camera now
                                    startActivityForResult(Intent(this@StoreManagerActivity, ScanQRCodeActivity::class.java), 444)
                                } else {
                                    // Oups permission denied
                                    Snackbar.make(findViewById(R.id.rl_store_main), resources.getString(R.string.permissionenable),
                                            Snackbar.LENGTH_LONG)
                                            .setActionTextColor(Color.WHITE)
                                            .setAction(resources.getString(R.string.ok), object : View.OnClickListener {
                                                override fun onClick(p0: View?) {
                                                    val intent = Intent()
                                                    intent.action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
                                                    val uri = Uri.fromParts("package", packageName, null)
                                                    intent.data = uri
                                                    startActivity(intent)
                                                }
                                            }).show()
                                }
                            }
                        })
            }
            "SALES QUEUE" -> {
                ll_salesQueue.visibility=View.GONE
                tv_title.text = "Sales Queue"
                fragment = SalesMenagerFragment()
                setFragmentWithBack(fragment!!, false, null, SalesMenagerFragment::class.java.simpleName)
                //   addOrReplace(fragment!!, "replace")
            }
            "ONLINE ORDERS" -> {
                tv_title.text = "Backroom Manager"
               // fragment = EmployeeOrdersFragment()
                fragment = OnLineOrdersFragment(logindata)
                setFragmentWithBack(fragment!!, false, null, OnLineOrdersFragment::class.java.simpleName)
                // addOrReplace(fragment!!, "replace")
            }
            "RECEPTIONIST" -> {
                ll_salesQueue.visibility=View.GONE
                fragment = ReceptionFragment()
                addOrReplace(fragment!!, "replace")
            }

            "FINANCIALS" -> {
                fragment = FinancialFragment(logindata)
                addOrReplace(fragment!!, "replace")
            }



            else -> {

            }
        }
    }


    lateinit var customDialog: Dialog
    private fun openCalculater() {
        customDialog = Dialog(this@StoreManagerActivity, R.style.TransparentProgressDialog)
        val policy = StrictMode.ThreadPolicy.Builder().permitAll().build()
        StrictMode.setThreadPolicy(policy)
        customDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        customDialog.setContentView(R.layout.calculator_item)
        customDialog.window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)
        customDialog.show()

        val tv_basic = customDialog.findViewById<TextView>(R.id.tv_basic)
        val tv_unit = customDialog.findViewById<TextView>(R.id.tv_unit)
        val tv_cancel = customDialog.findViewById<TextView>(R.id.tv_cancel)
        tv_cancel.setOnClickListener {
            customDialog.dismiss()
        }

        tv_unit.setOnClickListener {
            val dialog = CalcWindowUnit(this@StoreManagerActivity.iv_lock)
            dialog.show()
            customDialog.dismiss()
        }
        tv_basic.setOnClickListener {
            val dialog = CalcWindowBasic(this@StoreManagerActivity.iv_lock)
            dialog.show()
            customDialog.dismiss()
        }
    }

    lateinit var mDialog: Dialog

    var fragment: Fragment? = null
    var bottomUp: Animation? = null
    var bottomDown: Animation? = null
    var mAppBasic: GetEmployeeAppSetting? = null
    val REQUEST_CODE_GPS = 0x02
    var rxPermissions: RxPermissions? = null
    var device_id = ""
    lateinit var locationManager: LocationManager

    var store_code = ""
    var apiClass: APIClass? = null
    var to = ""
    var imageCompanyFile: File? = null
    private val REQUEST_CODE_HOVER_PERMISSION = 1000
    var uri: Uri? = null
    internal var bitmap: Bitmap? = null

    private var mPermissionsRequested = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Log.i("!@#CheckResult", "onCreate(), called...")
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN)
        window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN or WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN)
        val policy = StrictMode.ThreadPolicy.Builder().permitAll().build()
        StrictMode.setThreadPolicy(policy)
        setContentView(R.layout.activity_store_manager)

        val tv_cal = findViewById<CircleImageView>(R.id.calc_id)
        tv_cal.setOnClickListener {
            openCalculater()
        }
        sales_info.setOnClickListener {
            val mDialog = SalesQueueSearch(this@StoreManagerActivity)
            mDialog.show(fragmentManager, "Sales Queue")
        }

        product_info.setOnClickListener {
            val mDialog = EmployeeSearchProducts(this@StoreManagerActivity)
            mDialog.show(fragmentManager, "Product Info")
        }

        apiClass = APIClass(this@StoreManagerActivity, this)
        rxPermissions = RxPermissions(this)
        locationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager
        if (intent != null && intent.hasExtra("to")) {
            to = intent.getStringExtra("to")
        }
        FirebaseInstanceId.getInstance().instanceId.addOnSuccessListener(this@StoreManagerActivity) { instanceIdResult ->
            val mToken = instanceIdResult!!.token
            Log.i("MyToken", mToken)
        }
//        val startHoverIntent = Intent(this@StoreManagerActivity, SingleSectionNotificationHoverMenuService::class.java)
//        startService(startHoverIntent)
        init()
    }

    override fun onResume() {
        super.onResume()
        ll_salesQueue.visibility=View.VISIBLE
        if (!mPermissionsRequested && !OverlayPermission.hasRuntimePermissionToDrawOverlay(this@StoreManagerActivity)) {
            @SuppressWarnings("NewApi")
            val myIntent = OverlayPermission.createIntentToRequestOverlayPermission(this@StoreManagerActivity)
            startActivityForResult(myIntent, REQUEST_CODE_HOVER_PERMISSION)
        }

    }

    override fun onUserInteraction() {
        super.onUserInteraction()
        Log.i("StoreManager", "onUserInteraction()...")
    }

    override fun onStart() {
        super.onStart()
        Log.i("!@#CheckResult", "onStart(), called...")
        if (to == "splash") {
            to = ""
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                applicationContext.startForegroundService(Intent(applicationContext, SingleSectionNotificationHoverMenuService::class.java))
            } else {
                applicationContext.startService(Intent(applicationContext, SingleSectionNotificationHoverMenuService::class.java))
            }
            logindata = Gson().fromJson(Preferences.getPreference(applicationContext, "logindata"), GetEmployeeLogin::class.java)
            if (logindata!!.user_role == "2") {
                val lockManager = LockManager.getInstance()
                lockManager.enableAppLock(applicationContext, AppLockActivity::class.java)
                lockManager.appLock.setPasscode("${logindata!!.user_pin}")
                val intent = Intent(applicationContext, SessionTimeOutDialog::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TOP
                startActivityForResult(intent, 7575)
            }

        }
    }

    override fun onStop() {
        super.onStop()
        Log.i("!@#CheckResult", "onStop(), called...")
    }

    override fun onBackPressed() {
        //super.onBackPressed()
        val fragments = supportFragmentManager.backStackEntryCount

        if (fragments == 0) {
            val builder = AlertDialog.Builder(this)
            builder.setMessage(getString(R.string.are_you_sure_you_want_to_exit))

            builder.setPositiveButton("Yes") { dialog, which ->
                dialog.dismiss()
                finish()
            }

            builder.setNegativeButton("No") { dialog, which -> dialog.dismiss() }
            val alert = builder.create()
            alert.show()
        } else {
            super.onBackPressed()
        }
    }

    var p1 = Point()
    override fun onWindowFocusChanged(hasFocus: Boolean) {
        super.onWindowFocusChanged(hasFocus)
        val location = IntArray(2)
        // Get the x, y location and store it in the location[] array
        // location[0] = x, location[1] = y.
        ll_loginuser3.getLocationOnScreen(location)
        //Initialize the Point with x, and y positions
        p1 = Point()
        p1.x = location[0]
        p1.y = location[1]
    }

    fun init() {
        getDeviceId()
        mAppBasic = Gson().fromJson(Preferences.getPreference(this@StoreManagerActivity, "AppSetting"), GetEmployeeAppSetting::class.java)
        logindata = Gson().fromJson(Preferences.getPreference(this@StoreManagerActivity, "logindata"), GetEmployeeLogin::class.java)
        //tv_user_login.text = logindata.user_show_name
        bottomUp = AnimationUtils.loadAnimation(this@StoreManagerActivity, R.anim.slide_in_left)
        bottomDown = AnimationUtils.loadAnimation(this@StoreManagerActivity, R.anim.slide_out_left)

        if (mAppBasic != null) {
            if (mAppBasic!!.company_image != null && mAppBasic!!.company_image != "") {
                val circularProgressDrawable = CircularProgressDrawable(this@StoreManagerActivity)
                circularProgressDrawable.strokeWidth = 5f
                circularProgressDrawable.centerRadius = 30f
                circularProgressDrawable.start()
                Glide.with(this@StoreManagerActivity)
                        .applyDefaultRequestOptions(RequestOptions().format(DecodeFormat.PREFER_ARGB_8888).placeholder(circularProgressDrawable).error(R.drawable.logo_top))
                        .load("${mAppBasic!!.company_image}")
                        .into(iv_storelogo)
            }
        }

        Log.d("LoginUserRole", " = " + logindata.user_role)
        if (logindata.user_role == "2") {
            ll_dashboard.visibility = View.VISIBLE
            ll_create_patient.visibility = View.VISIBLE
            ll_backroom_manager.visibility = View.VISIBLE
            ll_driver.visibility = View.GONE
            ll_sales_manager.visibility = View.VISIBLE
            ll_employee_list.visibility = View.VISIBLE
            ll_calc.visibility = View.VISIBLE
            ll_reports.visibility = View.VISIBLE

            ll_create_financial.visibility=View.VISIBLE

            ll_sales_reports.visibility = View.VISIBLE
            ll_graph_reports.visibility = View.VISIBLE
            ll_terminal_sales.visibility = View.VISIBLE
            ll_queue_manager.visibility = View.GONE
            ll_returnpackage.visibility = View.VISIBLE
            ll_productprinter.visibility = View.VISIBLE
            ll_productinfo.visibility = View.VISIBLE
            ll_setting.visibility = View.VISIBLE
            iv_lock.visibility = View.VISIBLE

            tv_title.text = "Dashboard"
            if (fragment == null) {
                fragment = DashboardFragment()
                (fragment as DashboardFragment).setListner(this)
                addOrReplace(fragment!!, "add")
            }
        } else if (logindata.user_role == "3") {
            ll_dashboard.visibility = View.GONE
            ll_create_patient.visibility = View.GONE
            ll_backroom_manager.visibility = View.GONE
            ll_driver.visibility = View.VISIBLE
            ll_calc.visibility = View.VISIBLE
            ll_employee_list.visibility = View.GONE
            ll_reports.visibility = View.VISIBLE
            ll_sales_reports.visibility = View.VISIBLE
            ll_graph_reports.visibility = View.VISIBLE
            ll_terminal_sales.visibility = View.GONE
            ll_sales_manager.visibility = View.GONE
            ll_queue_manager.visibility = View.GONE
            ll_returnpackage.visibility = View.GONE
            ll_productinfo.visibility = View.GONE
            ll_salesQueue.visibility = View.GONE

            ll_productprinter.visibility = View.GONE
            ll_setting.visibility = View.GONE


            // Clickble false
            iv_home.isEnabled = false

            rxPermissions!!.request(
                    android.Manifest.permission.ACCESS_FINE_LOCATION)
                    .subscribe(object : Consumer<Boolean> {
                        override fun accept(granted: Boolean?) {
                            if (granted!!) { // Always true pre-M
                                // I can control the camera now
                                tv_title.text = "Driver"
                                if (fragment == null) {
                                    fragment = DOListingMapF()
                                    addOrReplace(fragment!!, "add")
                                }
                            } else {
                                // Oups permission denied
                                Snackbar.make(findViewById(R.id.rl_store_main), resources.getString(R.string.permissionenable),
                                        Snackbar.LENGTH_LONG)
                                        .setActionTextColor(Color.WHITE)
                                        .setAction(resources.getString(R.string.ok), object : View.OnClickListener {
                                            override fun onClick(p0: View?) {
                                                val intent = Intent()
                                                intent.action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
                                                val uri = Uri.fromParts("package", packageName, null)
                                                intent.data = uri
                                                startActivity(intent)
                                            }
                                        }).show()
                            }
                        }
                    })
        } else if (logindata.user_role == "4") {
            ll_dashboard.visibility = View.GONE
            ll_create_patient.visibility = View.VISIBLE
            ll_backroom_manager.visibility = View.GONE
            ll_driver.visibility = View.GONE
            ll_calc.visibility = View.VISIBLE
            ll_employee_list.visibility = View.GONE
            ll_reports.visibility = View.VISIBLE
            ll_sales_reports.visibility = View.VISIBLE
            ll_graph_reports.visibility = View.VISIBLE
            ll_terminal_sales.visibility = View.GONE
            ll_sales_manager.visibility = View.GONE
            ll_queue_manager.visibility = View.GONE
            ll_returnpackage.visibility = View.GONE
            ll_productinfo.visibility = View.GONE
            ll_productprinter.visibility = View.GONE
            ll_setting.visibility = View.VISIBLE

//            tv_title.text = "Patient Manager"
//            if (fragment == null) {
//                fragment = CustomerSearchFragment()
//                addOrReplace(fragment!!, "add")
//            }

            tv_title.text = "Dashboard"
            if (fragment == null) {
                fragment = DashboardFragment()
                (fragment as DashboardFragment).setListner(this)
                addOrReplace(fragment!!, "add")
            }

        } else if (logindata.user_role == "7") {
            ll_dashboard.visibility = View.GONE
            ll_create_patient.visibility = View.GONE
            ll_backroom_manager.visibility = View.GONE
            ll_driver.visibility = View.GONE
            ll_calc.visibility = View.VISIBLE
            ll_employee_list.visibility = View.GONE
            ll_reports.visibility = View.VISIBLE
            ll_sales_reports.visibility = View.VISIBLE
            ll_graph_reports.visibility = View.VISIBLE
            ll_terminal_sales.visibility = View.GONE
            ll_sales_manager.visibility = View.VISIBLE
            ll_queue_manager.visibility = View.GONE
            ll_returnpackage.visibility = View.GONE
            ll_productinfo.visibility = View.GONE
            ll_productprinter.visibility = View.GONE
            ll_setting.visibility = View.VISIBLE


//            tv_title.text = "Sales Manager"
//            if (fragment == null) {
//                fragment = SalesMenagerFragment()
//                setFragmentWithBack(fragment!!, false, null, SalesMenagerFragment::class.java.simpleName)
//                //   addOrReplace(fragment!!, "add")
//            }


            tv_title.text = "Dashboard"
            if (fragment == null) {
                fragment = DashboardFragment()
                (fragment as DashboardFragment).setListner(this)
                addOrReplace(fragment!!, "add")
            }

        }
//        else if (logindata.user_role == "8") {
//            ll_dashboard.visibility = View.GONE
//            ll_create_patient.visibility = View.GONE
//            ll_backroom_manager.visibility = View.GONE
//            ll_driver.visibility = View.GONE
//            ll_sales_manager.visibility = View.GONE
//            ll_queue_manager.visibility = View.VISIBLE
//            ll_returnpackage.visibility = View.GONE
//            ll_productprinter.visibility = View.GONE
//            ll_setting.visibility = View.GONE
//
//            rxPermissions!!.request(
//                    android.Manifest.permission.CAMERA)
//                    .subscribe(object : Consumer<Boolean> {
//                        override fun accept(granted: Boolean?) {
//                            if (granted!!) { // Always true pre-M
//                                // I can control the camera now
//                                val myIntent = Intent(this@StoreManagerActivity, MenuActivity::class.java)
//                                myIntent.putExtra("status", "0")
//                                startActivityForResult(myIntent, 444)
//                            } else {
//                                // Oups permission denied
//                                Snackbar.make(findViewById(R.id.rl_store_main), resources.getString(R.string.permissionenable),
//                                        Snackbar.LENGTH_LONG)
//                                        .setActionTextColor(Color.WHITE)
//                                        .setAction(resources.getString(R.string.ok), object : View.OnClickListener {
//                                            override fun onClick(p0: View?) {
//                                                val intent = Intent()
//                                                intent.action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
//                                                val uri = Uri.fromParts("package", packageName, null)
//                                                intent.data = uri
//                                                startActivity(intent)
//                                            }
//
//
//                                        }).show()
//                            }
//                        }
//                    })
//        }
        else if (logindata.user_role == "9") {
            ll_dashboard.visibility = View.GONE
            ll_create_patient.visibility = View.GONE
            ll_backroom_manager.visibility = View.VISIBLE
            ll_driver.visibility = View.GONE
            ll_calc.visibility = View.VISIBLE
            ll_employee_list.visibility = View.GONE
            ll_reports.visibility = View.VISIBLE
            ll_sales_reports.visibility = View.VISIBLE
            ll_graph_reports.visibility = View.VISIBLE
            ll_terminal_sales.visibility = View.GONE
            ll_sales_manager.visibility = View.GONE
            ll_queue_manager.visibility = View.GONE
            ll_returnpackage.visibility = View.GONE
            ll_productinfo.visibility = View.GONE
            ll_productprinter.visibility = View.GONE
            ll_setting.visibility = View.VISIBLE

//            tv_title.text = "Backroom Manager"
//            if (fragment == null) {
//                fragment = EmployeeOrdersFragment()
//                setFragmentWithBack(fragment!!, false, null, EmployeeOrdersFragment::class.java.simpleName)
//                //  addOrReplace(fragment!!, "add")
//            }


            tv_title.text = "Dashboard"
            if (fragment == null) {
                fragment = DashboardFragment()
                (fragment as DashboardFragment).setListner(this)
                addOrReplace(fragment!!, "add")
            }

        } else {
            ll_dashboard.visibility = View.GONE
            ll_create_patient.visibility = View.GONE
            ll_backroom_manager.visibility = View.GONE
            ll_driver.visibility = View.GONE
            ll_calc.visibility = View.VISIBLE
            ll_employee_list.visibility = View.GONE
            ll_reports.visibility = View.VISIBLE
            ll_sales_reports.visibility = View.VISIBLE
            ll_graph_reports.visibility = View.VISIBLE
            ll_terminal_sales.visibility = View.GONE
            ll_sales_manager.visibility = View.GONE
            ll_queue_manager.visibility = View.GONE
            ll_returnpackage.visibility = View.GONE
            ll_productinfo.visibility = View.GONE
            ll_productprinter.visibility = View.GONE
            ll_setting.visibility = View.GONE
            CUC.displayToast(this@StoreManagerActivity, "0", "Invalid Role")
        }

        iv_lock.setOnClickListener {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                applicationContext.startForegroundService(Intent(applicationContext, SingleSectionNotificationHoverMenuService::class.java))
            } else {
                applicationContext.startService(Intent(applicationContext, SingleSectionNotificationHoverMenuService::class.java))
            }
            logindata = Gson().fromJson(Preferences.getPreference(applicationContext, "logindata"), GetEmployeeLogin::class.java)
            val lockManager = LockManager.getInstance()
            if (lockManager.isAppLockEnabled) {
                lockManager.enableAppLock(applicationContext, AppLockActivity::class.java)
                lockManager.appLock.setPasscode("${logindata!!.user_pin}")
                val intent = Intent(applicationContext, SessionTimeOutDialog::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                startActivityForResult(intent, 7575)
            }
        }

        ll_calc_basic.setOnClickListener {
            val dialog = CalcWindowBasic(this@StoreManagerActivity.iv_lock)
            dialog.show()
        }

        ll_calc_unit.setOnClickListener {
            val dialog = CalcWindowUnit(this@StoreManagerActivity.iv_lock)
            dialog.show()
        }
        iv_home.setOnClickListener {
            tv_title.text = "Dashboard"
            //bottomViewShow(ll_menuview)
            ll_salesQueue.visibility=View.VISIBLE
            removeFragment()
            fragment = DashboardFragment()
            (fragment as DashboardFragment).setListner(this)
            addOrReplace(fragment!!, "replace")
        }

        ll_patient.setOnClickListener {
            if (!el_patient.isExpanded) {
                el_patient.expand()
            } else {
                el_patient.collapse()
            }
        }
        ll_dashboard.setOnClickListener {
            tv_title.text = "Dashboard"
            //bottomViewShow(ll_menuview)
            removeFragment()
            fragment = DashboardFragment()
            (fragment as DashboardFragment).setListner(this)
            addOrReplace(fragment!!, "replace")
        }
        tv_patient_manager.setOnClickListener {
            if (CUC.isClickEnable()) {
                removeFragment()
                tv_title.text = "Patient Manager"
                //bottomViewShow(ll_menuview)
                fragment = CustomerSearchFragment()
                addOrReplace(fragment!!, "replace")
            }
        }

        ll_sales_reports.setOnClickListener {
            removeFragment()
            tv_title.text = "Sales Reports"
            //bottomViewShow(ll_menuview)
            fragment = SalesReportFragment()
            setFragmentWithBack(fragment!!, false, null, SalesReportFragment::class.java.simpleName)
            //addOrReplace(fragment!!, "replace")
        }
        //graph reports
        ll_graph_reports.setOnClickListener {
            removeFragment()
            tv_title.text = "Graph Reports"
            fragment = GraphReportFragment()
            addOrReplace(fragment!!, "replace")

        }
        ll_employee_list.setOnClickListener {
            tv_title.text = "Employee Manager"
            //bottomViewShow(ll_menuview)
           // fragment = RecyclerViewFragment()
            //addOrReplace(fragment!!, "replace")
        }

        ll_terminal_sales.setOnClickListener {
            removeFragment()
            tv_title.text = "cash drawers"
            //bottomViewShow(ll_menuview)
            fragment = TerminalSalesFragment()
            setFragmentWithBack(fragment!!, false, null, TerminalSalesFragment::class.java.simpleName)
            //   addOrReplace(fragment!!, "replace")
        }
        llQueue.setOnClickListener {
            if (CUC.isClickEnable()) {
                removeFragment()
                tv_title.text = "Queue Manager"
                //bottomViewShow(ll_menuview)
                fragment = QueueFragment()
                addOrReplace(fragment!!, "replace")
            }
        }

        llSales.setOnClickListener {
            if (CUC.isClickEnable()) {
                removeFragment()
                tv_title.text = "Reception Sales"
                //bottomViewShow(ll_menuview)
                fragment = ReceptionSales()
                addOrReplace(fragment!!, "replace")
            }
        }



        ll_create_financial.setOnClickListener {
            if (CUC.isClickEnable()) {
                removeFragment()
                tv_title.text = "Reception Sales"
                //bottomViewShow(ll_menuview)
                fragment = ReceptionSales()
                addOrReplace(fragment!!, "replace")
            }
        }



        ll_calc.setOnClickListener {
            if (exoLayout.isExpanded) {
                exoLayout.collapse()
            } else {
                exoLayout.expand()
            }
        }
        //for reports expnadble
        ll_reports.setOnClickListener {
            if (exoLayout1.isExpanded) {
                exoLayout1.collapse()
            } else {
                exoLayout1.expand()
            }
        }
        ll_productinfo.setOnClickListener {
            val mDialog = EmployeeSearchProducts(this@StoreManagerActivity)
            mDialog.show(fragmentManager, "product")
        }

        tv_attendance.setOnClickListener {
            rxPermissions!!.request(
                    android.Manifest.permission.CAMERA)
                    .subscribe(object : Consumer<Boolean> {
                        override fun accept(granted: Boolean?) {
                            if (granted!!) { // Always true pre-M
                                // I can control the camera now
                                val myIntent = Intent(this@StoreManagerActivity, MenuActivity::class.java)
                                myIntent.putExtra("status", "0")
                                startActivityForResult(myIntent, 444)
                            } else {
                                // Oups permission denied
                                Snackbar.make(findViewById(R.id.rl_store_main), resources.getString(R.string.permissionenable),
                                        Snackbar.LENGTH_LONG)
                                        .setActionTextColor(Color.WHITE)
                                        .setAction(resources.getString(R.string.ok), object : View.OnClickListener {
                                            override fun onClick(p0: View?) {
                                                val intent = Intent()
                                                intent.action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
                                                val uri = Uri.fromParts("package", packageName, null)
                                                intent.data = uri
                                                startActivity(intent)
                                            }
                                        }).show()
                            }
                        }
                    })
        }

        ll_backroom_manager.setOnClickListener {
            if (CUC.isClickEnable()) {
                removeFragment()
                tv_title.text = "Backroom Manager"
               // fragment = EmployeeOrdersFragment()
                fragment = OnLineOrdersFragment(logindata)
                setFragmentWithBack(fragment!!, false, null, OnLineOrdersFragment::class.java.simpleName)
                //   addOrReplace(fragment as EmployeeOrdersFragment, "replace")
            }
        }

        ll_driver.setOnClickListener {
            rxPermissions!!.request(
                    android.Manifest.permission.ACCESS_FINE_LOCATION)
                    .subscribe(object : Consumer<Boolean> {
                        override fun accept(granted: Boolean?) {
                            if (granted!!) { // Always true pre-M
                                // I can control the camera now
                                callDriverFra()
                            } else {
                                // Oups permission denied
                                Snackbar.make(findViewById(R.id.rl_store_main), resources.getString(R.string.permissionenable),
                                        Snackbar.LENGTH_LONG)
                                        .setActionTextColor(Color.WHITE)
                                        .setAction(resources.getString(R.string.ok), object : View.OnClickListener {
                                            override fun onClick(p0: View?) {
                                                val intent = Intent()
                                                intent.action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
                                                val uri = Uri.fromParts("package", packageName, null)
                                                intent.data = uri
                                                startActivity(intent)
                                            }
                                        }).show()
                            }
                        }
                    })
        }

        ll_returnpackage.setOnClickListener {
            rxPermissions!!.request(
                    android.Manifest.permission.CAMERA)
                    .subscribe(object : Consumer<Boolean> {
                        override fun accept(granted: Boolean?) {
                            if (granted!!) { // Always true pre-M
                                // I can control the camera now
                                startActivityForResult(Intent(this@StoreManagerActivity, ScanQRCodeActivity::class.java), 444)
                            } else {
                                // Oups permission denied
                                Snackbar.make(findViewById(R.id.rl_store_main), resources.getString(R.string.permissionenable),
                                        Snackbar.LENGTH_LONG)
                                        .setActionTextColor(Color.WHITE)
                                        .setAction(resources.getString(R.string.ok), object : View.OnClickListener {
                                            override fun onClick(p0: View?) {
                                                val intent = Intent()
                                                intent.action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
                                                val uri = Uri.fromParts("package", packageName, null)
                                                intent.data = uri
                                                startActivity(intent)
                                            }
                                        }).show()
                            }
                        }
                    })
        }

        ll_productprinter.setOnClickListener {
            rxPermissions!!.request(
                    android.Manifest.permission.CAMERA)
                    .subscribe(object : Consumer<Boolean> {
                        override fun accept(granted: Boolean?) {
                            if (granted!!) { // Always true pre-M
                                // I can control the camera now
                                startActivity(Intent(this@StoreManagerActivity, ProductSearch::class.java))
                            } else {
                                // Oups permission denied
                                Snackbar.make(findViewById(R.id.rl_store_main), resources.getString(R.string.permissionenable),
                                        Snackbar.LENGTH_LONG)
                                        .setActionTextColor(Color.WHITE)
                                        .setAction(resources.getString(R.string.ok), object : View.OnClickListener {
                                            override fun onClick(p0: View?) {
                                                val intent = Intent()
                                                intent.action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
                                                val uri = Uri.fromParts("package", packageName, null)
                                                intent.data = uri
                                                startActivity(intent)
                                            }
                                        }).show()
                            }
                        }
                    })
        }

        ll_sales_manager.setOnClickListener {
            if (CUC.isClickEnable()) {
                removeFragment()
                tv_title.text = "Sales Manager"
                //bottomViewShow(ll_menuview)
                fragment = SalesMenagerFragment()
                setFragmentWithBack(fragment!!, false, null, SalesMenagerFragment::class.java.simpleName)
                // addOrReplace(fragment!!, "replace")
            }
        }

        ll_setting.setOnClickListener {
            if (CUC.isClickEnable()) {
                removeFragment()
                tv_title.text = "Settings"
                //bottomViewShow(ll_menuview)
               // fragment = SettingFragment()
                fragment = PrintPageFragment(logindata)
                addOrReplace(fragment!!, "replace")
            }
        }

        ll_loginuser3.setOnClickListener {
            val dialog = UPDialog(ll_loginuser3, object : UPDialog.onClickListener {
                override fun callchangeprofile() {
                    showChangeprofile()
                }

                override fun callChangePin() {
                    showDialogChangePin("1")
                }

                override fun callChangePassword() {
                    showDialogChangePin("0")
                }

                override fun callLogout() {
                    val builder = AlertDialog.Builder(this@StoreManagerActivity, R.style.MyAlertDialogStyle)
                    builder.setTitle(resources.getString(R.string.app_name))
                    builder.setMessage("Are you sure?\nDo you want to sign out?")
                    builder.setCancelable(false)
                    this@StoreManagerActivity.setFinishOnTouchOutside(false)
                    builder.setPositiveButton("YES") { dialog, which ->
                        val Logout_intent = Intent(this@StoreManagerActivity, LoginActivity::class.java)
                        Logout_intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_CLEAR_TOP
                        Preferences.removePreference(this@StoreManagerActivity, "logindata")
                        this@StoreManagerActivity.finish()
                        startActivity(Logout_intent)
                    }
                    builder.setNegativeButton("NO",
                            { arg0, arg1 -> arg0.dismiss() })
                    builder.show()
                }

            })
            dialog.show()
        }


       ll_loginuser2.setOnClickListener{
            val dialog = ChatDialog(ll_loginuser2, object : ChatDialog.onClickListener {

                override fun callChangePin() {
                    showDialogChangePin("1")
                }

                override fun callChangePassword() {
                    showDialogChangePin("0")
                }

                override fun callLogout() {
                    val builder = AlertDialog.Builder(this@StoreManagerActivity, R.style.MyAlertDialogStyle)
                    this@StoreManagerActivity.setFinishOnTouchOutside(false)



                }


            })

           dialog.show()
       }
    }

    fun callDriverFra() {
        if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            if (CUC.isClickEnable()) {
                removeFragment()
                tv_title.text = "Driver"
                fragment = DOListingMapF()
                addOrReplace(fragment!!, "replace")
            }

        } else {
            showGPSDisabledAlertToUser()
        }
    }

    private fun showGPSDisabledAlertToUser() {
        val alertDialogBuilder = android.app.AlertDialog.Builder(this)
        alertDialogBuilder.setMessage("GPS is disabled in your device. Would you like to enable it?")
                .setCancelable(false)
                .setPositiveButton("Settings"
                ) { dialog, id ->
                    val callGPSSettingIntent = Intent(
                            Settings.ACTION_LOCATION_SOURCE_SETTINGS)
                    startActivityForResult(callGPSSettingIntent, REQUEST_CODE_GPS)
                }
        alertDialogBuilder.setNegativeButton("Cancel") { dialog, id ->
            dialog.cancel()
            finish()
        }
        val alert = alertDialogBuilder.create()
        alert.setCanceledOnTouchOutside(false)
        alert.show()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        Log.i("MainActivity", "onActivityResult() $requestCode")
        for (fm in supportFragmentManager.fragments) {
            fm.onActivityResult(requestCode, resultCode, data)
        }
        if (requestCode == REQUEST_CODE_GPS) {
            println("GPS CALL")
            callDriverFra()
        } else if (requestCode == 444) {
            println("aaaaaaaaa  data  "+data)
            if (data != null) {
                var patient_dob = data.getStringExtra("patient_dob")
                var driving_license = data.getStringExtra("driving_license")
                var Name = data.getStringExtra("Name")
                var lName = data.getStringExtra("lName")
                var Postal = data.getStringExtra("Postal")
                var state_code = data.getStringExtra("state_code")
                var City = data.getStringExtra("City")
                var Timestamp = data.getStringExtra("Timestamp")
                callAppqEmployeeTracking(driving_license)
            }
        } else if (REQUEST_CODE_HOVER_PERMISSION == requestCode) {
            mPermissionsRequested = true
        }else if (requestCode == 7979) {
            fragment = RecyclerViewFragment()
            //fragment = EmployeeManagerFragment()
           // fragment = EmployeeManagerFragment()
            setFragmentWithBack(fragment!!, false, null, RecyclerViewFragment::class.java.simpleName)
        }else if (requestCode==1){
            var selectedImageUri: Uri? = null
            var filePath: String? = null

            selectedImageUri = data!!.data

            val selectedImage = data.data
            uri = data.data
            dialogChangeProfile.img_profile.setImageURI(selectedImage)
            //decodeFile(uri.getPath());
            if (selectedImageUri != null) {
                try {
                    // OI FILE Manager
                    val filemanagerstring = selectedImageUri.path

                    // MEDIA GALLERY
                    val selectedImagePath = getPath(selectedImageUri)

                    if (selectedImagePath != null) {
                        filePath = selectedImagePath
                    } else if (filemanagerstring != null) {
                        filePath = filemanagerstring
                    } else {

                    }

                    if (filePath != null) {
                        decodeFile(filePath)

                    } else {
                        bitmap = null
                    }
                } catch (e: Exception) {
                    Log.e(e.javaClass.name, e.message, e)
                }

            }
        }else if (requestCode==CAMERA_CAPTURE_IMAGE_REQUEST_CODE){
            val selectedImage = data?.data
            uri = data?.data
            println("aaaaaaaaaaa  uri  "+data +"   "+myDir.toString())
            if (data != null) {
                onCaptureImageResult(data)
            }
            /*var f = File(myDir.toString())
            for (temp in f.listFiles()!!) {
                if (temp.name == "$timeStamp.jpg") {
                    f = temp
                    break
                }
            }
            try {
                decodeFile(f.absolutePath)
                //val file = File(myDir, f.name)

            } catch (e: Exception) {
                e.printStackTrace()
            }*/
        }

        else {
            super.onActivityResult(requestCode, resultCode, data)
        }
    }
    private fun onCaptureImageResult(data: Intent) {
        val thumbnail = data.extras!!.get("data") as Bitmap
        val bytes = ByteArrayOutputStream()
        thumbnail.compress(Bitmap.CompressFormat.JPEG, 90, bytes)

        val destination = File(Environment.getExternalStorageDirectory(),
                System.currentTimeMillis().toString() + ".jpg")

        val fo: FileOutputStream
        try {
            destination.createNewFile()
            fo = FileOutputStream(destination)
            fo.write(bytes.toByteArray())
            fo.close()
        } catch (e: FileNotFoundException) {
            e.printStackTrace()
        } catch (e: IOException) {
            e.printStackTrace()
        }

        imageCompanyFile = destination
        println("aaaaaaaaaaa   file  $destination")
        dialogChangeProfile.img_profile.setImageBitmap(thumbnail)

    }
    fun getPath(uri: Uri): String? {
        val projection = arrayOf(MediaStore.Images.Media.DATA)
        val cursor = this!!.managedQuery(uri, projection, null, null, null)
        if (cursor != null) {
            // HERE YOU WILL GET A NULLPOINTER IF CURSOR IS NULL
            // THIS CAN BE, IF YOU USED OI FILE MANAGER FOR PICKING THE MEDIA
            val column_index = cursor!!
                    .getColumnIndexOrThrow(MediaStore.Images.Media.DATA)
            cursor!!.moveToFirst()
            return cursor!!.getString(column_index)
        } else
            return null
    }
    fun decodeFile(filePath: String?) {
        // Decode image size
        val o = BitmapFactory.Options()
        o.inJustDecodeBounds = true
        BitmapFactory.decodeFile(filePath, o)

        // The new size we want to scale to
        val REQUIRED_SIZE = 512

        // Find the correct scale value. It should be the power of 2.
        var width_tmp = o.outWidth
        var height_tmp = o.outHeight
        var scale = 1
        while (true) {
            if (width_tmp < REQUIRED_SIZE && height_tmp < REQUIRED_SIZE)
                break
            width_tmp /= 2
            height_tmp /= 2
            scale *= 2
        }

        // Decode with inSampleSize
        val o2 = BitmapFactory.Options()
        o2.inSampleSize = scale
        bitmap = BitmapFactory.decodeFile(filePath, o2)

        Glide.with(this).asBitmap().load(bitmap).apply(RequestOptions()).into(object : SimpleTarget<Bitmap>() {
            override fun onResourceReady(resource: Bitmap, transition: Transition<in Bitmap>?) {
                if (resource != null) {
                    val timeStamp = SimpleDateFormat("yyyy.MM.dd.HH.mm.ss", Locale.US).format(Date())
                    imageCompanyFile = persistImage(resource, timeStamp)

                    uri = Uri.fromFile(imageCompanyFile)
                    dialogChangeProfile.img_profile.setImageURI(uri)

                }
            }
        })
    }
    fun persistImage(bitmap: Bitmap, name: String): File {

        val filesDir: File = this.filesDir
        val imageFile: File = File(filesDir, name + ".jpeg");

        var os: OutputStream
        try {
            os = FileOutputStream(imageFile);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, os);
            os.flush();
            os.close();
        } catch (e: Exception) {
            Log.e(javaClass.getSimpleName(), "Error writing bitmap", e);
        }

        return imageFile
    }
    fun bottomViewShow(footer: View) {
        if (footer.visibility == View.VISIBLE) {
            if (bottomDown != null) {
                footer.startAnimation(bottomDown)
                footer.visibility = View.VISIBLE
                window.setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE, WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
                bottomDown!!.setAnimationListener(object : Animation.AnimationListener {
                    override fun onAnimationStart(animation: Animation) {

                    }

                    override fun onAnimationEnd(animation: Animation) {
                        footer.visibility = View.GONE
                        //Toast.makeText(ParticipateActivity.this, "Hide View.", Toast.LENGTH_LONG).show();
                        window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
                    }

                    override fun onAnimationRepeat(animation: Animation) {

                    }
                })
            }

        } else {
            if (bottomUp != null) {
                footer.startAnimation(bottomUp)
                footer.visibility = View.INVISIBLE
                window.setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE, WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
                bottomUp!!.setAnimationListener(object : Animation.AnimationListener {
                    override fun onAnimationStart(animation: Animation) {

                    }

                    override fun onAnimationEnd(animation: Animation) {
                        footer.visibility = View.VISIBLE
                        //Toast.makeText(ParticipateActivity.this, "Show View.", Toast.LENGTH_LONG).show();
                        window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
                    }

                    override fun onAnimationRepeat(animation: Animation) {

                    }
                })
            }

        }
    }

    fun addOrReplace(fragment: Fragment, text: String) {
        if (!isFinishing) {
            val fragmentManager = supportFragmentManager
            //fragmentManager.popBackStack()
            val transaction = supportFragmentManager.beginTransaction()
            if (text == "add")
                transaction.add(R.id.tvcontainer, fragment)
            else
                transaction.replace(R.id.tvcontainer, fragment)
            transaction.setCustomAnimations(R.anim.slide_in_right, 0)
            transaction.commit()
        }
    }

    fun setFragmentWithBack(fragment: Fragment, adddOrReplace: Boolean, mBundle: Bundle?, mFragmentTag: String) {
        val fragmentManager = supportFragmentManager
        val transaction = supportFragmentManager.beginTransaction()

        if (mBundle != null) {
            fragment.arguments = mBundle
        }

        if (adddOrReplace) {
            transaction.add(R.id.tvcontainer, fragment, mFragmentTag)
        } else {
            transaction.replace(R.id.tvcontainer, fragment, mFragmentTag)
        }

        transaction.addToBackStack("back")
        transaction.commitAllowingStateLoss()
    }

    fun addOrReplaceSearch(fragment: Fragment, text: String) {
        if (!isFinishing) {
            val transaction = supportFragmentManager.beginTransaction()
            if (text == "add")
                transaction.add(R.id.tvcontainer, fragment)
            else
                transaction.replace(R.id.tvcontainer, fragment)
            transaction.setCustomAnimations(R.anim.slide_in_right, 0)
            transaction.addToBackStack("back")
            transaction.commit()
        }
    }

    fun removeFragment() {
        try {
            val fm = supportFragmentManager
            for (i in 0 until fm.backStackEntryCount) {
                fm.popBackStack()
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun callEmployeeSalesReturnOrder() {
        mDialog = CUC.createDialog(this@StoreManagerActivity)
        val apiService = RequetsInterface.create()
        val parameterMap = HashMap<String, String>()
        parameterMap["api_key"] = "${Preferences.getPreference(this@StoreManagerActivity, "API_KEY")}"
        parameterMap["employee_id"] = "${logindata.user_id}"
        parameterMap["invoice_uniq_no"] = "$store_code"
        parameterMap["terminal_id"] = "${mAppBasic!!.terminal_id}"
        parameterMap["emp_lat"] = "${Preferences.getPreference(this@StoreManagerActivity, "latitude")}"
        parameterMap["emp_log"] = "${Preferences.getPreference(this@StoreManagerActivity, "longitude")}"
        Log.i("Result", "parameterMap : $parameterMap")
        CompositeDisposable().add(apiService.callEmployeeSalesReturnOrder(parameterMap)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe({ result ->
                    if (mDialog != null && mDialog.isShowing)
                        mDialog.dismiss()
                    if (result != null) {
                        Log.i("Result", "" + result.toString())
                        if (result.status == "0") {
                            CUC.displayToast(this@StoreManagerActivity, result.show_status, result.message)
                        } else if (result.status == "10") {
                            apiClass!!.callApiKey(1)
                        } else {
                            CUC.displayToast(this@StoreManagerActivity, result.show_status, result.message)
                        }
                    } else {
                        // CUC.displayToast(this@StoreManagerActivity, "0", getString(R.string.connection_to_database_failed))
                    }
                }, { error ->
                    if (mDialog != null && mDialog.isShowing)
                        mDialog.dismiss()
                    CUC.displayToast(this@StoreManagerActivity, "0", getString(R.string.connection_to_database_failed))

                    error.printStackTrace()
                })
        )
    }

    lateinit var dialogChangePin: Dialog
    lateinit var dialogChangeProfile: Dialog
    var employee_old_pin = ""
    var employee_new_pin = ""
    fun showDialogChangePin(status: String) {
        dialogChangePin = Dialog(this@StoreManagerActivity, R.style.TransparentProgressDialog)
        val policy = StrictMode.ThreadPolicy.Builder().permitAll().build()
        StrictMode.setThreadPolicy(policy)
        //dialog_changepass.window.setLayout(resources.getDimensionPixelSize(R.dimen._10sdp) ,ViewGroup.LayoutParams.WRAP_CONTENT)
        dialogChangePin.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialogChangePin.setContentView(R.layout.change_pin_dialog)
        dialogChangePin.window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)
        dialogChangePin.show()

        val cv_approve_request = dialogChangePin.findViewById<LinearLayout>(R.id.cv_approve_request)
        val cv_generate_invoice = dialogChangePin.findViewById<LinearLayout>(R.id.cv_generate_invoice)
        val edt_current_pin = dialogChangePin.findViewById<EditText>(R.id.edt_current_pin)
        val edt_new_pin = dialogChangePin.findViewById<EditText>(R.id.edt_new_pin)
        val edt_confirm_pin = dialogChangePin.findViewById<EditText>(R.id.edt_confirm_pin)
        val tv_cureent_lbl = dialogChangePin.findViewById<TextView>(R.id.tv_cureent_lbl)
        val tv_new_lbl = dialogChangePin.findViewById<TextView>(R.id.tv_new_lbl)
        val ll_confirm = dialogChangePin.findViewById<LinearLayout>(R.id.ll_confirm)
        val tv_confirm = dialogChangePin.findViewById<TextView>(R.id.tv_confirm)
        val tv_title = dialogChangePin.findViewById<TextView>(R.id.tv_title)

        ll_confirm.visibility = View.VISIBLE
        if (status == "1") {
            tv_cureent_lbl.text = "Old PIN"
            edt_current_pin.hint = "Old PIN"
            tv_new_lbl.text = "New PIN"
            edt_new_pin.hint = "New PIN"
            tv_confirm.text = "Confirm PIN"
            edt_confirm_pin.hint = "Confirm PIN"
            tv_title.text = "Change Pin"

            edt_current_pin.inputType = InputType.TYPE_CLASS_NUMBER or InputType.TYPE_NUMBER_VARIATION_PASSWORD
            edt_current_pin.filters = arrayOf(InputFilter.LengthFilter(4))

            edt_new_pin.inputType = InputType.TYPE_CLASS_NUMBER or InputType.TYPE_NUMBER_VARIATION_PASSWORD
            edt_new_pin.filters = arrayOf(InputFilter.LengthFilter(4))

            edt_confirm_pin.inputType = InputType.TYPE_CLASS_NUMBER or InputType.TYPE_NUMBER_VARIATION_PASSWORD
            edt_confirm_pin.filters = arrayOf(InputFilter.LengthFilter(4))
        } else {
            tv_cureent_lbl.text = "Old Password"
            edt_current_pin.hint = "Old Password"
            tv_new_lbl.text = "New Password"
            edt_new_pin.hint = "New Password"
            tv_confirm.text = "Confirm Password"
            edt_confirm_pin.hint = "Confirm Password"
            tv_title.text = "Change Password"

            edt_current_pin.inputType = InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PASSWORD

            edt_new_pin.inputType = InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PASSWORD

            edt_confirm_pin.inputType = InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PASSWORD
        }

        cv_approve_request.setOnClickListener {
            dialogChangePin.dismiss()
        }

        cv_generate_invoice.setOnClickListener {
            if (status == "1") {
                if (edt_current_pin.text.toString() == "") {
                    CUC.displayToast(this@StoreManagerActivity, "0", "Please Enter Your Current PIN")
                } else if (edt_new_pin.text.toString() == "") {
                    CUC.displayToast(this@StoreManagerActivity, "0", "Please Enter Your New PIN")
                } else if (edt_new_pin.text.toString() != edt_confirm_pin.text.toString()) {
                    CUC.displayToast(this@StoreManagerActivity, "0", getString(R.string.pin_do_not_match))
                } else {
                    employee_old_pin = "${edt_current_pin.text}"
                    employee_new_pin = "${edt_new_pin.text}"
                    callEmployeePinChange()
                }
            } else {
                if (edt_current_pin.text.toString() == "") {
                    CUC.displayToast(this@StoreManagerActivity, "0", "Please Enter Your Current Password")
                } else if (edt_new_pin.text.toString() == "") {
                    CUC.displayToast(this@StoreManagerActivity, "0", "Please Enter Your New Password")
                } else if (edt_new_pin.text.toString() != edt_confirm_pin.text.toString()) {
                    CUC.displayToast(this@StoreManagerActivity, "0", getString(R.string.password_do_not_match))
                } else {
                    employee_old_pin = "${edt_current_pin.text}"
                    employee_new_pin = "${edt_new_pin.text}"
                    callEmployeePasswordChange()
                }
            }
        }
    }

    fun callEmployeePinChange() {
        mDialog = CUC.createDialog(this@StoreManagerActivity)
        val apiService = RequetsInterface.create()
        val parameterMap = HashMap<String, String>()
        parameterMap["api_key"] = "${Preferences.getPreference(this@StoreManagerActivity, "API_KEY")}"
        parameterMap["employee_id"] = "${logindata.user_id}"
        parameterMap["employee_old_pin"] = employee_old_pin
        parameterMap["employee_new_pin"] = employee_new_pin
        Log.i("Result", "parameterMap : $parameterMap")
        CompositeDisposable().add(apiService.callEmployeePinChange(parameterMap)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe({ result ->
                    if (mDialog != null && mDialog.isShowing)
                        mDialog.dismiss()
                    if (result != null) {
                        Log.i("Result", "" + result.toString())
                        if (result.status == "0") {
                            if (::dialogChangePin.isInitialized) {
                                if (dialogChangePin.isShowing)
                                    dialogChangePin.dismiss()
                            }
                            CUC.displayToast(this@StoreManagerActivity, result.show_status, result.message)
                        } else if (result.status == "10") {
                            apiClass!!.callApiKey(2)
                        } else {
                            CUC.displayToast(this@StoreManagerActivity, result.show_status, result.message)
                        }
                    } else {
                        //     CUC.displayToast(this@StoreManagerActivity, "0", getString(R.string.connection_to_database_failed))
                    }
                }, { error ->
                    if (mDialog != null && mDialog.isShowing)
                        mDialog.dismiss()
                    CUC.displayToast(this@StoreManagerActivity, "0", getString(R.string.connection_to_database_failed))

                    error.printStackTrace()
                })
        )
    }


    fun callEmployeePasswordChange() {
        mDialog = CUC.createDialog(this@StoreManagerActivity)
        val apiService = RequetsInterface.create()
        val parameterMap = HashMap<String, String>()
        parameterMap["api_key"] = "${Preferences.getPreference(this@StoreManagerActivity, "API_KEY")}"
        parameterMap["employee_id"] = "${logindata.user_id}"
        parameterMap["employee_old_pass"] = employee_old_pin
        parameterMap["employee_new_pass"] = employee_new_pin
        Log.i("Result", "parameterMap : $parameterMap")
        CompositeDisposable().add(apiService.callEmployeePasswordChange(parameterMap)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe({ result ->
                    if (mDialog != null && mDialog.isShowing)
                        mDialog.dismiss()
                    if (result != null) {
                        Log.i("Result", "" + result.toString())
                        if (result.status == "0") {
                            if (::dialogChangePin.isInitialized) {
                                if (dialogChangePin.isShowing)
                                    dialogChangePin.dismiss()
                            }
                            CUC.displayToast(this@StoreManagerActivity, result.show_status, result.message)
                        } else if (result.status == "10") {
                            apiClass!!.callApiKey(2)
                        } else {
                            CUC.displayToast(this@StoreManagerActivity, result.show_status, result.message)
                        }
                    } else {
                        //  CUC.displayToast(this@StoreManagerActivity, "0", getString(R.string.connection_to_database_failed))
                    }
                }, { error ->
                    if (mDialog != null && mDialog.isShowing)
                        mDialog.dismiss()
                    CUC.displayToast(this@StoreManagerActivity, "0", getString(R.string.connection_to_database_failed))

                    error.printStackTrace()
                })
        )
    }


    fun callAppqEmployeeTracking(drivingLicense: String) {
        mDialog = CUC.createDialog(this@StoreManagerActivity)
        val apiService = RequetsInterface.create()
        val parameterMap = HashMap<String, String>()
        parameterMap["api_key"] = "${Preferences.getPreference(this@StoreManagerActivity, "API_KEY")}"
        parameterMap["licence_no"] = drivingLicense
        parameterMap["login_user"] = "${logindata.user_id}"
        println("aaaaaaaaa   attendence  "+parameterMap)
        Log.i("Result", "parameterMap : $parameterMap")
        CompositeDisposable().add(apiService.callAppqEmployeeTracking(parameterMap)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe({ result ->
                    if (mDialog != null && mDialog.isShowing)
                        mDialog.dismiss()
                    if (result != null) {
                        println("aaaaaaaaa   attendence result "+result.toString())
                        // Log.d("AAAAA111", "AAAA111")
                        Log.i("Result", "" + result.toString())
                        if (result.status == "0") {
                            if (result.image != null && result.image != "") {
                                /*  Glide.with(this@StoreManagerActivity)
                                          .applyDefaultRequestOptions(RequestOptions().format(DecodeFormat.PREFER_ARGB_8888).error(R.drawable.no_image_available))
                                          .load("${result.image}")
                                          .into(img_msg)*/
                            }
//                            tv_msg.text = result.message
//                            mHandler.postDelayed(mRunable, 10000)
//                            startBlinkAnim()
                            showdialog(result.message)
                        } else if (result.status == "10") {
                           // apiClass!!.callApiKey(APPQEMPLOYEETRACKING)
                        } else {
                            if (result.image != null && result.image != "") {
                                /*Glide.with(this@MessageActivity)
                                        .applyDefaultRequestOptions(RequestOptions().format(DecodeFormat.PREFER_ARGB_8888).error(R.drawable.no_image_available))
                                        .load("${result.image}")
                                        .into(img_msg)*/
                            }
//                            tv_msg.text = result.message
//                            mHandler.postDelayed(mRunable, 10000)
//                            startBlinkAnim()
                            showdialog(result.message)

                            // CommonUtils.displayToast(this@MessageActivity, result.show_status, result.message)
                        }
                    } else {
                        //   CUC.displayToast(this@MessageActivity, "0", getString(R.string.connection_to_database_failed))
                    }
                }, { error ->
                    if (mDialog != null && mDialog.isShowing)
                        mDialog.dismiss()
                    CUC.displayToast(this@StoreManagerActivity, "0", getString(R.string.connection_to_database_failed))

                    error.printStackTrace()
                })
        )
    }

    fun showdialog(msg:String) {
        val builder1 = android.app.AlertDialog.Builder(this@StoreManagerActivity)
        builder1.setMessage("" + msg)
        builder1.setCancelable(true)

        builder1.setPositiveButton(
                "Ok"
        ) { dialog, id ->
            dialog.cancel()

        }

        val alert11 = builder1.create()
        alert11.show()
    }

    fun showChangeprofile() {
        dialogChangeProfile = Dialog(this@StoreManagerActivity, R.style.TransparentProgressDialog)
        val policy = StrictMode.ThreadPolicy.Builder().permitAll().build()
        StrictMode.setThreadPolicy(policy)
        //dialog_changepass.window.setLayout(resources.getDimensionPixelSize(R.dimen._10sdp) ,ViewGroup.LayoutParams.WRAP_CONTENT)
        dialogChangeProfile.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialogChangeProfile.setContentView(R.layout.change_profile)
        dialogChangeProfile.window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)
        dialogChangeProfile.show()

        val img_profile = dialogChangeProfile.findViewById<ImageView>(R.id.img_profile)
        val cv_add_photo = dialogChangeProfile.findViewById<LinearLayout>(R.id.cv_add_photo)


        Glide.with(this@StoreManagerActivity) // safer!
                .asBitmap()
                .load(mAppBasic?.emp_image)
                .into(img_profile)

        cv_add_photo.setOnClickListener {
            dialogChangeProfile.dismiss()
            if (imageCompanyFile!=null){
                callEmployeeCreatePatientWithImage()
            }

        }

        img_profile.setOnClickListener {
            pickImage()

        }

    }
    internal var timeStamp = ""
    private val CAMERA_REQUEST = 1002
    val CAMERA_CAPTURE_IMAGE_REQUEST_CODE = 5263
    private val EXTERNAL_CAMERA = 1003
    var myDir: File? = null
    fun openCamera() {
        val version = Build.VERSION.SDK_INT
        if (version >= 23) {
            if (ContextCompat.checkSelfPermission(this@StoreManagerActivity, android.Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {

                // Should we show an explanation?
                if (ActivityCompat.shouldShowRequestPermissionRationale(this@StoreManagerActivity, android.Manifest.permission.CAMERA)) {

                    //This is called if user has denied the permission before
                    //In this case I am just asking the permission again
                    ActivityCompat.requestPermissions(this@StoreManagerActivity, arrayOf(android.Manifest.permission.CAMERA), CAMERA_REQUEST)

                } else {

                    ActivityCompat.requestPermissions(this@StoreManagerActivity, arrayOf(android.Manifest.permission.CAMERA), CAMERA_REQUEST)
                }
            } else {

                if (ContextCompat.checkSelfPermission(this@StoreManagerActivity, android.Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {

                    // Should we show an explanation?
                    if (ActivityCompat.shouldShowRequestPermissionRationale(this@StoreManagerActivity, android.Manifest.permission.WRITE_EXTERNAL_STORAGE)) {

                        //This is called if user has denied the permission before
                        //In this case I am just asking the permission again
                        ActivityCompat.requestPermissions(this@StoreManagerActivity, arrayOf(android.Manifest.permission.WRITE_EXTERNAL_STORAGE), EXTERNAL_CAMERA)

                    } else {

                        ActivityCompat.requestPermissions(this@StoreManagerActivity, arrayOf(android.Manifest.permission.WRITE_EXTERNAL_STORAGE), EXTERNAL_CAMERA)
                    }
                } else {

                    val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                    startActivityForResult(intent, CAMERA_CAPTURE_IMAGE_REQUEST_CODE)

                  /*  timeStamp = SimpleDateFormat("yyyy.MM.dd.HH.mm.ss", Locale.US).format(Date())
                    val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                    val f = File(myDir, timeStamp + ".jpg")
                    println("aaaaaaaaaaaa  file   "+f)
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f))
                    startActivityForResult(intent, CAMERA_CAPTURE_IMAGE_REQUEST_CODE)*/
                }
            }

        } else {
            val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            startActivityForResult(intent, CAMERA_CAPTURE_IMAGE_REQUEST_CODE)

           /* timeStamp = SimpleDateFormat("yyyy.MM.dd.HH.mm.ss", Locale.US).format(Date())
            val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            val f = File(myDir, timeStamp + ".jpg")
            intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f))
            startActivityForResult(intent, CAMERA_CAPTURE_IMAGE_REQUEST_CODE)*/
        }

    }
    fun pickImage() {
        /*var pickPhoto = Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
        startActivityForResult(pickPhoto, 1)*/
        //openCamera()

        val dialog = Dialog(this)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        val policy = StrictMode.ThreadPolicy.Builder().permitAll().build()
        StrictMode.setThreadPolicy(policy)
        dialog.setContentView(R.layout.pic_image_dialog)
        dialog.show()
        dialog.findViewById<TextView>(R.id.tv_cancelDialog).setOnClickListener {
            dialog.dismiss()
        }
        dialog.findViewById<TextView>(R.id.tv_openGallery).setOnClickListener {
            val pickPhoto = Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
            startActivityForResult(pickPhoto, 1)
            dialog.dismiss()
        }
        dialog.findViewById<TextView>(R.id.tv_OpenCamera).setOnClickListener {
            openCamera()
            dialog.dismiss()
        }


    }

    fun callEmployeeCreatePatientWithImage() {
        mDialog = CUC.createDialog(this@StoreManagerActivity)
        val apiService = RequetsInterface.create()
        val parameterMap = java.util.HashMap<String, RequestBody>()

        val requestFile = RequestBody.create(MediaType.parse("image/jpeg"), imageCompanyFile)
        val body = MultipartBody.Part.createFormData("document_image", imageCompanyFile!!.name, requestFile)

        parameterMap["api_key"] = RequestBody.create(MediaType.parse("text/plain"), "${Preferences.getPreference(this@StoreManagerActivity, "API_KEY")}")
        parameterMap["employee_id"] = RequestBody.create(MediaType.parse("text/plain"), "${logindata.user_id}")

        println("aaaaaaaaaaaaaa  parameterMap  "+parameterMap)
        Log.i("Result", "parameterMap : $parameterMap")
        CompositeDisposable().add(apiService.callEmployeeAddphoto(parameterMap, body, RequestBody.create(MediaType.parse("text/plain"), imageCompanyFile!!.name))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe({ result ->
                    if (mDialog != null && mDialog.isShowing)
                        mDialog.dismiss()
                    if (result != null) {
                        println("aaaaaaaaaaaaaa  result image "+result.toString())
                        Log.i("Result", "result : $result")
                        if (result.status == "0") {
                            showdialog(result.message)
                            CUC.displayToast(this@StoreManagerActivity, result.show_status, result.message)
                            callEmployeeAppSetting()

                        } else if (result.status == "10") {

                        } else {
                            CUC.displayToast(this@StoreManagerActivity, result.show_status, result.message)
                        }
                    } else {
                        // CUC.displayToast(this@CreateAccountActivity, "0", getString(R.string.connection_to_database_failed))
                    }
                }, { error ->
                    if (mDialog != null && mDialog.isShowing)
                        mDialog.dismiss()
                    CUC.displayToast(this@StoreManagerActivity, "0", getString(R.string.connection_to_database_failed))
                    error.printStackTrace()
                })
        )
    }

    fun callEmployeeAppSetting() {

        val apiService = RequetsInterface.create()
        val parameterMap = java.util.HashMap<String, String>()
        parameterMap["api_key"] = "${Preferences.getPreference(this@StoreManagerActivity, "API_KEY")}"
        parameterMap["terminal_device_id"] = "$device_id"
        println("aaaaaaaaa  appsetting  "+parameterMap.toString())
        Log.i("Result", "parameterMap : $parameterMap")
        CompositeDisposable().add(apiService.callEmployeeAppSetting(parameterMap)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe({ result ->
                    if (result != null) {
                        println("aaaaaaaaa  appsetting  "+result.toString())
                        Log.i("Result", "" + result.toString())
                        if (result.status == "0") {
                            Preferences.setPreference(this@StoreManagerActivity, "AppSetting", Gson().toJson(result))
                            mAppBasic = Gson().fromJson(Preferences.getPreference(this@StoreManagerActivity, "AppSetting"), GetEmployeeAppSetting::class.java)
                            CUC.displayToast(this@StoreManagerActivity, result.show_status, result.message)
                        } else if (result.status == "10") {
                            apiClass!!.callApiKey(1)
                        } else {
                            CUC.displayToast(this@StoreManagerActivity, result.show_status, result.message)
                        }
                    } else {
                        //  CUC.displayToast(this@SplashActivity, "0", getString(R.string.connection_to_database_failed))
                    }
                }, { error ->
                    CUC.displayToast(this@StoreManagerActivity, "0", getString(R.string.connection_to_database_failed))
                    error.printStackTrace()
                })
        )
    }
    fun getDeviceId() {
        device_id = Settings.Secure.getString(this@StoreManagerActivity.contentResolver, Settings.Secure.ANDROID_ID)
        if (device_id != "") {
            callEmployeeAppSetting()
        } else {
            getDeviceId()
        }
    }
}

