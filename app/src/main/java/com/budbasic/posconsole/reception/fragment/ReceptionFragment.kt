package com.budbasic.posconsole.reception.fragment

import android.app.Activity
import android.content.Intent
import android.graphics.Color
import android.net.Uri
import android.os.Bundle
import android.provider.Settings
import android.support.design.widget.Snackbar
import android.support.v4.app.Fragment
import android.support.v7.widget.CardView
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.budbasic.posconsole.R
import com.budbasic.posconsole.global.GridSpacingItemDecoration
import com.budbasic.posconsole.queue.MenuActivity
import com.budbasic.posconsole.reception.StoreManagerActivity
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DecodeFormat
import com.bumptech.glide.request.RequestOptions
import io.reactivex.functions.Consumer


class ReceptionFragment : Fragment() {
    lateinit var context: StoreManagerActivity
    var mView: View? = null

    companion object {
        var receptionList = ArrayList<String>()
        var listner: ClickListner? = null
    }

    fun setListner(listner1: ClickListner): ClickListner {
        listner = listner1
        return listner1
    }

    /*   companion object {
           var dashList = ArrayList<String>()
           var listner: ClickListner? = null
       }


      */

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        if (view != null)
            mView = view
        else
            mView = inflater.inflate(R.layout.reception_fragment, container, false)
        initView()
        return mView
    }

    fun initView() {
        receptionList.clear()
        receptionList.add("PATIENT CHECK-IN")
        receptionList.add("PATIENT MANAGER")
        receptionList.add("ATTENDANCE")
        receptionList.add("QUICK SALES")
        val spanCount = 2 // 3 columns
        val spacing = 40 // 50px
        val includeEdge = false
        val rv_reception_item = mView!!.findViewById<RecyclerView>(R.id.rv_reception_item)
        rv_reception_item.layoutManager = GridLayoutManager(activity!!, 2, GridLayoutManager.VERTICAL, false)
        rv_reception_item.addItemDecoration(GridSpacingItemDecoration(spanCount, spacing, includeEdge))
        val receptionAdapter = ReceptionAdapter(context)
        rv_reception_item.adapter = receptionAdapter
        //   ReceptionFragment.receptionList
    }

    class ReceptionAdapter(context: StoreManagerActivity?) : RecyclerView.Adapter<ReceptionAdapter.ViewHolder>() {
        lateinit var context: StoreManagerActivity
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            val rootView = LayoutInflater.from(parent.context).inflate(R.layout.reception_item, parent, false)
            return ViewHolder(rootView)
        }

        init {
            this.context = context!!
        }

        override fun getItemCount(): Int {
            return receptionList.size
        }

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            if (position == 3) {
                Glide.with(holder.iv_icon.context)
                        .applyDefaultRequestOptions(RequestOptions().format(DecodeFormat.PREFER_ARGB_8888))
                        .load(R.drawable.tv_quicksales)
                        .into(holder.iv_icon)
                holder.tv_title.text = "${receptionList[position]}"
            } else if (position == 2) {
                Glide.with(holder.iv_icon.context)
                        .applyDefaultRequestOptions(RequestOptions().format(DecodeFormat.PREFER_ARGB_8888))
                        .load(R.drawable.tv_attandance)
                        .into(holder.iv_icon)
                holder.tv_title.text = "${receptionList[position]}"
            } else if (position == 1) {
                Glide.with(holder.iv_icon.context)
                        .applyDefaultRequestOptions(RequestOptions().format(DecodeFormat.PREFER_ARGB_8888))
                        .load(R.drawable.tv_patientmanager)
                        .into(holder.iv_icon)
                holder.tv_title.text = "${receptionList[position]}"
            } else {
                Glide.with(holder.iv_icon.context)
                        .applyDefaultRequestOptions(RequestOptions().format(DecodeFormat.PREFER_ARGB_8888))
                        .load(R.drawable.tv_patientcheckin)
                        .into(holder.iv_icon)
                holder.tv_title.text = "${receptionList[position]}"
            }
            holder.cv_reception.setOnClickListener {
                if (position == 3) {
                    context.fragment = ReceptionSales()
                    context.setFragmentWithBack(context.fragment!!,false,null,ReceptionSales::class.java.simpleName)
                  //  context.addOrReplace(context.fragment!!, "replace")
                } else if (position == 2) {
                    context.rxPermissions!!.request(
                            android.Manifest.permission.CAMERA)
                            .subscribe(object : Consumer<Boolean> {
                                override fun accept(granted: Boolean?) {
                                    if (granted!!) { // Always true pre-M
                                        // I can control the camera now
                                        val myIntent = Intent(context, MenuActivity::class.java)
                                        myIntent.putExtra("status", "0")
                                        context.startActivityForResult(myIntent, 444)
                                    } else {
                                        // Oups permission denied
                                        Snackbar.make(context.findViewById(R.id.rl_store_main), context.resources.getString(R.string.permissionenable),
                                                Snackbar.LENGTH_LONG)
                                                .setActionTextColor(Color.WHITE)
                                                .setAction(context.resources.getString(R.string.ok), object : View.OnClickListener {
                                                    override fun onClick(p0: View?) {
                                                        val intent = Intent()
                                                        intent.action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
                                                        val uri = Uri.fromParts("package", context.packageName, null)
                                                        intent.data = uri
                                                        context.startActivity(intent)
                                                    }
                                                }).show()
                                    }
                                }
                            })
                } else if (position == 1) {
                    context.fragment = CustomerSearchFragment()
                    context.setFragmentWithBack(context.fragment!!,false,null,CustomerSearchFragment::class.java.simpleName)
                  //  context.addOrReplace(context.fragment!!, "replace")
                } else {
                    context.fragment = QueueFragment()
                    context.setFragmentWithBack(context.fragment!!,false,null,QueueFragment::class.java.simpleName)
                    //context.addOrReplace(context.fragment!!, "replace")
                }
                //  listner!!.onClickReceptionList(position)
            }
        }

        class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
            val iv_icon = itemView.findViewById<ImageView>(R.id.iv_icon)
            val tv_title = itemView.findViewById<TextView>(R.id.tv_title)
            val cv_reception = itemView.findViewById<CardView>(R.id.cv_reception)

            fun bindView() {
            }
        }
    }

    interface ClickListner {
        fun onClickReceptionList(position: Int)
    }

    override fun onAttach(activity: Activity?) {
        super.onAttach(activity)
        try {
            context = (activity as StoreManagerActivity?)!!
        } catch (e: ClassCastException) {
            throw ClassCastException(activity!!.toString() + " must implement onViewSelected")
        }

    }

}


