package com.budbasic.posconsole.reception.fragment

import android.app.Activity
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.CardView
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.OrientationHelper
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DecodeFormat
import com.bumptech.glide.request.RequestOptions
import com.budbasic.posconsole.R
import com.budbasic.posconsole.reception.StoreManagerActivity

class DashboardFragment : Fragment() {

    var mView: View? = null

    lateinit var context: StoreManagerActivity


    override fun onAttach(activity: Activity?) {
        super.onAttach(activity)
        try {
            context = (activity as StoreManagerActivity?)!!
        } catch (e: ClassCastException) {
            throw ClassCastException(activity!!.toString() + " must implement onViewSelected")
        }

    }

    companion object {
        var dashList = ArrayList<String>()
        var listner: ClickListner? = null
    }

    fun setListner(listner1: ClickListner): ClickListner {
        listner = listner1
        return listner1
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        if (view != null)
            mView = view
        else
            mView = inflater.inflate(R.layout.dashboard_fragment, container, false)
        initView()
        return mView
    }

    fun initView() {
        dashList.clear()
        if (context.logindata.user_role == "2") {
            dashList.add("RECEPTIONIST")//0
            dashList.add("ONLINE ORDERS")//1
            dashList.add("SALES QUEUE")//2
            dashList.add("DELIVERY")//3
            //dashList.add("PRODUCT INFO")//4
            dashList.add("INVOICES")//5.1.
            dashList.add("STORE ANALYTICS")
            //    dashList.add("SALES ANALYTICS")//5

            dashList.add("FINANCIALS")//5.1.
            dashList.add("EMPLOYEE MANAGER")//6
            //dashList.add("REPORT")//7
           // dashList.add("CALCULATOR")//8
            dashList.add("CASH DRAWERS")//9
            dashList.add("PRINT")//10
            //dashList.add("PRINT LABEL")//11
        } else if (context.logindata.user_role == "3") {
            dashList.add("RECEPTIONIST")//0
            dashList.add("ONLINE ORDERS")//1
            dashList.add("SALES QUEUE")//2
            dashList.add("DELIVERY")//3
            //dashList.add("PRODUCT INFO")//4
            dashList.add("INVOICES")//5.1.
            dashList.add("STORE ANALYTICS")
            //    dashList.add("SALES ANALYTICS")//5

            dashList.add("EMPLOYEE MANAGER")//6
            //dashList.add("REPORT")//7
            //dashList.add("CALCULATOR")//8
            dashList.add("CASH DRAWERS")//9
            dashList.add("PRINT")//10
            //dashList.add("PRINT LABEL")//11
        } else if (context.logindata.user_role == "4") {
            dashList.add("PATIENT CHECK-IN")
            dashList.add("PATIENT MANAGER")
            dashList.add("ATTENDANCE")
            dashList.add("QUICK SALES")
            //  dashList.add("RECEPTIONIST")
            //dashList.add("CALCULATOR")//8
            dashList.add("INVOICES")
            dashList.add("STORE ANALYTICS")

            dashList.add("PRINT")


            dashList.add("EXPENSES")
            dashList.add("DEPOSITS")
            dashList.add("FINANCIAL ANALYTICS")




        } else if (context.logindata.user_role == "7") {

            dashList.add("SALES QUEUE")
            //dashList.add("CALCULATOR")//8
            dashList.add("INVOICES")
            dashList.add("STORE ANALYTICS")

            dashList.add("PRINT")

        } else if (context.logindata.user_role == "9") {
            dashList.add("ONLINE ORDERS")//1
            //dashList.add("CALCULATOR")//8
            dashList.add("INVOICES")
            dashList.add("STORE ANALYTICS")

            dashList.add("PRINT")
        }

        val totalSize = dashList.size
        //var layoutManager = GridLayoutManager(context, 6)

        val rv_dashboard_item = mView!!.findViewById<RecyclerView>(R.id.rv_dashboard_item)
        var layoutManager = GridLayoutManager(activity!!, 4, OrientationHelper.VERTICAL, false)
        rv_dashboard_item.layoutManager = layoutManager
        val dashAdapter = DashboardAdapter()
        rv_dashboard_item.adapter = dashAdapter

    }

    class DashboardAdapter : RecyclerView.Adapter<DashboardAdapter.ViewHolder>() {
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            val rootView = LayoutInflater.from(parent.context).inflate(R.layout.dashboard_item, parent, false)
            return ViewHolder(rootView)
        }

        override fun getItemCount(): Int {
            return dashList.size
        }

        /*  dashList.add("PATIENT CHECK-IN")
                    dashList.add("PATIENT MANAGER")
                    dashList.add("ATTENDANCE")
                    dashList.add("QUICK SALES")*/
        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            when (dashList[position]) {
                "PATIENT CHECK-IN" -> {
                    Glide.with(holder.iv_icon.context)
                            .applyDefaultRequestOptions(RequestOptions().format(DecodeFormat.PREFER_ARGB_8888))
                            .load(R.drawable.tv_patientcheckin)
                            .into(holder.iv_icon)
                    holder.tv_title.text = "${dashList[position]}"
                }
                "PATIENT MANAGER" -> {
                    Glide.with(holder.iv_icon.context)
                            .applyDefaultRequestOptions(RequestOptions().format(DecodeFormat.PREFER_ARGB_8888))
                            .load(R.drawable.tv_patientmanager)
                            .into(holder.iv_icon)
                    holder.tv_title.text = "${dashList[position]}"
                }
                "ATTENDANCE" -> {
                    Glide.with(holder.iv_icon.context)
                            .applyDefaultRequestOptions(RequestOptions().format(DecodeFormat.PREFER_ARGB_8888))
                            .load(R.drawable.tv_attandance)
                            .into(holder.iv_icon)
                    holder.tv_title.text = "${dashList[position]}"
                }
                "QUICK SALES" -> {
                    Glide.with(holder.iv_icon.context)
                            .applyDefaultRequestOptions(RequestOptions().format(DecodeFormat.PREFER_ARGB_8888))
                            .load(R.drawable.tv_quicksales)
                            .into(holder.iv_icon)
                    holder.tv_title.text = "${dashList[position]}"
                }



                "EXPENSES" -> {
                    Glide.with(holder.iv_icon.context)
                            .applyDefaultRequestOptions(RequestOptions().format(DecodeFormat.PREFER_ARGB_8888))
                            .load(R.drawable.tv_expenses)
                            .into(holder.iv_icon)
                    holder.tv_title.text = "${dashList[position]}"
                }

                "DEPOSITS" -> {
                    Glide.with(holder.iv_icon.context)
                            .applyDefaultRequestOptions(RequestOptions().format(DecodeFormat.PREFER_ARGB_8888))
                            .load(R.drawable.tv_deposits)
                            .into(holder.iv_icon)
                    holder.tv_title.text = "${dashList[position]}"
                }
                "FINANCIAL ANALYTICS" -> {
                    Glide.with(holder.iv_icon.context)
                            .applyDefaultRequestOptions(RequestOptions().format(DecodeFormat.PREFER_ARGB_8888))
                            .load(R.drawable.tv_finacial_analytics)
                            .into(holder.iv_icon)
                    holder.tv_title.text = "${dashList[position]}"
                }


                "PRINT" -> {
                    Glide.with(holder.iv_icon.context)
                            .applyDefaultRequestOptions(RequestOptions().format(DecodeFormat.PREFER_ARGB_8888))
                            .load(R.drawable.tv_printersetting)
                            .into(holder.iv_icon)
                    //holder.iv_icon.setImageResource(R.drawable.tv_printer)
                    holder.tv_title.text = "${dashList[position]}"
                }
                "CASH DRAWERS" -> {
                    Glide.with(holder.iv_icon.context)
                            .applyDefaultRequestOptions(RequestOptions().format(DecodeFormat.PREFER_ARGB_8888))
                            .load(R.drawable.tv_terminalsales1)
                            .into(holder.iv_icon)
                    //holder.iv_icon.setImageResource(R.drawable.tv_printer)
                    holder.tv_title.text = "${dashList[position]}"
                }
                /*"SALES ANALYTICS" -> {
                    //now change
                    Glide.with(holder.iv_icon.context)
                            .applyDefaultRequestOptions(RequestOptions().format(DecodeFormat.PREFER_ARGB_8888))
                            //.load(R.drawable.tv_setting)
                            .load(R.drawable.sales_analysis)
                            .into(holder.iv_icon)
                    //holder.iv_icon.setImageResource(R.drawable.tv_printer)
                    holder.tv_title.text = "${dashList[position]}"
                }*/

                "INVOICES" -> {
                    //now change
                    Glide.with(holder.iv_icon.context)
                            .applyDefaultRequestOptions(RequestOptions().format(DecodeFormat.PREFER_ARGB_8888))
                            //.load(R.drawable.tv_setting)
                            .load(R.drawable.sales_report1)
                            .into(holder.iv_icon)
                    //holder.iv_icon.setImageResource(R.drawable.tv_printer)
                    holder.tv_title.text = "${dashList[position]}"
                }

                "STORE ANALYTICS" -> {
                    //now change
                    Glide.with(holder.iv_icon.context)
                            .applyDefaultRequestOptions(RequestOptions().format(DecodeFormat.PREFER_ARGB_8888))
                            //.load(R.drawable.tv_setting)
                            .load(R.drawable.sales_analysis)
                            .into(holder.iv_icon)
                    //holder.iv_icon.setImageResource(R.drawable.tv_printer)
                    holder.tv_title.text = "${dashList[position]}"
                }

                "EMPLOYEE MANAGER" -> {
                    Glide.with(holder.iv_icon.context)
                            .applyDefaultRequestOptions(RequestOptions().format(DecodeFormat.PREFER_ARGB_8888))
                            .load(R.drawable.tv_employeemanager)
                            .into(holder.iv_icon)
                    //holder.iv_icon.setImageResource(R.drawable.tv_queue)
                    holder.tv_title.text = "${dashList[position]}"
                }
/* "CALCULATOR" -> {
     Glide.with(holder.iv_icon.context)
             .applyDefaultRequestOptions(RequestOptions().format(DecodeFormat.PREFER_ARGB_8888))
             .load(R.drawable.calculator)
             .into(holder.iv_icon)
     //holder.iv_icon.setImageResource(R.drawable.tv_queue)
     holder.tv_title.text = "${dashList[position]}"
 }
 "PRINT LABEL" -> {
     Glide.with(holder.iv_icon.context)
             .applyDefaultRequestOptions(RequestOptions().format(DecodeFormat.PREFER_ARGB_8888))
             //.load(R.drawable.tv_product_info)
             // .load(R.drawable.tv_productinfo)
             .load(R.drawable.tv_labelprinter)
             .into(holder.iv_icon)
     //holder.iv_icon.setImageResource(R.drawable.tv_sales)
     holder.tv_title.text = "${dashList[position]}"
 }

 "PRODUCT INFO" -> {
     Glide.with(holder.iv_icon.context)
             .applyDefaultRequestOptions(RequestOptions().format(DecodeFormat.PREFER_ARGB_8888))
             //  .load(R.drawable.tv_return_cart)
             .load(R.drawable.tv_productinfo)
             .into(holder.iv_icon)
     //holder.iv_icon.setImageResource(R.drawable.tv_sales)
     holder.tv_title.text = "${dashList[position]}"
 }*/
 "DELIVERY" -> {
     Glide.with(holder.iv_icon.context)
             .applyDefaultRequestOptions(RequestOptions().format(DecodeFormat.PREFER_ARGB_8888))
             .load(R.drawable.delivery1)
             .into(holder.iv_icon)
     //holder.iv_icon.setImageResource(R.drawable.tv_queue)
     holder.tv_title.text = "${dashList[position]}"
 }
 "SALES QUEUE" -> {
     Glide.with(holder.iv_icon.context)
             .applyDefaultRequestOptions(RequestOptions().format(DecodeFormat.PREFER_ARGB_8888))
             // .load(R.drawable.tv_sales)
             .load(R.drawable.tv_salesqueue1)
             .into(holder.iv_icon)
     //holder.iv_icon.setImageResource(R.drawable.tv_sales)
     holder.tv_title.text = "${dashList[position]}"
 }
 //            else if (position == 2) {
 //                Glide.with(holder.iv_icon.context)
 //                        .applyDefaultRequestOptions(RequestOptions().format(DecodeFormat.PREFER_ARGB_8888))
 //                        .load(R.drawable.tv_driver)
 //                        .into(holder.iv_icon)
 //                //holder.iv_icon.setImageResource(R.drawable.tv_driver)
 //                holder.tv_title.text = "${dashList[position]}"
 //            }
 "ONLINE ORDERS" ->  {
     Glide.with(holder.iv_icon.context)
             .applyDefaultRequestOptions(RequestOptions().format(DecodeFormat.PREFER_ARGB_8888))
             //  .load(R.drawable.tv_backroom)
             .load(R.drawable.tv_backroommanager1)
             .into(holder.iv_icon)
     //holder.iv_icon.setImageResource(R.drawable.tv_backroom)
     holder.tv_title.text = "${dashList[position]}"
 }

 "RECEPTIONIST" -> {
     Glide.with(holder.iv_icon.context)
             .applyDefaultRequestOptions(RequestOptions().format(DecodeFormat.PREFER_ARGB_8888))
             // .load(R.drawable.tv_receptionist)
             .load(R.drawable.tv_receptionists)
             .into(holder.iv_icon)
     //holder.iv_icon.setImageResource(R.drawable.tv_receptionist)
     holder.tv_title.text = "${dashList[position]}"
 }


                "FINANCIALS" -> {
                    Glide.with(holder.iv_icon.context)
                            .applyDefaultRequestOptions(RequestOptions().format(DecodeFormat.PREFER_ARGB_8888))
                            // .load(R.drawable.tv_receptionist)
                            .load(R.drawable.tv_financial)
                            .into(holder.iv_icon)
                    //holder.iv_icon.setImageResource(R.drawable.tv_receptionist)
                    holder.tv_title.text = "${dashList[position]}"
                }





}
holder.cv_dashboard.setOnClickListener {
 listner!!.onClick(dashList[position])
}

}

class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
val iv_icon = itemView.findViewById<ImageView>(R.id.iv_icon)
val tv_title = itemView.findViewById<TextView>(R.id.tv_title)
val cv_dashboard = itemView.findViewById<CardView>(R.id.cv_dashboard)

fun bindView() {
}
}
}

interface ClickListner {
fun onClick(position: String)
}
}