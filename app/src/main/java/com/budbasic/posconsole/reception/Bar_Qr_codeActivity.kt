package com.budbasic.posconsole.reception

import android.annotation.SuppressLint
import android.app.Activity
import android.bluetooth.BluetoothAdapter
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Message
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.widget.Toast
import com.budbasic.customer.global.Preferences
import com.budbasic.global.CUC
import com.budbasic.posconsole.R
import com.budbasic.posconsole.printer.fragment.BluetoothScan
import com.budbasic.posconsole.printer.sdk.BluetoothService
import com.budbasic.posconsole.printer.sdk.Command
import com.budbasic.posconsole.printer.sdk.PrinterCommand
import com.budbasic.posconsole.webservice.RequetsInterface
import com.google.gson.Gson
import com.google.zxing.BarcodeFormat
import com.google.zxing.WriterException
import com.kotlindemo.model.GetEmployeeAppSetting
import com.kotlindemo.model.GetEmployeeLogin
import com.kotlindemo.model.GetTerminalPrinter
import com.kotlindemo.model.getprinterdata
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.bar_qr_code_layout.*
import java.io.UnsupportedEncodingException


class Bar_Qr_codeActivity : AppCompatActivity() {
    // Local Bluetooth adapter
    private var mBluetoothAdapter: BluetoothAdapter? = null
    var printerdata: GetTerminalPrinter? = null
    var getterminalprinter: ArrayList<getprinterdata>?=null
    // Member object for the services
    var mService: BluetoothService? = null
    var logindata: GetEmployeeLogin? = null
    var appSettingData: GetEmployeeAppSetting? = null
    // Intent request codes
    private val REQUEST_CONNECT_DEVICE = 1
    private val REQUEST_ENABLE_BT = 2

    // Message types sent from the BluetoothService Handler
    val MESSAGE_STATE_CHANGE = 1
    val MESSAGE_READ = 2
    val MESSAGE_WRITE = 3
    val MESSAGE_DEVICE_NAME = 4
    val MESSAGE_TOAST = 5
    val MESSAGE_CONNECTION_LOST = 6
    val MESSAGE_UNABLE_CONNECT = 7

    private val TAG = "Main_Activity"
    private val DEBUG = true

    // Key names received from the BluetoothService Handler
    val DEVICE_NAME = "device_name"
    val TOAST = "toast"
    // Name of the connected device
    private var mConnectedDeviceName: String? = null

    private var status = false
    var connecteddeviceaddress:String?=null
    var printerstatus:String?=null
    var printertype:String?=null

    private var printername: String? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.bar_qr_code_layout)

        if (intent != null && intent.hasExtra("status")) {
            status = intent.getStringExtra("status") == "1"
        }

        if (intent != null && intent.hasExtra("printername")) {
            printername=intent.getStringExtra("printername").toString();
        }
        Log.i("pname",printername);

        // Get local Bluetooth adapter
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter()
        // If the adapter is null, then Bluetooth is not supported
        if (mBluetoothAdapter == null) {
            Toast.makeText(this, "Bluetooth is not available",
                    Toast.LENGTH_LONG).show()
            //finish()
        }
        init()
    }

    fun init()
    {
        appSettingData = Gson().fromJson(Preferences.getPreference(this@Bar_Qr_codeActivity, "AppSetting"), GetEmployeeAppSetting::class.java)
        logindata = Gson().fromJson(Preferences.getPreference(this@Bar_Qr_codeActivity, "logindata"), GetEmployeeLogin::class.java)
        iv_back.setOnClickListener {
            finish()
        }
        ll_generate.setOnClickListener {




            if (edt_code_string.text.toString() == "") {
                CUC.displayToast(this@Bar_Qr_codeActivity, "0", "Enter code text")
            } else {
                if (status) {
                    try {
                        val bitmap = CUC.encodeAsBitmap("${edt_code_string.text}", BarcodeFormat.CODE_128, 600, 200)
                        iv_code_img.setImageBitmap(bitmap)
                    } catch (e: WriterException) {
                        e.printStackTrace()
                    }
                } else {
                    try {
                        val bitmap = CUC.encodeAsBitmap("${edt_code_string.text}", BarcodeFormat.QR_CODE, 400, 400)
                        iv_code_img.setImageBitmap(bitmap)
                    } catch (e: WriterException) {
                        e.printStackTrace()
                    }
                }

            }
        }

     ll_print.setOnClickListener{
            if (edt_code_string.text.toString() == "") {
                CUC.displayToast(this@Bar_Qr_codeActivity, "0", "Enter code text")
            } else {
                Log.i("pname printerconnected",mConnectedDeviceName)
             // printerselection()
                print()
            }
        }
    }

    override fun onStop() {
        super.onStop()
        if (mService != null)
            mService!!.stop()
    }

    override fun onStart() {
        super.onStart()
        // If Bluetooth is not on, request that it be enabled.
        // setupChat() will then be called during onActivityResult
        if (!mBluetoothAdapter!!.isEnabled) run {
            val enableIntent = Intent(
                    BluetoothAdapter.ACTION_REQUEST_ENABLE)
            startActivityForResult(enableIntent, REQUEST_ENABLE_BT)
            // Otherwise, setup the session
        } else {
            if (mService == null) {
                mService = BluetoothService(this, mHandler)

                mConnectedDeviceName = "";
                val address = Preferences.getPreference(this, DEVICE_NAME)
                Log.i("AddQueueActivity", "address : " + address)
                if (address != null && !address.equals("")) {
                    val mDevice = mBluetoothAdapter!!.getRemoteDevice(address)
                    connecteddeviceaddress= mDevice.name+ " "+mDevice.address
                    //BluetoothDevice mDevice= (BluetoothDevice)address;
                    Log.i("AddQueueActivity", "mDevice : " + mDevice.getAddress());
                    Log.i("AddQueueActivity", "mDevice : " + mDevice.getName());
                    Log.i("AddQueueActivity", "mDevice : " + mDevice.getUuids());
                    mService!!.connect(mDevice)
                }
            }
        }
    }


    fun print() = if (mBluetoothAdapter != null)
    {
        if (mBluetoothAdapter!!.isEnabled) {
            if (mService != null) {
                if (mService!!.getState() !== BluetoothService.STATE_CONNECTED)
                {


                    Log.i("pname printerconnected",mConnectedDeviceName)
                    Toast.makeText(this@Bar_Qr_codeActivity, getText(R.string.not_connected), Toast.LENGTH_SHORT).show()
                    this@Bar_Qr_codeActivity.setResult(Activity.RESULT_OK)
                    finish()
                } else {
                    callEmployeeTerminalPrinter()
                    /**
                     * Parameters:
                     * mBitmap  要打印的图片
                     * nWidth   打印宽度（58和80）
                     * nMode    打印模式
                     * Returns: byte[]
                     */
                    /**
                     * Parameters:
                     * mBitmap  要打印的图片
                     * nWidth   打印宽度（58和80）
                     * nMode    打印模式
                     * Returns: byte[]
                     */



                }
            } else {
//                this@Bar_Qr_codeActivity.setResult(Activity.RESULT_OK)
//                finish()
            }
        } else {
//            val enableIntent = Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE)
//            startActivityForResult(enableIntent, REQUEST_ENABLE_BT)
        }
    } else {
        Toast.makeText(this@Bar_Qr_codeActivity, "Bluetooth is not available", Toast.LENGTH_LONG).show()
    }

    private fun SendDataString(data: String) {
        Log.i("BTPWRITE", data)

        if (mService!!.getState() !== BluetoothService.STATE_CONNECTED) {
            Toast.makeText(this@Bar_Qr_codeActivity, getText(R.string.not_connected), Toast.LENGTH_SHORT)
                    .show()
            return
        }
        if (data.length > 0) {
            try {
                Log.i("BTPWRITE", data)
                mService!!.write(data.toByteArray(charset("GBK")))
            } catch (e: UnsupportedEncodingException) {
                e.printStackTrace()
            }

        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        Log.i("Result", "Main onActivityResult(), $requestCode : $resultCode")
        //fragment.onActivityResult(requestCode, resultCode, data)

        if (DEBUG)
            Log.d(TAG, "onActivityResult $resultCode")
        when (requestCode) {
            REQUEST_CONNECT_DEVICE -> {
                // When DeviceListActivity returns with a device to connect
                if (resultCode == Activity.RESULT_OK) {
                    // Get the device MAC address
                    val address = data?.getExtras()!!.getString(
                            BluetoothScan.EXTRA_DEVICE_ADDRESS)
                    // Get the BLuetoothDevice object
                    if (BluetoothAdapter.checkBluetoothAddress(address)) {
                        val device = mBluetoothAdapter!!.getRemoteDevice(address)
                        // Attempt to connect to the device
                        mService?.connect(device)
                    }
                }
            }
            REQUEST_ENABLE_BT -> {
                // When the request to enable Bluetooth returns
                if (resultCode == Activity.RESULT_OK) {
                    if (mService == null)
                        mService = BluetoothService(this, mHandler)
                    // Bluetooth is now enabled, so set up a session
                } else {
                    // User did not enable Bluetooth or an error occured
                    Log.d(TAG, "BT not enabled")
                    Toast.makeText(this, R.string.bt_not_enabled_leaving,
                            Toast.LENGTH_SHORT).show()
                    finish()
                }
            }
        }

    }

    val mHandler = @SuppressLint("HandlerLeak")
    object : Handler() {
        override fun handleMessage(msg: Message?) {
            if (msg != null) {
                when (msg.what) {
                    MESSAGE_STATE_CHANGE -> {
                        if (DEBUG)
                            Log.i(TAG, "MESSAGE_STATE_CHANGE: " + msg.arg1)
                        when (msg.arg1) {
                            BluetoothService.STATE_CONNECTED -> {
                            }
                            BluetoothService.STATE_CONNECTING -> {
                                //mTitle.setText(R.string.title_connecting)
                            }
                            BluetoothService.STATE_LISTEN, BluetoothService.STATE_NONE -> {
                                //mTitle.setText(R.string.title_not_connected)
                            }
                        }
                    }
                    MESSAGE_WRITE -> {
                    }
                    MESSAGE_READ -> {
                    }
                    MESSAGE_DEVICE_NAME -> {
                        // save the connected device's name
                        mConnectedDeviceName = msg.data.getString(DEVICE_NAME)
                        Toast.makeText(applicationContext,
                                "Connected to $mConnectedDeviceName",
                                Toast.LENGTH_SHORT).show()
                    }
                    MESSAGE_TOAST -> Toast.makeText(applicationContext,
                            msg.data.getString(TOAST), Toast.LENGTH_SHORT)
                            .show()
                    MESSAGE_CONNECTION_LOST    //蓝牙已断开连接
                    -> {
                        Toast.makeText(applicationContext, "Device connection was lost",
                                Toast.LENGTH_SHORT).show()
                    }
                    MESSAGE_UNABLE_CONNECT     //无法连接设备
                    -> Toast.makeText(applicationContext, "Unable to connect device",
                            Toast.LENGTH_SHORT).show()
                }
            }

        }
    }

    /*
     *SendDataByte
     */
    private fun SendDataByte(data: ByteArray) {
        if (mService!!.state != BluetoothService.STATE_CONNECTED) {
            Toast.makeText(this, R.string.not_connected, Toast.LENGTH_SHORT)
                    .show()
            return
        }
        mService?.write(data)
    }

    override fun onResume() {
        super.onResume()
        if (mService != null) {
            if (mService?.state == BluetoothService.STATE_NONE) {
                // Start the Bluetooth services
                mService?.start()
            }
        }
    }
    fun callEmployeeTerminalPrinter()
    {

        val apiService = RequetsInterface.create()
        val parameterMap = HashMap<String, String>()

        if (logindata != null) {
            parameterMap["api_key"] = "${Preferences.getPreference(this@Bar_Qr_codeActivity, "API_KEY")}"
            parameterMap["store_id"] = logindata!!.user_store_id
        }

        if (appSettingData != null)
        {
            //  parameterMap["store_id"]="${}"
            parameterMap["terminal_id"] = "${appSettingData!!.terminal_id}"
        }
        Log.i("Result", "parameterMapprinternew : $parameterMap")
        CompositeDisposable().add(apiService.callEmployeeTerminalPrinter(parameterMap)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe({ result ->
                    Log.i("Resultprinternew", "" + result.toString())

                            //   printerdata=  Gson().fromJson(Preferences.getPreference(applicationContext, "printerdata"),GetTerminalPrinter::class.java)
                    if(result.show_status=="1"|| result!!.message=="Available terminals") {
                        if (result.get_printers != null && result.get_printers.size > 0)
                        {
                            for(i  in result.get_printers.indices)
                            {
                        result.get_printers.get(i)
                            if (result.get_printers.get(i).hardware_manufacturer!!.equals(connecteddeviceaddress))
                            {
                                if (result.get_printers[i].hardware_status!!.equals("1", false))
                                {
                                    if(result.get_printers[i].hardware_type_id!!.equals("2",false))
                                    {
                                        printerstatus = result.get_printers!![0].hardware_status.toString()
                                        printertype = result.get_printers!!.get(0).hardware_type_id.toString()
                                        Log.i("printerstatusqrcode", printerstatus)
                                    }
                                }

                                    //   Toast.makeText(this@Bar_Qr_codeActivity,"statutype"+printerstatus+printertype,Toast.LENGTH_LONG).show()

                                }
                            }
                            }
                        try {
                            SendDataByte(Command.ESC_Init)

                            if (status)
                            {

                                //label printer
                                Log.i("pnamedevice", mConnectedDeviceName)
                                // Log.i("pname selectin",printername+ "mService"+mService.DEVICE_NAME);
                                if (printerstatus.equals("1",false) && printertype.equals("2",false))

                                {
                                    Toast.makeText(this@Bar_Qr_codeActivity, "Find Label  Printer Scessfully", Toast.LENGTH_LONG).show()
                                    Log.i("pnamedevice", mConnectedDeviceName)

                                    //Log.i("pname select",printername+mService!!.DEVICE_NAME);
                                    /*
                                * #######-----BarCode Print-----#######
                                * [START BarCode]
                                * */
                                    Command.ESC_Align[2] = 0x00
                                    SendDataByte(Command.ESC_Align)
                                    val barcodeData = edt_code_string.text.toString().toByteArray()
                                    val commands = ArrayList<ByteArray>()
                                    val command = ByteArray(barcodeData.size + 6 + 1)
                                    command[0] = 27.toByte()
                                    command[1] = 98.toByte()
                                    command[2] = 54.toByte()
                                    command[3] = 50 // option
                                    command[4] = 50 //width
                                    command[5] = 80 //height
                                    for (index in barcodeData.indices) {
                                        command[index + 6] = barcodeData[index]
                                    }
                                    command[command.size - 1] = 30.toByte()
                                    commands.add(command)

                                    for (data in commands) {
                                        SendDataByte(data)
                                    }

                                    SendDataByte("\n\n\n".toByteArray())
                                    SendDataByte(Command.LF)
                                    SendDataByte(PrinterCommand.POS_Set_PrtAndFeedPaper(2))
                                    SendDataByte(Command.ESC_d)
                                    SendDataByte(PrinterCommand.POS_Set_PrtInit())

                                } else {
                                    Toast.makeText(this@Bar_Qr_codeActivity, "Find Other Label Printer", Toast.LENGTH_LONG).show()
                                }
                                /*
                                * [END BarCode]
                                * */
                            } else {
//label printer
                                if (printerstatus.equals("1",false) && printertype.equals("2",false))
                                {
                                    Toast.makeText(this@Bar_Qr_codeActivity, "Find Label  Printer Scessfully", Toast.LENGTH_LONG).show()
                                    Command.ESC_Align[2] = 0x00
                                    SendDataByte(Command.ESC_Align)
                                    val QRData = edt_code_string.text.toString().toByteArray()
                                    val QRcommands = ArrayList<ByteArray>()
                                    val modelCommand = ByteArray(6)
                                    modelCommand[0] = 27.toByte()
                                    modelCommand[1] = 29.toByte()
                                    modelCommand[2] = 121.toByte()
                                    modelCommand[3] = 83.toByte()
                                    modelCommand[4] = 48.toByte()
                                    /*
                                * Model
                                * [START]
                                * */
                                    //modelCommand[5] = 1.toByte()
                                    modelCommand[5] = 2.toByte()
                                    /*
                                * [END]
                                * */
                                    QRcommands.add(modelCommand)
                                    val correctionLevelCommand = ByteArray(6)
                                    correctionLevelCommand[0] = 27.toByte()
                                    correctionLevelCommand[1] = 29.toByte()
                                    correctionLevelCommand[2] = 121.toByte()
                                    correctionLevelCommand[3] = 83.toByte()
                                    correctionLevelCommand[4] = 49.toByte()

                                    /*
                                * correction Level
                                * [START]
                                * */
                                    //correctionLevelCommand[5] = 0.toByte()
                                    //correctionLevelCommand[5] = 1.toByte()
                                    //correctionLevelCommand[5] = 2.toByte()
                                    correctionLevelCommand[5] = 3.toByte()
                                    /*
                                * [END]
                                * */

                                    QRcommands.add(correctionLevelCommand)
                                    QRcommands.add(byteArrayOf(27.toByte(), 29.toByte(), 121.toByte(), 83.toByte(), 50.toByte(), 8.toByte()))
                                    val obj = ByteArray(8)
                                    obj[0] = 27.toByte()
                                    obj[1] = 29.toByte()
                                    obj[2] = 121.toByte()
                                    obj[3] = 68.toByte()
                                    obj[4] = 49.toByte()
                                    obj[6] = (QRData.size % 256).toByte()
                                    obj[7] = (QRData.size / 256).toByte()
                                    QRcommands.add(obj)
                                    QRcommands.add(QRData)
                                    QRcommands.add(byteArrayOf(27.toByte(), 29.toByte(), 121.toByte(), 80.toByte()))

                                    for (Qrdata in QRcommands) {
                                        SendDataByte(Qrdata)
                                    }

                                    SendDataByte("\n\n\n".toByteArray())
                                    SendDataByte(Command.LF)
                                    SendDataByte(PrinterCommand.POS_Set_PrtAndFeedPaper(2))
                                    SendDataByte(Command.ESC_d)
                                    SendDataByte(PrinterCommand.POS_Set_PrtInit())
                                    /*
                                * [END QRCode]
                                * */
                                }
                                else
                                {
                                    Toast.makeText(this@Bar_Qr_codeActivity, "Find Other Label Printer", Toast.LENGTH_LONG).show()
                                }
                                /*
                            * #######-----QRCode-----#######
                            * [START QRCode]
                            * */

                            }


                        } catch (e: Exception) {
                            e.printStackTrace()
                        }



                    }
                    else
                    {

                    }
                    //   CUC.displayToast(this,"printetres",result.toString())

                }, { error ->
                    error.printStackTrace()
                })
        )

    }

}


