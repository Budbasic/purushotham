package com.budbasic.posconsole.reception;


import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.budbasic.customer.global.Preferences;
import com.budbasic.global.CUC;
import com.budbasic.posconsole.R;
import com.budbasic.posconsole.adapter.DailyReportAdapter;
import com.budbasic.posconsole.adapter.MonthlyReportAdapter;
import com.budbasic.posconsole.webservice.RequetsInterface;
import com.kotlindemo.model.GetDailyReport;
import com.kotlindemo.model.GetEmployeeLogin;
import com.kotlindemo.model.GetFinancialReport;
import com.kotlindemo.model.GetMonthlyreport;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

/**
 * A simple {@link Fragment} subclass.
 */
@SuppressLint("ValidFragment")
public class FinancialAnalyticsFragment extends Fragment {


    @SuppressLint("ValidFragment")
    public FinancialAnalyticsFragment(GetEmployeeLogin logindata3) {
        this.logindata=logindata3;
        // Required empty public constructor
    }

    private TextView tv_month,tv_start_date,tv_end_date,txtNoDataFounddaily;
    private RecyclerView recycle_dailyreport,recycle_monthlyreport;
    ArrayList<GetMonthlyreport> monthlyreports;
    ArrayList<GetDailyReport> dialyreports;
    public Dialog mDialog;
    private GetEmployeeLogin logindata;
    private int s_day,s_month,s_year,t_day,t_month,t_year;
    private Calendar calendar,calendar2;
    private MonthlyReportAdapter monthlyReportAdapter;
    private DailyReportAdapter dailyReportAdapter;
    private ImageView btn_back;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_financial_analytics, container, false);
        tv_month=view.findViewById(R.id.tv_month);
        tv_start_date=view.findViewById(R.id.tv_start_date);
        tv_end_date=view.findViewById(R.id.tv_end_date);
        recycle_dailyreport=view.findViewById(R.id.recycle_dailyreport);
        recycle_monthlyreport=view.findViewById(R.id.recycle_monthlyreport);
        btn_back=view.findViewById(R.id.btn_back);
        txtNoDataFounddaily=view.findViewById(R.id.txtNoDataFounddaily);

        recycle_dailyreport.setLayoutManager(new GridLayoutManager(getContext(),1));
        recycle_monthlyreport.setLayoutManager(new GridLayoutManager(getContext(),1));

        dialyreports=new ArrayList<>();
        monthlyreports=new ArrayList<>();

        calendar= Calendar.getInstance();
        calendar2=Calendar.getInstance();

        calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMinimum(Calendar.DAY_OF_MONTH));
        s_day=calendar.get(Calendar.DAY_OF_MONTH);
        s_month=calendar.get(Calendar.MONTH);
        s_year=calendar.get(Calendar.YEAR);
        tv_start_date.setText(s_day+"-"+(s_month+1)+"-"+s_year);
        calendar2.set(Calendar.DAY_OF_MONTH, calendar2.getActualMaximum(Calendar.DAY_OF_MONTH));
        t_day=calendar2.get(Calendar.DAY_OF_MONTH);
        t_month=calendar2.get(Calendar.MONTH);
        t_year=calendar2.get(Calendar.YEAR);
        tv_end_date.setText(t_day+"-"+(t_month+1)+"-"+t_year);

        monthlyReportAdapter=new MonthlyReportAdapter(getContext(),monthlyreports);
        recycle_monthlyreport.setAdapter(monthlyReportAdapter);

        dailyReportAdapter=new DailyReportAdapter(getContext(),dialyreports);
        recycle_dailyreport.setAdapter(dailyReportAdapter);

        getFinancial();

        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().onBackPressed();

            }
        });
        tv_start_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(),
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                s_day=dayOfMonth;
                                s_month=monthOfYear;
                                s_year=year;
                                tv_start_date.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);
                                getFinancial();
                            }
                        }, s_year, s_month, s_day);
                datePickerDialog.show();
            }
        });
        tv_end_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(),
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                t_day=dayOfMonth;
                                t_month=monthOfYear+1;
                                t_year=year;
                                tv_end_date.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);
                                getFinancial();

                            }
                        }, t_year, t_month, t_day);
                datePickerDialog.show();
            }
        });
        return view;
    }
    public void getFinancial(){
        monthlyreports.clear();
        dialyreports.clear();
        mDialog = CUC.Companion.createDialog(getActivity());
        RequetsInterface apiService = RequetsInterface.Factory.create();
        Map parameterMap =new HashMap<String, String>();
        parameterMap.put("api_key", String.valueOf(Preferences.INSTANCE.getPreference((getContext()), "API_KEY")));
        if (logindata != null) {
            parameterMap.put("store_id", logindata.getUser_store_id());
        }

        parameterMap.put("from_date",(s_month+1)+"-"+s_day+"-"+s_year);
        parameterMap.put("to_date",(t_month+1)+"-"+t_day+"-"+t_year);

        System.out.println("aaaaaaaaaaa  parameterMap remove discount "+parameterMap);
        Log.i("aaaaaaaa   Result", "parameterMap : " + parameterMap);
        (new CompositeDisposable()).add(apiService.callFinancialAnalytics(parameterMap).
                observeOn(AndroidSchedulers.mainThread()).
                subscribeOn(Schedulers.io()).subscribe((Consumer)(new Consumer() {
            // $FF: synthetic method
            // $FF: bridge method
            public void accept(Object var1) {
                this.accept((GetFinancialReport)var1);
            }

            public final void accept(GetFinancialReport result) {
                System.out.println("aaaaaaaaa  result  "+result.toString());
                mDialog.cancel();
                if (result != null) {
                    mDialog.cancel();
                    System.out.println("aaaaaaaaa  result remove discount  "+result.toString());
                    Log.i("aaaaaaa   Result", "" + result.toString());
                    if (result.getMonthly_report().size()>0){
                        monthlyreports.addAll(result.getMonthly_report());
                        monthlyReportAdapter.setRefresh(monthlyreports);
                        CUC.Companion.displayToast((Context)getContext(), "Sucess", "Sucess");
                    }else {
                        CUC.Companion.displayToast((Context)getContext(), "No Data", "No Data");
                    }
                    if (result.getDaily_report().size()>0){
                        txtNoDataFounddaily.setVisibility(View.GONE);
                        recycle_dailyreport.setVisibility(View.VISIBLE);
                        dialyreports.addAll(result.getDaily_report());
                        dailyReportAdapter.setRefresh(dialyreports);
                        CUC.Companion.displayToast((Context)getContext(), "Sucess", "Sucess");
                    }else {
                        txtNoDataFounddaily.setVisibility(View.VISIBLE);
                        recycle_dailyreport.setVisibility(View.GONE);
                        CUC.Companion.displayToast((Context)getContext(), "No Data", "No Data");
                    }

                } else {
                    mDialog.cancel();
                }
            }
        }), (Consumer)(new Consumer() {
            // $FF: synthetic method
            // $FF: bridge method
            public void accept(Object var1) {
                this.accept((Throwable)var1);
            }

            public final void accept(Throwable error) {
                mDialog.cancel();
                Toast.makeText(getContext(), getString(R.string.connection_to_database_failed), Toast.LENGTH_SHORT).show();
                error.printStackTrace();
                System.out.println("aaaaaaaa  error update "+error.getMessage());
            }
        })));
    }


}
