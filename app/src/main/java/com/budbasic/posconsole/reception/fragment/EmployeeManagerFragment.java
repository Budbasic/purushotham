package com.budbasic.posconsole.reception.fragment;


import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.budbasic.customer.global.Preferences;
import com.budbasic.global.CUC;
import com.budbasic.posconsole.R;
import com.budbasic.posconsole.adapter.EmployeeAdapter;
import com.budbasic.posconsole.dialogs.EmployeePinDialog;
import com.budbasic.posconsole.webservice.RequetsInterface;
import com.google.gson.Gson;
import com.kotlindemo.model.GetDrivers;
import com.kotlindemo.model.GetEmployeeAppSetting;
import com.kotlindemo.model.GetEmployeeLogin;
import com.kotlindemo.model.GetEmployeeSalesProducts;
import com.kotlindemo.model.GetEmployeeStoremList;
import com.kotlindemo.model.GetStatus;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import kotlin.jvm.internal.Intrinsics;

/**
 * A simple {@link Fragment} subclass.
 */
public class EmployeeManagerFragment extends Fragment {

    private ImageView btn_back;
    private EditText edt_employee_search;
    private TextView cv_search;
    private RecyclerView recycler_view;
    public Dialog mDialog;
    ArrayList<GetDrivers> employeelist;
    GetEmployeeLogin logindata;
    GetEmployeeAppSetting appSettingData;
    private EmployeeAdapter employeeAdapter;
    private Gson gson;
    private EmployeePinDialog employeePinDialog;

    public EmployeeManagerFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_employee_manager, container, false);
        btn_back=view.findViewById(R.id.btn_back);
        edt_employee_search=view.findViewById(R.id.edt_employee_search);
        cv_search=view.findViewById(R.id.cv_search);
        recycler_view=view.findViewById(R.id.recycler_view);

        recycler_view.setLayoutManager(new GridLayoutManager(getContext(),1));
        employeelist=new ArrayList<>();
        gson=new Gson();
        appSettingData = gson.fromJson(Preferences.INSTANCE.getPreference(getContext(), "AppSetting"), GetEmployeeAppSetting.class);
        logindata = gson.fromJson(Preferences.INSTANCE.getPreference(getContext(), "logindata"), GetEmployeeLogin.class);
        employeeAdapter=new EmployeeAdapter(getContext(),employeelist,EmployeeManagerFragment.this);
        recycler_view.setAdapter(employeeAdapter);
        edt_employee_search.setImeOptions(EditorInfo.IME_FLAG_NO_EXTRACT_UI | EditorInfo.IME_ACTION_DONE);
        cv_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().onBackPressed();
            }
        });

        getEmployees();
        return view;
    }

    public void getEmployees(){
        employeelist.clear();
        if (CUC.Companion.isNetworkAvailablewithPopup((getActivity()))) {
            mDialog = CUC.Companion.createDialog(getActivity());
            RequetsInterface apiService = RequetsInterface.Factory.create();
            Map parameterMap =new HashMap<String, String>();
            parameterMap.put("api_key", String.valueOf(Preferences.INSTANCE.getPreference((getActivity()), "API_KEY")));

            if (logindata != null) {
                parameterMap.put("employee_id", logindata.getUser_id());
                parameterMap.put("employee_store_id", logindata.getUser_store_id());
            }


            Log.i("aaaaaaaa   Result", "parameterMap : " + parameterMap);
            (new CompositeDisposable()).add(apiService.callEmployeeStoremList(parameterMap)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe((Consumer)(new Consumer() {
                        // $FF: synthetic method
                        // $FF: bridge method
                        public void accept(Object var1) {
                            this.accept((GetEmployeeStoremList)var1);
                        }

                        public final void accept(GetEmployeeStoremList result) {
                            mDialog.cancel();
                            if (result != null) {

                                System.out.println("aaaaaaaaa  result  "+result.toString());
                                Log.i("aaaaaaa   Result", "" + result.toString());
                                if (Intrinsics.areEqual(result.getStatus(), "0")) {

                                    if (result.getEmployee().size() > 0) {
                                        employeelist.addAll(result.getEmployee());
                                        employeeAdapter.setRefresh(employeelist);
                                    }
                                    CUC.Companion.displayToast(getActivity(), result.getShow_status(), result.getMessage());
                                } else if (Intrinsics.areEqual(result.getStatus(), "10")) {

                                } else {
                                    Toast.makeText(getActivity(), ""+result.getShow_status(), Toast.LENGTH_SHORT).show();
                                    CUC.Companion.displayToast((Context)getActivity(), result.getShow_status(), result.getMessage());
                                }
                            } else {
                            }
                        }
                    }), (Consumer)(new Consumer() {
                        // $FF: synthetic method
                        // $FF: bridge method
                        public void accept(Object var1) {
                            this.accept((Throwable)var1);
                        }

                        public final void accept(Throwable error) {
                            mDialog.cancel();
                            Toast.makeText(getActivity(), getString(R.string.connection_to_database_failed), Toast.LENGTH_SHORT).show();
                            error.printStackTrace();
                            System.out.println("aaaaaaaa  error "+error.getMessage());
                        }
                    })));

        }else {
            System.out.println("aaaaaaaaaa   else ");
        }
    }

    public void setChangedpin(GetDrivers getDrivers) {
        employeePinDialog=new EmployeePinDialog();
        employeePinDialog.showDialog(getContext(),getDrivers,1,EmployeeManagerFragment.this);
    }

    public void setChangepassword(GetDrivers getDrivers) {
        employeePinDialog=new EmployeePinDialog();
        employeePinDialog.showDialog(getContext(),getDrivers,0,EmployeeManagerFragment.this);
    }

    public void setREquirepin(final GetDrivers getDrivers) {
        AlertDialog.Builder builder1 = new AlertDialog.Builder(getContext());
        builder1.setMessage("Are you sure you want Require pin for this Employee ?");
        builder1.setTitle("Confirm");
        builder1.setCancelable(true);

        builder1.setPositiveButton(
                "Ok",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        setRequirepin(getDrivers);
                        dialog.cancel();
                    }
                });
        builder1.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.cancel();
            }
        });

        AlertDialog alert11 = builder1.create();
        alert11.show();
    }

    public void setRequirepin(GetDrivers getDrivers){
        if (CUC.Companion.isNetworkAvailablewithPopup((getActivity()))) {
            mDialog = CUC.Companion.createDialog(getActivity());
            RequetsInterface apiService = RequetsInterface.Factory.create();
            Map parameterMap =new HashMap<String, String>();
            parameterMap.put("api_key", String.valueOf(Preferences.INSTANCE.getPreference((getActivity()), "API_KEY")));

            if (logindata != null) {
                parameterMap.put("manager_id", logindata.getUser_store_id());
            }
            parameterMap.put("employee_id", getDrivers.getEmp_id());
            parameterMap.put("require", getDrivers.getEmp_requirepin());
            System.out.println("aaaaaaaaaa   requirepin "+parameterMap);
            Log.i("aaaaaaaa   Result", "parameterMap : " + parameterMap);
            (new CompositeDisposable()).add(apiService.callEmployeeRequirepin(parameterMap)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe((Consumer)(new Consumer() {
                        // $FF: synthetic method
                        // $FF: bridge method
                        public void accept(Object var1) {
                            this.accept((GetStatus)var1);
                        }

                        public final void accept(GetStatus result) {
                            mDialog.cancel();
                            if (result != null) {

                                System.out.println("aaaaaaaaa  requirepin result  "+result.toString());
                                Log.i("aaaaaaa   Result", "" + result.toString());
                                if (Intrinsics.areEqual(result.getStatus(), "0")) {
                                    getEmployees();
                                    CUC.Companion.displayToast(getActivity(), result.getShow_status(), result.getMessage());
                                } else if (Intrinsics.areEqual(result.getStatus(), "10")) {

                                } else {
                                    Toast.makeText(getActivity(), ""+result.getShow_status(), Toast.LENGTH_SHORT).show();
                                    CUC.Companion.displayToast((Context)getActivity(), result.getShow_status(), result.getMessage());
                                }
                            } else {
                            }
                        }
                    }), (Consumer)(new Consumer() {
                        // $FF: synthetic method
                        // $FF: bridge method
                        public void accept(Object var1) {
                            this.accept((Throwable)var1);
                        }

                        public final void accept(Throwable error) {
                            mDialog.cancel();
                            Toast.makeText(getActivity(), getString(R.string.connection_to_database_failed), Toast.LENGTH_SHORT).show();
                            error.printStackTrace();
                            System.out.println("aaaaaaaa  error "+error.getMessage());
                        }
                    })));

        }else {
            System.out.println("aaaaaaaaaa   else ");
        }
    }

    public void setPinChange(String emp_id, String toString) {

    }

    public void setpasswordchange(String emp_id, String toString) {

    }
}
