package com.budbasic.posconsole.reception.fragment

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.support.annotation.RequiresApi
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.RadioGroup
import android.widget.RelativeLayout
import android.widget.TextView
import com.google.gson.Gson
import com.kotlindemo.model.GetEmployeeLogin
import com.kotlindemo.model.GetStoreOrders
import com.budbasic.posconsole.webservice.RequetsInterface
import com.budbasic.posconsole.EmployeeCartDetailsActivity
import com.budbasic.posconsole.EmployeeInvoiceDetailsActivity
import com.budbasic.posconsole.R
import com.budbasic.customer.global.Preferences
import com.budbasic.global.CUC
import com.budbasic.posconsole.model.APIClass
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.employee_orders_fragment.view.*
import java.util.concurrent.TimeUnit

class EmployeeOrdersFragment : Fragment(), APIClass.onCallListner {
    override fun onFailed(requestCode: Int) {
    }

    override fun onSuccess(requestCode: Int) {
        if (requestCode == 1) {
            callGetEmployeeOrders()
        }
    }

    lateinit var mDialog: Dialog
    var list: ArrayList<GetStoreOrders> = ArrayList()
    var mView: View? = null
    lateinit var productAdapter: ProductAdapter
    lateinit var logindata: GetEmployeeLogin
    val mRequestCode = 7070
    lateinit var subscription: Disposable
    var order_type = "0"

    var assignedStatus = 0
    var assignedUniqno = ""
    var apiClass: APIClass? = null
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        if (view != null)
            mView = view
        else
            mView = inflater.inflate(R.layout.employee_orders_fragment, container, false)
        apiClass = APIClass(context!!, this)
        logindata = Gson().fromJson(Preferences.getPreference(activity!!, "logindata"), GetEmployeeLogin::class.java)
        init()
        return mView
    }

    private fun init() {
        mView!!.ll_employee_ordermain.setOnClickListener {

        }
        mView!!.btn_back.setOnClickListener {
            activity!!.onBackPressed()
        }
        mView!!.rv_employee_orders_fragment!!.layoutManager = GridLayoutManager(activity!!, 1)

        productAdapter = ProductAdapter(list, activity!!, object : onClickListener {
            override fun callClick(data: GetStoreOrders) {
                when (order_type) {
                    "3" -> {
                        val myIntent = Intent(activity!!, EmployeeInvoiceDetailsActivity::class.java)
                        val invoiceStr = Gson().toJson(data)
                        myIntent.putExtra("all_data", "$invoiceStr")
                        myIntent.putExtra("order_type", "$order_type")
                        activity!!.startActivityForResult(myIntent, mRequestCode)
                    }
                    "2" -> {
                        val myIntent = Intent(activity!!, EmployeeInvoiceDetailsActivity::class.java)
                        val invoiceStr = Gson().toJson(data)
                        myIntent.putExtra("all_data", "$invoiceStr")
                        myIntent.putExtra("order_type", "$order_type")
                        activity!!.startActivityForResult(myIntent, mRequestCode)
                    }
                    "1" -> {
                        val myIntent = Intent(activity!!, EmployeeInvoiceDetailsActivity::class.java)
                        val invoiceStr = Gson().toJson(data)
                        myIntent.putExtra("all_data", "$invoiceStr")
                        myIntent.putExtra("order_type", "$order_type")
                        activity!!.startActivityForResult(myIntent, mRequestCode)
                    }
                    "0" -> {
                        val myIntent = Intent(activity!!, EmployeeCartDetailsActivity::class.java)
                        myIntent.putExtra("store_id", "${data.cart_store_id}")
                        myIntent.putExtra("patient_id", "${data.cart_patient_id}")
                        myIntent.putExtra("cart_id", "${data.cart_id}")
                        activity!!.startActivityForResult(myIntent, mRequestCode)
                    }
                }
            }
        })

        productAdapter.orderType(order_type)
        mView!!.rv_employee_orders_fragment!!.adapter = productAdapter

        mView!!.rg_order_tabs.setOnCheckedChangeListener(object : RadioGroup.OnCheckedChangeListener {
            override fun onCheckedChanged(p0: RadioGroup?, p1: Int) {
                when (p1) {
                    R.id.rb_preorderplaced -> {
                        order_type = "0"
                        callGetEmployeeOrders()
                    }
                    R.id.rb_received_inprocess -> {
                        order_type = "1"
                        callGetEmployeeOrders()
                    }
                    R.id.rb_invoicegenerated -> {
                        order_type = "2"
                        callGetEmployeeOrders()
                    }
                    R.id.rb_paymentreceived -> {
                        order_type = "3"
                        callGetEmployeeOrders()
                    }
                }
            }
        })
        callGetEmployeeOrders()
    }

    override fun onStart() {
        super.onStart()
        Log.i(EmployeeOrdersFragment::class.java.simpleName, "onStart()")
        startInterval()
    }

    fun startInterval() {
        subscription = Observable.interval(120000, 120000, TimeUnit.MILLISECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    Log.i(EmployeeOrdersFragment::class.java.simpleName, "Interval : $it ")
                    assignedUniqno = ""
                    callGetEmployeeOrders()
                }
    }

    fun stopInterval() {
        subscription.dispose()
    }

    override fun onStop() {
        super.onStop()
        Log.i(EmployeeOrdersFragment::class.java.simpleName, "onStop()")
        stopInterval()
    }


    override fun onDestroy() {
        super.onDestroy()
        Log.i(EmployeeOrdersFragment::class.java.simpleName, "onDestroy()")
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        Log.i("Result", "onActivityResult(), $requestCode : $resultCode")
        if (requestCode == mRequestCode) {
            if (resultCode == Activity.RESULT_OK) {
                callGetEmployeeOrders()
            } else if (resultCode == Activity.RESULT_FIRST_USER) {
                if (data != null && data.hasExtra("message"))
                    CUC.displayToast(activity!!, "0", "${data.getStringExtra("message")}")
                callGetEmployeeOrders()
            }
        }
    }

    fun callGetEmployeeOrders() {
        try {
            if (!activity!!.isFinishing) {
                if (CUC.isNetworkAvailablewithPopup(activity!!)) {
                    try {
                        if (::mDialog.isInitialized) {
                            if (mDialog != null && mDialog.isShowing)
                                mDialog!!.dismiss()
                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                    mDialog = CUC.createDialog(activity!!)
                    val apiService = RequetsInterface.create()
                    val parameterMap = HashMap<String, String>()
                    parameterMap["api_key"] = "${Preferences.getPreference(activity!!, "API_KEY")}"
                    if (logindata != null) {
                        parameterMap["employee_id"] = "${logindata.user_id}"
                        parameterMap["employee_store_id"] = "${logindata.user_store_id}"
                    }
                    parameterMap["order_type"] = "$order_type"
                    Log.i("Result", "parameterMap : $parameterMap")
                    CompositeDisposable().add(apiService.callGetEmployeeOrders(parameterMap)
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribeOn(Schedulers.io())
                            .subscribe({ result ->
                                if (mDialog != null && mDialog!!.isShowing)
                                    mDialog!!.dismiss()

                                if (result != null) {
                                    Log.i("Result", "" + result.toString())
                                    if (result.status == "0") {
                                        when (order_type) {
                                            "3" -> {
                                                productAdapter.orderType(order_type)
                                                productAdapter.orderUniqno(assignedUniqno)
                                                assignedUniqno = ""
                                                if (result.invoice_orders != null && result.invoice_orders.size > 0) {
                                                    list.clear()
                                                    list.addAll(result.invoice_orders)
                                                    productAdapter.notifyDataSetChanged()
                                                    mView!!.rl_empty_view.visibility = View.GONE
                                                } else {
                                                    list.clear()
                                                    productAdapter.notifyDataSetChanged()
                                                    mView!!.rl_empty_view.visibility = View.VISIBLE
                                                }
                                            }
                                            "2" -> {
                                                productAdapter.orderType(order_type)
                                                productAdapter.orderUniqno(assignedUniqno)
                                                assignedUniqno = ""
                                                if (result.invoice_orders != null && result.invoice_orders.size > 0) {
                                                    list.clear()
                                                    list.addAll(result.invoice_orders)
                                                    productAdapter.notifyDataSetChanged()
                                                    mView!!.rl_empty_view.visibility = View.GONE
                                                } else {
                                                    list.clear()
                                                    productAdapter.notifyDataSetChanged()
                                                    mView!!.rl_empty_view.visibility = View.VISIBLE
                                                }
                                            }
                                            "1" -> {
                                                productAdapter.orderType(order_type)
                                                productAdapter.orderUniqno(assignedUniqno)
                                                assignedUniqno = ""
                                                if (result.invoice_orders != null && result.invoice_orders.size > 0) {
                                                    list.clear()
                                                    list.addAll(result.invoice_orders)
                                                    productAdapter.notifyDataSetChanged()
                                                    mView!!.rl_empty_view.visibility = View.GONE
                                                } else {
                                                    list.clear()
                                                    productAdapter.notifyDataSetChanged()
                                                    mView!!.rl_empty_view.visibility = View.VISIBLE
                                                }
                                            }
                                            "0" -> {
                                                productAdapter.orderType(order_type)
                                                productAdapter.orderUniqno(assignedUniqno)
                                                assignedUniqno = ""
                                                if (result.store_orders != null && result.store_orders.size > 0) {
                                                    list.clear()
                                                    list.addAll(result.store_orders)
                                                    productAdapter.notifyDataSetChanged()
                                                    mView!!.rl_empty_view.visibility = View.GONE
                                                } else {
                                                    list.clear()
                                                    productAdapter.notifyDataSetChanged()
                                                    mView!!.rl_empty_view.visibility = View.VISIBLE
                                                }
                                            }
                                        }

                                        if (result.assigned_order != null) {
                                            if (assignedStatus == 0) {
                                                assignedStatus = 1
                                                assignedUniqno = "${result.assigned_order.invoice_order_unique_no}"
                                                when (result.assigned_order.invoice_status) {
                                                    "1" -> {
                                                        mView!!.rb_received_inprocess.isChecked = true
                                                    }
                                                    "2" -> {
                                                        mView!!.rb_invoicegenerated.isChecked = true
                                                    }
                                                    "3" -> {
                                                        mView!!.rb_paymentreceived.isChecked = true
                                                    }
                                                }
                                            } else if (assignedStatus == 1) {
                                                assignedStatus = 2
                                            } else {
                                                assignedUniqno = ""
                                            }
                                        } else {
                                            assignedUniqno = ""
                                        }
                                        Log.i("Result", "$assignedStatus : $assignedUniqno")
                                        if (!activity!!.isFinishing)
                                            CUC.displayToast(activity!!, result.show_status, result.message)
                                    } else if (result.status == "10") {
                                        apiClass!!.callApiKey(1)
                                    } else {
                                        list.clear()
                                        productAdapter.notifyDataSetChanged()
                                        mView!!.rl_empty_view.visibility = View.VISIBLE
                                        CUC.displayToast(activity!!, result.show_status, result.message)
                                    }
                                } else {
                                    if (!activity!!.isFinishing) {
                                        if (mDialog != null && mDialog!!.isShowing)
                                            mDialog!!.dismiss()
                                       // CUC.displayToast(activity!!, "0", getString(R.string.connection_to_database_failed))
                                    }

                                }

                            }, { error ->
                                try {
                                    if (mDialog != null && mDialog!!.isShowing)
                                        mDialog!!.dismiss()
                                    CUC.displayToast(activity!!, "0", getString(R.string.connection_to_database_failed))
                                    error.printStackTrace()
                                } catch (ee: Exception) {
                                    ee.printStackTrace()
                                }

                            })
                    )
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
        Log.i("Result", "callGetEmployeeOrders()")

    }

    class ProductAdapter(val list: ArrayList<GetStoreOrders>, val context: Context, val onClick: onClickListener) : RecyclerView.Adapter<ProductAdapter.ViewHolder>() {
        var priviousView: RelativeLayout? = null


        var order_type: String = "0"
        var assignedUniqno: String = ""
        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            holder!!.bindItems(list[position], context, order_type, assignedUniqno, onClick)
        }

        override fun getItemCount(): Int {
            return list.size
        }

        fun orderType(type: String) {
            order_type = type
        }

        fun orderUniqno(unicno: String) {
            assignedUniqno = unicno
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            val v = LayoutInflater.from(parent!!.context).inflate(R.layout.employee_order_item, parent, false)
            return ViewHolder(v)
        }

        inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
            val tv_order_unique_no: TextView = itemView.findViewById(R.id.tv_order_unique_no)
            val tv_patient_name: TextView = itemView.findViewById(R.id.tv_patient_name)
            val tv_mobile_no: TextView = itemView.findViewById(R.id.tv_mobile_no)
            val ll_employee_order_item: RelativeLayout = itemView.findViewById(R.id.ll_employee_order_item)


            fun bindItems(data: GetStoreOrders, context: Context, order_type: String, assignedUniqno: String, onClick: onClickListener) {
                when (order_type) {

                    "3" -> {


                        /*if (data.invoice_void.isNotEmpty()) {
                            ll_order_no.setBackgroundColor(ContextCompat.getColor(context!!, R.color.orange_color))
                        } else {
                            if (data.invoice_cancel_request == "1") {
                                ll_order_no.setBackgroundColor(ContextCompat.getColor(context!!, R.color.close_red))
                            } else {
                                ll_order_no.setBackgroundColor(ContextCompat.getColor(context!!, R.color.colorPrimaryDark))
                            }
                        }*/

                        tv_order_unique_no.text = data.invoice_order_unique_no
                        tv_patient_name.text = "${data.patient_fname} ${data.patient_lname}"
                        tv_mobile_no.text = data.patient_mobile_no
//                        if (data.invoice_order_unique_no == "$assignedUniqno") {
//                            val startColor = ContextCompat.getColor(context, R.color.colorPrimary)
//                            val endColor = ContextCompat.getColor(context, R.color.white)
//                            val anim = ObjectAnimator.ofInt(tv_order_unique_no, "textColor", Color.RED, Color.WHITE)
//                            anim.duration = 300
//                            anim.setEvaluator(ArgbEvaluator())
//                            anim.repeatMode = ValueAnimator.REVERSE
//                            anim.repeatCount = Animation.INFINITE
//                            anim.start()
//                        }
                    }
                    "2" -> {
                        /*   if (data.invoice_cancel_request == "1") {
                               ll_order_no.setBackgroundColor(ContextCompat.getColor(context!!, R.color.close_red))
                           } else {
                               ll_order_no.setBackgroundColor(ContextCompat.getColor(context!!, R.color.colorPrimaryDark))
                           }

                           if (data.invoice_serve_type == "2") {
                               if (data.invoice_delivery_driver == "0") {
                                   ll_order_no.setBackgroundColor(ContextCompat.getColor(context!!, R.color.black))
                               }
                           }*/

                        tv_order_unique_no.text = data.invoice_order_unique_no
                        tv_patient_name.text = "${data.patient_fname} ${data.patient_lname}"
                        tv_mobile_no.text = data.patient_mobile_no
//                        if (data.invoice_order_unique_no == "$assignedUniqno") {
//                        }
                    }
                    "1" -> {
                        /* if (data.invoice_cancel_request == "1") {
                             ll_order_no.setBackgroundColor(ContextCompat.getColor(context!!, R.color.close_red))
                         } else {
                             ll_order_no.setBackgroundColor(ContextCompat.getColor(context!!, R.color.colorPrimaryDark))
                         }*/
                        tv_order_unique_no.text = data.invoice_order_unique_no
                        tv_patient_name.text = "${data.patient_fname} ${data.patient_lname}"
                        tv_mobile_no.text = data.patient_mobile_no

//                        if (data.invoice_order_unique_no == assignedUniqno) {
//                        }
                    }
                    "0" -> {
                        //ll_order_no.setBackgroundColor(ContextCompat.getColor(context!!, R.color.colorPrimaryDark))
                        tv_order_unique_no.text = data.cart_order_unique_no
                        tv_patient_name.text = "${data.patient_fname} ${data.patient_lname}"
                        tv_mobile_no.text = data.patient_mobile_no
                    }
                }

                ll_employee_order_item.setOnClickListener { it ->


                    if (priviousView != null) {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            priviousView!!.setBackgroundColor(context.getColor(R.color.white))
                        } else {
                            priviousView!!.setBackgroundColor(context.resources.getColor(R.color.white))
                        }
                    }

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        it.setBackgroundColor(context.getColor(R.color.blue))
                    } else {
                        it.setBackgroundColor(context.resources.getColor(R.color.blue))
                    }
                    priviousView = (it as RelativeLayout)
                    onClick.callClick(data)
                }
            }
        }
    }

    interface onClickListener {
        fun callClick(data: GetStoreOrders)
    }
}