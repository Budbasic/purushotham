package com.budbasic.posconsole.reception;

import android.app.Dialog;
import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.budbasic.customer.global.Preferences;
import com.budbasic.global.CUC;
import com.budbasic.posconsole.R;
import com.budbasic.posconsole.dialogs.AddProductDialog;
import com.budbasic.posconsole.webservice.RequetsInterface;
import com.google.gson.Gson;
import com.kotlindemo.model.GetEmpExpenses;
import com.kotlindemo.model.GetEmployeeAppSetting;
import com.kotlindemo.model.GetEmployeeExpanses;
import com.kotlindemo.model.GetEmployeeLogin;
import com.kotlindemo.model.GetEmployeeSalesProducts;
import com.kotlindemo.model.GetExpensesItems;
import com.kotlindemo.model.GetExpensesList;
import com.kotlindemo.model.GetStart_location;
import com.kotlindemo.model.GetStatus;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import kotlin.jvm.internal.Intrinsics;

public class AddExpensesActivity extends AppCompatActivity {

    private EditText edt_description,edt_amount;
    private LinearLayout cv_add_expenses1;
    private TextView tv_date,tv_expenseitem;
    private Calendar calendar;
    public Dialog mDialog;
    String st_amount,st_description;
    GetExpensesItems getExpensesItems;
    GetEmployeeLogin logindata;
    private Gson gson;
    GetEmployeeAppSetting mBAsis;
    AddProductDialog addProductDialog;
    private int day,month,year;
    GetEmployeeExpanses getEmployeeExpanses;
    private ArrayList<GetExpensesItems> getExpensesLists;
    int position;
    private ImageView btn_back;
    boolean check;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_expenses);

        edt_description=findViewById(R.id.edt_description);
        edt_amount=findViewById(R.id.edt_amount);
        cv_add_expenses1=findViewById(R.id.cv_add_expenses1);
        tv_date=findViewById(R.id.tv_date);
        tv_expenseitem=findViewById(R.id.tv_expenseitem);
        btn_back=findViewById(R.id.btn_back);
        gson=new Gson();
        mBAsis = gson.fromJson(Preferences.INSTANCE.getPreference(AddExpensesActivity.this, "AppSetting"), GetEmployeeAppSetting.class);

        logindata = gson.fromJson(getIntent().getStringExtra("logindata"), GetEmployeeLogin.class);

         position=getIntent().getIntExtra("position",0);
        if (position!=0){
            getEmployeeExpanses = gson.fromJson(getIntent().getStringExtra("expansedata"), GetEmployeeExpanses.class);
            edt_amount.setText(getEmployeeExpanses.getAmount());
            edt_description.setText(getEmployeeExpanses.getExpensedescription());
            tv_expenseitem.setText(getEmployeeExpanses.getExpensename());
            edt_description.setSelection(edt_description.getText().length());
        }

        getExpensesLists=new ArrayList<>();
        edt_amount.setImeOptions(EditorInfo.IME_FLAG_NO_EXTRACT_UI);
        edt_description.setImeOptions(EditorInfo.IME_FLAG_NO_EXTRACT_UI);

        calendar=Calendar.getInstance();
         day=calendar.get(Calendar.DAY_OF_MONTH);
         month=calendar.get(Calendar.MONTH);
         year=calendar.get(Calendar.YEAR);
        tv_date.setText(day+"-"+(month+1)+"-"+year);
        cv_add_expenses1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                st_amount=edt_amount.getText().toString().trim();
                st_description=edt_description.getText().toString().trim();
                if (position==1){
                    getEditExpenses(st_amount,st_description);
                }else {
                    getAddExpense(st_amount,st_description);
                }
            }
        });
        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        tv_expenseitem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                getExpensesList();
               // AddProductDialog addProductDialog=new AddProductDialog();
               // addProductDialog.showDialog(AddExpensesActivity.this);

            }
        });
    }

    public void getExpensesList(){
        if (CUC.Companion.isNetworkAvailablewithPopup((Context)this)) {
           
            mDialog = CUC.Companion.createDialog(AddExpensesActivity.this);
            RequetsInterface apiService = RequetsInterface.Factory.create();
            Map parameterMap =new HashMap<String, String>();
            parameterMap.put("api_key", String.valueOf(Preferences.INSTANCE.getPreference((Context)this, "API_KEY")));
             Log.i("aaaaaaaa   Result", "parameterMap : " + parameterMap);
            (new CompositeDisposable()).add(apiService.callExpenseslist(parameterMap)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io()).subscribe((Consumer)(new Consumer() {
                // $FF: synthetic method
                // $FF: bridge method
                public void accept(Object var1) {
                    this.accept((GetExpensesList)var1);
                }

                public final void accept(GetExpensesList result) {
                    mDialog.cancel();
                    if (result != null) {
                        System.out.println("aaaaaaaaa  result  "+result.toString());
                        Log.i("aaaaaaa   Result", "" + result.toString());
                        if(result.getExpensename().size()>0){
                            getExpensesLists.addAll(result.getExpensename());
                             addProductDialog=new AddProductDialog();
                           // addProductDialog.showDialog(AddExpensesActivity.this,getExpensesLists);
                        }else {

                        }
                    } else {
                    }
                }
            }), (Consumer)(new Consumer() {
                // $FF: synthetic method
                // $FF: bridge method
                public void accept(Object var1) {
                    this.accept((Throwable)var1);
                }

                public final void accept(Throwable error) {
                    mDialog.cancel();
                    Toast.makeText(AddExpensesActivity.this, getString(R.string.connection_to_database_failed), Toast.LENGTH_SHORT).show();
                    error.printStackTrace();
                    System.out.println("aaaaaaaa  error "+error.getMessage());
                }
            })));

        }else {
            System.out.println("aaaaaaaaaa   else ");
        }
    }

    public void setexpansesItem(GetExpensesItems getExpensesItems) {
        tv_expenseitem.setText(""+getExpensesItems.getExpensename());
        this.getExpensesItems=getExpensesItems;
        check=true;
    }

    public void getAddExpense(String st_amount, String st_description){
        if (CUC.Companion.isNetworkAvailablewithPopup((Context)this)) {

            mDialog = CUC.Companion.createDialog(AddExpensesActivity.this);
            RequetsInterface apiService = RequetsInterface.Factory.create();
            Map parameterMap =new HashMap<String, String>();
            parameterMap.put("api_key", String.valueOf(Preferences.INSTANCE.getPreference((Context)this, "API_KEY")));
            parameterMap.put("emp",""+logindata.getUser_id());
            parameterMap.put("store_id",""+logindata.getUser_store_id());
            parameterMap.put("terminal_id",mBAsis.getTerminal_id());
            parameterMap.put("date",""+day+"-"+(month+1)+"-"+year);
            parameterMap.put("expensesitem",getExpensesItems.getExpensename());
            parameterMap.put("expensedescription",""+st_description);
            parameterMap.put("amount",""+st_amount);

            Log.i("aaaaaaaa   Result", "parameterMap : " + parameterMap);
            (new CompositeDisposable()).add(apiService.callAddexpenses(parameterMap)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io()).subscribe((Consumer)(new Consumer() {
                        // $FF: synthetic method
                        // $FF: bridge method
                        public void accept(Object var1) {
                            this.accept((GetStatus)var1);
                        }

                        public final void accept(GetStatus result) {
                            mDialog.cancel();
                            if (result != null) {
                                System.out.println("aaaaaaaaa  result  "+result.toString());
                                Log.i("aaaaaaa   Result", "" + result.toString());
                                if(result.getStatus().equalsIgnoreCase("0")){
                                    Toast.makeText(AddExpensesActivity.this, ""+result.getMsg(), Toast.LENGTH_SHORT).show();
                                    CUC.Companion.displayToast(AddExpensesActivity.this, result.getMsg(), result.getMsg());
                                    finish();
                                }else {
                                    Toast.makeText(AddExpensesActivity.this, ""+result.getMsg(), Toast.LENGTH_SHORT).show();
                                    CUC.Companion.displayToast(AddExpensesActivity.this, result.getMsg(), result.getMsg());
                                }



                            } else {
                            }
                        }
                    }), (Consumer)(new Consumer() {
                        // $FF: synthetic method
                        // $FF: bridge method
                        public void accept(Object var1) {
                            this.accept((Throwable)var1);
                        }

                        public final void accept(Throwable error) {
                            mDialog.cancel();
                            Toast.makeText(AddExpensesActivity.this, getString(R.string.connection_to_database_failed), Toast.LENGTH_SHORT).show();
                            error.printStackTrace();
                            System.out.println("aaaaaaaa  error "+error.getMessage());
                        }
                    })));

        }else {
            System.out.println("aaaaaaaaaa   else ");
        }
    }

    public void getEditExpenses(String st_amount, String st_description){
        if (CUC.Companion.isNetworkAvailablewithPopup((Context)this)) {

            mDialog = CUC.Companion.createDialog(AddExpensesActivity.this);
            RequetsInterface apiService = RequetsInterface.Factory.create();
            Map parameterMap =new HashMap<String, String>();
            parameterMap.put("api_key", String.valueOf(Preferences.INSTANCE.getPreference((Context)this, "API_KEY")));
            parameterMap.put("emp",""+logindata.getUser_id());
            parameterMap.put("store_id",""+logindata.getUser_store_id());
            parameterMap.put("terminal_id",mBAsis.getTerminal_id());
            parameterMap.put("item_id",getEmployeeExpanses.getItem_id());
            parameterMap.put("date",""+day+"-"+(month+1)+"-"+year);
            if (check){
                parameterMap.put("expensesitem",getExpensesItems.getExpensename());
            }else {
                parameterMap.put("expensesitem",getEmployeeExpanses.getExpensename());
            }

            parameterMap.put("expensedescription",""+st_description);
            parameterMap.put("amount",""+st_amount);

            Log.i("aaaaaaaa   Result", "parameterMap : " + parameterMap);
            (new CompositeDisposable()).add(apiService.calleditExpenses(parameterMap)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io()).subscribe((Consumer)(new Consumer() {
                        // $FF: synthetic method
                        // $FF: bridge method
                        public void accept(Object var1) {
                            this.accept((GetStatus)var1);
                        }

                        public final void accept(GetStatus result) {
                            mDialog.cancel();
                            if (result != null) {
                                System.out.println("aaaaaaaaa  result  "+result.toString());
                                Log.i("aaaaaaa   Result", "" + result.toString());
                                if(result.getStatus().equalsIgnoreCase("0")){
                                    Toast.makeText(AddExpensesActivity.this, ""+result.getMessage(), Toast.LENGTH_SHORT).show();
                                    CUC.Companion.displayToast(AddExpensesActivity.this, result.getMessage(), result.getMessage());
                                    finish();

                                }else {
                                    Toast.makeText(AddExpensesActivity.this, ""+result.getMessage(), Toast.LENGTH_SHORT).show();
                                    CUC.Companion.displayToast(AddExpensesActivity.this, result.getMessage(), result.getMessage());
                                }

                            } else {
                            }
                        }
                    }), (Consumer)(new Consumer() {
                        // $FF: synthetic method
                        // $FF: bridge method
                        public void accept(Object var1) {
                            this.accept((Throwable)var1);
                        }

                        public final void accept(Throwable error) {
                            mDialog.cancel();
                            Toast.makeText(AddExpensesActivity.this, getString(R.string.connection_to_database_failed), Toast.LENGTH_SHORT).show();
                            error.printStackTrace();
                            System.out.println("aaaaaaaa  error "+error.getMessage());
                        }
                    })));

        }else {
            System.out.println("aaaaaaaaaa   else ");
        }
    }
}
