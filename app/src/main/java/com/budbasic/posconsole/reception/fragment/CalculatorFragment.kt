package com.budbasic.posconsole.reception.fragment

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.budbasic.posconsole.R

class CalculatorFragment : Fragment() {

    var mView: View? = null
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        if (view != null)
            mView = view
        else
            mView = inflater.inflate(R.layout.dashboard_fragment, container, false)
        //  initView()
        return mView
    }

}