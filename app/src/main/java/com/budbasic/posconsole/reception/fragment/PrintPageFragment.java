package com.budbasic.posconsole.reception.fragment;


import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.budbasic.global.CUC;
import com.budbasic.posconsole.R;
import com.budbasic.posconsole.adapter.PrintPageAdapter;
import com.budbasic.posconsole.dialogs.BloothDailog;
import com.budbasic.posconsole.dialogs.PrintpageDailogs;
import com.budbasic.posconsole.global.Apppreference;
import com.budbasic.posconsole.global.RasterDocument;
import com.budbasic.posconsole.global.StarBitmap;
import com.budbasic.posconsole.printer.sdk.BluetoothService;
import com.budbasic.posconsole.printer.sdk.Command;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.Writer;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.oned.Code128Writer;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;
import com.kotlindemo.model.GetEmployeeLogin;
import com.kotlindemo.model.GetEmployeeQenerateInvoice;
import com.kotlindemo.model.GetInvoiceDetails;
import com.kotlindemo.model.GetProducts;
import com.kotlindemo.model.GetSettingList;
import com.kotlindemo.model.GetTaxData;

import org.jetbrains.annotations.NotNull;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Set;
import java.util.UUID;

import kotlin.TypeCastException;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.StringCompanionObject;
import zj.com.customize.sdk.Other;

/**
 * A simple {@link Fragment} subclass.
 */
@SuppressLint("ValidFragment")
public class PrintPageFragment extends Fragment {


    @SuppressLint("ValidFragment")
    public PrintPageFragment(GetEmployeeLogin logindata) {
        // Required empty public constructor
        this.logindata=logindata;
    }
    ArrayList<GetSettingList> settingListing;
    private RecyclerView rv_setting_list;
    private PrintPageAdapter printPageAdapter;
    GetEmployeeLogin logindata;

    // android built in classes for bluetooth operations
    BluetoothAdapter mBluetoothAdapter;
    BluetoothSocket mmSocket;
    BluetoothDevice mmDevice;

    // needed for communication to bluetooth device / network
    OutputStream mmOutputStream;
    InputStream mmInputStream;
    Thread workerThread;

    byte[] readBuffer;
    int readBufferPosition;
    volatile boolean stopWorker;
    Bitmap btMap;
    Apppreference apppreference;
    private  int printercheck;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_print_page, container, false);
        rv_setting_list=view.findViewById(R.id.rv_setting_list);

        settingListing=new ArrayList<>();
        printercheck=0;
        apppreference=new Apppreference(getContext());
        initList();
        rv_setting_list.setLayoutManager(new GridLayoutManager(getContext(),3));
        printPageAdapter=new PrintPageAdapter(getContext(),settingListing,PrintPageFragment.this,logindata);
        rv_setting_list.setAdapter(printPageAdapter);
       /* try {
            findBT();
            openBT();
        } catch (IOException ex) {
            ex.printStackTrace();
        }*/

        return view;
    }

    public void initList() {
        settingListing.add(new GetSettingList(0, R.drawable.ic_printerqrcode, "PRINT QR CODE"));
            settingListing.add(new GetSettingList(1, R.drawable.ic_labelprinter_connection, "PRINTER LABEL"));
        settingListing.add(new GetSettingList(2, R.drawable.ic_receipt_connection, "PRINTER BARCODE"));
        settingListing.add(new GetSettingList(3, R.drawable.print_connection, "PRINTER CONNECTION"));
        settingListing.add(new GetSettingList(3, R.drawable.blooth_connection, "BLUETOOTH CONNECTION"));
    }


    public void showprintlable() {
        PrintpageDailogs printpageDailogs=new PrintpageDailogs();
        printpageDailogs.showDialog(getContext(),PrintPageFragment.this,logindata);
    }

    public void showlabledialog(GetProducts getProducts, Dialog dialog) {
        try {
            findBT();
            openBT();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        if (printercheck==1){
            PrintpageDailogs printpageDailogs=new PrintpageDailogs();
            printpageDailogs.showDialog(getContext(),PrintPageFragment.this,logindata,getProducts,dialog);
        }else {
            showAlert("Label Printer has not be configured.\n Please do so in the settings");
        }

    }

    public void showPrintlable(int position) {
        try {
            findBT();
            openBT();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        if (printercheck==1){
            BloothDailog bloothDailog=new BloothDailog();
            bloothDailog.showDialog(getContext(),1,PrintPageFragment.this,position);
        }else {
            showAlert("Label Printer has not be configured.\n Please do so in the settings");
        }

    }
    public void showAlert(String msg){
        AlertDialog.Builder builder1 = new AlertDialog.Builder(getContext());
        builder1.setMessage(""+msg);
        builder1.setCancelable(true);

        builder1.setPositiveButton(
                "Ok",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        AlertDialog alert11 = builder1.create();
        alert11.show();
    }

    public void printLable(Bitmap bitmap, LinearLayout rlLogos) {
        int nPaperWidth = 576;
     //   sendPrintReceipt(bitmap,rlLogos);
        try {
            try {
                findBT();
                openBT();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
            // the text typed by the user
            String msg = "hello";
            msg += "\n";
            System.out.println("aaaaaaaaaaa  msg "+msg);
            // mmOutputStream.write(msg.getBytes());
           mmOutputStream.write(msg.getBytes());


            String barCodeVal = "ASDFC028060000005";
            System.out.println("aaaaaaaa  Barcode Length : "
                    + barCodeVal.length());
            int n1 = barCodeVal.length();
            //mmOutputStream.write(intToByteArray(n1));

            for (int i = 0; i < barCodeVal.length(); i++) {
                mmOutputStream.write((barCodeVal.charAt(i) + "").getBytes());
            }

            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
            byte[] byteArray = stream.toByteArray();
            mmOutputStream.write(byteArray);

            bitmap.recycle();

        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("aaaaaaaaaaaaa  error  "+e.getMessage());
            Toast.makeText(getContext(), ""+e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    public void print(Bitmap bitmap, LinearLayout rlLogos,String entretext){
        int nPaperWidth = 576;
        ArrayList mCommands = new ArrayList();
        RasterDocument rasterDoc = new RasterDocument(RasterDocument.RasSpeed.Medium, RasterDocument.RasPageEndMode.None,
                RasterDocument.RasPageEndMode.None, RasterDocument.RasTopMargin.Small, 0, 0, 0);

        rlLogos.setDrawingCacheEnabled(true);
        rlLogos.buildDrawingCache();
        Bitmap newBitmap = rlLogos.getDrawingCache();
        StarBitmap starbitmap = new StarBitmap(newBitmap, false, nPaperWidth);
        Command.alignmentCommand[3] = 49;
        byte[] var10001 = Command.alignmentCommand;

        this.SendDataByte(var10001);
        mCommands.add(rasterDoc.BeginDocumentCommandData());
        mCommands.add(starbitmap.getImageRasterDataForPrinting_Standard(true));
        mCommands.add(rasterDoc.EndDocumentCommandData());
        Iterator var9 = mCommands.iterator();

        while(var9.hasNext()) {
            byte[] data = (byte[])var9.next();
           // Intrinsics.checkExpressionValueIsNotNull(data, "data");
            this.SendDataByte(data);
        }
        SendDataString(""+entretext);
        SendDataString("\n\n\n\n");
        SendDataByte(Command.ESC_d);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        try {
            mmOutputStream.close();
            mmInputStream.close();
            mmSocket.close();
            System.out.println("Bluetooth Closed");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void SendDataString(String data) {
       /* Log.i("BTPWRITE", data);
        if (mService!!.getState() !== BluetoothService.STATE_CONNECTED) {
            Toast.makeText(this@SalesReceiptPrintActivity, getText(R.string.not_connected), Toast.LENGTH_SHORT)
                    .show()
            return
        }
        if (data.length > 0) {
            try {
                Log.i("BTPWRITE", data)
                mService!!.write(data.toByteArray(charset("GBK")))
            } catch (e: UnsupportedEncodingException) {
                e.printStackTrace()
            }

        }*/

        try {
            mmOutputStream.write(data.getBytes());
        } catch (IOException e) {
            System.out.println("aaaaaaaaaaa   exception "+e.getMessage());
            e.printStackTrace();
        }
    }


    public final void sendPrintReceipt(Bitmap mBitmap,LinearLayout rlLogos) {
       // Intrinsics.checkParameterIsNotNull(mBitmap, "mBitmap");

               // if (this.mService != null) {

                        int nPaperWidth = 576;
                        ArrayList mCommands = new ArrayList();
                        RasterDocument rasterDoc = new RasterDocument(RasterDocument.RasSpeed.Medium,
                                RasterDocument.RasPageEndMode.None, RasterDocument.RasPageEndMode.None,
                                RasterDocument.RasTopMargin.Small, 0, 0, 0);

                        rlLogos.setDrawingCacheEnabled(true);
                        Bitmap newBitmap = rlLogos.getDrawingCache();
                        StarBitmap starbitmap = new StarBitmap(newBitmap, false, nPaperWidth);
                        Command.alignmentCommand[3] = 49;
                        byte[] var10001 = Command.alignmentCommand;
                        System.out.println("aaaaaaaaaaaa  byte "+var10001+"   bitmap  "+newBitmap+" starbitmap  "+starbitmap);
                        this.SendDataByte(var10001);
                        mCommands.add(rasterDoc.BeginDocumentCommandData());
                        mCommands.add(starbitmap.getImageRasterDataForPrinting_Standard(true));
                        mCommands.add(rasterDoc.EndDocumentCommandData());

              //  } else {
                  //  this.setResult(-1);
                //                   // this.finish();

          //  }


    }
    private final void SendDataByte(byte[] data) {
        try {
            mmOutputStream.write(data);
        } catch (IOException e) {
            System.out.println("aaaaaaaaaaa   exception "+e.getMessage());
            e.printStackTrace();
        }
    }
    void findBT() {

        try {
            mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

            if(mBluetoothAdapter == null) {
              //  myLabel.setText("No bluetooth adapter available");
            }

            if(!mBluetoothAdapter.isEnabled()) {
                Intent enableBluetooth = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                startActivityForResult(enableBluetooth, 0);
            }

            Set<BluetoothDevice> pairedDevices = mBluetoothAdapter.getBondedDevices();

            if(pairedDevices.size() > 0) {
                for (BluetoothDevice device : pairedDevices) {
                    System.out.println("aaaaaaaaaa address  "+device.getAddress()+"  name  "+device.getName()+"  uuid "+device.getUuids());
                    // RPP300 is the name of the bluetooth printer device
                    // we got this name from the list of paired devices
                    System.out.println("aaaaaaaaa   getPrinterName "+apppreference.getPrinterName());
                    if (device.getName().equals(apppreference.getPrinterName())) {
                        mmDevice = device;
                        System.out.println("aaaaaaaaaaa  ifff   equal");
                        break;
                    }
                }
            }

          //  myLabel.setText("Bluetooth device found.");

        }catch(Exception e){
            e.printStackTrace();
        }
    }

    void openBT() throws IOException {
        try {

            // Standard SerialPortService ID
            UUID uuid = UUID.fromString("00001101-0000-1000-8000-00805f9b34fb");
            mmSocket = mmDevice.createRfcommSocketToServiceRecord(uuid);
            mmSocket.connect();
            mmOutputStream = mmSocket.getOutputStream();
            mmInputStream = mmSocket.getInputStream();
            beginListenForData();
            printercheck=1;
            System.out.println("aaaaaaaaa  opened");
          //  myLabel.setText("Bluetooth Opened");

        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("aaaaaaaaa  catch "+e.getMessage());
        }
    }

    void beginListenForData() {
        try {
            final Handler handler = new Handler();

            // this is the ASCII code for a newline character
            final byte delimiter = 10;

            stopWorker = false;
            readBufferPosition = 0;
            readBuffer = new byte[1024];

            workerThread = new Thread(new Runnable() {
                public void run() {

                    while (!Thread.currentThread().isInterrupted() && !stopWorker) {

                        try {

                            int bytesAvailable = mmInputStream.available();

                            if (bytesAvailable > 0) {

                                byte[] packetBytes = new byte[bytesAvailable];
                                mmInputStream.read(packetBytes);

                                for (int i = 0; i < bytesAvailable; i++) {

                                    byte b = packetBytes[i];
                                    if (b == delimiter) {

                                        byte[] encodedBytes = new byte[readBufferPosition];
                                        System.arraycopy(
                                                readBuffer, 0,
                                                encodedBytes, 0,
                                                encodedBytes.length
                                        );

                                        // specify US-ASCII encoding
                                        final String data = new String(encodedBytes, "US-ASCII");
                                        readBufferPosition = 0;

                                        // tell the user data were sent to bluetooth printer device
                                        handler.post(new Runnable() {
                                            public void run() {
                                             //   myLabel.setText(data);
                                            }
                                        });

                                    } else {
                                        readBuffer[readBufferPosition++] = b;
                                    }
                                }
                            }

                        } catch (IOException ex) {
                            stopWorker = true;
                        }

                    }
                }
            });

            workerThread.start();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
