package com.budbasic.posconsole.reception.fragment

import android.app.Activity
import android.app.DatePickerDialog
import android.app.Dialog
import android.graphics.Color
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.budbasic.customer.global.Preferences
import com.budbasic.global.CUC
import com.budbasic.posconsole.R
import com.budbasic.posconsole.model.APIClass
import com.budbasic.posconsole.reception.StoreManagerActivity
import com.budbasic.posconsole.webservice.RequetsInterface
import com.bumptech.glide.Glide
import com.github.mikephil.charting.charts.LineChart
import com.github.mikephil.charting.components.AxisBase
import com.github.mikephil.charting.components.XAxis
import com.github.mikephil.charting.components.YAxis
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.LineData
import com.github.mikephil.charting.data.LineDataSet
import com.github.mikephil.charting.formatter.IAxisValueFormatter
import com.github.mikephil.charting.highlight.Highlight
import com.github.mikephil.charting.listener.OnChartValueSelectedListener
import com.github.mikephil.charting.utils.ColorTemplate
import com.google.gson.Gson
import com.kotlindemo.model.*
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.graph_report_fragment1.*
import kotlinx.android.synthetic.main.graph_report_fragment1.view.*
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.set

class GraphReportFragment : Fragment(), APIClass.onCallListner, OnChartValueSelectedListener {

    var myFirstCalendar = Calendar.getInstance()
    var mySecondCalendar = Calendar.getInstance()
    lateinit var context: StoreManagerActivity
    val yAxisValues: ArrayList<String>? = ArrayList()
    val axisValues: ArrayList<String>? = ArrayList()
    var recyclerView: RecyclerView? = null

    val entries: ArrayList<Entry> = ArrayList()

    var productTopAdapter: ProductTopAdapter? = null
    var patientAdapter: PatientAdapter? = null
    var employeeAdapter: EmployeeAdapter? = null

    var topinvoicesdata = ArrayList<topinvoices>()

    var topproduct = ArrayList<items>()

    //EmployeeStoreAnaliticsData

    var productTabData = ArrayList<ProductTabData>()
    var employeeTabData = ArrayList<EmployeeTabData>()
    var patientabData = ArrayList<PatintTabData>()
    var Hour_sales = ArrayList<Hour_sales>()
    var employeeStoreSalesData: EmployeeStoreSalesData? = null

    var hourSales: HashMap<String, Any>? = null
    // var top
    override fun onFailed(requestCode: Int) {
    }

    override fun onSuccess(requestCode: Int) {

    }

    val EMPLOYEEQUEUELIST = 4
    var mView: View? = null
    var mDialog: Dialog? = null
    var logindata: GetEmployeeLogin? = null

    var selectDate = ""
    var isLoading = false
    var isLastPage = false
    var apiClass: APIClass? = null

    var appSettingData: GetEmployeeAppSetting? = null

    var chart: LineChart? = null

    var patient: TextView? = null
    var employee: TextView? = null
    var product: TextView? = null
    var tvStartDate: TextView? = null
    var tvEndDate: TextView? = null
    var patientImagePath = ""
    var productImagePath = ""

    companion object {

        var Currency_value = ""
        var list: ArrayList<GetInvoiceOrders> = ArrayList()
    }


    override fun onAttach(activity: Activity?) {
        super.onAttach(activity)
        try {
            context = (activity as StoreManagerActivity?)!!
        } catch (e: ClassCastException) {
            throw ClassCastException(activity!!.toString() + " must implement onViewSelected")
        }

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        mView = inflater.inflate(R.layout.graph_report_fragment1, container, false)
        apiClass = APIClass(context!!, this)
        appSettingData = Gson().fromJson(Preferences.getPreference(mView!!.context, "AppSetting"), GetEmployeeAppSetting::class.java)
        logindata = Gson().fromJson(Preferences.getPreference(mView!!.context, "logindata"), GetEmployeeLogin::class.java)
        Currency_value = "${appSettingData!!.Currency_value}"

        init(mView!!)
        //    GetFirstAndEndDate()
        recyclerView = mView!!.findViewById(R.id.rv_toplist)
        val layoutManager = LinearLayoutManager(mView!!.context, LinearLayoutManager.VERTICAL, false)
        mView!!.rv_toplist!!.layoutManager = layoutManager
        // mView!!.rv_toplist.adapter=productTopAdapter
        // recyclerView!!.layoutManager = LinearLayoutManager(context)

        //   initChart(mView!!)

        AllViewOnClick(mView!!)
        return mView
    }

    fun init(view: View) {
        tvStartDate = view.findViewById(R.id.tvStartDate)
        tvEndDate = view.findViewById(R.id.tvEndDate)
        chart = view.findViewById(R.id.lineChartData)
        // tvStartDate!!.setText("2018-12-01")
        // tvEndDate!!.setText("2018-12-31")
        if (logindata != null) {
            view.tv_head_title.setText(logindata!!.user_store.toString())
        }


        GetFirstAndEndDate()
        // callEmployeeStoreAnalytics()


    }


    fun AllViewOnClick(view: View) {
        mView!!.btn_back.setOnClickListener {
            activity!!.onBackPressed()
        }
        patient = view.findViewById(R.id.tv_btn_patient)
        patient!!.setOnClickListener {
            //   if (patientabData != null && patientabData.isNotEmpty() && patientabData.size > 0) {
            patientAdapter = PatientAdapter()
            recyclerView!!.adapter = patientAdapter
            patient!!.setBackgroundDrawable(resources.getDrawable(R.drawable.gradiant_line_background))
            employee!!.setBackgroundDrawable(resources.getDrawable(R.drawable.gray_line_background_toside))
            product!!.setBackgroundDrawable(resources.getDrawable(R.drawable.gray_line_background_toside))
            //  }
        }
        employee = view.findViewById(R.id.tv_btn_employee)
        employee!!.setOnClickListener {
            //  if (employeeTabData != null && employeeTabData.isNotEmpty() && employeeTabData.size > 0) {
            employeeAdapter = EmployeeAdapter()
            recyclerView!!.adapter = employeeAdapter
            employee!!.setBackgroundDrawable(resources.getDrawable(R.drawable.gradiant_line_background_toside))
            patient!!.setBackgroundDrawable(resources.getDrawable(R.drawable.gray_line_background))
            product!!.setBackgroundDrawable(resources.getDrawable(R.drawable.gray_line_background_toside))

            //  }
        }
        product = view.findViewById(R.id.tv_btn_product)
        product!!.setOnClickListener {
            // if (productTabData != null && productTabData.isNotEmpty() && productTabData.size > 0) {
            productTopAdapter = ProductTopAdapter()
            recyclerView!!.adapter = productTopAdapter
            product!!.setBackgroundDrawable(resources.getDrawable(R.drawable.gradiant_line_background_toside))
            employee!!.setBackgroundDrawable(resources.getDrawable(R.drawable.gray_line_background_toside))
            patient!!.setBackgroundDrawable(resources.getDrawable(R.drawable.gray_line_background))
            // }
        }
        tvStartDate!!.setOnClickListener {

            val mDate = DatePickerDialog(activity!!, startDate, myFirstCalendar
                    .get(Calendar.YEAR), myFirstCalendar.get(Calendar.MONTH),
                    myFirstCalendar.get(Calendar.DAY_OF_MONTH))
            //   mDate.getDatePicker().setMinDate(System.currentTimeMillis() + 1000)
            mDate.show()

        }
        tvEndDate!!.setOnClickListener {
            val mDate = DatePickerDialog(activity!!, endDate, mySecondCalendar
                    .get(Calendar.YEAR), mySecondCalendar.get(Calendar.MONTH),
                    mySecondCalendar.get(Calendar.DAY_OF_MONTH))
            //  mDate.getDatePicker().setMinDate(System.currentTimeMillis() + 1000)
            mDate.show()
        }
    }

    fun GetFirstAndEndDate() {

        val myFormat = "yyyy-MM-dd" //In which you need put here
        // val myFormat = "dd/MM/yy"
        val sdf = SimpleDateFormat(myFormat, Locale.US)


        val aCalendar = Calendar.getInstance()
        aCalendar.add(Calendar.MONTH, 0)
        aCalendar.set(Calendar.DATE, 1)
        val firstDateOfPreviousMonth = aCalendar.getTime()
        //  Log.d("FirstDate"," = "+firstDateOfPreviousMonth)

        tvStartDate!!.setText(sdf.format(firstDateOfPreviousMonth))

        aCalendar.set(Calendar.DATE, aCalendar.getActualMaximum(Calendar.DAY_OF_MONTH))
        val lastDateOfPreviousMonth = aCalendar.getTime()
        //   Log.d("EndDate"," = "+lastDateOfPreviousMonth)
        tvEndDate!!.setText(sdf.format(lastDateOfPreviousMonth))
        callEmployeeStoreAnalytics()
    }

    var startDate: DatePickerDialog.OnDateSetListener = DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->

        myFirstCalendar.set(Calendar.YEAR, year)
        myFirstCalendar.set(Calendar.MONTH, monthOfYear)
        myFirstCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)
        /* if(myCalendar.before(cal)){
             return@OnDateSetListener
         }*/
        updateLabelStartDate()
    }
    var endDate: DatePickerDialog.OnDateSetListener = DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->

        mySecondCalendar.set(Calendar.YEAR, year)
        mySecondCalendar.set(Calendar.MONTH, monthOfYear)
        mySecondCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)
        /* if(myCalendar.before(cal)){
             return@OnDateSetListener
         }*/
        updateLabelEndDate()
    }

    private fun updateLabelStartDate() {
        val myFormat = "yyyy-MM-dd" //In which you need put here
        //val myFormat = "dd/MM/yy"
        val sdf = SimpleDateFormat(myFormat, Locale.US)
        tvStartDate!!.setText(sdf.format(myFirstCalendar.time))
        callEmployeeStoreAnalytics()

    }

    private fun updateLabelEndDate() {
        val myFormat = "yyyy-MM-dd" //In which you need put here
        // val myFormat = "dd/MM/yy"
        val sdf = SimpleDateFormat(myFormat, Locale.US)
        tvEndDate!!.setText(sdf.format(mySecondCalendar.time))
        callEmployeeStoreAnalytics()

    }


    fun callEmployeeStoreAnalytics() {
        //    Log.i("Result", "callEmployeeReportMetric()")
        //   Log.d("AAAAA",""+tvEndDate!!.text.toString())
        //    Log.d("AAAAA11",""+tvStartDate!!.text.toString())
        mDialog = CUC.createDialog(activity!!)
        val apiService = RequetsInterface.Factory.create()
        val parameterMap = HashMap<String, String>()
        parameterMap["api_key"] = "${Preferences.getPreference(activity!!, "API_KEY")}"
        if (logindata != null) {
            parameterMap["employee_id"] = "${logindata!!.user_id}"
        }
        parameterMap["store_id"] = logindata!!.user_store_id
        parameterMap["date_from"] = tvStartDate!!.text.toString()
        parameterMap["date_to"] = tvEndDate!!.text.toString()
        Log.i("Strore id", logindata!!.user_store_id)
        println("aaaaaaaaaaaa  graph result   "+parameterMap.toString())
        Log.i("aaaaaaaaa Result", "$parameterMap")
        CompositeDisposable().add(apiService.callEmployeeStoreAnalytics(parameterMap)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe { result ->
                    if (mDialog != null && mDialog!!.isShowing)
                        mDialog!!.dismiss()
                    if (result != null) {
                        val GetEmployeeStoreAnalytics = result
                        println("aaaaaaaaaaaa  graph result   "+result.toString())
                        Log.i("aaaaa  graph Result", "" + result.toString())

                        if (GetEmployeeStoreAnalytics != null) {
                            if (GetEmployeeStoreAnalytics.store_sales != null) {
                                employeeStoreSalesData = GetEmployeeStoreAnalytics.store_sales!!
                                if (employeeStoreSalesData != null) {
                                    if (employeeStoreSalesData!!.Employee_tab != null && employeeStoreSalesData!!.Employee_tab!!.isNotEmpty()) {
                                        employeeTabData.clear()
                                        employeeTabData = employeeStoreSalesData!!.Employee_tab!!
                                      /*  employeeAdapter = EmployeeAdapter()
                                        recyclerView!!.adapter = employeeAdapter
                                        employee!!.setBackgroundDrawable(resources.getDrawable(R.drawable.gradiant_line_background_toside))
                                        patient!!.setBackgroundDrawable(resources.getDrawable(R.drawable.gray_line_background))
                                        product!!.setBackgroundDrawable(resources.getDrawable(R.drawable.gray_line_background_toside))*/
                                    } else {
                                        employeeTabData.clear()
                                       /* employeeAdapter = EmployeeAdapter()
                                        recyclerView!!.adapter = employeeAdapter
                                        employee!!.setBackgroundDrawable(resources.getDrawable(R.drawable.gradiant_line_background_toside))
                                        patient!!.setBackgroundDrawable(resources.getDrawable(R.drawable.gray_line_background))
                                        product!!.setBackgroundDrawable(resources.getDrawable(R.drawable.gray_line_background_toside))*/
                                    }
                                    if (employeeStoreSalesData!!.Patient_tab != null && employeeStoreSalesData!!.Patient_tab!!.isNotEmpty()) {
                                        patientabData.clear()
                                        patientabData = employeeStoreSalesData!!.Patient_tab!!
                                        patientAdapter = PatientAdapter()
                                        recyclerView!!.adapter = patientAdapter
                                        patient!!.setBackgroundDrawable(resources.getDrawable(R.drawable.gradiant_line_background))
                                        employee!!.setBackgroundDrawable(resources.getDrawable(R.drawable.gray_line_background_toside))
                                        product!!.setBackgroundDrawable(resources.getDrawable(R.drawable.gray_line_background_toside))
                                    } else {
                                        patientabData.clear()
                                        patientAdapter = PatientAdapter()
                                        recyclerView!!.adapter = patientAdapter
                                        patient!!.setBackgroundDrawable(resources.getDrawable(R.drawable.gradiant_line_background))
                                        employee!!.setBackgroundDrawable(resources.getDrawable(R.drawable.gray_line_background_toside))
                                        product!!.setBackgroundDrawable(resources.getDrawable(R.drawable.gray_line_background_toside))
                                    }
                                    if (employeeStoreSalesData!!.Product_tab != null && employeeStoreSalesData!!.Product_tab!!.isNotEmpty()) {
                                        productTabData.clear()
                                        productTabData = employeeStoreSalesData!!.Product_tab!!

                                    } else {
                                        productTabData.clear()
                                    }
                                    if (employeeStoreSalesData!!.Hour_sales != null && employeeStoreSalesData!!.Hour_sales!!.isNotEmpty()) {
                                        Hour_sales.clear()
                                        Hour_sales = employeeStoreSalesData!!.Hour_sales!!

                                        setGraphData()
                                    } else {
                                        Hour_sales.clear()
                                        setGraphData()
                                    }
                                    patientImagePath = GetEmployeeStoreAnalytics.patient_image!!
                                    productImagePath = GetEmployeeStoreAnalytics.product_image!!

                                    if (isNullOrEmpty(employeeStoreSalesData!!.total_sales.toString())) {
                                        mView!!.tv_total_sales.setText("$ 0.00")
                                        tv_gross.setText("$ 0.00")
                                    } else {
                                        mView!!.tv_total_sales.setText("$" + employeeStoreSalesData!!.total_sales)
                                        tv_gross.setText("$" + employeeStoreSalesData!!.total_sales)
                                    }
                                    if (isNullOrEmpty(employeeStoreSalesData!!.total_tax.toString())) {
                                        tv_tax.setText("$ 0.00")
                                    } else {
                                        tv_tax.setText("$" + employeeStoreSalesData!!.total_tax)
                                    }
                                    if (isNullOrEmpty(employeeStoreSalesData!!.net_total.toString())) {
                                        tv_net.setText("$ 0.00")
                                    } else {
                                        tv_net.setText("$" + employeeStoreSalesData!!.net_total)
                                    }
                                    /* if(employeeStoreSalesData!!.total_sales!!.isBlank()){
                                         tv_total_sales.setText("$ 0.00")
                                         tv_gross.setText("$ 0.00")
                                     }else{

                                         tv_total_sales.setText("$" + employeeStoreSalesData!!.total_sales)
                                         tv_gross.setText("$" + employeeStoreSalesData!!.total_sales)
                                     }
                                     if(employeeStoreSalesData!!.total_tax!!.isBlank()){
                                         tv_tax.setText("$ 0.00")

                                     }else{
                                         tv_tax.setText("$" + employeeStoreSalesData!!.total_tax)
                                     }

                                     if(!employeeStoreSalesData!!.net_total!!.isBlank()){
                                         tv_net.setText("$ 0.00")
                                     }else{
                                         tv_net.setText("$" + employeeStoreSalesData!!.net_total)
                                     }
 */

                                    amount_patients.setText(employeeStoreSalesData!!.total_patient)
                                    amount_new_patient.setText(employeeStoreSalesData!!.new_patient)
                                    amount_avg_speed.setText("$" + employeeStoreSalesData!!.average_spent)
                                    amount_item_sold.setText(employeeStoreSalesData!!.item_sold)


                                }

                            }
                        }


                    }


                }

        )


    }

    fun isNullOrEmpty(str: String?): Boolean {
        return if (str != null && !str.isEmpty() && !str.equals("null")) false else true
    }

    fun setGraphData() {
        //  Log.d("AAAA","AAAA")
        //  chart!!.clearValues()
        chart!!.clear()

        val values1: ArrayList<Entry> = ArrayList()
        if (Hour_sales != null && Hour_sales.isNotEmpty() && Hour_sales.size > 0) {
            for (i in Hour_sales.indices) {
                println("aaaaaaaaaa   "+Hour_sales.get(i).time+"   "+Hour_sales.get(i).time!!.length+"   "+Hour_sales.get(i).price.toString())
                println("aaaaaaa   "+Hour_sales.get(i).time!!.split("\\s".toRegex())[0]);

                var  time=Hour_sales.get(i).time!!.split("\\s".toRegex())[0];
                time.split(":".toRegex())[0];

                values1.add(Entry((time.split(":".toRegex())[0]).toFloat(), Hour_sales.get(i).price.toString().toFloat()))

               // values1.add(Entry(Hour_sales.get(i).time!!.substring(0, Hour_sales.get(i).time!!.length - 2).toFloat(), Hour_sales.get(i).price.toString().toFloat()))
            }
        }


        val set1 = LineDataSet(values1, "DataSet 1")
        set1.axisDependency = YAxis.AxisDependency.LEFT
        set1.color = resources.getColor(R.color.yellowlight)
        set1.disableDashedLine()
        set1.setCircleColor(resources.getColor(R.color.yellowlight))
        set1.lineWidth = 2f
        set1.circleRadius = 0f
        set1.fillAlpha = 65
        set1.fillColor = ColorTemplate.getHoloBlue()
        set1.highLightColor = Color.rgb(244, 117, 117)
        set1.setDrawCircleHole(false)
        set1.setMode(LineDataSet.Mode.CUBIC_BEZIER)
        val data = LineData(set1)
        //   data.setValueTextColor(Color.BLACK)
        data.setValueTextSize(10f)
        chart!!.setBackgroundColor(Color.WHITE)
        chart!!.axisLeft.setDrawGridLines(false)
        chart!!.getXAxis().setDrawGridLines(false)
        chart!!.setDrawGridBackground(false)
        chart!!.xAxis.setGranularity(1f)
        // chart!!.xAxis.setDrawAxisLine(false)
        chart!!.getAxisRight().setDrawAxisLine(false)
        chart!!.getDescription().setEnabled(false)
        chart!!.setPinchZoom(false)
        chart!!.getAxisLeft().setDrawLabels(true)
        chart!!.getAxisRight().setDrawLabels(false)
        chart!!.getXAxis().setValueFormatter(object : IAxisValueFormatter {
            override fun getFormattedValue(value: Float, axis: AxisBase?): String {

                if (value.toInt() < 13) {
                    return value.toInt().toString() + "AM"
                } else {
                    return value.toInt().toString() + "PM"
                }

            }
        })
        chart!!.getXAxis().setDrawLabels(true)
        chart!!.getXAxis().setPosition(XAxis.XAxisPosition.BOTTOM)
        chart!!.getLegend().setEnabled(false)
        chart!!.setData(data)

    }

    inner class ProductTopAdapter : RecyclerView.Adapter<ProductTopAdapter.ViewHolder>() {

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            holder.tv_suggest_total_sale.setText("TOTAL SELL : ")
            holder.tv_suggest_avg_sale.setText("AVG QT SOLD : ")
            holder.amount_avg_sale.setText( productTabData.get(position).packet_average_qty_sold)
            holder.amount_total_sale.setText("$"+ productTabData.get(position).packet_total_sell)
            holder.userName.setText(productTabData.get(position).product_name)
            if (productTabData.get(position).product_image!!.isNotEmpty()) {
                Glide.with(activity!!).load(productImagePath + productTabData.get(position).product_image).into(holder.imagProfili)
            }
            holder.llView.setOnClickListener {
                val document = PatientDetailFragment()
                val bundle: Bundle = Bundle()
                bundle.putString("Details", "Product")
                bundle.putString("ProductId", productTabData.get(position).product_id)
                bundle.putString("packet_id", productTabData.get(position).packet_id)
                bundle.putString("StartDate", tvStartDate!!.text.toString())
                bundle.putString("EndDate", tvEndDate!!.text.toString())
                bundle.putString("totalsale", productTabData.get(position).packet_total_sell)
                document.arguments = bundle
                (activity as StoreManagerActivity).setFragmentWithBack(document, false, null, PatientDetailFragment::class.java.simpleName)
                // (activity as StoreManagerActivity).addOrReplace(document, PatientDetailFragment.fragmentName)
            }

        }

        override fun getItemCount(): Int {
            return productTabData.size
            // return 10
            //   Log.i("Top size", topproduct.size.toString())
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            val v = LayoutInflater.from(parent!!.context).inflate(R.layout.graph_analytic_item_view, parent, false)
            return ViewHolder(v)
        }

        inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
            val userName: TextView = itemView.findViewById(R.id.userName)
            val amount_total_sale: TextView = itemView.findViewById(R.id.amount_total_sale)
            val amount_avg_sale: TextView = itemView.findViewById(R.id.amount_avg_sale)
            val llView: TextView = itemView.findViewById(R.id.llView)
            val imagProfili: ImageView = itemView.findViewById(R.id.imagProfilie)
            val tv_suggest_total_sale: TextView = itemView.findViewById(R.id.tv_suggest_total_sale)
            val tv_suggest_avg_sale: TextView = itemView.findViewById(R.id.tv_suggest_avg_sale)
        }
    }

    inner class PatientAdapter : RecyclerView.Adapter<PatientAdapter.ViewHolder>() {

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {

            holder.tv_suggest_total_sale.setText("SPEND : ")
            holder.tv_suggest_avg_sale.setText("AVG SPEND : ")
            holder.amount_avg_sale.setText("$" + patientabData.get(position).average_sell)
            holder.amount_total_sale.setText("$" + patientabData.get(position).total_selling)
            holder.userName.setText(CUC.getCapsSentences(patientabData.get(position).patient_fname.toString()))
            if (patientabData.get(position).patient_image!!.isNotEmpty()) {
                Glide.with(activity!!).load(patientImagePath + patientabData.get(position).patient_image).into(holder.imagProfili)
            }
            holder.llView.setOnClickListener {
                val document = PatientDetailFragment()
                val bundle: Bundle = Bundle()
                bundle.putString("Details", "Patient")
                bundle.putString("PatientId", patientabData.get(position).patient_id)
                bundle.putString("StartDate", tvStartDate!!.text.toString())
                bundle.putString("EndDate", tvEndDate!!.text.toString())
                bundle.putString("TotalSell", patientabData.get(position).total_selling)
                bundle.putString("AvgSell", patientabData.get(position).average_sell)
                document.arguments = bundle
                (activity as StoreManagerActivity).setFragmentWithBack(document, false, null, PatientDetailFragment::class.java.simpleName)
                //(activity as StoreManagerActivity).addOrReplace(document, PatientDetailFragment.fragmentName)
            }

        }

        override fun getItemCount(): Int {
            return patientabData.size
            // return 10
            //   Log.i("Top size", topproduct.size.toString())
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            val v = LayoutInflater.from(parent!!.context).inflate(R.layout.graph_analytic_item_view, parent, false)
            return ViewHolder(v)
        }

        inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

            val userName: TextView = itemView.findViewById(R.id.userName)
            val amount_total_sale: TextView = itemView.findViewById(R.id.amount_total_sale)
            val amount_avg_sale: TextView = itemView.findViewById(R.id.amount_avg_sale)
            val llView: TextView = itemView.findViewById(R.id.llView)
            val imagProfili: ImageView = itemView.findViewById(R.id.imagProfilie)
            val tv_suggest_total_sale: TextView = itemView.findViewById(R.id.tv_suggest_total_sale)
            val tv_suggest_avg_sale: TextView = itemView.findViewById(R.id.tv_suggest_avg_sale)

        }
    }

    inner class EmployeeAdapter : RecyclerView.Adapter<EmployeeAdapter.ViewHolder>() {

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {

            holder.tv_suggest_total_sale.setText("TOTAL SELL : ")
            holder.tv_suggest_avg_sale.setText("AVG SELL : ")
            holder.amount_avg_sale.setText("$" + employeeTabData.get(position).average_sell)
            holder.amount_total_sale.setText("$" + employeeTabData.get(position).total_selling)
            holder.userName.setText(CUC.getCapsSentences(employeeTabData.get(position).emp_first_name.toString()))
            holder.llView.setOnClickListener {
                val document = PatientDetailFragment()
                val bundle: Bundle = Bundle()
                bundle.putString("Details", "Employee")
                bundle.putString("StartDate", tvStartDate!!.text.toString())
                bundle.putString("EndDate", tvEndDate!!.text.toString())
                bundle.putString("totalsale", employeeTabData.get(position).total_selling)
                bundle.putString("EmployeeId", employeeTabData.get(position).emp_id)
               // bundle.putString("TotalSell", employeeTabData.get(position).total_selling)
                bundle.putString("AvgSell", employeeTabData.get(position).average_sell)
                //  bundle.putString("TempId",employeeTabData.get(position).an)
                document.arguments = bundle
                (activity as StoreManagerActivity).setFragmentWithBack(document, false, null, PatientDetailFragment::class.java.simpleName)
                // (activity as StoreManagerActivity).addOrReplace(document, PatientDetailFragment.fragmentName)
            }

            //   Glide.with(activity!!).load(topproduct.get(position).product_image).into(holder.img_product)

        }

        override fun getItemCount(): Int {
            return employeeTabData.size
            // return 10
            //   Log.i("Top size", topproduct.size.toString())
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            val v = LayoutInflater.from(parent!!.context).inflate(R.layout.graph_analytic_item_view, parent, false)
            return ViewHolder(v)
        }

        inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
            val userName: TextView = itemView.findViewById(R.id.userName)
            val amount_total_sale: TextView = itemView.findViewById(R.id.amount_total_sale)
            val amount_avg_sale: TextView = itemView.findViewById(R.id.amount_avg_sale)
            val llView: TextView = itemView.findViewById(R.id.llView)
            val imagProfili: ImageView = itemView.findViewById(R.id.imagProfilie)
            val tv_suggest_total_sale: TextView = itemView.findViewById(R.id.tv_suggest_total_sale)
            val tv_suggest_avg_sale: TextView = itemView.findViewById(R.id.tv_suggest_avg_sale)

        }
    }

    override fun onNothingSelected() {
        // Toast.makeText(activity!!,"nothing selcted",Toast.LENGTH_LONG).show()

    }

    /*override fun onValueSelected(e: Entry?, dataSetIndex: Int, h: Highlight?) {


        //Toast.makeText(activity!!,"highlight"+h.toString(),Toast.LENGTH_LONG).show()
        //Toast.makeText(activity!!,"e"+e!!.xIndex.toString(),Toast.LENGTH_LONG).show()
        //Toast.makeText(activity!!,"invoice id"+topinvoicesdata.get(e!!.xIndex).invoice_id,Toast.LENGTH_LONG).show()
        //Toast.makeText(activity!!,"invoice amt"+topinvoicesdata.get(e!!.xIndex).invoice_total_payble,Toast.LENGTH_LONG).show()

        val dialog = Dialog(activity);
        dialog.getWindow().setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_barinvoicedata);
        val textinvoid: TextView = dialog.findViewById(R.id.tv_invid);
        val textinamt: TextView = dialog.findViewById(R.id.tv_paybleamt);
        textinamt.text = topinvoicesdata.get(e!!.xIndex).invoice_total_payble.toString()
        textinvoid.text = topinvoicesdata.get(e!!.xIndex).invoice_no.toString()
        dialog.show()
        //  barchart.isHighlightEnabled=true

        Handler().postDelayed({
            dialog.dismiss()
            //  BarDataSet.setBarShadowColor(int color)
            barchart.setDrawBarShadow(false)

        }, 1500)

    }*/


    /*   private fun callEmployeePatientCart() {
           if (CUC.isNetworkAvailablewithPopup(activity!!)) {
               mDialog = CUC.createDialog(activity!!)
               val apiService = RequetsInterface.Factory.create()
               val parameterMap = HashMap<String, String>()
               parameterMap["api_key"] = "${Preferences.getPreference(activity!!, "API_KEY")}"
               parameterMap["invoice_id"] = "$mainCartId"
               Log.i("Result", "parameterMap : $parameterMap")
               CompositeDisposable().add(apiService.callEmployeeInvoiceDetails(parameterMap)
                       .observeOn(AndroidSchedulers.mainThread())
                       .subscribeOn(Schedulers.io())
                       .subscribe({ result ->
                           if (mDialog != null && mDialog.isShowing)
                               mDialog.dismiss()
                           if (result != null) {
                               Log.i("Result", "" + result.toString())
                               Log.d("AAAAAA",""+result.invoice.invoice_manual_discount_type)
                               if (result.status == "0") {
                                   payment_path = result.payment_path
                                   Store_sign_required = "${result.Store_sign_required}"
                                   //cart_serve_type = "${result.main_cart.cart_serve_type}"
                                   if (result.invoice != null) {
                                       discount_value = result.invoice.invoice_manual_discount
                                       cart_manual_discount_type = result.invoice.invoice_manual_discount_type
                                   }
                                   /*  if (!intent!!.hasExtra("status")) {
                                         Log.d("AAAAAA11",""+result.invoice.invoice_manual_discount_type)
                                     } else*/// {
                                   Log.d("AAAAAA22",""+result.invoice.invoice_manual_discount_type)
                                   if (order_type == "1") {
                                       Log.d("AAAAAA33",""+result.invoice.invoice_manual_discount_type)
                                       if (logindata != null) {
                                           if (logindata.user_role == "2") {
                                               ll_redeem_option.visibility = View.GONE
                                           } else {
                                               ll_redeem_option.visibility = View.VISIBLE
                                           }
                                       } else {
                                           ll_redeem_option.visibility = View.GONE
                                       }

                                       ll_promoCode.visibility = View.VISIBLE
                                       if (result.invoice.invoice_promo_code != "") {
                                           ll_promoCode.setBackgroundResource(R.color.blue)
                                           tv_promoCode.text = "Remove Promo Code"
                                       } else {
                                           ll_promoCode.setBackgroundResource(R.color.blue)
                                           tv_promoCode.text = "Apply Promo Code"
                                       }

                                       tv_promoCode.setOnClickListener {
                                           if (tv_promoCode.text == "Remove Promo Code") {
                                               val builder = AlertDialog.Builder(activity!!)
                                               builder.setMessage(getString(R.string.are_you_sure_you_want_to_remove_promo_code))

                                               builder.setPositiveButton("Yes") { dialog, which ->
                                                   dialog.dismiss()
                                                   callEmployeeRemovePromocode()
                                               }
                                               builder.setNegativeButton("No") { dialog, which -> dialog.dismiss() }
                                               val alert = builder.create()
                                               alert.show()
                                           } else {
                                               val mDialog = PromoCodeDialog(this@EmployeeInvoiceDetailsActivity, object : PromoCodeDialog.onCallListner {
                                                   override fun onRefresh() {
                                                       callEmployeePatientCart()
                                                   }
                                               })
                                               val myBundle = Bundle()
                                               myBundle.putString("cart_id", mainCartId)
                                               myBundle.putString("patient_id", patient_id)
                                               myBundle.putString("app_status", "2")

                                               mDialog.arguments = myBundle
                                               mDialog.show(fragmentManager, "promo_code")
                                           }
                                       }
                                       Log.d("CartDiscountTypeAAAA","AASAAAAA")
                                       Log.d("CartDiscountType"," = "+cart_manual_discount_type)
                                       if (cart_manual_discount_type == "1" || cart_manual_discount_type == "2") {

                                           tv_applyremove.text = "Remove Discount"
                                           Log.d("Remove", " = ")
                                           Log.d("RemoveDis", " = ")
                                       } else {
                                           tv_applyremove.text = "Apply Discount"
                                           Log.d("Apply", " = ")
                                       }
                                   }
                                   //    }

                                   if(order_type=="2"){
                                       if (cart_manual_discount_type == "1" || cart_manual_discount_type == "2") {

                                           tv_applyremove.text = "Remove Discount"
                                           Log.d("Remove", " = ")
                                           Log.d("RemoveDis", " = ")
                                       } else {
                                           tv_applyremove.text = "Apply Discount"
                                           Log.d("Apply", " = ")
                                       }
                                   }
                                   EmployeeInvoiceDetailsActivity.taxList.clear()
                                   tv_total_qty.text = "$invoice_total_qty"
                                   tv_subtotal.text = "$Currency_value$invoice_sub_total"
                                   if (result.invoice.invoice_taxes != null && result.invoice.invoice_taxes.isNotEmpty()) {
                                       EmployeeInvoiceDetailsActivity.taxList = Gson().fromJson(result.invoice.invoice_taxes,
                                               object : TypeToken<java.util.ArrayList<GetTaxData>>() {

                                               }.type) as java.util.ArrayList<GetTaxData>
                                   }
                                   try {
                                       if (loyalty_amount != null && loyalty_amount != "") {
                                           if ("$loyalty_amount".toFloat() > 0) {
                                               EmployeeInvoiceDetailsActivity.taxList.add(GetTaxData("989", "Loyalty Amount", loyalty_amount))
                                           }
                                       }
                                   } catch (e: Exception) {
                                       e.printStackTrace()
                                   }
                                   try {
                                       if (discount_value != null && discount_value != "") {
                                           if ("$discount_value".toFloat() > 0) {
                                               if (cart_manual_discount_type == "2") {
                                                   EmployeeInvoiceDetailsActivity.taxList.add(GetTaxData("2", "Discount", discount_value, "2"))
                                               } else {
                                                   EmployeeInvoiceDetailsActivity.taxList.add(GetTaxData("2", "Discount", discount_value, "1"))
                                               }
                                           }
                                       }
                                   } catch (e: Exception) {
                                       e.printStackTrace()
                                   }
                                   if (result.invoice.invoice_promo_code != "") {
                                       EmployeeInvoiceDetailsActivity.taxList.add(GetTaxData("101", "Promo Code (${result.invoice.invoice_promo_code})", result.invoice.invoice_promo_discount))
                                   }
                                   EmployeeInvoiceDetailsActivity.taxList.add(GetTaxData("102", "Grand Total", result.invoice.invoice_total_payble, "1"))
                                   if (order_type == "3" || order_type == "4") {
                                       EmployeeInvoiceDetailsActivity.taxList.add(GetTaxData("103", "Paid Amount", result.invoice.invoice_paid_amount))
                                       EmployeeInvoiceDetailsActivity.taxList.add(GetTaxData("104", "Tender Cash", result.invoice.invoice_change))
                                       if (cart_manual_discount_type == "1" || cart_manual_discount_type == "2") {

                                           tv_applyremove.text = "Remove Discount"
                                           Log.d("Remove", " = ")
                                           Log.d("RemoveDis", " = ")
                                       } else {
                                           tv_applyremove.text = "Apply Discount"
                                           Log.d("Apply", " = ")
                                       }
                                   }
                                   taxItemAdapter.notifyDataSetChanged()
                                   if (result.invoice_detail != null && result.invoice_detail.size > 0) {
                                       list.clear()
                                       list.addAll(result.invoice_detail)
                                       productAdapter.notifyDataSetChanged()

                                       driverList.clear()
                                       driverList.addAll(result.drivers)

                                       paymentTypeList.clear()
                                       paymentTypeList.addAll(result.payment_types)
                                       Log.i(EmployeeInvoiceDetailsActivity::class.java.simpleName, "paymentTypeList : $paymentTypeList")
                                       Log.i("Result", "$driverList")
                                   } else {
                                       list.clear()
                                       productAdapter.notifyDataSetChanged()
                                       EmployeeInvoiceDetailsActivity.taxList.clear()
                                       taxItemAdapter.notifyDataSetChanged()
                                   }
                                   //Preferences.setPreference(this@EmployeeInvoiceDetailsActivity, "total_qty", result.total_qty)
                                   CUC.displayToast(this@EmployeeInvoiceDetailsActivity, result.show_status, result.message)
                               } else if (result.status == "10") {
                                   apiClass.callApiKey(EMPLOYEEPATIENTCART)
                               } else {
                                   list.clear()
                                   productAdapter.notifyDataSetChanged()
                                   EmployeeInvoiceDetailsActivity.taxList.clear()
                                   taxItemAdapter.notifyDataSetChanged()
                                   CUC.displayToast(this@EmployeeInvoiceDetailsActivity, result.show_status, result.message)
                               }

                           } else {
                               CUC.displayToast(this@EmployeeInvoiceDetailsActivity, "0", getString(R.string.connection_to_database_failed))
                           }

                       }, { error ->
                           if (mDialog != null && mDialog.isShowing)
                               mDialog.dismiss()
                           CUC.displayToast(this@EmployeeInvoiceDetailsActivity, "0", getString(R.string.connection_to_database_failed))
                           error.printStackTrace()
                       })
               )
           }
       }*/
    override fun onValueSelected(e: Entry?, h: Highlight?) {

        /*val dialog = Dialog(activity);
        dialog.getWindow().setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_barinvoicedata);
        val textinvoid: TextView = dialog.findViewById(R.id.tv_invid);
        val textinamt: TextView = dialog.findViewById(R.id.tv_paybleamt);

        textinamt.text = topinvoicesdata.get(h!!.x.toInt()).invoice_total_payble.toString()
        textinvoid.text = topinvoicesdata.get(h!!.x.toInt()).invoice_no.toString()
        dialog.show()
        //  barchart.isHighlightEnabled=true

        Handler().postDelayed({
            dialog.dismiss()
            //  BarDataSet.setBarShadowColor(int color)
            barchart.setDrawBarShadow(false)

        }, 1500)*/
    }
}


