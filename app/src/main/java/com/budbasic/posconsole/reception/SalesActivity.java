package com.budbasic.posconsole.reception;

import android.Manifest;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.usage.ExternalStorageStats;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.StrictMode;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.InputFilter;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;

import com.budbasic.customer.global.Preferences;
import com.budbasic.global.CUC;
import com.budbasic.posconsole.R;
import com.budbasic.posconsole.UserInfoDetailActivity;
import com.budbasic.posconsole.adapter.CgAdapter;
import com.budbasic.posconsole.adapter.ProductCartAdapter;
import com.budbasic.posconsole.adapter.TaxitemAdapter;
import com.budbasic.posconsole.customeview.CustomePieGraph;
import com.budbasic.posconsole.dialogs.AddPaymentDialog;
import com.budbasic.posconsole.dialogs.AddProductDialog;
import com.budbasic.posconsole.dialogs.AddPromocardDialog;
import com.budbasic.posconsole.dialogs.ProductquantityDialog;
import com.budbasic.posconsole.dialogs.ProductquantityupdateDialog;
import com.budbasic.posconsole.dialogs.PromoCodeDialog;
import com.budbasic.posconsole.driver.SignatureActivity;
import com.budbasic.posconsole.global.Apppreference;
import com.budbasic.posconsole.global.CalcWindowBasic;
import com.budbasic.posconsole.global.CalcWindowUnit;
import com.budbasic.posconsole.global.ChatDialog;
import com.budbasic.posconsole.global.EmployeeSearchProducts;
import com.budbasic.posconsole.model.APIClass;
import com.budbasic.posconsole.queue.BarCodeScan;
import com.budbasic.posconsole.queue.MenuActivity;
import com.budbasic.posconsole.queue.SaleQueueScan;
import com.budbasic.posconsole.sales.SalesReceiptPrintActivity;
import com.budbasic.posconsole.sales.UserInfoDetails;
import com.budbasic.posconsole.webservice.RequetsInterface;
import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.kotlindemo.model.EmployeePatientInfo;
import com.kotlindemo.model.GetCartData;
import com.kotlindemo.model.GetData;
import com.kotlindemo.model.GetEmployeeAppSetting;
import com.kotlindemo.model.GetEmployeeLogin;
import com.kotlindemo.model.GetEmployeePatientCart;
import com.kotlindemo.model.GetEmployeePatientPromocodeList;
import com.kotlindemo.model.GetEmployeeQenerateInvoice;
import com.kotlindemo.model.GetEmployeeSalesProducts;
import com.kotlindemo.model.GetEmployeeSalesSearchPatient;
import com.kotlindemo.model.GetEmployeeSearchProducts;
import com.kotlindemo.model.GetPaymentTypes;
import com.kotlindemo.model.GetProduct;
import com.kotlindemo.model.GetProducts;
import com.kotlindemo.model.GetPromosales;
import com.kotlindemo.model.GetQueueList;
import com.kotlindemo.model.GetSalesProduct;
import com.kotlindemo.model.GetSalesProductScan;
import com.kotlindemo.model.GetScanProducts;
import com.kotlindemo.model.GetStatus;
import com.kotlindemo.model.GetTaxData;
import com.kotlindemo.model.GetUpdateCart;
import com.kotlindemo.model.getCaregiver;
import com.kotlindemo.model.getCartadd;
import com.kotlindemo.model.getcaregivers;
import com.kotlindemo.model.notesdata;
import com.squareup.picasso.Picasso;
import com.starmicronics.starmgsio.ConnectionInfo;
import com.starmicronics.starmgsio.Scale;
import com.starmicronics.starmgsio.ScaleCallback;
import com.starmicronics.starmgsio.ScaleData;
import com.starmicronics.starmgsio.ScaleSetting;
import com.starmicronics.starmgsio.StarDeviceManager;

import org.jetbrains.annotations.Nullable;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import kotlin.jvm.internal.Intrinsics;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class SalesActivity extends AppCompatActivity  {

    private TabLayout tab_layout;
    private ViewPager mViewPager;
    private ImageView iv_lock,iv_home,scan_barcode;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    public Dialog mDialog;
    public GetEmployeeLogin logindata;
    private LinearLayout rl_bottom,ll_loginuser1,ll_prductinfo,ll_loginuser2;
    private String queue_patient_id,assign_msg,queue_id,mainCartId,payment_path,Store_sign_required,patient_point,discount_value,
            cart_manual_discount_type,Currency_value,total_payble,image_path;
    private List<GetData> cateList;
    List<GetPaymentTypes> paymentTypeList ;
    private List<GetProducts> searchdataArrayList;
    GetQueueList queueList;
    GetEmployeeAppSetting mBAsis;
    Gson gson;
    private ImageView ivpatientcart;
    private LinearLayout img_exmlator;
    private TextView tvcustomername,tv_limit_value,textLimitUsage,tv_current_order,tv_applyremove,tv_cartisempty,tv_total_qty,
            tv_subtotal,tv_promocode,tvGetPromoCode,tv_grandtotal;
    Float noOfAddedItem = 0f,pieGraphLimite=0f;
    GetEmployeePatientCart viewCard;
    List<GetCartData> list;
    private ProductCartAdapter productCartAdapter;
    private LinearLayout ll_redeem_option,ll_send_order,ll_cancel,ll_promoTab,total_unit;
    private RecyclerView rv_cart_list,rv_tax_list;
    List<GetTaxData> taxList;
    private TaxitemAdapter taxItemAdapter;
    String discount_type;
    private Dialog dialogProductQty;
    private ArrayList<GetPromosales> peomocedelist;
    private String siganture_path;
    private ArrayList permissionsToRequest;
    private ArrayList permissionsRejected = new ArrayList();
    private ArrayList permissions = new ArrayList();
    private final static int ALL_PERMISSIONS_RESULT = 107;
    int request_code = 7070, tabposition;
    Float paymentamt;
    ArrayList<notesdata> notsdata;
    private FragmentInterface fragmentInterfaceListener;
    private Scale mScale;
    private String weight;
    private Apppreference apppreference;
    ProductquantityDialog productquantityDialog;
    ProductquantityupdateDialog productquantityupdateDialog;
    GetStatus getstatus;
    GetSalesProduct getProductsa;
    GetCartData getCartData;
    private  ProductsFragment productsFragment;
    private ArrayList<getCaregiver> caregiverlist;
    private RecyclerView recyclecaregivers;
    private CgAdapter cgAdapter;
    int caregiver=0,cartdetails=0;
    String caregiverid;
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_sales);
       /* Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
*/
        tab_layout=findViewById(R.id.tab_layout);
        mViewPager=findViewById(R.id.mViewPager);
        mSwipeRefreshLayout=findViewById(R.id.mSwipeRefreshLayout);
        ivpatientcart=findViewById(R.id.ivpatientcart);
        tvcustomername=findViewById(R.id.tvcustomername);
        tv_limit_value=findViewById(R.id.tv_limit_value);
        textLimitUsage=findViewById(R.id.textLimitUsage);
        img_exmlator=findViewById(R.id.img_exmlator);
        tv_current_order=findViewById(R.id.tv_current_order);
        ll_send_order=findViewById(R.id.ll_send_order);
        ll_redeem_option=findViewById(R.id.ll_redeem_option);
        ll_cancel=findViewById(R.id.ll_cancel);
        ll_promoTab=findViewById(R.id.ll_promoTab);
        total_unit=findViewById(R.id.total_unit);
        tv_applyremove=findViewById(R.id.tv_applyremove);
        tv_cartisempty=findViewById(R.id.tv_cartisempty);
        tv_total_qty=findViewById(R.id.tv_total_qty);
        tv_subtotal=findViewById(R.id.tv_subtotal);
        rv_cart_list=findViewById(R.id.rv_cart_list);
        tv_promocode=findViewById(R.id.tv_promocode);
        tvGetPromoCode=findViewById(R.id.tvGetPromoCode);
        rv_tax_list=findViewById(R.id.rv_tax_list);
        tv_grandtotal=findViewById(R.id.tv_grandtotal);
        iv_lock=findViewById(R.id.iv_lock);
        ll_loginuser1=findViewById(R.id.ll_loginuser1);
        ll_prductinfo=findViewById(R.id.ll_prductinfo);
        ll_loginuser2=findViewById(R.id.ll_loginuser2);
        iv_home=findViewById(R.id.btn_back);
        scan_barcode=findViewById(R.id.scan_barcode);
        recyclecaregivers=findViewById(R.id.recyclecaregivers);


        rl_bottom=findViewById(R.id.rl_bottom);
        rl_bottom.setVisibility(View.GONE);
        gson = new Gson();
        mBAsis = gson.fromJson(Preferences.INSTANCE.getPreference(SalesActivity.this, "AppSetting"), GetEmployeeAppSetting.class);
        apppreference=new Apppreference(SalesActivity.this);
        Currency_value=mBAsis.getCurrency_value();
        LinearLayoutManager layoutManager
                = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        recyclecaregivers.setLayoutManager(layoutManager);
        rv_cart_list.setLayoutManager(new GridLayoutManager(SalesActivity.this,1));
        cateList =new  ArrayList<GetData>();
        list=new  ArrayList<GetCartData>();
        taxList=new  ArrayList<GetTaxData>();
        notsdata=new  ArrayList<notesdata>();
        caregiverlist=new ArrayList<getCaregiver>();
        peomocedelist=new ArrayList<GetPromosales>();
        paymentTypeList =new  ArrayList<GetPaymentTypes>();
        permissions.add(Manifest.permission.READ_EXTERNAL_STORAGE);
        permissions.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        permissionsToRequest = findUnAskedPermissions(permissions);

        cgAdapter=new CgAdapter(SalesActivity.this,caregiverlist);
        recyclecaregivers.setAdapter(cgAdapter);
        //get the permissions we have asked for before but are not granted..
        //we will store this in a global list to access later.
        if(mScale == null) {
            System.out.println("aaaaaaaaaaa  scale not found ");
            try{
                String identifier = apppreference.getidentifier();
                ConnectionInfo.InterfaceType interfaceType = ConnectionInfo.InterfaceType.valueOf(apppreference.getinterfaceType());

                StarDeviceManager starDeviceManager = new StarDeviceManager(SalesActivity.this);

                ConnectionInfo connectionInfo;

                switch (interfaceType) {
                    default:
                    case BLE:
                        connectionInfo = new ConnectionInfo.Builder()
                                .setBleInfo(identifier)
                                .build();
                        break;
                    case USB:
                        connectionInfo = new ConnectionInfo.Builder()
                                .setUsbInfo(identifier)
                                .setBaudRate(1200)
                                .build();
                        break;
                }

                mScale = starDeviceManager.createScale(connectionInfo);
                mScale.connect(mScaleCallback);
            }catch (Exception e){

            }

        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if(permissionsToRequest.size() > 0)
                requestPermissions((String[]) permissionsToRequest.toArray(new String[permissionsToRequest.size()]), ALL_PERMISSIONS_RESULT);
        }
        queue_patient_id = getIntent().getStringExtra("queue_patient_id");
        queue_id = getIntent().getStringExtra("queue_id");
        logindata = gson.fromJson(getIntent().getStringExtra("logindata"), GetEmployeeLogin.class);
        queueList = gson.fromJson(getIntent().getStringExtra("QueueData"), GetQueueList.class);
        productCartAdapter=new ProductCartAdapter(SalesActivity.this,list,Currency_value,viewCard);
        rv_cart_list.setAdapter(productCartAdapter);
        rv_tax_list.setLayoutManager(new GridLayoutManager(SalesActivity.this,1));
        taxItemAdapter = new TaxitemAdapter(SalesActivity.this,taxList, Currency_value);
        rv_tax_list.setAdapter(taxItemAdapter);

        try{
            queueList.getFirst_name();
            tvcustomername.setText(queueList.getFirst_name()+" "+queueList.getLast_name());
            tv_current_order.setText(queueList.getFirst_name()+" "+queueList.getLast_name());
            Picasso.with(SalesActivity.this)
                    .load(queueList.getPatient_image())
                    .placeholder(R.drawable.logo_top)
                    .error(R.drawable.logo_top)
                    .into(ivpatientcart);
         /*
            Glide.with(this)
                    .load(queueList.getPatient_image())
                    .into(ivpatientcart);*/
            System.out.println("aaaaaaaa  queue_patient_id  "+queue_patient_id+"   "+queueList.getQueue_patient_id());
        }catch (NullPointerException ex){

        }

        //System.out.println("aaaaaaaaaa    queue_patient_id  "+queue_patient_id+"  getUser_store_id   "+logindata.getUser_store_id());
        getapiCallForEmployeeDocument(0);
        //getcallEmployeePatientCart();
        getTAbsProducts();
        tv_current_order.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tv_current_order.setBackground(getResources().getDrawable(R.drawable.select_cg));
                tv_current_order.setTextColor(getResources().getColor(R.color.white));
                tvcustomername.setText(queueList.getFirst_name()+" "+queueList.getLast_name());
                tv_current_order.setText(queueList.getFirst_name()+" "+queueList.getLast_name());
                cgAdapter.backgroundchange(1);
                caregiver=0;
                caregiverid="";
                String id;
                if (queue_patient_id.equals("0")){
                    id= queueList.getQueue_patient_id();

                }else {
                    id= queue_patient_id;
                }
                getapiCallForEmployeecaregiver(id,0);
                //getapiCallForEmployeeDocument(1);
            }
        });
        img_exmlator.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String myJson = gson.toJson(logindata);
                Intent i=new Intent(SalesActivity.this, UserInfoDetails.class);
                if (caregiver==0){
                    if (!queue_patient_id.equals("0")){
                        i.putExtra("patient_id", queue_patient_id);
                    }else {
                        i.putExtra("patient_id", queueList.getQueue_patient_id());
                    }
                }else {
                    i.putExtra("patient_id", caregiverid);
                }

                i.putExtra("queue_id", queue_id);
                i.putExtra("logindata", myJson);
                startActivity(i);
            }
        });
        tvGetPromoCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                System.out.println("aaaaaaaaaaaa   tvGetPromoCode  "+tvGetPromoCode.getText().toString());
                if (tvGetPromoCode.getText().toString().equalsIgnoreCase("Remove")){
                    removePromocart();
                }else {
                    getpromocodes();
                }
            }
        });
        ll_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showdialogback();
            }
        });
        ll_send_order.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                Fragment prev = getSupportFragmentManager().findFragmentByTag("dialog");
                if (prev != null) {
                    ft.remove(prev);
                }
                ft.addToBackStack(null);
                AddPaymentDialog dialogFragment = new AddPaymentDialog(SalesActivity.this,viewCard);
                dialogFragment.show(ft,"dialog");
                //dialogFragment.showDialog(SalesActivity.this,viewCard.getPayment_types());
            }
        });
        tv_applyremove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AddProductDialog addProductDialog=new AddProductDialog();
                if (viewCard.getManager_discount() != null && viewCard.getManager_discount() != "") {
                    addProductDialog.showDialog(SalesActivity.this,1);
                }else {
                    addProductDialog.showDialog(SalesActivity.this,0);
                }
            }
        });
        ll_loginuser1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openCalculater();
            }
        });
        ll_prductinfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EmployeeSearchProducts mDialog =new EmployeeSearchProducts(SalesActivity.this);
                mDialog.show(getFragmentManager(), "Product Info");
            }
        });

        ll_loginuser2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ChatDialog dialog =new ChatDialog(ll_loginuser2, new  ChatDialog.onClickListener() {

                    public void callChangePin() {
                        showDialogChangePin("1");
                    }

                    public void  callChangePassword() {
                        showDialogChangePin("0");
                    }

                    public void callLogout() {
                        AlertDialog.Builder builder =new AlertDialog.Builder(SalesActivity.this, R.style.MyAlertDialogStyle);
                        setFinishOnTouchOutside(false);
                    }

                });

                dialog.show();
            }
        });
        iv_home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        scan_barcode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent goIntent =new Intent(SalesActivity.this, SaleQueueScan.class);
                startActivityForResult(goIntent, 444);

            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if(mScale != null) {
            mScale.disconnect();
        }
    }
    private ArrayList<String> findUnAskedPermissions(ArrayList<String> wanted) {
        ArrayList<String> result = new ArrayList<String>();

        for (String perm : wanted) {
            if (!hasPermission(perm)) {
                result.add(perm);
            }
        }

        return result;
    }
    private boolean hasPermission(String permission) {
        if (canMakeSmores()) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                return (checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED);
            }
        }
        return true;
    }

    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(this)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();
    }

    private boolean canMakeSmores() {
        return (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1);
    }

    @TargetApi(Build.VERSION_CODES.M)
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        switch (requestCode) {

            case ALL_PERMISSIONS_RESULT:
                for (Object perms : permissionsToRequest) {
                    if (hasPermission((String) perms)) {

                    } else {

                        permissionsRejected.add(perms);
                    }
                }

                if (permissionsRejected.size() > 0) {


                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (shouldShowRequestPermissionRationale((String) permissionsRejected.get(0))) {
                            showMessageOKCancel("These permissions are mandatory for the application. Please allow access.",
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

                                                //Log.d("API123", "permisionrejected " + permissionsRejected.size());

                                                requestPermissions((String[]) permissionsRejected.toArray(new String[permissionsRejected.size()]), ALL_PERMISSIONS_RESULT);
                                            }
                                        }
                                    });
                            return;
                        }
                    }

                }

                break;
        }

    }
    @Override
    protected void onResume() {
        super.onResume();
        getcallEmployeePatientCart("",0);
    }

    public void getTAbsProducts(){
        if (CUC.Companion.isNetworkAvailablewithPopup((Context)this)) {
            if (mSwipeRefreshLayout != null) {
                mSwipeRefreshLayout.setRefreshing(false);
            }
            mDialog = CUC.Companion.createDialog(SalesActivity.this);
            RequetsInterface apiService = RequetsInterface.Factory.create();
            Map parameterMap =new HashMap<String, String>();
            parameterMap.put("api_key", String.valueOf(Preferences.INSTANCE.getPreference((Context)this, "API_KEY")));

            if (logindata != null) {
                parameterMap.put("store_id", logindata.getUser_store_id());
            }

            if (!queue_patient_id.equals("0")){
                parameterMap.put("patient_id", queue_patient_id);
            }else {
                parameterMap.put("patient_id", queueList.getQueue_patient_id());
            }

            Log.i("aaaaaaaa   Result", "parameterMap : " + parameterMap);
            (new CompositeDisposable()).add(apiService.callEmployeeSalesProducts(parameterMap).observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io()).subscribe((Consumer)(new Consumer() {
                // $FF: synthetic method
                // $FF: bridge method
                public void accept(Object var1) {
                    this.accept((GetEmployeeSalesProducts)var1);
                }

                public final void accept(GetEmployeeSalesProducts result) {
                    mDialog.cancel();
                    if (result != null) {

                        System.out.println("aaaaaaaaa  result  "+result.toString());
                        Log.i("aaaaaaa   Result", "" + result.toString());
                        if (Intrinsics.areEqual(result.getStatus(), "0")) {
                            getcallEmployeePatientCart("",0);
                            getcgpatients();
                            if (result.getPro_cat().getData().size() > 0) {
                                image_path=result.getImage_path();
                                cateList.clear();
                                cateList = result.getPro_cat().getData();
                                Collections.reverse(cateList);
                                populateViewPager(cateList,image_path);
                            }
                            CUC.Companion.displayToast(SalesActivity.this, result.getMessage(), result.getMessage());
                        } else if (Intrinsics.areEqual(result.getStatus(), "10")) {

                        } else {
                            Toast.makeText(SalesActivity.this, ""+result.getMessage(), Toast.LENGTH_SHORT).show();
                            CUC.Companion.displayToast((Context)SalesActivity.this, result.getMessage(), result.getMessage());
                        }
                    } else {
                    }
                }
            }), (Consumer)(new Consumer() {
                // $FF: synthetic method
                // $FF: bridge method
                public void accept(Object var1) {
                    this.accept((Throwable)var1);
                }

                public final void accept(Throwable error) {
                    mDialog.cancel();
                    Toast.makeText(SalesActivity.this, getString(R.string.connection_to_database_failed), Toast.LENGTH_SHORT).show();
                    error.printStackTrace();
                    System.out.println("aaaaaaaa  error "+error.getMessage());
                }
            })));

        }else {
            System.out.println("aaaaaaaaaa   else ");
        }
    }

    private void populateViewPager(final List<GetData> cateList,String image_path) {

        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager(),cateList,logindata,queue_patient_id,queue_id,list,image_path,queueList);
        mViewPager.setAdapter(adapter);

        //Make tabs scrollable
        tab_layout.setTabMode(TabLayout.MODE_SCROLLABLE);
        tab_layout.setupWithViewPager(mViewPager);
    }

    public void getapiCallForEmployeeDocument(final int i){
        if (CUC.Companion.isNetworkAvailablewithPopup((Context)this)) {
           // mDialog = CUC.Companion.createDialog(SalesActivity.this);
            RequetsInterface apiService = RequetsInterface.Factory.create();
            Map parameterMap =new HashMap<String, String>();
            parameterMap.put("api_key", String.valueOf(Preferences.INSTANCE.getPreference((Context)this, "API_KEY")));

            if (logindata != null) {
                parameterMap.put("employee_id", logindata.getUser_id());
                //parameterMap.put("patient_id", queue_patient_id);
                parameterMap.put("store_id", logindata.getUser_store_id());
            }

            if (caregiver==0){
                if (queue_patient_id.equals("0")){
                    parameterMap.put("patient_id", queueList.getQueue_patient_id());

                }else {
                    parameterMap.put("patient_id", queue_patient_id);
                }
            }else {
                parameterMap.put("patient_id", caregiverid);
            }



            Log.i("aaaaaaaa   Result", "parameterMap : " + parameterMap);
            (new CompositeDisposable()).add(apiService.callEmployeePatientInfo(parameterMap).observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io()).subscribe((Consumer)(new Consumer() {
                // $FF: synthetic method
                // $FF: bridge method
                public void accept(Object var1) {
                    this.accept((EmployeePatientInfo)var1);
                }

                public final void accept(EmployeePatientInfo result) {
                   // mDialog.cancel();
                    if (result != null) {
                        System.out.println("aaaaaaaaa  result  "+result.toString());
                        Log.i("aaaaaaa   Result", "" + result.toString());
                        if (Intrinsics.areEqual(result.getStatus(), "0")) {
                            if (result.getStatus().equals("0")) {
                                textLimitUsage.setText(""+result.getLimit());
                               // tv_limit_value.setText(""+result.getSpent_limit());
                                noOfAddedItem = Float.parseFloat(result.getSpent_limit());
                                String str = result.getLimit();
                                if (i==1){
                                    getcallEmployeePatientCart("",0);
                                }
                                try{
                                    notsdata=result.getNotes_data();
                                    for (int i=0;i<notsdata.size();i++){
                                        shownotedialog(notsdata.get(i).getPnote_title(),notsdata.get(i).getPnote_notes());
                                    }
                                }catch (Exception e){

                                }

                                pieGraphLimite = Float.parseFloat(result.getLimit());
                                setChartData();
                            }
                            CUC.Companion.displayToast(SalesActivity.this, result.getMessage(), result.getMessage());
                        }  else {
                            Toast.makeText(SalesActivity.this, ""+result.getMessage(), Toast.LENGTH_SHORT).show();
                            CUC.Companion.displayToast((Context)SalesActivity.this, result.getMessage(), result.getMessage());
                        }
                    } else {
                    }
                }
            }), (Consumer)(new Consumer() {
                // $FF: synthetic method
                // $FF: bridge method
                public void accept(Object var1) {
                    this.accept((Throwable)var1);
                }

                public final void accept(Throwable error) {
                   // mDialog.cancel();
                    Toast.makeText(SalesActivity.this, getString(R.string.connection_to_database_failed), Toast.LENGTH_SHORT).show();
                    error.printStackTrace();
                    System.out.println("aaaaaaaa  error "+error.getMessage());
                }
            })));

        }else {
            System.out.println("aaaaaaaaaa   else ");
        }
    }

    public final void setChartData() {
        if (this.pieGraphLimite >= 0.0F && this.noOfAddedItem >= 0.0F) {
            textLimitUsage.setText(String.format("%.2f G", pieGraphLimite));
            tv_limit_value.setText(""+noOfAddedItem+"G/");

            Float usesPersntage = ((noOfAddedItem * 100) / pieGraphLimite);

        }

    }

    public void getcallEmployeePatientCart(String patientid,int i){
        if (CUC.Companion.isNetworkAvailablewithPopup((Context)this)) {
            if (mSwipeRefreshLayout != null) {
                mSwipeRefreshLayout.setRefreshing(false);
            }
          //  mDialog = CUC.Companion.createDialog(SalesActivity.this);
            RequetsInterface apiService = RequetsInterface.Factory.create();
            Map parameterMap =new HashMap<String, String>();
            parameterMap.put("api_key", String.valueOf(Preferences.INSTANCE.getPreference((Context)this, "API_KEY")));

            if (logindata != null) {
                parameterMap.put("store_id", logindata.getUser_store_id());
             //   parameterMap.put("patient_id", queue_patient_id);
                parameterMap.put("queue_id", queue_id);
            }

            if (caregiver==0){
                if (queue_patient_id.equals("0")){
                    parameterMap.put("patient_id", queueList.getQueue_patient_id());
                }else {
                    parameterMap.put("patient_id", queue_patient_id);
                }
            }else {
                parameterMap.put("patient_id", caregiverid);
            }


            System.out.println("aaaaaaaaa   viewcart parameter   "+parameterMap);
            Log.i("aaaaaaaa   Result", "parameterMap : " + parameterMap);
            (new CompositeDisposable()).add(apiService.callEmployeeSalesViewcart(parameterMap).observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io()).subscribe((Consumer)(new Consumer() {
                // $FF: synthetic method
                // $FF: bridge method
                public void accept(Object var1) {
                    this.accept((GetEmployeePatientCart)var1);
                }

                public final void accept(GetEmployeePatientCart result) {
                   // mDialog.cancel();
                    if (result != null) {
                        System.out.println("aaaaaaaaa  result viewcart  "+result.toString());
                        Log.i("aaaaaaa   Result", "" + result.toString());
                        if (Intrinsics.areEqual(result.getStatus(), "0")) {
                            viewCard=result;
                            System.out.println("aaaaaaaaaa  viewCard "+viewCard.getMain_cart().getCart_id());
                            textLimitUsage.setText(""+result.getLimit());
                           // tv_limit_value.setText(""+result.getSpent_limit());
                            System.out.println("aaaaaaaaa status  "+result.getStatus());
                           if (result.getStatus().equals("0")){
                              // mainCartId = result.getMain_cart().getCart_id();
                               payment_path = result.getPayment_path();
                               Store_sign_required = result.getStore_sign_required();
                               paymentTypeList.clear();
                               paymentTypeList.addAll(result.getPayment_types());
                               list.clear();
                               list=result.getCart_data();
                               System.out.println("aaaaaaaaa   "+result.getCart_data());
                               System.out.println("aaaaaaaaa   "+list.size());

                               if (list.size()>0){
                                   rl_bottom.setVisibility(View.VISIBLE);
                                   rv_cart_list.setVisibility(View.VISIBLE);
                                   tv_cartisempty.setVisibility(View.GONE);
                                   productCartAdapter.dataChanged(list,viewCard);
                                   tv_grandtotal.setText(Currency_value+""+result.getTotal_payble());
                                   tv_subtotal.setText(Currency_value+""+result.getSubtotal());
                                   tv_total_qty.setText(result.getTotal_qty());
                               }else {
                                   productCartAdapter.dataChanged(list,viewCard);
                                   rv_cart_list.setVisibility(View.GONE);
                                   rl_bottom.setVisibility(View.GONE);
                                   tv_cartisempty.setVisibility(View.VISIBLE);
                               }
                               taxList.clear();
                               taxList.addAll(result.getTax_data());
                               System.out.println("aaaaaaaaaaaa  taxdata  "+result.getTax_data());
                               if (taxList.size()>=0){
                                   rv_tax_list.setVisibility(View.VISIBLE);
                               }else {
                                   rv_tax_list.setVisibility(View.GONE);
                               }

                               try {
                                   if (discount_value != null && discount_value != "") {
                                       if (Float.parseFloat(discount_value) > 0) {
                                           if (cart_manual_discount_type.equals("2")) {
                                               taxList.add(new GetTaxData("202", getResources().getString(R.string.lbl_discount), discount_value, "2"));
                                           } else {
                                               taxList.add(new GetTaxData("202",getResources().getString(R.string.lbl_discount) , discount_value, "1"));
                                           }
                                       }
                                   }
                               } catch (Exception e) {
                                   e.printStackTrace();
                               }
                               tv_applyremove.setText("Add Discount");
                               try {
                                   if (result.getManager_discount() != null && result.getManager_discount() != "") {
                                       tv_applyremove.setText("Remove Discount");
                                       taxList.add(new GetTaxData("207", getResources().getString(R.string.lbl_discount), result.getManager_discount(), "2"));
                                   }
                               } catch (Exception e) {
                                   e.printStackTrace();
                                   tv_applyremove.setText("Add Discount");
                               }
                               if ((result.getPromocode_name() != "") || (result.getPromocode_name()!=null)) {
                                   if (result.getPromosales_amount().equals("0.00")){
                                       ll_promoTab.setVisibility(View.VISIBLE);
                                       tv_promocode.setText("");
                                       tvGetPromoCode.setText("Have A Promo Code?");
                                       tvGetPromoCode.setVisibility(View.VISIBLE);
                                   }else {
                                       ll_promoTab.setVisibility(View.VISIBLE);
                                       tv_promocode.setText(result.getPromocode_name());
                                       taxList.add(new GetTaxData("204", "Promo Code", result.getPromosales_amount(),""));
                                       tvGetPromoCode.setText("REMOVE");
                                       tvGetPromoCode.setVisibility(View.GONE);
                                   }

                               } else {
                                   ll_promoTab.setVisibility(View.VISIBLE);
                                   tv_promocode.setText("");
                                   tvGetPromoCode.setText("Have A Promo Code?");
                               }
                               total_payble = result.getTotal_payble();
                               taxList.add(new GetTaxData("201", "Grand Total", result.getTotal_payble(), "1"));
                               taxItemAdapter.dataChanged(taxList);


                               /*if (result.getCart_data() != null && result.getCart_data().size() > 0) {
                                   list.clear();
                                   patient_point = result.getPatient_point();
                                   discount_value = result.getMain_cart().getCart_manual_discount();
                                   cart_manual_discount_type = result.getMain_cart().getCart_manual_discount_type();
                                   String str = result.getLimit();
                                   noOfAddedItem = Float.parseFloat(result.getSpent_limit());
                                   setChartData();
                                   if (logindata != null) {
                                       if (logindata.getUser_role().equals("2")) {
                                           ll_redeem_option.setVisibility(View.VISIBLE);
                                           ll_send_order.setVisibility(View.VISIBLE);
                                           ll_cancel.setVisibility(View.VISIBLE);
                                           ll_promoTab.setVisibility(View.VISIBLE);
                                           //   rl_bottom.visibility = View.VISIBLE
                                           total_unit.setVisibility(View.VISIBLE);
                                       }else {
                                           ll_redeem_option.setVisibility(View.GONE);
                                           ll_send_order.setVisibility(View.GONE);
                                           ll_cancel.setVisibility(View.GONE);
                                           ll_promoTab.setVisibility(View.GONE);
                                           //   rl_bottom.visibility = View.VISIBLE
                                           total_unit.setVisibility(View.GONE);
                                       }
                                   }else {
                                       ll_redeem_option.setVisibility(View.GONE);
                                       ll_send_order.setVisibility(View.GONE);
                                       ll_cancel.setVisibility(View.GONE);
                                       ll_promoTab.setVisibility(View.GONE);
                                       //   rl_bottom.visibility = View.VISIBLE
                                       total_unit.setVisibility(View.GONE);
                                   }

                                   if (cart_manual_discount_type.equals("1") || cart_manual_discount_type.equals("2")) {
                                       tv_applyremove.setText("Remove Discount");
                                   } else {
                                       tv_applyremove.setText("Discount");
                                   }
                                   tv_cartisempty.setVisibility(View.GONE);
                                   tv_total_qty.setText(result.getTotal_qty());
                                   tv_subtotal.setText(Currency_value+""+result.getSubtotal());
                                   list.addAll(result.getCart_data());
                                   productCartAdapter.dataChanged(list);
                                   taxList.clear();
                                   taxList.addAll(result.getTax_data());

                                   try {
                                       if (result.getLoyalty_amount() != null && result.getLoyalty_amount() != "") {
                                           if (Float.parseFloat(result.getLoyalty_amount()) > 0) {
                                               taxList.add(new GetTaxData("989", "Loyalty Amount", result.getLoyalty_amount(),""));
                                           }
                                       }
                                   } catch (Exception e) {
                                       e.printStackTrace();
                                   }
                                   try {
                                       if (discount_value != null && discount_value != "") {
                                           if (Float.parseFloat(discount_value) > 0) {
                                               if (cart_manual_discount_type.equals("2")) {
                                                   taxList.add(new GetTaxData("202", getResources().getString(R.string.lbl_discount), discount_value, "2"));
                                               } else {
                                                   taxList.add(new GetTaxData("202",getResources().getString(R.string.lbl_discount) , discount_value, "1"));
                                               }
                                           }
                                       }
                                   } catch (Exception e) {
                                       e.printStackTrace();
                                   }

                                   if (result.getPromocode_name() != "") {
                                       ll_promoTab.setVisibility(View.VISIBLE);
                                       tv_promocode.setText(result.getPromocode_name());
                                       taxList.add(new GetTaxData("204", "Promo Code"+result.getPromocode_name(), result.getPromosales_amount(),""));
                                       tvGetPromoCode.setText("REMOVE");
                                   } else {
                                       ll_promoTab.setVisibility(View.VISIBLE);
                                       tv_promocode.setText("");
                                       tvGetPromoCode.setText("Have A Promo Code?");
                                   }
                                   total_payble = result.getTotal_payble();
                                   taxList.add(new GetTaxData("201", "Grand Total", result.getTotal_payble(), "1"));
                                   taxItemAdapter.dataChanged(taxList);

                               }
                               else {
                                   if (result.getPromocode_name().equals("")) {
                                       ll_promoTab.setVisibility(View.GONE);
                                       tv_promocode.setText("");
                                       tvGetPromoCode.setText("Have A Promo Code?");
                                   } else {
                                       ll_promoTab.setVisibility(View.GONE);
                                       tv_promocode.setText(""+result.getPromocode_name());
                                       tvGetPromoCode.setText("REMOVE");
                                   }
                                   tv_total_qty.setText("");
                                   tv_subtotal.setText("");
                                   tv_cartisempty.setVisibility(View.VISIBLE);
                                   total_unit.setVisibility(View.GONE);
                                   list.clear();
                                   productAdapter.notifyDataSetChanged();
                                   taxList.clear();
                                   taxItemAdapter.notifyDataSetChanged();
                                   //finish()
                               }*/
                               Preferences.INSTANCE.setPreference(SalesActivity.this, "total_qty", result.getTotal_qty());
                           }else if (result.getStatus().equals("10")){

                           }else {

                           }
                            CUC.Companion.displayToast(SalesActivity.this, result.getMessage(), result.getMessage());
                        }  else {
                            rv_cart_list.setVisibility(View.GONE);
                            rl_bottom.setVisibility(View.GONE);
                            tv_cartisempty.setVisibility(View.VISIBLE);
                           // Toast.makeText(SalesActivity.this, ""+result.getMessage(), Toast.LENGTH_SHORT).show();
                            CUC.Companion.displayToast((Context)SalesActivity.this, result.getMessage(), result.getMessage());
                        }
                    } else {
                    }
                }
            }), (Consumer)(new Consumer() {
                // $FF: synthetic method
                // $FF: bridge method
                public void accept(Object var1) {
                    this.accept((Throwable)var1);
                }

                public final void accept(Throwable error) {
                   // mDialog.cancel();
                    Toast.makeText(SalesActivity.this, getString(R.string.connection_to_database_failed), Toast.LENGTH_SHORT).show();
                    error.printStackTrace();
                    System.out.println("aaaaaaaa  error "+error.getMessage());
                }
            })));

        }else {
            System.out.println("aaaaaaaaaa   else ");
        }
    }

    public void onCart() {
        getcallEmployeePatientCart("",0);
    }

    public void setUpdateCart(GetCartData getCartData) {
        getStores(getCartData);
    }

    public void getStores(final GetCartData getProducts){

        mDialog = CUC.Companion.createDialog(SalesActivity.this);
        RequetsInterface apiService = RequetsInterface.Factory.create();
        Map parameterMap =new HashMap<String, String>();
        parameterMap.put("api_key", String.valueOf(Preferences.INSTANCE.getPreference((SalesActivity.this), "API_KEY")));

        if (logindata != null) {
            parameterMap.put("store_id", logindata.getUser_store_id());
            parameterMap.put("cart_detail_id", getProducts.getCart_detail_id());
            parameterMap.put("product_id", getProducts.getCart_detail_product());
            parameterMap.put("packet_id", getProducts.getCart_detail_packet_id());
        }

        System.out.println("aaaaaaaaaaa  parameterMap "+parameterMap);
        Log.i("aaaaaaaa   Result", "parameterMap : " + parameterMap);
        (new CompositeDisposable()).add(apiService.callEmployeeProductQty(parameterMap).
                observeOn(AndroidSchedulers.mainThread()).
                subscribeOn(Schedulers.io()).subscribe((Consumer)(new Consumer() {
            // $FF: synthetic method
            // $FF: bridge method
            public void accept(Object var1) {
                this.accept((GetStatus)var1);
            }

            public final void accept(GetStatus result) {
                mDialog.cancel();
                if (result != null) {
                    mDialog.cancel();
                    System.out.println("aaaaaaaaa  result getStores  "+result.toString());
                    Log.i("aaaaaaa   Result", "" + result.toString());
                    if (Intrinsics.areEqual(result.getStatus(), "0")) {
                      /* if (result.getInventory_qty().equals("0")){
                           Toast.makeText(SalesActivity.this, "Currently Products Not There", Toast.LENGTH_SHORT).show();
                           CUC.Companion.displayToast(SalesActivity.this, "Currently Products Not There", result.getMessage());
                       }else {*/
                        cartdetails=1;
                        getstatus=result;
                        getCartData=getProducts;
                        productquantityupdateDialog=new ProductquantityupdateDialog(SalesActivity.this,result,getProducts,"0");
                        productquantityupdateDialog.show();

                      //  AddProductDialog addProductDialog=new AddProductDialog();
                      //  addProductDialog.showDialog(SalesActivity.this,result,getProducts);
                         // }
                        CUC.Companion.displayToast(SalesActivity.this, result.getMessage(), result.getMessage());
                    } else if (Intrinsics.areEqual(result.getStatus(), "10")) {

                    } else {
                        Toast.makeText(SalesActivity.this, ""+result.getMessage(), Toast.LENGTH_SHORT).show();
                        CUC.Companion.displayToast((Context)SalesActivity.this, result.getMessage(), result.getMessage());
                    }
                } else {
                    mDialog.cancel();
                }
            }
        }), (Consumer)(new Consumer() {
            // $FF: synthetic method
            // $FF: bridge method
            public void accept(Object var1) {
                this.accept((Throwable)var1);
            }

            public final void accept(Throwable error) {
                mDialog.cancel();
                Toast.makeText(SalesActivity.this, getString(R.string.connection_to_database_failed), Toast.LENGTH_SHORT).show();
                error.printStackTrace();
                System.out.println("aaaaaaaa  error "+error.getMessage());
            }
        })));
    }

    public void sendUpdatecartea(final GetCartData getProducts, Double value,Float discount,int i){

        mDialog = CUC.Companion.createDialog(SalesActivity.this);
        RequetsInterface apiService = RequetsInterface.Factory.create();
        Map parameterMap =new HashMap<String, String>();
        parameterMap.put("api_key", String.valueOf(Preferences.INSTANCE.getPreference((SalesActivity.this), "API_KEY")));

        if (logindata != null) {
            parameterMap.put("store_id", logindata.getUser_store_id());
            parameterMap.put("employee_id", logindata.getUser_id());
        }
        parameterMap.put("cart_detail_id",getProducts.getCart_detail_id());
        if (!queue_patient_id.equals("0")){
            parameterMap.put("patient_id", queue_patient_id);
        }else {
            parameterMap.put("patient_id", queueList.getQueue_patient_id());
        }
        if (i==1){
            parameterMap.put("percentage_discount", discount);
        }else if (i==2){
            parameterMap.put("fixed_discount", discount);
        }

        // parameterMap.put("patient_id",queue_patient_id);
        parameterMap.put("product_id", getProducts.getCart_detail_product());
        parameterMap.put("category_id", getProducts.getCart_detail_category_id());
        parameterMap.put("package_id", getProducts.getCart_detail_packet_id());
        parameterMap.put("qty", ""+value);
        parameterMap.put("terminal_id", mBAsis.getTerminal_id());
        parameterMap.put("emp_lat", Preferences.INSTANCE.getPreference(SalesActivity.this, "latitude"));
        parameterMap.put("emp_log", Preferences.INSTANCE.getPreference(SalesActivity.this, "longitude"));
        parameterMap.put("queue_id", queue_id);

        System.out.println("aaaaaaaaaaa  parameterMap update cart "+parameterMap);
        Log.i("aaaaaaaa   Result", "parameterMap : " + parameterMap);
        (new CompositeDisposable()).add(apiService.callEmployeeSalesUpdatecart(parameterMap).
                observeOn(AndroidSchedulers.mainThread()).
                subscribeOn(Schedulers.io()).subscribe((Consumer)(new Consumer() {
            // $FF: synthetic method
            // $FF: bridge method
            public void accept(Object var1) {
                this.accept((GetStatus)var1);
            }

            public final void accept(GetStatus result) {
                mDialog.cancel();
                if (result != null) {
                    mDialog.cancel();

                    System.out.println("aaaaaaaaa  result updatecart  "+result.toString());
                    Log.i("aaaaaaa   Result", "" + result.toString());
                    if (Intrinsics.areEqual(result.getStatus(), "0")) {
                        try{
                            if (result.getStatus().equals("0") && result.getGrade_status().equals("0")){
                                showdialog(result.getGrade_message());
                            }
                        }catch (NullPointerException e){

                        }try{
                            if (result.getStatus().equals("0") && result.getGrade_warning_status().equals("0")){
                                showdialog(result.getGrade_warning());
                            }
                        }catch (NullPointerException e){

                        }
                        System.out.println("aaaaaaaaaa  updatecart sucess");
                        ((SalesActivity)SalesActivity.this).onCart();
                        CUC.Companion.displayToast(SalesActivity.this, result.getMessage(), result.getMessage());
                    } else if (Intrinsics.areEqual(result.getStatus(), "10")) {

                    } else {
                       // Toast.makeText(SalesActivity.this, ""+result.getShow_status(), Toast.LENGTH_SHORT).show();
                        CUC.Companion.displayToast((Context)SalesActivity.this, result.getMessage(), result.getMessage());
                    }
                } else {
                    mDialog.cancel();
                }
            }
        }), (Consumer)(new Consumer() {
            // $FF: synthetic method
            // $FF: bridge method
            public void accept(Object var1) {
                this.accept((Throwable)var1);
            }

            public final void accept(Throwable error) {
                mDialog.cancel();
                Toast.makeText(SalesActivity.this, getString(R.string.connection_to_database_failed), Toast.LENGTH_SHORT).show();
                error.printStackTrace();
                System.out.println("aaaaaaaa  error update "+error.getMessage());
            }
        })));
    }
    public void sendUpdatecart(final GetCartData getProducts, Float value,Float discount,int i){

        mDialog = CUC.Companion.createDialog(SalesActivity.this);
        RequetsInterface apiService = RequetsInterface.Factory.create();
        Map parameterMap =new HashMap<String, String>();
        parameterMap.put("api_key", String.valueOf(Preferences.INSTANCE.getPreference((SalesActivity.this), "API_KEY")));

        if (logindata != null) {
            parameterMap.put("store_id", logindata.getUser_store_id());
            parameterMap.put("employee_id", logindata.getUser_id());
        }
        parameterMap.put("cart_detail_id",getProducts.getCart_detail_id());
        if (!queue_patient_id.equals("0")){
            parameterMap.put("patient_id", queue_patient_id);
        }else {
            parameterMap.put("patient_id", queueList.getQueue_patient_id());
        }
        if (i==1){
            parameterMap.put("percentage_discount", discount);
        }else if (i==2){
            parameterMap.put("fixed_discount", discount);
        }

       // parameterMap.put("patient_id",queue_patient_id);
        parameterMap.put("product_id", getProducts.getCart_detail_product());
        parameterMap.put("category_id", getProducts.getCart_detail_category_id());
        parameterMap.put("package_id", getProducts.getCart_detail_packet_id());
        parameterMap.put("qty", ""+value);
        parameterMap.put("terminal_id", mBAsis.getTerminal_id());
        parameterMap.put("emp_lat", Preferences.INSTANCE.getPreference(SalesActivity.this, "latitude"));
        parameterMap.put("emp_log", Preferences.INSTANCE.getPreference(SalesActivity.this, "longitude"));
        parameterMap.put("queue_id", queue_id);

        System.out.println("aaaaaaaaaaa  parameterMap update cart "+parameterMap);
        Log.i("aaaaaaaa   Result", "parameterMap : " + parameterMap);
        (new CompositeDisposable()).add(apiService.callEmployeeSalesUpdatecart(parameterMap).
                observeOn(AndroidSchedulers.mainThread()).
                subscribeOn(Schedulers.io()).subscribe((Consumer)(new Consumer() {
            // $FF: synthetic method
            // $FF: bridge method
            public void accept(Object var1) {
                this.accept((GetStatus)var1);
            }

            public final void accept(GetStatus result) {
                mDialog.cancel();
                if (result != null) {
                    mDialog.cancel();

                    System.out.println("aaaaaaaaa  result updatecart  "+result.toString());
                    Log.i("aaaaaaa   Result", "" + result.toString());
                    if (Intrinsics.areEqual(result.getStatus(), "0")) {
                        try{
                            if (result.getStatus().equals("0") && result.getGrade_status().equals("0")){
                                showdialog(result.getGrade_message());
                            }
                        }catch (NullPointerException e){

                        }try{
                            if (result.getStatus().equals("0") && result.getGrade_warning_status().equals("0")){
                                showdialog(result.getGrade_warning());
                            }
                        }catch (NullPointerException e){

                        }
                        System.out.println("aaaaaaaaaa  updatecart sucess");
                        ((SalesActivity)SalesActivity.this).onCart();
                        CUC.Companion.displayToast(SalesActivity.this, result.getMessage(), result.getMessage());
                    } else if (Intrinsics.areEqual(result.getStatus(), "10")) {

                    } else {
                       // Toast.makeText(SalesActivity.this, ""+result.getShow_status(), Toast.LENGTH_SHORT).show();
                        CUC.Companion.displayToast((Context)SalesActivity.this, result.getMessage(), result.getMessage());
                    }
                } else {
                    mDialog.cancel();
                }
            }
        }), (Consumer)(new Consumer() {
            // $FF: synthetic method
            // $FF: bridge method
            public void accept(Object var1) {
                this.accept((Throwable)var1);
            }

            public final void accept(Throwable error) {
                mDialog.cancel();
                Toast.makeText(SalesActivity.this, getString(R.string.connection_to_database_failed), Toast.LENGTH_SHORT).show();
                error.printStackTrace();
                System.out.println("aaaaaaaa  error update "+error.getMessage());
            }
        })));
    }

    public void cartDelete(final GetCartData getProducts){

        mDialog = CUC.Companion.createDialog(SalesActivity.this);
        RequetsInterface apiService = RequetsInterface.Factory.create();
        Map parameterMap =new HashMap<String, String>();
        parameterMap.put("api_key", String.valueOf(Preferences.INSTANCE.getPreference((SalesActivity.this), "API_KEY")));

        if (logindata != null) {
            parameterMap.put("employee_id", logindata.getUser_id());
        }
        parameterMap.put("cart_detail_id",getProducts.getCart_detail_id());

        if (caregiver==0){
            if (!queue_patient_id.equals("0")){
                parameterMap.put("patient_id", queue_patient_id);
            }else {
                parameterMap.put("patient_id", queueList.getQueue_patient_id());
            }
        }else {
            parameterMap.put("patient_id", caregiverid);
        }

       // parameterMap.put("patient_id", queue_patient_id);
        parameterMap.put("terminal_id", mBAsis.getTerminal_id());
        parameterMap.put("emp_lat", Preferences.INSTANCE.getPreference(SalesActivity.this, "latitude"));
        parameterMap.put("emp_log", Preferences.INSTANCE.getPreference(SalesActivity.this, "longitude"));

        System.out.println("aaaaaaaaaaa  parameterMap delete cart "+parameterMap);
        Log.i("aaaaaaaa   Result", "parameterMap : " + parameterMap);
        (new CompositeDisposable()).add(apiService.callEmployeePatientRemovecart(parameterMap).
                observeOn(AndroidSchedulers.mainThread()).
                subscribeOn(Schedulers.io()).subscribe((Consumer)(new Consumer() {
            // $FF: synthetic method
            // $FF: bridge method
            public void accept(Object var1) {
                this.accept((GetStatus)var1);
            }

            public final void accept(GetStatus result) {
                mDialog.cancel();
                if (result != null) {
                    mDialog.cancel();

                    System.out.println("aaaaaaaaa  result deletecart  "+result.toString());
                    Log.i("aaaaaaa   Result", "" + result.toString());
                    if (Intrinsics.areEqual(result.getShow_status(), "0")) {
                        System.out.println("aaaaaaaaaa  deletecart sucess");
                       onCart();
                        CUC.Companion.displayToast(SalesActivity.this, result.getMessage(), result.getMessage());
                    } else if (Intrinsics.areEqual(result.getStatus(), "10")) {

                    } else {
                        Toast.makeText(SalesActivity.this, ""+result.getMessage(), Toast.LENGTH_SHORT).show();
                        CUC.Companion.displayToast((Context)SalesActivity.this, result.getMessage(), result.getMessage());
                    }
                } else {
                    mDialog.cancel();
                }
            }
        }), (Consumer)(new Consumer() {
            // $FF: synthetic method
            // $FF: bridge method
            public void accept(Object var1) {
                this.accept((Throwable)var1);
            }

            public final void accept(Throwable error) {
                mDialog.cancel();
                Toast.makeText(SalesActivity.this, getString(R.string.connection_to_database_failed), Toast.LENGTH_SHORT).show();
                error.printStackTrace();
                System.out.println("aaaaaaaa  error update "+error.getMessage());
            }
        })));
    }

    public void removePromocart(){

        mDialog = CUC.Companion.createDialog(SalesActivity.this);
        RequetsInterface apiService = RequetsInterface.Factory.create();
        Map parameterMap =new HashMap<String, String>();
        parameterMap.put("api_key", String.valueOf(Preferences.INSTANCE.getPreference((SalesActivity.this), "API_KEY")));

        if (logindata != null) {
            parameterMap.put("employee_id", logindata.getUser_id());
            parameterMap.put("store_id", logindata.getUser_store_id());
        }
        if (mBAsis != null) {
            parameterMap.put("cart_detail_id",mBAsis.getTerminal_id());
        }

        parameterMap.put("emp_lat", Preferences.INSTANCE.getPreference(SalesActivity.this, "latitude"));
        parameterMap.put("emp_log", Preferences.INSTANCE.getPreference(SalesActivity.this, "longitude"));
        parameterMap.put("queue_id",queue_id);
        if (caregiver==0){
            if (!queue_patient_id.equals("0")){
                parameterMap.put("patient_id", queue_patient_id);
            }else {
                parameterMap.put("patient_id", queueList.getQueue_patient_id());
            }
        }else {
            parameterMap.put("patient_id", caregiverid);
        }

        System.out.println("aaaaaaaaaaa  parameterMap removepromo cart "+parameterMap);
        Log.i("aaaaaaaa   Result", "parameterMap : " + parameterMap);
        (new CompositeDisposable()).add(apiService.callEmployeeSalesRemovePromocode(parameterMap).
                observeOn(AndroidSchedulers.mainThread()).
                subscribeOn(Schedulers.io()).subscribe((Consumer)(new Consumer() {
            // $FF: synthetic method
            // $FF: bridge method
            public void accept(Object var1) {
                this.accept((GetStatus)var1);
            }

            public final void accept(GetStatus result) {
                mDialog.cancel();
                if (result != null) {
                    mDialog.cancel();
                    System.out.println("aaaaaaaaa  result removepromo  "+result.toString());
                    Log.i("aaaaaaa   Result", "" + result.toString());
                    if (Intrinsics.areEqual(result.getShow_status(), "0")) {
                        ((SalesActivity)SalesActivity.this).onCart();
                        CUC.Companion.displayToast(SalesActivity.this, result.getMessage(), result.getMessage());
                    } else if (Intrinsics.areEqual(result.getStatus(), "10")) {

                    } else {
                        Toast.makeText(SalesActivity.this, ""+result.getMessage(), Toast.LENGTH_SHORT).show();
                        CUC.Companion.displayToast((Context)SalesActivity.this, result.getMessage(), result.getMessage());
                    }
                } else {
                    mDialog.cancel();
                }
            }
        }), (Consumer)(new Consumer() {
            // $FF: synthetic method
            // $FF: bridge method
            public void accept(Object var1) {
                this.accept((Throwable)var1);
            }

            public final void accept(Throwable error) {
                mDialog.cancel();
                Toast.makeText(SalesActivity.this, getString(R.string.connection_to_database_failed), Toast.LENGTH_SHORT).show();
                error.printStackTrace();
                System.out.println("aaaaaaaa  error update "+error.getMessage());
            }
        })));
    }

    public void removecart(GetCartData getCartData, GetEmployeePatientCart viewCard){

        System.out.println("aaaaaaaaaaaaa  viewCard "+viewCard.getMain_cart().getCart_id());
        mDialog = CUC.Companion.createDialog(SalesActivity.this);
        RequetsInterface apiService = RequetsInterface.Factory.create();
        Map parameterMap =new HashMap<String, String>();
        parameterMap.put("api_key", String.valueOf(Preferences.INSTANCE.getPreference((SalesActivity.this), "API_KEY")));
        parameterMap.put("cart_id",viewCard.getMain_cart().getCart_id());

        System.out.println("aaaaaaaaaaa  parameterMap removepcart "+parameterMap);
        Log.i("aaaaaaaa   Result", "parameterMap : " + parameterMap);
        (new CompositeDisposable()).add(apiService.callEmployeeSalesCancelOrder(parameterMap).
                observeOn(AndroidSchedulers.mainThread()).
                subscribeOn(Schedulers.io()).subscribe((Consumer)(new Consumer() {
            // $FF: synthetic method
            // $FF: bridge method
            public void accept(Object var1) {
                this.accept((GetEmployeeQenerateInvoice)var1);
            }

            public final void accept(GetEmployeeQenerateInvoice result) {
                mDialog.cancel();
                if (result != null) {
                    mDialog.cancel();
                    System.out.println("aaaaaaaaa  result removecart  "+result.toString());
                    Log.i("aaaaaaa   Result", "" + result.toString());
                    if (Intrinsics.areEqual(result.getShow_status(), "0")) {
                        rl_bottom.setVisibility(View.GONE);
                        rv_cart_list.setVisibility(View.GONE);
                        //((SalesActivity)SalesActivity.this).onCart();
                        CUC.Companion.displayToast(SalesActivity.this, result.getMessage(), result.getMessage());
                    } else if (Intrinsics.areEqual(result.getStatus(), "10")) {

                    } else {
                        Toast.makeText(SalesActivity.this, ""+result.getMessage(), Toast.LENGTH_SHORT).show();
                        CUC.Companion.displayToast((Context)SalesActivity.this, result.getMessage(), result.getMessage());
                    }
                } else {
                    mDialog.cancel();
                }
            }
        }), (Consumer)(new Consumer() {
            // $FF: synthetic method
            // $FF: bridge method
            public void accept(Object var1) {
                this.accept((Throwable)var1);
            }

            public final void accept(Throwable error) {
                mDialog.cancel();
                Toast.makeText(SalesActivity.this, getString(R.string.connection_to_database_failed), Toast.LENGTH_SHORT).show();
                error.printStackTrace();
                System.out.println("aaaaaaaa  error update "+error.getMessage());
            }
        })));
    }

    public void getpromocodes(){

        System.out.println("aaaaaaaaaaaaa  viewCard "+viewCard.getMain_cart().getCart_id());
        mDialog = CUC.Companion.createDialog(SalesActivity.this);
        RequetsInterface apiService = RequetsInterface.Factory.create();
        Map parameterMap =new HashMap<String, String>();
        parameterMap.put("api_key", String.valueOf(Preferences.INSTANCE.getPreference((SalesActivity.this), "API_KEY")));
        parameterMap.put("cart_id",viewCard.getMain_cart().getCart_id());
        parameterMap.put("patient_id",viewCard.getMain_cart().getCart_patient_id());
        parameterMap.put("app_status","1");

        System.out.println("aaaaaaaaaaa  parameterMap removepcart "+parameterMap);
        Log.i("aaaaaaaa   Result", "parameterMap : " + parameterMap);
        (new CompositeDisposable()).add(apiService.callEmployeePatientPromocodeList(parameterMap).
                observeOn(AndroidSchedulers.mainThread()).
                subscribeOn(Schedulers.io()).subscribe((Consumer)(new Consumer() {
            // $FF: synthetic method
            // $FF: bridge method
            public void accept(Object var1) {
                this.accept((GetEmployeePatientPromocodeList)var1);
            }

            public final void accept(GetEmployeePatientPromocodeList result) {
                mDialog.cancel();
                if (result != null) {
                    mDialog.cancel();
                    System.out.println("aaaaaaaaa  result promocodelist  "+result.toString());
                    Log.i("aaaaaaa   Result", "" + result.toString());
                    if (result.getStatus().equalsIgnoreCase("0")){
                        CUC.Companion.displayToast((Context)SalesActivity.this, result.getMessage(), result.getMessage());
                    }else {
                        if (result.getShow_status().equals("0")){
                            CUC.Companion.displayToast((Context)SalesActivity.this, result.getMessage(), result.getMessage());
                        }else {
                            peomocedelist.addAll(result.getPromosales());
                            AddPromocardDialog promoCodeDialog=new AddPromocardDialog();
                            promoCodeDialog.showDialog(SalesActivity.this,peomocedelist);
                            CUC.Companion.displayToast(SalesActivity.this, result.getMessage(), result.getMessage());
                        }

                    }
                    /*if (Intrinsics.areEqual(result.getShow_status(), "0")) {
                        peomocedelist.addAll(result.getPromosales());
                        AddPromocardDialog promoCodeDialog=new AddPromocardDialog();
                        promoCodeDialog.showDialog(SalesActivity.this,peomocedelist);
                        CUC.Companion.displayToast(SalesActivity.this, result.getShow_status(), result.getMessage());
                    } else if (Intrinsics.areEqual(result.getStatus(), "10")) {

                    } else {
                        Toast.makeText(SalesActivity.this, ""+result.getShow_status(), Toast.LENGTH_SHORT).show();
                        CUC.Companion.displayToast((Context)SalesActivity.this, result.getShow_status(), result.getMessage());
                    }*/
                } else {
                    mDialog.cancel();
                }
            }
        }), (Consumer)(new Consumer() {
            // $FF: synthetic method
            // $FF: bridge method
            public void accept(Object var1) {
                this.accept((Throwable)var1);
            }

            public final void accept(Throwable error) {
                mDialog.cancel();
                Toast.makeText(SalesActivity.this, getString(R.string.connection_to_database_failed), Toast.LENGTH_SHORT).show();
                error.printStackTrace();
                System.out.println("aaaaaaaa  error update "+error.getMessage());
            }
        })));
    }

    public void showdialog(String msg){
        AlertDialog.Builder builder1 = new AlertDialog.Builder(SalesActivity.this);
        builder1.setMessage(""+msg);
        builder1.setCancelable(true);

        builder1.setPositiveButton(
                "Ok",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        AlertDialog alert11 = builder1.create();
        alert11.show();
    }
    public void shownotedialog(String title,String message){
        AlertDialog.Builder builder1 = new AlertDialog.Builder(SalesActivity.this);
        builder1.setTitle(title);
        builder1.setMessage(""+message);
        builder1.setCancelable(true);

        builder1.setPositiveButton(
                "Ok",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        AlertDialog alert11 = builder1.create();
        alert11.show();
    }

    public void showdialogask(final GetCartData getProducts){
        AlertDialog.Builder builder1 = new AlertDialog.Builder(SalesActivity.this);
        builder1.setMessage("Are You Sure You Want To Delete?");
        builder1.setCancelable(true);

        builder1.setPositiveButton(
                "Yes",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        cartDelete(getProducts);
                        dialog.cancel();
                    }
                });

        builder1.setNegativeButton(
                "No",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        AlertDialog alert11 = builder1.create();
        alert11.show();
    }
    public void showdialogback(){
        AlertDialog.Builder builder1 = new AlertDialog.Builder(SalesActivity.this);
        builder1.setMessage("Are You Want to leave Sales POS?");
        builder1.setTitle("Confitm");
        builder1.setCancelable(true);

        builder1.setPositiveButton(
                "Yes",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        finish();
                        dialog.cancel();
                    }
                });

        builder1.setNegativeButton(
                "No",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        AlertDialog alert11 = builder1.create();
        alert11.show();
    }

    public void setPromocde() {

    }

    public void setapplyPromocard(GetPromosales getPromosales) {

    }

    public void setDiscount(String entertxt, int k, String pin){

        System.out.println("aaaaaaaaaaaaa  viewCard "+viewCard.getMain_cart().getCart_id());
        mDialog = CUC.Companion.createDialog(SalesActivity.this);
        RequetsInterface apiService = RequetsInterface.Factory.create();
        Map parameterMap =new HashMap<String, String>();
        parameterMap.put("api_key", String.valueOf(Preferences.INSTANCE.getPreference((SalesActivity.this), "API_KEY")));
        if (logindata != null) {
            parameterMap.put("employee_id", logindata.getUser_id());
            parameterMap.put("store_id", logindata.getUser_store_id());
        }
        if (mBAsis != null) {
            parameterMap.put("terminal_id",mBAsis.getTerminal_id());
        }
        if (caregiver==0){
            parameterMap.put("patient_id",queue_patient_id);
        }else {
            parameterMap.put("patient_id",caregiverid);
        }

        parameterMap.put("queue_id",queue_id);
        parameterMap.put("discount_type",(k+1));
        parameterMap.put("discount_value",entertxt);
        parameterMap.put("employee_pin",pin);
        parameterMap.put("emp_lat", Preferences.INSTANCE.getPreference(SalesActivity.this, "latitude"));
        parameterMap.put("emp_log", Preferences.INSTANCE.getPreference(SalesActivity.this, "longitude"));

        System.out.println("aaaaaaaaaaa  parameterMap adddiscount "+parameterMap);
        Log.i("aaaaaaaa   Result", "parameterMap : " + parameterMap);
        (new CompositeDisposable()).add(apiService.callEmployeeSalesApplyStmanDiscount(parameterMap).
                observeOn(AndroidSchedulers.mainThread()).
                subscribeOn(Schedulers.io()).subscribe((Consumer)(new Consumer() {
            // $FF: synthetic method
            // $FF: bridge method
            public void accept(Object var1) {
                this.accept((GetStatus)var1);
            }

            public final void accept(GetStatus result) {
                mDialog.cancel();
                if (result != null) {
                    mDialog.cancel();
                    System.out.println("aaaaaaaaa  result adddiscount  "+result.toString());
                    Log.i("aaaaaaa   Result", "" + result.toString());
                    if (result.getStatus().equalsIgnoreCase("0")){
                        tv_applyremove.setText("Remove Discount");
                        getcallEmployeePatientCart("",0);
                        CUC.Companion.displayToast((Context)SalesActivity.this, result.getMessage(), result.getMessage());
                    }else {
                        if (result.getShow_status().equals("0")){
                            tv_applyremove.setText("Remove Discount");
                            getcallEmployeePatientCart("",0);
                            CUC.Companion.displayToast((Context)SalesActivity.this, result.getMessage(), result.getMessage());
                        }else {

                            CUC.Companion.displayToast(SalesActivity.this, result.getMessage(), result.getMessage());
                        }

                    }
                } else {
                    mDialog.cancel();
                }
            }
        }), (Consumer)(new Consumer() {
            // $FF: synthetic method
            // $FF: bridge method
            public void accept(Object var1) {
                this.accept((Throwable)var1);
            }

            public final void accept(Throwable error) {
                mDialog.cancel();
                Toast.makeText(SalesActivity.this, getString(R.string.connection_to_database_failed), Toast.LENGTH_SHORT).show();
                error.printStackTrace();
                System.out.println("aaaaaaaa  error update "+error.getMessage());
            }
        })));
    }


    public void setRemovediscount(String pin){

        System.out.println("aaaaaaaaaaaaa  viewCard "+viewCard.getMain_cart().getCart_id());
        mDialog = CUC.Companion.createDialog(SalesActivity.this);
        RequetsInterface apiService = RequetsInterface.Factory.create();
        Map parameterMap =new HashMap<String, String>();
        parameterMap.put("api_key", String.valueOf(Preferences.INSTANCE.getPreference((SalesActivity.this), "API_KEY")));
        if (logindata != null) {
            parameterMap.put("employee_id", logindata.getUser_id());
            parameterMap.put("store_id", logindata.getUser_store_id());
        }
        if (mBAsis != null) {
            parameterMap.put("terminal_id",mBAsis.getTerminal_id());
        }

        if (caregiver==0){
            parameterMap.put("patient_id",queue_patient_id);
        }else {
            parameterMap.put("patient_id",caregiverid);
        }

        parameterMap.put("queue_id",queue_id);
        parameterMap.put("employee_pin",pin);
        parameterMap.put("emp_lat", Preferences.INSTANCE.getPreference(SalesActivity.this, "latitude"));
        parameterMap.put("emp_log", Preferences.INSTANCE.getPreference(SalesActivity.this, "longitude"));

        System.out.println("aaaaaaaaaaa  parameterMap remove discount "+parameterMap);
        Log.i("aaaaaaaa   Result", "parameterMap : " + parameterMap);
        (new CompositeDisposable()).add(apiService.callEmployeeSalesRemoveStmanDiscount(parameterMap).
                observeOn(AndroidSchedulers.mainThread()).
                subscribeOn(Schedulers.io()).subscribe((Consumer)(new Consumer() {
            // $FF: synthetic method
            // $FF: bridge method
            public void accept(Object var1) {
                this.accept((GetStatus)var1);
            }

            public final void accept(GetStatus result) {
                mDialog.cancel();
                if (result != null) {
                    mDialog.cancel();
                    System.out.println("aaaaaaaaa  result remove discount  "+result.toString());
                    Log.i("aaaaaaa   Result", "" + result.toString());
                    if (result.getStatus().equalsIgnoreCase("0")){
                        tv_applyremove.setText("Add Discount");
                        getcallEmployeePatientCart("",0);
                        CUC.Companion.displayToast((Context)SalesActivity.this, result.getShow_status(), result.getMessage());
                    }else {
                        if (result.getShow_status().equals("0")){
                            tv_applyremove.setText("Add Discount");
                            getcallEmployeePatientCart("",0);
                            CUC.Companion.displayToast((Context)SalesActivity.this, result.getShow_status(), result.getMessage());
                        }else {

                            CUC.Companion.displayToast(SalesActivity.this, result.getShow_status(), result.getMessage());
                        }

                    }
                } else {
                    mDialog.cancel();
                }
            }
        }), (Consumer)(new Consumer() {
            // $FF: synthetic method
            // $FF: bridge method
            public void accept(Object var1) {
                this.accept((Throwable)var1);
            }

            public final void accept(Throwable error) {
                mDialog.cancel();
                Toast.makeText(SalesActivity.this, getString(R.string.connection_to_database_failed), Toast.LENGTH_SHORT).show();
                error.printStackTrace();
                System.out.println("aaaaaaaa  error update "+error.getMessage());
            }
        })));
    }

    public void sendPayment(Float enteramt,int position) {
        this.tabposition=position;
        paymentamt=enteramt;
      String payment =viewCard.getPayment_types().get(tabposition).getPayment_detail();
        System.out.println("aaaaaaaaaa  payment   "+position+"   "+payment+"   enteramt  "+enteramt);
        Intent siantureIntent = new Intent(SalesActivity.this, SignatureActivity.class);
        startActivityForResult(siantureIntent, 8989);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
         if (requestCode == 444) {
             System.out.println("aaaaaaaaaaaa  inetent "+data);

            if (data != null) {
                String data1 = data.getStringExtra("data");
                System.out.println("aaaaaaaaa  data  "+data1);
                callEmployeeScanProducts(data1);
            }
        }
       else if (requestCode == 8989){
            if (data != null && data.hasExtra("siganture_path")) {
                siganture_path = data.getStringExtra("siganture_path");
                if (siganture_path != null ) {
                    System.out.println("aaaaaaaaa   siganture_path   "+siganture_path);
                    callEmployeePatientConfirmOrder();
                }

                }
        }
    }

    public void callEmployeePatientConfirmOrder(){

        String payment =viewCard.getPayment_types().get(tabposition).getPayment_id();
        System.out.println("aaaaaaaaaaaaaaa  payment "+tabposition);

        System.out.println("aaaaaaaaaaaaa  viewCard "+viewCard.getMain_cart().getCart_id());
        mDialog = CUC.Companion.createDialog(SalesActivity.this);
        RequetsInterface apiService = RequetsInterface.Factory.create();
        Map parameterMap =new HashMap<String, String>();
        parameterMap.put("api_key", RequestBody.create(MediaType.parse("text/plain"), String.valueOf(Preferences.INSTANCE.getPreference((SalesActivity.this), "API_KEY"))));
        if (logindata != null) {
            parameterMap.put("employee_id", RequestBody.create(MediaType.parse("text/plain"), String.valueOf(logindata.getUser_id())));
        }
        if (mBAsis != null) {
            parameterMap.put("terminal_id",RequestBody.create(MediaType.parse("text/plain"), String.valueOf(mBAsis.getTerminal_id())));
        }
        parameterMap.put("discount_price",RequestBody.create(MediaType.parse("text/plain"), String.valueOf(viewCard.getManager_discount())));
        if (caregiver==0){
            parameterMap.put("patient_id",RequestBody.create(MediaType.parse("text/plain"), String.valueOf(queue_patient_id)));
        }else {
            parameterMap.put("patient_id",RequestBody.create(MediaType.parse("text/plain"), String.valueOf(caregiverid)));
        }

        parameterMap.put("cart_id",RequestBody.create(MediaType.parse("text/plain"), String.valueOf(this.viewCard.getMain_cart().getCart_id())));
        parameterMap.put("payment_id",RequestBody.create(MediaType.parse("text/plain"), String.valueOf(this.viewCard.getPayment_types().get(tabposition).getPayment_id())));
        parameterMap.put("queue_id",RequestBody.create(MediaType.parse("text/plain"), String.valueOf(queue_id)));
        Float changeamt=paymentamt-Float.parseFloat(viewCard.getTotal_payble());
        parameterMap.put("payment_amount",RequestBody.create(MediaType.parse("text/plain"), String.valueOf(paymentamt)));
        parameterMap.put("change_amount",RequestBody.create(MediaType.parse("text/plain"), String.valueOf(changeamt)));
        parameterMap.put("emp_lat", RequestBody.create(MediaType.parse("text/plain"), String.valueOf(Preferences.INSTANCE.getPreference(SalesActivity.this, "latitude"))));
        parameterMap.put("emp_log", RequestBody.create(MediaType.parse("text/plain"), String.valueOf(Preferences.INSTANCE.getPreference(SalesActivity.this, "longitude"))));
       // Observable disposable;
        File sigantureFile = new File(this.siganture_path);
        //RequestBody reqFile = RequestBody.create(MediaType.parse("image/jpeg"), sigantureFile);


        MultipartBody.Part var8 = MultipartBody.Part.createFormData("digital_siganture", String.valueOf(sigantureFile.getName()),
                RequestBody.create(MediaType.parse("image*//*"), sigantureFile));
        Intrinsics.checkExpressionValueIsNotNull(var8, "MultipartBody.Part.creat…ge*//*\"), sigantureFile))");
        RequestBody var14 = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(sigantureFile.getName()));
        Intrinsics.checkExpressionValueIsNotNull(var14, "RequestBody.create(Media… \"${sigantureFile.name}\")");
        Observable disposable = apiService.callEmployeeSalesInvoice(parameterMap, var8, var14);


        System.out.println("aaaaaaaaaaa  parameterMap orderconfirm "+disposable.toString()+"   parameters   "+parameterMap );
        Log.i("aaaaaaaa   Result", "parameterMap : " + disposable);
        (new CompositeDisposable()).add(disposable.
                observeOn(AndroidSchedulers.mainThread()).
                subscribeOn(Schedulers.io()).subscribe((Consumer)(new Consumer() {
            // $FF: synthetic method
            // $FF: bridge method
            public void accept(Object var1) {
                this.accept((GetEmployeeQenerateInvoice)var1);
            }

            public final void accept(GetEmployeeQenerateInvoice result) {
                mDialog.cancel();
                if (result != null) {
                    mDialog.cancel();
                    System.out.println("aaaaaaaaa  result orderconfirm  "+result.toString());
                    Log.i("aaaaaaa   Result", "" + result.toString());
                    if (result.getStatus().equalsIgnoreCase("0")){
                        String invoiceStr = (new Gson()).toJson(result);
                        Intent myIntent = new Intent(SalesActivity.this, SalesReceiptPrintActivity.class);
                        //myIntent.putExtra("all_data", "$all_data")
                        myIntent.putExtra("invoiceStr", invoiceStr);
                        startActivityForResult(myIntent, request_code);
                        finish();
                        CUC.Companion.displayToast((Context)SalesActivity.this, result.getShow_status(), result.getMessage());
                    }else {
                        if (result.getShow_status().equals("0")){
                            String invoiceStr = new Gson().toJson(result);
                            Intent myIntent = new Intent(SalesActivity.this, SalesReceiptPrintActivity.class);
                            //myIntent.putExtra("all_data", "$all_data")
                            myIntent.putExtra("invoiceStr", invoiceStr);
                            startActivityForResult(myIntent, request_code);
                            finish();
                            CUC.Companion.displayToast((Context)SalesActivity.this, result.getShow_status(), result.getMessage());
                        }else {

                            CUC.Companion.displayToast(SalesActivity.this, result.getShow_status(), result.getMessage());
                        }

                    }
                } else {
                    mDialog.cancel();
                }
            }
        }), (Consumer)(new Consumer() {
            // $FF: synthetic method
            // $FF: bridge method
            public void accept(Object var1) {
                this.accept((Throwable)var1);
            }

            public final void accept(Throwable error) {
                mDialog.cancel();
                Toast.makeText(SalesActivity.this, getString(R.string.connection_to_database_failed), Toast.LENGTH_SHORT).show();
                error.printStackTrace();
                System.out.println("aaaaaaaa  error update "+error.getMessage());
            }
        })));
    }

    private void openCalculater() {
        final Dialog customDialog =new  Dialog(SalesActivity.this, R.style.TransparentProgressDialog);
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        customDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        customDialog.setContentView(R.layout.calculator_item);
        //customDialog.window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        customDialog.show();


        TextView tv_basic = customDialog.findViewById(R.id.tv_basic);
        TextView tv_unit = customDialog.findViewById(R.id.tv_unit);
        TextView tv_cancel = customDialog.findViewById(R.id.tv_cancel);
        tv_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                customDialog.dismiss();
            }
        });

        tv_unit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CalcWindowUnit dialog = new CalcWindowUnit(SalesActivity.this.iv_lock);
                dialog.show();
                customDialog.dismiss();
            }
        });
        tv_basic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CalcWindowBasic dialog = new CalcWindowBasic(SalesActivity.this.iv_lock);
                dialog.show();
                customDialog.dismiss();
            }
        });


    }

    Dialog dialogChangePin;
    String employee_old_pin = "";
    String employee_new_pin = "";
    void showDialogChangePin(final String status) {
        dialogChangePin = new Dialog(SalesActivity.this, R.style.TransparentProgressDialog);
        StrictMode.ThreadPolicy policy =new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        //dialog_changepass.window.setLayout(resources.getDimensionPixelSize(R.dimen._10sdp) ,ViewGroup.LayoutParams.WRAP_CONTENT)
        dialogChangePin.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogChangePin.setContentView(R.layout.change_pin_dialog);
        //dialogChangePin.window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)
        dialogChangePin.show();

        LinearLayout cv_approve_request = dialogChangePin.findViewById(R.id.cv_approve_request);
        LinearLayout cv_generate_invoice = dialogChangePin.findViewById(R.id.cv_generate_invoice);
        final EditText edt_current_pin = dialogChangePin.findViewById(R.id.edt_current_pin);
        final EditText edt_new_pin = dialogChangePin.findViewById(R.id.edt_new_pin);
        final EditText edt_confirm_pin = dialogChangePin.findViewById(R.id.edt_confirm_pin);
        EditText tv_cureent_lbl = dialogChangePin.findViewById(R.id.tv_cureent_lbl);
        TextView tv_new_lbl = dialogChangePin.findViewById(R.id.tv_new_lbl);
                LinearLayout ll_confirm = dialogChangePin.findViewById(R.id.ll_confirm);
        TextView tv_confirm = dialogChangePin.findViewById(R.id.tv_confirm);
        TextView tv_title = dialogChangePin.findViewById(R.id.tv_title);

                ll_confirm.setVisibility(View.VISIBLE);
        if (status == "1") {
            tv_cureent_lbl.setText("Old PIN");
            edt_current_pin.setText("Old PIN");
            tv_new_lbl.setText("New PIN");
            edt_new_pin.setText("New PIN");
            tv_confirm.setText("Confirm PIN");
            edt_confirm_pin.setText("Confirm PIN");
            tv_title.setText("Change PIN");

            edt_current_pin.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_VARIATION_PASSWORD);
            edt_current_pin.setFilters(new InputFilter.LengthFilter[]{new InputFilter.LengthFilter(4)});

            edt_new_pin.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_VARIATION_PASSWORD);
            edt_new_pin.setFilters(new InputFilter.LengthFilter[]{new InputFilter.LengthFilter(4)});

            edt_confirm_pin.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_VARIATION_PASSWORD);
            edt_confirm_pin.setFilters(new InputFilter.LengthFilter[]{new InputFilter.LengthFilter(4)});


        } else {

            tv_cureent_lbl.setText("Old Password");
            edt_current_pin.setText("Old Password");
            tv_new_lbl.setText("New Password");
            edt_new_pin.setText("New Password");
            tv_confirm.setText("Confirm Password");
            edt_confirm_pin.setText("Confirm Password");
            tv_title.setText("Change Password");

            edt_current_pin.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_NUMBER_VARIATION_PASSWORD);
            edt_new_pin.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_NUMBER_VARIATION_PASSWORD);
            edt_confirm_pin.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_NUMBER_VARIATION_PASSWORD);


        }

        cv_approve_request.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogChangePin.dismiss();
            }
        });
        cv_generate_invoice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (status == "1") {

                    if (edt_current_pin.getText().toString().isEmpty()){
                        CUC.Companion.displayToast(SalesActivity.this, "0", "Please Enter Your Current PIN");
                    }else if (edt_new_pin.getText().toString().isEmpty()){
                        CUC.Companion.displayToast(SalesActivity.this, "0", "Please Enter Your New PIN");
                    }else if (edt_new_pin.getText().toString().equals(edt_confirm_pin.getText().toString())) {
                        CUC.Companion.displayToast(SalesActivity.this, "0", getString(R.string.pin_do_not_match));
                    } else {

                        employee_old_pin = edt_current_pin.getText().toString();
                        employee_new_pin = edt_new_pin.getText().toString();
                       // callEmployeePinChange()
                    }
                } else {
                    if (edt_current_pin.getText().toString().isEmpty()){
                        CUC.Companion.displayToast(SalesActivity.this, "0", "Please Enter Your Current Password");
                    }else if (edt_new_pin.getText().toString().isEmpty()){
                        CUC.Companion.displayToast(SalesActivity.this, "0", "Please Enter Your New Password");
                    }else if (edt_new_pin.getText().toString().equals(edt_confirm_pin.getText().toString())) {
                        CUC.Companion.displayToast(SalesActivity.this, "0", getString(R.string.pin_do_not_match));
                    } else {

                        employee_old_pin = edt_current_pin.getText().toString();
                        employee_new_pin = edt_new_pin.getText().toString();
                        // callEmployeePasswordChange()
                    }
                }
            }
        });
    }

    public void callEmployeeScanProducts(String producttag){

        mDialog = CUC.Companion.createDialog(SalesActivity.this);
        RequetsInterface apiService = RequetsInterface.Factory.create();
        Map parameterMap =new HashMap<String, String>();
        parameterMap.put("api_key", String.valueOf(Preferences.INSTANCE.getPreference((SalesActivity.this), "API_KEY")));
        if (logindata != null) {
            parameterMap.put("employee_id", logindata.getUser_id());
            parameterMap.put("store_id", logindata.getUser_store_id());
        }
        if (mBAsis != null) {
            parameterMap.put("terminal_id",mBAsis.getTerminal_id());
        }
        parameterMap.put("product_tag",producttag);
        parameterMap.put("emp_lat", Preferences.INSTANCE.getPreference(SalesActivity.this, "latitude"));
        parameterMap.put("emp_log", Preferences.INSTANCE.getPreference(SalesActivity.this, "longitude"));

        System.out.println("aaaaaaaaaaa  parameterMap scan "+parameterMap);
        Log.i("aaaaaaaa   Result", "parameterMap : " + parameterMap);
        (new CompositeDisposable()).add(apiService.callEmployeeScanProducts(parameterMap).
                observeOn(AndroidSchedulers.mainThread()).
                subscribeOn(Schedulers.io()).subscribe((Consumer)(new Consumer() {
            // $FF: synthetic method
            // $FF: bridge method
            public void accept(Object var1) {
                this.accept((GetScanProducts)var1);
            }

            public final void accept(GetScanProducts result) {
                mDialog.cancel();
                if (result != null) {
                    mDialog.cancel();
                    System.out.println("aaaaaaaaa  result scanresult  "+result.toString());
                    Log.i("aaaaaaa   Result", "" + result.toString());
                    if (result.getStatus().equalsIgnoreCase("0")){
                        getStores(result.getProduct_block().getData().getCategory().getProduct());
                        CUC.Companion.displayToast((Context)SalesActivity.this, result.getShow_status(), result.getMessage());
                    }else {
                        CUC.Companion.displayToast(SalesActivity.this, result.getShow_status(), result.getMessage());
                    }
                } else {
                    mDialog.cancel();
                }
            }
        }), (Consumer)(new Consumer() {
            // $FF: synthetic method
            // $FF: bridge method
            public void accept(Object var1) {
                this.accept((Throwable)var1);
            }

            public final void accept(Throwable error) {
                mDialog.cancel();
                Toast.makeText(SalesActivity.this, getString(R.string.connection_to_database_failed), Toast.LENGTH_SHORT).show();
                error.printStackTrace();
                System.out.println("aaaaaaaa  error update "+error.getMessage());
            }
        })));
    }
    public void getStores(final GetSalesProductScan getProducts){

        mDialog = CUC.Companion.createDialog(SalesActivity.this);
        RequetsInterface apiService = RequetsInterface.Factory.create();
        Map parameterMap =new HashMap<String, String>();
        parameterMap.put("api_key", String.valueOf(Preferences.INSTANCE.getPreference((SalesActivity.this), "API_KEY")));

        if (logindata != null) {
            parameterMap.put("store_id", logindata.getUser_store_id());
            parameterMap.put("cart_detail_id", "");
            parameterMap.put("product_id", getProducts.getProduct_id());
            parameterMap.put("packet_id", getProducts.getPacket_id());
        }

        System.out.println("aaaaaaaaaaa  parameterMap "+parameterMap);
        Log.i("aaaaaaaa   Result", "parameterMap : " + parameterMap);
        (new CompositeDisposable()).add(apiService.callEmployeeProductQty(parameterMap).
                observeOn(AndroidSchedulers.mainThread()).
                subscribeOn(Schedulers.io()).subscribe((Consumer)(new Consumer() {
            // $FF: synthetic method
            // $FF: bridge method
            public void accept(Object var1) {
                this.accept((GetStatus)var1);
            }

            public final void accept(GetStatus result) {
                mDialog.cancel();
                if (result != null) {
                    mDialog.cancel();
                    System.out.println("aaaaaaaaa  result  "+result.toString());
                    Log.i("aaaaaaa   Result", "" + result.toString());
                    if (result.getStatus().equalsIgnoreCase("0")) {
                      /* if (result.getInventory_qty().equals("0")){
                           Toast.makeText(SalesActivity.this, "Currently Products Not There", Toast.LENGTH_SHORT).show();
                           CUC.Companion.displayToast(SalesActivity.this, "Currently Products Not There", result.getMessage());
                       }else {*/
                        AddProductDialog addProductDialog=new AddProductDialog();
                        addProductDialog.showDialog(SalesActivity.this,result,getProducts,logindata,mBAsis,queue_patient_id,queue_id);
                        // }
                        CUC.Companion.displayToast(SalesActivity.this, result.getShow_status(), result.getMessage());
                    } else {
                        Toast.makeText(SalesActivity.this, ""+result.getShow_status(), Toast.LENGTH_SHORT).show();
                        CUC.Companion.displayToast(SalesActivity.this, result.getShow_status(), result.getMessage());
                    }
                } else {
                    mDialog.cancel();
                }
            }
        }), (Consumer)(new Consumer() {
            // $FF: synthetic method
            // $FF: bridge method
            public void accept(Object var1) {
                this.accept((Throwable)var1);
            }

            public final void accept(Throwable error) {
                mDialog.cancel();
                Toast.makeText(SalesActivity.this, getString(R.string.connection_to_database_failed), Toast.LENGTH_SHORT).show();
                error.printStackTrace();
                System.out.println("aaaaaaaa  error "+error.getMessage());
            }
        })));
    }

    public void getcgpatients(){
        caregiverlist.clear();
        mDialog = CUC.Companion.createDialog(SalesActivity.this);
        RequetsInterface apiService = RequetsInterface.Factory.create();
        Map parameterMap =new HashMap<String, String>();
        parameterMap.put("api_key", String.valueOf(Preferences.INSTANCE.getPreference((SalesActivity.this), "API_KEY")));
        if (logindata != null) {
            parameterMap.put("employee_id", logindata.getUser_id());
            // parameterMap.put("store_id", logindata.getUser_store_id());
        }

        if (queueList.getQueue_patient_id()!=null){
            parameterMap.put("patient_id",queueList.getQueue_patient_id());
        }else {
            parameterMap.put("patient_id",queue_patient_id);
        }


        System.out.println("aaaaaaaaaaa  parameterMap getpatientmmp  "+parameterMap);
        Log.i("aaaaaaaa   Result", "parameterMap : " + parameterMap);
        (new CompositeDisposable()).add(apiService.callgetCaregivers(parameterMap).
                observeOn(AndroidSchedulers.mainThread()).
                subscribeOn(Schedulers.io()).subscribe((Consumer)(new Consumer() {
            // $FF: synthetic method
            // $FF: bridge method
            public void accept(Object var1) {
                this.accept((getcaregivers)var1);
            }

            public final void accept(getcaregivers result) {
                System.out.println("aaaaaaaaa  result  "+result.toString());
                mDialog.cancel();
                if (result != null) {
                    mDialog.cancel();
                    System.out.println("aaaaaaaaa  result caregivers  "+result.toString());
                    Log.i("aaaaaaa   Result", "" + result.toString());

                    if (result.getStatus().equals("0")){
                        try{
                            if (!result.getPatient_1().getPatient_id().isEmpty()){
                                caregiverlist.add(0,result.getPatient_1());
                            }

                        }catch (Exception e){

                        }
                        try{
                            if (!result.getPatient_2().getPatient_id().isEmpty()){
                                caregiverlist.add(0,result.getPatient_2());
                            }
                        }catch (Exception e){

                        }
                        try{
                            if (!result.getPatient_3().getPatient_id().isEmpty()){
                                caregiverlist.add(0,result.getPatient_3());
                            }
                        }catch (Exception e){

                        }
                        try{
                            if (!result.getPatient_4().getPatient_id().isEmpty()){
                                caregiverlist.add(0,result.getPatient_4());
                            }
                        }catch (Exception e){

                        }
                        try{
                            if (!result.getPatient_5().getPatient_id().isEmpty()){
                                caregiverlist.add(0,result.getPatient_5());
                            }

                        }catch (Exception e){

                        }

                       // Collections.reverse(caregiverlist);
                        cgAdapter.dataChanged(caregiverlist);
                       // CUC.Companion.displayToast(SalesActivity.this, "Sucess", "Sucess");
                    }else {
                       // CUC.Companion.displayToast(SalesActivity.this, "No Data", "No Data");
                    }
                } else {
                    mDialog.cancel();
                }
            }
        }), (Consumer)(new Consumer() {
            // $FF: synthetic method
            // $FF: bridge method
            public void accept(Object var1) {
                this.accept((Throwable)var1);
            }

            public final void accept(Throwable error) {
                mDialog.cancel();
                Toast.makeText(SalesActivity.this, getResources().getString(R.string.connection_to_database_failed), Toast.LENGTH_SHORT).show();
                error.printStackTrace();
                System.out.println("aaaaaaaa  error update "+error.getMessage());
            }
        })));
    }

    private final ScaleCallback mScaleCallback = new ScaleCallback() {
        @Override
        public void onConnect(Scale scale, int status) {
            boolean connectSuccess = false;

            switch (status) {
                case Scale.CONNECT_SUCCESS:
                    connectSuccess = true;
                    Toast.makeText(SalesActivity.this, "Connect success.", Toast.LENGTH_SHORT).show();
                    break;

                case Scale.CONNECT_NOT_AVAILABLE:
                    Toast.makeText(SalesActivity.this, "Failed to connect. (Not available)", Toast.LENGTH_SHORT).show();
                    break;

                case Scale.CONNECT_ALREADY_CONNECTED:
                    Toast.makeText(SalesActivity.this, "Failed to connect. (Already connected)", Toast.LENGTH_SHORT).show();
                    break;

                case Scale.CONNECT_TIMEOUT:
                    Toast.makeText(SalesActivity.this, "Failed to connect. (Timeout)", Toast.LENGTH_SHORT).show();
                    break;

                case Scale.CONNECT_READ_WRITE_ERROR:
                    Toast.makeText(SalesActivity.this, "Failed to connect. (Read Write error)", Toast.LENGTH_SHORT).show();
                    break;

                case Scale.CONNECT_NOT_SUPPORTED:
                    Toast.makeText(SalesActivity.this, "Failed to connect. (Not supported device)", Toast.LENGTH_SHORT).show();
                    break;

                case Scale.CONNECT_NOT_GRANTED_PERMISSION:
                    Toast.makeText(SalesActivity.this, "Failed to connect. (Not granted permission)", Toast.LENGTH_SHORT).show();
                    break;

                default:
                case Scale.CONNECT_UNEXPECTED_ERROR:
                    Toast.makeText(SalesActivity.this, "Failed to connect. (Unexpected error)", Toast.LENGTH_SHORT).show();
                    break;
            }

            if(!connectSuccess) {
                mScale = null;
                // finish();
            }
        }

        @Override
        public void onDisconnect(Scale scale, int status) {
            mScale = null;

            switch(status) {
                case Scale.DISCONNECT_SUCCESS:
                    Toast.makeText(SalesActivity.this, "Disconnect success.", Toast.LENGTH_SHORT).show();
                    break;

                case Scale.DISCONNECT_NOT_CONNECTED:
                    Toast.makeText(SalesActivity.this, "Failed to disconnect. (Not connected)", Toast.LENGTH_SHORT).show();
                    //finish();
                    break;

                case Scale.DISCONNECT_TIMEOUT:
                    Toast.makeText(SalesActivity.this, "Failed to disconnect. (Timeout)", Toast.LENGTH_SHORT).show();
                    // finish();
                    break;

                case Scale.DISCONNECT_READ_WRITE_ERROR:
                    Toast.makeText(SalesActivity.this, "Failed to disconnect. (Read Write error)", Toast.LENGTH_SHORT).show();
                    // finish();
                    break;

                case Scale.DISCONNECT_UNEXPECTED_ERROR:
                    Toast.makeText(SalesActivity.this, "Failed to disconnect. (Unexpected error)", Toast.LENGTH_SHORT).show();
                    // finish();
                    break;

                default:
                case Scale.DISCONNECT_UNEXPECTED_DISCONNECTION:
                    Toast.makeText(SalesActivity.this, "Unexpected disconnection.", Toast.LENGTH_SHORT).show();
                    // finish();
                    break;
            }
        }


        @Override
        public void onReadScaleData(Scale scale, ScaleData scaleData) {
            if(scaleData.getStatus() == ScaleData.Status.ERROR) { // Error
                System.out.println("aaaaaaaaa   invalid weight");
            } else {
                String weight1 = String.format(Locale.US,"%."+ scaleData.getNumberOfDecimalPlaces() +"f", scaleData.getWeight());
                String unit = scaleData.getUnit().toString();
                weight=weight1;
                String weightStr = weight1 + " [" + unit + "]";
                System.out.println("aaaaaaaaaaaaaa  weight  "+weight1);
                if (cartdetails==1){
                    productquantityupdateDialog.dismiss();
                    productquantityupdateDialog=new ProductquantityupdateDialog(SalesActivity.this,getstatus,getCartData,""+weight);
                    productquantityupdateDialog.show();
                }else {
                    productquantityDialog.dismiss();
                    productquantityDialog=new ProductquantityDialog(SalesActivity.this,getstatus,getProductsa,productsFragment,""+weight);
                    productquantityDialog.show();
                }


               /* addProductDialog1=new AddProductDialog();
                addProductDialog1.showDialog(SalesActivity.this,getstatus,getProductsa,productsFragment,weight1);*/
               /* try{
                    fragmentInterfaceListener.sendDataMethod(""+weight1);
                }catch (NullPointerException e){
                    System.out.println("aaaaaaaa  exceptions   "+e.getMessage());
                }*/


            }
        }

        @Override
        public void onUpdateSetting(Scale scale, ScaleSetting scaleSetting, int status) {
            if (scaleSetting == ScaleSetting.ZeroPointAdjustment) {
                switch(status) {
                    case Scale.UPDATE_SETTING_SUCCESS:
                        Toast.makeText(SalesActivity.this, "Succeeded.", Toast.LENGTH_SHORT).show();
                        break;

                    case Scale.UPDATE_SETTING_NOT_CONNECTED:
                        Toast.makeText(SalesActivity.this, "Failed. (Not connected)", Toast.LENGTH_SHORT).show();
                        break;

                    case Scale.UPDATE_SETTING_REQUEST_REJECTED:
                        Toast.makeText(SalesActivity.this, "Failed. (Request rejected)", Toast.LENGTH_SHORT).show();
                        break;

                    case Scale.UPDATE_SETTING_TIMEOUT:
                        Toast.makeText(SalesActivity.this, "Failed. (Timeout)", Toast.LENGTH_SHORT).show();
                        break;

                    case Scale.UPDATE_SETTING_ALREADY_EXECUTING:
                        Toast.makeText(SalesActivity.this, "Failed. (Already executing)", Toast.LENGTH_SHORT).show();
                        break;

                    default:
                    case Scale.UPDATE_SETTING_UNEXPECTED_ERROR:
                        Toast.makeText(SalesActivity.this, "Failed. (Unexpected error)", Toast.LENGTH_SHORT).show();
                        break;
                }
            }
        }
    };

    public void getStoresproducts(final GetSalesProduct getProducts, final ProductsFragment productsFragment1) {
        mDialog = CUC.Companion.createDialog(SalesActivity.this);
        RequetsInterface apiService = RequetsInterface.Factory.create();
        Map parameterMap =new HashMap<String, String>();
        parameterMap.put("api_key", String.valueOf(Preferences.INSTANCE.getPreference((SalesActivity.this), "API_KEY")));

        if (logindata != null) {
            parameterMap.put("store_id", logindata.getUser_store_id());
            parameterMap.put("cart_detail_id", "");
            parameterMap.put("product_id", getProducts.getProduct_id());
            parameterMap.put("packet_id", getProducts.getPacket_id());
        }

        System.out.println("aaaaaaaaaaa  parameterMap "+parameterMap);
        Log.i("aaaaaaaa   Result", "parameterMap : " + parameterMap);
        (new CompositeDisposable()).add(apiService.callEmployeeProductQty(parameterMap).
                observeOn(AndroidSchedulers.mainThread()).
                subscribeOn(Schedulers.io()).subscribe((Consumer)(new Consumer() {
            // $FF: synthetic method
            // $FF: bridge method
            public void accept(Object var1) {
                this.accept((GetStatus)var1);
            }

            public final void accept(GetStatus result) {
                mDialog.cancel();
                if (result != null) {
                    mDialog.cancel();
                    System.out.println("aaaaaaaaa  result  "+result.toString());
                    Log.i("aaaaaaa   Result", "" + result.toString());
                    if (result.getStatus().equalsIgnoreCase("0")) {
                      /* if (result.getInventory_qty().equals("0")){
                           Toast.makeText(getContext(), "Currently Products Not There", Toast.LENGTH_SHORT).show();
                           CUC.Companion.displayToast(getContext(), "Currently Products Not There", result.getMessage());
                       }else {*/
                        getstatus=result;
                        cartdetails=0;
                        getProductsa=getProducts;
                        productsFragment=productsFragment1;
                         productquantityDialog=new ProductquantityDialog(SalesActivity.this,result,getProducts,productsFragment1,"0");
                        productquantityDialog.show();
                       /* if (mScale==null){
                            addProductDialog1=new AddProductDialog();
                            addProductDialog1.showDialog(SalesActivity.this,result,getProducts,productsFragment1,"0");
                        }*/
                        // }
                        CUC.Companion.displayToast(SalesActivity.this, result.getShow_status(), result.getMessage());
                    } else {
                        Toast.makeText(SalesActivity.this, ""+result.getShow_status(), Toast.LENGTH_SHORT).show();
                        CUC.Companion.displayToast(SalesActivity.this, result.getShow_status(), result.getMessage());
                    }
                } else {
                    mDialog.cancel();
                }
            }
        }), (Consumer)(new Consumer() {
            // $FF: synthetic method
            // $FF: bridge method
            public void accept(Object var1) {
                this.accept((Throwable)var1);
            }

            public final void accept(Throwable error) {
                mDialog.cancel();
                Toast.makeText(SalesActivity.this, getString(R.string.connection_to_database_failed), Toast.LENGTH_SHORT).show();
                error.printStackTrace();
                System.out.println("aaaaaaaa  error "+error.getMessage());
            }
        })));
    }

    public void setCareGiver(getCaregiver getCaregiver) {
        caregiverid=getCaregiver.getPatient_id();
        caregiver=1;
        tv_current_order.setBackground(getResources().getDrawable(R.drawable.black_border_text));
        tv_current_order.setTextColor(getResources().getColor(R.color.blue));
        tvcustomername.setText(getCaregiver.getPatient_fname()+" "+getCaregiver.getPatient_lname());
        getapiCallForEmployeecaregiver(getCaregiver.getPatient_id(),1);
    }

    public void sendToCartea(GetSalesProduct getProducts, Double value, Float discount, int i) {
        mDialog = CUC.Companion.createDialog(SalesActivity.this);
        RequetsInterface apiService = RequetsInterface.Factory.create();
        Map parameterMap =new HashMap<String, String>();
        parameterMap.put("api_key", String.valueOf(Preferences.INSTANCE.getPreference((SalesActivity.this), "API_KEY")));

        if (logindata != null) {
            parameterMap.put("store_id", logindata.getUser_store_id());
            parameterMap.put("employee_id", logindata.getUser_id());
        }
        parameterMap.put("package_id",getProducts.getPacket_id());
        if (caregiver==0){
            if (queue_patient_id.equals("0")){
                parameterMap.put("patient_id",queueList.getQueue_patient_id());
            }else {
                parameterMap.put("patient_id",queue_patient_id);
            }
        }else {
            parameterMap.put("patient_id",caregiverid);
        }


        if (i==1){
            parameterMap.put("percentage_discount", discount);
        }else if (i==2){
            parameterMap.put("fixed_discount", discount);
        }

        parameterMap.put("product_id", getProducts.getProduct_id());
        parameterMap.put("category_id", getProducts.getProduct_cat_id());
        parameterMap.put("package_id", getProducts.getPacket_id());
        parameterMap.put("qty", ""+value);
        parameterMap.put("terminal_id", mBAsis.getTerminal_id());
        parameterMap.put("emp_lat", Preferences.INSTANCE.getPreference(SalesActivity.this, "latitude"));
        parameterMap.put("emp_log", Preferences.INSTANCE.getPreference(SalesActivity.this, "longitude"));
        parameterMap.put("queue_id", queue_id);

        System.out.println("aaaaaaaaaaa  parameterMap add cart "+parameterMap);
        Log.i("aaaaaaaa   Result", "parameterMap : " + parameterMap);
        (new CompositeDisposable()).add(apiService.callEmployeeSalesAddcart(parameterMap).
                observeOn(AndroidSchedulers.mainThread()).
                subscribeOn(Schedulers.io()).subscribe((Consumer)(new Consumer() {
            // $FF: synthetic method
            // $FF: bridge method
            public void accept(Object var1) {
                this.accept((GetEmployeeSalesProducts)var1);
            }

            public final void accept(GetEmployeeSalesProducts result) {
                mDialog.cancel();
                if (result != null) {
                    mDialog.cancel();

                    System.out.println("aaaaaaaaa  result  "+result.toString());
                    Log.i("aaaaaaa   Result", "" + result.toString());
                    if (result.getStatus().equalsIgnoreCase("0")) {
                        System.out.println("aaaaaaaaaa  addcart sucess");

                        try{
                            if (result.getStatus().equals("0") && result.getGrade_status().equals("0")){
                                showdialog(result.getGrade_message());
                            }
                        }catch (NullPointerException e){

                        }try{
                            if (result.getStatus().equals("0") && result.getGrade_warning_status().equals("0")){
                                showdialog(result.getGrade_warning());
                            }
                        }catch (NullPointerException e){

                        }
                        onCart();
                        CUC.Companion.displayToast(SalesActivity.this, result.getShow_status(), result.getMessage());
                    } else if (result.getStatus().equalsIgnoreCase("10")) {

                    } else {
                        //  Toast.makeText(getContext(), ""+result.getShow_status(), Toast.LENGTH_SHORT).show();
                        CUC.Companion.displayToast((Context)SalesActivity.this, result.getShow_status(), result.getMessage());
                    }
                } else {
                    mDialog.cancel();
                }
            }
        }), (Consumer)(new Consumer() {
            // $FF: synthetic method
            // $FF: bridge method
            public void accept(Object var1) {
                this.accept((Throwable)var1);
            }

            public final void accept(Throwable error) {
                mDialog.cancel();
                Toast.makeText(SalesActivity.this, getString(R.string.connection_to_database_failed), Toast.LENGTH_SHORT).show();
                error.printStackTrace();
                System.out.println("aaaaaaaa  error "+error.getMessage());
            }
        })));

    }

    public void sendToCart(GetSalesProduct getProducts, Float value, Float discount, int i) {

        mDialog = CUC.Companion.createDialog(SalesActivity.this);
        RequetsInterface apiService = RequetsInterface.Factory.create();
        Map parameterMap =new HashMap<String, String>();
        parameterMap.put("api_key", String.valueOf(Preferences.INSTANCE.getPreference((SalesActivity.this), "API_KEY")));

        if (logindata != null) {
            parameterMap.put("store_id", logindata.getUser_store_id());
            parameterMap.put("employee_id", logindata.getUser_id());
        }
        parameterMap.put("package_id",getProducts.getPacket_id());
        if (caregiver==0){
            if (queue_patient_id.equals("0")){
                parameterMap.put("patient_id",queueList.getQueue_patient_id());
            }else {
                parameterMap.put("patient_id",queue_patient_id);
            }
        }else {
            parameterMap.put("patient_id",caregiverid);
        }

        if (i==1){
            parameterMap.put("percentage_discount", discount);
        }else if (i==2){
            parameterMap.put("fixed_discount", discount);
        }

        parameterMap.put("product_id", getProducts.getProduct_id());
        parameterMap.put("category_id", getProducts.getProduct_cat_id());
        parameterMap.put("package_id", getProducts.getPacket_id());
        parameterMap.put("qty", ""+value);
        parameterMap.put("terminal_id", mBAsis.getTerminal_id());
        parameterMap.put("emp_lat", Preferences.INSTANCE.getPreference(SalesActivity.this, "latitude"));
        parameterMap.put("emp_log", Preferences.INSTANCE.getPreference(SalesActivity.this, "longitude"));
        parameterMap.put("queue_id", queue_id);

        System.out.println("aaaaaaaaaaa  parameterMap add cart "+parameterMap);
        Log.i("aaaaaaaa   Result", "parameterMap : " + parameterMap);
        (new CompositeDisposable()).add(apiService.callEmployeeSalesAddcart(parameterMap).
                observeOn(AndroidSchedulers.mainThread()).
                subscribeOn(Schedulers.io()).subscribe((Consumer)(new Consumer() {
            // $FF: synthetic method
            // $FF: bridge method
            public void accept(Object var1) {
                this.accept((GetEmployeeSalesProducts)var1);
            }

            public final void accept(GetEmployeeSalesProducts result) {
                mDialog.cancel();
                if (result != null) {
                    mDialog.cancel();

                    System.out.println("aaaaaaaaa  result  "+result.toString());
                    Log.i("aaaaaaa   Result", "" + result.toString());
                    if (result.getStatus().equalsIgnoreCase("0")) {
                        System.out.println("aaaaaaaaaa  addcart sucess");
                        /*try{
                            if (result.getStatus().equals("0") && result.getGrade_status().equals("0")){
                                showdialog(result.getGrade_message());
                            }
                        }catch (NullPointerException e){

                        }try{
                            if (result.getStatus().equals("0") && result.getGrade_warning_status().equals("0")){
                                showdialog(result.getGrade_warning());
                            }
                        }catch (NullPointerException e){

                        }*/
                        try{
                            if (result.getStatus().equals("0") && result.getGrade_status().equals("0")){
                                showdialog(result.getGrade_message());
                            }
                        }catch (NullPointerException e){

                        }try{
                            if (result.getStatus().equals("0") && result.getGrade_warning_status().equals("0")){
                                showdialog(result.getGrade_warning());
                            }
                        }catch (NullPointerException e){

                        }
                      onCart();
                        CUC.Companion.displayToast(SalesActivity.this, result.getShow_status(), result.getMessage());
                    } else {
                        //  Toast.makeText(getContext(), ""+result.getShow_status(), Toast.LENGTH_SHORT).show();
                        CUC.Companion.displayToast((Context)SalesActivity.this, result.getShow_status(), result.getMessage());
                    }
                } else {
                    mDialog.cancel();
                }
            }
        }), (Consumer)(new Consumer() {
            // $FF: synthetic method
            // $FF: bridge method
            public void accept(Object var1) {
                this.accept((Throwable)var1);
            }

            public final void accept(Throwable error) {
                mDialog.cancel();
                Toast.makeText(SalesActivity.this, getString(R.string.connection_to_database_failed), Toast.LENGTH_SHORT).show();
                error.printStackTrace();
                System.out.println("aaaaaaaa  error "+error.getMessage());
            }
        })));
    }

    public interface FragmentInterface {
        void sendDataMethod(String weight);
    }

    public void getapiCallForEmployeecaregiver(final String patientid, final int i){
        if (CUC.Companion.isNetworkAvailablewithPopup((Context)this)) {
             mDialog = CUC.Companion.createDialog(SalesActivity.this);
            RequetsInterface apiService = RequetsInterface.Factory.create();
            Map parameterMap =new HashMap<String, String>();
            parameterMap.put("api_key", String.valueOf(Preferences.INSTANCE.getPreference((Context)this, "API_KEY")));

            if (logindata != null) {
                parameterMap.put("employee_id", logindata.getUser_id());
                //parameterMap.put("patient_id", queue_patient_id);
                parameterMap.put("store_id", logindata.getUser_store_id());
            }

            parameterMap.put("patient_id", patientid);

            System.out.println("aaaaaaaaaa  caregiver click "+parameterMap);
            Log.i("aaaaaaaa   Result", "parameterMap : " + parameterMap);
            (new CompositeDisposable()).add(apiService.callEmployeePatientInfo(parameterMap).observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io()).subscribe((Consumer)(new Consumer() {
                // $FF: synthetic method
                // $FF: bridge method
                public void accept(Object var1) {
                    this.accept((EmployeePatientInfo)var1);
                }

                public final void accept(EmployeePatientInfo result) {
                     mDialog.cancel();
                    if (result != null) {
                        System.out.println("aaaaaaaaa  result  caregiver "+result.toString());
                        Log.i("aaaaaaa   Result", "" + result.toString());
                        if (Intrinsics.areEqual(result.getStatus(), "0")) {
                            if (result.getStatus().equals("0")) {
                                textLimitUsage.setText(""+result.getLimit());
                                // tv_limit_value.setText(""+result.getSpent_limit());
                                noOfAddedItem = Float.parseFloat(result.getSpent_limit());
                                String str = result.getLimit();
                                getcallEmployeePatientCart(patientid,i);
                                try{
                                    notsdata=result.getNotes_data();
                                    for (int i=0;i<notsdata.size();i++){
                                        shownotedialog(notsdata.get(i).getPnote_title(),notsdata.get(i).getPnote_notes());
                                    }
                                }catch (Exception e){

                                }

                                pieGraphLimite = Float.parseFloat(result.getLimit());
                                setChartData();
                            }
                            CUC.Companion.displayToast(SalesActivity.this, result.getShow_status(), result.getMessage());
                        }  else {
                            Toast.makeText(SalesActivity.this, ""+result.getShow_status(), Toast.LENGTH_SHORT).show();
                            CUC.Companion.displayToast((Context)SalesActivity.this, result.getShow_status(), result.getMessage());
                        }
                    } else {
                    }
                }
            }), (Consumer)(new Consumer() {
                // $FF: synthetic method
                // $FF: bridge method
                public void accept(Object var1) {
                    this.accept((Throwable)var1);
                }

                public final void accept(Throwable error) {
                    // mDialog.cancel();
                    Toast.makeText(SalesActivity.this, getString(R.string.connection_to_database_failed), Toast.LENGTH_SHORT).show();
                    error.printStackTrace();
                    System.out.println("aaaaaaaa  error "+error.getMessage());
                }
            })));

        }else {
            System.out.println("aaaaaaaaaa   else ");
        }
    }
}
