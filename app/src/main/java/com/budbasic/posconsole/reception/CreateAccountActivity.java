package com.budbasic.posconsole.reception;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.budbasic.customer.global.Preferences;
import com.budbasic.global.CUC;
import com.budbasic.posconsole.CountryCitySearch;
import com.budbasic.posconsole.Manifest;
import com.budbasic.posconsole.R;
import com.budbasic.posconsole.dialogs.SessionTimeOutDialog;
import com.budbasic.posconsole.global.SingleSectionNotificationHoverMenuService;
import com.budbasic.posconsole.webservice.RequetsInterface;
import com.github.omadahealth.lollipin.lib.managers.AppLockActivity;
import com.github.omadahealth.lollipin.lib.managers.LockManager;
import com.google.gson.Gson;
import com.kotlindemo.model.GetEmployeeAppSetting;
import com.kotlindemo.model.GetEmployeeLogin;
import com.kotlindemo.model.GetStatus;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import kotlin.jvm.internal.Intrinsics;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class CreateAccountActivity extends AppCompatActivity {

    ImageView iv_back,iv_lock;
    private EditText edt_firstname,edt_lastname,edt_address,edt_zipcode,edt_city,edit_email,edit_mobilenumber,
            edit_password,edit_retypepassword,edit_referedby,edit_mmmplicense,edit_note,edt_mmplicenceno_one,edt_mmplicenceno_two,
            edt_mmplicenceno_three,edt_mmplicenceno_four,edt_mmplicenceno_five;
    private TextView edt_dateofbirth,edt_experationdate,edt_state,edit_loyalitygroup,edit_expirationdatemmp,edt_driverlicence,edt_country;
    private CheckBox iscaregiver;
    private ImageView img_licensefront,img_licenseback,img_mmmpfront,img_mmmpback,img_user_profile;
    private LinearLayout layout_caregive,cv_add_patient;
    private String userChoosenTask;
    private int REQUEST_CAMERA = 0, SELECT_FILE = 1;
    private File licencefront,licenceback,mmpfront,mmpback,profilefile;
    private int selectimage=0;
    private  GetEmployeeLogin logindata;
    private Gson gson;
    private int c_day,c_month,c_year;
    Calendar calendar;
    private String driving_license,state_code="";
    public Dialog mDialog;
    GetEmployeeAppSetting mBAsis;
    private String mmplicensenoone="",mmplicensenotwo="",mmplicensenothree="",mmmplicensenofour="",mmmplicensenofive="",patient_id="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_account);

        iv_back=findViewById(R.id.iv_back);
        iv_lock=findViewById(R.id.iv_lock);
        edt_firstname=findViewById(R.id.edt_firstname);
        edt_lastname=findViewById(R.id.edt_lastname);
        edt_dateofbirth=findViewById(R.id.edt_dateofbirth);
        edt_driverlicence=findViewById(R.id.edt_driverlicence);
        edt_experationdate=findViewById(R.id.edt_experationdate);
        edt_address=findViewById(R.id.edt_address);
        edt_zipcode=findViewById(R.id.edt_zipcode);
        edt_city=findViewById(R.id.edt_city);
        edt_state=findViewById(R.id.edt_state);
        edt_country=findViewById(R.id.edt_country);
        edit_email=findViewById(R.id.edit_email);
        edit_mobilenumber=findViewById(R.id.edit_mobilenumber);
        edit_loyalitygroup=findViewById(R.id.edit_loyalitygroup);
        edit_password=findViewById(R.id.edit_password);
        edit_retypepassword=findViewById(R.id.edit_retypepassword);
        edit_referedby=findViewById(R.id.edit_referedby);
        edit_mmmplicense=findViewById(R.id.edit_mmmplicense);
        edit_expirationdatemmp=findViewById(R.id.edit_expirationdatemmp);
        iscaregiver=findViewById(R.id.iscaregiver);
        layout_caregive=findViewById(R.id.layout_caregive);
        edit_note=findViewById(R.id.edit_note);
        img_licensefront=findViewById(R.id.img_licensefront);
        img_licenseback=findViewById(R.id.img_licenseback);
        img_mmmpfront=findViewById(R.id.img_mmmpfront);
        img_mmmpback=findViewById(R.id.img_mmmpback);
        cv_add_patient=findViewById(R.id.cv_add_patient);
        edt_mmplicenceno_one=findViewById(R.id.edt_mmplicenceno_one);
        edt_mmplicenceno_two=findViewById(R.id.edt_mmplicenceno_two);
        edt_mmplicenceno_three=findViewById(R.id.edt_mmplicenceno_three);
        edt_mmplicenceno_four=findViewById(R.id.edt_mmplicenceno_four);
        edt_mmplicenceno_five=findViewById(R.id.edt_mmplicenceno_five);
        img_user_profile=findViewById(R.id.img_user_profile);

        gson=new Gson();

        edt_firstname.setImeOptions(EditorInfo.IME_FLAG_NO_EXTRACT_UI | EditorInfo.IME_ACTION_NEXT);
        edt_lastname.setImeOptions(EditorInfo.IME_FLAG_NO_EXTRACT_UI | EditorInfo.IME_ACTION_NEXT);
        edt_driverlicence.setImeOptions(EditorInfo.IME_FLAG_NO_EXTRACT_UI | EditorInfo.IME_ACTION_NEXT);
        edt_address.setImeOptions(EditorInfo.IME_FLAG_NO_EXTRACT_UI | EditorInfo.IME_ACTION_NEXT);
        edt_zipcode.setImeOptions(EditorInfo.IME_FLAG_NO_EXTRACT_UI | EditorInfo.IME_ACTION_NEXT);
        edt_city.setImeOptions(EditorInfo.IME_FLAG_NO_EXTRACT_UI | EditorInfo.IME_ACTION_NEXT);
        edt_country.setImeOptions(EditorInfo.IME_FLAG_NO_EXTRACT_UI | EditorInfo.IME_ACTION_NEXT);
        edit_email.setImeOptions(EditorInfo.IME_FLAG_NO_EXTRACT_UI | EditorInfo.IME_ACTION_NEXT);
        edit_mobilenumber.setImeOptions(EditorInfo.IME_FLAG_NO_EXTRACT_UI | EditorInfo.IME_ACTION_NEXT);
        edit_password.setImeOptions(EditorInfo.IME_FLAG_NO_EXTRACT_UI | EditorInfo.IME_ACTION_NEXT);
        edit_retypepassword.setImeOptions(EditorInfo.IME_FLAG_NO_EXTRACT_UI | EditorInfo.IME_ACTION_NEXT);
        edit_referedby.setImeOptions(EditorInfo.IME_FLAG_NO_EXTRACT_UI | EditorInfo.IME_ACTION_NEXT);
        edit_mmmplicense.setImeOptions(EditorInfo.IME_FLAG_NO_EXTRACT_UI | EditorInfo.IME_ACTION_NEXT);
        edit_note.setImeOptions(EditorInfo.IME_FLAG_NO_EXTRACT_UI | EditorInfo.IME_ACTION_NEXT);
        edt_mmplicenceno_one.setImeOptions(EditorInfo.IME_FLAG_NO_EXTRACT_UI | EditorInfo.IME_ACTION_NEXT);
        edt_mmplicenceno_two.setImeOptions(EditorInfo.IME_FLAG_NO_EXTRACT_UI | EditorInfo.IME_ACTION_NEXT);
        edt_mmplicenceno_three.setImeOptions(EditorInfo.IME_FLAG_NO_EXTRACT_UI | EditorInfo.IME_ACTION_NEXT);
        edt_mmplicenceno_four.setImeOptions(EditorInfo.IME_FLAG_NO_EXTRACT_UI | EditorInfo.IME_ACTION_NEXT);
        edt_mmplicenceno_five.setImeOptions(EditorInfo.IME_FLAG_NO_EXTRACT_UI | EditorInfo.IME_ACTION_NEXT);

        calendar=Calendar.getInstance();
        c_day=calendar.get(Calendar.DAY_OF_MONTH);
        c_month=calendar.get(Calendar.MONTH);
        c_year=calendar.get(Calendar.YEAR);

        driving_license=getIntent().getStringExtra("driving_license");

        logindata = gson.fromJson(getIntent().getStringExtra("logindata"), GetEmployeeLogin.class);
        edt_driverlicence.setText(driving_license);


        mBAsis = gson.fromJson(Preferences.INSTANCE.getPreference(CreateAccountActivity.this, "AppSetting"), GetEmployeeAppSetting.class);

        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        img_licensefront.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (patient_id.isEmpty() || patient_id.equals("")){
                    checkdatafields(1);
                }else {
                    selectimage=1;
                     selectImage();
                }
            }
        });
        img_licenseback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (patient_id.isEmpty() || patient_id.equals("")){
                    checkdatafields(2);
                }else {
                    selectimage=2;
                    selectImage();
                }
               // checkdatafields(2);
               // selectimage=2;
               // selectImage();
            }
        });
        img_mmmpfront.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               // checkdatafields(3);
                if (patient_id.isEmpty() || patient_id.equals("")){
                    checkdatafields(3);
                }else {
                    selectimage=3;
                    selectImage();
                }
               // selectimage=3;
              //  selectImage();
            }
        });
        img_mmmpback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               // checkdatafields(4);
                if (patient_id.isEmpty() || patient_id.equals("")){
                    checkdatafields(4);
                }else {
                    selectimage=4;
                    selectImage();
                }
              //  selectimage=4;
               // selectImage();
            }
        });

        iv_lock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    startForegroundService(new Intent(CreateAccountActivity.this, SingleSectionNotificationHoverMenuService.class));
                } else {
                    startService(new Intent(CreateAccountActivity.this, SingleSectionNotificationHoverMenuService.class));
                }

                logindata = gson.fromJson(Preferences.INSTANCE.getPreference(CreateAccountActivity.this, "logindata"), GetEmployeeLogin.class);

                LockManager lockManager = LockManager.getInstance();
                if (lockManager.isAppLockEnabled()) {
                    lockManager.enableAppLock(CreateAccountActivity.this, AppLockActivity.class);
                    lockManager.getAppLock().setPasscode(logindata.getUser_pin());
                    Intent intent = new Intent(CreateAccountActivity.this, SessionTimeOutDialog.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivityForResult(intent, 7575);
                }
            }
        });
        iscaregiver.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b){
                    layout_caregive.setVisibility(View.VISIBLE);
                }else {
                    layout_caregive.setVisibility(View.INVISIBLE);
                }
            }
        });
        edt_dateofbirth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DatePickerDialog datePickerDialog = new DatePickerDialog(CreateAccountActivity.this,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                edt_dateofbirth.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);
                            }
                        }, c_year, c_month, c_day);
                datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
                datePickerDialog.show();
            }
        });
        edt_experationdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DatePickerDialog datePickerDialog = new DatePickerDialog(CreateAccountActivity.this,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                edt_experationdate.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);
                            }
                        }, c_year, c_month, c_day);
                datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis());
                datePickerDialog.show();
            }
        });
        edit_expirationdatemmp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DatePickerDialog datePickerDialog = new DatePickerDialog(CreateAccountActivity.this,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                edit_expirationdatemmp.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);
                            }
                        }, c_year, c_month, c_day);
                datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis());
                datePickerDialog.show();
            }
        });
        edt_state.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent passIntent =new Intent(CreateAccountActivity.this, CountryCitySearch.class);
                passIntent.putExtra("IsCity", "1");
                passIntent.putExtra("state_code", "");
                startActivityForResult(passIntent, 9998);
            }
        });
        cv_add_patient.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               if (selectimage==0){
                   CUC.Companion.displayToast(CreateAccountActivity.this, "0", "Upload Drivier License Front");
               }else {
                       if (selectimage==2){
                           CUC.Companion.displayToast(CreateAccountActivity.this, "0", "Upload MMMP License Front");
                       }else {
                           if (selectimage == 5) {
                               if (!patient_id.isEmpty()) {
                                   finish();
                               } else {
                                   CUC.Companion.displayToast(CreateAccountActivity.this, "0", "Upload Drivier License Front");
                               }
                           }
                       }
               }
                
            }
        });
        img_user_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectimage=5;
                selectImage();
            }
        });
    }

    public void  checkdatafields(int i){
        if (edt_firstname.getText().toString().trim().isEmpty()) {
            CUC.Companion.displayToast(CreateAccountActivity.this, "0", "Enter first name");
            edt_firstname.requestFocus();
        }else if (edt_lastname.getText().toString().trim().isEmpty()){
            CUC.Companion.displayToast(CreateAccountActivity.this, "0", "Enter last name");
            edt_lastname.requestFocus();
        }else {
            int conVaild;
            if (!edit_email.getText().toString().trim().isEmpty()) {
                conVaild = 1;
            } else if (!edit_mobilenumber.getText().toString().trim().isEmpty()) {
                conVaild = 2;
            } else {
                conVaild = 0;
            }

            if (conVaild == 0) {
                CUC.Companion.displayToast(CreateAccountActivity.this, "0", "Email or Contact no# please enter atleast one of them. it will be used for username.");
            } else {
                if (conVaild == 1 && edit_email.getText().toString().trim().isEmpty()) {
                    CUC.Companion.displayToast(CreateAccountActivity.this, "0", "Enter email address");
                    edit_email.requestFocus();
                } else if (conVaild == 1 && !CUC.Companion.isValidEmail(edit_email.getText().toString().trim())) {
                    CUC.Companion.displayToast(CreateAccountActivity.this, "0", "Enter valid email address");
                    edit_email.requestFocus();
                } else if (conVaild == 2 && edit_mobilenumber.getText().toString().trim() == "") {
                    CUC.Companion.displayToast(CreateAccountActivity.this, "0", "Enter mobile number");
                    edit_mobilenumber.requestFocus();
                }

                else if (edt_driverlicence.getText().toString().trim().isEmpty()) {
                    CUC.Companion.displayToast(CreateAccountActivity.this, "0", "Scan driver licence number");
                    edt_driverlicence.requestFocus();
                } else if (edt_experationdate.getText().toString().trim().isEmpty()) {

                    CUC.Companion.displayToast(CreateAccountActivity.this, "0", "Enter driver licence expiration date");
                    edt_experationdate.requestFocus();
                }/* else if (iscaregiver.isChecked() && edt_mmplicenceno.getText().toString().trim().isEmpty()) {
                            CUC.Companion.displayToast(CreateAccountActivity.this, "0", "Enter MMMP Licence Number");
                            edt_mmplicenceno.requestFocus();
                        } else if (iscaregiver.isChecked && edt_mmmpliceexpdt.text.toString().trim().isEmpty()) {
                            CUC.displayToast(CreateAccountActivity.this, "0", "Enter MMMP licence expiration date");
                            edt_liceexpdt.requestFocus();
                        } else if (!iscaregiver.isChecked && "${tvselectcaregiver.text}" != "" && edt_mmplicenceno.text.toString().trim() == "") {
                            CUC.displayToast(CreateAccountActivity.this, "0", "Enter MMMP Licence Number");
                            edt_liceno.requestFocus();
                        } else if (!iscaregiver.isChecked && "${tvselectcaregiver.text}" != "" && edt_mmmpliceexpdt.text.toString().trim() == "") {
                            CUC.displayToast(CreateAccountActivity.this, "0", "Enter MMMP licence expiration date");
                            edt_liceexpdt.requestFocus();
                        } */else if (edt_address.getText().toString().trim().isEmpty()) {
                    CUC.Companion.displayToast(CreateAccountActivity.this, "0", "Enter address");
                    edt_address.requestFocus();
                } else if (edt_city.getText().toString().trim().isEmpty()) {
                    CUC.Companion.displayToast(CreateAccountActivity.this, "0", "Enter city");
                    edt_city.requestFocus();
                } else if (state_code.isEmpty()) {
                    CUC.Companion.displayToast(CreateAccountActivity.this, "0", "Enter state");
                    edt_state.requestFocus();
                }/* else if (edt_mobileapp.text.toString().trim() == "") {
                        CUC.displayToast(CreateAccountActivity.this, "0", "Enter mobile App")
                        edt_mobileapp.requestFocus()
                    } */ else if (edt_zipcode.getText().toString().trim().isEmpty()) {
                    CUC.Companion.displayToast(CreateAccountActivity.this, "0", "Enter zipCode");
                    edt_zipcode.requestFocus();
                } else {
                        mmplicensenoone=edt_mmplicenceno_one.getText().toString().trim();
                        mmplicensenotwo=edt_mmplicenceno_two.getText().toString().trim();
                        mmplicensenothree=edt_mmplicenceno_three.getText().toString().trim();
                        mmmplicensenofour=edt_mmplicenceno_four.getText().toString().trim();
                        mmmplicensenofive=edt_mmplicenceno_five.getText().toString().trim();

                        if (selectimage==5){
                            callEmployeeCreatePatientWithImage(i);
                        }else {
                            callEmployeeCreatePatient(i);
                        }



                }
            }

        }
    }

    private void selectImage() {
        final CharSequence[] items = { "Take Photo", "Choose from Library",
                "Cancel" };

        AlertDialog.Builder builder = new AlertDialog.Builder(CreateAccountActivity.this);
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                boolean result=Utility.checkPermission(CreateAccountActivity.this);

                if (items[item].equals("Take Photo")) {
                    userChoosenTask ="Take Photo";
                    if(result)
                        cameraIntent();

                } else if (items[item].equals("Choose from Library")) {
                    userChoosenTask ="Choose from Library";
                    if(result)
                        galleryIntent();

                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    private void galleryIntent()
    {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);//
        startActivityForResult(Intent.createChooser(intent, "Select File"),SELECT_FILE);
    }

    private void cameraIntent()
    {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, REQUEST_CAMERA);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode==9998){
            if (data != null && data.hasExtra("state_code")) {
                state_code = data.getStringExtra("state_code");
                edt_state.setTag(data.getStringExtra("state_id"));

                edt_state.setText(data.getStringExtra("state"));
            }
        }
        else {
            if (resultCode == Activity.RESULT_OK) {
                if (requestCode == SELECT_FILE)
                    onSelectFromGalleryResult(data);
                else if (requestCode == REQUEST_CAMERA)
                    onCaptureImageResult(data);
            }
        }
    }

    private void onCaptureImageResult(Intent data) {
        Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        thumbnail.compress(Bitmap.CompressFormat.JPEG, 90, bytes);

        File destination = new File(Environment.getExternalStorageDirectory(),
                System.currentTimeMillis() + ".jpg");

        FileOutputStream fo;
        try {
            destination.createNewFile();
            fo = new FileOutputStream(destination);
            fo.write(bytes.toByteArray());
            fo.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("aaaaaaaaaaa   file  "+destination);
        if (selectimage==1){
            licencefront=destination;
            img_licensefront.setImageBitmap(thumbnail);
            addDocumetImg(licencefront,"DL","FR");
        }else if (selectimage==2){
            licenceback=destination;
            img_licenseback.setImageBitmap(thumbnail);
            addDocumetImg(licenceback,"DL","");
        }else if (selectimage==3){
            mmpfront=destination;
            img_mmmpfront.setImageBitmap(thumbnail);
            addDocumetImg(mmpfront,"","FR");
        }else if (selectimage==4){
            mmpback=destination;
            img_mmmpback.setImageBitmap(thumbnail);
            addDocumetImg(mmpback,"","");
        }else if (selectimage==5){
            profilefile=destination;
            img_user_profile.setImageBitmap(thumbnail);
        }

    }

    @SuppressWarnings("deprecation")
    private void onSelectFromGalleryResult(Intent data) {

        ByteArrayOutputStream bytes = new ByteArrayOutputStream();

        Bitmap bm=null;
        if (data != null) {
            try {
                bm = MediaStore.Images.Media.getBitmap(getApplicationContext().getContentResolver(), data.getData());
                bm.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        File destination = new File(Environment.getExternalStorageDirectory(),
                System.currentTimeMillis() + ".jpg");

        FileOutputStream fo;
        try {
            destination.createNewFile();
            fo = new FileOutputStream(destination);
            fo.write(bytes.toByteArray());
            fo.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("aaaaaaaaaaa   file  "+destination);
        if (selectimage==1){
            licencefront=destination;
            img_licensefront.setImageBitmap(bm);
            addDocumetImg(licencefront,"DL","FR");
        }else if (selectimage==2){
            licenceback=destination;
            img_licenseback.setImageBitmap(bm);
            addDocumetImg(licenceback,"DL","");
        }else if (selectimage==3){
            mmpfront=destination;
            img_mmmpfront.setImageBitmap(bm);
            addDocumetImg(mmpfront,"","FR");
        }else if (selectimage==4){
            mmpback=destination;
            img_mmmpback.setImageBitmap(bm);
            addDocumetImg(mmpback,"","FR");
        }else if (selectimage==5){
            profilefile=destination;
            img_user_profile.setImageBitmap(bm);
        }
    }


    public void callEmployeeCreatePatient(final int i){
        mDialog = CUC.Companion.createDialog(CreateAccountActivity.this);
        RequetsInterface apiService = RequetsInterface.Factory.create();
        Map parameterMap =new HashMap<String, String>();
        parameterMap.put("api_key", String.valueOf(Preferences.INSTANCE.getPreference((CreateAccountActivity.this), "API_KEY")));

        parameterMap.put("patient_fname", edt_firstname.getText().toString().trim());
        parameterMap.put("patient_lname", edt_lastname.getText().toString().trim());
        parameterMap.put("patient_reffered", edit_referedby.getText().toString().trim());
        parameterMap.put("patient_mobile_no", ""+edit_mobilenumber.getText().toString().trim());
        parameterMap.put("patient_licence_no", ""+edt_driverlicence.getText().toString().trim());
        parameterMap.put("patient_licence_exp_date", ""+edt_experationdate.getText().toString().trim());
        parameterMap.put("patient_mmpp_licence_no", ""+edit_mmmplicense.getText().toString().trim());
        parameterMap.put("patient_mmpp_licence_exp_date", ""+edit_expirationdatemmp.getText().toString().trim());
        parameterMap.put("patient_address", ""+edt_address.getText().toString().trim());
        parameterMap.put("patient_city", ""+edt_city.getText().toString().trim());
        parameterMap.put("patient_state", ""+state_code.toString());
        parameterMap.put("patient_country", ""+edt_country.getText().toString().trim());
        parameterMap.put("patient_zipcode", ""+edt_zipcode.getText().toString().trim());
        parameterMap.put("patient_mobile_app", "2");
        //parameterMap.put("patient_mobile_app", ""+edt_zipcode.getText().toString().trim());
        parameterMap.put("patient_dob", ""+edt_dateofbirth.getText().toString().trim());
        parameterMap.put("patient_loyalty_group", ""+edit_loyalitygroup.getText().toString().trim());
        parameterMap.put("patient_email_id", ""+edit_email.getText().toString().trim());
        parameterMap.put("patient_updated_by", ""+logindata.getUser_id());
       // parameterMap.put("patient_regester_at", ""+logindata.getUser_id());
        parameterMap.put("patient_terminal_id", ""+mBAsis.getTerminal_id());
        parameterMap.put("patient_terminal_lat", ""+Preferences.INSTANCE.getPreference(CreateAccountActivity.this, "latitude"));
        parameterMap.put("patient_terminal_log", ""+Preferences.INSTANCE.getPreference(CreateAccountActivity.this, "longitude"));
        parameterMap.put("patient_password", ""+edit_password.getText().toString());
        parameterMap.put("patient_notes", ""+edit_note.getText().toString());
        parameterMap.put("patient_type", "");
        parameterMap.put("patient_loyalty_group", "");
        parameterMap.put("patient_regester_at", ""+logindata.getUser_store_id());

        if (!iscaregiver.isChecked()){
            parameterMap.put("patient_is_caregiver", "0");
            parameterMap.put("patient_caregiver_id", "");
        }else {
            parameterMap.put("patient_is_caregiver", "1");
            parameterMap.put("patient_caregiver_id", "");
            parameterMap.put("patient1_mmmp_no", ""+mmplicensenoone);
            parameterMap.put("patient2_mmmp_no", ""+mmplicensenotwo);
            parameterMap.put("patient3_mmmp_no", ""+mmplicensenothree);
            parameterMap.put("patient4_mmmp_no", ""+mmmplicensenofour);
            parameterMap.put("patient5_mmmp_no", ""+mmmplicensenofive);

        }
        System.out.println("aaaaaaaaa  createacc   "+parameterMap.toString());

        (new CompositeDisposable()).add(apiService.callGetEmployeeCreatePatient(parameterMap).
                observeOn(AndroidSchedulers.mainThread()).
                subscribeOn(Schedulers.io()).subscribe((Consumer)(new Consumer() {
            // $FF: synthetic method
            // $FF: bridge method
            public void accept(Object var1) {
                this.accept((GetStatus)var1);
            }

            public final void accept(GetStatus result) {
                mDialog.cancel();
                if (result != null) {
                    mDialog.cancel();

                    System.out.println("aaaaaaaaa  result createacc  "+result.toString());
                    Log.i("aaaaaaa   Result", "" + result.toString());
                    if (Intrinsics.areEqual(result.getStatus(), "0")) {
                        patient_id=result.getPatient_id();
                        selectimage=i;
                        selectImage();
                        System.out.println("aaaaaaaaa patient_id  "+patient_id);
                      //  CUC.Companion.displayToast(CreateAccountActivity.this, result.getShow_status(), result.getMessage());
                    } else {
                         Toast.makeText(CreateAccountActivity.this, ""+result.getMessage(), Toast.LENGTH_SHORT).show();
                        CUC.Companion.displayToast((Context)CreateAccountActivity.this, result.getMessage(), result.getMessage());
                    }
                } else {
                    mDialog.cancel();
                }
            }
        }), (Consumer)(new Consumer() {
            // $FF: synthetic method
            // $FF: bridge method
            public void accept(Object var1) {
                this.accept((Throwable)var1);
            }

            public final void accept(Throwable error) {
                mDialog.cancel();
                Toast.makeText(CreateAccountActivity.this, getString(R.string.connection_to_database_failed), Toast.LENGTH_SHORT).show();
                error.printStackTrace();
                System.out.println("aaaaaaaa  error update "+error.getMessage());
            }
        })));


    }


    public void callEmployeeCreatePatientWithImage(final int i){
        mDialog = CUC.Companion.createDialog(CreateAccountActivity.this);
        RequetsInterface apiService = RequetsInterface.Factory.create();
        Map parameterMap =new HashMap<String, String>();

        parameterMap.put("api_key", RequestBody.create(MediaType.parse("text/plain"), Preferences.INSTANCE.getPreference(CreateAccountActivity.this, "API_KEY")));
        parameterMap.put("employee_id", RequestBody.create(MediaType.parse("text/plain"),  logindata.getUser_id()));
        parameterMap.put("patient_fname", RequestBody.create(MediaType.parse("text/plain"),  edt_firstname.getText().toString().trim()));
        parameterMap.put("patient_lname", RequestBody.create(MediaType.parse("text/plain"),  edt_lastname.getText().toString().trim()));
        parameterMap.put("patient_reffered", RequestBody.create(MediaType.parse("text/plain"),  edit_referedby.getText().toString().trim()));
        parameterMap.put("patient_mobile_no", RequestBody.create(MediaType.parse("text/plain"),  edit_mobilenumber.getText().toString().trim()));
        parameterMap.put("patient_licence_no", RequestBody.create(MediaType.parse("text/plain"),  edt_driverlicence.getText().toString().trim()));
        parameterMap.put("patient_licence_exp_date", RequestBody.create(MediaType.parse("text/plain"),  edt_experationdate.getText().toString().trim()));
        parameterMap.put("patient_mmpp_licence_no", RequestBody.create(MediaType.parse("text/plain"),  edit_mmmplicense.getText().toString().trim()));
        parameterMap.put("patient_mmpp_licence_exp_date", RequestBody.create(MediaType.parse("text/plain"),  edit_expirationdatemmp.getText().toString().trim()));
        parameterMap.put("patient_address", RequestBody.create(MediaType.parse("text/plain"),  edt_address.getText().toString().trim()));
        parameterMap.put("patient_city", RequestBody.create(MediaType.parse("text/plain"),  edt_city.getText().toString().trim()));
        parameterMap.put("patient_state", RequestBody.create(MediaType.parse("text/plain"),  state_code.toString()));
        parameterMap.put("patient_country", RequestBody.create(MediaType.parse("text/plain"),  edt_country.getText().toString().trim()));
        parameterMap.put("patient_zipcode", RequestBody.create(MediaType.parse("text/plain"),  edt_zipcode.getText().toString().trim()));
        parameterMap.put("patient_dob", RequestBody.create(MediaType.parse("text/plain"),  edt_dateofbirth.getText().toString().trim()));
        parameterMap.put("patient_loyalty_group", RequestBody.create(MediaType.parse("text/plain"),  edit_loyalitygroup.getText().toString().trim()));
        parameterMap.put("patient_email_id", RequestBody.create(MediaType.parse("text/plain"),  edit_email.getText().toString().trim()));
        parameterMap.put("patient_updated_by", RequestBody.create(MediaType.parse("text/plain"),  logindata.getUser_id()));
        parameterMap.put("patient_terminal_id", RequestBody.create(MediaType.parse("text/plain"),  mBAsis.getTerminal_id()));
        parameterMap.put("patient_terminal_lat", RequestBody.create(MediaType.parse("text/plain"),  Preferences.INSTANCE.getPreference(CreateAccountActivity.this, "latitude")));
        parameterMap.put("patient_terminal_log", RequestBody.create(MediaType.parse("text/plain"),  Preferences.INSTANCE.getPreference(CreateAccountActivity.this, "longitude")));
        parameterMap.put("patient_password", RequestBody.create(MediaType.parse("text/plain"),  edit_password.getText().toString()));
        parameterMap.put("patient_notes", RequestBody.create(MediaType.parse("text/plain"),  edit_note.getText().toString()));
        parameterMap.put("patient_mobile_app", RequestBody.create(MediaType.parse("text/plain"),  "2"));
        parameterMap.put("patient_type", RequestBody.create(MediaType.parse("text/plain"),  ""));
        parameterMap.put("patient_regester_at", RequestBody.create(MediaType.parse("text/plain"),  ""+logindata.getUser_store_id()));



        if (!iscaregiver.isChecked()){
            parameterMap.put("patient_is_caregiver", RequestBody.create(MediaType.parse("text/plain"),  "0"));
            parameterMap.put("patient_caregiver_id", RequestBody.create(MediaType.parse("text/plain"),  ""));
        }else {
            parameterMap.put("patient_is_caregiver", RequestBody.create(MediaType.parse("text/plain"),  "1"));
            parameterMap.put("patient_caregiver_id", RequestBody.create(MediaType.parse("text/plain"),  ""));
            parameterMap.put("patient1_mmmp_no", RequestBody.create(MediaType.parse("text/plain"),  ""+mmplicensenoone));
            parameterMap.put("patient2_mmmp_no", RequestBody.create(MediaType.parse("text/plain"),  ""+mmplicensenotwo));
            parameterMap.put("patient3_mmmp_no", RequestBody.create(MediaType.parse("text/plain"),  ""+mmplicensenothree));
            parameterMap.put("patient4_mmmp_no", RequestBody.create(MediaType.parse("text/plain"),  ""+mmmplicensenofour));
            parameterMap.put("patient5_mmmp_no", RequestBody.create(MediaType.parse("text/plain"),  ""+mmmplicensenofive));

        }



        RequestBody requestFile = RequestBody.create(MediaType.parse("image/jpeg"), profilefile);

        MultipartBody.Part body = MultipartBody.Part.createFormData("patient_image", profilefile.getName(), requestFile);

        System.out.println("aaaaaaaaa  create img "+parameterMap.toString());

        (new CompositeDisposable()).add(apiService.callGetEmployeeCreatePatientWithImage(parameterMap,body,RequestBody.create(MediaType.parse("text/plain"),
                profilefile.getName()))
                .observeOn(AndroidSchedulers.mainThread()).
                        subscribeOn(Schedulers.io()).subscribe((Consumer)(new Consumer() {
            // $FF: synthetic method
            // $FF: bridge method
            public void accept(Object var1) {
                this.accept((GetStatus)var1);
            }

            public final void accept(GetStatus result) {
                mDialog.cancel();
                if (result != null) {
                    mDialog.cancel();

                    System.out.println("aaaaaaaaa  result img createacc  "+result.toString());
                    Log.i("aaaaaaa   Result", "" + result.toString());
                    if (Intrinsics.areEqual(result.getStatus(), "0")) {
                        patient_id=result.getPatient_id();
                        selectimage=i;
                        selectImage();
                        System.out.println("aaaaaaaa  patient_id  "+patient_id);
                        //CUC.Companion.displayToast(CreateAccountActivity.this, result.getShow_status(), result.getMessage());
                    } else {
                         Toast.makeText(CreateAccountActivity.this, ""+result.getMessage(), Toast.LENGTH_SHORT).show();
                        CUC.Companion.displayToast((Context)CreateAccountActivity.this, result.getMessage(), result.getMessage());
                    }
                } else {
                    mDialog.cancel();
                }
            }
        }), (Consumer)(new Consumer() {
            // $FF: synthetic method
            // $FF: bridge method
            public void accept(Object var1) {
                this.accept((Throwable)var1);
            }

            public final void accept(Throwable error) {
                mDialog.cancel();
                Toast.makeText(CreateAccountActivity.this, getString(R.string.connection_to_database_failed), Toast.LENGTH_SHORT).show();
                error.printStackTrace();
                System.out.println("aaaaaaaa  error update "+error.getMessage());
            }
        })));


    }


    public void addDocumetImg(File licencefront, String dl, String title){

        mDialog = CUC.Companion.createDialog(CreateAccountActivity.this);
        RequetsInterface apiService = RequetsInterface.Factory.create();
        Map parameterMap =new HashMap<String, String>();

        parameterMap.put("api_key", RequestBody.create(MediaType.parse("text/plain"), Preferences.INSTANCE.getPreference(CreateAccountActivity.this, "API_KEY")));
        parameterMap.put("employee_id", RequestBody.create(MediaType.parse("text/plain"),  logindata.getUser_id()));
      //  parameterMap.put("patient_id", RequestBody.create(MediaType.parse("text/plain"), patient_data.getPatient_id()));
        parameterMap.put("document_title", RequestBody.create(MediaType.parse("text/plain"), title));
        parameterMap.put("upload_type", RequestBody.create(MediaType.parse("text/plain"), dl));
        parameterMap.put("type", RequestBody.create(MediaType.parse("text/plain"), title));
        parameterMap.put("patient_id", RequestBody.create(MediaType.parse("text/plain"), patient_id));


        RequestBody requestFile = RequestBody.create(MediaType.parse("image/jpeg"), licencefront);

        MultipartBody.Part body = MultipartBody.Part.createFormData("document_image", licencefront.getName(), requestFile);


        System.out.println("aaaaaaaaaaa  parameterMap add document  "+parameterMap);
        Log.i("aaaaaaaa   Result", "parameterMap : " + parameterMap);
        (new CompositeDisposable()).add(apiService.calldocumentsadd(parameterMap,body,RequestBody.create(MediaType.parse("text/plain"),
                licencefront.getName()))
                .observeOn(AndroidSchedulers.mainThread()).
                        subscribeOn(Schedulers.io()).subscribe((Consumer)(new Consumer() {
                    // $FF: synthetic method
                    // $FF: bridge method
                    public void accept(Object var1) {
                        this.accept((GetStatus)var1);
                    }

                    public final void accept(GetStatus result) {
                        System.out.println("aaaaaaaaa  result  "+result.toString());
                        mDialog.cancel();
                        if (result != null) {
                            mDialog.cancel();
                            System.out.println("aaaaaaaaa  result add document  "+result.toString());
                            Log.i("aaaaaaa   Result", "" + result.toString());
                            if (result.getStatus().equalsIgnoreCase("0")){

                              //  Toast.makeText(CreateAccountActivity.this, ""+result.getMessage(), Toast.LENGTH_SHORT).show();
                                CUC.Companion.displayToast(CreateAccountActivity.this, ""+result.getMessage(), ""+result.getMessage());
                            }else {
                                CUC.Companion.displayToast(CreateAccountActivity.this, ""+result.getMessage(), ""+result.getMessage());
                            }
                        } else {
                            mDialog.cancel();
                        }
                    }
                }), (Consumer)(new Consumer() {
                    // $FF: synthetic method
                    // $FF: bridge method
                    public void accept(Object var1) {
                        this.accept((Throwable)var1);
                    }

                    public final void accept(Throwable error) {
                        mDialog.cancel();
                        Toast.makeText(CreateAccountActivity.this, getResources().getString(R.string.connection_to_database_failed), Toast.LENGTH_SHORT).show();
                        error.printStackTrace();
                        System.out.println("aaaaaaaa  error update "+error.getMessage());
                    }
                })));
    }


}
