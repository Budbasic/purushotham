package com.budbasic.posconsole.reception.fragment

import android.app.Activity
import android.app.DatePickerDialog
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.net.Uri
import android.os.Bundle
import android.provider.Settings
import android.support.design.widget.Snackbar
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import com.budbasic.customer.global.Preferences
import com.budbasic.global.CUC
import com.budbasic.posconsole.EmployeeInvoiceDetailsActivity
import com.budbasic.posconsole.R
import com.budbasic.posconsole.ScanQRCodeActivity
import com.budbasic.posconsole.model.APIClass
import com.budbasic.posconsole.reception.EmployeeInvoiceDetails
import com.budbasic.posconsole.reception.StoreManagerActivity
import com.budbasic.posconsole.webservice.RequetsInterface
import com.google.gson.Gson
import com.kotlindemo.model.GetEmployeeAppSetting
import com.kotlindemo.model.GetEmployeeLogin
import com.kotlindemo.model.GetInvoiceOrders
import com.tbruyelle.rxpermissions2.RxPermissions
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.functions.Consumer
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.sales_report_fragment_new.view.*
import java.util.*
import kotlin.collections.HashMap
import kotlin.collections.set


class SalesReportFragment : Fragment(), APIClass.onCallListner {
    override fun onFailed(requestCode: Int) {
    }

    override fun onSuccess(requestCode: Int) {
        if (requestCode == EMPLOYEEQUEUELIST) {
            callEmployeeSalesReport()
        }

    }

    val EMPLOYEEQUEUELIST = 4
    var mView: View? = null
    var mDialog: Dialog? = null
    var logindata: GetEmployeeLogin? = null
    var productAdapter: ProductAdapter? = null
    var selectDate = ""
    var isLoading = false
    var isLastPage = false
    var apiClass: APIClass? = null
    var rxPermissions: RxPermissions? = null
    var appSettingData: GetEmployeeAppSetting? = null

    companion object {
        var Currency_value = ""
        var list: ArrayList<GetInvoiceOrders> = ArrayList()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        if (view != null)
            mView = view!!
        else
            mView = inflater.inflate(R.layout.sales_report_fragment_new, container, false)
        apiClass = APIClass(context!!, this)
        appSettingData = Gson().fromJson(Preferences.getPreference(mView!!.context, "AppSetting"), GetEmployeeAppSetting::class.java)
        logindata = Gson().fromJson(Preferences.getPreference(mView!!.context, "logindata"), GetEmployeeLogin::class.java)
        Currency_value = "${appSettingData!!.Currency_value}"
        rxPermissions = RxPermissions(activity!!)
        init()
        return mView
    }

    var queueStatus: Array<String>? = null
    fun init() {
        list.clear()
        mView!!.btn_back.setOnClickListener {
            activity!!.onBackPressed()
        }
        mView!!.ll_search_main.setOnClickListener {
        }
        mView!!.btn_Scan.setOnClickListener {
           // val scanQRCode = Intent(activity!!, ScanQRCodeActivity::class.java)
            Log.i("Qr","ScanQr Open")
           // startActivityForResult(scanQRCode, 305)
            rxPermissions!!.request(
                    android.Manifest.permission.CAMERA)
                    .subscribe(object : Consumer<Boolean> {
                        override fun accept(granted: Boolean?) {
                            if (granted!!) { // Always true pre-M
                                // I can control the camera now
                                val scanQRCode = Intent(activity!!, ScanQRCodeActivity::class.java)
                                startActivityForResult(scanQRCode, 305)
                            } else {
                                // Oups permission denied
                                Snackbar.make(activity!!.findViewById(R.id.rl_store_main), resources.getString(R.string.permissionenable),
                                        Snackbar.LENGTH_LONG)
                                        .setActionTextColor(Color.WHITE)
                                        .setAction(resources.getString(R.string.ok), object : View.OnClickListener {
                                            override fun onClick(p0: View?) {
                                                val scanQRCode = Intent(activity!!, ScanQRCodeActivity::class.java)
                                                startActivityForResult(scanQRCode, 305)
                                            }
                                        }).show()
                            }
                        }
                    })
        }
        val layout_manager = LinearLayoutManager(mView!!.context)
        mView!!.rv_customer_list!!.layoutManager = layout_manager
        productAdapter = ProductAdapter(mView!!.context, object : onClickListener {
            override fun callClick(data: GetInvoiceOrders) {
              //  val myIntent = Intent(activity!!, EmployeeInvoiceDetailsActivity::class.java)
                val myIntent = Intent(activity!!, EmployeeInvoiceDetails::class.java)
                myIntent.putExtra("status", "sales_report")
                myIntent.putExtra("order_type", "4")
                myIntent.putExtra("user_info_page", ""+data.patient_fname+" "+data.patient_lname)

                val invoiceStr = Gson().toJson(data)
                myIntent.putExtra("all_data", "$invoiceStr")
                activity!!.startActivityForResult(myIntent, 605)
            }
        })

        queueStatus = resources.getStringArray(R.array.queue_status)
        mView!!.rv_customer_list!!.adapter = productAdapter
        var mYear = Calendar.getInstance().get(Calendar.YEAR)
        var mMonth = Calendar.getInstance().get(Calendar.MONTH)
        var mDay = Calendar.getInstance().get(Calendar.DAY_OF_MONTH)
        selectDate = "$mYear-${(if (mMonth + 1 >= 10) mMonth + 1 else "0" + (mMonth + 1))}-${(if (mDay >= 10) mDay else "0$mDay")}"

        mView!!.tv_start_date.text = "${(if (mMonth + 1 >= 10) mMonth + 1 else "0" + (mMonth + 1))}-${(if (mDay >= 10) mDay else "0$mDay")}-${mYear}"

        mView!!.tv_start_date.setOnClickListener {
            val datePickerDialog = DatePickerDialog(mView!!.context, DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                mYear = year
                mMonth = monthOfYear
                mDay = dayOfMonth

                selectDate = "$year-${(if (monthOfYear + 1 >= 10) monthOfYear + 1 else "0" + (monthOfYear + 1))}-${(if (dayOfMonth >= 10) dayOfMonth else "0$dayOfMonth")}"
                mView!!.tv_start_date.text = "${(if (monthOfYear + 1 >= 10) monthOfYear + 1 else "0" + (monthOfYear + 1))}-${(if (dayOfMonth >= 10) dayOfMonth else "0$dayOfMonth")}-$year"
            }, mYear, mMonth, mDay)
            datePickerDialog.datePicker.maxDate = System.currentTimeMillis()
            datePickerDialog.show()
        }

        mView!!.btn_search.setOnClickListener {
            paging = 0
            offset = 0
            isLoading = false
            isLastPage = false
            list.clear()
            productAdapter!!.notifyDataSetChanged()
            callEmployeeSalesReport()
        }

        mView!!.rv_customer_list!!.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView?, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)

                val lastvisibleitemposition = layout_manager.findLastVisibleItemPosition()

                if (lastvisibleitemposition == productAdapter!!.itemCount - 1) {
                    if (!isLoading && !isLastPage) {
                        isLoading = true
                        callEmployeeSalesReport()
                        // Increment the pagecount everytime we scroll to fetch data from the next page
                        // make loading = false once the data is loaded
                        // call mAdapter.notifyDataSetChanged() to refresh the Adapter and Layout
                    }
                }
            }
        })
        mView!!.mSwipeRefreshLayout.setOnRefreshListener {
            paging = 0
            offset = 0
            isLoading = false
            isLastPage = false
            list.clear()
            productAdapter!!.notifyDataSetChanged()
            callEmployeeSalesReport()
        }
        callEmployeeSalesReport()
    }

    override fun onResume() {
        super.onResume()
       // callEmployeeSalesReport()
    }

    override fun onStart() {
        super.onStart()
    }

    override fun onStop() {
        super.onStop()
    }

    var paging = 0
    var offset = 0
    fun callEmployeeSalesReport() {
        list.clear()
        Log.i("Result", "callEmployeeSalesReport()")
        try {
            if (!activity!!.isFinishing) {
                if (mView!!.mSwipeRefreshLayout != null) {
                    mView!!.mSwipeRefreshLayout.isRefreshing = false
                }
                if (mDialog != null && mDialog!!.isShowing)
                    mDialog!!.dismiss()
                mDialog = CUC.createDialog(activity!!)
                val apiService = RequetsInterface.Factory.create()
                val parameterMap = HashMap<String, String>()
                parameterMap["api_key"] = "${Preferences.getPreference(activity!!, "API_KEY")}"
                if (logindata != null) {
                    parameterMap["employee_id"] = "${logindata!!.user_id}"
                }
                parameterMap["invoice_date"] = "$selectDate"
                parameterMap["paging"] = "$paging"
                parameterMap["offset"] = "$offset"
                println("aaaaaaaaaa salesreport  parameters "+parameterMap.toString())
                Log.i("Result", "$parameterMap")
                CompositeDisposable().add(apiService.callEmployeeSalesReport(parameterMap)
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.io())
                        .subscribe({ result ->
                            if (mDialog != null && mDialog!!.isShowing)
                                mDialog!!.dismiss()
                            if (result != null) {
                                println("aaaaaaaaaa salesreport  result "+result.toString())
                                Log.i("Result", "" + result.toString())
                                if (result.status == "0") {
                                    if (result.invoice_orders != null && result.invoice_orders.size > 0) {
                                        if (result!!.offset.isNotEmpty())
                                            offset = result!!.offset.toInt()

                                        paging += result.invoice_orders.size
                                        list.addAll(result.invoice_orders)
                                        productAdapter!!.notifyDataSetChanged()
                                        isLoading = false
                                        isLastPage = false
                                    } else {
                                        isLoading = true
                                        isLastPage = true
                                    }
                                    if (!activity!!.isFinishing)
                                        CUC.displayToast(activity!!, result.show_status, result.message)
                                } else if (result.status == "10") {
                                    apiClass!!.callApiKey(EMPLOYEEQUEUELIST)
                                } else {
                                    try {
                                        if (!activity!!.isFinishing) {
                                            CUC.displayToast(activity!!, result.show_status, result.message)
                                            list.clear()
                                            productAdapter!!.notifyDataSetChanged()
                                        }
                                    } catch (e: Exception) {
                                        e.printStackTrace()
                                    }
                                }
                            } else {
                                if (!activity!!.isFinishing) {
                                   // CUC.displayToast(activity!!, "0", getString(R.string.connection_to_database_failed))
                                    list.clear()
                                    productAdapter!!.notifyDataSetChanged()
                                }
                            }
                        }, { error ->
                            if (!activity!!.isFinishing) {
                                list.clear()
                                productAdapter!!.notifyDataSetChanged()
                                if (mDialog != null && mDialog!!.isShowing)
                                    mDialog!!.dismiss()
                                CUC.displayToast(activity!!, "0", getString(R.string.connection_to_database_failed))
                            }
                            error.printStackTrace()
                        })
                )
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun callEmployeeSearchInvoice() {
        Log.i("Result", "callEmployeeSearchInvoice()")
        try {
            if (!activity!!.isFinishing) {
                mDialog = CUC.createDialog(activity!!)
                val apiService = RequetsInterface.create()
                val parameterMap = HashMap<String, String>()
                parameterMap["api_key"] = "${Preferences.getPreference(activity!!, "API_KEY")}"
                if (logindata != null) {
                    parameterMap["employee_id"] = "${logindata!!.user_id}"
                }
                parameterMap["invoice_order_no"] = "$invoice_order_no"
                Log.i("Result", "$parameterMap")
                CompositeDisposable().add(apiService.callEmployeeSearchInvoice(parameterMap)
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.io())
                        .subscribe({ result ->
                            if (mDialog != null && mDialog!!.isShowing)
                                mDialog!!.dismiss()
                            if (result != null) {
                                Log.i("Result", "" + result.toString())
                                if (result.status == "0") {
                                    if (result.invoice_orders != null && result.invoice_orders.size > 0) {
//                                        if (result!!.offset.isNotEmpty())
//                                            offset = result!!.offset.toInt()
                                        //paging += result.invoice_orders.size
                                        list.clear()
                                        list.addAll(result.invoice_orders)
                                        productAdapter!!.notifyDataSetChanged()
                                        //isLoading = false
                                        //isLastPage = false
                                    } else {
                                        //isLoading = true
                                        //isLastPage = true
                                    }
                                    if (!activity!!.isFinishing)
                                        CUC.displayToast(activity!!, result.show_status, result.message)
                                } else if (result.status == "10") {
                                    apiClass!!.callApiKey(EMPLOYEEQUEUELIST)
                                } else {
                                    try {
                                        if (!activity!!.isFinishing) {
                                            CUC.displayToast(activity!!, result.show_status, result.message)
                                            list.clear()
                                            productAdapter!!.notifyDataSetChanged()
                                        }
                                    } catch (e: Exception) {
                                        e.printStackTrace()
                                    }
                                }
                            } else {
                                if (!activity!!.isFinishing) {
                                  //  CUC.displayToast(activity!!, "0", getString(R.string.connection_to_database_failed))
                                    list.clear()
                                    productAdapter!!.notifyDataSetChanged()
                                }
                            }
                        }, { error ->
                            if (!activity!!.isFinishing) {
                                list.clear()
                                productAdapter!!.notifyDataSetChanged()
                                if (mDialog != null && mDialog!!.isShowing)
                                    mDialog!!.dismiss()
                               CUC.displayToast(activity!!, "0", getString(R.string.connection_to_database_failed))
                            }
                            error.printStackTrace()
                        })
                )
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    var invoice_order_no = ""
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 305) {
            if (resultCode == Activity.RESULT_OK) {
                if (data != null && data.hasExtra("qrcode")) {
                    invoice_order_no = data.getStringExtra("qrcode")
                    callEmployeeSearchInvoice()
                }
            }
        }
        if (requestCode == 605) {
            if (resultCode == Activity.RESULT_FIRST_USER) {
                paging = 0
                offset = 0
                isLoading = false
                isLastPage = false
                list.clear()
                productAdapter!!.notifyDataSetChanged()
                var mYear = Calendar.getInstance().get(Calendar.YEAR)
                var mMonth = Calendar.getInstance().get(Calendar.MONTH)
                var mDay = Calendar.getInstance().get(Calendar.DAY_OF_MONTH)
                selectDate = "$mYear-${(if (mMonth + 1 >= 10) mMonth + 1 else "0" + (mMonth + 1))}-${(if (mDay >= 10) mDay else "0$mDay")}"

                mView!!.tv_start_date.text = "${(if (mMonth + 1 >= 10) mMonth + 1 else "0" + (mMonth + 1))}-${(if (mDay >= 10) mDay else "0$mDay")}-${mYear}"
                callEmployeeSalesReport()
            }

        }
    }

    class ProductAdapter(val context: Context, val onClick: onClickListener) : RecyclerView.Adapter<ProductAdapter.ViewHolder>() {

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            holder!!.bindItems(list[position], context, onClick)
        }

        override fun getItemCount(): Int {
            return list.size
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            val v = LayoutInflater.from(parent!!.context).inflate(R.layout.sales_report_item_new, parent, false)
            return ViewHolder(v)
        }

        class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
            val tv_name: TextView = itemView.findViewById(R.id.tv_name)
            val tv_mobilenumber: TextView = itemView.findViewById(R.id.tv_mobilenumber)
            val tv_phonenumber: TextView = itemView.findViewById(R.id.tv_phonenumber)
            val tv_patienttype: TextView = itemView.findViewById(R.id.tv_patienttype)
            val tv_mobileapp: TextView = itemView.findViewById(R.id.tv_mobileapp)
            val tv_loyaltygroup: TextView = itemView.findViewById(R.id.tv_loyaltygroup)
            val tv_close: TextView = itemView.findViewById(R.id.tv_close)
            val ll_item_close: LinearLayout = itemView.findViewById(R.id.ll_item_close)
            val ll_queuemain: LinearLayout = itemView.findViewById(R.id.ll_queuemain)

            fun bindItems(data: GetInvoiceOrders, context: Context, onClick: onClickListener) {
                tv_name.text = "${data.invoice_no}"
                tv_mobilenumber.text = CUC.getCapsSentences("${data.patient_fname} ${data.patient_lname}")
                tv_phonenumber.text = "$Currency_value${data.invoice_total_payble}"
                tv_patienttype.text = "$Currency_value${data.invoice_paid_amount}"
                tv_mobileapp.text = "$Currency_value${data.invoice_change}"

                if ("${data.invoice_status}" != "") {
                    if ("${data.invoice_status}" == "3") {

                        tv_close.text = "Paid"
                    } else if ("${data.invoice_status}" == "2") {
                        tv_close.text = "Invoice Generated"
                    } else {
                        tv_close.text = "Received & Progressing"
                    }
                } else {
                    tv_close.text = "--"
                }
               /* if (data.invoice_void.isEmpty()){
                    tv_name.setBackgroundColor(context.resources.getColor(R.color.blue))
                }*/
                 if (data.void_color.equals("1")){
                     if (data.invoice_void.isEmpty()){
                         tv_name.setBackgroundColor(context.resources.getColor(R.color.orange_color))
                     }else{
                         tv_name.setBackgroundColor(context.resources.getColor(R.color.red))
                     }
                }else{
                         tv_name.setBackgroundColor(context.resources.getColor(R.color.blue))
                }

                if (!data.invoice_void.isEmpty()){
                    tv_name.setBackgroundColor(context.resources.getColor(R.color.red))
                    tv_close.text="Void"
                }

                if ("${data.invoice_serve_type}" != "") {
                    if ("${data.invoice_serve_type}" == "2") {
                        tv_loyaltygroup.text = "Delivery"
                    } else {
                        tv_loyaltygroup.text = "Pick Up"
                    }
                } else {
                    tv_loyaltygroup.text = "--"
                }



                ll_queuemain.setOnClickListener {
                    onClick.callClick(data)
                }
            }
        }
    }

    interface onClickListener {
        fun callClick(data: GetInvoiceOrders)
    }
}


