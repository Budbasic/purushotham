package com.budbasic.posconsole.reception.fragment

import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.graphics.Color
import android.net.Uri
import android.os.Bundle
import android.provider.Settings
import android.support.design.widget.Snackbar
import android.support.v4.app.Fragment
import android.support.v7.app.AlertDialog
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.ArrayAdapter
import com.budbasic.customer.global.Preferences
import com.budbasic.global.CUC
import com.budbasic.posconsole.R
import com.budbasic.posconsole.model.APIClass
import com.budbasic.posconsole.queue.BarCodeScan
import com.budbasic.posconsole.queue.MenuActivity
import com.budbasic.posconsole.reception.ReceptionSalesPatient
import com.budbasic.posconsole.reception.ReceptionSalesPatientActivity
import com.budbasic.posconsole.reception.SalesActivity
import com.budbasic.posconsole.sales.SalesCategoryActivity
import com.budbasic.posconsole.webservice.RequetsInterface
import com.google.gson.Gson
import com.kotlindemo.model.GetEmployeeAppSetting
import com.kotlindemo.model.GetEmployeeLogin
import com.kotlindemo.model.GetPatient
import com.tbruyelle.rxpermissions2.RxPermissions
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.functions.Consumer
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.product_items.*
import kotlinx.android.synthetic.main.queue_fragment.view.*
import kotlinx.android.synthetic.main.quick_sale.*
import kotlinx.android.synthetic.main.quick_sale.view.*
import kotlinx.android.synthetic.main.quick_sale.view.btn_back
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap


class ReceptionSales : Fragment(), APIClass.onCallListner {
    override fun onFailed(requestCode: Int) {
    }

    override fun onSuccess(requestCode: Int) {
        if (requestCode == EMPLOYEEPATIENTASSIGNCOMPANY) {
            callEmployeePatientAssignCompany()
        } else if (requestCode == EMPLOYEESALESCREATEQUEUE) {
            callEmployeeSalesCreateQueue()
        } else if (requestCode == EMPLOYEESALESPATIENTCAREGIVERLIST) {
            callEmployeeSalesPatientCaregiverlist()
        }
    }

    val EMPLOYEEPATIENTASSIGNCOMPANY = 1
    val EMPLOYEESALESCREATEQUEUE = 2
    val EMPLOYEESALESPATIENTCAREGIVERLIST = 3
    var appSettingData: GetEmployeeAppSetting? = null
    var mView: View? = null

    val intentGet = "data"

    lateinit var mDialog: Dialog
    var patient_id = ""
    var caregiver_id = "0"
    var storeopen = "0"
    var tillopen = "0"

    lateinit var logindata: GetEmployeeLogin
    lateinit var mPatient: GetPatient
    var assign_msg = ""
    var caregiver = ArrayList<GetPatient>()

    lateinit var careData: GetPatient
    var apiClass: APIClass? = null
    var rxPermissions: RxPermissions? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        if (view != null)
            mView = view!!
        else
            mView = inflater.inflate(R.layout.quick_sale, container, false)
        apiClass = APIClass(context!!, this)
        rxPermissions = RxPermissions(activity!!)
        logindata = Gson().fromJson(Preferences.getPreference(activity!!, "logindata"), GetEmployeeLogin::class.java)
        initView()
        return mView
    }

    fun initView() {

        mView!!.btn_back.setOnClickListener {
            activity!!.onBackPressed()
        }
        mView!!.tv_selectpatient.setOnClickListener {
            val gson = Gson()
            val myJson = gson.toJson(logindata)
            val patientIntent = Intent(activity!!, ReceptionSalesPatientActivity::class.java)
            patientIntent.putExtra("logindata", myJson)
            startActivityForResult(patientIntent, 636)
        }
        mView!!.tvpatientname.setOnClickListener {
            val gson = Gson()
            val myJson = gson.toJson(logindata)
            val patientIntent = Intent(activity!!, ReceptionSalesPatientActivity::class.java)
            patientIntent.putExtra("logindata", myJson)
            startActivityForResult(patientIntent, 636)
        }
        mView!!.tvpatientlicence.setOnClickListener {
            val gson = Gson()
            val myJson = gson.toJson(logindata)

            val patientIntent = Intent(activity!!, ReceptionSalesPatientActivity::class.java)
            patientIntent.putExtra("logindata", myJson)
            startActivityForResult(patientIntent, 636)
        }
        mView!!.ll_cancel.setOnClickListener {
            mView!!.tv_selectpatient.text = ""
            mView!!.tvpatientname.text = ""
            mView!!.tvpatientlicence.text = ""
            mView!!.tvpatientscan.text = ""
            mView!!.tvpatientexpdate.text = ""
            mView!!.edtpatientmmmp.setText("")
            mView!!.ll_mmmp.visibility = View.GONE
           // mView!!.llexpdate.visibility = View.GONE
            patient_id = ""
            caregiver_id = "0"
        }
        appSettingData = Gson().fromJson(Preferences.getPreference(context!!, "AppSetting"), GetEmployeeAppSetting::class.java)
        callCheckStoreOpen()
        mView!!.cv_add_patient.setOnClickListener {

            if (storeopen.equals("0")){
                CUC.displayToast(activity!!, "0", "Please Open Store")
            }else{
                if(tillopen.equals("0")){
                    CUC.displayToast(activity!!, "0", "Please Open Till")
                }else{
                    if (::mPatient.isInitialized) {
                        if (patient_id != "") {
                            if ("${mPatient.patient_caregiver}" == "1") {
                                /*if ("${mView!!.edtpatientmmmp.text}" == "") {
                                    CUC.displayToast(activity!!, "0", "Enter Patient MMMP Licence Number")
                                } else {*/
                                    if ("${mView!!.tvpatientlicence.text}" == "") {
                                        CUC.displayToast(activity!!, "0", "Enter Patient Licence Number")
                                    }
                                    else {
                                       /* var ISMMMP = false
                                        for (data in caregiver) {
                                            if ("${mView!!.edtpatientmmmp.text}" == "${data.patient_mmpp_licence_no}") {
                                                careData = data
                                                ISMMMP = true
                                                break
                                            }
                                        }
                                        if (ISMMMP) {*/
                                            patient_id = "${mPatient.patient_id}"
                                            caregiver_id = "${mPatient.patient_caregiver_id}"
                                            if ("${mPatient.haveCompany}" == "0") {
                                                val builder = AlertDialog.Builder(activity!!)
                                                builder.setMessage("$assign_msg")
                                                builder.setPositiveButton("Yes") { dialog, which ->
                                                    dialog.dismiss()
                                                    patient_id = "${mPatient.patient_id}"
                                                    caregiver_id = "${mPatient.patient_caregiver_id}"
                                                    callEmployeePatientAssignCompany()
                                                }
                                                builder.setNegativeButton("No") { dialog, which -> dialog.dismiss() }
                                                val alert = builder.create()
                                                alert.show()
                                            } else {
                                                callEmployeeSalesCreateQueue()
                                            }
                                        /*} else {
                                            patient_id = "${mPatient.patient_id}"
                                            caregiver_id = "0"
                                            CUC.displayToast(activity!!, "0", "Not Found Patient MMMP Licence Number")
                                        }*/
                                    }
                              // }
                            } else {
                                callEmployeeSalesCreateQueue()
                            }
                        } else {
                            CUC.displayToast(activity!!, "0", "Please Select Patient")
                        }
                    } else {
                        CUC.displayToast(activity!!, "0", "Please Select Patient")
                    }
                }
            }

        }
        mView!!.edtpatientmmmp.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }
        })


        mView!!.tvpatientscan.setOnClickListener {

            if (!storeopen.equals("0")){
                if (!tillopen.equals("0")){
                    val goIntent = Intent(activity, BarCodeScan::class.java)
                    // goIntent.putExtra("status", "1")
                    // goIntent.putExtra("printername","Recep")
                    startActivityForResult(goIntent, 444)
                }else{
                    CUC.displayToast(activity!!, "0","Open Till First")
                }
            }else{
                CUC.displayToast(activity!!, "0", "Open Store First")
            }



        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        println("aaaaaaaaaaa   datatostring   "+data.toString())
        when (requestCode) {
            636 -> if (resultCode == Activity.RESULT_OK) {
                if (data != null && data.hasExtra("data")) {
                    try {
                        assign_msg = data.getStringExtra("assign_msg")
                        mPatient = Gson().fromJson("${data.getStringExtra(intentGet)}", GetPatient::class.java)
                        println("aaaaaaaaaaaaa mPatient    "+mPatient.toString())
                        mView!!.tv_selectpatient.text = "${mPatient.Patientname}"
                        mView!!.tvpatientname.text = "${mPatient.Patientname}"
                        mView!!.tvpatientlicence.text = "${mPatient.patient_licence_no}"
                        mView!!.tvpatientscan.text = "${mPatient.patient_mmpp_licence_no}"

                        if ("${mPatient.patient_licence_exp_date}" != "") {
                            //08252018
                            mView!!.tvpatientexpdate.text = "${CUC.getdatefromoneformattoAnother("yyyy-MM-dd", "MM-dd-yyyy", "${mPatient.patient_licence_exp_date}")}"
                        }

                        //mView!!.tvpatientexpdate.text = "${mPatient.patient_licence_exp_date}"
                        mView!!.llexpdate.visibility = View.VISIBLE

                        patient_id = "${mPatient.patient_id}"

                        if ("${mPatient.patient_caregiver}" == "1") {
                            mView!!.ll_mmmp.visibility = View.GONE
                            callEmployeeSalesPatientCaregiverlist()
                        } else {
                            mView!!.ll_mmmp.visibility = View.GONE
                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            }
            444 -> if (resultCode == Activity.RESULT_OK) {

                if (data != null) {
                    println("aaaaaaaaaaaa   data  "+data.getStringExtra("driving_license"))
                    var patient_dob = data.getStringExtra("patient_dob")
                    var driving_license = data.getStringExtra("driving_license")
                    var Name = data.getStringExtra("Name")
                    var lName = data.getStringExtra("lName")
                    var Postal = data.getStringExtra("Postal")
                    var state_code = data.getStringExtra("state_code")
                    var City = data.getStringExtra("City")
                    var Timestamp = data.getStringExtra("Timestamp")

                    mPatient= GetPatient("",Name,"",driving_license,Timestamp,"","","","","","")

                    println("aaaaaaaaa  patient_dob "+patient_dob +" Name  "+Name+" lName  "+lName+" lName "+lName+" state_code  "+state_code+"  city  "+City)

                    mView!!.tv_selectpatient.text = "${Name + lName}"
                    mView!!.tvpatientname.text = "${Name + lName}"
                    mView!!.tvpatientlicence.text = "${driving_license}"

                    mView!!.tvpatientexpdate.text = "${Timestamp}"

                    callAppqVerifyDlicence(driving_license);


                    mView!!.llexpdate.visibility = View.VISIBLE


                    /*if ("${mPatient.patient_caregiver}" == "1") {
                        mView!!.ll_mmmp.visibility = View.VISIBLE
                        callEmployeeSalesPatientCaregiverlist()
                    } else {
                        mView!!.ll_mmmp.visibility = View.GONE
                    }*/
                   /* var mobileappAdapter = ArrayAdapter<String>(mView!!.context, R.layout.spinner_item, R.id.dropdown_item, data)
                    mView!!.bs_status.setText("${resources.getStringArray(R.array.queue_status)[0]}")
                    mView!!.bs_status.setAdapter(mobileappAdapter)
                    selectDate = "${Calendar.getInstance().get(Calendar.YEAR)}-" +
                            "${(if (Calendar.getInstance().get(Calendar.MONTH) + 1 >= 10) Calendar.getInstance().get(Calendar.MONTH) + 1 else "0" + (Calendar.getInstance().get(Calendar.MONTH) + 1))}-" +
                            "${(if (Calendar.getInstance().get(Calendar.DAY_OF_MONTH) >= 10) Calendar.getInstance().get(Calendar.DAY_OF_MONTH) else "0${Calendar.getInstance().get(Calendar.DAY_OF_MONTH)}")}"
                    //bs_status.setText(queueStatus!![0])
                    callEmployeeQueueList()*/
                }

            }
        }
    }

    fun callEmployeeSalesPatientCaregiverlist() {
        if (CUC.isNetworkAvailablewithPopup(activity!!)) {
            mDialog = CUC.createDialog(activity!!)
            val apiService = RequetsInterface.Factory.create()
            val parameterMap = HashMap<String, String>()
            parameterMap["api_key"] = "${Preferences.getPreference(activity!!, "API_KEY")}"
            parameterMap["patient_id"] = "$patient_id"
            println("aaaaaaaaa  caregiver  "+parameterMap)
            Log.i("Result", "parameterMap : $parameterMap")
            CompositeDisposable().add(apiService.callEmployeeSalesPatientCaregiverlist(parameterMap)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe({ result ->
                        if (mDialog != null && mDialog.isShowing)
                            mDialog.dismiss()
                        println("aaaaaaaaa caregiver result  "+result.toString())
                        Log.i("Result", "" + result.toString())
                        if (result != null) {
                            if (result.status == "0") {
                                caregiver = result.caregiver
                                CUC.displayToast(activity!!, result.show_status, result.message)
                            } else if (result.status == "10") {
                                apiClass!!.callApiKey(EMPLOYEESALESPATIENTCAREGIVERLIST)
                            } else {
                                CUC.displayToast(activity!!, result.show_status, result.message)
                            }
                        } else {
                          //  CUC.displayToast(activity!!, "0", getString(R.string.connection_to_database_failed))
                        }
                    }, { error ->
                        if (mDialog != null && mDialog.isShowing)
                            mDialog.dismiss()
                        CUC.displayToast(activity!!, "0", getString(R.string.connection_to_database_failed))
                        error.printStackTrace()
                    })
            )
        }
    }

    fun callEmployeeSalesCreateQueue() {
        if (CUC.isNetworkAvailablewithPopup(activity!!)) {
            mDialog = CUC.createDialog(activity!!)
            val apiService = RequetsInterface.Factory.create()
            val parameterMap = HashMap<String, String>()
            parameterMap["api_key"] = "${Preferences.getPreference(activity!!, "API_KEY")}"
            parameterMap["patient_id"] = "$patient_id"
            parameterMap["caregiver_id"] = "$caregiver_id"
            if (logindata != null) {
                parameterMap["employee_id"] = "${logindata.user_id}"
            }
            Log.i("aaaa patient Result", "parameterMap : $parameterMap")
            CompositeDisposable().add(apiService.callEmployeeSalesCreateQueue(parameterMap)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe({ result ->
                        if (mDialog != null && mDialog.isShowing)
                            mDialog.dismiss()
                        println("aaaaaaaaaa  patient api Result  "+result.toString())
                        Log.i("api Result", "" + result.toString())
                        if (result != null) {
                            if (result.status == "0") {
                                CUC.displayToast(activity!!, result.show_status, result.message)
                                val QueueData = Gson().toJson(result.queue)
                                val gson = Gson()
                                val myJson = gson.toJson(logindata)
                                //val goIntent = Intent(activity!!, SalesCategoryActivity::class.java)
                                val goIntent = Intent(activity!!, SalesActivity::class.java)
                                goIntent.putExtra("queue_patient_id", "$patient_id")
                                goIntent.putExtra("queue_id", "${result.queue.queue_id}")
                                goIntent.putExtra("QueueData", "$QueueData")
                                goIntent.putExtra("logindata",myJson)
                                activity!!.startActivityForResult(goIntent, 664)
                                mView!!.tv_selectpatient.text = ""
                                mView!!.tvpatientname.text = ""
                                mView!!.tvpatientlicence.text = ""
                                mView!!.tvpatientscan.text = ""
                                mView!!.tvpatientexpdate.text = ""
                                mView!!.edtpatientmmmp.setText("")
                                mView!!.ll_mmmp.visibility = View.GONE
                              //  mView!!.llexpdate.visibility = View.GONE
                                patient_id = ""
                                caregiver_id = "0"
                            } else if (result.status == "10") {
                                apiClass!!.callApiKey(EMPLOYEESALESCREATEQUEUE)
                            } else {
                                CUC.displayToast(activity!!, result.show_status, result.message)
                            }
                        } else {
                           // CUC.displayToast(activity!!, "0", getString(R.string.connection_to_database_failed))
                        }
                    }, { error ->
                        if (mDialog != null && mDialog.isShowing)
                            mDialog.dismiss()
                        CUC.displayToast(activity!!, "0", getString(R.string.connection_to_database_failed))
                        error.printStackTrace()
                    })
            )
        }
    }

    fun callEmployeePatientAssignCompany() {
        if (CUC.isNetworkAvailablewithPopup(activity!!)) {
            mDialog = CUC.createDialog(activity!!)
            val apiService = RequetsInterface.Factory.create()
            val parameterMap = HashMap<String, String>()
            parameterMap["api_key"] = "${Preferences.getPreference(activity!!, "API_KEY")}"
            parameterMap["patient_licence_no"] = "${mPatient.patient_licence_no}"
            parameterMap["patient_id"] = "${mPatient.patient_id}"
            if (logindata != null) {
                parameterMap["store_id"] = "${logindata.user_store_id}"
            }
            Log.i("Result", "parameterMap : $parameterMap")
            CompositeDisposable().add(apiService.callEmployeePatientAssignCompany(parameterMap)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe({ result ->
                        if (mDialog != null && mDialog.isShowing)
                            mDialog.dismiss()
                        Log.i("Result", "" + result.toString())
                        if (result != null) {
                            if (result.status == "0") {
                                CUC.displayToast(activity!!, result.show_status, result.message)
                                callEmployeeSalesCreateQueue()
                            } else if (result.status == "10") {
                                apiClass!!.callApiKey(EMPLOYEEPATIENTASSIGNCOMPANY)
                            } else {
                                CUC.displayToast(activity!!, result.show_status, result.message)
                            }
                        } else {
                          //  CUC.displayToast(activity!!, "0", getString(R.string.connection_to_database_failed))
                        }
                    }, { error ->
                        if (mDialog != null && mDialog.isShowing)
                            mDialog.dismiss()
                        CUC.displayToast(activity!!, "0", getString(R.string.connection_to_database_failed))
                        error.printStackTrace()
                    })
            )
        }
    }


    fun callAppqVerifyDlicence(drivinglicenceno : String) {
        mDialog = CUC.createDialog(activity!!)
        val apiService = RequetsInterface.create()
        val parameterMap = HashMap<String, String>()
        parameterMap["api_key"] = "${Preferences.getPreference(activity!!, "API_KEY")}"
        parameterMap["licence_no"] = "$drivinglicenceno"
        parameterMap["login_user"] = "${logindata.user_id}"
        parameterMap["store_id"] = "${logindata.user_store_id}"
        parameterMap["company_id"] = "0"

        Log.i("aaaaaa  Result", "parameterMap : $parameterMap")
        CompositeDisposable().add(apiService.callGetPatientid(parameterMap)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe({ result ->
                    if (mDialog != null && mDialog.isShowing)
                        mDialog.dismiss()
                    if (result != null) {
                        Log.i("aaaa Result", "" + result.toString())
                        if (result.status == "1") {


                            patient_id= result.notes_data?.pnote_patient.toString()

                            callEmployeeSalesCreateQueue()

                        } else if (result.status == "10") {

                        } else {
                            if (result.image != null && result.image != "") {
//                                Glide.with(this@MessageActivity)
//                                        .applyDefaultRequestOptions(RequestOptions().format(DecodeFormat.PREFER_ARGB_8888).error(R.drawable.no_image_available))
//                                        .load("${result.image}")
//                                        .into(img_msg)
                            }

                        }
                    } else {
                        //  CUC.displayToast(this@MessageActivity, "0", getString(R.string.connection_to_database_failed))
                    }
                }, { error ->
                    if (mDialog != null && mDialog.isShowing)
                        mDialog.dismiss()
                    CUC.displayToast(activity!!, "0", getString(R.string.connection_to_database_failed))

                    error.printStackTrace()
                })
        )
    }

    fun callCheckStoreOpen() {
        val terminalid = appSettingData?.terminal_id
        if (CUC.isNetworkAvailablewithPopup(activity!!)) {
            mDialog = CUC.createDialog(activity!!)
            val apiService = RequetsInterface.Factory.create()
            val parameterMap = HashMap<String, String>()
            parameterMap["api_key"] = "${Preferences.getPreference(activity!!, "API_KEY")}"
            parameterMap["terminal_id"] = "${terminalid}"
            if (logindata != null) {
                parameterMap["emp_id"] = "${logindata.user_id}"
                parameterMap["store_id"] = "${logindata.user_store_id}"
            }
            println("aaaaaaaaaaa  storecheck  "+parameterMap)
            Log.i("aaaa patient Result", "parameterMap : $parameterMap")
            CompositeDisposable().add(apiService.callCheckStoreOpen(parameterMap)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe({ result ->
                        if (mDialog != null && mDialog.isShowing)
                            mDialog.dismiss()
                        println("aaaaaaaaaa  storecheck  Result  "+result.toString())
                        Log.i("api Result", "" + result.toString())
                        if (result != null) {
                            if (result.store_chk == "0") {
                               // CUC.displayToast(activity!!, result.show_status, result.message)
                                showdialog(result.message)
                            } else {
                                storeopen="1"
                                calltillCheck()
                                //CUC.displayToast(activity!!, result.show_status, result.message)
                            }
                        } else {
                            // CUC.displayToast(activity!!, "0", getString(R.string.connection_to_database_failed))
                        }
                    }, { error ->
                        if (mDialog != null && mDialog.isShowing)
                            mDialog.dismiss()
                        CUC.displayToast(activity!!, "0", getString(R.string.connection_to_database_failed))
                        error.printStackTrace()
                    })
            )
        }
    }

    fun calltillCheck() {
        val terminalid = appSettingData?.terminal_id
        if (CUC.isNetworkAvailablewithPopup(activity!!)) {
            mDialog = CUC.createDialog(activity!!)
            val apiService = RequetsInterface.Factory.create()
            val parameterMap = HashMap<String, String>()
            parameterMap["api_key"] = "${Preferences.getPreference(activity!!, "API_KEY")}"
            parameterMap["terminal_id"] = "${terminalid}"
            if (logindata != null) {
                parameterMap["emp_id"] = "${logindata.user_id}"
                parameterMap["store_id"] = "${logindata.user_store_id}"
            }
            println("aaaaaaaaaaa  tillcheck  "+parameterMap)
            Log.i("aaaa patient Result", "parameterMap : $parameterMap")
            CompositeDisposable().add(apiService.callTillOpenCheck(parameterMap)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe({ result ->
                        if (mDialog != null && mDialog.isShowing)
                            mDialog.dismiss()
                        println("aaaaaaaaaa  tillcheck  Result  "+result.toString())
                        Log.i("api Result", "" + result.toString())
                        if (result != null) {
                            if (result.till_check == "0") {
                                //CUC.displayToast(activity!!, result.show_status, result.message)
                                showdialog(result.message)
                            } else {
                                tillopen="1"
                               // CUC.displayToast(activity!!, result.show_status, result.message)
                            }
                        } else {
                            // CUC.displayToast(activity!!, "0", getString(R.string.connection_to_database_failed))
                        }
                    }, { error ->
                        if (mDialog != null && mDialog.isShowing)
                            mDialog.dismiss()
                        CUC.displayToast(activity!!, "0", getString(R.string.connection_to_database_failed))
                        error.printStackTrace()
                    })
            )
        }
    }

    fun showdialog(msg:String) {
        val builder1 = android.app.AlertDialog.Builder(context)
        builder1.setMessage("" + msg)
        builder1.setCancelable(true)

        builder1.setPositiveButton(
                "Ok"
        ) { dialog, id ->
            dialog.cancel()

        }

        val alert11 = builder1.create()
        alert11.show()
    }
}