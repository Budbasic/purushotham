package com.budbasic.posconsole.reception.fragment

import android.app.Activity
import android.app.DatePickerDialog
import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AlertDialog
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.budbasic.customer.global.Preferences
import com.budbasic.global.CUC
import com.budbasic.posconsole.R
import com.budbasic.posconsole.model.APIClass
import com.budbasic.posconsole.reception.Deposit_Add_Button
import com.budbasic.posconsole.reception.Expenses_Add_Button
import com.budbasic.posconsole.reception.ReceptionSalesPatient
import com.budbasic.posconsole.sales.SalesCategoryActivity
import com.budbasic.posconsole.webservice.RequetsInterface
import com.google.gson.Gson
import com.kotlindemo.model.GetEmployeeLogin
import com.kotlindemo.model.GetPatient
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.deposit_view.view.*
import kotlinx.android.synthetic.main.queue_fragment.view.*
import kotlinx.android.synthetic.main.queue_fragment.view.tv_start_date
import kotlinx.android.synthetic.main.quick_sale.view.*
import kotlinx.android.synthetic.main.quick_sale.view.btn_back
import java.util.*
import kotlin.collections.ArrayList

class DepositFinance : Fragment(), APIClass.onCallListner {
    override fun onFailed(requestCode: Int) {
    }

    override fun onSuccess(requestCode: Int) {
        if (requestCode == EMPLOYEEPATIENTASSIGNCOMPANY) {
            //  callEmployeePatientAssignCompany()
        } else if (requestCode == EMPLOYEESALESCREATEQUEUE) {
            //  callEmployeeSalesCreateQueue()
        } else if (requestCode == EMPLOYEESALESPATIENTCAREGIVERLIST) {
            //  callEmployeeSalesPatientCaregiverlist()
        }
    }

    val EMPLOYEEPATIENTASSIGNCOMPANY = 1
    val EMPLOYEESALESCREATEQUEUE = 2
    val EMPLOYEESALESPATIENTCAREGIVERLIST = 3

    var mView: View? = null

    val intentGet = "data"

    lateinit var mDialog: Dialog
    var patient_id = ""
    var caregiver_id = "0"

    lateinit var logindata: GetEmployeeLogin
    lateinit var mPatient: GetPatient
    var assign_msg = ""
    var caregiver = ArrayList<GetPatient>()
    var selectDate = ""

    lateinit var careData: GetPatient
    var apiClass: APIClass? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        if (view != null)
            mView = view!!
        else
            mView = inflater.inflate(R.layout.deposit_view, container, false)
        apiClass = APIClass(context!!, this)
        logindata = Gson().fromJson(Preferences.getPreference(activity!!, "logindata"), GetEmployeeLogin::class.java)
         initView()
        return mView
    }

    fun initView() {
          mView!!.btn_back.setOnClickListener {
              activity!!.onBackPressed()
          }


        mView!!.cv_create_deposit.setOnClickListener {



            val intent = Intent(context!!, Deposit_Add_Button::class.java)
            startActivityForResult(intent, 122)
        }





        var mYear = Calendar.getInstance().get(Calendar.YEAR)
        var mMonth = Calendar.getInstance().get(Calendar.MONTH)
        var mDay = Calendar.getInstance().get(Calendar.DAY_OF_MONTH)

        selectDate = "${mYear}-" +
                "${(if (mMonth + 1 >= 10) mMonth + 1 else "0" + (mMonth + 1))}" +
                "-${(if (mDay >= 10) mDay else "0$mDay")}"

        mView!!.tv_start_date.text = "${(if (mMonth + 1 >= 10) mMonth + 1 else "0" + (mMonth + 1))}" +
                "-${(if (mDay >= 10) mDay else "0$mDay")}" +
                "-${mYear}"




        mView!!.tv_start_date.setOnClickListener {
            val datePickerDialog = DatePickerDialog(mView!!.context, DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                mYear = year
                mMonth = monthOfYear
                mDay = dayOfMonth

                selectDate = "$year-${(if (monthOfYear + 1 >= 10) monthOfYear + 1 else "0" + (monthOfYear + 1))}-${(if (dayOfMonth >= 10) dayOfMonth else "0$dayOfMonth")}"
                mView!!.tv_start_date.text = "${(if (monthOfYear + 1 >= 10) monthOfYear + 1 else "0" + (monthOfYear + 1))}-${(if (dayOfMonth >= 10) dayOfMonth else "0$dayOfMonth")}-$year"
                //callEmployeeQueueList()

            }, mYear, mMonth, mDay)
            datePickerDialog.datePicker.maxDate = System.currentTimeMillis()
            datePickerDialog.show()
            // callEmployeeQueueList()
        }








       /*   mView!!.tv_selectpatient.setOnClickListener {
            val patientIntent = Intent(activity!!, ReceptionSalesPatient::class.java)
            startActivityForResult(patientIntent, 636)
        }
        mView!!.tvpatientname.setOnClickListener {
            val patientIntent = Intent(activity!!, ReceptionSalesPatient::class.java)
            startActivityForResult(patientIntent, 636)
        }
        mView!!.tvpatientlicence.setOnClickListener {
            val patientIntent = Intent(activity!!, ReceptionSalesPatient::class.java)
            startActivityForResult(patientIntent, 636)
        }
        mView!!.ll_cancel.setOnClickListener {
            mView!!.tv_selectpatient.text = ""
            mView!!.tvpatientname.text = ""
            mView!!.tvpatientlicence.text = ""
            mView!!.tvpatientexpdate.text = ""
            mView!!.edtpatientmmmp.setText("")
            mView!!.ll_mmmp.visibility = View.GONE
            // mView!!.llexpdate.visibility = View.GONE
            patient_id = ""
            caregiver_id = "0"
        }
        mView!!.cv_add_patient.setOnClickListener {
            if (::mPatient.isInitialized) {
                if (patient_id != "") {
                    if ("${mPatient.patient_caregiver}" == "1") {
                        if ("${mView!!.edtpatientmmmp.text}" == "") {
                            CUC.displayToast(activity!!, "0", "Enter Patient MMMP Licence Number")
                        } else {
                            var ISMMMP = false
                            for (data in caregiver) {
                                if ("${mView!!.edtpatientmmmp.text}" == "${data.patient_mmpp_licence_no}") {
                                    careData = data
                                    ISMMMP = true
                                    break
                                }
                            }
                            if (ISMMMP) {
                                patient_id = "${careData.patient_id}"
                                caregiver_id = "${careData.patient_caregiver_id}"
                                if ("${mPatient.haveCompany}" == "0") {
                                    val builder = AlertDialog.Builder(activity!!)
                                    builder.setMessage("$assign_msg")
                                    builder.setPositiveButton("Yes") { dialog, which ->
                                        dialog.dismiss()
                                        patient_id = "${careData.patient_id}"
                                        caregiver_id = "${careData.patient_caregiver_id}"
                                        callEmployeePatientAssignCompany()
                                    }
                                    builder.setNegativeButton("No") { dialog, which -> dialog.dismiss() }
                                    val alert = builder.create()
                                    alert.show()
                                } else {
                                    callEmployeeSalesCreateQueue()
                                }
                            } else {
                                patient_id = "${mPatient.patient_id}"
                                caregiver_id = "0"
                                CUC.displayToast(activity!!, "0", "Not Found Patient MMMP Licence Number")
                            }
                        }
                    } else {
                        callEmployeeSalesCreateQueue()
                    }
                } else {
                    CUC.displayToast(activity!!, "0", "Please Select Patient")
                }
            } else {
                CUC.displayToast(activity!!, "0", "Please Select Patient")
            }
        }
        mView!!.edtpatientmmmp.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

        })*/
    }

 /*   override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            636 -> if (resultCode == Activity.RESULT_OK) {
                if (data != null && data.hasExtra("data")) {
                    try {
                        assign_msg = data.getStringExtra("assign_msg")
                        mPatient = Gson().fromJson("${data.getStringExtra(intentGet)}", GetPatient::class.java)
                        mView!!.tv_selectpatient.text = "${mPatient.Patientname}"
                        mView!!.tvpatientname.text = "${mPatient.Patientname}"
                        mView!!.tvpatientlicence.text = "${mPatient.patient_licence_no}"

                        if ("${mPatient.patient_licence_exp_date}" != "") {
                            //08252018
                            mView!!.tvpatientexpdate.text = "${CUC.getdatefromoneformattoAnother("yyyy-MM-dd", "MM-dd-yyyy", "${mPatient.patient_licence_exp_date}")}"
                        }

                        //mView!!.tvpatientexpdate.text = "${mPatient.patient_licence_exp_date}"
                        mView!!.llexpdate.visibility = View.VISIBLE
                        patient_id = "${mPatient.patient_id}"
                        if ("${mPatient.patient_caregiver}" == "1") {
                            mView!!.ll_mmmp.visibility = View.VISIBLE
                            callEmployeeSalesPatientCaregiverlist()
                        } else {
                            mView!!.ll_mmmp.visibility = View.GONE
                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            }
        }
    }

    fun callEmployeeSalesPatientCaregiverlist() {
        if (CUC.isNetworkAvailablewithPopup(activity!!)) {
            mDialog = CUC.createDialog(activity!!)
            val apiService = RequetsInterface.Factory.create()
            val parameterMap = HashMap<String, String>()
            parameterMap["api_key"] = "${Preferences.getPreference(activity!!, "API_KEY")}"
            parameterMap["patient_id"] = "$patient_id"
            Log.i("Result", "parameterMap : $parameterMap")
            CompositeDisposable().add(apiService.callEmployeeSalesPatientCaregiverlist(parameterMap)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe({ result ->
                        if (mDialog != null && mDialog.isShowing)
                            mDialog.dismiss()
                        Log.i("Result", "" + result.toString())
                        if (result != null) {
                            if (result.status == "0") {
                                caregiver = result.caregiver
                                CUC.displayToast(activity!!, result.show_status, result.message)
                            } else if (result.status == "10") {
                                apiClass!!.callApiKey(EMPLOYEESALESPATIENTCAREGIVERLIST)
                            } else {
                                CUC.displayToast(activity!!, result.show_status, result.message)
                            }
                        } else {
                            //  CUC.displayToast(activity!!, "0", getString(R.string.connection_to_database_failed))
                        }
                    }, { error ->
                        if (mDialog != null && mDialog.isShowing)
                            mDialog.dismiss()
                        CUC.displayToast(activity!!, "0", getString(R.string.connection_to_database_failed))
                        error.printStackTrace()
                    })
            )
        }
    }

    fun callEmployeeSalesCreateQueue() {
        if (CUC.isNetworkAvailablewithPopup(activity!!)) {
            mDialog = CUC.createDialog(activity!!)
            val apiService = RequetsInterface.Factory.create()
            val parameterMap = HashMap<String, String>()
            parameterMap["api_key"] = "${Preferences.getPreference(activity!!, "API_KEY")}"
            parameterMap["patient_id"] = "$patient_id"
            parameterMap["caregiver_id"] = "$caregiver_id"
            if (logindata != null) {
                parameterMap["employee_id"] = "${logindata.user_id}"
            }
            Log.i("Result", "parameterMap : $parameterMap")
            CompositeDisposable().add(apiService.callEmployeeSalesCreateQueue(parameterMap)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe({ result ->
                        if (mDialog != null && mDialog.isShowing)
                            mDialog.dismiss()
                        Log.i("Result", "" + result.toString())
                        if (result != null) {
                            if (result.status == "0") {
                                CUC.displayToast(activity!!, result.show_status, result.message)
                                val QueueData = Gson().toJson(result.queue)
                                val goIntent = Intent(activity!!, SalesCategoryActivity::class.java)
                                goIntent.putExtra("queue_patient_id", "$patient_id")
                                goIntent.putExtra("queue_id", "${result.queue.queue_id}")
                                goIntent.putExtra("QueueData", "$QueueData")
                                activity!!.startActivityForResult(goIntent, 664)
                                mView!!.tv_selectpatient.text = ""
                                mView!!.tvpatientname.text = ""
                                mView!!.tvpatientlicence.text = ""
                                mView!!.tvpatientexpdate.text = ""
                                mView!!.edtpatientmmmp.setText("")
                                mView!!.ll_mmmp.visibility = View.GONE
                                //  mView!!.llexpdate.visibility = View.GONE
                                patient_id = ""
                                caregiver_id = "0"
                            } else if (result.status == "10") {
                                apiClass!!.callApiKey(EMPLOYEESALESCREATEQUEUE)
                            } else {
                                CUC.displayToast(activity!!, result.show_status, result.message)
                            }
                        } else {
                            // CUC.displayToast(activity!!, "0", getString(R.string.connection_to_database_failed))
                        }
                    }, { error ->
                        if (mDialog != null && mDialog.isShowing)
                            mDialog.dismiss()
                        CUC.displayToast(activity!!, "0", getString(R.string.connection_to_database_failed))
                        error.printStackTrace()
                    })
            )
        }
    }

    fun callEmployeePatientAssignCompany() {
        if (CUC.isNetworkAvailablewithPopup(activity!!)) {
            mDialog = CUC.createDialog(activity!!)
            val apiService = RequetsInterface.Factory.create()
            val parameterMap = HashMap<String, String>()
            parameterMap["api_key"] = "${Preferences.getPreference(activity!!, "API_KEY")}"
            parameterMap["patient_licence_no"] = "${mPatient.patient_licence_no}"
            parameterMap["patient_id"] = "${mPatient.patient_id}"
            if (logindata != null) {
                parameterMap["store_id"] = "${logindata.user_store_id}"
            }
            Log.i("Result", "parameterMap : $parameterMap")
            CompositeDisposable().add(apiService.callEmployeePatientAssignCompany(parameterMap)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe({ result ->
                        if (mDialog != null && mDialog.isShowing)
                            mDialog.dismiss()
                        Log.i("Result", "" + result.toString())
                        if (result != null) {
                            if (result.status == "0") {
                                CUC.displayToast(activity!!, result.show_status, result.message)
                                callEmployeeSalesCreateQueue()
                            } else if (result.status == "10") {
                                apiClass!!.callApiKey(EMPLOYEEPATIENTASSIGNCOMPANY)
                            } else {
                                CUC.displayToast(activity!!, result.show_status, result.message)
                            }
                        } else {
                            //  CUC.displayToast(activity!!, "0", getString(R.string.connection_to_database_failed))
                        }
                    }, { error ->
                        if (mDialog != null && mDialog.isShowing)
                            mDialog.dismiss()
                        CUC.displayToast(activity!!, "0", getString(R.string.connection_to_database_failed))
                        error.printStackTrace()
                    })
            )
        }
    }*/
}