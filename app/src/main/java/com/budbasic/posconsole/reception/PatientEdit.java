package com.budbasic.posconsole.reception;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.budbasic.customer.global.Preferences;
import com.budbasic.global.CUC;
import com.budbasic.posconsole.R;
import com.budbasic.posconsole.adapter.DocumentPatientAdapter;
import com.budbasic.posconsole.adapter.NotesDialogAdaper;
import com.budbasic.posconsole.adapter.PatientCgAdapter;
import com.budbasic.posconsole.queue.BarCodeScan;
import com.budbasic.posconsole.webservice.RequetsInterface;
import com.google.gson.Gson;
import com.kotlindemo.model.DocumentModel;
import com.kotlindemo.model.GetDocumentNotes;
import com.kotlindemo.model.GetEmployeeAppSetting;
import com.kotlindemo.model.GetEmployeeLogin;
import com.kotlindemo.model.GetNotes;
import com.kotlindemo.model.GetNotesDatashow;
import com.kotlindemo.model.GetStatus;
import com.kotlindemo.model.Patient_data;
import com.kotlindemo.model.getCaregiver;
import com.kotlindemo.model.getcaregivers;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

import static android.Manifest.permission.CAMERA;

public class PatientEdit extends AppCompatActivity {

    private TextView tvPhone,tvDocuments,tvNotes,tvpatients,tv_patientName,tv_patientlicence,tv_mmpNumber,tv_alertcheck,tv_alertscreen,
            add_saveinfo,tv_nodata;
    private LinearLayout ll_phone,ll_document,ll_patient,ll_cancel;
    private RelativeLayout rl_Notes,layout_relative;
    private ImageView img_infoImage,iv_scan_id,img_doucument_data,img_full;
    private CardView cv_savephoto,cv_saveinfo,alertcheck,alertscreen,cv_submit,cv_delete,cv_upload_image,cv_submit11;
    private EditText tv_email,tv_phoneNumber,edt_note_name,edt_note_message,edt_document_name,edt_name_patient;
    private RecyclerView recycle_notes,recycle_documents,recycle_cgpatient;
    private ArrayList<GetNotes> noteslist;
    private ArrayList<getCaregiver> caregiverlist;
    private int REQUEST_CAMERA = 0, SELECT_FILE = 1;
    private Button btnSelect;
    private NotesDialogAdaper notesDialogAdaper;

    private String userChoosenTask;
    private Patient_data patient_data;
    GetEmployeeLogin logindata;
    private String patientImagePath;
    private Gson gson;
    public Dialog mDialog;
    private File imageCompanyFile;
    private int notespriority=0;
    private  String noteid,documentid;
    private ArrayList<DocumentModel> documentlist;
    private DocumentPatientAdapter documentPatientAdapter;
    private GetEmployeeAppSetting mBAsis;
    private int imgcheck=0;
    private String documentpath;
    String driving_license;
    private PatientCgAdapter patientCgAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_patient_edit);

        tvPhone=findViewById(R.id.tvPhone);
        tvDocuments=findViewById(R.id.tvDocuments);
        tvNotes=findViewById(R.id.tvNotes);
        ll_phone=findViewById(R.id.ll_phone);
        ll_patient=findViewById(R.id.ll_patient);
        rl_Notes=findViewById(R.id.rl_Notes);
        tvpatients=findViewById(R.id.tvpatients);
        ll_document=findViewById(R.id.ll_document);
        tv_patientName=findViewById(R.id.tv_patientName);
        tv_patientlicence=findViewById(R.id.tv_patientlicence);
        img_infoImage=findViewById(R.id.img_infoImage);
        cv_savephoto=findViewById(R.id.cv_savephoto);
        iv_scan_id=findViewById(R.id.iv_scan_id);
        tv_mmpNumber=findViewById(R.id.tv_mmpNumber);
        tv_phoneNumber=findViewById(R.id.tv_phoneNumber);
        tv_email=findViewById(R.id.tv_email);
        cv_saveinfo=findViewById(R.id.cv_saveinfo);
        recycle_notes=findViewById(R.id.recycle_notes);
        edt_note_name=findViewById(R.id.edt_note_name);
        alertcheck=findViewById(R.id.alertcheck);
        alertscreen=findViewById(R.id.alertscreen);
        cv_submit=findViewById(R.id.cv_submit);
        cv_delete=findViewById(R.id.cv_delete);
        edt_note_message=findViewById(R.id.edt_note_message);
        tv_alertcheck=findViewById(R.id.tv_alertcheck);
        tv_alertscreen=findViewById(R.id.tv_alertscreen);
        recycle_documents=findViewById(R.id.recycle_documents);
        edt_document_name=findViewById(R.id.edt_document_name);
        img_doucument_data=findViewById(R.id.img_doucument_data);
        cv_upload_image=findViewById(R.id.cv_upload_image);
        ll_cancel=findViewById(R.id.ll_cancel);
        img_full=findViewById(R.id.img_full);
        layout_relative=findViewById(R.id.layout_relative);
        add_saveinfo=findViewById(R.id.add_saveinfo);
        edt_name_patient=findViewById(R.id.edt_name_patient);
        cv_submit11=findViewById(R.id.cv_submit11);
        recycle_cgpatient=findViewById(R.id.recycle_cgpatient);
        tv_nodata=findViewById(R.id.tv_nodata);


        recycle_notes.setLayoutManager(new GridLayoutManager(PatientEdit.this,3));
        noteslist=new ArrayList<GetNotes>();

        recycle_documents.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        recycle_cgpatient.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));

        gson=new Gson();
        documentlist=new ArrayList<>();
        caregiverlist=new ArrayList<>();
        mBAsis = gson.fromJson(Preferences.INSTANCE.getPreference(PatientEdit.this, "AppSetting"), GetEmployeeAppSetting.class);

        logindata = gson.fromJson(getIntent().getStringExtra("logindata"), GetEmployeeLogin.class);
        patient_data = gson.fromJson(getIntent().getStringExtra("patientdata"), Patient_data.class);
        patientImagePath=getIntent().getStringExtra("patientImagePath");

        notesDialogAdaper=new NotesDialogAdaper(PatientEdit.this,noteslist);
        recycle_notes.setAdapter(notesDialogAdaper);
        patientCgAdapter=new PatientCgAdapter(PatientEdit.this,caregiverlist,mBAsis.getPatient_image_path());
        recycle_cgpatient.setAdapter(patientCgAdapter);
        documentPatientAdapter=new DocumentPatientAdapter(PatientEdit.this,documentlist,mBAsis,documentpath);
        recycle_documents.setAdapter(documentPatientAdapter);
        driving_license=patient_data.getPatient_licence_no();
        tv_patientName.setText(patient_data.getPatient_fname()+" "+patient_data.getPatient_lname());
        tv_patientlicence.setText("Drivers Licences : "+patient_data.getPatient_licence_no());
        tv_mmpNumber.setText("MMP: "+patient_data.getPatient_mmpp_licence_no());
        tv_phoneNumber.setText(patient_data.getPatient_mobile_no());
        tv_email.setText(patient_data.getPatient_email_id());

        tv_email.setImeOptions(EditorInfo.IME_FLAG_NO_EXTRACT_UI | EditorInfo.IME_ACTION_DONE);
        tv_phoneNumber.setImeOptions(EditorInfo.IME_FLAG_NO_EXTRACT_UI | EditorInfo.IME_ACTION_DONE);
        edt_note_name.setImeOptions(EditorInfo.IME_FLAG_NO_EXTRACT_UI | EditorInfo.IME_ACTION_DONE);
        edt_note_message.setImeOptions(EditorInfo.IME_FLAG_NO_EXTRACT_UI | EditorInfo.IME_ACTION_DONE);
        edt_document_name.setImeOptions(EditorInfo.IME_FLAG_NO_EXTRACT_UI | EditorInfo.IME_ACTION_DONE);
        edt_name_patient.setImeOptions(EditorInfo.IME_FLAG_NO_EXTRACT_UI | EditorInfo.IME_ACTION_DONE);

        System.out.println("aaaaaaaaaaa  patientid  "+patient_data.getPatient_id());
        System.out.println("aaaaaaaaaaaaa  image "+patientImagePath +"   "+patient_data.getPatient_image());
        try {
            Picasso.with(PatientEdit.this)
                    .load(patient_data.getPatient_profilepic())
                    .placeholder(R.drawable.no_image_available)
                    .error(R.drawable.no_image_available)
                    .into(img_infoImage);
        }catch (Exception e){
            System.out.println("aaaaaaaa  exception "+e.getMessage());
        }

        tvPhone.setBackgroundColor(getResources().getColor(R.color.blue));
        tvNotes.setBackgroundColor(getResources().getColor(R.color.graylight));
        tvDocuments.setBackgroundColor(getResources().getColor(R.color.graylight));
        tvpatients.setBackgroundColor(getResources().getColor(R.color.graylight));

        ll_patient.setVisibility(View.GONE);
        rl_Notes.setVisibility(View.GONE);
        ll_document.setVisibility(View.GONE);
        ll_phone.setVisibility(View.VISIBLE);
        img_full.setVisibility(View.GONE);
        cv_upload_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String title=edt_document_name.getText().toString().trim();
                if (title.isEmpty()){
                    Toast.makeText(PatientEdit.this, "Please Enter Document Title", Toast.LENGTH_SHORT).show();
                }else {
                    try{
                        addDocumetImg(title);
                    }catch (NullPointerException e){
                        Toast.makeText(PatientEdit.this, "Please Add Document ", Toast.LENGTH_SHORT).show();
                    }
                    /*if (imageCompanyFile.getName().isEmpty()){
                        Toast.makeText(PatientEdit.this, "Please Add Document ", Toast.LENGTH_SHORT).show();
                    }else {
                        addDocumetImg(title);
                    }*/

                }
            }
        });
        iv_scan_id.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent goIntent =new Intent(PatientEdit.this, BarCodeScan.class);
                startActivityForResult(goIntent, 444);
            }
        });
        img_full.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                layout_relative.setVisibility(View.VISIBLE);
                img_full.setVisibility(View.GONE);
            }
        });
        img_infoImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                imgcheck=0;
                selectImage();
            }
        }); img_doucument_data.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                imgcheck=1;
                selectImage();
            }
        });
        ll_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        cv_submit11.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (edt_name_patient.getText().toString().isEmpty()){
                    Toast.makeText(PatientEdit.this, "Please Enter MMMP Number ", Toast.LENGTH_SHORT).show();
                }else {
                    addcgpatient();
                }

            }
        });
        tvPhone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tvPhone.setBackgroundColor(getResources().getColor(R.color.blue));
                tvNotes.setBackgroundColor(getResources().getColor(R.color.graylight));
                tvDocuments.setBackgroundColor(getResources().getColor(R.color.graylight));
                tvpatients.setBackgroundColor(getResources().getColor(R.color.graylight));

                ll_patient.setVisibility(View.GONE);
                rl_Notes.setVisibility(View.GONE);
                ll_document.setVisibility(View.GONE);
                ll_phone.setVisibility(View.VISIBLE);

            }
        });
        tvNotes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tvPhone.setBackgroundColor(getResources().getColor(R.color.graylight));
                tvNotes.setBackgroundColor(getResources().getColor(R.color.blue));
                tvDocuments.setBackgroundColor(getResources().getColor(R.color.graylight));
                tvpatients.setBackgroundColor(getResources().getColor(R.color.graylight));

                ll_patient.setVisibility(View.GONE);
                rl_Notes.setVisibility(View.VISIBLE);
                ll_document.setVisibility(View.GONE);
                ll_phone.setVisibility(View.GONE);
                getNoteslist();

            }
        });
        tvDocuments.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tvPhone.setBackgroundColor(getResources().getColor(R.color.graylight));
                tvNotes.setBackgroundColor(getResources().getColor(R.color.graylight));
                tvDocuments.setBackgroundColor(getResources().getColor(R.color.blue));
                tvpatients.setBackgroundColor(getResources().getColor(R.color.graylight));

                ll_patient.setVisibility(View.GONE);
                rl_Notes.setVisibility(View.GONE);
                ll_document.setVisibility(View.VISIBLE);
                ll_phone.setVisibility(View.GONE);
                getDocuments();

            }
        });

        tvpatients.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tvPhone.setBackgroundColor(getResources().getColor(R.color.graylight));
                tvNotes.setBackgroundColor(getResources().getColor(R.color.graylight));
                tvDocuments.setBackgroundColor(getResources().getColor(R.color.graylight));
                tvpatients.setBackgroundColor(getResources().getColor(R.color.blue));

                ll_patient.setVisibility(View.VISIBLE);
                rl_Notes.setVisibility(View.GONE);
                ll_document.setVisibility(View.GONE);
                ll_phone.setVisibility(View.GONE);
                getcgpatients();
            }
        });
        cv_savephoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try{
                    addInfoImg();
                }catch (NullPointerException e){
                    Toast.makeText(PatientEdit.this, "Please Add Image ", Toast.LENGTH_SHORT).show();
                }

            }
        });
        alertcheck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                notespriority=0;
                tv_alertcheck.setBackgroundColor(getResources().getColor(R.color.blue));
                tv_alertscreen.setBackgroundColor(getResources().getColor(R.color.white));
                tv_alertcheck.setTextColor(getResources().getColor(R.color.white));
                tv_alertscreen.setTextColor(getResources().getColor(R.color.black));

            }
        });
        alertscreen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                notespriority=1;
                tv_alertcheck.setBackgroundColor(getResources().getColor(R.color.white));
                tv_alertscreen.setBackgroundColor(getResources().getColor(R.color.blue));
                tv_alertcheck.setTextColor(getResources().getColor(R.color.black));
                tv_alertscreen.setTextColor(getResources().getColor(R.color.white));

            }
        });
        cv_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AddNote();
            }
        });
        cv_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (noteid.isEmpty() || noteid.equalsIgnoreCase("")){
                    Toast.makeText(PatientEdit.this, "Not Valid Note", Toast.LENGTH_SHORT).show();
                }else {
                    noteDelete();
                }
            }
        });
        add_saveinfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updatepatient();
            }
        });
    }

        @Override
        public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
            switch (requestCode) {
                case Utility.MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE:
                    if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                        if(userChoosenTask.equals("Take Photo"))
                            cameraIntent();
                        else if(userChoosenTask.equals("Choose from Library"))
                            galleryIntent();
                    } else {
                        //code for deny
                    }
                    break;
            }
        }

        private void selectImage() {
            final CharSequence[] items = { "Take Photo", "Choose from Library",
                    "Cancel" };

            AlertDialog.Builder builder = new AlertDialog.Builder(PatientEdit.this);
            builder.setTitle("Add Photo!");
            builder.setItems(items, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int item) {
                    boolean result=Utility.checkPermission(PatientEdit.this);

                    if (items[item].equals("Take Photo")) {
                        userChoosenTask ="Take Photo";
                        if(result)
                            cameraIntent();

                    } else if (items[item].equals("Choose from Library")) {
                        userChoosenTask ="Choose from Library";
                        if(result)
                            galleryIntent();

                    } else if (items[item].equals("Cancel")) {
                        dialog.dismiss();
                    }
                }
            });
            builder.show();
        }

        private void galleryIntent()
        {
            Intent intent = new Intent();
            intent.setType("image/*");
            intent.setAction(Intent.ACTION_GET_CONTENT);//
            startActivityForResult(Intent.createChooser(intent, "Select File"),SELECT_FILE);
        }

        private void cameraIntent()
        {
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            startActivityForResult(intent, REQUEST_CAMERA);
        }

        @Override
        public void onActivityResult(int requestCode, int resultCode, Intent data) {
            super.onActivityResult(requestCode, resultCode, data);

            if (resultCode == Activity.RESULT_OK) {
                if (requestCode == SELECT_FILE)
                    onSelectFromGalleryResult(data);
                else if (requestCode == REQUEST_CAMERA)
                    onCaptureImageResult(data);
            }
            if (requestCode==444){
                try{
                    System.out.println("aaaaaaaaaa  data  "+driving_license);
                    driving_license = data.getStringExtra("driving_license");
                    tv_patientlicence.setText("Drivers Licences : "+driving_license);
                }catch (NullPointerException e){
                    Toast.makeText(this, "Doen Not Scan Try Again!....", Toast.LENGTH_SHORT).show();
                }

            }
        }

        private void onCaptureImageResult(Intent data) {
            Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            thumbnail.compress(Bitmap.CompressFormat.JPEG, 90, bytes);

            File destination = new File(Environment.getExternalStorageDirectory(),
                    System.currentTimeMillis() + ".jpg");

            FileOutputStream fo;
            try {
                destination.createNewFile();
                fo = new FileOutputStream(destination);
                fo.write(bytes.toByteArray());
                fo.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            imageCompanyFile=destination;
            System.out.println("aaaaaaaaaaa   file  "+destination);
            if (imgcheck==0){
                img_infoImage.setImageBitmap(thumbnail);
            }else {
                img_doucument_data.setImageBitmap(thumbnail);
            }
        }

        @SuppressWarnings("deprecation")
        private void onSelectFromGalleryResult(Intent data) {

            ByteArrayOutputStream bytes = new ByteArrayOutputStream();

            Bitmap bm=null;
            if (data != null) {
                try {
                    bm = MediaStore.Images.Media.getBitmap(getApplicationContext().getContentResolver(), data.getData());
                    bm.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            File destination = new File(Environment.getExternalStorageDirectory(),
                    System.currentTimeMillis() + ".jpg");

            FileOutputStream fo;
            try {
                destination.createNewFile();
                fo = new FileOutputStream(destination);
                fo.write(bytes.toByteArray());
                fo.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            imageCompanyFile=destination;
            System.out.println("aaaaaaaaaaa   file  "+destination);
            if (imgcheck==0){
                img_infoImage.setImageBitmap(bm);
            }else {
                img_doucument_data.setImageBitmap(bm);
            }

        }

    public void updatepatient(){
        noteslist.clear();
        mDialog = CUC.Companion.createDialog(PatientEdit.this);
        RequetsInterface apiService = RequetsInterface.Factory.create();
        Map parameterMap =new HashMap<String, String>();
        parameterMap.put("api_key", String.valueOf(Preferences.INSTANCE.getPreference(PatientEdit.this, "API_KEY")));
        if (logindata != null) {
            parameterMap.put("employee_id", logindata.getUser_id());
            // parameterMap.put("store_id", logindata.getUser_store_id());
        }

        parameterMap.put("patient_id",patient_data.getPatient_id());
        parameterMap.put("email_id",tv_email.getText().toString().trim());
        parameterMap.put("mobile",tv_phoneNumber.getText().toString().trim());
        parameterMap.put("driver_license",""+driving_license);

        System.out.println("aaaaaaaaaaa  parameterMap update  "+parameterMap);
        Log.i("aaaaaaaa   Result", "parameterMap : " + parameterMap);
        (new CompositeDisposable()).add(apiService.callPatientUpdate(parameterMap).
                observeOn(AndroidSchedulers.mainThread()).
                subscribeOn(Schedulers.io()).subscribe((Consumer)(new Consumer() {
            // $FF: synthetic method
            // $FF: bridge method
            public void accept(Object var1) {
                this.accept((GetStatus)var1);
            }

            public final void accept(GetStatus result) {
                System.out.println("aaaaaaaaa  result update "+result.toString());
                mDialog.cancel();
                if (result != null) {
                    mDialog.cancel();
                    System.out.println("aaaaaaaaa  result update  "+result.toString());
                    Log.i("aaaaaaa   Result", "" + result.toString());
                    if (result.getStatus().equalsIgnoreCase("0")){
                        Toast.makeText(PatientEdit.this, ""+result.getMessage(), Toast.LENGTH_SHORT).show();
                        CUC.Companion.displayToast(PatientEdit.this, result.getMessage(), result.getMessage());

                    }else {
                        CUC.Companion.displayToast(PatientEdit.this, ""+result.getMessage(), ""+result.getMessage());
                    }
                } else {
                    mDialog.cancel();
                }
            }
        }), (Consumer)(new Consumer() {
            // $FF: synthetic method
            // $FF: bridge method
            public void accept(Object var1) {
                this.accept((Throwable)var1);
            }

            public final void accept(Throwable error) {
                mDialog.cancel();
                Toast.makeText(PatientEdit.this, getResources().getString(R.string.connection_to_database_failed), Toast.LENGTH_SHORT).show();
                error.printStackTrace();
                System.out.println("aaaaaaaa  error update "+error.getMessage());
            }
        })));
    }

    public void addInfoImg(){

        mDialog = CUC.Companion.createDialog(PatientEdit.this);
        RequetsInterface apiService = RequetsInterface.Factory.create();
        Map parameterMap =new HashMap<String, String>();

        parameterMap.put("api_key", RequestBody.create(MediaType.parse("text/plain"), Preferences.INSTANCE.getPreference(PatientEdit.this, "API_KEY")));
        parameterMap.put("employee_id", RequestBody.create(MediaType.parse("text/plain"),  logindata.getUser_id()));
        parameterMap.put("patient_id", RequestBody.create(MediaType.parse("text/plain"), patient_data.getPatient_id()));

        RequestBody requestFile = RequestBody.create(MediaType.parse("image/jpeg"), imageCompanyFile);

        MultipartBody.Part body = MultipartBody.Part.createFormData("document_image", imageCompanyFile.getName(), requestFile);


        System.out.println("aaaaaaaaaaa  parameterMap expenses  "+parameterMap);
        Log.i("aaaaaaaa   Result", "parameterMap : " + parameterMap);
        (new CompositeDisposable()).add(apiService.addEmployeeinfoImg(parameterMap,body,RequestBody.create(MediaType.parse("text/plain"), imageCompanyFile.getName()))
                .observeOn(AndroidSchedulers.mainThread()).
                subscribeOn(Schedulers.io()).subscribe((Consumer)(new Consumer() {
            // $FF: synthetic method
            // $FF: bridge method
            public void accept(Object var1) {
                this.accept((GetStatus)var1);
            }

            public final void accept(GetStatus result) {
                System.out.println("aaaaaaaaa  result  "+result.toString());
                mDialog.cancel();
                if (result != null) {
                    mDialog.cancel();
                    System.out.println("aaaaaaaaa  result expenses  "+result.toString());
                    Log.i("aaaaaaa   Result", "" + result.toString());
                    if (result.getStatus().equalsIgnoreCase("0")){
                        Toast.makeText(PatientEdit.this, ""+result.getMessage(), Toast.LENGTH_SHORT).show();
                        CUC.Companion.displayToast(PatientEdit.this, ""+result.getMessage(), ""+result.getMessage());
                    }else {
                        CUC.Companion.displayToast(PatientEdit.this, ""+result.getMessage(), ""+result.getMessage());
                    }
                } else {
                    mDialog.cancel();
                }
            }
        }), (Consumer)(new Consumer() {
            // $FF: synthetic method
            // $FF: bridge method
            public void accept(Object var1) {
                this.accept((Throwable)var1);
            }

            public final void accept(Throwable error) {
                mDialog.cancel();
                Toast.makeText(PatientEdit.this, getResources().getString(R.string.connection_to_database_failed), Toast.LENGTH_SHORT).show();
                error.printStackTrace();
                System.out.println("aaaaaaaa  error update "+error.getMessage());
            }
        })));
    }

    public void getNoteslist(){
        noteslist.clear();
        mDialog = CUC.Companion.createDialog(PatientEdit.this);
        RequetsInterface apiService = RequetsInterface.Factory.create();
        Map parameterMap =new HashMap<String, String>();
        parameterMap.put("api_key", String.valueOf(Preferences.INSTANCE.getPreference((PatientEdit.this), "API_KEY")));
        if (logindata != null) {
            parameterMap.put("employee_id", logindata.getUser_id());
            // parameterMap.put("store_id", logindata.getUser_store_id());
        }

        parameterMap.put("patient_id",patient_data.getPatient_id());

        System.out.println("aaaaaaaaaaa  parameterMap expenses  "+parameterMap);
        Log.i("aaaaaaaa   Result", "parameterMap : " + parameterMap);
        (new CompositeDisposable()).add(apiService.callGetNotes(parameterMap).
                observeOn(AndroidSchedulers.mainThread()).
                subscribeOn(Schedulers.io()).subscribe((Consumer)(new Consumer() {
            // $FF: synthetic method
            // $FF: bridge method
            public void accept(Object var1) {
                this.accept((GetNotesDatashow)var1);
            }

            public final void accept(GetNotesDatashow result) {
                System.out.println("aaaaaaaaa  result  "+result.toString());
                mDialog.cancel();
                if (result != null) {
                    mDialog.cancel();
                    System.out.println("aaaaaaaaa  result expenses  "+result.toString());
                    Log.i("aaaaaaa   Result", "" + result.toString());
                    if (result.getNotes().size()>0){
                        noteslist.addAll(result.getNotes());
                        notesDialogAdaper.setRefresh(noteslist);
                        CUC.Companion.displayToast(PatientEdit.this, "Sucess", "Sucess");
                    }else {
                        CUC.Companion.displayToast(PatientEdit.this, "No Data", "No Data");
                    }
                } else {
                    mDialog.cancel();
                }
            }
        }), (Consumer)(new Consumer() {
            // $FF: synthetic method
            // $FF: bridge method
            public void accept(Object var1) {
                this.accept((Throwable)var1);
            }

            public final void accept(Throwable error) {
                mDialog.cancel();
                Toast.makeText(PatientEdit.this, getResources().getString(R.string.connection_to_database_failed), Toast.LENGTH_SHORT).show();
                error.printStackTrace();
                System.out.println("aaaaaaaa  error update "+error.getMessage());
            }
        })));
    }

    public void AddNote(){
        noteslist.clear();
        mDialog = CUC.Companion.createDialog(PatientEdit.this);
        RequetsInterface apiService = RequetsInterface.Factory.create();
        Map parameterMap =new HashMap<String, String>();
        parameterMap.put("api_key", String.valueOf(Preferences.INSTANCE.getPreference(PatientEdit.this, "API_KEY")));
        if (logindata != null) {
            parameterMap.put("employee_id", logindata.getUser_id());
            // parameterMap.put("store_id", logindata.getUser_store_id());
        }

        parameterMap.put("patient_id",patient_data.getPatient_id());
        parameterMap.put("notes_title",edt_note_name.getText().toString().trim());
        parameterMap.put("notes",edt_note_message.getText().toString().trim());
        parameterMap.put("notes_priority",""+notespriority);

        System.out.println("aaaaaaaaaaa  parameterMap expenses  "+parameterMap);
        Log.i("aaaaaaaa   Result", "parameterMap : " + parameterMap);
        (new CompositeDisposable()).add(apiService.callAddNote(parameterMap).
                observeOn(AndroidSchedulers.mainThread()).
                subscribeOn(Schedulers.io()).subscribe((Consumer)(new Consumer() {
            // $FF: synthetic method
            // $FF: bridge method
            public void accept(Object var1) {
                this.accept((GetStatus)var1);
            }

            public final void accept(GetStatus result) {
                System.out.println("aaaaaaaaa  result  "+result.toString());
                mDialog.cancel();
                if (result != null) {
                    mDialog.cancel();
                    System.out.println("aaaaaaaaa  result expenses  "+result.toString());
                    Log.i("aaaaaaa   Result", "" + result.toString());
                    if (result.getStatus().equalsIgnoreCase("0")){
                        edt_note_name.setText("");
                        edt_note_message.setText("");
                        getNoteslist();
                        CUC.Companion.displayToast(PatientEdit.this, result.getMessage(), result.getMessage());
                    }else {
                        CUC.Companion.displayToast(PatientEdit.this, ""+result.getMessage(), ""+result.getMessage());
                    }
                } else {
                    mDialog.cancel();
                }
            }
        }), (Consumer)(new Consumer() {
            // $FF: synthetic method
            // $FF: bridge method
            public void accept(Object var1) {
                this.accept((Throwable)var1);
            }

            public final void accept(Throwable error) {
                mDialog.cancel();
                Toast.makeText(PatientEdit.this, getResources().getString(R.string.connection_to_database_failed), Toast.LENGTH_SHORT).show();
                error.printStackTrace();
                System.out.println("aaaaaaaa  error update "+error.getMessage());
            }
        })));
    }

    public void noteDelete(){
        noteslist.clear();
        mDialog = CUC.Companion.createDialog(PatientEdit.this);
        RequetsInterface apiService = RequetsInterface.Factory.create();
        Map parameterMap =new HashMap<String, String>();
        parameterMap.put("api_key", String.valueOf(Preferences.INSTANCE.getPreference((PatientEdit.this), "API_KEY")));
        if (logindata != null) {
            parameterMap.put("employee_id", logindata.getUser_id());
            // parameterMap.put("store_id", logindata.getUser_store_id());
        }

        parameterMap.put("patient_id",patient_data.getPatient_id());
        parameterMap.put("notes_id",noteid);

        System.out.println("aaaaaaaaaaa  parameterMap expenses  "+parameterMap);
        Log.i("aaaaaaaa   Result", "parameterMap : " + parameterMap);
        (new CompositeDisposable()).add(apiService.callDeleteNote(parameterMap).
                observeOn(AndroidSchedulers.mainThread()).
                subscribeOn(Schedulers.io()).subscribe((Consumer)(new Consumer() {
            // $FF: synthetic method
            // $FF: bridge method
            public void accept(Object var1) {
                this.accept((GetStatus)var1);
            }

            public final void accept(GetStatus result) {
                System.out.println("aaaaaaaaa  result  "+result.toString());
                mDialog.cancel();
                if (result != null) {
                    mDialog.cancel();
                    System.out.println("aaaaaaaaa  result expenses  "+result.toString());
                    Log.i("aaaaaaa   Result", "" + result.toString());
                    if (result.getStatus().equalsIgnoreCase("0")){
                        edt_note_name.setText("");
                        edt_note_message.setText("");
                        getNoteslist();
                        CUC.Companion.displayToast(PatientEdit.this, result.getMessage(), result.getMessage());
                    }else {
                        CUC.Companion.displayToast((PatientEdit.this), ""+result.getMessage(), ""+result.getMessage());
                    }
                } else {
                    mDialog.cancel();
                }
            }
        }), (Consumer)(new Consumer() {
            // $FF: synthetic method
            // $FF: bridge method
            public void accept(Object var1) {
                this.accept((Throwable)var1);
            }

            public final void accept(Throwable error) {
                mDialog.cancel();
                Toast.makeText(PatientEdit.this, getResources().getString(R.string.connection_to_database_failed), Toast.LENGTH_SHORT).show();
                error.printStackTrace();
                System.out.println("aaaaaaaa  error update "+error.getMessage());
            }
        })));
    }

    public void noteClick(GetNotes getNotes) {
        this.noteid=getNotes.getPnote_id();
        System.out.println("aaaaaaaaaa  "+getNotes.getPnote_title());
        edt_note_name.setText(getNotes.getPnote_title());
        edt_note_message.setText(getNotes.getPnote_notes());
        edt_note_name.setSelection(edt_note_name.getText().toString().length());
        edt_note_message.setSelection(edt_note_message.getText().toString().length());
    }

    public void getDocuments(){
        documentlist.clear();
        mDialog = CUC.Companion.createDialog(PatientEdit.this);
        RequetsInterface apiService = RequetsInterface.Factory.create();
        Map parameterMap =new HashMap<String, String>();
        parameterMap.put("api_key", String.valueOf(Preferences.INSTANCE.getPreference((PatientEdit.this), "API_KEY")));
        if (logindata != null) {
            parameterMap.put("employee_id", logindata.getUser_id());
            // parameterMap.put("store_id", logindata.getUser_store_id());
        }

        parameterMap.put("patient_id",patient_data.getPatient_id());

        System.out.println("aaaaaaaaaaa  parameterMap expenses  "+parameterMap);
        Log.i("aaaaaaaa   Result", "parameterMap : " + parameterMap);
        (new CompositeDisposable()).add(apiService.callPatientDocument(parameterMap).
                observeOn(AndroidSchedulers.mainThread()).
                subscribeOn(Schedulers.io()).subscribe((Consumer)(new Consumer() {
            // $FF: synthetic method
            // $FF: bridge method
            public void accept(Object var1) {
                this.accept((GetDocumentNotes)var1);
            }

            public final void accept(GetDocumentNotes result) {
                System.out.println("aaaaaaaaa  result  "+result.toString());
                mDialog.cancel();
                if (result != null) {
                    mDialog.cancel();
                    documentpath=result.getDocument_path();
                    System.out.println("aaaaaaaaa  result expenses  "+result.toString());
                    Log.i("aaaaaaa   Result", "" + result.toString());
                    try{
                        if (TextUtils.isEmpty(result.getLicense_data().getPatient_dl_front())){
                            System.out.println("aaaaaaaaaaaaa  license  iffff  "+result.getLicense_data().getPatient_mmmp_front());
                            DocumentModel documentModel=new DocumentModel("",patient_data.getPatient_id(),"Driver License Front","","0","","",
                                    "","");
                            documentlist.add(documentModel);
                        }else {
                            System.out.println("aaaaaaaaaaaaa  license  elsee  "+result.getLicense_data().getPatient_mmmp_front());
                            DocumentModel documentModel=new DocumentModel("",patient_data.getPatient_id(),"Driver License Front",result.getLicense_data().getPatient_dl_front(),"0","","",
                                    "","");
                            documentlist.add(documentModel);
                        }
                        if (TextUtils.isEmpty(result.getLicense_data().getPatient_dl_back())){
                            DocumentModel documentModel=new DocumentModel("",patient_data.getPatient_id(),"Driver License Back","","0","","",
                                    "","");
                            documentlist.add(documentModel);
                        }else {
                            DocumentModel documentModel=new DocumentModel("",patient_data.getPatient_id(),"Driver License Back",result.getLicense_data().getPatient_dl_back(),"0","","",
                                    "","");
                            documentlist.add(documentModel);
                        }
                        if (TextUtils.isEmpty(result.getLicense_data().getPatient_mmmp_front())){
                            DocumentModel documentModel=new DocumentModel("",patient_data.getPatient_id(),"MMMP Front","","0","","",
                                    "","");
                            documentlist.add(documentModel);
                        }else {
                            DocumentModel documentModel=new DocumentModel("",patient_data.getPatient_id(),"MMMP Front",""+result.getLicense_data().getPatient_mmmp_front(),"0","","",
                                    "","");
                            documentlist.add(documentModel);
                        }
                        if (TextUtils.isEmpty(result.getLicense_data().getPatient_dl_back())){
                            DocumentModel documentModel=new DocumentModel("",patient_data.getPatient_id(),"MMMP Back","","0","","",
                                    "","");
                            documentlist.add(documentModel);
                        }else {
                            DocumentModel documentModel=new DocumentModel("",patient_data.getPatient_id(),"MMMP Back",result.getLicense_data().getPatient_mmmp_back(),"0","","",
                                    "","");
                            documentlist.add(documentModel);
                        }
                    }catch (Exception e){

                    }


                    if (result.getDocuments().size()>0){
                        /*for (int k=0;k<result.getDocuments().size();k++){
                            documentlist.add(result.getDocuments().get(k));
                        }*/
                        documentlist.addAll(result.getDocuments());
                        CUC.Companion.displayToast(PatientEdit.this, "Sucess", "Sucess");
                    }
                    documentPatientAdapter.setRefresh(documentlist,documentpath);
                } else {
                    mDialog.cancel();
                }
            }
        }), (Consumer)(new Consumer() {
            // $FF: synthetic method
            // $FF: bridge method
            public void accept(Object var1) {
                this.accept((Throwable)var1);
            }

            public final void accept(Throwable error) {
                mDialog.cancel();
                Toast.makeText(PatientEdit.this, getResources().getString(R.string.connection_to_database_failed), Toast.LENGTH_SHORT).show();
                error.printStackTrace();
                System.out.println("aaaaaaaa  error update "+error.getMessage());
            }
        })));
    }

    public void addDocumetImg(String title){

        mDialog = CUC.Companion.createDialog(PatientEdit.this);
        RequetsInterface apiService = RequetsInterface.Factory.create();
        Map parameterMap =new HashMap<String, String>();

        parameterMap.put("api_key", RequestBody.create(MediaType.parse("text/plain"), Preferences.INSTANCE.getPreference(PatientEdit.this, "API_KEY")));
        parameterMap.put("employee_id", RequestBody.create(MediaType.parse("text/plain"),  logindata.getUser_id()));
        parameterMap.put("patient_id", RequestBody.create(MediaType.parse("text/plain"), patient_data.getPatient_id()));
        parameterMap.put("document_title", RequestBody.create(MediaType.parse("text/plain"), title));


        RequestBody requestFile = RequestBody.create(MediaType.parse("image/jpeg"), imageCompanyFile);

        MultipartBody.Part body = MultipartBody.Part.createFormData("document_image", imageCompanyFile.getName(), requestFile);


        System.out.println("aaaaaaaaaaa  parameterMap add document  "+parameterMap);
        Log.i("aaaaaaaa   Result", "parameterMap : " + parameterMap);
        (new CompositeDisposable()).add(apiService.callPatientDocumentAdd(parameterMap,body,RequestBody.create(MediaType.parse("text/plain"), imageCompanyFile.getName()))
                .observeOn(AndroidSchedulers.mainThread()).
                        subscribeOn(Schedulers.io()).subscribe((Consumer)(new Consumer() {
                    // $FF: synthetic method
                    // $FF: bridge method
                    public void accept(Object var1) {
                        this.accept((GetStatus)var1);
                    }

                    public final void accept(GetStatus result) {
                        System.out.println("aaaaaaaaa  result  "+result.toString());
                        mDialog.cancel();
                        if (result != null) {
                            mDialog.cancel();
                            System.out.println("aaaaaaaaa  result add document  "+result.toString());
                            Log.i("aaaaaaa   Result", "" + result.toString());
                            if (result.getStatus().equalsIgnoreCase("0")){
                                edt_document_name.setText("");
                                img_doucument_data.setImageDrawable(getResources().getDrawable(R.drawable.add_photo));
                                getDocuments();
                                Toast.makeText(PatientEdit.this, ""+result.getMessage(), Toast.LENGTH_SHORT).show();
                                CUC.Companion.displayToast(PatientEdit.this, ""+result.getMessage(), ""+result.getMessage());
                            }else {
                                CUC.Companion.displayToast(PatientEdit.this, ""+result.getMessage(), ""+result.getMessage());
                            }
                        } else {
                            mDialog.cancel();
                        }
                    }
                }), (Consumer)(new Consumer() {
                    // $FF: synthetic method
                    // $FF: bridge method
                    public void accept(Object var1) {
                        this.accept((Throwable)var1);
                    }

                    public final void accept(Throwable error) {
                        mDialog.cancel();
                        Toast.makeText(PatientEdit.this, getResources().getString(R.string.connection_to_database_failed), Toast.LENGTH_SHORT).show();
                        error.printStackTrace();
                        System.out.println("aaaaaaaa  error update "+error.getMessage());
                    }
                })));
    }

    public void documentDelete(String document_id){
        noteslist.clear();
        mDialog = CUC.Companion.createDialog(PatientEdit.this);
        RequetsInterface apiService = RequetsInterface.Factory.create();
        Map parameterMap =new HashMap<String, String>();
        parameterMap.put("api_key", String.valueOf(Preferences.INSTANCE.getPreference((PatientEdit.this), "API_KEY")));
        if (logindata != null) {
            parameterMap.put("employee_id", logindata.getUser_id());
            // parameterMap.put("store_id", logindata.getUser_store_id());
        }

        parameterMap.put("patient_id",patient_data.getPatient_id());
        parameterMap.put("document_id",document_id);

        System.out.println("aaaaaaaaaaa  parameterMap delete document  "+parameterMap);
        Log.i("aaaaaaaa   Result", "parameterMap : " + parameterMap);
        (new CompositeDisposable()).add(apiService.callDeleteDocument(parameterMap).
                observeOn(AndroidSchedulers.mainThread()).
                subscribeOn(Schedulers.io()).subscribe((Consumer)(new Consumer() {
            // $FF: synthetic method
            // $FF: bridge method
            public void accept(Object var1) {
                this.accept((GetStatus)var1);
            }

            public final void accept(GetStatus result) {
                System.out.println("aaaaaaaaa  result  "+result.toString());
                mDialog.cancel();
                if (result != null) {
                    mDialog.cancel();
                    System.out.println("aaaaaaaaa  result deletedocument  "+result.toString());
                    Log.i("aaaaaaa   Result", "" + result.toString());
                    if (result.getStatus().equalsIgnoreCase("0")){
                        getDocuments();
                        CUC.Companion.displayToast(PatientEdit.this, result.getMessage(), result.getMessage());
                    }else {
                        CUC.Companion.displayToast((PatientEdit.this), ""+result.getMessage(), ""+result.getMessage());
                    }
                } else {
                    mDialog.cancel();
                }
            }
        }), (Consumer)(new Consumer() {
            // $FF: synthetic method
            // $FF: bridge method
            public void accept(Object var1) {
                this.accept((Throwable)var1);
            }

            public final void accept(Throwable error) {
                mDialog.cancel();
                Toast.makeText(PatientEdit.this, getResources().getString(R.string.connection_to_database_failed), Toast.LENGTH_SHORT).show();
                error.printStackTrace();
                System.out.println("aaaaaaaa  error update "+error.getMessage());
            }
        })));
    }

    public void setDocumentDelete(DocumentModel documentModel) {
        documentDelete(documentModel.getDocument_id());
    }

    public void setImageSee(DocumentModel documentModel) {
        layout_relative.setVisibility(View.GONE);
        img_full.setVisibility(View.VISIBLE);

        try {

            Picasso.with(PatientEdit.this)
                    .load(documentpath + documentModel.getDocument_path())
                    .placeholder(R.drawable.no_image_available)
                    .error(R.drawable.no_image_available)
                    .into(img_full);

        } catch (Exception e) {

            e.printStackTrace();
        }

    }


    public void addcgpatient(){
        noteslist.clear();
        mDialog = CUC.Companion.createDialog(PatientEdit.this);
        RequetsInterface apiService = RequetsInterface.Factory.create();
        Map parameterMap =new HashMap<String, String>();
        parameterMap.put("api_key", String.valueOf(Preferences.INSTANCE.getPreference(PatientEdit.this, "API_KEY")));
        if (logindata != null) {
            parameterMap.put("employee_id", logindata.getUser_id());
            // parameterMap.put("store_id", logindata.getUser_store_id());
        }

        parameterMap.put("patient_id",patient_data.getPatient_id());
        parameterMap.put("mmpp_id",""+edt_name_patient.getText().toString().trim());

        System.out.println("aaaaaaaaaaa  parameterMap expenses  "+parameterMap);
        Log.i("aaaaaaaa   Result", "parameterMap : " + parameterMap);
        (new CompositeDisposable()).add(apiService.calladdcgpatient(parameterMap).
                observeOn(AndroidSchedulers.mainThread()).
                subscribeOn(Schedulers.io()).subscribe((Consumer)(new Consumer() {
            // $FF: synthetic method
            // $FF: bridge method
            public void accept(Object var1) {
                this.accept((GetStatus)var1);
            }

            public final void accept(GetStatus result) {
                System.out.println("aaaaaaaaa  result  "+result.toString());
                mDialog.cancel();
                if (result != null) {
                    mDialog.cancel();
                    System.out.println("aaaaaaaaa  result expenses  "+result.toString());
                    Log.i("aaaaaaa   Result", "" + result.toString());
                    if (result.getStatus().equalsIgnoreCase("0")){
                        edt_name_patient.setText("");
                        getcgpatients();
                        Toast.makeText(PatientEdit.this, ""+result.getMessage(), Toast.LENGTH_SHORT).show();
                      //  showdialog(result.getMessage());
                        CUC.Companion.displayToast(PatientEdit.this, result.getMessage(), result.getMessage());
                    }else {
                        showdialog(result.getMessage());
                        CUC.Companion.displayToast(PatientEdit.this, ""+result.getMessage(), ""+result.getMessage());
                    }
                } else {
                    mDialog.cancel();
                }
            }
        }), (Consumer)(new Consumer() {
            // $FF: synthetic method
            // $FF: bridge method
            public void accept(Object var1) {
                this.accept((Throwable)var1);
            }

            public final void accept(Throwable error) {
                mDialog.cancel();
                Toast.makeText(PatientEdit.this, getResources().getString(R.string.connection_to_database_failed), Toast.LENGTH_SHORT).show();
                error.printStackTrace();
                System.out.println("aaaaaaaa  error update "+error.getMessage());
            }
        })));
    }

    public void getcgpatients(){
        caregiverlist.clear();
        mDialog = CUC.Companion.createDialog(PatientEdit.this);
        RequetsInterface apiService = RequetsInterface.Factory.create();
        Map parameterMap =new HashMap<String, String>();
        parameterMap.put("api_key", String.valueOf(Preferences.INSTANCE.getPreference((PatientEdit.this), "API_KEY")));
        if (logindata != null) {
            parameterMap.put("employee_id", logindata.getUser_id());
            // parameterMap.put("store_id", logindata.getUser_store_id());
        }

        parameterMap.put("patient_id",patient_data.getPatient_id());

        System.out.println("aaaaaaaaaaa  parameterMap getpatientmmp  "+parameterMap);
        Log.i("aaaaaaaa   Result", "parameterMap : " + parameterMap);
        (new CompositeDisposable()).add(apiService.callgetCaregivers(parameterMap).
                observeOn(AndroidSchedulers.mainThread()).
                subscribeOn(Schedulers.io()).subscribe((Consumer)(new Consumer() {
            // $FF: synthetic method
            // $FF: bridge method
            public void accept(Object var1) {
                this.accept((getcaregivers)var1);
            }

            public final void accept(getcaregivers result) {
                System.out.println("aaaaaaaaa  result  "+result.toString());
                mDialog.cancel();
                if (result != null) {
                    mDialog.cancel();
                    System.out.println("aaaaaaaaa  result caregivers  "+result.toString());
                    Log.i("aaaaaaa   Result", "" + result.toString());

                    if (result.getStatus().equals("0")){
                       try{
                           if (!result.getPatient_1().getPatient_id().isEmpty()){
                               caregiverlist.add(0,result.getPatient_1());
                           }

                       }catch (Exception e){

                       }
                        try{
                            if (!result.getPatient_2().getPatient_id().isEmpty()){
                                caregiverlist.add(0,result.getPatient_2());
                            }
                        }catch (Exception e){

                        }
                        try{
                            if (!result.getPatient_3().getPatient_id().isEmpty()){
                                caregiverlist.add(0,result.getPatient_3());
                            }
                        }catch (Exception e){

                        }
                        try{
                            if (!result.getPatient_4().getPatient_id().isEmpty()){
                                caregiverlist.add(0,result.getPatient_4());
                            }
                        }catch (Exception e){

                        }
                        try{
                            if (!result.getPatient_5().getPatient_id().isEmpty()){
                                caregiverlist.add(0,result.getPatient_5());
                            }

                        }catch (Exception e){

                        }

                        if (caregiverlist.size()!=0){
                            Collections.reverse(caregiverlist);
                            tv_nodata.setVisibility(View.GONE);
                            recycle_cgpatient.setVisibility(View.VISIBLE);
                            patientCgAdapter.setRefresh(caregiverlist);

                        }else {
                            tv_nodata.setVisibility(View.VISIBLE);
                            recycle_cgpatient.setVisibility(View.GONE);
                        }
                        notesDialogAdaper.setRefresh(noteslist);
                        CUC.Companion.displayToast(PatientEdit.this, "Sucess", "Sucess");
                    }else {
                        CUC.Companion.displayToast(PatientEdit.this, "No Data", "No Data");
                    }
                } else {
                    mDialog.cancel();
                }
            }
        }), (Consumer)(new Consumer() {
            // $FF: synthetic method
            // $FF: bridge method
            public void accept(Object var1) {
                this.accept((Throwable)var1);
            }

            public final void accept(Throwable error) {
                mDialog.cancel();
                Toast.makeText(PatientEdit.this, getResources().getString(R.string.connection_to_database_failed), Toast.LENGTH_SHORT).show();
                error.printStackTrace();
                System.out.println("aaaaaaaa  error update "+error.getMessage());
            }
        })));
    }

    public void showdialog(String msg){
        AlertDialog.Builder builder1 = new AlertDialog.Builder(PatientEdit.this);
        builder1.setMessage(""+msg);
        builder1.setCancelable(true);

        builder1.setPositiveButton(
                "Ok",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                        System.out.println("aaaaaaaaaaa  ");
                    }
                });

        AlertDialog alert11 = builder1.create();
        alert11.show();
    }

    public void setRemovecgpatient(getCaregiver getCaregiver) {
        cgpatientDelete(getCaregiver.getPatient_mmpp_licence_no());
    }

    public void cgpatientDelete(String mmpnumber){
        noteslist.clear();
        mDialog = CUC.Companion.createDialog(PatientEdit.this);
        RequetsInterface apiService = RequetsInterface.Factory.create();
        Map parameterMap =new HashMap<String, String>();
        parameterMap.put("api_key", String.valueOf(Preferences.INSTANCE.getPreference((PatientEdit.this), "API_KEY")));
        if (logindata != null) {
            parameterMap.put("employee_id", logindata.getUser_id());
            // parameterMap.put("store_id", logindata.getUser_store_id());
        }

        parameterMap.put("patient_id",patient_data.getPatient_id());
        parameterMap.put("mmpp_id",mmpnumber);

        System.out.println("aaaaaaaaaaa  parameterMap delete patient  "+parameterMap);
        Log.i("aaaaaaaa   Result", "parameterMap : " + parameterMap);
        (new CompositeDisposable()).add(apiService.callcgdelete(parameterMap).
                observeOn(AndroidSchedulers.mainThread()).
                subscribeOn(Schedulers.io()).subscribe((Consumer)(new Consumer() {
            // $FF: synthetic method
            // $FF: bridge method
            public void accept(Object var1) {
                this.accept((GetStatus)var1);
            }

            public final void accept(GetStatus result) {
                System.out.println("aaaaaaaaa  result  "+result.toString());
                mDialog.cancel();
                if (result != null) {
                    mDialog.cancel();
                    System.out.println("aaaaaaaaa  result delete patient  "+result.toString());
                    Log.i("aaaaaaa   Result", "" + result.toString());
                    if (result.getStatus().equalsIgnoreCase("0")){

                        getcgpatients();
                        CUC.Companion.displayToast(PatientEdit.this, result.getMessage(), result.getMessage());
                    }else {
                        CUC.Companion.displayToast((PatientEdit.this), ""+result.getMessage(), ""+result.getMessage());
                    }
                } else {
                    mDialog.cancel();
                }
            }
        }), (Consumer)(new Consumer() {
            // $FF: synthetic method
            // $FF: bridge method
            public void accept(Object var1) {
                this.accept((Throwable)var1);
            }

            public final void accept(Throwable error) {
                mDialog.cancel();
                Toast.makeText(PatientEdit.this, getResources().getString(R.string.connection_to_database_failed), Toast.LENGTH_SHORT).show();
                error.printStackTrace();
                System.out.println("aaaaaaaa  error update "+error.getMessage());
            }
        })));
    }
}

