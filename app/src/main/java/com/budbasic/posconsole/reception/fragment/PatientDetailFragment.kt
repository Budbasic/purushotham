package com.budbasic.posconsole.reception.fragment

import android.app.Activity
import android.app.Dialog
import android.graphics.Color
import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.budbasic.customer.global.Preferences
import com.budbasic.global.CUC
import com.budbasic.posconsole.R
import com.budbasic.posconsole.model.APIClass
import com.budbasic.posconsole.reception.StoreManagerActivity
import com.budbasic.posconsole.webservice.RequetsInterface
import com.bumptech.glide.Glide
import com.github.mikephil.charting.charts.LineChart
import com.github.mikephil.charting.components.AxisBase
import com.github.mikephil.charting.components.XAxis
import com.github.mikephil.charting.components.YAxis
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.LineData
import com.github.mikephil.charting.data.LineDataSet
import com.github.mikephil.charting.formatter.IAxisValueFormatter
import com.github.mikephil.charting.utils.ColorTemplate
import com.google.gson.Gson
import com.kotlindemo.model.GetEmployeeAppSetting
import com.kotlindemo.model.GetEmployeeLogin
import com.kotlindemo.model.GetInvoiceOrders
import com.kotlindemo.model.Hour_sales
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.grph_patient_info.*
import kotlinx.android.synthetic.main.grph_patient_info.view.*
import org.json.JSONObject

class PatientDetailFragment : Fragment(), APIClass.onCallListner {

    override fun onFailed(requestCode: Int) {

    }

    override fun onSuccess(requestCode: Int) {

    }
    lateinit var context: StoreManagerActivity
    var mView: View? = null
    var mDialog: Dialog? = null
    var logindata: GetEmployeeLogin? = null

    var apiClass: APIClass? = null

    var appSettingData: GetEmployeeAppSetting? = null
    var Hour_sales = ArrayList<Hour_sales>()

    var chart: LineChart? = null
    var bundle: Bundle = Bundle()
    var details: String = String()
    var patientId: String = String()
    var startDate: String = String()
    var endDate: String = String()
    var totalsale: String = String()
    var productId: String = String()
    var packet_id: String = String()
    var employeeId: String = String()
    var totalSpend:String= String()
    var avgSpend:String= String()
    var firstDate:String=""
    var middleDate:String=""
    var lastDate:String=""

    companion object {
        val fragmentName: String = "PatientDetailFragment"
        var Currency_value = ""
        var list: ArrayList<GetInvoiceOrders> = ArrayList()
    }
    override fun onAttach(activity: Activity?) {
        super.onAttach(activity)
        try {
            context = (activity as StoreManagerActivity?)!!
        } catch (e: ClassCastException) {
            throw ClassCastException(activity!!.toString() + " must implement onViewSelected")
        }

    }
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        mView = inflater.inflate(R.layout.grph_patient_info, container, false)
        logindata = Gson().fromJson(Preferences.getPreference(mView!!.context, "logindata"), GetEmployeeLogin::class.java)
        chart = mView!!.findViewById(R.id.line_chart)
        init(mView!!)

        return mView
    }

    fun init(view: View) {


        view.img_close.setOnClickListener {
            activity!!.onBackPressed()
        }
        bundle = arguments!!
        details = bundle.getString("Details")
        if (details.equals("Product")) {
            productId = bundle.getString("ProductId")
            packet_id = bundle.getString("packet_id")
            startDate = bundle.getString("StartDate")
            endDate = bundle.getString("EndDate")
            totalsale = bundle.getString("totalsale")
            view.tv_title.setText("Product")
            callEmployeeStoreAnalyticsProduct(view)

        } else if (details.equals("Patient")) {
            view.tv_batchtemp.setText("Phone #")
            view.tv_quntytemp.setText("Favorite day's")
            view.tv_totaltemp.setText("Total Spend")
            view.tv_pricetemp.setText("AVG.Spend")
            view.tv_title.setText("Patient")
            patientId = bundle.getString("PatientId")
            startDate = bundle.getString("StartDate")
            endDate = bundle.getString("EndDate")
            totalSpend=bundle.getString("TotalSell")
            avgSpend=bundle.getString("AvgSell")
            callEmployeeStoreAnalyticsPatient(view)
        } else if (details.equals("Employee")) {
            view.tv_batchtemp.setText("Phone #")
            view.tv_quntytemp.setText("Favorite day's")
            view.tv_totaltemp.setText("Total Spend")
            view.tv_pricetemp.setText("AVG.Spend")
            view.tv_title.setText("Employee")
            try{
                employeeId=bundle.getString("EmployeeId")
            }catch (e : NullPointerException){
                try{
                    employeeId=bundle.getString("EmployeeId")
                }catch (ee : IllegalStateException){

                }
            }
            try{
                startDate = bundle.getString("StartDate")
            }catch (e : NullPointerException){

            }
            try{
                endDate = bundle.getString("EndDate")
            }catch (e : NullPointerException){

            }
            try{
                totalSpend=bundle.getString("TotalSell")
            }catch (e : NullPointerException){

            }
            try{
                avgSpend=bundle.getString("AvgSell")
            }catch (e : NullPointerException){

            }

            callEmployeeStoreAnalyticsEmployee(view)

        }


    }
    fun setGraphData() {
        val maps = HashMap<String, String>()
        val values1: ArrayList<Entry> = ArrayList()
        for(i in Hour_sales.indices){

            maps.put(Hour_sales.get(i).time!!.substring(0,Hour_sales.get(i).time!!.length -2),Hour_sales.get(i).price.toString())
            values1.add(Entry(Hour_sales.get(i).time!!.substring(0,Hour_sales.get(i).time!!.length -2).toFloat(), Hour_sales.get(i).price.toString().toFloat()))
        }
        val set1 = LineDataSet(values1, "DataSet 1")
        set1.axisDependency = YAxis.AxisDependency.LEFT
        set1.color = resources.getColor(R.color.yellowlight)
        set1.disableDashedLine()
        set1.setCircleColor(resources.getColor(R.color.yellowlight))
        set1.lineWidth = 1f
        set1.circleRadius = 0f
        set1.fillAlpha = 25
        set1.fillColor = ColorTemplate.getHoloBlue()
        set1.highLightColor = Color.rgb(244, 117, 117)
        set1.setDrawCircleHole(false)
        set1.setMode(LineDataSet.Mode.CUBIC_BEZIER)
        val data = LineData(set1)
        //   data.setValueTextColor(Color.BLACK)
        data.setValueTextSize(10f)
        chart!!.setBackgroundColor(Color.WHITE)
        chart!!.axisLeft.setDrawGridLines(false)
        chart!!.getXAxis().setDrawGridLines(false)
        chart!!.setDrawGridBackground(false)
        // chart!!.xAxis.setDrawAxisLine(false)
        chart!!.getAxisRight().setDrawAxisLine(false)
        chart!!.getDescription().setEnabled(false)
        chart!!.setPinchZoom(false)
        chart!!.xAxis.setGranularity(1f)
        chart!!.getAxisLeft().setDrawLabels(true)
        chart!!.getAxisRight().setDrawLabels(false)
        chart!!.getXAxis().setValueFormatter(object : IAxisValueFormatter {
            override fun getFormattedValue(value: Float, axis: AxisBase?): String {
                if(value.toInt()<13){
                    return value.toInt().toString()+"AM"
                }else{
                    return value.toInt().toString()+"PM"
                }

            }
        })
        chart!!.getXAxis().setDrawLabels(false)
        chart!!.getXAxis().setPosition(XAxis.XAxisPosition.BOTTOM)
        chart!!.getLegend().setEnabled(false)
        chart!!.setData(data)


    }

    fun callEmployeeStoreAnalyticsProduct(view: View) {

        mDialog = CUC.createDialog(activity!!)
        val apiService = RequetsInterface.Factory.create()
        val parameterMap = HashMap<String, String>()
        parameterMap["api_key"] = "${Preferences.getPreference(activity!!, "API_KEY")}"
        if (logindata != null) {
            parameterMap["employee_id"] = "${logindata!!.user_id}"
        }
        parameterMap["store_id"] = logindata!!.user_store_id
        parameterMap["date_from"] = startDate
        parameterMap["date_to"] = endDate
        parameterMap["packet_id"] = packet_id
        parameterMap["product_id"] = productId
        println("aaaaaaaaa   parameters product   "+parameterMap)
        CompositeDisposable().add(apiService.callEmployeeStoreAnalyticsProduct(parameterMap)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe { result ->
                    if (mDialog != null && mDialog!!.isShowing)
                        mDialog!!.dismiss()
                    if (result != null) {
                        val GetEmployeeStoreAnalyticsProduct = result
                        Log.i("Result", "" + result.toString())

                        if (GetEmployeeStoreAnalyticsProduct != null) {
                            tv_name.setText(GetEmployeeStoreAnalyticsProduct.product_name)
                            tv_price.setText("$"+GetEmployeeStoreAnalyticsProduct.product_price)
                            tv_batch.setText(GetEmployeeStoreAnalyticsProduct.product_batch)
                            tv_qunty.setText(GetEmployeeStoreAnalyticsProduct.product_qty+" "+GetEmployeeStoreAnalyticsProduct.product_unit_ab)
                            tv_total_sales.setText("$"+totalsale)
                            Glide.with(activity!!).load(GetEmployeeStoreAnalyticsProduct.product_image).into(img)

                            if(GetEmployeeStoreAnalyticsProduct.graph_data!=null){
                                Hour_sales.clear()
                                Hour_sales=GetEmployeeStoreAnalyticsProduct.graph_data!!
                                setGraphData()
                            }
                            if(GetEmployeeStoreAnalyticsProduct.graph_dates!=null && !GetEmployeeStoreAnalyticsProduct.graph_dates!!.equals("")){
                                firstDate=GetEmployeeStoreAnalyticsProduct.graph_dates!!.first_date.toString()
                                middleDate=GetEmployeeStoreAnalyticsProduct.graph_dates!!.middle_date.toString()
                                lastDate=GetEmployeeStoreAnalyticsProduct.graph_dates!!.last_date.toString()
                                mView!!.tv_firstDate.setText(firstDate)
                                mView!!.tv_middleDate.setText(middleDate)
                                mView!!.tv_lastDate.setText(lastDate)
                            }else{
                                firstDate=""
                                middleDate=""
                                lastDate=""
                            }
                        }


                    }


                }

        )


    }

    fun callEmployeeStoreAnalyticsEmployee(view: View) {
        mDialog = CUC.createDialog(activity!!)
        val apiService = RequetsInterface.Factory.create()
        val parameterMap = HashMap<String, String>()
        parameterMap["api_key"] = "${Preferences.getPreference(activity!!, "API_KEY")}"
        if (logindata != null) {
            parameterMap["employee_id"] = "${logindata!!.user_id}"
        }
        parameterMap["store_id"] = logindata!!.user_store_id
        parameterMap["date_from"] = startDate
        parameterMap["date_to"] = endDate
        parameterMap["analytics_emp_id"] = employeeId

        CompositeDisposable().add(apiService.callEmployeeStoreAnalyticsEmployee(parameterMap)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe { result ->
                    if (mDialog != null && mDialog!!.isShowing)
                        mDialog!!.dismiss()
                    if (result != null) {
                        val GetEmployeeStoreAnalyticsEmployee = result
                        Log.i("Result", "" + result.toString())

                        if (GetEmployeeStoreAnalyticsEmployee != null) {
                            tv_name.setText(GetEmployeeStoreAnalyticsEmployee.emp_name)
                            tv_price.setText("$"+avgSpend)
                            tv_batch.setText(GetEmployeeStoreAnalyticsEmployee.emp_phone)
                            tv_qunty.setText(GetEmployeeStoreAnalyticsEmployee.top3days)
                            tv_total_sales.setText("$"+totalSpend)
                            tv_lince.setText(GetEmployeeStoreAnalyticsEmployee.emp_license)
                          //  Glide.with(activity!!).load(GetEmployeeStoreAnalyticsEmployee.patient_image).into(img)
                            if(GetEmployeeStoreAnalyticsEmployee.graph_data!=null){
                                Hour_sales.clear()
                                Hour_sales=GetEmployeeStoreAnalyticsEmployee.graph_data!!
                                setGraphData()
                            }
                            if(GetEmployeeStoreAnalyticsEmployee.graph_dates!=null && !GetEmployeeStoreAnalyticsEmployee.graph_dates!!.equals("")){
                                firstDate=GetEmployeeStoreAnalyticsEmployee.graph_dates!!.first_date.toString()
                                middleDate=GetEmployeeStoreAnalyticsEmployee.graph_dates!!.middle_date.toString()
                                lastDate=GetEmployeeStoreAnalyticsEmployee.graph_dates!!.last_date.toString()
                                mView!!.tv_firstDate.setText(firstDate)
                                mView!!.tv_middleDate.setText(middleDate)
                                mView!!.tv_lastDate.setText(lastDate)
                            }else{
                                firstDate=""
                                middleDate=""
                                lastDate=""
                            }
                        }


                    }


                }

        )


    }

    fun callEmployeeStoreAnalyticsPatient(view: View) {
        mDialog = CUC.createDialog(activity!!)
        val apiService = RequetsInterface.Factory.create()
        val parameterMap = HashMap<String, String>()
        parameterMap["api_key"] = "${Preferences.getPreference(activity!!, "API_KEY")}"
        if (logindata != null) {
            parameterMap["employee_id"] = "${logindata!!.user_id}"
        }
        parameterMap["store_id"] = logindata!!.user_store_id
        parameterMap["date_from"] = startDate
        parameterMap["date_to"] = endDate
        parameterMap["patient_id"] = patientId

        CompositeDisposable().add(apiService.callEmployeeStoreAnalyticsPatient(parameterMap)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe { result ->
                    if (mDialog != null && mDialog!!.isShowing)
                        mDialog!!.dismiss()
                    if (result != null) {
                        val GetEmployeeStoreAnalyticsPatient = result
                        Log.i("Result", "" + result.toString())

                        if (GetEmployeeStoreAnalyticsPatient != null) {
                            tv_name.setText(GetEmployeeStoreAnalyticsPatient.patient_name)
                            tv_price.setText("$"+avgSpend)
                            tv_batch.setText(GetEmployeeStoreAnalyticsPatient.patient_phone)
                            tv_qunty.setText(GetEmployeeStoreAnalyticsPatient.top3days)
                            tv_total_sales.setText("$"+totalSpend)
                            Glide.with(activity!!).load(GetEmployeeStoreAnalyticsPatient.patient_image).into(img)
                            tv_lince.setText(GetEmployeeStoreAnalyticsPatient.patient_licence)
                            if(GetEmployeeStoreAnalyticsPatient.graph_data!=null){
                                Hour_sales.clear()
                                Hour_sales=GetEmployeeStoreAnalyticsPatient.graph_data!!
                                setGraphData()
                            }
                            if(GetEmployeeStoreAnalyticsPatient.graph_dates!=null && !GetEmployeeStoreAnalyticsPatient.graph_dates!!.equals("")){
                               firstDate=GetEmployeeStoreAnalyticsPatient.graph_dates!!.first_date.toString()
                                middleDate=GetEmployeeStoreAnalyticsPatient.graph_dates!!.middle_date.toString()
                                lastDate=GetEmployeeStoreAnalyticsPatient.graph_dates!!.last_date.toString()
                                mView!!.tv_firstDate.setText(firstDate)
                                mView!!.tv_middleDate.setText(middleDate)
                                mView!!.tv_lastDate.setText(lastDate)
                            }else{
                                firstDate=""
                                middleDate=""
                                lastDate=""
                            }
                        }


                    }


                }

        )


    }

}


