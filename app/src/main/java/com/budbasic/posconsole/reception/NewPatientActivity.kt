package com.budbasic.posconsole.reception

import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.os.Parcelable
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import com.google.gson.Gson
import com.kotlindemo.model.GetEmployeeAppSetting
import com.kotlindemo.model.GetEmployeeLogin
import com.budbasic.posconsole.webservice.RequetsInterface
import com.budbasic.posconsole.R
import com.budbasic.posconsole.reception.fragment.CreateAccountActivity
import com.budbasic.customer.global.Preferences
import com.budbasic.global.CUC
import com.budbasic.posconsole.model.APIClass
import com.mylibrary.DDJActivity
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import org.json.JSONObject

class NewPatientActivity : AppCompatActivity(), APIClass.onCallListner {
    override fun onFailed(requestCode: Int) {
    }

    override fun onSuccess(requestCode: Int) {
        if (requestCode == 1) {
            callEmployeeCheckPatient()
        } else if (requestCode == 2) {
            callEmployeePatientAssignCompany()
        }
    }

    lateinit var mDialog: Dialog
    internal val CAMERA = 0x7
    lateinit var logindata: GetEmployeeLogin
    lateinit var edt_driving_license: TextView

    var patient_id = ""
    var driving_license = ""
    var Timestamp = ""
    var dob = ""
    var Address = ""
    var City = ""
    var state_code = ""
    var Name = ""
    var lName = ""
    var Postal = ""
    lateinit var appSettingData: GetEmployeeAppSetting
    var apiClass: APIClass? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.add_new_patient)
        apiClass = APIClass(this@NewPatientActivity, this)
        logindata = Gson().fromJson(Preferences.getPreference(this@NewPatientActivity, "logindata"), GetEmployeeLogin::class.java)
        appSettingData = Gson().fromJson(Preferences.getPreference(this@NewPatientActivity, "AppSetting"), GetEmployeeAppSetting::class.java)
        initView()
    }

    fun initView() {
        val nextButton = findViewById<TextView>(R.id.button_next)
        val iv_scan_id = findViewById<ImageView>(R.id.iv_scan_id)
        val iv_back = findViewById<TextView>(R.id.iv_back)
        edt_driving_license = findViewById(R.id.edt_driving_license)
        iv_back.setOnClickListener {
            finish()
        }
        nextButton.setOnClickListener {
            if (edt_driving_license.text.toString().trim() == "") {
                CUC.displayToast(this@NewPatientActivity, "0", "Enter patient driving license")
            } else {
                callEmployeeCheckPatient()
            }
        }
        iv_scan_id.setOnClickListener {
            askForPermission(android.Manifest.permission.CAMERA, CAMERA)
        }
    }

    private fun askForPermission(permission: String, requestCode: Int?) {
        if (ContextCompat.checkSelfPermission(this@NewPatientActivity, permission) != PackageManager.PERMISSION_GRANTED) {
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this@NewPatientActivity, permission)) {
                //This is called if user has denied the permission before
                //In this case I am just asking the permission again
                ActivityCompat.requestPermissions(this@NewPatientActivity, arrayOf(permission), requestCode!!)
            } else {
                ActivityCompat.requestPermissions(this@NewPatientActivity, arrayOf(permission), requestCode!!)
            }
        } else {
            val startScanner = Intent(this@NewPatientActivity, DDJActivity::class.java)
            startActivityForResult(startScanner, 0x101)
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (ActivityCompat.checkSelfPermission(this@NewPatientActivity, permissions[0]) == PackageManager.PERMISSION_GRANTED) {
            when (requestCode) {
                //Camera
                CAMERA -> {
                    val startScanner = Intent(this@NewPatientActivity, DDJActivity::class.java)
                    startActivityForResult(startScanner, 0x101)
                }
            }
        } else {
            Toast.makeText(this@NewPatientActivity, "Permission denied", Toast.LENGTH_SHORT).show()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when (requestCode) {
            0x101 -> if (resultCode == Activity.RESULT_OK) {
                if (resultCode == Activity.RESULT_OK && data != null && data.hasExtra("scannerResult")) {
                    if (data.getStringExtra("scannerResult").isNotEmpty()) {
                        val resultOBJ = JSONObject(data.getStringExtra("scannerResult"))
                        Log.i("scannerResult", "scannerResult : $resultOBJ")
                        driving_license = resultOBJ.optString("DAQ")
                        Timestamp = resultOBJ.optString("DBA")
                        dob = resultOBJ.optString("DBD")
                        Address = "${resultOBJ.optString("DAG")},${resultOBJ.optString("DAI")},${resultOBJ.optString("DAJ")},${resultOBJ.optString("DAK")}"
                        //Address = resultOBJ.optString("DAG")
                        City = resultOBJ.optString("DAI")
                        Name = resultOBJ.optString("DCT")
                        Postal = resultOBJ.optString("DAK")
                        lName = resultOBJ.optString("DCS")
                        //edt_licence.text = "$driving_license"
                        if (Address != "") {
                            val splitAddress = Address.split(",")
                            Postal = "${splitAddress[splitAddress.size - 1]}"
                            state_code = "${splitAddress[splitAddress.size - 2]}"
                            City = "${splitAddress[splitAddress.size - 3]}"
                            Log.i("combinedResult", "${splitAddress[splitAddress.size - 1]}")
                            Log.i("combinedResult", "${splitAddress[splitAddress.size - 2]}")
                            Log.i("combinedResult", "${splitAddress[splitAddress.size - 3]}")
                        }
//                        if (Timestamp != "") {
//                            //08252018
//                            Timestamp = "${CUC.getdatefromoneformattoAnother("MMddyyyy", "yyyy-MM-dd", Timestamp)}"
//                        }
//                        if (dob != "") {
//                            //08252018
//                            dob = "${CUC.getdatefromoneformattoAnother("MMddyyyy", "yyyy-MM-dd", dob)}"
//                        }
                        edt_driving_license.text = "$driving_license"
                        callEmployeeCheckPatient()
                    }
                } else {
                    // if BlinkID activity did not return result, user has probably
                    // pressed Back button and cancelled scanning
                    driving_license = ""
                    Timestamp = ""
                    dob = ""
                    Address = ""
                    City = ""
                    Name = ""
                    Postal = ""
                    lName = ""
                    edt_driving_license.text = ""
                    CUC.displayToast(this@NewPatientActivity, "0", "please scan valid license.")
                }
            }

            123 -> if (resultCode == Activity.RESULT_FIRST_USER) {
                setResult(Activity.RESULT_FIRST_USER)
                finish()
            }
        }
    }

    fun callEmployeeCheckPatient() {
        if (CUC.isNetworkAvailablewithPopup(this@NewPatientActivity)) {
            mDialog = CUC.createDialog(this@NewPatientActivity)
            val apiService = RequetsInterface.Factory.create()
            val parameterMap = HashMap<String, String>()
            parameterMap["api_key"] = "${Preferences.getPreference(this@NewPatientActivity, "API_KEY")}"
            parameterMap["patient_licence_no"] = "${edt_driving_license.text.toString().trim()}"
            parameterMap["store_id"] = "${logindata.user_store_id}"
            println("aaaaaaaaaaaa  new patient  "+parameterMap)
            Log.i("Result", "parameterMap : $parameterMap")
            CompositeDisposable().add(apiService.callEmployeeCheckPatient(parameterMap)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe({ result ->
                        if (mDialog != null && mDialog.isShowing)
                            mDialog.dismiss()
                        println("aaaaaaaaaaaa   new Patient result   "+result.toString())
                        Log.i("Result", "" + result.toString())
                        if (result != null) {
                            if (result.status == "0") {
                                if (result.show_popup_aquire == "0") {
                                    //popup
                                    val builder = AlertDialog.Builder(this)
                                    builder.setMessage("${result.message}")

                                    builder.setPositiveButton("Yes") { dialog, which ->
                                        dialog.dismiss()
                                        patient_id = "${result.patient_id}"
                                        callEmployeePatientAssignCompany()
                                    }

                                    builder.setNegativeButton("No") { dialog, which -> dialog.dismiss() }
                                    val alert = builder.create()
                                    alert.show()
                                } else if (result.show_popup_aquire == "2") {
                                    //Create
                                    //val intent = Intent(this@NewPatientActivity, CreateAccountActivity::class.java)
                                    val intent = Intent(this@NewPatientActivity, com.budbasic.posconsole.reception.CreateAccountActivity::class.java)
                                    intent.putExtra("driving_license", "${edt_driving_license.text}")
                                    val logindata = Gson().toJson(logindata)
                                    intent.putExtra("logindata", "${logindata}")

                                   /* intent.putExtra("Timestamp", "${CUC.getdatefromoneformattoAnother("MMddyyyy", "MM-dd-yyyy", Timestamp)}")
                                    intent.putExtra("dob", "${CUC.getdatefromoneformattoAnother("MMddyyyy", "MM-dd-yyyy", dob)}")
                                    intent.putExtra("Address", "$Address")
                                    intent.putExtra("City", "$City")
                                    intent.putExtra("state_code", "$state_code")
                                    intent.putExtra("Name", "$Name")
                                    intent.putExtra("lName", "$lName")
                                    intent.putExtra("Postal", "$Postal")*/
                                    startActivityForResult(intent, 123)
                                    //finish()
                                } else if (result.show_popup_aquire == "1") {
                                    //not vaild
                                    edt_driving_license.text = ""
                                }
                                CUC.displayToast(this@NewPatientActivity, result.show_status, result.message)
                            } else if (result.status == "10") {
                                apiClass!!.callApiKey(1)
                            } else {
                                CUC.displayToast(this@NewPatientActivity, result.show_status, result.message)
                            }
                        } else {
                           // CUC.displayToast(this@NewPatientActivity, "0", getString(R.string.connection_to_database_failed))
                        }
                    }, { error ->
                        if (mDialog != null && mDialog.isShowing)
                            mDialog.dismiss()
                        CUC.displayToast(this@NewPatientActivity, "0", getString(R.string.connection_to_database_failed))
                        error.printStackTrace()
                    })
            )
        }
    }

    fun callEmployeePatientAssignCompany() {
        if (CUC.isNetworkAvailablewithPopup(this@NewPatientActivity)) {
            mDialog = CUC.createDialog(this@NewPatientActivity)
            val apiService = RequetsInterface.Factory.create()
            val parameterMap = HashMap<String, String>()
            parameterMap["api_key"] = "${Preferences.getPreference(this@NewPatientActivity, "API_KEY")}"
            parameterMap["patient_licence_no"] = "${edt_driving_license.text.toString().trim()}"
            parameterMap["store_id"] = "${logindata.user_store_id}"
            parameterMap["patient_id"] = "$patient_id"
            Log.i("Result", "parameterMap : $parameterMap")
            CompositeDisposable().add(apiService.callEmployeePatientAssignCompany(parameterMap)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe({ result ->
                        if (mDialog != null && mDialog.isShowing)
                            mDialog.dismiss()
                        Log.i("Result", "" + result.toString())
                        if (result != null) {
                            if (result.status == "0") {
                                CUC.displayToast(this@NewPatientActivity, result.show_status, result.message)
                                finish()
                            } else if (result.status == "10") {
                                apiClass!!.callApiKey(2)
                            } else {
                                CUC.displayToast(this@NewPatientActivity, result.show_status, result.message)
                            }
                        } else {
                          //  CUC.displayToast(this@NewPatientActivity, "0", getString(R.string.connection_to_database_failed))
                        }
                    }, { error ->
                        if (mDialog != null && mDialog.isShowing)
                            mDialog.dismiss()
                        CUC.displayToast(this@NewPatientActivity, "0", getString(R.string.connection_to_database_failed))
                        error.printStackTrace()
                    })
            )
        }
    }
}