package com.budbasic.posconsole.reception.fragment

import android.app.Activity
import android.app.DatePickerDialog
import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.graphics.Color
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.provider.Settings
import android.support.design.widget.Snackbar
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v7.app.AlertDialog
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.*
import android.widget.*
import com.budbasic.customer.global.Preferences
import com.budbasic.global.CUC
import com.budbasic.posconsole.R
import com.budbasic.posconsole.dialogs.AddLicenceDialog
import com.budbasic.posconsole.model.APIClass
import com.budbasic.posconsole.queue.MenuActivity
import com.budbasic.posconsole.queue.MenuCopyActivity
import com.budbasic.posconsole.sales.SalesCategoryActivity
import com.budbasic.posconsole.webservice.RequetsInterface
import com.bumptech.glide.Glide
import com.google.gson.Gson
import com.kotlindemo.model.GetEmployeeLogin
import com.kotlindemo.model.GetQueueList
import com.kotlindemo.model.queue_licence_data
import com.tbruyelle.rxpermissions2.RxPermissions
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.functions.Consumer
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.queue_fragment.*
import kotlinx.android.synthetic.main.queue_fragment.view.*
import java.util.*
import java.util.concurrent.TimeUnit
import kotlin.collections.HashMap
import kotlin.collections.set
import com.budbasic.posconsole.reception.NewCheckInActivity
import com.budbasic.posconsole.reception.NewPatientActivity
import com.budbasic.posconsole.reception.SalesActivity
import com.kotlindemo.model.GetEmployeeAppSetting
import io.fabric.sdk.android.services.common.CommonUtils

class QueueFragment : Fragment(), APIClass.onCallListner {


    override fun onFailed(requestCode: Int) {

    }

    override fun onSuccess(requestCode: Int) {
        if (requestCode == EMPLOYEEQUEUECLOSE) {
            callEmployeeQueueClose()
        } else if (requestCode == APPQVALIDATEQUEUEPUSH) {
            callAppqValidateQueuePush()
        } else if (requestCode == EMPLOYEESALESQUEUEPATIENT) {
            callEmployeeSalesQueuePatient()
        } else if (requestCode == EMPLOYEEQUEUELIST) {
            callEmployeeQueueList()
        } else if (requestCode == APPQVALIDATEQUEUE) {
           // callAppqValidateQueue()
        } else if (requestCode == APPQVERIFYDLICENCE) {
            callEmployeeQueueList()
        }

    }

    val EMPLOYEEQUEUECLOSE = 1
    val APPQVALIDATEQUEUEPUSH = 2
    val EMPLOYEESALESQUEUEPATIENT = 3
    val EMPLOYEEQUEUELIST = 4
    val APPQVALIDATEQUEUE = 5
    val EMPLOYEEPATIENTASSIGNCOMPANY = 6
    val APPQVERIFYDLICENCE = 7

    var chaeckQueue = ""

     var mView: View? = null
    lateinit var root: View
    var paging = 0
    lateinit var mDialog: Dialog
    lateinit var logindata: GetEmployeeLogin

    lateinit var productAdapter: ProductAdapter
    var appSettingData: GetEmployeeAppSetting? = null
    var list: ArrayList<GetQueueList> = ArrayList()

    var tillopen="0"
    var storeopen="0"
    var queue_id = ""
    var queue_remove_id = ""
    var patient_id = "0"
    var driving_license = ""
    var queue_type = "0"
    var selectDate = ""
    lateinit var subscription: Disposable
    lateinit var licenceData: queue_licence_data

    lateinit var dialog_changepass: Dialog
    var queueStatus: Array<String>? = null
    var rxPermissions: RxPermissions? = null
    var apiClass: APIClass? = null
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        if (view != null)
            mView = view
        else
            mView = inflater.inflate(R.layout.queue_fragment, container, false)
        apiClass = APIClass(context!!, this)
        rxPermissions = RxPermissions(activity!!)
        logindata = Gson().fromJson(Preferences.getPreference(mView!!.context, "logindata"), GetEmployeeLogin::class.java)
        init()
        return mView
    }

    fun init() {
        mView!!.ll_search_main.setOnClickListener {
        }
        mView!!.btn_back.setOnClickListener {
          activity!!.onBackPressed()
        }
        val layout_manager = LinearLayoutManager(mView!!.context)
        mView!!.rv_customer_list!!.layoutManager = layout_manager
        productAdapter = ProductAdapter(list, mView!!.context, object : onClickListener {
            override fun callClick(data: GetQueueList) {
                if (queue_type == "3") {
                    //   queue_id = data.queue_id
                    try {
                        queue_id = data.queue_id
                     //   Log.d("ASASA"," = "+queue_id)
                    //    Log.i("Result", "queue_licence_data  : ${data.queue_licence_data}")
                        licenceData = Gson().fromJson(data.queue_licence_data, queue_licence_data::class.java)
                   //     Log.i("Result", "licenceData  : $licenceData")
                        if (licenceData.licence_data != null) {
                            //QueueInfoDialog()
                            chaeckQueue = "LeftUnassisted"
                            callAppqValidateQueue(data)
                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                    //  callEmployeeSalesQueuePatient()
                } else if (queue_type == "2") {
                    queue_id = data.queue_id
                    callEmployeeSalesQueuePatient()
                } else if (queue_type == "0") {
                    println("aaaaaaaaa   queue_id "+data.queue_id)
                    try {
                        queue_id = data.queue_id
                        Log.i("Result", "queue_licence_data  : ${data.queue_licence_data}")

                       // licenceData = Gson().fromJson(data.queue_licence_data, queue_licence_data::class.java)
                       // Log.i("Result", "licenceData  : $licenceData")
                        if (data.queue_licence_data != null) {
                            println("aaaaaaaaa   queue_id ifff "+data.queue_id)
                            //QueueInfoDialog()
                            chaeckQueue = "InQueue"
                            callAppqValidateQueue(data)
                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                        println("aaaaaaaaaaaa   exception   "+e.message)
                    }
                }
            }
        })
        appSettingData = Gson().fromJson(Preferences.getPreference(context!!, "AppSetting"), GetEmployeeAppSetting::class.java)

        queueStatus = resources.getStringArray(R.array.queue_status)
        mView!!.rv_customer_list!!.adapter = productAdapter
        var mobileappAdapter = ArrayAdapter<String>(mView!!.context, R.layout.spinner_item, R.id.dropdown_item, queueStatus)
        mView!!.bs_status.setText("${resources.getStringArray(R.array.queue_status)[0]}")
        mView!!.bs_status.setAdapter(mobileappAdapter)
        mView!!.bs_status.setOnItemClickListener { adapterView, view, i, l ->
            if (mView!!.bs_status.text.toString() == "Left Unassisted") {
                queue_type = "3"
                setVisibilityRemoveButton(true)
            } else if (mView!!.bs_status.text.toString() == "Quick Order") {
                queue_type = "2"
                setVisibilityRemoveButton(false)
            } else if (mView!!.bs_status.text.toString() == "Pushed to Sales") {
                queue_type = "1"
                setVisibilityRemoveButton(false)
            } else {
                queue_type = "0"
                setVisibilityRemoveButton(false)
            }
            callEmployeeQueueList()
            //Log.i(CreateAccountActivity::class.java.simpleName, "queue_type  : $queue_type")
        }

        mView!!.btn_add.setOnClickListener {
            if (storeopen.equals("0")){
                CUC.displayToast(activity!!, "0", "Please Open Store")
            }else{
                if (tillopen.equals("0")){
                    CUC.displayToast(activity!!, "0", "Please Open Till")
                }else{
                    rxPermissions!!.request(
                            android.Manifest.permission.CAMERA)
                            .subscribe(object : Consumer<Boolean> {
                                override fun accept(granted: Boolean?) {
                                    if (granted!!) { // Always true pre-M
                                        // I can control the camera now
                                        val myIntent = Intent(activity!!, MenuCopyActivity::class.java)
                                        myIntent.putExtra("status", "2")
                                        activity!!.startActivityForResult(myIntent, 555)
                                    } else {
                                        // Oups permission denied
                                        Snackbar.make(mView!!.findViewById(R.id.ll_search_main), resources.getString(R.string.permissionenable),
                                                Snackbar.LENGTH_LONG)
                                                .setActionTextColor(Color.WHITE)
                                                .setAction(resources.getString(R.string.ok), object : View.OnClickListener {
                                                    override fun onClick(p0: View?) {
                                                        val intent = Intent()
                                                        intent.action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
                                                        val uri = Uri.fromParts("package", activity!!.packageName, null)
                                                        intent.data = uri
                                                        startActivity(intent)
                                                    }
                                                }).show()
                                    }
                                }
                            })
                }
            }

        }

        var mYear = Calendar.getInstance().get(Calendar.YEAR)
        var mMonth = Calendar.getInstance().get(Calendar.MONTH)
        var mDay = Calendar.getInstance().get(Calendar.DAY_OF_MONTH)

        selectDate = "${mYear}-" +
                "${(if (mMonth + 1 >= 10) mMonth + 1 else "0" + (mMonth + 1))}" +
                "-${(if (mDay >= 10) mDay else "0$mDay")}"

        mView!!.tv_start_date.text = "${(if (mMonth + 1 >= 10) mMonth + 1 else "0" + (mMonth + 1))}" +
                "-${(if (mDay >= 10) mDay else "0$mDay")}" +
                "-${mYear}"

        mView!!.tv_start_date.setOnClickListener {
            val datePickerDialog = DatePickerDialog(mView!!.context, DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                mYear = year
                mMonth = monthOfYear
                mDay = dayOfMonth

                selectDate = "$year-${(if (monthOfYear + 1 >= 10) monthOfYear + 1 else "0" + (monthOfYear + 1))}-${(if (dayOfMonth >= 10) dayOfMonth else "0$dayOfMonth")}"
                mView!!.tv_start_date.text = "${(if (monthOfYear + 1 >= 10) monthOfYear + 1 else "0" + (monthOfYear + 1))}-${(if (dayOfMonth >= 10) dayOfMonth else "0$dayOfMonth")}-$year"
                callEmployeeQueueList()

            }, mYear, mMonth, mDay)
            datePickerDialog.datePicker.maxDate = System.currentTimeMillis()
            datePickerDialog.show()
           // callEmployeeQueueList()
        }

        mView!!.manual_btu.setOnClickListener {

            if (!storeopen.equals("0")){
                if (!tillopen.equals("0")){
                    val mDialog = AddLicenceDialog(this!!.context!!, object : AddLicenceDialog.AddLicence {
                        override fun addLicence(Licencenumber: String) {
                            println("aaaaaaaa  licence number "+Licencenumber.toString())
                            mDialog.dismiss()
                            callAppqVerifyDlicence(Licencenumber.toString())
                        }
                    })

                    mDialog.show(activity!!.fragmentManager, "promo_code")

                }else{
                    CUC.displayToast(activity!!, "0", "Please Open Till")
                }
            }else{
                CUC.displayToast(activity!!, "0", "Please Open Store")
            }


            //val intent = Intent(context!!, NewCheckInActivity::class.java)
            //startActivityForResult(intent, 122)
        }
        callCheckStoreOpen()


    }
    fun callAppqVerifyDlicence(drivinglicenceno : String) {
        mDialog = CUC.createDialog(activity!!)
        val apiService = RequetsInterface.create()
        val parameterMap = HashMap<String, String>()
        parameterMap["api_key"] = "${Preferences.getPreference(activity!!, "API_KEY")}"
        parameterMap["licence_no"] = "$drivinglicenceno"
        parameterMap["login_user"] = "${logindata.user_id}"
        parameterMap["store_id"] = "${logindata.user_store_id}"
        parameterMap["company_id"] = "0"

        Log.i("aaaaaa  Result", "parameterMap : $parameterMap")
        CompositeDisposable().add(apiService.callAppqVerifyDlicence(parameterMap)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe({ result ->
                    if (mDialog != null && mDialog.isShowing)
                        mDialog.dismiss()
                    if (result != null) {
                        //  Log.d("AAAAA", "AAAA")
                        Log.i("aaaa Result", "" + result.toString())
                        if (result.status == "0") {
                            if (result.image != null && result.image != "") {
//                                Glide.with(this@MessageActivity)
//                                        .applyDefaultRequestOptions(RequestOptions().format(DecodeFormat.PREFER_ARGB_8888).error(R.drawable.no_image_available))
//                                        .load("${result.image}")
//                                        .into(img_msg)
                            }
                            callEmployeeQueueList()
//                            tv_msg.text = result.message
//                            mHandler.postDelayed(mRunable, 10000)
//                            startBlinkAnim()
                        } else if (result.status == "10") {
                            apiClass!!.callApiKey(APPQVERIFYDLICENCE)
                        } else {
                            if (result.image != null && result.image != "") {
//                                Glide.with(this@MessageActivity)
//                                        .applyDefaultRequestOptions(RequestOptions().format(DecodeFormat.PREFER_ARGB_8888).error(R.drawable.no_image_available))
//                                        .load("${result.image}")
//                                        .into(img_msg)
                            }

//                            tv_msg.text = result.message
//                            mHandler.postDelayed(mRunable, 10000)
//                            startBlinkAnim()
                            println("aaaaaaaaaaaaa   else  ")
                            context?.let { CUC.displayToast(it, result.show_status, result.message) }
                        }
                    } else {
                        println("aaaaaaaaaaaaa   else else ")
                        context?.let { CUC.displayToast(it, "0", getString(R.string.connection_to_database_failed)) }
                    }
                }, { error ->
                    if (mDialog != null && mDialog.isShowing)
                        mDialog.dismiss()
                    CUC.displayToast(activity!!, "0", getString(R.string.connection_to_database_failed))

                    error.printStackTrace()
                })
        )
    }
    private fun setVisibilityRemoveButton(isLeftAssist: Boolean) {
        if (isLeftAssist) {
            ll_extra.visibility = View.GONE
            ll_close.visibility = View.GONE
        } else {
            ll_extra.visibility = View.INVISIBLE
            ll_close.visibility = View.INVISIBLE
        }
    }

    override fun onStart() {
        super.onStart()
        startInterval()
    }

    override fun onStop() {
        super.onStop()
        stopInterval()
    }


    fun startInterval() {
        subscription = Observable.interval(30000, 30000, TimeUnit.MILLISECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    Log.i(EmployeeOrdersFragment::class.java.simpleName, "Interval : $it ")
                    callEmployeeQueueList()
                }
    }

    fun stopInterval() {
        subscription.dispose()
    }

    fun QueueInfoDialog() {
        dialog_changepass = Dialog(mView!!.context, R.style.TransparentProgressDialog)
        dialog_changepass.window.setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        dialog_changepass.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog_changepass.setContentView(R.layout.queue_info_layout)
        dialog_changepass.window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)
        dialog_changepass.show()
        val mColor1 = ContextCompat.getColor(mView!!.context, R.color.colorPrimary)
        val mColor2 = ContextCompat.getColor(mView!!.context, R.color.border_gray)
        val cv_validate = dialog_changepass.findViewById<LinearLayout>(R.id.cv_validate)
        val tvaddress = dialog_changepass.findViewById<TextView>(R.id.tvaddress)

        val tv_customer_name = dialog_changepass.findViewById<TextView>(R.id.tv_customer_name)
        val tvpatientdob = dialog_changepass.findViewById<TextView>(R.id.tvpatientdob)
        val tvlicenceno = dialog_changepass.findViewById<TextView>(R.id.tvlicenceno)
        val tvlicenceexpdt = dialog_changepass.findViewById<TextView>(R.id.tvlicenceexpdt)
        tv_customer_name.text = "${licenceData.licence_data.Name} ${licenceData.licence_data.lName}"

        tvpatientdob.text = "${CUC.getdatefromoneformattoAnother("yyyy-MM-dd", "MM-dd-yyyy", licenceData.licence_data.patient_dob)}"
        tvlicenceno.text = "${licenceData.licence_data.driving_license}"
        tvaddress.text = "${licenceData.licence_data.Address}"
        tvlicenceexpdt.text = "${CUC.getdatefromoneformattoAnother("yyyy-MM-dd", "MM-dd-yyyy", licenceData.licence_data.Timestamp)}"
        cv_validate.setOnClickListener {
           // callAppqValidateQueue()
        }
    }

    fun callEmployeeQueueList() {
        Log.i("Result", "callEmployeeSalesReport()")
        try {
            if (mView!!.mSwipeRefreshLayout != null) {
                mView!!.mSwipeRefreshLayout.isRefreshing = false
            }
            if (!activity!!.isFinishing) {
                mDialog = CUC.createDialog(activity!!)
                val apiService = RequetsInterface.create()
                val parameterMap = HashMap<String, String>()
                parameterMap["api_key"] = "${Preferences.getPreference(activity!!, "API_KEY")}"
                if (logindata != null) {
                    parameterMap["employee_id"] = "${logindata.user_id}"
                    parameterMap["store_id"] = "${logindata.user_store_id}"
                }
                parameterMap["queue_date"] = "$selectDate"
                parameterMap["queue_type"] = "$queue_type"
                println("aaaaaaaaa queue  params  "+parameterMap)
                Log.i("Result", "$parameterMap")
                CompositeDisposable().add(apiService.callEmployeeQueueList(parameterMap)
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.io())
                        .subscribe({ result ->
                            if (mDialog != null && mDialog.isShowing)
                                mDialog.dismiss()
                            if (result != null) {
                                println("aaaaaaaaa queue  result  "+result)
                               Log.i("Result", "" + result.toString())
                                mView!!.tv_nopatient.visibility = View.GONE
                                if (queue_type == "0") {
                                    mView!!.ll_close.visibility = View.VISIBLE
                                } else {
                                    mView!!.ll_close.visibility = View.GONE
                                }
                                if (result.status == "0") {
                                    list.clear()
                                    list.addAll(result.queue_list)
                                    productAdapter.changeQType(queue_type)
                                    productAdapter.notifyDataSetChanged()
                                    if (!activity!!.isFinishing)
                                        CUC.displayToast(activity!!, result.show_status, result.message)
                                } else if (result.status == "10") {
                                    apiClass!!.callApiKey(EMPLOYEEQUEUELIST)
                                } else {
                                    mView!!.tv_nopatient.visibility = View.VISIBLE
                                    try {
                                        if (!activity!!.isFinishing) {
                                           // Toast.makeText(context, "" + result.message, Toast.LENGTH_SHORT).show()
                                            CUC.displayToast(activity!!, result.show_status, result.message)
                                            list.clear()
                                            list.addAll(list)
                                            productAdapter.changeQType(queue_type)
                                            productAdapter.notifyDataSetChanged()
                                        }
                                    } catch (e: Exception) {
                                        e.printStackTrace()
                                    }
                                }
                            } else {
                                mView!!.tv_nopatient.visibility = View.VISIBLE
                                if (!activity!!.isFinishing) {
                                 //
                                    //   CUC.displayToast(activity!!, "0", getString(R.string.connection_to_database_failed))
                                    list.clear()
                                    list.addAll(list)
                                    productAdapter.changeQType(queue_type)
                                    productAdapter.notifyDataSetChanged()
                                }

                            }

                        }, { error ->
                            if (!activity!!.isFinishing) {
                                mView!!.tv_nopatient.visibility = View.VISIBLE
                                list.clear()
                                list.addAll(list)
                                productAdapter.changeQType(queue_type)
                                productAdapter.notifyDataSetChanged()
                                if (mDialog != null && mDialog.isShowing)
                                    mDialog.dismiss()
                                CUC.displayToast(activity!!, "0", getString(R.string.connection_to_database_failed))
                            }
                            error.printStackTrace()
                        })
                )
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun callEmployeeQueueClose() {
        mDialog = CUC.createDialog(activity!!)
        val apiService = RequetsInterface.create()
        val parameterMap = HashMap<String, String>()
        parameterMap["api_key"] = "${Preferences.getPreference(activity!!, "API_KEY")}"
        parameterMap["queue_id"] = "$queue_remove_id"
        Log.i("Result", "parameterMap : $parameterMap")
        CompositeDisposable().add(apiService.callEmployeeQueueClose(parameterMap)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe({ result ->
                    if (mDialog != null && mDialog.isShowing)
                        mDialog.dismiss()
                    if (result != null) {
                        Log.i("Result", "" + result.toString())
                        if (result.status == "0") {
                            callEmployeeQueueList()
                        } else if (result.status == "10") {
                            apiClass!!.callApiKey(EMPLOYEEQUEUECLOSE)
                        } else {
                            CUC.displayToast(activity!!, result.show_status, result.message)
                        }
                    } else {
                       // CUC.displayToast(activity!!, "0", getString(R.string.connection_to_database_failed))
                    }
                }, { error ->
                    if (mDialog != null && mDialog.isShowing)
                        mDialog.dismiss()
                    CUC.displayToast(activity!!, "0", getString(R.string.connection_to_database_failed))

                    error.printStackTrace()
                })
        )
    }

    lateinit var levelDialog: AlertDialog
    var QueueStatus = "0"
    fun dialogQueueStatus() {
        QueueStatus = "1"
        val items = arrayOf("Pushed to Sales", "Quick Order")

        val builder = AlertDialog.Builder(activity!!)
        builder.setTitle("Queue Status")
        builder.setPositiveButton("OK", object : DialogInterface.OnClickListener {
            override fun onClick(p0: DialogInterface?, p1: Int) {
                if (QueueStatus == "") {
                    CUC.displayToast(activity!!, "0", "Please select Queue Status")
                    dialogQueueStatus()
                } else {
                    callAppqValidateQueuePush()
                }
            }
        })
        builder.setSingleChoiceItems(items, 0, object : DialogInterface.OnClickListener {
            override fun onClick(p0: DialogInterface?, p1: Int) {
                when (p1) {
                    0 -> {
                        QueueStatus = "1"
                    }
                    1 -> {
                        QueueStatus = "2"
                    }
                    2 -> {
                        QueueStatus = "3"
                    }
                }
            }
        })
        levelDialog = builder.create()
        levelDialog.show()
    }

    fun callAppqValidateQueuePush() {
        mDialog = CUC.createDialog(activity!!)
        val apiService = RequetsInterface.create()
        val parameterMap = HashMap<String, String>()
        parameterMap["api_key"] = "${Preferences.getPreference(activity!!, "API_KEY")}"
        parameterMap["queue_id"] = "$queue_id"
        if (logindata != null) {
            parameterMap["employee_id"] = "${logindata.user_id}"
            parameterMap["store_id"] = "${logindata.user_store_id}"
        }
        parameterMap["queue_status"] = "$QueueStatus"
        Log.i("Result", "parameterMap : $parameterMap")
        CompositeDisposable().add(apiService.callAppqValidateQueuePush(parameterMap)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe({ result ->
                    if (mDialog != null && mDialog.isShowing)
                        mDialog.dismiss()
                    if (result != null) {
                        Log.i("Result", "" + result.toString())
                        if (result.status == "0") {
                            Handler().postDelayed({
                                callEmployeeQueueList()
                            }, 200)

                            if ("$QueueStatus" == "2") {
                                val gson = Gson()
                                val myJson = gson.toJson(logindata)
                                val QueueData = Gson().toJson(result.queue)
                                //val goIntent = Intent(activity!!, SalesCategoryActivity::class.java)
                                val goIntent = Intent(activity!!, SalesActivity::class.java)
                                goIntent.putExtra("queue_patient_id", "$patient_id")
                                goIntent.putExtra("queue_id", "${result.queue.queue_id}")
                                goIntent.putExtra("QueueData", "$QueueData")
                                goIntent.putExtra("logindata",myJson)
                                activity!!.startActivityForResult(goIntent, 664)
                            }
                        } else if (result.status == "10") {
                            apiClass!!.callApiKey(APPQVALIDATEQUEUEPUSH)
                        } else {
                            CUC.displayToast(activity!!, result.show_status, result.message)
                        }
                    } else {
                     //   CUC.displayToast(activity!!, "0", getString(R.string.connection_to_database_failed))
                    }
                }, { error ->
                    if (mDialog != null && mDialog.isShowing)
                        mDialog.dismiss()
                    CUC.displayToast(activity!!, "0", getString(R.string.connection_to_database_failed))

                    error.printStackTrace()
                })
        )
    }

    fun callAppqValidateQueueRemoveStatus() {
        mDialog = CUC.createDialog(activity!!)
        val apiService = RequetsInterface.create()
        val parameterMap = HashMap<String, String>()
        parameterMap["api_key"] = "${Preferences.getPreference(activity!!, "API_KEY")}"
        parameterMap["queue_id"] = "$queue_remove_id"
        if (logindata != null) {
            parameterMap["employee_id"] = "${logindata.user_id}"
            parameterMap["store_id"] = "${logindata.user_store_id}"
        }
        parameterMap["queue_status"] = "3"
        Log.i("Result", "parameterMap : $parameterMap")
        CompositeDisposable().add(apiService.callAppqValidateQueuePush(parameterMap)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe({ result ->
                    if (mDialog != null && mDialog.isShowing)
                        mDialog.dismiss()
                    if (result != null) {
                        Log.i("Result", "" + result.toString())
                        if (result.status == "0") {
                            Handler().postDelayed({
                                callEmployeeQueueList()
                            }, 200)


                            if ("$QueueStatus" == "2") {
                                val gson = Gson()
                                val myJson = gson.toJson(logindata)
                                val QueueData = Gson().toJson(result.queue)
                               // val goIntent = Intent(activity!!, SalesCategoryActivity::class.java)
                                val goIntent = Intent(activity!!, SalesActivity::class.java)
                                goIntent.putExtra("queue_patient_id", "$patient_id")
                                goIntent.putExtra("queue_id", "${result.queue.queue_id}")
                                goIntent.putExtra("QueueData", "$QueueData")
                                goIntent.putExtra("logindata",myJson)
                                activity!!.startActivityForResult(goIntent, 664)
                            }
                        } else if (result.status == "10") {
                            apiClass!!.callApiKey(APPQVALIDATEQUEUEPUSH)
                        } else {
                            CUC.displayToast(activity!!, result.show_status, result.message)
                        }
                    } else {
                     //   CUC.displayToast(activity!!, "0", getString(R.string.connection_to_database_failed))
                    }
                }, { error ->
                    if (mDialog != null && mDialog.isShowing)
                        mDialog.dismiss()
                    CUC.displayToast(activity!!, "0", getString(R.string.connection_to_database_failed))

                    error.printStackTrace()
                })
        )
    }

    fun callEmployeeSalesQueuePatient() {

        Log.d("SASASASASASAS","SSSSSSSSSS")
        mDialog = CUC.createDialog(activity!!)
        val apiService = RequetsInterface.create()
        val parameterMap = HashMap<String, String>()
        parameterMap["api_key"] = "${Preferences.getPreference(activity!!, "API_KEY")}"
        parameterMap["queue_id"] = "$queue_id"
        Log.i("Result", "parameterMap : $parameterMap")
        CompositeDisposable().add(apiService.callEmployeeSalesQueuePatient(parameterMap)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe({ result ->
                    if (mDialog != null && mDialog.isShowing)
                        mDialog.dismiss()
                    if (result != null) {
                        Log.d("Result2222", "" + result.toString())
                        if (result.status == "0") {
                            val gson = Gson()
                            val myJson = gson.toJson(logindata)
                            val QueueData = Gson().toJson(result.queue)
                           // val goIntent = Intent(activity!!, SalesCategoryActivity::class.java)
                            val goIntent = Intent(activity!!, SalesActivity::class.java)
                            goIntent.putExtra("queue_patient_id", "$patient_id")
                            goIntent.putExtra("queue_id", "${result.queue.queue_id}")
                            goIntent.putExtra("patientImage",result.patient.patient_image)
                            goIntent.putExtra("QueueData", "$QueueData")
                            goIntent.putExtra("request_code", 664)
                            goIntent.putExtra("logindata",myJson)
                            activity!!.startActivityForResult(goIntent, 664)
                        } else if (result.status == "10") {
                            apiClass!!.callApiKey(EMPLOYEESALESQUEUEPATIENT)
                        } else {
                            CUC.displayToast(activity!!, result.show_status, result.message)
                        }
                    } else {
                      //  CUC.displayToast(activity!!, "0", getString(R.string.connection_to_database_failed))
                    }
                }, { error ->
                    if (mDialog != null && mDialog.isShowing)
                        mDialog.dismiss()
                    CUC.displayToast(activity!!, "0", getString(R.string.connection_to_database_failed))

                    error.printStackTrace()
                })
        )
    }


    fun callAppqValidateQueue(data: GetQueueList) {
        mDialog = CUC.createDialog(activity!!)
        val apiService = RequetsInterface.create()
        val parameterMap = HashMap<String, String>()
        parameterMap["api_key"] = "${Preferences.getPreference(activity!!, "API_KEY")}"
        parameterMap["queue_id"] = "$queue_id"
        if (logindata != null) {
            parameterMap["employee_id"] = "${logindata.user_id}"
            parameterMap["store_id"] = "${logindata.user_store_id}"
        }
        Log.i("Result", "parameterMap : $parameterMap")
        println("aaaaaaaaaa   dataqueue  "+parameterMap)
        CompositeDisposable().add(apiService.callAppqValidateQueue(parameterMap)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe({ result ->
                    if (mDialog != null && mDialog.isShowing)
                        mDialog.dismiss()
                    if (result != null) {
                        Log.i("Result", "" + result.toString())
                        println("aaaaaaaaaa   dataqueue  Result "+result.toString())
                        if (result.status == "0") {
                            try {
                                if (dialog_changepass != null && dialog_changepass.isShowing)
                                    dialog_changepass.dismiss()
                            } catch (e: Exception) {
                                e.printStackTrace()


                            }

                            /*
                            * 1 For New customer registration
                            * 2 For Patient Need to add in our company
                            * 3 For Patient Already with us just push to sales
                            * */

                            patient_id = "0"
                            driving_license = ""
                            if (result.patient_status == "1") {
                                val intent = Intent(activity!!, CreateAccountActivity::class.java)
                                intent.putExtra("driving_license", "${licenceData.licence_data.driving_license}")
                                intent.putExtra("dob", "${CUC.getdatefromoneformattoAnother("yyyy-MM-dd", "MM-dd-yyyy", "${licenceData.licence_data.patient_dob}")}")
                                intent.putExtra("Timestamp", "${CUC.getdatefromoneformattoAnother("yyyy-MM-dd", "MM-dd-yyyy", "${licenceData.licence_data.Timestamp}")}")
                                intent.putExtra("Address", "${licenceData.licence_data.Address}")
                                intent.putExtra("City", "${licenceData.licence_data.City}")
                                intent.putExtra("state_code", "${licenceData.licence_data.state_code}")
                                intent.putExtra("Name", "${licenceData.licence_data.Name}")
                                intent.putExtra("lName", "${licenceData.licence_data.lName}")
                                intent.putExtra("Postal", "${licenceData.licence_data.Postal}")
                                intent.putExtra("queue_id", "$queue_id")
                                //intent.putExtra("status", "2")
                                startActivityForResult(intent, 120)
                            } else if (result.patient_status == "2") {
                                val builder = AlertDialog.Builder(activity!!)
                                builder.setMessage("${result.message}")
                                builder.setPositiveButton("Yes") { dialog, which ->
                                    dialog.dismiss()
                                    patient_id = "${result.patient_id}"
                                    driving_license = "${licenceData.licence_data.driving_license}"
                                    callEmployeePatientAssignCompany()

                                }
                                builder.setNegativeButton("No") { dialog, which -> dialog.dismiss() }
                                val alert = builder.create()
                                alert.show()
                            } else if (result.patient_status == "3") {
                                if (chaeckQueue.equals("InQueue")) {
                                    dialogQueueStatus()
                                } else {
                                    patient_id = "${result.patient_id}"
                                    val gson = Gson()
                                    val myJson = gson.toJson(logindata)
                                    val QueueData = Gson().toJson(data)
                                    //val QueueData = Gson().toJson(result.queue)
                                   // val goIntent = Intent(activity!!, SalesCategoryActivity::class.java)
                                    val goIntent = Intent(activity!!, SalesActivity::class.java)
                                    goIntent.putExtra("queue_patient_id", patient_id)
                                    goIntent.putExtra("queue_id", "${queue_id}")
                                    goIntent.putExtra("logindata",myJson)
                                    goIntent.putExtra("QueueData", "$QueueData")
                                    activity!!.startActivityForResult(goIntent, 664)
                                }
                                //  dialogQueueStatus()
                            }else{
                                println("aaaaaaaaaaa   else  ")
                            }
//                            }else {
//                               /* chaeckQueue="LeftUnassisted"
//                                chaeckQueue="InQueue"*/
//                                /*if(chaeckQueue.equals("InQueue")){
//                                    dialogQueueStatus()
//                                }*/
//                               /* if (result.patient_status == "3") {
//                                    dialogQueueStatus()
//                                }*/
//                            }

                        } else if (result.status == "10") {
                            apiClass!!.callApiKey(APPQVALIDATEQUEUE)
                        } else {
                            CUC.displayToast(activity!!, result.show_status, result.message)
                        }
                    } else {
                     //   CUC.displayToast(activity!!, "0", getString(R.string.connection_to_database_failed))
                    }
                }, { error ->
                    if (mDialog != null && mDialog.isShowing)
                        mDialog.dismiss()
                    CUC.displayToast(activity!!, "0", getString(R.string.connection_to_database_failed))

                    error.printStackTrace()
                })
        )
    }

    fun callEmployeePatientAssignCompany() {
        if (CUC.isNetworkAvailablewithPopup(activity!!)) {
            mDialog = CUC.createDialog(activity!!)
            val apiService = RequetsInterface.Factory.create()
            val parameterMap = HashMap<String, String>()
            parameterMap["api_key"] = "${Preferences.getPreference(activity!!, "API_KEY")}"
            parameterMap["patient_licence_no"] = "$driving_license"
            parameterMap["patient_id"] = "$patient_id"
            if (logindata != null) {
                parameterMap["store_id"] = "${logindata.user_store_id}"
            }
            Log.i("Result", "parameterMap : $parameterMap")
            CompositeDisposable().add(apiService.callEmployeePatientAssignCompany(parameterMap)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe({ result ->
                        if (mDialog != null && mDialog.isShowing)
                            mDialog.dismiss()
                        Log.i("Result", "" + result.toString())
                        if (result != null) {
                            if (result.status == "0") {

                                CUC.displayToast(activity!!, result.show_status, result.message)
                                if (chaeckQueue.equals("InQueue")) {
                                    dialogQueueStatus()
                                }else{
                                    val gson = Gson()
                                    val myJson = gson.toJson(logindata)
                                   // val goIntent = Intent(activity!!, SalesCategoryActivity::class.java)
                                    val goIntent = Intent(activity!!, SalesActivity::class.java)
                                    goIntent.putExtra("queue_patient_id","$patient_id")
                                    goIntent.putExtra("queue_id", "${queue_id}")
                                    goIntent.putExtra("logindata",myJson)
                                    //goIntent.putExtra("QueueData", "$QueueData")
                                    activity!!.startActivityForResult(goIntent, 664)
                                }

                            } else if (result.status == "10") {
                                apiClass!!.callApiKey(EMPLOYEEPATIENTASSIGNCOMPANY)
                            } else {
                                CUC.displayToast(activity!!, result.show_status, result.message)
                            }
                        } else {
                           // CUC.displayToast(activity!!, "0", getString(R.string.connection_to_database_failed))
                        }
                    }, { error ->
                        if (mDialog != null && mDialog.isShowing)
                            mDialog.dismiss()
                        CUC.displayToast(activity!!, "0", getString(R.string.connection_to_database_failed))
                        error.printStackTrace()
                    })
            )
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            120 -> if (resultCode == Activity.RESULT_FIRST_USER) {
                if (data != null && data.hasExtra("queue_id")) {
                    queue_id = data.getStringExtra("queue_id")
                    if (chaeckQueue.equals("InQueue")) {
                        dialogQueueStatus()
                    } else {
                        val gson = Gson()
                        val myJson = gson.toJson(logindata)
                      //  val QueueData = Gson().toJson(result.queue)
                       // val goIntent = Intent(activity!!, SalesCategoryActivity::class.java)
                        val goIntent = Intent(activity!!, SalesActivity::class.java)
                        goIntent.putExtra("queue_patient_id", "$patient_id")
                        goIntent.putExtra("queue_id", "${queue_id}")
                        goIntent.putExtra("logindata",myJson)
                        //goIntent.putExtra("QueueData", "$QueueData")
                        activity!!.startActivityForResult(goIntent, 664)
                    }
                  //  dialogQueueStatus()
                }
            }
            664 -> if (resultCode == Activity.RESULT_OK) {
                callEmployeeQueueList()
            }
            555 -> if (resultCode == Activity.RESULT_OK) {
                if (queueStatus != null) {
                    queue_type = "0"

                    var mobileappAdapter = ArrayAdapter<String>(mView!!.context, R.layout.spinner_item, R.id.dropdown_item, queueStatus)
                    mView!!.bs_status.setText("${resources.getStringArray(R.array.queue_status)[0]}")
                    mView!!.bs_status.setAdapter(mobileappAdapter)
                    selectDate = "${Calendar.getInstance().get(Calendar.YEAR)}-" +
                            "${(if (Calendar.getInstance().get(Calendar.MONTH) + 1 >= 10) Calendar.getInstance().get(Calendar.MONTH) + 1 else "0" + (Calendar.getInstance().get(Calendar.MONTH) + 1))}-" +
                            "${(if (Calendar.getInstance().get(Calendar.DAY_OF_MONTH) >= 10) Calendar.getInstance().get(Calendar.DAY_OF_MONTH) else "0${Calendar.getInstance().get(Calendar.DAY_OF_MONTH)}")}"
                    //bs_status.setText(queueStatus!![0])
                    callEmployeeQueueList()
                }

            }

        }
    }

    inner class ProductAdapter(val list: ArrayList<GetQueueList>, val context: Context, val onClick: onClickListener) : RecyclerView.Adapter<ProductAdapter.ViewHolder>() {

        var queue_type = "1"

        fun changeQType(qtype: String) {
            queue_type = qtype
        }

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            holder!!.bindItems(list[position], context, onClick, queue_type)
        }

        override fun getItemCount(): Int {
            return list.size
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            val v = LayoutInflater.from(parent!!.context).inflate(R.layout.queue_item, parent, false)
            return ViewHolder(v)
        }

        inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
            val tv_name: TextView = itemView.findViewById(R.id.tv_name)
            //   val tv_mobilenumber: TextView = itemView.findViewById(R.id.tv_mobilenumber)
            // val tv_phonenumber: TextView = itemView.findViewById(R.id.tv_phonenumber)
            // val tv_patienttype: TextView = itemView.findViewById(R.id.tv_patienttype)
            //   val tv_validate:TextView=itemView.findViewById(R.id.tv_validate)
            val tv_timing: TextView = itemView.findViewById(R.id.tv_timing)
            //   val img_delete:ImageView=itemView.findViewById(R.id.img_delete)
            val tv_mobileapp: TextView = itemView.findViewById(R.id.tv_mobileapp)
            //   val tv_loyaltygroup: TextView = itemView.findViewById(R.id.tv_loyaltygroup)
            //  val tv_close: TextView = itemView.findViewById(R.id.tv_close)
            val ll_item_close: LinearLayout = itemView.findViewById(R.id.ll_item_close)
            val ll_queuemain: TextView = itemView.findViewById(R.id.ll_queuemain)
            val img_profile: ImageView = itemView.findViewById(R.id.img_profile)
            val ll_remove: LinearLayout = itemView.findViewById(R.id.ll_remove)

            fun bindItems(data: GetQueueList, context: Context, onClick: onClickListener, queue_type: String) {
                tv_name.text = CUC.getCapsSentences("${data.first_name} ${data.last_name}")
                tv_mobileapp.text = "${data.queue_desc}"
                tv_timing.text = "${data.queue_minutes}"
                var minutes = "${data.queue_minutes}".toInt()
                if (minutes >= 30) {
                    tv_timing.setTextColor(context.resources.getColor(R.color.redeem_color))
                } else if (minutes >= 20) {
                    tv_timing.setTextColor(context.resources.getColor(R.color.roti))
                } else {
                    tv_timing.setTextColor(context.resources.getColor(R.color.yellowlight))
                }
                Glide.with(context) // safer!
                        .asBitmap()
                        .load(data.patient_image)
                        .into(img_profile)

                ll_remove.setOnClickListener {
                    queue_remove_id = data.queue_id
                 //   Log.d("ASASA111"," = "+data.queue_id)
                    if (queue_type == "3") {
                        val builder = AlertDialog.Builder(activity!!)
                        builder.setMessage("Are you sure you want to delete patient?")
                        builder.setPositiveButton("Yes") { dialog, which ->

                            callEmployeeQueueClose()
                            dialog.dismiss()
                        }
                        builder.setNegativeButton("No") { dialog, which -> dialog.dismiss() }
                        val alert = builder.create()
                        alert.show()

                    }else{
                        callAppqValidateQueueRemoveStatus()
                    }

                }

                if (queue_type == "3") {
                    ll_remove.visibility = View.VISIBLE
                } else {
                    ll_remove.visibility = View.VISIBLE
                }
                //  tv_mobilenumber.text =  "${data.queue_number}"
                //  tv_phonenumber.text = "${data.patient_licence_number}"
//                if (data.patient_mmmp_licence == "1") {
//                    tv_patienttype.text = "YES"
//                } else {
//                    tv_patienttype.text = "NO"
//                }
//                tv_mobileapp.text = "${data.queue_desc}"
//                tv_loyaltygroup.text = "${data.queue_minutes}"

                if (queue_type == "0") {
                    ll_item_close.visibility = View.VISIBLE
                } else if(queue_type=="1"){
                    ll_item_close.visibility = View.VISIBLE
                } else {
                    ll_item_close.visibility = View.VISIBLE
                }
                ll_queuemain.setOnClickListener {
                    onClick.callClick(data)
                }
            }
        }
    }

    interface onClickListener {
        fun callClick(data: GetQueueList)
    }

    fun callCheckStoreOpen() {
        val terminalid = appSettingData?.terminal_id
        if (CUC.isNetworkAvailablewithPopup(activity!!)) {
            mDialog = CUC.createDialog(activity!!)
            val apiService = RequetsInterface.Factory.create()
            val parameterMap = HashMap<String, String>()
            parameterMap["api_key"] = "${Preferences.getPreference(activity!!, "API_KEY")}"
            parameterMap["terminal_id"] = "${terminalid}"
            if (logindata != null) {
                parameterMap["emp_id"] = "${logindata.user_id}"
                parameterMap["store_id"] = "${logindata.user_store_id}"
            }
            println("aaaaaaaaaaa  storecheck  "+parameterMap)
            Log.i("aaaa patient Result", "parameterMap : $parameterMap")
            CompositeDisposable().add(apiService.callCheckStoreOpen(parameterMap)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe({ result ->
                        if (mDialog != null && mDialog.isShowing)
                            mDialog.dismiss()
                        println("aaaaaaaaaa  storecheck  Result  "+result.toString())
                        Log.i("api Result", "" + result.toString())
                        if (result != null) {
                            if (result.store_chk == "0") {
                                // CUC.displayToast(activity!!, result.show_status, result.message)
                                showdialog(result.message)
                            } else {
                                storeopen="1"
                                calltillCheck()
                                //CUC.displayToast(activity!!, result.show_status, result.message)
                            }
                        } else {
                            // CUC.displayToast(activity!!, "0", getString(R.string.connection_to_database_failed))
                        }
                    }, { error ->
                        if (mDialog != null && mDialog.isShowing)
                            mDialog.dismiss()
                        CUC.displayToast(activity!!, "0", getString(R.string.connection_to_database_failed))
                        error.printStackTrace()
                    })
            )
        }
    }

    fun calltillCheck() {
        val terminalid = appSettingData?.terminal_id
        if (CUC.isNetworkAvailablewithPopup(activity!!)) {
            mDialog = CUC.createDialog(activity!!)
            val apiService = RequetsInterface.Factory.create()
            val parameterMap = HashMap<String, String>()
            parameterMap["api_key"] = "${Preferences.getPreference(activity!!, "API_KEY")}"
            parameterMap["terminal_id"] = "${terminalid}"
            if (logindata != null) {
                parameterMap["emp_id"] = "${logindata.user_id}"
                parameterMap["store_id"] = "${logindata.user_store_id}"
            }
            println("aaaaaaaaaaa  tillcheck  "+parameterMap)
            Log.i("aaaa patient Result", "parameterMap : $parameterMap")
            CompositeDisposable().add(apiService.callTillOpenCheck(parameterMap)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe({ result ->
                        if (mDialog != null && mDialog.isShowing)
                            mDialog.dismiss()
                        println("aaaaaaaaaa  tillcheck  Result  "+result.toString())
                        Log.i("api Result", "" + result.toString())
                        if (result != null) {
                            if (result.till_check == "0") {

                                //CUC.displayToast(activity!!, result.show_status, result.message)
                                showdialog(result.message)
                            } else {
                                tillopen="1"
                                paging = 0
                                callEmployeeQueueList()
                                // CUC.displayToast(activity!!, result.show_status, result.message)
                            }
                        } else {
                            // CUC.displayToast(activity!!, "0", getString(R.string.connection_to_database_failed))
                        }
                    }, { error ->
                        if (mDialog != null && mDialog.isShowing)
                            mDialog.dismiss()
                        CUC.displayToast(activity!!, "0", getString(R.string.connection_to_database_failed))
                        error.printStackTrace()
                    })
            )
        }
    }

    fun showdialog(msg:String) {
        val builder1 = android.app.AlertDialog.Builder(context)
        builder1.setMessage("" + msg)
        builder1.setCancelable(true)

        builder1.setPositiveButton(
                "Ok"
        ) { dialog, id ->
            dialog.cancel()

        }

        val alert11 = builder1.create()
        alert11.show()
    }

}