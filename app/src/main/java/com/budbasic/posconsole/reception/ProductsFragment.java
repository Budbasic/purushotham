package com.budbasic.posconsole.reception;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.TextView;
import android.widget.Toast;

import com.budbasic.customer.global.Preferences;
import com.budbasic.global.CUC;
import com.budbasic.posconsole.R;
import com.budbasic.posconsole.adapter.PatientAdapter;
import com.budbasic.posconsole.adapter.ProductAdapter;
import com.budbasic.posconsole.dialogs.AddProductDialog;
import com.budbasic.posconsole.global.Apppreference;
import com.budbasic.posconsole.webservice.RequetsInterface;
import com.google.gson.Gson;
import com.kotlindemo.model.GetCartData;
import com.kotlindemo.model.GetData;
import com.kotlindemo.model.GetEmployeeAppSetting;
import com.kotlindemo.model.GetEmployeeLogin;
import com.kotlindemo.model.GetEmployeePatientCart;
import com.kotlindemo.model.GetEmployeeSalesProducts;
import com.kotlindemo.model.GetEmployeeSearchProducts;
import com.kotlindemo.model.GetPatient;
import com.kotlindemo.model.GetPaymentTypes;
import com.kotlindemo.model.GetProducts;
import com.kotlindemo.model.GetQueueList;
import com.kotlindemo.model.GetSalesProduct;
import com.kotlindemo.model.GetStatus;
import com.kotlindemo.model.getCartadd;
import com.starmicronics.starmgsio.ConnectionInfo;
import com.starmicronics.starmgsio.Scale;
import com.starmicronics.starmgsio.ScaleCallback;
import com.starmicronics.starmgsio.ScaleData;
import com.starmicronics.starmgsio.ScaleSetting;
import com.starmicronics.starmgsio.StarDeviceManager;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

@SuppressLint("ValidFragment")
public class ProductsFragment extends Fragment implements ProductAdapter.ContactsAdapterListener, SalesActivity.FragmentInterface {

    int position;
    private TextView textView;
    List<GetData> cateList;
    GetEmployeeLogin logindata;
    String queue_patient_id,queue_id, mainCartId,payment_path,Store_sign_required,image_path,cat_name;
    private RecyclerView product_recycleview;
    ProductAdapter productAdapter;
    private List<GetProducts> searchdataArrayList;
    public Dialog mDialog;
    private SearchView search_product;
    private CircleImageView ivpatientcart;
    private List<GetCartData> cartlist;
    private List<GetSalesProduct> productlist,categorylist;
    private Gson gson;
    GetEmployeeAppSetting mBAsis;
    GetQueueList queueList;
    private Scale mScale;
    private String weight;
    GetStatus getstatus;
    GetSalesProduct getProductsa;
    private Apppreference apppreference;
    AddProductDialog addProductDialog1;
    List<GetPaymentTypes> paymentTypeList = new ArrayList<GetPaymentTypes>();
    @SuppressLint("ValidFragment")
    public ProductsFragment(List<GetData> cateList, GetEmployeeLogin logindata, String queue_patient_id, String queue_id,
                            List<GetCartData> list,String image_path,String cat_name, GetQueueList queueList) {
        this.cateList=cateList;
        this.logindata=logindata;
        this.queue_patient_id=queue_patient_id;
        this.queue_id=queue_id;
        this.cartlist=list;
        this.image_path=image_path;
        this.cat_name=cat_name;
        this.queueList=queueList;

    }
    public static Fragment getInstance(int position, List<GetData> cateList, GetEmployeeLogin logindata, String queue_patient_id,
                                       String queue_id, List<GetCartData> list, String imagePath, String cat_name, GetQueueList queueList) {
        Bundle bundle = new Bundle();
        bundle.putInt("pos", position);
        ProductsFragment tabFragment = new ProductsFragment(cateList,logindata,queue_patient_id,queue_id,list,imagePath,cat_name,queueList);
        tabFragment.setArguments(bundle);
        return tabFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        position = getArguments().getInt("pos");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v= inflater.inflate(R.layout.fragment_counter, container, false);

        product_recycleview=v.findViewById(R.id.product_recycleview);
        search_product=v.findViewById(R.id.search_product);
        ivpatientcart=v.findViewById(R.id.ivpatientcart);

        gson=new Gson();
        product_recycleview.setLayoutManager(new GridLayoutManager(getContext(),1));
        searchdataArrayList=new ArrayList<>();
        productlist=new ArrayList<GetSalesProduct>();
        mBAsis = gson.fromJson(Preferences.INSTANCE.getPreference(getContext(), "AppSetting"), GetEmployeeAppSetting.class);

        Display display = getActivity().getWindowManager().getDefaultDisplay();
        DisplayMetrics outMetrics = new DisplayMetrics();
        display.getMetrics(outMetrics);

        float density  = getResources().getDisplayMetrics().density;
        float dpWidth  = outMetrics.widthPixels / density;
        float dpheight  = outMetrics.widthPixels / density;
        int columns = Math.round(dpWidth/700);
        System.out.println("aaaaaaaaaaaaa  width "+dpWidth+"   coloumns "+columns+"   dpheight  "+dpheight);

        product_recycleview.setLayoutManager(new GridLayoutManager(getActivity(),columns));
        apppreference=new Apppreference(getActivity());
        /*if(mScale == null) {
            System.out.println("aaaaaaaaaaa  scale not found ");
            String identifier = apppreference.getidentifier();
            ConnectionInfo.InterfaceType interfaceType = ConnectionInfo.InterfaceType.valueOf(apppreference.getinterfaceType());

            StarDeviceManager starDeviceManager = new StarDeviceManager(getContext());

            ConnectionInfo connectionInfo;

            switch (interfaceType) {
                default:
                case BLE:
                    connectionInfo = new ConnectionInfo.Builder()
                            .setBleInfo(identifier)
                            .build();
                    break;
                case USB:
                    connectionInfo = new ConnectionInfo.Builder()
                            .setUsbInfo(identifier)
                            .setBaudRate(1200)
                            .build();
                    break;
            }

            mScale = starDeviceManager.createScale(connectionInfo);
            mScale.connect(mScaleCallback);
        }*/
       /* if (position==0){

        }else {
            categorylist.clear();
            for (int k=0;k<productlist.size();k++){
                if (cat_name.equals(productlist.get(position).getCategory_name())){
                    categorylist.add(productlist.get(k));
                }
            }
        }*/

         //getEachproductlist(cateList.get(position).getCategory().getCategory_name());

        search_product.setImeOptions(EditorInfo.IME_FLAG_NO_EXTRACT_UI);
        search_product.setFocusable(false);
        search_product.setIconified(false);
        search_product.onActionViewExpanded();

        search_product.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                // filter recycler view when query submitted
                System.out.println("aaaaaaaaaaaaa  search  "+query);
                productAdapter.getFilter().filter(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {
                // filter recycler view when text is changed
                productAdapter.getFilter().filter(query);
                return false;
            }
        });
        return v;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //mBAsis = gson.fromJson(Preferences.INSTANCE.getPreference(getActivity(), "AppSetting"), GetEmployeeAppSetting.class);

       String Currency_value=mBAsis.getCurrency_value();
        productlist.clear();
        for (int i=0;i<cateList.size();i++){
            for (int j=0;j<cateList.get(i).getCategory().getProduct().size();j++){
                if (position==0){
                    productlist.add(cateList.get(i).getCategory().getProduct().get(j));
                }else {
                    if (cat_name.equals(cateList.get(i).getCategory().getCategory_name())){
                        productlist.add(cateList.get(i).getCategory().getProduct().get(j));
                    }
                }
            }
        }

        productAdapter =new ProductAdapter(getContext(),productlist,image_path,ProductsFragment.this,Currency_value);
        product_recycleview.setAdapter(productAdapter);
        System.out.println("aaaaaaaaaa  category name   "+cateList.get(position).getCategory().getCategory_name());

    }
    /*@Override
    public boolean getUserVisibleHint() {

        product_recycleview.setLayoutManager(new GridLayoutManager(getContext(),2));
        searchdataArrayList=new ArrayList<>();

        productAdapter =new ProductAdapter(getContext(),searchdataArrayList,cartlist);
        product_recycleview.setAdapter(productAdapter);
        System.out.println("aaaaaaaaaa  category name   "+cateList.get(position).getCategory().getCategory_name());
        getEachproductlist(cateList.get(position).getCategory().getCategory_name());
        return super.getUserVisibleHint();
    }*/

    /* @Override
        public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
            super.onViewCreated(view, savedInstanceState);
            product_recycleview=view.findViewById(R.id.product_recycleview);
            product_recycleview.setLayoutManager(new GridLayoutManager(getContext(),2));
        }*/
    public void getEachproductlist(final String productname){

       // mDialog = CUC.Companion.createDialog(getActivity());
        RequetsInterface apiService = RequetsInterface.Factory.create();
        Map parameterMap =new HashMap<String, String>();
        parameterMap.put("api_key", String.valueOf(Preferences.INSTANCE.getPreference((getContext()), "API_KEY")));

        if (logindata != null) {
            parameterMap.put("store_id", logindata.getUser_store_id());
            parameterMap.put("employee_id", logindata.getUser_id());
        }
        if (position!=0){
            parameterMap.put("product", productname);
        }
        System.out.println("aaaaaaaaaaa  parameterMap "+parameterMap);
        Log.i("aaaaaaaa   Result", "parameterMap : " + parameterMap);
        (new CompositeDisposable()).add(apiService.callEmployeeSearchProducts(parameterMap).
                observeOn(AndroidSchedulers.mainThread()).
                subscribeOn(Schedulers.io()).subscribe((Consumer)(new Consumer() {
            // $FF: synthetic method
            // $FF: bridge method
            public void accept(Object var1) {
                this.accept((GetEmployeeSearchProducts)var1);
            }

            public final void accept(GetEmployeeSearchProducts result) {
               // mDialog.cancel();
                if (result != null) {
                   // mDialog.cancel();
                    System.out.println("aaaaaaaaa  result  "+result.toString());
                    Log.i("aaaaaaa   Result", "" + result.toString());
                   /* if (Intrinsics.areEqual(result.getStatus(), "0")) {

                        searchdataArrayList.clear();
                        searchdataArrayList = result.getProducts();
                       // productAdapter.dataChanged(searchdataArrayList);

                        CUC.Companion.displayToast(getContext(), result.getShow_status(), result.getMessage());
                    } else if (Intrinsics.areEqual(result.getStatus(), "10")) {

                    } else {
                        Toast.makeText(getContext(), ""+result.getShow_status(), Toast.LENGTH_SHORT).show();
                        CUC.Companion.displayToast((Context)getContext(), result.getShow_status(), result.getMessage());
                    }*/
                } else {
                   // mDialog.cancel();
                }
            }
        }), (Consumer)(new Consumer() {
            // $FF: synthetic method
            // $FF: bridge method
            public void accept(Object var1) {
                this.accept((Throwable)var1);
            }

            public final void accept(Throwable error) {
               // mDialog.cancel();
                Toast.makeText(getContext(), getString(R.string.connection_to_database_failed), Toast.LENGTH_SHORT).show();
                error.printStackTrace();
                System.out.println("aaaaaaaa  error "+error.getMessage());
            }
        })));
    }


    @Override
    public void onContactSelected(GetSalesProduct getProducts) {
        System.out.println("aaaaaaaaaaaa   "+getProducts.getPacket_name());

        ((SalesActivity)getContext()).getStoresproducts(getProducts,ProductsFragment.this);
        //getStores(getProducts);
    }

    public void getStores(final GetSalesProduct getProducts){

        mDialog = CUC.Companion.createDialog(getActivity());
        RequetsInterface apiService = RequetsInterface.Factory.create();
        Map parameterMap =new HashMap<String, String>();
        parameterMap.put("api_key", String.valueOf(Preferences.INSTANCE.getPreference((getContext()), "API_KEY")));

        if (logindata != null) {
            parameterMap.put("store_id", logindata.getUser_store_id());
            parameterMap.put("cart_detail_id", "");
            parameterMap.put("product_id", getProducts.getProduct_id());
            parameterMap.put("packet_id", getProducts.getPacket_id());
        }

        System.out.println("aaaaaaaaaaa  parameterMap "+parameterMap);
        Log.i("aaaaaaaa   Result", "parameterMap : " + parameterMap);
        (new CompositeDisposable()).add(apiService.callEmployeeProductQty(parameterMap).
                observeOn(AndroidSchedulers.mainThread()).
                subscribeOn(Schedulers.io()).subscribe((Consumer)(new Consumer() {
            // $FF: synthetic method
            // $FF: bridge method
            public void accept(Object var1) {
                this.accept((GetStatus)var1);
            }

            public final void accept(GetStatus result) {
                mDialog.cancel();
                if (result != null) {
                    mDialog.cancel();
                    System.out.println("aaaaaaaaa  result  "+result.toString());
                    Log.i("aaaaaaa   Result", "" + result.toString());
                    if (result.getStatus().equalsIgnoreCase("0")) {
                      /* if (result.getInventory_qty().equals("0")){
                           Toast.makeText(getContext(), "Currently Products Not There", Toast.LENGTH_SHORT).show();
                           CUC.Companion.displayToast(getContext(), "Currently Products Not There", result.getMessage());
                       }else {*/
                      getstatus=result;
                        getProductsa=getProducts;

                            addProductDialog1=new AddProductDialog();
                           addProductDialog1.showDialog(getContext(),result,getProducts,ProductsFragment.this,"0");
                      // }
                        CUC.Companion.displayToast(getContext(), result.getShow_status(), result.getMessage());
                    } else {
                        Toast.makeText(getContext(), ""+result.getShow_status(), Toast.LENGTH_SHORT).show();
                        CUC.Companion.displayToast((Context)getContext(), result.getShow_status(), result.getMessage());
                    }
                } else {
                    mDialog.cancel();
                }
            }
        }), (Consumer)(new Consumer() {
            // $FF: synthetic method
            // $FF: bridge method
            public void accept(Object var1) {
                this.accept((Throwable)var1);
            }

            public final void accept(Throwable error) {
                mDialog.cancel();
                Toast.makeText(getContext(), getString(R.string.connection_to_database_failed), Toast.LENGTH_SHORT).show();
                error.printStackTrace();
                System.out.println("aaaaaaaa  error "+error.getMessage());
            }
        })));
    }


    public void sendToCartea(final GetSalesProduct getProducts, Double value, Float discount, int i){

        ((SalesActivity)getContext()).sendToCartea(getProducts,value,discount,i);
      /*  mDialog = CUC.Companion.createDialog(getActivity());
        RequetsInterface apiService = RequetsInterface.Factory.create();
        Map parameterMap =new HashMap<String, String>();
        parameterMap.put("api_key", String.valueOf(Preferences.INSTANCE.getPreference((getContext()), "API_KEY")));

        if (logindata != null) {
            parameterMap.put("store_id", logindata.getUser_store_id());
            parameterMap.put("employee_id", logindata.getUser_id());
        }
        parameterMap.put("package_id",getProducts.getPacket_id());
        if (queue_patient_id.equals("0")){
            parameterMap.put("patient_id",queueList.getQueue_patient_id());
        }else {
            parameterMap.put("patient_id",queue_patient_id);
        }
        if (i==1){
            parameterMap.put("percentage_discount", discount);
        }else if (i==2){
            parameterMap.put("fixed_discount", discount);
        }

        parameterMap.put("product_id", getProducts.getProduct_id());
        parameterMap.put("category_id", getProducts.getProduct_cat_id());
        parameterMap.put("package_id", getProducts.getPacket_id());
        parameterMap.put("qty", ""+value);
        parameterMap.put("terminal_id", mBAsis.getTerminal_id());
        parameterMap.put("emp_lat", Preferences.INSTANCE.getPreference(getContext(), "latitude"));
        parameterMap.put("emp_log", Preferences.INSTANCE.getPreference(getContext(), "longitude"));
        parameterMap.put("queue_id", queue_id);

        System.out.println("aaaaaaaaaaa  parameterMap add cart "+parameterMap);
        Log.i("aaaaaaaa   Result", "parameterMap : " + parameterMap);
        (new CompositeDisposable()).add(apiService.callEmployeeSalesAddcart(parameterMap).
                observeOn(AndroidSchedulers.mainThread()).
                subscribeOn(Schedulers.io()).subscribe((Consumer)(new Consumer() {
            // $FF: synthetic method
            // $FF: bridge method
            public void accept(Object var1) {
                this.accept((GetEmployeeSalesProducts)var1);
            }

            public final void accept(GetEmployeeSalesProducts result) {
                mDialog.cancel();
                if (result != null) {
                    mDialog.cancel();

                    System.out.println("aaaaaaaaa  result  "+result.toString());
                    Log.i("aaaaaaa   Result", "" + result.toString());
                    if (result.getStatus().equalsIgnoreCase("0")) {
                        System.out.println("aaaaaaaaaa  addcart sucess");
                        *//*try{
                            if (result.getStatus().equals("0") && result.getGrade_status().equals("0")){
                                showdialog(result.getGrade_message());
                            }
                        }catch (NullPointerException e){

                        }try{
                            if (result.getStatus().equals("0") && result.getGrade_warning_status().equals("0")){
                                showdialog(result.getGrade_warning());
                            }
                        }catch (NullPointerException e){

                        }*//*
                        try{
                            if (result.getStatus().equals("0") && result.getGrade_status().equals("0")){
                                showdialog(result.getGrade_message());
                            }
                        }catch (NullPointerException e){

                        }try{
                            if (result.getStatus().equals("0") && result.getGrade_warning_status().equals("0")){
                                showdialog(result.getGrade_warning());
                            }
                        }catch (NullPointerException e){

                        }
                        ((SalesActivity)getContext()).onCart();
                        CUC.Companion.displayToast(getContext(), result.getShow_status(), result.getMessage());
                    } else if (result.getStatus().equalsIgnoreCase("10")) {

                    } else {
                        //  Toast.makeText(getContext(), ""+result.getShow_status(), Toast.LENGTH_SHORT).show();
                        CUC.Companion.displayToast((Context)getContext(), result.getShow_status(), result.getMessage());
                    }
                } else {
                    mDialog.cancel();
                }
            }
        }), (Consumer)(new Consumer() {
            // $FF: synthetic method
            // $FF: bridge method
            public void accept(Object var1) {
                this.accept((Throwable)var1);
            }

            public final void accept(Throwable error) {
                mDialog.cancel();
                Toast.makeText(getContext(), getString(R.string.connection_to_database_failed), Toast.LENGTH_SHORT).show();
                error.printStackTrace();
                System.out.println("aaaaaaaa  error "+error.getMessage());
            }
        })));*/
    }

    public void sendToCart(final GetSalesProduct getProducts, Float value, Float discount, int i){
        ((SalesActivity)getContext()).sendToCart(getProducts,value,discount,i);

       /* mDialog = CUC.Companion.createDialog(getActivity());
        RequetsInterface apiService = RequetsInterface.Factory.create();
        Map parameterMap =new HashMap<String, String>();
        parameterMap.put("api_key", String.valueOf(Preferences.INSTANCE.getPreference((getContext()), "API_KEY")));

        if (logindata != null) {
            parameterMap.put("store_id", logindata.getUser_store_id());
            parameterMap.put("employee_id", logindata.getUser_id());
        }
        parameterMap.put("package_id",getProducts.getPacket_id());
        if (queue_patient_id.equals("0")){
            parameterMap.put("patient_id",queueList.getQueue_patient_id());
        }else {
            parameterMap.put("patient_id",queue_patient_id);
        }
        if (i==1){
            parameterMap.put("percentage_discount", discount);
        }else if (i==2){
            parameterMap.put("fixed_discount", discount);
        }

        parameterMap.put("product_id", getProducts.getProduct_id());
        parameterMap.put("category_id", getProducts.getProduct_cat_id());
        parameterMap.put("package_id", getProducts.getPacket_id());
        parameterMap.put("qty", ""+value);
        parameterMap.put("terminal_id", mBAsis.getTerminal_id());
        parameterMap.put("emp_lat", Preferences.INSTANCE.getPreference(getContext(), "latitude"));
        parameterMap.put("emp_log", Preferences.INSTANCE.getPreference(getContext(), "longitude"));
        parameterMap.put("queue_id", queue_id);

        System.out.println("aaaaaaaaaaa  parameterMap add cart "+parameterMap);
        Log.i("aaaaaaaa   Result", "parameterMap : " + parameterMap);
        (new CompositeDisposable()).add(apiService.callEmployeeSalesAddcart(parameterMap).
                observeOn(AndroidSchedulers.mainThread()).
                subscribeOn(Schedulers.io()).subscribe((Consumer)(new Consumer() {
            // $FF: synthetic method
            // $FF: bridge method
            public void accept(Object var1) {
                this.accept((GetEmployeeSalesProducts)var1);
            }

            public final void accept(GetEmployeeSalesProducts result) {
                mDialog.cancel();
                if (result != null) {
                    mDialog.cancel();

                    System.out.println("aaaaaaaaa  result  "+result.toString());
                    Log.i("aaaaaaa   Result", "" + result.toString());
                    if (result.getStatus().equalsIgnoreCase("0")) {
                        System.out.println("aaaaaaaaaa  addcart sucess");
                        *//*try{
                            if (result.getStatus().equals("0") && result.getGrade_status().equals("0")){
                                showdialog(result.getGrade_message());
                            }
                        }catch (NullPointerException e){

                        }try{
                            if (result.getStatus().equals("0") && result.getGrade_warning_status().equals("0")){
                                showdialog(result.getGrade_warning());
                            }
                        }catch (NullPointerException e){

                        }*//*
                        try{
                            if (result.getStatus().equals("0") && result.getGrade_status().equals("0")){
                                showdialog(result.getGrade_message());
                            }
                        }catch (NullPointerException e){

                        }try{
                            if (result.getStatus().equals("0") && result.getGrade_warning_status().equals("0")){
                                showdialog(result.getGrade_warning());
                            }
                        }catch (NullPointerException e){

                        }
                        ((SalesActivity)getContext()).onCart();
                        CUC.Companion.displayToast(getContext(), result.getShow_status(), result.getMessage());
                    } else {
                      //  Toast.makeText(getContext(), ""+result.getShow_status(), Toast.LENGTH_SHORT).show();
                        CUC.Companion.displayToast((Context)getContext(), result.getShow_status(), result.getMessage());
                    }
                } else {
                    mDialog.cancel();
                }
            }
        }), (Consumer)(new Consumer() {
            // $FF: synthetic method
            // $FF: bridge method
            public void accept(Object var1) {
                this.accept((Throwable)var1);
            }

            public final void accept(Throwable error) {
                mDialog.cancel();
                Toast.makeText(getContext(), getString(R.string.connection_to_database_failed), Toast.LENGTH_SHORT).show();
                error.printStackTrace();
                System.out.println("aaaaaaaa  error "+error.getMessage());
            }
        })));*/
    }




    public void showdialog(String msg){
        AlertDialog.Builder builder1 = new AlertDialog.Builder(getActivity());
        builder1.setMessage(""+msg);
        builder1.setCancelable(true);

        builder1.setPositiveButton(
                "Ok",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        AlertDialog alert11 = builder1.create();
        alert11.show();
    }


    @Override
    public void sendDataMethod(String weight) {
        addProductDialog1=new AddProductDialog();
        addProductDialog1.showDialog(getContext(),getstatus,getProductsa,ProductsFragment.this,weight);
    }
}
