package com.budbasic.posconsole.reception;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.budbasic.customer.global.Preferences;
import com.budbasic.global.CUC;
import com.budbasic.posconsole.R;
import com.budbasic.posconsole.adapter.ExpenseCashDrawerAdapter;
import com.budbasic.posconsole.adapter.TillsAdapter;
import com.budbasic.posconsole.dialogs.AddCashDrawer;
import com.budbasic.posconsole.dialogs.AddCashDrop;
import com.budbasic.posconsole.dialogs.StoreCloseDailog;
import com.budbasic.posconsole.webservice.RequetsInterface;
import com.google.gson.Gson;
import com.kotlindemo.model.GetEmpExpenses;
import com.kotlindemo.model.GetEmployeeAppSetting;
import com.kotlindemo.model.GetEmployeeExpanses;
import com.kotlindemo.model.GetEmployeeLogin;
import com.kotlindemo.model.GetStatus;
import com.kotlindemo.model.GetStoreDatas;
import com.kotlindemo.model.GetStoreOpen;
import com.kotlindemo.model.GetTerminalEmployee;
import com.kotlindemo.model.GetTillOpen;
import com.kotlindemo.model.GetsalesTerminalsdata;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

/**
 * A simple {@link Fragment} subclass.
 */

@SuppressLint("ValidFragment")
public class CashDrawerFragment extends Fragment {


    @SuppressLint("ValidFragment")
    public CashDrawerFragment(GetEmployeeLogin logindata) {
        // Required empty public constructor
        this.logindata=logindata;
    }
    public Dialog mDialog;
    private GetEmployeeLogin logindata;
    GetEmployeeAppSetting mBAsis;
    private Gson gson;
    private GetStoreDatas getStoreDatas;
    private ArrayList<GetsalesTerminalsdata> getsalesTerminalslist;
    private RecyclerView recycle_tills;
    private TillsAdapter tillsAdapter;
    private TextView tv_store_open;
    private AddCashDrawer addCashDrawer;
    private ImageView btn_back;
    private TextView tv_open_status,tv_closestatus;
    private LinearLayout ll_storeclose;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_cash_drawer, container, false);
        recycle_tills=view.findViewById(R.id.recycle_tills);
        tv_store_open=view.findViewById(R.id.tv_store_open);
        btn_back=view.findViewById(R.id.btn_back);
        tv_open_status=view.findViewById(R.id.tv_open_status);
        tv_closestatus=view.findViewById(R.id.tv_closestatus);
        ll_storeclose=view.findViewById(R.id.ll_storeclose);

        gson=new Gson();
        mBAsis = gson.fromJson(Preferences.INSTANCE.getPreference(getContext(), "AppSetting"), GetEmployeeAppSetting.class);
        getsalesTerminalslist=new ArrayList<GetsalesTerminalsdata>();
        recycle_tills.setLayoutManager(new GridLayoutManager(getContext(),1));
        tillsAdapter=new TillsAdapter(getContext(),getsalesTerminalslist,CashDrawerFragment.this,logindata);
        recycle_tills.setAdapter(tillsAdapter);


        getTerminal();

        tv_store_open.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getStoreopen(0,"","");
            }
        });
        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().onBackPressed();
            }
        });
        ll_storeclose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                StoreCloseDailog addCashDrop=new StoreCloseDailog();
                addCashDrop.showDialog(getContext(),getsalesTerminalslist,logindata,CashDrawerFragment.this);
            }
        });

        return view;
    }

    public void getTerminal(){
        getsalesTerminalslist.clear();
        mDialog = CUC.Companion.createDialog(getActivity());
        RequetsInterface apiService = RequetsInterface.Factory.create();
        Map parameterMap =new HashMap<String, String>();
        parameterMap.put("api_key", String.valueOf(Preferences.INSTANCE.getPreference((getContext()), "API_KEY")));
        if (logindata != null) {
              parameterMap.put("employee_id", logindata.getUser_id());
        }
        if (mBAsis != null) {
            parameterMap.put("term_id",mBAsis.getTerminal_id());
        }

        System.out.println("aaaaaaaaaaa  parameterMap getTerminal "+parameterMap);
        Log.i("aaaaaaaa   Result", "parameterMap : " + parameterMap);
        (new CompositeDisposable()).add(apiService.callEmployeeTerminal(parameterMap).
                observeOn(AndroidSchedulers.mainThread()).
                subscribeOn(Schedulers.io()).subscribe((Consumer)(new Consumer() {
            // $FF: synthetic method
            // $FF: bridge method
            public void accept(Object var1) {
                this.accept((GetTerminalEmployee)var1);
            }

            public final void accept(GetTerminalEmployee result) {
                System.out.println("aaaaaaaaa  result  "+result.toString());
                mDialog.cancel();
                if (result != null) {
                    mDialog.cancel();
                    System.out.println("aaaaaaaaa  result getTernimal  "+result.toString());
                    Log.i("aaaaaaa   Result", "" + result.toString());
                    if (result.getStatus().equalsIgnoreCase("0")){
                        System.out.println("aaaaaaaaaa  refresh");
                        tv_open_status.setText(result.getStore_start());
                        if (result.getStore_start().equalsIgnoreCase("")){
                            tv_closestatus.setText("Store Not Closed");
                            tv_open_status.setText("Store Not Opened");

                        }else {
                            if (result.getStore_close().isEmpty()){
                                tv_closestatus.setText("Store Not Closed");
                            }else {
                                tv_closestatus.setText(result.getStore_close());
                            }
                        }


                        getStoreDatas=result.getStore();
                        getsalesTerminalslist.addAll(result.getSales_terminals());

                        for (int i=0;i<getsalesTerminalslist.size();i++){
                            if (mBAsis.getTerminal_id().equalsIgnoreCase(getsalesTerminalslist.get(i).getTerminal_id())){
                                getsalesTerminalslist.remove(result.getSales_terminals().get(i));
                                getsalesTerminalslist.add(0,result.getSales_terminals().get(i));
                            }
                        }

                        tillsAdapter.setRefresh(getsalesTerminalslist);
                        CUC.Companion.displayToast((Context)getContext(), result.getMessage(), result.getMessage());
                    }else {

                        CUC.Companion.displayToast((Context)getContext(), result.getMessage(), result.getMessage());

                    }
                } else {
                    mDialog.cancel();
                }
            }
        }), (Consumer)(new Consumer() {
            // $FF: synthetic method
            // $FF: bridge method
            public void accept(Object var1) {
                this.accept((Throwable)var1);
            }

            public final void accept(Throwable error) {
                mDialog.cancel();
                Toast.makeText(getContext(), getString(R.string.connection_to_database_failed), Toast.LENGTH_SHORT).show();
                error.printStackTrace();
                System.out.println("aaaaaaaa  error update "+error.getMessage());
            }
        })));
    }

    public void getStoreopen(final int i,String creditcash,String tillcash){

        mDialog = CUC.Companion.createDialog(getActivity());
        RequetsInterface apiService = RequetsInterface.Factory.create();
        Map parameterMap =new HashMap<String, String>();
        parameterMap.put("api_key", String.valueOf(Preferences.INSTANCE.getPreference((getContext()), "API_KEY")));
        if (logindata != null) {
            parameterMap.put("emp_id", logindata.getUser_id());
            parameterMap.put("store_id", logindata.getUser_store_id());
        }

        if (i==1){
            parameterMap.put("till_cash",tillcash);
            parameterMap.put("credit_cash",creditcash);
        }

        System.out.println("aaaaaaaaaaa  parameterMap getStore "+parameterMap);
        Log.i("aaaaaaaa   Result", "parameterMap : " + parameterMap);
        (new CompositeDisposable()).add(apiService.callEmployeeStoreopen(parameterMap).
                observeOn(AndroidSchedulers.mainThread()).
                subscribeOn(Schedulers.io()).subscribe((Consumer)(new Consumer() {
            // $FF: synthetic method
            // $FF: bridge method
            public void accept(Object var1) {
                this.accept((GetStoreOpen)var1);
            }

            public final void accept(GetStoreOpen result) {
                System.out.println("aaaaaaaaa  result  "+result.toString());
                mDialog.cancel();
                if (result != null) {
                    mDialog.cancel();
                    System.out.println("aaaaaaaaa  result getStore  "+result.toString());
                    Log.i("aaaaaaa   Result", "" + result.toString());
                    if (result.getShow_status().equalsIgnoreCase("0")){
                        addCashDrawer=new AddCashDrawer();
                        if (i==1){
                            Toast.makeText(getContext(), ""+result.getMessage(), Toast.LENGTH_SHORT).show();
                            getTerminal();
                        }else {

                            if (result.getOpen_check().equalsIgnoreCase("1")){
                               showAlert("Store Already Opened...!");
                                addCashDrawer.showDialog(getContext(),result,CashDrawerFragment.this,getStoreDatas,1);
                            }else {
                                addCashDrawer.showDialog(getContext(),result,CashDrawerFragment.this,getStoreDatas,0);
                            }
                        }
                        CUC.Companion.displayToast((Context)getContext(), "Sucess", "Sucess");
                    }
                    else {
                        showAlert(""+result.getMessage());
                        CUC.Companion.displayToast((Context)getContext(), "No Data", "No Data");

                    }
                } else {
                    mDialog.cancel();
                }
            }
        }), (Consumer)(new Consumer() {
            // $FF: synthetic method
            // $FF: bridge method
            public void accept(Object var1) {
                this.accept((Throwable)var1);
            }

            public final void accept(Throwable error) {
                mDialog.cancel();
                Toast.makeText(getContext(), getString(R.string.connection_to_database_failed), Toast.LENGTH_SHORT).show();
                error.printStackTrace();
                System.out.println("aaaaaaaa  error update "+error.getMessage());
            }
        })));
    }
    public void showAlert(String msg){
        AlertDialog.Builder builder1 = new AlertDialog.Builder(getContext());
        builder1.setMessage(""+msg);
        builder1.setCancelable(true);

        builder1.setPositiveButton(
                "Ok",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        AlertDialog alert11 = builder1.create();
        alert11.show();
    }

    public void setTillOpen(final GetsalesTerminalsdata getsalesTerminalsdata, final int i,String alertcash) {
        mDialog = CUC.Companion.createDialog(getActivity());
        RequetsInterface apiService = RequetsInterface.Factory.create();
        Map parameterMap =new HashMap<String, String>();
        parameterMap.put("api_key", String.valueOf(Preferences.INSTANCE.getPreference((getContext()), "API_KEY")));
        if (logindata != null) {
            parameterMap.put("emp_id", logindata.getUser_id());
            parameterMap.put("store_id", logindata.getUser_store_id());
        }

        parameterMap.put("terminal_id",getsalesTerminalsdata.getTerminal_id());

        if (i==1){
            parameterMap.put("till_open","0");
        }else{
            parameterMap.put("till_open","1");
            parameterMap.put("petty_cash",alertcash);
        }

        System.out.println("aaaaaaaaaaa  parameterMap tillopen "+parameterMap);
        Log.i("aaaaaaaa   Result", "parameterMap : " + parameterMap);
        (new CompositeDisposable()).add(apiService.callTillsOpen(parameterMap).
                observeOn(AndroidSchedulers.mainThread()).
                subscribeOn(Schedulers.io()).subscribe((Consumer)(new Consumer() {
            // $FF: synthetic method
            // $FF: bridge method
            public void accept(Object var1) {
                this.accept((GetTillOpen)var1);
            }

            public final void accept(GetTillOpen result) {
                System.out.println("aaaaaaaaa  result  "+result.toString());
                mDialog.cancel();
                if (result != null) {
                    mDialog.cancel();
                    System.out.println("aaaaaaaaa  result tillopen  "+result.toString());
                    Log.i("aaaaaaa   Result", "" + result.toString());
                    if (result.getStatus().equalsIgnoreCase("1")){
                        addCashDrawer=new AddCashDrawer();
                        if (i==1){
                            if (TextUtils.isEmpty(result.getOpentillCheck()) || result.getOpentillCheck().equalsIgnoreCase("0")) {
                                addCashDrawer.showDialog(getContext(),result,CashDrawerFragment.this,getsalesTerminalsdata);
                            }else {
                                showAlert(result.getMessage());
                            }
                            /*if (result.getOpentillCheck().equalsIgnoreCase("1")){
                                showAlert(result.getMessage());
                               // Toast.makeText(getContext(), ""+result.getMessage(), Toast.LENGTH_SHORT).show();
                            }else {

                            }*/
                        }else {
                            getTerminal();
                        }
                        CUC.Companion.displayToast((Context)getContext(), "Sucess", "Sucess");
                    }
                    else {
                        showAlert(result.getMessage());
                        CUC.Companion.displayToast((Context)getContext(), "No Data", "No Data");

                    }
                } else {
                    mDialog.cancel();
                }
            }
        }), (Consumer)(new Consumer() {
            // $FF: synthetic method
            // $FF: bridge method
            public void accept(Object var1) {
                this.accept((Throwable)var1);
            }

            public final void accept(Throwable error) {
                mDialog.cancel();
                Toast.makeText(getContext(), getString(R.string.connection_to_database_failed), Toast.LENGTH_SHORT).show();
                error.printStackTrace();
                System.out.println("aaaaaaaa  error update "+error.getMessage());
            }
        })));
    }


    public void setCashDrop(final GetsalesTerminalsdata getsalesTerminalsdata) {
        mDialog = CUC.Companion.createDialog(getActivity());
        RequetsInterface apiService = RequetsInterface.Factory.create();
        Map parameterMap =new HashMap<String, String>();
        parameterMap.put("api_key", String.valueOf(Preferences.INSTANCE.getPreference((getContext()), "API_KEY")));
        if (logindata != null) {
            parameterMap.put("employee_id", logindata.getUser_id());
            parameterMap.put("employee_pin", logindata.getUser_pin());
        }

        parameterMap.put("terminal_id",getsalesTerminalsdata.getTerminal_id());
        parameterMap.put("takeout_cash ",getsalesTerminalsdata.getTerminal_alert());


        System.out.println("aaaaaaaaaaa  parameterMap cashdrop "+parameterMap);
        Log.i("aaaaaaaa   Result", "parameterMap : " + parameterMap);
        (new CompositeDisposable()).add(apiService.callemployeecashdrop(parameterMap).
                observeOn(AndroidSchedulers.mainThread()).
                subscribeOn(Schedulers.io()).subscribe((Consumer)(new Consumer() {
            // $FF: synthetic method
            // $FF: bridge method
            public void accept(Object var1) {
                this.accept((GetStatus)var1);
            }

            public final void accept(GetStatus result) {
                System.out.println("aaaaaaaaa  result  "+result.toString());
                mDialog.cancel();
                if (result != null) {
                    mDialog.cancel();
                    System.out.println("aaaaaaaaa  result cashdrop  "+result.toString());
                    Log.i("aaaaaaa   Result", "" + result.toString());
                    if (result.getStatus().equalsIgnoreCase("1")){

                        Toast.makeText(getContext(), ""+result.getMessage(), Toast.LENGTH_SHORT).show();
                        CUC.Companion.displayToast((Context)getContext(), "Sucess", "Sucess");
                    }else {
                       // showAlert(result.getGrade_message());
                        Toast.makeText(getContext(), ""+result.getMessage(), Toast.LENGTH_SHORT).show();
                        CUC.Companion.displayToast((Context)getContext(), ""+result.getMessage(), ""+result.getMessage());
                        getTerminal();
                        showAlert(result.getMessage());
                    }
                } else {
                    mDialog.cancel();
                }
            }
        }), (Consumer)(new Consumer() {
            // $FF: synthetic method
            // $FF: bridge method
            public void accept(Object var1) {
                this.accept((Throwable)var1);
            }

            public final void accept(Throwable error) {
                mDialog.cancel();
                Toast.makeText(getContext(), getString(R.string.connection_to_database_failed), Toast.LENGTH_SHORT).show();
                error.printStackTrace();
                System.out.println("aaaaaaaa  error update "+error.getMessage());
            }
        })));
    }

}
