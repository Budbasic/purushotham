package com.budbasic.posconsole.reception;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.budbasic.customer.global.Preferences;
import com.budbasic.global.CUC;
import com.budbasic.posconsole.R;
import com.budbasic.posconsole.adapter.PatientAdapter;
import com.budbasic.posconsole.model.APIClass;
import com.budbasic.posconsole.webservice.RequetsInterface;
import com.google.android.gms.common.api.Api;
import com.google.gson.Gson;
import com.kotlindemo.model.GetEmployeeLogin;
import com.kotlindemo.model.GetEmployeeSalesSearchPatient;
import com.kotlindemo.model.GetPatient;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import kotlin.jvm.internal.Intrinsics;

public class ReceptionSalesPatientActivity extends AppCompatActivity implements APIClass.onCallListner, PatientAdapter.ContactsAdapterListener {

    private ImageView iv_back;
    private ProgressBar simpleProgressBar;
    @Nullable
    private APIClass apiClass;
    private String mSearchProductName = "";
    @NotNull
    public GetEmployeeLogin logindata;
    private Gson gson;
    private List<GetPatient> searchdataArrayList;
    private PatientAdapter patientAdapter;
    private RecyclerView recycle_searchproduct;
    private SearchView search_product;
    @NotNull
    private String assign_msg = "We already having this patient,do you want to aquire this details to your store?";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reception_sales_patient);

        iv_back=findViewById(R.id.iv_back);
        simpleProgressBar=findViewById(R.id.simpleProgressBar);
        recycle_searchproduct=findViewById(R.id.recycle_searchproduct);
        search_product=findViewById(R.id.search_product);
        EditText searchEditText = (EditText) search_product.findViewById(android.support.v7.appcompat.R.id.search_src_text);
        searchEditText.setTextColor(getResources().getColor(R.color.colorPrimary));
        searchEditText.setHintTextColor(getResources().getColor(R.color.colorPrimary));

        // logindata logindata = gson.fromJson(String.valueOf(Preferences.INSTANCE.getPreference(ReceptionSalesPatientActivity.this, "logindata")), GetEmployeeLogin::class.java)
        Gson gson = new Gson();
        logindata = gson.fromJson(getIntent().getStringExtra("logindata"), GetEmployeeLogin.class);
        System.out.println("aaaaaaaaaa   logindata  "+logindata.getUser_store_id()+"   "+logindata.getUser_email());
        searchdataArrayList =new ArrayList<GetPatient>();
        recycle_searchproduct.setLayoutManager(new GridLayoutManager(ReceptionSalesPatientActivity.this,1));
        patientAdapter=new PatientAdapter(ReceptionSalesPatientActivity.this,searchdataArrayList);
        recycle_searchproduct.setAdapter(patientAdapter);
        search_product.setImeOptions(EditorInfo.IME_FLAG_NO_EXTRACT_UI);
        search_product.setFocusable(true);
        search_product.setIconified(false);

        search_product.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                // filter recycler view when query submitted
                patientAdapter.getFilter().filter(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {
                // filter recycler view when text is changed
                patientAdapter.getFilter().filter(query);
                return false;
            }
        });
        callEmployeeSalesSearchPatient();
        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    @Override
    public void onFailed(int requestCode) {

    }
    public final void setLogindata(@NotNull GetEmployeeLogin var1) {
        Intrinsics.checkParameterIsNotNull(var1, "<set-?>");
        this.logindata = var1;
    }
    @Override
    public void onSuccess(int requestCode) {
        if (requestCode == 1) {
            callEmployeeSalesSearchPatient();
        }
    }

    public final APIClass getApiClass() {
        return this.apiClass;
    }

    public final void callEmployeeSalesSearchPatient() {
        simpleProgressBar.setVisibility(View.VISIBLE);
        RequetsInterface apiService = RequetsInterface.Factory.create();
        Map parameterMap = new HashMap();
        parameterMap.put("api_key", String.valueOf(Preferences.INSTANCE.getPreference((Context)this, "API_KEY")));
        parameterMap.put("patient_search", String.valueOf(this.mSearchProductName));
        parameterMap.put("store_id", logindata.getUser_store_id());
        parameterMap.put("employee_id", logindata.getUser_id());

        Log.i("aaaaaaaa   Result", "parameterMap : " + parameterMap);
        (new CompositeDisposable()).add(apiService.callEmployeeSalesSearchPatient(parameterMap).observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io()).subscribe((Consumer)(new Consumer() {
            // $FF: synthetic method
            // $FF: bridge method
            public void accept(Object var1) {
                this.accept((GetEmployeeSalesSearchPatient)var1);
            }

            public final void accept(GetEmployeeSalesSearchPatient result) {
                if (result != null) {
                    simpleProgressBar.setVisibility(View.GONE);
                    Log.i("aaaaaaa   Result", "" + result.toString());
                    assign_msg=result.getAssign_msg();
                    if (Intrinsics.areEqual(result.getStatus(), "0")) {
                        searchdataArrayList.addAll(result.getPatient());
                        patientAdapter.dataChanged(searchdataArrayList);
                        CUC.Companion.displayToast(ReceptionSalesPatientActivity.this, result.getShow_status(), result.getMessage());
                    } else if (Intrinsics.areEqual(result.getStatus(), "10")) {
                        APIClass var4 = ReceptionSalesPatientActivity.this.getApiClass();
                        if (var4 == null) {
                            Intrinsics.throwNpe();
                        }
                        var4.callApiKey(1);
                    } else {
                       simpleProgressBar.setVisibility(View.GONE);
                        Toast.makeText(ReceptionSalesPatientActivity.this, ""+result.getShow_status(), Toast.LENGTH_SHORT).show();
                        CUC.Companion.displayToast((Context)ReceptionSalesPatientActivity.this, result.getShow_status(), result.getMessage());
                    }
                } else {
                    simpleProgressBar.setVisibility(View.GONE);
                }
            }
        }), (Consumer)(new Consumer() {
            // $FF: synthetic method
            // $FF: bridge method
            public void accept(Object var1) {
                this.accept((Throwable)var1);
            }

            public final void accept(Throwable error) {
                simpleProgressBar.setVisibility(View.GONE);
                Toast.makeText(ReceptionSalesPatientActivity.this, "getString(R.string.connection_to_database_failed", Toast.LENGTH_SHORT).show();
                error.printStackTrace();
            }
        })));
    }

    public void setSendData(GetPatient getPatient) {
       // String myJson = gson.toJson(getPatient);
        Intent returnIntent = new Intent();
        String data = new Gson().toJson(getPatient);
        returnIntent.putExtra("assign_msg", assign_msg);
        returnIntent.putExtra("data", data);
        setResult(Activity.RESULT_OK, returnIntent);
        finish();
    }

    @Override
    public void onContactSelected(GetPatient contact) {

    }
}
