package com.budbasic.posconsole.reception.fragment

import android.annotation.SuppressLint
import android.app.Activity
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.CardView
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.budbasic.posconsole.R
import com.budbasic.posconsole.global.GridSpacingItemDecoration
import com.budbasic.posconsole.reception.DepositFragment
import com.budbasic.posconsole.reception.ExpensesFragment
import com.budbasic.posconsole.reception.FinancialAnalyticsFragment
import com.budbasic.posconsole.reception.StoreManagerActivity
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DecodeFormat
import com.bumptech.glide.request.RequestOptions
import com.kotlindemo.model.GetEmployeeLogin


@SuppressLint("ValidFragment")
class FinancialFragment @SuppressLint("ValidFragment") constructor(logindata2: GetEmployeeLogin) : Fragment() {
    lateinit var context: StoreManagerActivity
    var mView: View? = null
    var logindata1 : GetEmployeeLogin= logindata2

    companion object {
        var financialList = ArrayList<String>()
        var listner: ClickListner? = null
    }

    fun setListner(listner1: ClickListner): ClickListner {
        listner = listner1
        return listner1
    }

    /*   companion object {
           var dashList = ArrayList<String>()
           var listner: ClickListner? = null
       }


      */

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        if (view != null)
            mView = view
        else
            mView = inflater.inflate(R.layout.financial_fragment, container, false)
        initView()
        return mView
    }

    fun initView() {
        financialList.clear()
        financialList.add("EXPENSES")
        financialList.add("DEPOSITS")
        financialList.add("FINANCIAL ANALYTICS")

        val spanCount = 2 // 3 columns
        val spacing = 40 // 50px
        val includeEdge = false
        val rv_financial_item = mView!!.findViewById<RecyclerView>(R.id.rv_financial_item)
        rv_financial_item.layoutManager = GridLayoutManager(activity!!, 2, GridLayoutManager.VERTICAL, false)
        rv_financial_item.addItemDecoration(GridSpacingItemDecoration(spanCount, spacing, includeEdge))
        val financialAdapter = FinancialAdapter(context,logindata1)
        rv_financial_item.adapter = financialAdapter
        //   ReceptionFragment.receptionList
    }

    class FinancialAdapter(context: StoreManagerActivity?, logindata1: GetEmployeeLogin?) : RecyclerView.Adapter<FinancialAdapter.ViewHolder>() {
        lateinit var context: StoreManagerActivity
        lateinit var logindata3:GetEmployeeLogin
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            val rootView = LayoutInflater.from(parent.context).inflate(R.layout.financial_item, parent, false)
            return ViewHolder(rootView)
        }

        init {
            this.context = context!!
            this.logindata3=logindata1!!
        }

        override fun getItemCount(): Int {
            return financialList.size
        }

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            if (position == 3) {
                Glide.with(holder.iv_icon.context)
                        .applyDefaultRequestOptions(RequestOptions().format(DecodeFormat.PREFER_ARGB_8888))
                        .load(R.drawable.tv_expenses)
                        .into(holder.iv_icon)
                holder.tv_title.text = "${financialList[position]}"
            } else if (position == 2) {
                Glide.with(holder.iv_icon.context)
                        .applyDefaultRequestOptions(RequestOptions().format(DecodeFormat.PREFER_ARGB_8888))
                        .load(R.drawable.tv_finacial_analytics)
                        .into(holder.iv_icon)
                holder.tv_title.text = "${financialList[position]}"
            } else if (position == 1) {
                Glide.with(holder.iv_icon.context)
                        .applyDefaultRequestOptions(RequestOptions().format(DecodeFormat.PREFER_ARGB_8888))
                        .load(R.drawable.tv_deposits)
                        .into(holder.iv_icon)
                holder.tv_title.text = "${financialList[position]}"
            } else {
                Glide.with(holder.iv_icon.context)
                        .applyDefaultRequestOptions(RequestOptions().format(DecodeFormat.PREFER_ARGB_8888))
                        .load(R.drawable.tv_expenses)
                        .into(holder.iv_icon)
                holder.tv_title.text = "${financialList[position]}"
            }
           holder.cv_reception.setOnClickListener {
                if (position == 2) {
                    context.fragment = FinancialAnalyticsFragment(logindata3)
                    context.setFragmentWithBack(context.fragment!!,false,null,FinancialAnalyticsFragment::class.java.simpleName)
                    //  context.addOrReplace(context.fragment!!, "replace")
                } /*else if (position == 2) {
                    context.rxPermissions!!.request(
                            android.Manifest.permission.CAMERA)
                            .subscribe(object : Consumer<Boolean> {
                                override fun accept(granted: Boolean?) {
                                    if (granted!!) { // Always true pre-M
                                        // I can control the camera now
                                        val myIntent = Intent(context, MenuActivity::class.java)
                                        myIntent.putExtra("status", "0")
                                        context.startActivityForResult(myIntent, 444)
                                    } else {
                                        // Oups permission denied
                                        Snackbar.make(context.findViewById(R.id.rl_store_main), context.resources.getString(R.string.permissionenable),
                                                Snackbar.LENGTH_LONG)
                                                .setActionTextColor(Color.WHITE)
                                                .setAction(context.resources.getString(R.string.ok), object : View.OnClickListener {
                                                    override fun onClick(p0: View?) {
                                                        val intent = Intent()
                                                        intent.action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
                                                        val uri = Uri.fromParts("package", context.packageName, null)
                                                        intent.data = uri
                                                        context.startActivity(intent)
                                                    }
                                                }).show()
                                    }
                                }
                            })
                }*/ else if (position == 1) {
                    context.fragment = DepositFragment(logindata3)
                    context.setFragmentWithBack(context.fragment!!,false,null,DepositFragment::class.java.simpleName)
                    //  context.addOrReplace(context.fragment!!, "replace")
                } else {
                    println("aaaaaaaaa   logindata  "+ logindata3.user_id)
                    context.fragment = ExpensesFragment(logindata3)
                    context.setFragmentWithBack(context.fragment!!,false,null,ExpenseFinance::class.java.simpleName)
                    context.fragment = ExpensesFragment(logindata3)
                    context.setFragmentWithBack(context.fragment!!,false,null,ExpenseFinance::class.java.simpleName)
                    //context.addOrReplace(context.fragment!!, "replace")
                }
                //  listner!!.onClickReceptionList(position)
            }
        }

        class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
            val iv_icon = itemView.findViewById<ImageView>(R.id.iv_icon)
            val tv_title = itemView.findViewById<TextView>(R.id.tv_title)
            val cv_reception = itemView.findViewById<CardView>(R.id.cv_reception)

            fun bindView() {
            }
        }
    }


    interface ClickListner {
        fun onClickReceptionList(position: Int)
    }

    override fun onAttach(activity: Activity?) {
        super.onAttach(activity)
        try {
            context = (activity as StoreManagerActivity?)!!
        } catch (e: ClassCastException) {
            throw ClassCastException(activity!!.toString() + " must implement onViewSelected")
        }

    }

}


