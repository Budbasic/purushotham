package com.budbasic.posconsole.reception

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.github.vipulasri.timelineview.TimelineView
import com.kotlindemo.model.GetTrackDetails
import com.budbasic.posconsole.webservice.RequetsInterface
import com.budbasic.posconsole.R
import com.budbasic.customer.global.Preferences
import com.budbasic.global.CUC
import com.budbasic.posconsole.model.APIClass
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.timeline_view_layout.*

class TimelineViewActivity : AppCompatActivity(), APIClass.onCallListner {
    override fun onFailed(requestCode: Int) {
    }

    override fun onSuccess(requestCode: Int) {
        if (requestCode == 1) {
            callGetEmployeeInvoiceTracking()
        }
    }

    lateinit var mDialog: Dialog
    var list: ArrayList<GetTrackDetails> = ArrayList()
    lateinit var timlineAdapter: TimeLineAdapter

    var invoice_id = ""
    var order_no = ""
    var invoice_no = ""
    var apiClass: APIClass? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.timeline_view_layout)
        apiClass = APIClass(this@TimelineViewActivity, this)
        if (intent != null && intent.hasExtra("invoice_id")) {
            invoice_id = intent.getStringExtra("invoice_id")
            order_no = intent.getStringExtra("order_no")
            invoice_no = intent.getStringExtra("invoice_no")
        }
        initView()
    }

    fun initView() {
        tv_invoice_no.text = "Invoice No. $invoice_no"
        tv_invoice_unique_no.text = "Order No. $order_no"

        iv_back.setOnClickListener {
            finish()
        }
        rv_timeline.layoutManager = LinearLayoutManager(this@TimelineViewActivity)
        rv_timeline.setHasFixedSize(true)
        callGetEmployeeInvoiceTracking()
    }


    fun callGetEmployeeInvoiceTracking() {
        if (CUC.isNetworkAvailablewithPopup(this@TimelineViewActivity)) {
            try {
                if (mDialog != null)
                    mDialog.dismiss()
            } catch (e: Exception) {
                e.printStackTrace()
            }
            mDialog = CUC.createDialog(this@TimelineViewActivity)
            val apiService = RequetsInterface.Factory.create()
            val parameterMap = HashMap<String, String>()
            parameterMap["api_key"] = "${Preferences.getPreference(this@TimelineViewActivity, "API_KEY")}"
            parameterMap["invoice_id"] = "$invoice_id"
            Log.i("Result", "parameterMap : $parameterMap")
            CompositeDisposable().add(apiService.callEmployeeInvoiceTracking(parameterMap)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe({ result ->
                        if (mDialog != null && mDialog.isShowing)
                            mDialog.dismiss()
                        if (result != null) {
                            Log.i("Result", "" + result.toString())
                            if (result.status == "0") {
                                if (result.track_details != null && result.track_details.size > 0) {
                                    list.clear()
                                    list.addAll(result.track_details)
                                    timlineAdapter = TimeLineAdapter(this@TimelineViewActivity, list)
                                    rv_timeline.adapter = timlineAdapter
                                    //timlineAdapter.notifyDataSetChanged()
                                    //rl_empty_view.visibility = View.GONE
                                } else {
                                    list.clear()
                                    timlineAdapter.notifyDataSetChanged()
                                    //rl_empty_view.visibility = View.VISIBLE
                                }
                                CUC.displayToast(this@TimelineViewActivity, result.show_status, result.message)
                            } else if (result.status == "10") {
                                apiClass!!.callApiKey(1)
                            } else {
                                list.clear()
                                timlineAdapter.notifyDataSetChanged()
                                //rl_empty_view.visibility = View.VISIBLE
                                CUC.displayToast(this@TimelineViewActivity, result.show_status, result.message)
                            }

                        } else {
                            if (mDialog != null && mDialog.isShowing)
                                mDialog.dismiss()
                         //   CUC.displayToast(this@TimelineViewActivity, "0", getString(R.string.connection_to_database_failed))
                        }

                    }, { error ->
                        if (mDialog != null && mDialog.isShowing)
                            mDialog.dismiss()
                        CUC.displayToast(this@TimelineViewActivity, "0", getString(R.string.connection_to_database_failed))
                        error.printStackTrace()
                    })
            )
        }

    }

    class TimeLineAdapter(val mContext: Context, val list: ArrayList<GetTrackDetails>) : RecyclerView.Adapter<TimeLineAdapter.ViewHolder>() {
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            val mView = LayoutInflater.from(parent.context).inflate(R.layout.item_timeline_line, parent, false)
            return ViewHolder(mView, viewType)
        }

        override fun getItemCount(): Int {
            return if (list != null) list.size else 0
        }

        override fun getItemViewType(position: Int): Int {
            return TimelineView.getTimeLineViewType(position, itemCount)
        }

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            holder.bindView(mContext, position, itemCount, list[position])
        }

        class ViewHolder(itemView: View, val viewType: Int) : RecyclerView.ViewHolder(itemView) {

            val mTimelineView = itemView.findViewById<TimelineView>(R.id.time_marker)
            val text_timeline_title = itemView.findViewById<TextView>(R.id.text_timeline_title)
            val text_timeline_date = itemView.findViewById<TextView>(R.id.text_timeline_date)

            val tv_lat = itemView.findViewById<TextView>(R.id.tv_lat)
            val tv_long = itemView.findViewById<TextView>(R.id.tv_long)
            val tv_name = itemView.findViewById<TextView>(R.id.tv_name)
            val tv_email = itemView.findViewById<TextView>(R.id.tv_email)

            fun bindView(mContext: Context, position: Int, itemCount: Int, data: GetTrackDetails) {
                mTimelineView.initLine(viewType)
                mTimelineView.setMarker(ContextCompat.getDrawable(mContext, R.drawable.tv_user))
                text_timeline_title.text = "${data.i_track_desc}"
                tv_lat.text = "${data.terminal_name}"
                tv_long.text = "${data.i_track_lat} | ${data.i_track_log}"

                tv_name.text = "${data.emp_first_name} ${data.emp_last_name}"
                tv_email.text = "${data.emp_email}"

                if (data.i_track_datetime != null && data.i_track_datetime.isNotEmpty()) {
                    text_timeline_date.text = "${CUC.getdatefromoneformattoAnother("yyyy-MM-dd HH:mm:ss", "dd-MMM-yyyy  hh:mm a", "${data.i_track_datetime}")}"
                } else {
                    text_timeline_date.text = "-"
                }


            }

        }
    }
}