package com.budbasic.posconsole.reception.fragment

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.app.DatePickerDialog
import android.app.Dialog
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.os.StrictMode
import android.provider.MediaStore
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.text.InputFilter
import android.text.InputType
import android.util.Log
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.widget.*
import com.budbasic.customer.global.Preferences
import com.budbasic.global.CUC
import com.budbasic.posconsole.CountryCitySearch
import com.budbasic.posconsole.EmployeeSearchCaregiver
import com.budbasic.posconsole.LoginActivity
import com.budbasic.posconsole.R
import com.budbasic.posconsole.dialogs.SessionTimeOutDialog
import com.budbasic.posconsole.global.SingleSectionNotificationHoverMenuService
import com.budbasic.posconsole.global.UPDialog
import com.budbasic.posconsole.model.APIClass
import com.budbasic.posconsole.webservice.RequetsInterface
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.SimpleTarget
import com.bumptech.glide.request.transition.Transition
import com.github.omadahealth.lollipin.lib.managers.AppLockActivity
import com.github.omadahealth.lollipin.lib.managers.LockManager
import com.google.gson.Gson
import com.kotlindemo.model.GetEmployeeAppSetting
import com.kotlindemo.model.GetEmployeeLogin
import com.tbruyelle.rxpermissions2.RxPermissions
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.new_petient_create1.*
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.File
import java.io.FileOutputStream
import java.io.OutputStream
import java.text.SimpleDateFormat
import java.util.*


class CreateAccountActivity : AppCompatActivity(), APIClass.onCallListner {
    override fun onFailed(requestCode: Int) {
    }

    override fun onSuccess(requestCode: Int) {
        if (requestCode == EMPLOYEECREATEPATIENT) {
            if (!isImageSelected) {
                callEmployeeCreatePatient()
            } else {
                callEmployeeCreatePatientWithImage()
            }
        } else if (requestCode == EMPLOYEEPRECUSTOMER) {
            callEmployeePreCustomer()
        }
    }

    lateinit var logindata: GetEmployeeLogin
    lateinit var appSettingData: GetEmployeeAppSetting

    lateinit var mDialog: Dialog
    var loyalty_id = ""
    //var patient_id = ""
    var mobileapp = "1"
    val EMPLOYEECREATEPATIENT = 1
    val EMPLOYEEPRECUSTOMER = 2
    internal val CAMERA = 0x7

    var state_code = ""

    var driving_license = ""
    var Timestamp = ""
    var dob = ""
    var Address = ""
    var City = ""
    var Name = ""
    var Postal = ""
    var lName = ""
    var queue_id = ""
    var apiClass: APIClass? = null
    var rxPermissions: RxPermissions? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN)
        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE)
        setContentView(R.layout.new_petient_create1)
        apiClass = APIClass(this@CreateAccountActivity, this)
        if (intent != null && intent.hasExtra("driving_license")) {
           /* driving_license = intent.getStringExtra("driving_license")
            Timestamp = intent.getStringExtra("Timestamp")
            if (intent.hasExtra("dob"))
                dob = intent.getStringExtra("dob")
            Address = intent.getStringExtra("Address")
            City = intent.getStringExtra("City")
            state_code = intent.getStringExtra("state_code")
            Name = intent.getStringExtra("Name")
            lName = intent.getStringExtra("lName")
            Postal = intent.getStringExtra("Postal")
            if (intent.hasExtra("queue_id")) {
                queue_id = intent.getStringExtra("queue_id")
            }*/
        }
        appSettingData = Gson().fromJson(Preferences.getPreference(this@CreateAccountActivity, "AppSetting"), GetEmployeeAppSetting::class.java)
        logindata = Gson().fromJson(Preferences.getPreference(this@CreateAccountActivity, "logindata"), GetEmployeeLogin::class.java)
        init()
    }


    @SuppressLint("CheckResult")
    fun getPermissions() {

        rxPermissions = RxPermissions(this@CreateAccountActivity)
        rxPermissions!!.request(android.Manifest.permission.READ_EXTERNAL_STORAGE, android.Manifest.permission.WRITE_EXTERNAL_STORAGE).subscribe { grant ->
            if (grant) {
                pickImage()
            } else {
                CUC.showToast(this@CreateAccountActivity, "Permission Denied", 100)
            }
        }
    }

    fun pickImage() {
        /*var pickPhoto = Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
        startActivityForResult(pickPhoto, 1)*/
        //openCamera()

        val dialog = Dialog(this)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        val policy = StrictMode.ThreadPolicy.Builder().permitAll().build()
        StrictMode.setThreadPolicy(policy)
        dialog.setContentView(R.layout.pic_image_dialog)
        dialog.show()
        dialog.findViewById<TextView>(R.id.tv_cancelDialog).setOnClickListener {
            dialog.dismiss()
        }
        dialog.findViewById<TextView>(R.id.tv_openGallery).setOnClickListener {
            val pickPhoto = Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
            startActivityForResult(pickPhoto, 1)
            dialog.dismiss()
        }
        dialog.findViewById<TextView>(R.id.tv_OpenCamera).setOnClickListener {
            openCamera()
            dialog.dismiss()
        }


    }

    private fun init() {
        val builder = StrictMode.VmPolicy.Builder()
        StrictMode.setVmPolicy(builder.build())

        initList()
        if (logindata != null) {
            tv_register_at.text = logindata.user_store_id
        }

        myDir = File(Environment.getExternalStorageDirectory().toString() + "/TerminalVaultAdmin/")
        if (!myDir!!.exists()) {
            myDir!!.mkdirs()
        }

        /* iv_scan_id.setOnClickListener {
             if (driving_license == "") {
                 askForPermission(android.Manifest.permission.CAMERA, CAMERA)
             } else {
             }
         }*/

        iv_back.setOnClickListener {
            finish()
        }

        img_user_profile.setOnClickListener {

            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                getPermissions()
            } else {
                pickImage()
            }


        }
        iv_lock.setOnClickListener {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                applicationContext.startForegroundService(Intent(applicationContext, SingleSectionNotificationHoverMenuService::class.java))
            } else {
                applicationContext.startService(Intent(applicationContext, SingleSectionNotificationHoverMenuService::class.java))
            }
            logindata = Gson().fromJson(Preferences.getPreference(applicationContext, "logindata"), GetEmployeeLogin::class.java)
            val lockManager = LockManager.getInstance()
            if (lockManager.isAppLockEnabled) {
                lockManager.enableAppLock(applicationContext, AppLockActivity::class.java)
                lockManager.appLock.setPasscode("${logindata!!.user_pin}")
                val intent = Intent(applicationContext, SessionTimeOutDialog::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                startActivityForResult(intent, 7575)
            }
        }
        llright.setOnClickListener {

            val dialog = UPDialog(ll_loginuser, object : UPDialog.onClickListener {
                override fun callchangeprofile() {
                    pickImage()
                }

                override fun callChangePin() {
                    showDialogChangePin("1")
                }

                override fun callChangePassword() {
                    showDialogChangePin("0")
                }

                override fun callLogout() {
                    val builder = AlertDialog.Builder(this@CreateAccountActivity, R.style.MyAlertDialogStyle)
                    builder.setTitle(resources.getString(R.string.app_name))
                    builder.setMessage("Are you sure?\nDo you want to sign out?")
                    builder.setCancelable(false)
                    this@CreateAccountActivity.setFinishOnTouchOutside(false)
                    builder.setPositiveButton("YES") { dialog, which ->
                        val Logout_intent = Intent(this@CreateAccountActivity, LoginActivity::class.java)
                        Logout_intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_CLEAR_TOP
                        Preferences.removePreference(this@CreateAccountActivity, "logindata")
                        this@CreateAccountActivity.finish()
                        startActivity(Logout_intent)
                    }
                    builder.setNegativeButton("NO",
                            { arg0, arg1 -> arg0.dismiss() })
                    builder.show()
                }

            })
            dialog.show()
        }

        if (driving_license != "") {
            edt_liceno.isEnabled = false
            edt_liceno.inputType = InputType.TYPE_NULL
            //iv_scan_id.isClickable = false
            edt_liceno.setText("$driving_license")
            edt_address.setText("$Address")
            edt_firstname.setText("$Name")
            edt_lastname.setText("$lName")
            edt_zipcode.setText("$Postal")
            edt_city.setText("$City")
            //07262014
            try {
                if (Timestamp.isNotEmpty()) {
                    edt_liceexpdt.text = "$Timestamp" //"${CommonUtils.getdatefromoneformattoAnother("MMddyyyy", "MM-dd-yyyy", Timestamp)}"
                    edt_liceexpdt.tag = "${CUC.getdatefromoneformattoAnother("MM-dd-yyyy", "yyyy-MM-dd", Timestamp)}"
                    edt_liceexpdt.inputType = InputType.TYPE_NULL
                    edt_liceexpdt.isClickable = false
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
            try {
                if (dob.isNotEmpty()) {
                    edt_patient_dob.text = "$dob" //"${CommonUtils.getdatefromoneformattoAnother("MMddyyyy", "MM-dd-yyyy", Timestamp)}"
                    edt_patient_dob.tag = "${CUC.getdatefromoneformattoAnother("MM-dd-yyyy", "yyyy-MM-dd", dob)}"
                    edt_patient_dob.inputType = InputType.TYPE_NULL
                    edt_patient_dob.isClickable = false
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

//        edt_city.setOnClickListener {
//            if (state_code == "") {
//                CommonUtils.displayToast(this@CreateAccountActivity, "0", "Please select State")
//            } else {
//                val passIntent = Intent(this@CreateAccountActivity, CountryCitySearch::class.java)
//                passIntent.putExtra("IsCity", "2")
//                passIntent.putExtra("state_code", "$state_code")
//                startActivityForResult(passIntent, 9999)
//            }
//        }

        edt_state.setOnClickListener {
            val passIntent = Intent(this@CreateAccountActivity, CountryCitySearch::class.java)
            passIntent.putExtra("IsCity", "1")
            passIntent.putExtra("state_code", "")
            startActivityForResult(passIntent, 9998)
        }


        iscaregiver.setOnCheckedChangeListener(object : CompoundButton.OnCheckedChangeListener {
            override fun onCheckedChanged(p0: CompoundButton?, p1: Boolean) {
                if (p1) {
                    tvselectcaregiver.tag = ""
                    tvselectcaregiver.text = ""
                    //ivClearSearchText.visibility = View.INVISIBLE
                }
            }
        })

        tvselectcaregiver.tag = ""
        tvselectcaregiver.setOnClickListener {
            if (iscaregiver.isChecked) {
                CUC.displayToast(this@CreateAccountActivity, "0", "You are already a caregiver !")
            } else {
                val goIntent = Intent(this@CreateAccountActivity, EmployeeSearchCaregiver::class.java)
                startActivityForResult(goIntent, 4554)
            }
        }

        edt_liceexpdt.setOnClickListener {
            if (Timestamp.isEmpty()) {
                val datePickerDialog = DatePickerDialog(this@CreateAccountActivity, DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                    val selectDate = "" + (if (monthOfYear + 1 >= 10) monthOfYear + 1 else "0" + (monthOfYear + 1)) +
                            "-" + (if (dayOfMonth >= 10) dayOfMonth else "0$dayOfMonth") +
                            "-" + year
                    edt_liceexpdt.text = selectDate
                }, Calendar.getInstance().get(Calendar.YEAR), Calendar.getInstance().get(Calendar.MONTH), Calendar.getInstance().get(Calendar.DAY_OF_MONTH))
                datePickerDialog.show()
            }
        }

        edt_mmmpliceexpdt.setOnClickListener {
            val datePickerDialog = DatePickerDialog(this@CreateAccountActivity, DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                val selectDate = "" + (if (monthOfYear + 1 >= 10) monthOfYear + 1 else "0" + (monthOfYear + 1)) +
                        "-" + (if (dayOfMonth >= 10) dayOfMonth else "0$dayOfMonth") +
                        "-" + year
                edt_mmmpliceexpdt.text = selectDate
                edt_mmmpliceexpdt.tag = "${CUC.getdatefromoneformattoAnother("MM-dd-yyyy", "yyyy-MM-dd", selectDate)}"
            }, Calendar.getInstance().get(Calendar.YEAR), Calendar.getInstance().get(Calendar.MONTH), Calendar.getInstance().get(Calendar.DAY_OF_MONTH))
            datePickerDialog.datePicker.minDate = System.currentTimeMillis()
            datePickerDialog.show()
        }

        ll_cancel.setOnClickListener {
            val builder = AlertDialog.Builder(this)
            builder.setMessage(getString(R.string.are_you_sure_you_want_to_exit))
            builder.setPositiveButton("Yes") { dialog, which ->
                dialog.dismiss()
                finish()
            }
            builder.setNegativeButton("No") { dialog, which -> dialog.dismiss() }
            val alert = builder.create()
            alert.show()
        }


        /*ivClearSearchText.setOnClickListener {
            tvselectcaregiver.tag = ""
            tvselectcaregiver.text = ""
            ivClearSearchText.visibility = View.INVISIBLE
        }*/

        cv_add_patient.setOnClickListener {
            if (edt_firstname.text.toString().trim() == "") {
                CUC.displayToast(this@CreateAccountActivity, "0", "Enter first name")
                edt_firstname.requestFocus()
            } else if (edt_lastname.text.toString().trim() == "") {
                CUC.displayToast(this@CreateAccountActivity, "0", "Enter last name")
                edt_lastname.requestFocus()
            } else {
                var conVaild: Int
                if (edt_emailid.text.toString().trim() != "") {
                    conVaild = 1
                } else if (edt_mobileno.text.toString().trim() != "") {
                    conVaild = 2
                } else {
                    conVaild = 0
                }
                if (conVaild == 0) {
                    CUC.displayToast(this@CreateAccountActivity, "0", "Email or Contact no# please enter atleast one of them. it will be used for username.")
                } else {
                    if (conVaild == 1 && edt_emailid.text.toString().trim() == "") {
                        CUC.displayToast(this@CreateAccountActivity, "0", "Enter email address")
                        edt_emailid.requestFocus()
                    } else if (conVaild == 1 && !CUC.isValidEmail(edt_emailid.text.toString().trim())) {
                        CUC.displayToast(this@CreateAccountActivity, "0", "Enter valid email address")
                        edt_emailid.requestFocus()
                    } else if (conVaild == 2 && edt_mobileno.text.toString().trim() == "") {
                        CUC.displayToast(this@CreateAccountActivity, "0", "Enter mobile number")
                        edt_mobileno.requestFocus()
                    }
//                    else if (edtpassword.text.toString() == "") {
//                        CUC.displayToast(this@CreateAccountActivity, "0", "Enter new Password")
//                    } else if (edtreenterpassword.text.toString() == "") {
//                        CUC.displayToast(this@CreateAccountActivity, "0", "Enter re-enter password")
//                    }
                    else if (edt_referredby.text.toString() == "") {
                        CUC.displayToast(this@CreateAccountActivity, "0", "Enter referred by ")
                    }
//                    else if (edtreenterpassword.text.toString() != edtpassword.text.toString()) {
//                        CUC.displayToast(this@CreateAccountActivity, "0", "Password and re-enter password do not match")
//                    }
                    else if (edt_liceno.text.toString().trim() == "") {
                        CUC.displayToast(this@CreateAccountActivity, "0", "Scan driver licence number")
                        edt_liceno.requestFocus()
                    } else if (edt_liceexpdt.text.toString().trim() == "") {
                        CUC.displayToast(this@CreateAccountActivity, "0", "Enter driver licence expiration date")
                        edt_liceexpdt.requestFocus()
                    } else if (iscaregiver.isChecked && edt_mmplicenceno.text.toString().trim().isEmpty()) {
                        CUC.displayToast(this@CreateAccountActivity, "0", "Enter MMMP Licence Number")
                        edt_liceno.requestFocus()
                    } else if (iscaregiver.isChecked && edt_mmmpliceexpdt.text.toString().trim().isEmpty()) {
                        CUC.displayToast(this@CreateAccountActivity, "0", "Enter MMMP licence expiration date")
                        edt_liceexpdt.requestFocus()
                    } else if (!iscaregiver.isChecked && "${tvselectcaregiver.text}" != "" && edt_mmplicenceno.text.toString().trim() == "") {
                        CUC.displayToast(this@CreateAccountActivity, "0", "Enter MMMP Licence Number")
                        edt_liceno.requestFocus()
                    } else if (!iscaregiver.isChecked && "${tvselectcaregiver.text}" != "" && edt_mmmpliceexpdt.text.toString().trim() == "") {
                        CUC.displayToast(this@CreateAccountActivity, "0", "Enter MMMP licence expiration date")
                        edt_liceexpdt.requestFocus()
                    } else if (edt_address.text.toString().trim() == "") {
                        CUC.displayToast(this@CreateAccountActivity, "0", "Enter address")
                        edt_address.requestFocus()
                    } else if (edt_city.text.toString().trim() == "") {
                        CUC.displayToast(this@CreateAccountActivity, "0", "Enter city")
                        edt_city.requestFocus()
                    } else if (edt_state.text.toString().trim() == "") {
                        CUC.displayToast(this@CreateAccountActivity, "0", "Enter state")
                        edt_state.requestFocus()
                    }/* else if (edt_mobileapp.text.toString().trim() == "") {
                        CUC.displayToast(this@CreateAccountActivity, "0", "Enter mobile App")
                        edt_mobileapp.requestFocus()
                    } */ else if (edt_zipcode.text.toString().trim() == "") {
                        CUC.displayToast(this@CreateAccountActivity, "0", "Enter zipCode")
                        edt_zipcode.requestFocus()
                    } else {
                        if (!isImageSelected) {
                            callEmployeeCreatePatient()
                        } else {
                            callEmployeeCreatePatientWithImage()
                        }
                    }
                }
            }
        }
    }

    private fun initList() {
        callEmployeePreCustomer()
    }

    var isImageSelected: Boolean = false
    var uri: Uri? = null
    internal var bitmap: Bitmap? = null
    var imageCompanyFile: File? = null


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            9998 -> if (resultCode === Activity.RESULT_OK) {
                if (data != null && data.hasExtra("state_code")) {
                    state_code = data.getStringExtra("state_code")
                    edt_state.tag = "${data.getStringExtra("state_id")}"
                    edt_state.text = "${data.getStringExtra("state")}"
                }

            }
            4554 -> if (resultCode === Activity.RESULT_OK) {
                if (data != null && data.hasExtra("caregiver_id")) {
                    tvselectcaregiver.tag = "${data.getStringExtra("caregiver_id")}"
                    tvselectcaregiver.text = "${data.getStringExtra("caregiver_name")}"
                    //ivClearSearchText.visibility = View.VISIBLE
                    tvselectcaregiver.requestFocus()
                }
            }
            1 -> if (resultCode == Activity.RESULT_OK) {
                var selectedImageUri: Uri? = null
                var filePath: String? = null

                selectedImageUri = data!!.data

                val selectedImage = data.data
                uri = data.data
                img_user_profile.setImageURI(selectedImage)
                //decodeFile(uri.getPath());
                if (selectedImageUri != null) {
                    try {
                        // OI FILE Manager
                        val filemanagerstring = selectedImageUri.path

                        // MEDIA GALLERY
                        val selectedImagePath = getPath(selectedImageUri)

                        if (selectedImagePath != null) {
                            filePath = selectedImagePath
                        } else if (filemanagerstring != null) {
                            filePath = filemanagerstring
                        } else {

                        }

                        if (filePath != null) {
                            decodeFile(filePath)

                        } else {
                            bitmap = null
                        }
                    } catch (e: Exception) {
                        Log.e(e.javaClass.name, e.message, e)
                    }

                }


            }

            CAMERA_CAPTURE_IMAGE_REQUEST_CODE -> if (resultCode == Activity.RESULT_OK) {
                var f = File(myDir.toString())
                for (temp in f.listFiles()!!) {
                    if (temp.name == "$timeStamp.jpg") {
                        f = temp
                        break
                    }
                }
                try {
                    decodeFile(f.absolutePath)
                    //val file = File(myDir, f.name)

                } catch (e: Exception) {
                    e.printStackTrace()
                }

            }
//            9999 -> if (resultCode === Activity.RESULT_OK) {
//                if (data != null && data.hasExtra("city_id")) {
//                    edt_city.tag = "${data.getStringExtra("city_id")}"
//                    edt_city.text = "${data.getStringExtra("city")}"
//                }
//            }
//            0 -> if (resultCode === Activity.RESULT_OK) {
//                init()
//            }
            /*0x101 -> if (resultCode === Activity.RESULT_OK) {
                if (resultCode == Activity.RESULT_OK && data != null) {
                    val extras: Bundle = data.extras
                    var mResults: RecognitionResults = extras.getParcelable(ScanActivity.EXTRAS_RECOGNITION_RESULTS)
                    Log.i("combinedResult", "${mResults.recognitionResults}")
                    //
                    // set intent's component to ResultActivity and pass its contents
                    // to ResultActivity. ResultActivity will show how to extract
                    // data from result.
                    val Number = mResults.recognitionResults!![0].getStringElement("Customer ID Number")
                    val Timestamp = mResults.recognitionResults!![0].getStringElement("Document Expiration Date")
                    val Address = mResults.recognitionResults!![0].getStringElement("Full Address")
                    val City = mResults.recognitionResults!![0].getStringElement("Address - City")
                    val fName = mResults.recognitionResults!![0].getStringElement("Customer First Name")
                    val lName = mResults.recognitionResults!![0].getStringElement("Customer Family Name")
                    val Postal = mResults.recognitionResults!![0].getStringElement("Address - Postal Code")


                    edt_liceno.setText("$Number")
                    edt_address.setText("$Address")
                    edt_city.setText("$City")
                    edt_firstname.setText("$fName")
                    edt_lastname.setText("$lName")
                    edt_zipcode.setText("$Postal")
                    //07262014
                    if (Timestamp.isNotEmpty()) {
                        edt_liceexpdt.text = "${CUC.getdatefromoneformattoAnother("MMddyyyy", "MM-dd-yyyy", Timestamp)}"
                    }

                } else {
                    // if BlinkID activity did not return result, user has probably
                    // pressed Back button and cancelled scanning
                    Toast.makeText(this@CreateAccountActivity, "Scan cancelled!", Toast.LENGTH_SHORT).show()
                }
            }*/
        }
    }

    fun getPath(uri: Uri): String? {
        val projection = arrayOf(MediaStore.Images.Media.DATA)
        val cursor = this!!.managedQuery(uri, projection, null, null, null)
        if (cursor != null) {
            // HERE YOU WILL GET A NULLPOINTER IF CURSOR IS NULL
            // THIS CAN BE, IF YOU USED OI FILE MANAGER FOR PICKING THE MEDIA
            val column_index = cursor!!
                    .getColumnIndexOrThrow(MediaStore.Images.Media.DATA)
            cursor!!.moveToFirst()
            return cursor!!.getString(column_index)
        } else
            return null
    }

    fun decodeFile(filePath: String?) {
        // Decode image size
        val o = BitmapFactory.Options()
        o.inJustDecodeBounds = true
        BitmapFactory.decodeFile(filePath, o)

        // The new size we want to scale to
        val REQUIRED_SIZE = 512

        // Find the correct scale value. It should be the power of 2.
        var width_tmp = o.outWidth
        var height_tmp = o.outHeight
        var scale = 1
        while (true) {
            if (width_tmp < REQUIRED_SIZE && height_tmp < REQUIRED_SIZE)
                break
            width_tmp /= 2
            height_tmp /= 2
            scale *= 2
        }

        // Decode with inSampleSize
        val o2 = BitmapFactory.Options()
        o2.inSampleSize = scale
        bitmap = BitmapFactory.decodeFile(filePath, o2)

        /* Glide.with(this@CreateAccountActivity).asBitmap().load(bitmap).apply(RequestOptions()).into(object : SimpleTarget<Bitmap>() {
             override fun onResourceReady(resource: Bitmap, transition: com.bumptech.glide.request.transition.Transition<in Bitmap>?) {

             }
         })*/

        Glide.with(this).asBitmap().load(bitmap).apply(RequestOptions()).into(object : SimpleTarget<Bitmap>() {
            override fun onResourceReady(resource: Bitmap, transition: Transition<in Bitmap>?) {
                if (resource != null) {
                    val timeStamp = SimpleDateFormat("yyyy.MM.dd.HH.mm.ss", Locale.US).format(Date())
                    imageCompanyFile = persistImage(resource, timeStamp)

                    uri = Uri.fromFile(imageCompanyFile)
                    img_user_profile.setImageURI(uri)
                    isImageSelected = true
                }
            }
        })


    }

    fun persistImage(bitmap: Bitmap, name: String): File {

        val filesDir: File = this.filesDir
        val imageFile: File = File(filesDir, name + ".jpeg");

        var os: OutputStream
        try {
            os = FileOutputStream(imageFile);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, os);
            os.flush();
            os.close();
        } catch (e: Exception) {
            Log.e(javaClass.getSimpleName(), "Error writing bitmap", e);
        }

        return imageFile
    }

    override fun onBackPressed() {
        super.onBackPressed()
        setResult(Activity.RESULT_CANCELED)
    }

    override fun onStart() {
        super.onStart()
        Log.i("CreateAccountActivity", "onStart(), called...")
    }

    override fun onStop() {
        super.onStop()
        Log.i("CreateAccountActivity", "onStop(), called...")
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.i("CreateAccountActivity", "onDestroy(), called...")
    }

    fun callEmployeeCreatePatient() {
        mDialog = CUC.createDialog(this@CreateAccountActivity)
        val apiService = RequetsInterface.create()
        val parameterMap = HashMap<String, String>()
        parameterMap["api_key"] = "${Preferences.getPreference(this@CreateAccountActivity, "API_KEY")}"
        parameterMap["patient_fname"] = "${edt_firstname.text}"
        parameterMap["patient_lname"] = "${edt_lastname.text}"
        parameterMap["patient_reffered"] = "${edt_referredby.text}"
        parameterMap["patient_mobile_no"] = "${edt_mobileno.text}"
        //parameterMap["patient_phone_no"] = "${edt_phnno.text}"
        parameterMap["patient_licence_no"] = "${edt_liceno.text}"
        parameterMap["patient_licence_exp_date"] = "${edt_liceexpdt.tag}"
        parameterMap["patient_mmpp_licence_no"] = "${edt_mmplicenceno.text}"
        parameterMap["patient_mmpp_licence_exp_date"] = "${edt_mmmpliceexpdt.tag}"
        parameterMap["patient_address"] = "${edt_address.text}"
        parameterMap["patient_city"] = "${edt_city.text.trim()}"
        parameterMap["patient_state"] = "${edt_state.tag}"
        parameterMap["patient_country"] = "${edt_country.text}"
        parameterMap["patient_zipcode"] = "${edt_zipcode.text}"
        parameterMap["patient_mobile_app"] = "$mobileapp"
        parameterMap["patient_dob"] = "${edt_patient_dob.tag}"
        //parameterMap["patient_type"] = "$patient_id"
        parameterMap["patient_loyalty_group"] = "$loyalty_id"
        parameterMap["patient_email_id"] = "${edt_emailid.text}"
        parameterMap["patient_updated_by"] = "${logindata.user_id}"
        parameterMap["patient_regester_at"] = "${tv_register_at.text}"
        parameterMap["patient_terminal_id"] = "${appSettingData.terminal_id}"
        parameterMap["patient_terminal_lat"] = "${Preferences.getPreference(this@CreateAccountActivity, "latitude")}"
        parameterMap["patient_terminal_log"] = "${Preferences.getPreference(this@CreateAccountActivity, "longitude")}"

        parameterMap["patient_password"] = "${edtpassword.text.toString()}"
        parameterMap["patient_notes"] = "${edt_notes.text.toString()}"


        if (!iscaregiver.isChecked) {
            parameterMap["patient_is_caregiver"] = "0"
            parameterMap["patient_caregiver_id"] = "${tvselectcaregiver.tag} "
        } else {
            parameterMap["patient_is_caregiver"] = "1"
            parameterMap["patient_caregiver_id"] = "0"
        }

        Log.i("Result", "parameterMap : $parameterMap")
        CompositeDisposable().add(apiService.callGetEmployeeCreatePatient(parameterMap)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe({ result ->
                    if (mDialog != null && mDialog.isShowing)
                        mDialog.dismiss()
                    if (result != null) {
                        Log.i("Result", "result : $result")
                        if (result.status == "0") {
                            //val logindata = Gson().toJson(result)
                            CUC.displayToast(this@CreateAccountActivity, result.show_status, result.message)
                            if (queue_id != "") {
                                val calBack = Intent()
                                calBack.putExtra("queue_id", "$queue_id")
                                setResult(Activity.RESULT_FIRST_USER, calBack)
                                finish()
                            } else {
                                setResult(Activity.RESULT_FIRST_USER)
                                finish()
                            }

                        } else if (result.status == "10") {
                            apiClass!!.callApiKey(EMPLOYEECREATEPATIENT)
                        } else {
                            CUC.displayToast(this@CreateAccountActivity, result.show_status, result.message)
                        }
                    } else {
                       // CUC.displayToast(this@CreateAccountActivity, "0", getString(R.string.connection_to_database_failed))
                    }
                }, { error ->
                    if (mDialog != null && mDialog.isShowing)
                        mDialog.dismiss()
                    CUC.displayToast(this@CreateAccountActivity, "0", getString(R.string.connection_to_database_failed))
                    error.printStackTrace()
                })
        )
    }

    fun callEmployeeCreatePatientWithImage() {
        mDialog = CUC.createDialog(this@CreateAccountActivity)
        val apiService = RequetsInterface.create()
        val parameterMap = HashMap<String, RequestBody>()

        val requestFile = RequestBody.create(MediaType.parse("image/jpeg"), imageCompanyFile)
        val body = MultipartBody.Part.createFormData("patient_image", imageCompanyFile!!.name, requestFile)

        parameterMap["api_key"] = RequestBody.create(MediaType.parse("text/plain"), "${Preferences.getPreference(this@CreateAccountActivity, "API_KEY")}")
        parameterMap["patient_fname"] = RequestBody.create(MediaType.parse("text/plain"), "${edt_firstname.text}")
        parameterMap["patient_lname"] = RequestBody.create(MediaType.parse("text/plain"), "${edt_lastname.text}")
        parameterMap["patient_reffered"] = RequestBody.create(MediaType.parse("text/plain"), "${edt_referredby.text}")
        parameterMap["patient_mobile_no"] = RequestBody.create(MediaType.parse("text/plain"), "${edt_mobileno.text}")
        //parameterMap["patient_phone_no"] =                      okhttp3.RequestBody.create(okhttp3.MediaType.parse("text/plain) ,           "${edt_phnno.text}")
        parameterMap["patient_licence_no"] = RequestBody.create(MediaType.parse("text/plain"), "${edt_liceno.text}")
        parameterMap["patient_licence_exp_date"] = RequestBody.create(MediaType.parse("text/plain"), "${edt_liceexpdt.tag}")
        parameterMap["patient_mmpp_licence_no"] = RequestBody.create(MediaType.parse("text/plain"), "${edt_mmplicenceno.text}")
        parameterMap["patient_mmpp_licence_exp_date"] = RequestBody.create(MediaType.parse("text/plain"), "${edt_mmmpliceexpdt.tag}")
        parameterMap["patient_address"] = RequestBody.create(MediaType.parse("text/plain"), "${edt_address.text}")
        parameterMap["patient_city"] = RequestBody.create(MediaType.parse("text/plain"), "${edt_city.text.trim()}")
        parameterMap["patient_state"] = RequestBody.create(MediaType.parse("text/plain"), "${edt_state.tag}")
        parameterMap["patient_country"] = RequestBody.create(MediaType.parse("text/plain"), "${edt_country.text}")
        parameterMap["patient_zipcode"] = RequestBody.create(MediaType.parse("text/plain"), "${edt_zipcode.text}")
        parameterMap["patient_mobile_app"] = RequestBody.create(MediaType.parse("text/plain"), "$mobileapp")
        parameterMap["patient_dob"] = RequestBody.create(MediaType.parse("text/plain"), "${edt_patient_dob.tag}")
        //parameterMap["patient_type"] =                          okhttp3.RequestBody.create(okhttp3.MediaType.parse("text/plain) ,       "$patient_id")
        parameterMap["patient_loyalty_group"] = RequestBody.create(MediaType.parse("text/plain"), "$loyalty_id")
        parameterMap["patient_email_id"] = RequestBody.create(MediaType.parse("text/plain"), "${edt_emailid.text}")
        parameterMap["patient_updated_by"] = RequestBody.create(MediaType.parse("text/plain"), "${logindata.user_id}")
        parameterMap["patient_regester_at"] = RequestBody.create(MediaType.parse("text/plain"), "${tv_register_at.text}")
        parameterMap["patient_terminal_id"] = RequestBody.create(MediaType.parse("text/plain"), "${appSettingData.terminal_id}")
        parameterMap["patient_terminal_lat"] = RequestBody.create(MediaType.parse("text/plain"), "${Preferences.getPreference(this@CreateAccountActivity, "latitude")}")
        parameterMap["patient_terminal_log"] = RequestBody.create(MediaType.parse("text/plain"), "${Preferences.getPreference(this@CreateAccountActivity, "longitude")}")

        parameterMap["patient_password"] = RequestBody.create(MediaType.parse("text/plain"), "${edtpassword.text.toString()}")
        parameterMap["patient_notes"] = RequestBody.create(MediaType.parse("text/plain"), "${edt_notes.text.toString()}")


        /*parameterMap["api_key"] = "${Preferences.getPreference(this@CreateAccountActivity, "API_KEY")}"
        parameterMap["patient_fname"] = "${edt_firstname.text}"
        parameterMap["patient_lname"] = "${edt_lastname.text}"
        parameterMap["patient_reffered"] = "${edt_referredby.text}"
        parameterMap["patient_mobile_no"] = "${edt_mobileno.text}"
        //parameterMap["patient_phone_no"] = "${edt_phnno.text}"
        parameterMap["patient_licence_no"] = "${edt_liceno.text}"
        parameterMap["patient_licence_exp_date"] = "${edt_liceexpdt.tag}"
        parameterMap["patient_mmpp_licence_no"] = "${edt_mmplicenceno.text}"
        parameterMap["patient_mmpp_licence_exp_date"] = "${edt_mmmpliceexpdt.tag}"
        parameterMap["patient_address"] = "${edt_address.text}"
        parameterMap["patient_city"] = "${edt_city.text.trim()}"
        parameterMap["patient_state"] = "${edt_state.tag}"
        parameterMap["patient_country"] = "${edt_country.text}"
        parameterMap["patient_zipcode"] = "${edt_zipcode.text}"
        parameterMap["patient_mobile_app"] = "$mobileapp"
        parameterMap["patient_dob"] = "${edt_patient_dob.tag}"
        //parameterMap["patient_type"] = "$patient_id"
        parameterMap["patient_loyalty_group"] = "$loyalty_id"
        parameterMap["patient_email_id"] = "${edt_emailid.text}"
        parameterMap["patient_updated_by"] = "${logindata.user_id}"
        parameterMap["patient_regester_at"] = "${tv_register_at.text}"
        parameterMap["patient_terminal_id"] = "${appSettingData.terminal_id}"
        parameterMap["patient_terminal_lat"] = "${Preferences.getPreference(this@CreateAccountActivity, "latitude")}"
        parameterMap["patient_terminal_log"] = "${Preferences.getPreference(this@CreateAccountActivity, "longitude")}"

        parameterMap["patient_password"] = "${edtpassword.text.toString()}"
        parameterMap["patient_notes"] = "${edt_notes.text.toString()}"*/


        if (!iscaregiver.isChecked) {
            parameterMap["patient_is_caregiver"] = RequestBody.create(MediaType.parse("text/plain"), "0")
            parameterMap["patient_caregiver_id"] = RequestBody.create(MediaType.parse("text/plain"), "${tvselectcaregiver.tag}")
        } else {
            parameterMap["patient_is_caregiver"] = RequestBody.create(MediaType.parse("text/plain"), "1")
            parameterMap["patient_caregiver_id"] = RequestBody.create(MediaType.parse("text/plain"), "0")
        }

        Log.i("Result", "parameterMap : $parameterMap")
        CompositeDisposable().add(apiService.callGetEmployeeCreatePatientWithImage(parameterMap, body, RequestBody.create(MediaType.parse("text/plain"), imageCompanyFile!!.name))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe({ result ->
                    if (mDialog != null && mDialog.isShowing)
                        mDialog.dismiss()
                    if (result != null) {
                        Log.i("Result", "result : $result")
                        if (result.status == "0") {
                            //val logindata = Gson().toJson(result)
                            CUC.displayToast(this@CreateAccountActivity, result.show_status, result.message)
                            if (queue_id != "") {
                                val calBack = Intent()
                                calBack.putExtra("queue_id", "$queue_id")
                                setResult(Activity.RESULT_FIRST_USER, calBack)
                                finish()
                            } else {
                                setResult(Activity.RESULT_FIRST_USER)
                                finish()
                            }

                        } else if (result.status == "10") {
                            apiClass!!.callApiKey(EMPLOYEECREATEPATIENT)
                        } else {
                            CUC.displayToast(this@CreateAccountActivity, result.show_status, result.message)
                        }
                    } else {
                       // CUC.displayToast(this@CreateAccountActivity, "0", getString(R.string.connection_to_database_failed))
                    }
                }, { error ->
                    if (mDialog != null && mDialog.isShowing)
                        mDialog.dismiss()
                    CUC.displayToast(this@CreateAccountActivity, "0", getString(R.string.connection_to_database_failed))
                    error.printStackTrace()
                })
        )
    }

    lateinit var dialogChangePin: Dialog
    var employee_old_pin = ""
    var employee_new_pin = ""
    fun showDialogChangePin(status: String) {
        dialogChangePin = Dialog(this@CreateAccountActivity, R.style.TransparentProgressDialog)
        val policy = StrictMode.ThreadPolicy.Builder().permitAll().build()
        StrictMode.setThreadPolicy(policy)
        //dialog_changepass.window.setLayout(resources.getDimensionPixelSize(R.dimen._10sdp) ,ViewGroup.LayoutParams.WRAP_CONTENT)
        dialogChangePin.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialogChangePin.setContentView(R.layout.change_pin_dialog)
        dialogChangePin.window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)
        dialogChangePin.show()

        val cv_approve_request = dialogChangePin.findViewById<LinearLayout>(R.id.cv_approve_request)
        val cv_generate_invoice = dialogChangePin.findViewById<LinearLayout>(R.id.cv_generate_invoice)
        val edt_current_pin = dialogChangePin.findViewById<EditText>(R.id.edt_current_pin)
        val edt_new_pin = dialogChangePin.findViewById<EditText>(R.id.edt_new_pin)
        val edt_confirm_pin = dialogChangePin.findViewById<EditText>(R.id.edt_confirm_pin)
        val tv_cureent_lbl = dialogChangePin.findViewById<TextView>(R.id.tv_cureent_lbl)
        val tv_new_lbl = dialogChangePin.findViewById<TextView>(R.id.tv_new_lbl)
        val ll_confirm = dialogChangePin.findViewById<LinearLayout>(R.id.ll_confirm)
        val tv_confirm = dialogChangePin.findViewById<TextView>(R.id.tv_confirm)
        val tv_title = dialogChangePin.findViewById<TextView>(R.id.tv_title)

        ll_confirm.visibility = View.VISIBLE
        if (status == "1") {
            tv_cureent_lbl.text = "Old PIN"
            edt_current_pin.hint = "Old PIN"
            tv_new_lbl.text = "New PIN"
            edt_new_pin.hint = "New PIN"
            tv_confirm.text = "Confirm PIN"
            edt_confirm_pin.hint = "Confirm PIN"
            tv_title.text = "Change Pin"

            edt_current_pin.inputType = InputType.TYPE_CLASS_NUMBER or InputType.TYPE_NUMBER_VARIATION_PASSWORD
            edt_current_pin.filters = arrayOf(InputFilter.LengthFilter(4))

            edt_new_pin.inputType = InputType.TYPE_CLASS_NUMBER or InputType.TYPE_NUMBER_VARIATION_PASSWORD
            edt_new_pin.filters = arrayOf(InputFilter.LengthFilter(4))

            edt_confirm_pin.inputType = InputType.TYPE_CLASS_NUMBER or InputType.TYPE_NUMBER_VARIATION_PASSWORD
            edt_confirm_pin.filters = arrayOf(InputFilter.LengthFilter(4))
        } else {
            tv_cureent_lbl.text = "Old Password"
            edt_current_pin.hint = "Old Password"
            tv_new_lbl.text = "New Password"
            edt_new_pin.hint = "New Password"
            tv_confirm.text = "Confirm Password"
            edt_confirm_pin.hint = "Confirm Password"
            tv_title.text = "Change Password"

            edt_current_pin.inputType = InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PASSWORD

            edt_new_pin.inputType = InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PASSWORD

            edt_confirm_pin.inputType = InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PASSWORD
        }

        cv_approve_request.setOnClickListener {
            dialogChangePin.dismiss()
        }

        cv_generate_invoice.setOnClickListener {
            if (status == "1") {
                if (edt_current_pin.text.toString() == "") {
                    CUC.displayToast(this@CreateAccountActivity, "0", "Please Enter Your Current PIN")
                } else if (edt_new_pin.text.toString() == "") {
                    CUC.displayToast(this@CreateAccountActivity, "0", "Please Enter Your New PIN")
                } else if (edt_new_pin.text.toString() != edt_confirm_pin.text.toString()) {
                    CUC.displayToast(this@CreateAccountActivity, "0", getString(R.string.pin_do_not_match))
                } else {
                    employee_old_pin = "${edt_current_pin.text}"
                    employee_new_pin = "${edt_new_pin.text}"
                    callEmployeePinChange()
                }
            } else {
                if (edt_current_pin.text.toString() == "") {
                    CUC.displayToast(this@CreateAccountActivity, "0", "Please Enter Your Current Password")
                } else if (edt_new_pin.text.toString() == "") {
                    CUC.displayToast(this@CreateAccountActivity, "0", "Please Enter Your New Password")
                } else if (edt_new_pin.text.toString() != edt_confirm_pin.text.toString()) {
                    CUC.displayToast(this@CreateAccountActivity, "0", getString(R.string.password_do_not_match))
                } else {
                    employee_old_pin = "${edt_current_pin.text}"
                    employee_new_pin = "${edt_new_pin.text}"
                    callEmployeePasswordChange()
                }
            }
        }
    }

    fun callEmployeePinChange() {
        mDialog = CUC.createDialog(this@CreateAccountActivity)
        val apiService = RequetsInterface.create()
        val parameterMap = HashMap<String, String>()
        parameterMap["api_key"] = "${Preferences.getPreference(this@CreateAccountActivity, "API_KEY")}"
        parameterMap["employee_id"] = "${logindata.user_id}"
        parameterMap["employee_old_pin"] = employee_old_pin
        parameterMap["employee_new_pin"] = employee_new_pin
        Log.i("Result", "parameterMap : $parameterMap")
        CompositeDisposable().add(apiService.callEmployeePinChange(parameterMap)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe({ result ->
                    if (mDialog != null && mDialog.isShowing)
                        mDialog.dismiss()
                    if (result != null) {
                        Log.i("Result", "" + result.toString())
                        if (result.status == "0") {
                            if (::dialogChangePin.isInitialized) {
                                if (dialogChangePin.isShowing)
                                    dialogChangePin.dismiss()
                            }
                            CUC.displayToast(this@CreateAccountActivity, result.show_status, result.message)
                        } else if (result.status == "10") {
                            apiClass!!.callApiKey(2)
                        } else {
                            CUC.displayToast(this@CreateAccountActivity, result.show_status, result.message)
                        }
                    } else {
                      //  CUC.displayToast(this@CreateAccountActivity, "0", getString(R.string.connection_to_database_failed))
                    }
                }, { error ->
                    if (mDialog != null && mDialog.isShowing)
                        mDialog.dismiss()
                    CUC.displayToast(this@CreateAccountActivity, "0", getString(R.string.connection_to_database_failed))

                    error.printStackTrace()
                })
        )
    }

    fun callEmployeePasswordChange() {
        mDialog = CUC.createDialog(this@CreateAccountActivity)
        val apiService = RequetsInterface.create()
        val parameterMap = HashMap<String, String>()
        parameterMap["api_key"] = "${Preferences.getPreference(this@CreateAccountActivity, "API_KEY")}"
        parameterMap["employee_id"] = "${logindata.user_id}"
        parameterMap["employee_old_pass"] = employee_old_pin
        parameterMap["employee_new_pass"] = employee_new_pin
        Log.i("Result", "parameterMap : $parameterMap")
        CompositeDisposable().add(apiService.callEmployeePasswordChange(parameterMap)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe({ result ->
                    if (mDialog != null && mDialog.isShowing)
                        mDialog.dismiss()
                    if (result != null) {
                        Log.i("Result", "" + result.toString())
                        if (result.status == "0") {
                            if (::dialogChangePin.isInitialized) {
                                if (dialogChangePin.isShowing)
                                    dialogChangePin.dismiss()
                            }
                            CUC.displayToast(this@CreateAccountActivity, result.show_status, result.message)
                        } else if (result.status == "10") {
                            apiClass!!.callApiKey(2)
                        } else {
                            CUC.displayToast(this@CreateAccountActivity, result.show_status, result.message)
                        }
                    } else {
                     //   CUC.displayToast(this@CreateAccountActivity, "0", getString(R.string.connection_to_database_failed))
                    }
                }, { error ->
                    if (mDialog != null && mDialog.isShowing)
                        mDialog.dismiss()
                    CUC.displayToast(this@CreateAccountActivity, "0", getString(R.string.connection_to_database_failed))

                    error.printStackTrace()
                })
        )
    }

    fun callEmployeePreCustomer() {
        mDialog = CUC.createDialog(this@CreateAccountActivity)
        val apiService = RequetsInterface.Factory.create()
        val parameterMap = HashMap<String, String>()
        parameterMap["api_key"] = "${Preferences.getPreference(this@CreateAccountActivity, "API_KEY")}"
        parameterMap["store_id"] = "${logindata.user_store_id}"
        Log.i("Result", "parameterMap : $parameterMap")
        CompositeDisposable().add(apiService.callGetEmployeePreCustomer(parameterMap)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe({ result ->
                    if (mDialog != null && mDialog.isShowing)
                        mDialog.dismiss()
                    if (result != null) {
                        Log.i("Result", "" + result.toString())
                        if (result.status == "0") {
                            Log.i("Result", "state_code : $state_code")
                            for (data in result.state_data) {
                                if (data.state_code == "${state_code.trim()}") {
                                    edt_state.text = "${data.state}"
                                    edt_state.tag = "${data.state_id}"
                                    edt_state.isEnabled = false
                                    edt_state.isClickable = false
                                    break
                                }
                            }

                            val mobileApp = ArrayList<String>()
                            mobileApp.add("Yes")
                            mobileApp.add("No")
                            val mobileappAdapter = ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, mobileApp)
                            mobileappAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                            /*edt_mobileapp.setAdapter(mobileappAdapter)
                            edt_mobileapp.setOnItemClickListener { adapterView, view, i, l ->
                                if (edt_mobileapp.text.toString() == "No") {
                                    mobileapp = "2"
                                } else {
                                    mobileapp = "1"
                                }

                                Log.i(CreateAccountActivity::class.java.simpleName, "patient_id  : $mobileapp")
                            }*/

//                            val patient_typeMap = HashMap<String, String>()
//                            for (data in result.patient_type_data) {
//                                patient_typeMap["${data.type_name}"] = "${data.patient_type_id}"
//                            }
//                            val patient_typAdapter = ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, patient_typeMap.keys.distinct())
//                            patient_typAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
//                            edt_patienttype.setAdapter(patient_typAdapter)
//                            edt_patienttype.setOnItemClickListener { adapterView, view, i, l ->
//                                patient_id = "${patient_typeMap["${edt_patienttype.text}"]}"
//                                Log.i(CreateAccountActivity::class.java.simpleName, "patient_id  : $patient_id")
//                            }

                            val loyalty_dataMap = HashMap<String, String>()
                            for (data in result.loyalty_data) {
                                loyalty_dataMap["${data.discount_name}"] = "${data.discount_id}"
                            }
                            val loyalty_dataAdapter = ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, loyalty_dataMap.keys.distinct())
                            loyalty_dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                            edt_loyaltygroup.setAdapter(loyalty_dataAdapter)

                         //   edt_loyaltygroup.setCompoundDrawablesWithIntrinsicBounds(null,null,ContextCompat.getDrawable(this, R.drawable.tv_down),null)

                            edt_loyaltygroup.setOnItemClickListener { adapterView, view, i, l ->
                                loyalty_id = "${loyalty_dataMap["${edt_loyaltygroup.text}"]}"
                                Log.i(CreateAccountActivity::class.java.simpleName, "loyalty_id  : $loyalty_id")
                            }

                            CUC.displayToast(this@CreateAccountActivity, result.show_status, result.message)
                        } else if (result.status == "10") {
                            apiClass!!.callApiKey(EMPLOYEEPRECUSTOMER)
                        } else {
                            CUC.displayToast(this@CreateAccountActivity, result.show_status, result.message)
                        }
                    } else {
                       // CUC.displayToast(this@CreateAccountActivity, "0", getString(R.string.connection_to_database_failed))
                    }


                }, { error ->
                    if (mDialog != null && mDialog.isShowing)
                        mDialog.dismiss()
                    CUC.displayToast(this@CreateAccountActivity, "0", getString(R.string.connection_to_database_failed))
                    error.printStackTrace()
                })
        )
    }

    private fun askForPermission(permission: String, requestCode: Int?) {
        if (ContextCompat.checkSelfPermission(this@CreateAccountActivity, permission) != PackageManager.PERMISSION_GRANTED) {
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this@CreateAccountActivity, permission)) {
                //This is called if user has denied the permission before
                //In this case I am just asking the permission again
                ActivityCompat.requestPermissions(this@CreateAccountActivity, arrayOf(permission), requestCode!!)
            } else {
                ActivityCompat.requestPermissions(this@CreateAccountActivity, arrayOf(permission), requestCode!!)
            }
        } else {
            //scanID()
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (ActivityCompat.checkSelfPermission(this@CreateAccountActivity, permissions[0]) == PackageManager.PERMISSION_GRANTED) {
            when (requestCode) {
                //Camera
                CAMERA -> { /*scanID()*/
                }
            }
        } else {
            Toast.makeText(this@CreateAccountActivity, "Permission denied", Toast.LENGTH_SHORT).show()
        }

    }

    internal var timeStamp = ""
    private val CAMERA_REQUEST = 1002
    val CAMERA_CAPTURE_IMAGE_REQUEST_CODE = 5263
    private val EXTERNAL_CAMERA = 1003
    var myDir: File? = null
    fun openCamera() {
        val version = Build.VERSION.SDK_INT
        if (version >= 23) {
            if (ContextCompat.checkSelfPermission(this@CreateAccountActivity, android.Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {

                // Should we show an explanation?
                if (ActivityCompat.shouldShowRequestPermissionRationale(this@CreateAccountActivity, android.Manifest.permission.CAMERA)) {

                    //This is called if user has denied the permission before
                    //In this case I am just asking the permission again
                    ActivityCompat.requestPermissions(this@CreateAccountActivity, arrayOf(android.Manifest.permission.CAMERA), CAMERA_REQUEST)

                } else {

                    ActivityCompat.requestPermissions(this@CreateAccountActivity, arrayOf(android.Manifest.permission.CAMERA), CAMERA_REQUEST)
                }
            } else {

                if (ContextCompat.checkSelfPermission(this@CreateAccountActivity, android.Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {

                    // Should we show an explanation?
                    if (ActivityCompat.shouldShowRequestPermissionRationale(this@CreateAccountActivity, android.Manifest.permission.WRITE_EXTERNAL_STORAGE)) {

                        //This is called if user has denied the permission before
                        //In this case I am just asking the permission again
                        ActivityCompat.requestPermissions(this@CreateAccountActivity, arrayOf(android.Manifest.permission.WRITE_EXTERNAL_STORAGE), EXTERNAL_CAMERA)

                    } else {

                        ActivityCompat.requestPermissions(this@CreateAccountActivity, arrayOf(android.Manifest.permission.WRITE_EXTERNAL_STORAGE), EXTERNAL_CAMERA)
                    }
                } else {
                    timeStamp = SimpleDateFormat("yyyy.MM.dd.HH.mm.ss", Locale.US).format(Date())
                    val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                    val f = File(myDir, timeStamp + ".jpg")
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f))
                    startActivityForResult(intent, CAMERA_CAPTURE_IMAGE_REQUEST_CODE)
                }
            }

        } else {
            timeStamp = SimpleDateFormat("yyyy.MM.dd.HH.mm.ss", Locale.US).format(Date())
            val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            val f = File(myDir, timeStamp + ".jpg")
            intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f))
            startActivityForResult(intent, CAMERA_CAPTURE_IMAGE_REQUEST_CODE)
        }

    }


    /*fun scanID() {
        val usdl = USDLRecognizerSettings()
        startActivityForResult(buildIntent(arrayOf(usdl), ScanCard::class.java, null), 0x101)
    }*/

    /*private fun buildIntent(settArray: Array<RecognizerSettings>, target: Class<*>, helpIntent: Intent?): Intent {
        // first create intent for given activity
        val intent = Intent(this@CreateAccountActivity, target)
        // optionally, if you want the beep sound to be played after a scan
        // add a sound resource id as EXTRAS_BEEP_RESOURCE extra
        intent.putExtra(ScanActivity.EXTRAS_BEEP_RESOURCE, R.raw.beep)
        // if we have help intent, we can pass it to scan activity so it can invoke
        // it if user taps the help button. If we do not set the help intent,
        // scan activity will hide the help button.
        if (helpIntent != null) {
            intent.putExtra(ScanActivity.EXTRAS_HELP_INTENT, helpIntent)
        }
        // prepare the recognition settings
        val settings = RecognitionSettings()
        // with setNumMsBeforeTimeout you can define number of miliseconds that must pass
        // after first partial scan result has arrived before scan activity triggers a timeout.
        // Timeout is good for preventing infinitely long scanning experience when user attempts
        // to scan damaged or unsupported slip. After timeout, scan activity will return only
        // data that was read successfully. This might be incomplete data.
        //        settings.setNumMsBeforeTimeout(10000);

        // If you add more recognizers to recognizer settings array, you can choose whether you
        // want to have the ability to obtain multiple scan results from same video frame. For example,
        // if both payment slip and payment barcode are visible on a single frame, by setting
        // setAllowMultipleScanResultsOnSingleImage to true you can obtain both scan results
        // from barcode and slip. If this is false (default), you will get the first valid result
        // (i.e. first result that contains all required data). Having this option turned off
        // creates better and faster user experience.
        //        settings.setAllowMultipleScanResultsOnSingleImage(true);

        // now add array with recognizer settings so that scan activity will know
        // what do you want to scan. Setting recognizer settings array is mandatory.
        settings.recognizerSettingsArray = settArray
        intent.putExtra(ScanActivity.EXTRAS_RECOGNITION_SETTINGS, settings)

        // In order for scanning to work, you must enter a valid licence key. Without licence key,
        // scanning will not work. Licence key is bound the the package name of your app, so when
        // obtaining your licence key from Microblink make sure you give us the correct package name
        // of your app. You can obtain your licence key at http://microblink.com/login or contact us
        // at http://help.microblink.com.
        // Licence key also defines which recognizers are enabled and which are not. Since the licence
        // key validation is performed on image processing thread in native code, all enabled recognizers
        // that are disallowed by licence key will be turned off without any error and information
        // about turning them off will be logged to ADB logcat.
        intent.putExtra(ScanActivity.EXTRAS_LICENSE_KEY, appSettingData.Scanner_key_employee_android)

        // If you want, you can disable drawing of OCR results on scan activity. Drawing OCR results can be visually
        // appealing and might entertain the user while waiting for scan to complete, but might introduce a small
        // performance penalty.
        // intent.putExtra(ScanActivity.EXTRAS_SHOW_OCR_RESULT, false);

        /// If you want you can have scan activity display the focus rectangle whenever camera
        // attempts to focus, similarly to various camera app's touch to focus effect.
        // By default this is off, and you can turn this on by setting EXTRAS_SHOW_FOCUS_RECTANGLE
        // extra to true.
        intent.putExtra(ScanActivity.EXTRAS_SHOW_FOCUS_RECTANGLE, true)

        // If you want, you can enable the pinch to zoom feature of scan activity.
        // By enabling this you allow the user to use the pinch gesture to zoom the camera.
        // By default this is off and can be enabled by setting EXTRAS_ALLOW_PINCH_TO_ZOOM extra to true.
        intent.putExtra(ScanActivity.EXTRAS_ALLOW_PINCH_TO_ZOOM, true)

        // Enable showing of OCR results as animated dots. This does not have effect if non-OCR recognizer like
        // barcode recognizer is active.
        intent.putExtra(SegmentScanActivity.EXTRAS_SHOW_OCR_RESULT_MODE, ShowOcrResultMode.ANIMATED_DOTS as Parcelable)
        return intent
    }*/
}