package com.budbasic.posconsole.reception;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.kotlindemo.model.GetCartData;
import com.kotlindemo.model.GetData;
import com.kotlindemo.model.GetEmployeeLogin;
import com.kotlindemo.model.GetQueueList;

import java.util.List;

public class ViewPagerAdapter extends FragmentPagerAdapter {

    private String title[] = {"One", "Two", "Three"};
    List<GetData> cateList;
    GetEmployeeLogin logindata;
    String queue_patient_id,queue_id,image_path;
    List<GetCartData> list;
    String Currency_value;
    GetQueueList queueList;

    public ViewPagerAdapter(FragmentManager manager, List<GetData> cateList, GetEmployeeLogin logindata, String queue_patient_id,
                            String queue_id, List<GetCartData> list, String image_path, GetQueueList queueList) {
        super(manager);
        this.cateList=cateList;
        this.logindata=logindata;
        this.queue_patient_id=queue_patient_id;
        this.queue_id=queue_id;
        this.list=list;
        this.image_path=image_path;
        this.queueList=queueList;
    }

    @Override
    public Fragment getItem(int position) {
        return ProductsFragment.getInstance(position,cateList,logindata,queue_patient_id,queue_id,list,image_path,
                cateList.get(position).getCategory().getCategory_name(),queueList);
    }

    @Override
    public int getCount() {
        return cateList.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return cateList.get(position).getCategory().getCategory_name();
    }

}
