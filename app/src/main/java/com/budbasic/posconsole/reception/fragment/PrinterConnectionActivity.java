package com.budbasic.posconsole.reception.fragment;

import android.app.Dialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;

import com.budbasic.customer.global.Preferences;
import com.budbasic.global.CUC;
import com.budbasic.posconsole.R;
import com.budbasic.posconsole.adapter.BloothPairedAdapter;
import com.budbasic.posconsole.dialogs.BloothDailog;
import com.budbasic.posconsole.global.Apppreference;
import com.budbasic.posconsole.webservice.RequetsInterface;
import com.google.gson.Gson;
import com.kotlindemo.model.GetEmployeeAppSetting;
import com.kotlindemo.model.GetEmployeeLogin;
import com.kotlindemo.model.GetPrinterRegister;
import com.kotlindemo.model.GetTerminalPrinter;
import com.kotlindemo.model.getprinterdata;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

public class PrinterConnectionActivity extends AppCompatActivity {

    private RecyclerView paired_devices,new_devices;
    private TextView button_scan,title_new_devices,title_paired_devices;

    BluetoothAdapter mBluetoothAdapter;
    BluetoothSocket mmSocket;
    BluetoothDevice mmDevice;

    OutputStream mmOutputStream;
    InputStream mmInputStream;
    Thread workerThread;

    byte[] readBuffer;
    int readBufferPosition;
    volatile boolean stopWorker;
    private int REQUEST_ENABLE_BT = 2;
    ArrayList<String> list;
    private BloothPairedAdapter bloothPairedAdapter;
     Dialog mDialog;
    GetEmployeeLogin logindata;
    private Gson gson;
    private GetEmployeeAppSetting mBAsis;
    private ImageView tv_back;
    private ArrayList<getprinterdata> printerlist;
    private Apppreference sharedPreference;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_printer_connection);

        paired_devices=findViewById(R.id.paired_devices);
        button_scan=findViewById(R.id.button_scan);
        new_devices=findViewById(R.id.new_devices);
        title_paired_devices=findViewById(R.id.title_paired_devices);
        title_new_devices=findViewById(R.id.title_new_devices);
        tv_back=findViewById(R.id.tv_back);

        gson=new Gson();
        logindata = gson.fromJson(getIntent().getStringExtra("logindata"), GetEmployeeLogin.class);
        mBAsis = gson.fromJson(Preferences.INSTANCE.getPreference(PrinterConnectionActivity.this, "AppSetting"), GetEmployeeAppSetting.class);
        sharedPreference=new Apppreference(PrinterConnectionActivity.this);
        new_devices.setLayoutManager(new GridLayoutManager(PrinterConnectionActivity.this,1));
        list=new ArrayList<>();
        printerlist=new ArrayList<>();
        bloothPairedAdapter=new BloothPairedAdapter(PrinterConnectionActivity.this,list,0);
        new_devices.setAdapter(bloothPairedAdapter);

        paired_devices.setLayoutManager(new GridLayoutManager(PrinterConnectionActivity.this,1));
        bloothPairedAdapter=new BloothPairedAdapter(PrinterConnectionActivity.this,1,printerlist);
        paired_devices.setAdapter(bloothPairedAdapter);

        title_new_devices.setVisibility(View.GONE);
        new_devices.setVisibility(View.GONE);
        paired_devices.setVisibility(View.VISIBLE);
        title_paired_devices.setVisibility(View.VISIBLE);



        callEmployeeTerminalPrinter();
        tv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        button_scan.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR2)
            @Override
            public void onClick(View view) {

                title_new_devices.setVisibility(View.VISIBLE);
                new_devices.setVisibility(View.VISIBLE);
                paired_devices.setVisibility(View.GONE);
                title_paired_devices.setVisibility(View.GONE);
                //try {
                    findBT();
                   // openBT();
               /* } catch (IOException ex) {
                    ex.printStackTrace();
                }*/

                
                /*Intent enableIntent =new  Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                startActivityForResult(enableIntent, REQUEST_ENABLE_BT);*/

            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode==REQUEST_ENABLE_BT){

        }
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR2)
    void findBT() {
        list.clear();
        try {
            mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

            if(mBluetoothAdapter == null) {
                System.out.println("aaaaaaaaaa  No bluetooth adapter available");
              //  myLabel.setText("No bluetooth adapter available");
            }

            if(!mBluetoothAdapter.isEnabled()) {
                Intent enableBluetooth = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                startActivityForResult(enableBluetooth, 0);
            }

            Set<BluetoothDevice> pairedDevices = mBluetoothAdapter.getBondedDevices();

            if(pairedDevices.size() > 0) {
                for (BluetoothDevice device : pairedDevices) {
                    list.add(device.getName());
                    System.out.println("aaaaaaaaaa address  "+device.getAddress()+"  name  "+device.getName()+"  uuid "+device.getUuids().toString()+
                            " getBondState "+device.getBondState()+" getBluetoothClass "+device.getBluetoothClass()+"  getType "+device.getType());
                    // RPP300 is the name of the bluetooth printer device
                    // we got this name from the list of paired devices

                    if (device.getName().equals("NTPL Receipt")) {
                        mmDevice = device;
                        System.out.println("aaaaaaaaaaa  ifff   equal");
                        break;
                    }
                }
                bloothPairedAdapter.setRefresh(list);

            }
            System.out.println("aaaaaaaaaa   bluetooth adapter Connected");
            //myLabel.setText("Bluetooth device found.");

        }catch(Exception e){
            e.printStackTrace();
        }
    }

    void openBT() throws IOException {
        try {

            // Standard SerialPortService ID
            UUID uuid = UUID.fromString("00001101-0000-1000-8000-00805f9b34fb");
            mmSocket = mmDevice.createRfcommSocketToServiceRecord(uuid);
            mmSocket.connect();
            mmOutputStream = mmSocket.getOutputStream();
            mmInputStream = mmSocket.getInputStream();

           // beginListenForData();
            System.out.println("aaaaaaaaaa   Bluetooth Opened");


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setConnect(String s) {
        callEmployeePrinterRegister(s,1);
    }

    public void callEmployeePrinterRegister(final String port_name, int i){

        mDialog = CUC.Companion.createDialog(PrinterConnectionActivity.this);
        RequetsInterface apiService = RequetsInterface.Factory.create();
        Map parameterMap =new HashMap<String, String>();
        parameterMap.put("api_key", String.valueOf(Preferences.INSTANCE.getPreference((PrinterConnectionActivity.this), "API_KEY")));

        if (logindata != null) {
            parameterMap.put("employee_id", logindata.getUser_id());
        }
        parameterMap.put("printer_port", port_name);
        parameterMap.put("printer_type", ""+i);
        parameterMap.put("terminal_id", ""+mBAsis.getTerminal_id());
        System.out.println("aaaaaaaaaaa  parameterMap "+parameterMap);
        Log.i("aaaaaaaa   Result", "parameterMap : " + parameterMap);
        (new CompositeDisposable()).add(apiService.callEmployeePrinterRegister(parameterMap).
                observeOn(AndroidSchedulers.mainThread()).
                subscribeOn(Schedulers.io()).subscribe((Consumer)(new Consumer() {
            // $FF: synthetic method
            // $FF: bridge method
            public void accept(Object var1) {
                this.accept((GetPrinterRegister)var1);
            }

            public final void accept(GetPrinterRegister result) {
                mDialog.cancel();
                if (result != null) {
                    mDialog.cancel();
                    System.out.println("aaaaaaaaa  result registerprint "+result.toString());
                    Log.i("aaaaaaa   Result", "" + result.toString());
                    if (result.getStatus().equalsIgnoreCase("0")) {
                        title_new_devices.setVisibility(View.GONE);
                        new_devices.setVisibility(View.GONE);
                        paired_devices.setVisibility(View.VISIBLE);
                        title_paired_devices.setVisibility(View.VISIBLE);
                        callEmployeeTerminalPrinter();
                        sharedPreference.savePrintdetails(port_name);
                        CUC.Companion.displayToast(PrinterConnectionActivity.this, result.getShow_status(), result.getMessage());
                    } else {
                        sharedPreference.savePrintdetails(port_name);
                        callEmployeeTerminalPrinter();
                        title_new_devices.setVisibility(View.GONE);
                        new_devices.setVisibility(View.GONE);
                        paired_devices.setVisibility(View.VISIBLE);
                        title_paired_devices.setVisibility(View.VISIBLE);
                       // Toast.makeText(PrinterConnectionActivity.this, ""+result.getMessage(), Toast.LENGTH_SHORT).show();
                        CUC.Companion.displayToast(PrinterConnectionActivity.this, result.getShow_status(), result.getMessage());
                    }
                } else {
                    mDialog.cancel();
                }
            }
        }), (Consumer)(new Consumer() {
            // $FF: synthetic method
            // $FF: bridge method
            public void accept(Object var1) {
                this.accept((Throwable)var1);
            }

            public final void accept(Throwable error) {
                mDialog.cancel();
                Toast.makeText(PrinterConnectionActivity.this, getString(R.string.connection_to_database_failed), Toast.LENGTH_SHORT).show();
                error.printStackTrace();
                System.out.println("aaaaaaaa  error "+error.getMessage());
            }
        })));
    }

    public void callEmployeeTerminalPrinter(){

        mDialog = CUC.Companion.createDialog(PrinterConnectionActivity.this);
        RequetsInterface apiService = RequetsInterface.Factory.create();
        Map parameterMap =new HashMap<String, String>();
        parameterMap.put("api_key", String.valueOf(Preferences.INSTANCE.getPreference((PrinterConnectionActivity.this), "API_KEY")));

        if (logindata != null) {
            parameterMap.put("emp_id", logindata.getUser_id());
        }
        parameterMap.put("terminal_id", ""+mBAsis.getTerminal_id());
        System.out.println("aaaaaaaaaaa  parameterMap "+parameterMap);
        Log.i("aaaaaaaa   Result", "parameterMap : " + parameterMap);
        (new CompositeDisposable()).add(apiService.callEmployeeTerminalPrinter(parameterMap).
                observeOn(AndroidSchedulers.mainThread()).
                subscribeOn(Schedulers.io()).subscribe((Consumer)(new Consumer() {
            // $FF: synthetic method
            // $FF: bridge method
            public void accept(Object var1) {
                this.accept((GetTerminalPrinter)var1);
            }

            public final void accept(GetTerminalPrinter result) {
                mDialog.cancel();
                if (result != null) {
                    mDialog.cancel();
                    System.out.println("aaaaaaaaa  result registerprint "+result.toString());
                    Log.i("aaaaaaa   Result", "" + result.toString());
                    if (result.getStatus().equalsIgnoreCase("0")) {
                        printerlist.clear();
                        printerlist.addAll(result.getGet_printers());
                        bloothPairedAdapter.setrefreshPrinters(printerlist);
                        CUC.Companion.displayToast(PrinterConnectionActivity.this, result.getShow_status(), result.getMessage());
                    } else {
                       // Toast.makeText(PrinterConnectionActivity.this, ""+result.getMessage(), Toast.LENGTH_SHORT).show();
                        CUC.Companion.displayToast(PrinterConnectionActivity.this, result.getShow_status(), result.getMessage());
                    }
                } else {
                    mDialog.cancel();
                }
            }
        }), (Consumer)(new Consumer() {
            // $FF: synthetic method
            // $FF: bridge method
            public void accept(Object var1) {
                this.accept((Throwable)var1);
            }

            public final void accept(Throwable error) {
                mDialog.cancel();
                Toast.makeText(PrinterConnectionActivity.this, getString(R.string.connection_to_database_failed), Toast.LENGTH_SHORT).show();
                error.printStackTrace();
                System.out.println("aaaaaaaa  error "+error.getMessage());
            }
        })));
    }

    public void setEditprinter(getprinterdata getprinterdata) {
        BloothDailog bloothDailog=new BloothDailog();
        bloothDailog.showDialog(PrinterConnectionActivity.this,getprinterdata);
    }
}
