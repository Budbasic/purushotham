package com.budbasic.posconsole.reception;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.budbasic.posconsole.R;
import com.budbasic.posconsole.adapter.SettingAdapter;
import com.kotlindemo.model.GetEmployeeAppSetting;
import com.kotlindemo.model.GetEmployeeLogin;
import com.kotlindemo.model.GetSettingList;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class SettingsFragment extends Fragment {


    public SettingsFragment() {
        // Required empty public constructor
    }
    private RecyclerView rv_setting_list;
    GetEmployeeLogin logindata;
     GetEmployeeAppSetting appSettingData;
    ArrayList<GetSettingList> settingListing ;
    private SettingAdapter settingAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_settings, container, false);
        rv_setting_list=view.findViewById(R.id.rv_setting_list);

        settingListing =new ArrayList<GetSettingList>();

        rv_setting_list.setLayoutManager(new GridLayoutManager(getContext(),4));
        settingListing.add(new GetSettingList(0, R.drawable.ic_labelprinter_connection, "PRINTER CONNECTION"));
        settingListing.add(new GetSettingList(1, R.drawable.tv_idcard, "ID Verification"));
        settingListing.add(new GetSettingList(2, R.drawable.ic_receipt_connection, "PRINTER BARCODE"));
        settingListing.add(new GetSettingList(3, R.drawable.ic_printerqrcode, "PRINT QR CODE"));
        settingListing.add(new GetSettingList(4, R.drawable.ic_labelprinter_connection, "PRINT LABLE"));

        settingAdapter=new SettingAdapter(getContext(),settingListing,SettingsFragment.this);
        rv_setting_list.setAdapter(settingAdapter);

        return view;
    }

    public void setClick(GetSettingList getSettingList, int position) {
        if (position==4){

        }

    }
}
