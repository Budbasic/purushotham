package com.budbasic.posconsole.reception.fragment;


import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.budbasic.customer.global.Preferences;
import com.budbasic.global.CUC;
import com.budbasic.posconsole.R;
import com.budbasic.posconsole.adapter.OnLIneOrdersAdapter;
import com.budbasic.posconsole.webservice.RequetsInterface;
import com.kotlindemo.model.GetEmployeeLogin;
import com.kotlindemo.model.GetEmployeeOrders;
import com.kotlindemo.model.GetStoreOrders;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;


import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import kotlin.jvm.internal.Intrinsics;

/**
 * A simple {@link Fragment} subclass.
 */
@SuppressLint("ValidFragment")
public class OnLineOrdersFragment extends Fragment {

    private ImageView btn_back;
    private RadioGroup rg_order_tabs;
    private RecyclerView rv_employee_orders_fragment;
    private RelativeLayout rl_empty_view;
    private int ordertype=0;
    public Dialog mDialog;
    public GetEmployeeLogin logindata;
    private ArrayList<GetStoreOrders> list;
    private OnLIneOrdersAdapter onLIneOrdersAdapter;

    String assignedUniqno;
    Disposable subscription;
    @SuppressLint("ValidFragment")
    public OnLineOrdersFragment(GetEmployeeLogin logindata) {
        this.logindata=logindata;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_on_line_orders, container, false);
        btn_back=view.findViewById(R.id.btn_back);
        rg_order_tabs=view.findViewById(R.id.rg_order_tabs);
        rv_employee_orders_fragment=view.findViewById(R.id.rv_employee_orders_fragment);
        rl_empty_view=view.findViewById(R.id.rl_empty_view);

        rv_employee_orders_fragment.setLayoutManager(new GridLayoutManager(getContext(),1));
        list=new ArrayList<>();
        onLIneOrdersAdapter=new OnLIneOrdersAdapter(getContext(),list,logindata);
        rv_employee_orders_fragment.setAdapter(onLIneOrdersAdapter);
        callGetEmployeeOrders();
        rg_order_tabs.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                if (i==R.id.rb_preorderplaced){
                    ordertype=0;
                }else if (i==R.id.rb_received_inprocess){
                    ordertype=1;
                }else if (i==R.id.rb_invoicegenerated){
                    ordertype=2;
                }else if (i==R.id.rb_paymentreceived){
                    ordertype=3;
                }
                callGetEmployeeOrders();
            }
        });
        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().onBackPressed();
            }
        });
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        System.out.println("aaaaaaaaaaaaaa  onStart ");
        startInterval();
    }
    public void startInterval() {
        Disposable var10001 = Observable.interval(120000L, 120000L, TimeUnit.MILLISECONDS).observeOn(AndroidSchedulers.mainThread()).subscribe((Consumer)(new Consumer() {
            // $FF: synthetic method
            // $FF: bridge method
            public void accept(Object var1) {
                this.accept((Long)var1);
            }

            public final void accept(Long it) {
                Log.i(EmployeeOrdersFragment.class.getSimpleName(), "Interval : " + it + ' ');
                assignedUniqno="";
                System.out.println("aaaaaaaaaaaaaa  intervel start");
                callGetEmployeeOrders();
            }
        }));
        Intrinsics.checkExpressionValueIsNotNull(var10001, "Observable.interval(1200…rders()\n                }");
        this.subscription = var10001;
    }
    public void stopInterval() {
        subscription.dispose();
    }

    @Override
    public void onStop() {
        super.onStop();
        stopInterval();
    }

    public void callGetEmployeeOrders(){
        mDialog = CUC.Companion.createDialog(getActivity());
        RequetsInterface apiService = RequetsInterface.Factory.create();
        Map parameterMap =new HashMap<String, String>();
        parameterMap.put("api_key", String.valueOf(Preferences.INSTANCE.getPreference((getActivity()), "API_KEY")));
        if (logindata != null) {
            parameterMap.put("employee_id", logindata.getUser_id());
            parameterMap.put("employee_store_id", logindata.getUser_store_id());
        }
        parameterMap.put("order_type", ordertype);

        System.out.println("aaaaaaaaaaa  parameterMap online "+parameterMap);
        Log.i("aaaaaaaa   Result", "parameterMap : " + parameterMap);
        (new CompositeDisposable()).add(apiService.callGetEmployeeOrders(parameterMap).
                observeOn(AndroidSchedulers.mainThread()).
                subscribeOn(Schedulers.io()).subscribe((Consumer)(new Consumer() {
            // $FF: synthetic method
            // $FF: bridge method
            public void accept(Object var1) {
                this.accept((GetEmployeeOrders)var1);
            }

            public final void accept(GetEmployeeOrders result) {
                mDialog.cancel();
                if (result != null) {
                    mDialog.cancel();
                    System.out.println("aaaaaaaaa  result onlineorders  "+result.toString());
                    Log.i("aaaaaaa   Result", "" + result.toString());
                    if (result.getStatus().equalsIgnoreCase("0")){
                        CUC.Companion.displayToast((Context)getActivity(), result.getShow_status(), result.getMessage());
                        if (ordertype==0){
                            if (result.getStore_orders().size()>0){
                                list.clear();
                                list.addAll(result.getStore_orders());
                                onLIneOrdersAdapter.dataChanged(list);
                            }
                        }else if (ordertype==1){
                            if (result.getInvoice_orders().size()>0){
                                list.clear();
                                list.addAll(result.getInvoice_orders());
                                onLIneOrdersAdapter.dataChanged(list);
                            }
                        }else if (ordertype==2){
                            if (result.getEnroute().size()>0){
                                list.clear();
                                list.addAll(result.getEnroute());
                                onLIneOrdersAdapter.dataChanged(list);
                            }
                        }else {
                            if (result.getInvoice_orders().size()>0){
                                list.clear();
                                list.addAll(result.getInvoice_orders());
                                onLIneOrdersAdapter.dataChanged(list);
                            }
                        }


                    }else {
                        if (result.getShow_status().equals("0")){
                            CUC.Companion.displayToast((Context)getActivity(), result.getShow_status(), result.getMessage());
                        }else {
                            CUC.Companion.displayToast(getActivity(), result.getShow_status(), result.getMessage());
                        }

                    }
                } else {
                    mDialog.cancel();
                }
            }
        }), (Consumer)(new Consumer() {
            // $FF: synthetic method
            // $FF: bridge method
            public void accept(Object var1) {
                this.accept((Throwable)var1);
            }

            public final void accept(Throwable error) {
                mDialog.cancel();
                Toast.makeText(getActivity(), getString(R.string.connection_to_database_failed), Toast.LENGTH_SHORT).show();
                error.printStackTrace();
                System.out.println("aaaaaaaa  error update "+error.getMessage());
            }
        })));
    }

}
