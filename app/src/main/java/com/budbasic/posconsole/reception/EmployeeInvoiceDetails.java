package com.budbasic.posconsole.reception;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.budbasic.customer.global.Preferences;
import com.budbasic.global.CUC;
import com.budbasic.posconsole.EmployeeInvoiceDetailsActivity;
import com.budbasic.posconsole.R;
import com.budbasic.posconsole.adapter.InvoiceAdapter;
import com.budbasic.posconsole.adapter.TaxlistAdapter;
import com.budbasic.posconsole.dialogs.AddVoidItem;
import com.budbasic.posconsole.global.BaseActivity;
import com.budbasic.posconsole.model.APIClass;
import com.budbasic.posconsole.webservice.RequetsInterface;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.kotlindemo.model.EmployeePatientInfo;
import com.kotlindemo.model.GetDrivers;
import com.kotlindemo.model.GetEmployeeAppSetting;
import com.kotlindemo.model.GetEmployeeInvoiceDetails;
import com.kotlindemo.model.GetEmployeeLogin;
import com.kotlindemo.model.GetInvoiceData;
import com.kotlindemo.model.GetInvoiceDetail;
import com.kotlindemo.model.GetInvoiceOrders;
import com.kotlindemo.model.GetPaymentTypes;
import com.kotlindemo.model.GetStatus;
import com.kotlindemo.model.GetTaxData;
import com.tbruyelle.rxpermissions2.RxPermissions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import kotlin.jvm.internal.Intrinsics;

public class EmployeeInvoiceDetails extends BaseActivity {
    
    private RecyclerView rv_cart_list,rv_tax_list;
    private LinearLayout layout_void;
    private TextView tv_total_qty,tv_subtotal,tv_generate_bill,iv_tracking,tv_applyremove;
    String order_type,status,all_data,Currency_value,payment_path,Store_sign_required,discount_value,cart_manual_discount_type;
    private Gson gson;
    private ImageView iv_back;
    private GetInvoiceOrders invoiceOrders;
    public GetEmployeeLogin logindata;
    private GetEmployeeAppSetting appSettingData;
    RxPermissions rxPermissions;
    public Dialog mDialog;
    ArrayList<GetTaxData> taxList;
    ArrayList<GetInvoiceDetail> invoicelist;
    ArrayList<GetDrivers> driverlist;
    ArrayList<GetPaymentTypes> paymentpathlist;
    TaxlistAdapter taxlistAdapter;
    InvoiceAdapter invoiceAdapter;
    GetInvoiceData getinvoice;
    private String user_info_page;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.employee_cart_details_layout);


        rv_cart_list=findViewById(R.id.rv_cart_list);
        tv_total_qty=findViewById(R.id.tv_total_qty);
        tv_subtotal=findViewById(R.id.tv_subtotal);
        tv_generate_bill=findViewById(R.id.tv_generate_bill);
        iv_tracking=findViewById(R.id.iv_tracking);
        tv_applyremove=findViewById(R.id.tv_applyremove);
        rv_tax_list=findViewById(R.id.rv_tax_list);
        iv_back=findViewById(R.id.iv_back);
        layout_void=findViewById(R.id.layout_void);

        gson=new Gson();
        taxList=new ArrayList<>();
        invoicelist=new ArrayList<>();
        driverlist=new ArrayList<>();
        paymentpathlist=new ArrayList<>();
        rv_tax_list.setLayoutManager(new GridLayoutManager(EmployeeInvoiceDetails.this,1));
        rv_cart_list.setLayoutManager(new GridLayoutManager(EmployeeInvoiceDetails.this,1));
        order_type=getIntent().getStringExtra("order_type");
        status=getIntent().getStringExtra("status");
        all_data=getIntent().getStringExtra("all_data");
        user_info_page=getIntent().getStringExtra("user_info_page");

        
        invoiceOrders = gson.fromJson(all_data, GetInvoiceOrders.class);
       
        rxPermissions =new RxPermissions(this);
        appSettingData = gson.fromJson(Preferences.INSTANCE.getPreference(EmployeeInvoiceDetails.this, "AppSetting"), GetEmployeeAppSetting.class);
        logindata = gson.fromJson(Preferences.INSTANCE.getPreference(EmployeeInvoiceDetails.this, "logindata"), GetEmployeeLogin.class);
        Currency_value = appSettingData.getCurrency_value();

        taxlistAdapter=new TaxlistAdapter(EmployeeInvoiceDetails.this,Currency_value,taxList);
        rv_tax_list.setAdapter(taxlistAdapter);

        invoiceAdapter =new InvoiceAdapter(EmployeeInvoiceDetails.this,invoicelist,Currency_value,getinvoice);
        rv_cart_list.setAdapter(invoiceAdapter);
        if (order_type.equalsIgnoreCase("4")){
            tv_generate_bill.setVisibility(View.VISIBLE);
            tv_generate_bill.setText("Void Order");
            iv_tracking.setVisibility(View.VISIBLE);
            tv_generate_bill.setTextColor(getResources().getColor(R.color.close_red));

        }

        if (!invoiceOrders.getInvoice_void().isEmpty()){
            tv_generate_bill.setText("ORDER VOIDED");
            tv_generate_bill.setBackgroundColor(getResources().getColor(R.color.blue));
            tv_generate_bill.setTextColor(getResources().getColor(R.color.white));
        }

        getapiCallForEmployeeDocument();

        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        tv_generate_bill.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (invoiceOrders.getInvoice_void().isEmpty()){
                    AddVoidItem addVoidItem=new AddVoidItem();
                    addVoidItem.showDialog(EmployeeInvoiceDetails.this,invoiceOrders);
                }else {
                    tv_generate_bill.setText("ORDER VOIDED");
                    tv_generate_bill.setBackgroundColor(getResources().getColor(R.color.blue));
                }

            }
        });

        iv_tracking.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent passIntent =new Intent(EmployeeInvoiceDetails.this, TimelineViewActivity.class);
                if (order_type == "4") {
                    passIntent.putExtra("invoice_id", ""+invoiceOrders.getInvoice_id());
                    passIntent.putExtra("order_no", getinvoice.getInvoice_order_unique_no());
                    passIntent.putExtra("invoice_no", ""+invoiceOrders.getInvoice_no());
                } else {
                    passIntent.putExtra("invoice_id", ""+invoiceOrders.getInvoice_id());
                    passIntent.putExtra("order_no", ""+getinvoice.getInvoice_order_unique_no());
                    if (getinvoice != null) {
                        passIntent.putExtra("invoice_no", ""+getinvoice.getInvoice_no());
                    }
                }
                startActivity(passIntent);
            }
        });


    }

    public void getapiCallForEmployeeDocument(){
        if (CUC.Companion.isNetworkAvailablewithPopup((Context)this)) {
             mDialog = CUC.Companion.createDialog(EmployeeInvoiceDetails.this);
            RequetsInterface apiService = RequetsInterface.Factory.create();
            Map parameterMap =new HashMap<String, String>();
            parameterMap.put("api_key", String.valueOf(Preferences.INSTANCE.getPreference((Context)this, "API_KEY")));

            parameterMap.put("invoice_id", invoiceOrders.getInvoice_id());


            Log.i("aaaaaaaa   Result", "parameterMap : " + parameterMap);
            (new CompositeDisposable()).add(apiService.callEmployeeInvoiceDetails(parameterMap).
                    observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe((Consumer)(new Consumer() {
                // $FF: synthetic method
                // $FF: bridge method
                public void accept(Object var1) throws JSONException {
                    this.accept((GetEmployeeInvoiceDetails)var1);
                }

                public final void accept(GetEmployeeInvoiceDetails result) throws JSONException {
                     mDialog.cancel();
                    if (result != null) {
                        System.out.println("aaaaaaaaa  result  "+result.toString());
                        Log.i("aaaaaaa   Result", "" + result.toString());
                        if (Intrinsics.areEqual(result.getStatus(), "0")) {
                            if (result.getStatus().equals("0")) {
                                payment_path=result.getPayment_path();
                                Store_sign_required=result.getStore_sign_required();

                                if (result.getInvoice() != null) {
                                    getinvoice=result.getInvoice();
                                    discount_value = result.getInvoice().getInvoice_manual_discount();
                                    cart_manual_discount_type = result.getInvoice().getInvoice_manual_discount_type();
                                    if (result.getInvoice().getInvoice_void().isEmpty()){
                                        layout_void.setVisibility(View.VISIBLE);
                                    }else {
                                        layout_void.setVisibility(View.GONE);
                                    }
                                }


                                taxList.clear();
                                tv_total_qty.setText(""+invoiceOrders.getInvoice_total_qty());
                                tv_subtotal.setText(""+Currency_value+""+invoiceOrders.getInvoice_sub_total());

                                if (invoiceOrders.getInvoice_taxes() != null && !invoiceOrders.getInvoice_taxes().isEmpty()) {
                                    try {
                                        JSONArray jsonArray=new JSONArray(invoiceOrders.getInvoice_taxes());
                                        for (int i=0;i<jsonArray.length();i++){
                                            JSONObject jsonObject=jsonArray.getJSONObject(i);
                                            String tax_type_id=jsonObject.getString("tax_type_id");
                                            String tax_name=jsonObject.getString("tax_name");
                                            String tax_total=jsonObject.getString("tax_total");
                                            GetTaxData getTaxData=new GetTaxData(tax_type_id,tax_name,tax_total,"0");
                                            taxList.add(getTaxData);
                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }

                                try {
                                    if (invoiceOrders.getInvoice_loyalty_redeem() != null && invoiceOrders.getInvoice_loyalty_redeem() != "") {
                                        if (Float.parseFloat(invoiceOrders.getInvoice_loyalty_redeem()) > 0) {
                                            taxList.add(new GetTaxData("989", "Loyalty Amount", invoiceOrders.getInvoice_loyalty_redeem(),""));
                                        }
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    System.out.println("aaaaaaaaa  getInvoice_loyalty_redeem "+e.getMessage());
                                }
                                try {
                                    if (discount_value != null && !discount_value.isEmpty()) {
                                        if (Float.parseFloat(result.getInvoice().getInvoice_manual_discount()) > 0) {
                                            if (cart_manual_discount_type == "2") {
                                                taxList.add(new GetTaxData("2", "Discount", discount_value, "2"));
                                            } else {
                                                taxList.add(new GetTaxData("2", "Discount", discount_value, "1"));
                                            }
                                        }
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    System.out.println("aaaaaaaaa  discount_value "+e.getMessage());
                                }
                                if (!result.getInvoice().getInvoice_promo_code().isEmpty()) {
                                    taxList.add(new GetTaxData("101", "Promo Code "+result.getInvoice().getInvoice_promo_code(), result.getInvoice().getInvoice_promo_code(),"0"));
                                }
                                taxList.add(new GetTaxData("102", "Grand Total", result.getInvoice().getInvoice_total_payble(), "1"));

                               /* if (order_type .equalsIgnoreCase("3") || order_type.equalsIgnoreCase("4")) {
                                    taxList.add(new GetTaxData("103", "Paid Amount", result.getInvoice().getInvoice_paid_amount(),"0"));
                                    taxList.add(new GetTaxData("104", "Tender Cash", result.getInvoice().getInvoice_change(),"0"));
                                    if (cart_manual_discount_type == "1" || cart_manual_discount_type == "2") {

                                        tv_applyremove.setText("Remove Discount");

                                    } else {
                                        tv_applyremove.setText("Apply Discount");

                                    }
                                }*/
                                taxlistAdapter.dataChanged(taxList);

                                if (result.getInvoice_detail() != null && result.getInvoice_detail().size() > 0) {
                                    invoicelist.clear();
                                    invoicelist.addAll(result.getInvoice_detail());
                                    invoiceAdapter.setDatachanged(invoicelist,getinvoice);

                                    driverlist.clear();
                                    driverlist.addAll(result.getDrivers());

                                    paymentpathlist.clear();
                                    paymentpathlist.addAll(result.getPayment_types());

                                } else {
                                    invoicelist.clear();
                                    invoiceAdapter.notifyDataSetChanged();
                                    taxList.clear();
                                    taxlistAdapter.notifyDataSetChanged();
                                }

                            }
                            CUC.Companion.displayToast(EmployeeInvoiceDetails.this, result.getShow_status(), result.getMessage());
                        }  else {
                            Toast.makeText(EmployeeInvoiceDetails.this, ""+result.getShow_status(), Toast.LENGTH_SHORT).show();
                            CUC.Companion.displayToast((Context)EmployeeInvoiceDetails.this, result.getShow_status(), result.getMessage());
                        }
                    } else {
                    }
                }
            }), (Consumer)(new Consumer() {
                // $FF: synthetic method
                // $FF: bridge method
                public void accept(Object var1) {
                    this.accept((Throwable)var1);
                }

                public final void accept(Throwable error) {
                      mDialog.cancel();
                    Toast.makeText(EmployeeInvoiceDetails.this, getString(R.string.connection_to_database_failed), Toast.LENGTH_SHORT).show();
                    error.printStackTrace();
                    System.out.println("aaaaaaaa  error "+error.getMessage());
                }
            })));

        }else {
            System.out.println("aaaaaaaaaa   else ");
        }
    }

    public void setVoid(GetInvoiceDetail getInvoiceDetail) {
        AddVoidItem addVoidItem=new AddVoidItem();
        addVoidItem.showDialog(EmployeeInvoiceDetails.this,invoiceOrders,getInvoiceDetail,user_info_page);
    }

    @Override
    protected void setUp() {

    }

    @Override
    public boolean isNetworkConnected() {
        return false;
    }

    public void sendSinglevoid(final String reason, GetInvoiceDetail getInvoiceDetail){
        if (CUC.Companion.isNetworkAvailablewithPopup((Context)this)) {
            mDialog = CUC.Companion.createDialog(EmployeeInvoiceDetails.this);
            RequetsInterface apiService = RequetsInterface.Factory.create();
            Map parameterMap =new HashMap<String, String>();
            parameterMap.put("api_key", String.valueOf(Preferences.INSTANCE.getPreference((Context)this, "API_KEY")));

            parameterMap.put("invoice_id", getInvoiceDetail.getInvoice_detail_id());
            parameterMap.put("employee_id", logindata.getUser_id());
            parameterMap.put("terminal_id", appSettingData.getTerminal_id());
            parameterMap.put("employee_lat", Preferences.INSTANCE.getPreference(EmployeeInvoiceDetails.this, "latitude"));
            parameterMap.put("employee_log", Preferences.INSTANCE.getPreference(EmployeeInvoiceDetails.this, "longitude"));
            parameterMap.put("cancel_reason", reason);


            Log.i("aaaaaaaa   Result", "parameterMap : " + parameterMap);
            (new CompositeDisposable()).add(apiService.CallSingleVoid(parameterMap).
                    observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe((Consumer)(new Consumer() {
                        // $FF: synthetic method
                        // $FF: bridge method
                        public void accept(Object var1) throws JSONException {
                            this.accept((GetStatus)var1);
                        }

                        public final void accept(GetStatus result) throws JSONException {
                            mDialog.cancel();
                            if (result != null) {
                                System.out.println("aaaaaaaaa  result single void "+result.toString());
                                Log.i("aaaaaaa   Result", "" + result.toString());
                                if (Intrinsics.areEqual(result.getStatus(), "0")) {
                                    if (result.getStatus().equals("0")) {
                                        showAlert(result.getMessage());
                                        getapiCallForEmployeeDocument();
                                    }
                                    CUC.Companion.displayToast(EmployeeInvoiceDetails.this, result.getShow_status(), result.getMessage());
                                }  else {
                                    Toast.makeText(EmployeeInvoiceDetails.this, ""+result.getShow_status(), Toast.LENGTH_SHORT).show();
                                    CUC.Companion.displayToast((Context)EmployeeInvoiceDetails.this, result.getShow_status(), result.getMessage());
                                }
                            } else {
                            }
                        }
                    }), (Consumer)(new Consumer() {
                        // $FF: synthetic method
                        // $FF: bridge method
                        public void accept(Object var1) {
                            this.accept((Throwable)var1);
                        }

                        public final void accept(Throwable error) {
                            mDialog.cancel();
                            Toast.makeText(EmployeeInvoiceDetails.this, getString(R.string.connection_to_database_failed), Toast.LENGTH_SHORT).show();
                            error.printStackTrace();
                            System.out.println("aaaaaaaa  error "+error.getMessage());
                        }
                    })));

        }else {
            System.out.println("aaaaaaaaaa   else ");
        }
    }

    public void sendVoidItem(final String reason){
        if (CUC.Companion.isNetworkAvailablewithPopup((Context)this)) {
            mDialog = CUC.Companion.createDialog(EmployeeInvoiceDetails.this);
            RequetsInterface apiService = RequetsInterface.Factory.create();
            Map parameterMap =new HashMap<String, String>();
            parameterMap.put("api_key", String.valueOf(Preferences.INSTANCE.getPreference((Context)this, "API_KEY")));

            parameterMap.put("invoice_id", invoiceOrders.getInvoice_id());
            parameterMap.put("employee_id", logindata.getUser_id());
            parameterMap.put("terminal_id", appSettingData.getTerminal_id());
            parameterMap.put("employee_lat", Preferences.INSTANCE.getPreference(EmployeeInvoiceDetails.this, "latitude"));
            parameterMap.put("employee_log", Preferences.INSTANCE.getPreference(EmployeeInvoiceDetails.this, "longitude"));
            parameterMap.put("cancel_reason", reason);


            Log.i("aaaaaaaa   Result", "parameterMap : " + parameterMap);
            (new CompositeDisposable()).add(apiService.callEmployeeVoidOrder(parameterMap).
                    observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe((Consumer)(new Consumer() {
                        // $FF: synthetic method
                        // $FF: bridge method
                        public void accept(Object var1) throws JSONException {
                            this.accept((GetStatus)var1);
                        }

                        public final void accept(GetStatus result) throws JSONException {
                            mDialog.cancel();
                            if (result != null) {
                                System.out.println("aaaaaaaaa  result void "+result.toString());
                                Log.i("aaaaaaa   Result", "" + result.toString());
                                if (Intrinsics.areEqual(result.getStatus(), "0")) {
                                    if (result.getStatus().equals("0")) {
                                        showAlert(result.getMessage());
                                        getapiCallForEmployeeDocument();
                                    }
                                    CUC.Companion.displayToast(EmployeeInvoiceDetails.this, result.getShow_status(), result.getMessage());
                                }  else {
                                    Toast.makeText(EmployeeInvoiceDetails.this, ""+result.getShow_status(), Toast.LENGTH_SHORT).show();
                                    CUC.Companion.displayToast((Context)EmployeeInvoiceDetails.this, result.getShow_status(), result.getMessage());
                                }
                            } else {
                            }
                        }
                    }), (Consumer)(new Consumer() {
                        // $FF: synthetic method
                        // $FF: bridge method
                        public void accept(Object var1) {
                            this.accept((Throwable)var1);
                        }

                        public final void accept(Throwable error) {
                            mDialog.cancel();
                            Toast.makeText(EmployeeInvoiceDetails.this, getString(R.string.connection_to_database_failed), Toast.LENGTH_SHORT).show();
                            error.printStackTrace();
                            System.out.println("aaaaaaaa  error "+error.getMessage());
                        }
                    })));

        }else {
            System.out.println("aaaaaaaaaa   else ");
        }
    }

    public void showAlert(String msg){
        AlertDialog.Builder builder1 = new AlertDialog.Builder(EmployeeInvoiceDetails.this);
        builder1.setMessage(""+msg);
        builder1.setCancelable(true);

        builder1.setPositiveButton(
                "Ok",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        AlertDialog alert11 = builder1.create();
        alert11.show();
    }

    @Override
    public void showImage() {

    }
}
