package com.budbasic.posconsole

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.Window
import android.view.WindowManager
import kotlinx.android.synthetic.main.registration_approved.*

class RegistrationApproved : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN)
        setContentView(R.layout.registration_approved)
        init()
    }

    fun init() {
        if (intent != null && intent.hasExtra("status")) {
            if (intent.getStringExtra("status") == "4") {
                iv_image.setImageResource(R.drawable.worried_face_emoji)
            } else {
                iv_image.setImageResource(R.drawable.tv_admin_approve)
            }
        }

        if (intent != null && intent.hasExtra("message")) {
            tv_message.text = "${intent.getStringExtra("message")}"
        }
    }
}