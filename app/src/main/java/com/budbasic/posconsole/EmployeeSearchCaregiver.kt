package com.budbasic.posconsole

import android.app.Activity
import android.content.Intent
import android.graphics.PorterDuff
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.kotlindemo.model.GetAreaData
import com.budbasic.posconsole.webservice.RequetsInterface
import com.budbasic.customer.global.Preferences
import com.budbasic.global.CUC
import com.budbasic.posconsole.model.APIClass
import com.kotlindemo.model.GetCaregiverData
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.store_search.*
import java.util.HashMap
import kotlin.collections.ArrayList
import kotlin.collections.set


class EmployeeSearchCaregiver : AppCompatActivity(), APIClass.onCallListner {
    override fun onFailed(requestCode: Int) {
    }

    override fun onSuccess(requestCode: Int) {
        if (requestCode == 1) {
            callEmployeeSearchCaregiver()
        }
    }

    lateinit var locationCityAdapter: LocationCityAdapter
    var stateList = ArrayList<GetCaregiverData>()
    var mSearchProductName = ""
    lateinit var apiClass: APIClass
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.store_search)
        apiClass = APIClass(this@EmployeeSearchCaregiver, this)
        initView()
    }

    fun initView() {
        iv_back.setOnClickListener {
            finish()
        }
        setupRecylcer()
        customizeSearchView()
    }

    private fun setupRecylcer() {
        recycle_searchproduct.setHasFixedSize(true)
        recycle_searchproduct.layoutManager = LinearLayoutManager(this)
        recycle_searchproduct.itemAnimator = DefaultItemAnimator()
        locationCityAdapter = LocationCityAdapter(stateList, object : OnClickCityListner {
            override fun onclick(stateData: GetCaregiverData) {
                CUC.hideSoftKeyboard(this@EmployeeSearchCaregiver)
                val returnIntent = Intent()
                returnIntent.putExtra("caregiver_id", "${stateData.caregiver_id}")
                returnIntent.putExtra("caregiver_name", "${stateData.caregiver_name}")
                setResult(Activity.RESULT_OK, returnIntent)
                finish()
            }

        })
        recycle_searchproduct.adapter = locationCityAdapter
    }

    private fun customizeSearchView() {
        search_product.isFocusable = true
        search_product.isIconified = false
        search_product.setOnSearchClickListener(object : View.OnClickListener {
            override fun onClick(p0: View?) {
            }

        })
      /*  val searchButton = search_product.findViewById<ImageView>(android.support.v7.appcompat.R.id.search_button)
        searchButton.setColorFilter(ContextCompat.getColor(this@EmployeeSearchCaregiver, R.color.colorPrimary), PorterDuff.Mode.SRC_ATOP)

        val searchBox = search_product.findViewById(android.support.v7.appcompat.R.id.search_src_text) as EditText
        searchBox.setBackgroundResource(R.drawable.null_selector)
*/
        val searchBox = search_product.findViewById(android.support.v7.appcompat.R.id.search_src_text) as EditText
        // searchBox.setBackgroundResource(R.drawable.null_selector)
        searchBox.setTextColor(resources.getColor(R.color.border_gray))
        searchBox.setHintTextColor(resources.getColor(R.color.graylight))
        searchBox.hint = "Search"
        /*   val searchClose= search_product.findViewById<ImageView>(android.support.v7.appcompat.R.id.search_close_btn)
           // searchClose.setImageResource(R.drawable.ic_)
           searchClose.setImageBitmap(CUC.IconicsBitmap(this, FontAwesome.Icon.faw_times, ContextCompat.getColor(this, R.color.graylight)))

   */

        //  val img_close=mView!!.findViewById<ImageView>(R.id.img_close)
        //  img_close.setImageBitmap(CUC.IconicsBitmap(activity, FontAwesome.Icon.faw_times, ContextCompat.getColor(activity, R.color.graylight)))
        img_close.setOnClickListener {
            finish()
        }
        //View v = search_product.findViewById(android.support.v7.appcompat.R.id.search_plate);
        //v.setBackgroundColor(ContextCompat.getColor(CountryCitySearch.this, R.color.search));
        search_product.setOnCloseListener(object : android.support.v7.widget.SearchView.OnCloseListener {
            override fun onClose(): Boolean {
                //searchdataArrayList.clear()
                //adapter.notifyDataSetChanged()
                setResult(Activity.RESULT_OK)
                finish()
                return false
            }
        })

        search_product.setOnQueryTextListener(object : android.support.v7.widget.SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String): Boolean {
                return false
            }

            override fun onQueryTextChange(newText: String): Boolean {
                if (locationCityAdapter!! != null) {
                    if (!newText.isEmpty()) {
                        if (newText.length >= 2) {
                            mSearchProductName = newText
                            callEmployeeSearchCaregiver()
                        }
                    } else {
                        mSearchProductName = newText
//                            stateList.clear()
//                            locationCityAdapter.notifyDataSetChanged()
//                            tv_nodata.visibility = View.VISIBLE
//                            recycle_searchproduct.visibility = View.GONE
                        callEmployeeSearchCaregiver()
                    }
                } else {
                    if (newText.length >= 2) {
                        mSearchProductName = newText
                        callEmployeeSearchCaregiver()
                    }
                }
                return false
            }
        })
    }

    fun callEmployeeSearchCaregiver() {
        simpleProgressBar.visibility = View.VISIBLE;
        //mDialog = CommonUtils.createDialog(this@CountryCitySearch)
        val apiService = RequetsInterface.Factory.create()
        val parameterMap = HashMap<String, String>()
        parameterMap["api_key"] = "${Preferences.getPreference(this@EmployeeSearchCaregiver, "API_KEY")}"
        parameterMap["caregiver_name"] = "$mSearchProductName"
        Log.i("Result", "parameterMap : $parameterMap")
        CompositeDisposable().add(apiService.callEmployeeSearchCaregiver(parameterMap)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe({ result ->
                    //                    if (mDialog != null && mDialog.isShowing)
//                        mDialog.dismiss()
                    if (result != null) {
                        Log.i("Result", "" + result.toString())
                        if (result.status == "0") {

                            if (result.caregiver_data != null && result.caregiver_data.size > 0) {
                                stateList.clear()
                                stateList.addAll(result.caregiver_data)
                                locationCityAdapter.notifyDataSetChanged()
                                tv_nodata.visibility = View.GONE
                                recycle_searchproduct.visibility = View.VISIBLE
                                simpleProgressBar.visibility = View.INVISIBLE
                            } else {
                                stateList.clear()
                                locationCityAdapter.notifyDataSetChanged()
                                tv_nodata.visibility = View.VISIBLE
                                recycle_searchproduct.visibility = View.GONE
                                simpleProgressBar.visibility = View.INVISIBLE
                            }
                            CUC.displayToast(this@EmployeeSearchCaregiver, result.show_status, result.message)
                        } else if (result.status == "10") {
                            apiClass.callApiKey(1)
                        } else {
                            simpleProgressBar.setVisibility(View.INVISIBLE);
                            CUC.displayToast(this@EmployeeSearchCaregiver, result.show_status, result.message)
                        }
                    } else {
                        simpleProgressBar.setVisibility(View.INVISIBLE);
                       // CUC.displayToast(this@EmployeeSearchCaregiver, "0", getString(R.string.connection_to_database_failed))
                    }


                }, { error ->
                    simpleProgressBar.setVisibility(View.INVISIBLE);
                    //                    if (mDialog != null && mDialog.isShowing)
//                        mDialog.dismiss()
                    CUC.displayToast(this@EmployeeSearchCaregiver, "0", getString(R.string.connection_to_database_failed))
                    error.printStackTrace()
                })
        )
    }

    class LocationCityAdapter(val cityList: ArrayList<GetCaregiverData>, val onClickListner: OnClickCityListner) :
            RecyclerView.Adapter<LocationCityAdapter.LocationHolder>() {

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): LocationHolder {
            val mView = LayoutInflater.from(parent.context).inflate(R.layout.text_with_check_layout, parent, false)
            return LocationHolder(mView)
        }

        override fun getItemCount(): Int {
            return cityList.size
        }

        override fun onBindViewHolder(holder: LocationHolder, position: Int) {
            holder.bindView(cityList[position], onClickListner)
        }

        class LocationHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
            val tv_text = itemView.findViewById<TextView>(R.id.tv_text)
            val rl_location_item = itemView.findViewById<RelativeLayout>(R.id.rl_location_item)

            fun bindView(stateData: GetCaregiverData, onClickListner: OnClickCityListner) {
                tv_text.text = stateData.caregiver_name
                rl_location_item.setOnClickListener {
                    onClickListner.onclick(stateData)
                }
            }
        }
    }

    interface OnClickCityListner {
        fun onclick(stateData: GetCaregiverData)
    }
}