package com.budbasic.posconsole.printer.fragment

import android.Manifest
import android.annotation.SuppressLint
import android.annotation.TargetApi
import android.app.Activity
import android.app.Dialog
import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.PackageManager
import android.os.*
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.*
import android.widget.*

import com.budbasic.posconsole.R
import com.budbasic.posconsole.printer.sdk.BluetoothService
import com.budbasic.customer.global.Preferences
import com.budbasic.global.CUC
import com.budbasic.posconsole.webservice.RequetsInterface
import com.google.gson.Gson
import com.kotlindemo.model.*
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_scan.*

import java.util.ArrayList

/**
 * Created by Dev009 on 9/30/2016 30.
 */
class BluetoothScan : AppCompatActivity() {

    lateinit var mDialog: Dialog
    //  lateinit var priterterminadata:GetTerminalPrinter

    lateinit var logindata: GetEmployeeLogin
    lateinit var appSettingData: GetEmployeeAppSetting
    lateinit var dialogPrinterRegister: Dialog
    lateinit var dialogPrinterRegisterTemp: Dialog
    var printertype: Int? = null
    // Debugging
    private val TAG = "Main_Activity"
    private val DEBUG = true

    private val REQUEST_CONNECT_DEVICE = 1
    private val REQUEST_ENABLE_BT = 2


    // Message types sent from the BluetoothService Handler
    val MESSAGE_STATE_CHANGE = 1
    val MESSAGE_READ = 2
    val MESSAGE_WRITE = 3
    val MESSAGE_DEVICE_NAME = 4
    val MESSAGE_TOAST = 5
    val MESSAGE_CONNECTION_LOST = 6
    val MESSAGE_UNABLE_CONNECT = 7
    var device_port: String? = null
    var newstring: String? = null
    // Key names received from the BluetoothService Handler
    val DEVICE_NAME = "device_name"
    val TOAST = "toast"


    //    ArrayAdapter<String> mPairedDevicesArrayAdapter;
    //    private ArrayAdapter<String> mNewDevicesArrayAdapter;

    internal var mArrayDeviceName = ArrayList<String>()
    internal var yourArray = ArrayList<String>()

    internal var mArrayListPairedDevices = ArrayList<String>()
    internal var mArrayListScanDevices = ArrayList<String>()
    var mArrayListterminalregisterprinter = ArrayList<getprinterdata>()

    lateinit var mPairedDevicesArrayAdapter: PairedDeviceAdapter
    lateinit var mNewDevicesArrayAdapter: ScanDeviceAdapter

    //Dialog dialog;
    internal var mBluetoothAdapter: BluetoothAdapter? = null
    var mService: BluetoothService? = null

    val mReceiver: BroadcastReceiver = object : BroadcastReceiver() {
        @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR2)
        override fun onReceive(context: Context, intent: Intent) {
            val action = intent.action

            // When discovery finds a device
            if (BluetoothDevice.ACTION_FOUND == action) {
                // Get the BluetoothDevice object from the Intent
                val device = intent.getParcelableExtra<BluetoothDevice>(BluetoothDevice.EXTRA_DEVICE)
                Log.i("port", device.type.toString())
                //log printname


                // If it's already paired, skip it, because it's been listed already
                var deviceType = 0
                try {
                    if (device != null)
                        deviceType = device.type

                } catch (e: NoSuchMethodError) {
                    //                    if (dialog != null && dialog.isShowing())
                    //                        dialog.dismiss();
                    e.printStackTrace()
                }


                if (deviceType == BluetoothDevice.DEVICE_TYPE_DUAL) {
                    if (device!!.bondState != BluetoothDevice.BOND_BONDED) {

                        if (mArrayDeviceName.size > 0) {
                            for (i in mArrayDeviceName.indices) {
                                if (!mArrayDeviceName.contains(device.name + "\n" + device.address)) {
                                    mArrayDeviceName.add(device.name + "\n" + device.address)

                                    //   mArrayListPairedDevices.add(device.name + "\n" + device.address)
                                    mArrayListScanDevices.add(device.name + "\n" + device.address)
                                    //                                    if (dialog != null && dialog.isShowing())
                                    //                                        dialog.dismiss();
                                    mNewDevicesArrayAdapter.notifyDataSetChanged()
                                }
                            }
                        } else {
                            println("aaaaaaaaaa  adddress   "+device.address)
                            mArrayDeviceName.add(device.name + "\n" + device.address)
                            mArrayListScanDevices.add(device.name + "\n" + device.address)
                            //                            if (dialog != null && dialog.isShowing())
                            //                                dialog.dismiss();
                            mNewDevicesArrayAdapter.notifyDataSetChanged()

                        }

                    } else {
                        //new test add 5/12/2018

                        //                        if (dialog != null && dialog.isShowing())
                        //                            dialog.dismiss();
                    }

                } else {
                    //                    if (dialog != null && dialog.isShowing())
                    //                        dialog.dismiss();
                }

                // When discovery is finished, change the Activity title
            } else if (BluetoothAdapter.ACTION_DISCOVERY_FINISHED == action) {
                if (mArrayListScanDevices.size == 0) {
                    val noDevices = resources.getText(R.string.none_found).toString()
                    mArrayListScanDevices.add(noDevices)
                    mNewDevicesArrayAdapter.notifyDataSetChanged()
                    button_scan.visibility = View.VISIBLE
                    //                    if (dialog != null && dialog.isShowing())
                    //                        dialog.dismiss();

                }
            }
        }
    }

    //    /****************************************************************************************************/
    @SuppressLint("HandlerLeak")
    val mHandler: Handler = object : Handler() {
        override fun handleMessage(msg: Message) {
            when (msg.what) {
                MESSAGE_STATE_CHANGE -> {
                    if (DEBUG)
                        Log.i(TAG, "MESSAGE_STATE_CHANGE: " + msg.arg1)
                    when (msg.arg1) {
                        BluetoothService.STATE_CONNECTED -> Toast.makeText(applicationContext, getText(R.string.Connecting), Toast.LENGTH_LONG).show()
                        BluetoothService.STATE_CONNECTING -> Toast.makeText(applicationContext, getText(R.string.title_connecting), Toast.LENGTH_LONG).show()
                        BluetoothService.STATE_LISTEN,

                        BluetoothService.STATE_NONE -> {
                        }
                    }
                }
                MESSAGE_WRITE -> {
                }
                MESSAGE_READ -> {
                }
                MESSAGE_DEVICE_NAME -> {
                    // save the connected device's name
                    println("Device Store:" + msg.data.getString(DEVICE_NAME)!!)
                    mConnectedDeviceName = msg.data.getString(DEVICE_NAME)
                    //mDevicesStatus.onConnect(null, mConnectedDeviceName, msg.getData());
                    Toast.makeText(applicationContext,
                            "Connected to " + mConnectedDeviceName!!,
                            Toast.LENGTH_SHORT).show()
                }
                MESSAGE_TOAST -> Toast.makeText(applicationContext,
                        msg.data.getString(TOAST), Toast.LENGTH_SHORT)
                        .show()
                MESSAGE_CONNECTION_LOST    //蓝牙已断开连接
                -> {
                    mConnectedDeviceName = msg.data.getString(DEVICE_NAME)
                    //                    if (mDevicesStatus != null) {
                    //                        mDevicesStatus.onDisconnect(null, mConnectedDeviceName, msg.getData());
                    //                    }

                    println("Device connection was lost")
                    Toast.makeText(applicationContext, "Device connection was lost",
                            Toast.LENGTH_SHORT).show()
                }
                MESSAGE_UNABLE_CONNECT     //无法连接设备
                -> {
                    mConnectedDeviceName = msg.data.getString(DEVICE_NAME)
                    //                    if (mDevicesStatus != null) {
                    //                        mDevicesStatus.onDisconnect(null, mConnectedDeviceName, msg.getData());
                    //                    }
                    Toast.makeText(applicationContext, "Unable to connect device",
                            Toast.LENGTH_SHORT).show()
                }
            }
        }
    }

    // The on-click listener for all devices in the ListViews
    private val mDeviceClickListener = AdapterView.OnItemClickListener { av, v, arg2, arg3 ->
        // Cancel discovery because it's costly and we're about to connect
        mBluetoothAdapter!!.cancelDiscovery()
        // Get the device MAC address, which is the last 17 chars in the View
        val info = (v as TextView).text.toString()
        val noDevices = resources.getText(R.string.none_paired).toString()
        val noNewDevice = resources.getText(R.string.none_found).toString()
        Log.i("tag", info)

        if (info != noDevices && info != noNewDevice) {
            val address = info.substring(info.length - 17)
            println("aaaaaaaaaa  adddress   "+address)
            if (BluetoothAdapter.checkBluetoothAddress(address)) {
                val device = mBluetoothAdapter!!.getRemoteDevice(address)
                Log.i(TAG, "device.getAddress() : " + device.address)
                Preferences.setPreference(this@BluetoothScan, DEVICE_NAME, device.getAddress());
                // Attempt to connect to the device
                Log.i(TAG, "Preferences: " + Preferences.getPreference(this@BluetoothScan, DEVICE_NAME));
                println("Address DAta$address")
                //   mService!!.connect(device)

            }
        }
    }

    override fun onBackPressed() {
        //super.onBackPressed()
        setResult(Activity.RESULT_OK)
        finish()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_scan)

        //  priterterminadata=Gson().fromJson(Preferences.getPreference(this!!,"printerdata"),GetTerminalPrinter::class.java)
        logindata = Gson().fromJson(Preferences.getPreference(this!!, "logindata"), GetEmployeeLogin::class.java)

        appSettingData = Gson().fromJson(Preferences.getPreference(this!!, "AppSetting"), GetEmployeeAppSetting::class.java)
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter()

        // If the adapter is null, then Bluetooth is not supported
        if (mBluetoothAdapter == null) {
            Toast.makeText(this, "Bluetooth is not available", Toast.LENGTH_LONG).show()
        }
        //   mArrayListterminalregisterprinter.addAll(priterterminadata.get_printers);


        paired_devices.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        new_devices.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        tv_back.setOnClickListener {
            setResult(Activity.RESULT_OK)
            finish()
        }
        if (this.mBluetoothAdapter != null) {
            // Get a set of currently paired devices
            val pairedDevices = this.mBluetoothAdapter!!.bondedDevices

            mPairedDevicesArrayAdapter = PairedDeviceAdapter()
            paired_devices.adapter = mPairedDevicesArrayAdapter

            // If there are paired devices, add each one to the ArrayAdapter
            if (pairedDevices.size > 0) {
                findViewById<View>(R.id.title_paired_devices).visibility = View.VISIBLE
                for (device in pairedDevices) {

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
                        println("aaaaaaaaaa  adddress   "+device.address)
                        mArrayListPairedDevices.add(device.name + " " + device.address)
                        //  device_port = device.name +" "+device.address;
                        // Toast.makeText(this@BluetoothScan,"deviceport"+device_port,Toast.LENGTH_LONG).show()
                        Log.i("device type", device.type.toString())
                    }
                    mPairedDevicesArrayAdapter.notifyDataSetChanged()

                }
            } else {
                val noDevices = resources.getText(R.string.none_paired).toString()
                mArrayListPairedDevices.add(noDevices)
                mPairedDevicesArrayAdapter.notifyDataSetChanged()
            }
        }

        callEmployeeTerminalPrinter()


        mNewDevicesArrayAdapter = ScanDeviceAdapter()
        new_devices.adapter = mNewDevicesArrayAdapter

        button_scan.setOnClickListener { v ->
            if (mBluetoothAdapter != null) {
                if (!mBluetoothAdapter!!.isEnabled) {
                    val enableIntent = Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE)
                    startActivityForResult(enableIntent, REQUEST_ENABLE_BT)
                    // Otherwise, setup the session
                } else {
                    //                        dialog = CommonUtils.createDialog(FragmentScan.this);
                    doDiscovery()
                    v.visibility = View.VISIBLE
                    //  v.visibility = View.GONE
                }
            } else {
                Toast.makeText(this@BluetoothScan, "Bluetooth is not available",
                        Toast.LENGTH_LONG).show()
            }
        }

    }

    protected fun checkLocationPermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this,
                    arrayOf(Manifest.permission.ACCESS_COARSE_LOCATION),
                    8080)
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int,
                                            permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            8080 -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    doDiscovery()
                } else {
                    //TODO re-request
                }
            }
        }
    }

    override fun onStop() {
        super.onStop()
        if (mService != null)
            mService!!.stop()
    }

    public override fun onStart() {
        super.onStart()
        checkLocationPermission()
        // If Bluetooth is not on, request that it be enabled.
        // setupChat() will then be called during onActivityResult
        //7/12/2018 put comment
//        if (mBluetoothAdapter != null) {
//            if (!mBluetoothAdapter!!.isEnabled) {
//                val enableIntent = Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE)
//                startActivityForResult(enableIntent, REQUEST_ENABLE_BT)
//                // Otherwise, setup the session
//            } else {
//                if (mService == null)
//                    mService = BluetoothService(this, mHandler)
//                doDiscovery()
//            }
//        }
//        // Register for broadcasts when a device is discovered
//        var filter = IntentFilter(BluetoothDevice.ACTION_FOUND)
//        this.registerReceiver(mReceiver, filter)
//
//        // Register for broadcasts when discovery has finished
//        filter = IntentFilter(BluetoothAdapter.ACTION_DISCOVERY_FINISHED)
//        this.registerReceiver(mReceiver, filter)
    }


    public override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (DEBUG)
            Log.d(TAG, "onActivityResult $resultCode$requestCode")
        when (requestCode) {
            REQUEST_ENABLE_BT -> {
                // When the request to enable Bluetooth returns
                if (resultCode == Activity.RESULT_OK) {
                    doDiscovery()
                    // Bluetooth is now enabled, so set up a session
                    //onStart()
                } else {
                    // User did not enable Bluetooth or an error occured
                    Log.d(TAG, "BT not enabled")
                    Toast.makeText(this@BluetoothScan, R.string.bt_not_enabled_leaving,
                            Toast.LENGTH_SHORT).show()

                }
            }
        }
    }


    @Synchronized
    public override fun onResume() {
        super.onResume()
        if (mService != null) {
            if (mService!!.state == BluetoothService.STATE_NONE) {
                // Start the Bluetooth services
                mService!!.start()
            }
        }
    }

    /**
     * Start device discover with the BluetoothAdapter
     */
    private fun doDiscovery() {
        if (DEBUG)
            Log.d(TAG, "doDiscovery()")

        // Indicate scanning in the title


        // Turn on sub-title for new devices
        findViewById<View>(R.id.title_new_devices).visibility = View.VISIBLE

        // If we're already discovering, stop it
        if (mBluetoothAdapter!!.isDiscovering) {
            mBluetoothAdapter!!.cancelDiscovery()
        }
        mArrayListScanDevices.clear()//20160617
        //  mPairedDevicesArrayAdapter.clear();//20160617
        // Request discover from BluetoothAdapter
        mBluetoothAdapter!!.startDiscovery()
    }

    public override fun onDestroy() {
        super.onDestroy()

        if (mBluetoothAdapter != null) {
            mBluetoothAdapter!!.cancelDiscovery()
        }

        // Unregister broadcast listeners
        //   unregisterReceiver(mReceiver)

    }

    inner class PairedDeviceAdapter : RecyclerView.Adapter<PairedDeviceAdapter.PairDeviceViewHolder>() {


        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PairDeviceViewHolder {
            val view = LayoutInflater.from(parent.context).inflate(R.layout.raw_devicelist1, parent, false)
            return PairDeviceViewHolder(view)
        }

        override fun onBindViewHolder(holder: PairDeviceViewHolder, position: Int) {

            holder.mTextViewPairDeviceName.text = mArrayListPairedDevices[position]

            if (mArrayListPairedDevices[position].equals(resources.getText(R.string.none_paired).toString(), ignoreCase = true)) {
                holder.mCheckBoxPairDevice.visibility = View.GONE
            } else {
                holder.mCheckBoxPairDevice.visibility = View.VISIBLE
            }

            //   Log.i("string",device_port)
            //   Log.i("string",newstring)

            //    Toast.makeText(this@BluetoothScan,"printerdata "+ newstring,Toast.LENGTH_LONG).show()
            System.out.println("FragmentScan" + Preferences.getPreference(this@BluetoothScan, DEVICE_NAME) + " Device Name"
                    + mConnectedDeviceName + "DEvice" + mArrayListPairedDevices.get(position));

            if (Preferences.getPreference(this@BluetoothScan, DEVICE_NAME) != "" && mConnectedDeviceName != "") {
                if (mArrayListPairedDevices.get(position).contains(Preferences.getPreference(this@BluetoothScan, DEVICE_NAME))) {
                    System.out.println("Checked")
                    holder.mCheckBoxPairDevice.setChecked(true)
                } else {
                    System.out.println("UnChecked")
                    holder.mCheckBoxPairDevice.setChecked(false)
                }
            }

            /* if(mArrayListPairedDevices.size>0){
                 Log.d("SSSASASA","SASAS")*/
            if (mArrayListPairedDevices[position].equals("No matching devices")) {
                holder.edit.visibility = View.GONE
            } else {
                holder.edit.visibility = View.VISIBLE
            }
            //Log.d("SSSASASA","SASAS"+mArrayListPairedDevices[position])
            //  holder.edit.visibility=View.VISIBLE

            /* }else{
                 Log.d("SSSASASA11","SASAS")
                 holder.edit.visibility=View.GONE

             }*/
            holder.edit.setOnClickListener {
                dialogPrinterRegisterTemp = Dialog(this@BluetoothScan, R.style.TransparentProgressDialog)
                val policy = StrictMode.ThreadPolicy.Builder().permitAll().build()
                StrictMode.setThreadPolicy(policy)
                //dialog_changepass.window.setLayout(resources.getDimensionPixelSize(R.dimen._10sdp) ,ViewGroup.LayoutParams.WRAP_CONTENT)
                dialogPrinterRegisterTemp.requestWindowFeature(Window.FEATURE_NO_TITLE)
                dialogPrinterRegisterTemp.setContentView(R.layout.dialog_printerregister1)
                dialogPrinterRegisterTemp.window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)
                dialogPrinterRegisterTemp.show()
                val lp = WindowManager.LayoutParams();
                lp.copyFrom(dialogPrinterRegisterTemp.getWindow().getAttributes());
                lp.width = WindowManager.LayoutParams.MATCH_PARENT
                lp.height = WindowManager.LayoutParams.WRAP_CONTENT
                lp.gravity = Gravity.CENTER;

                dialogPrinterRegisterTemp.getWindow().setAttributes(lp)
                //     dialogPrinterRegister.getWindow().setLayout(500, 500)

                val rb_recepprinter: TextView = dialogPrinterRegisterTemp.findViewById(R.id.rb_recepprinter)
                val rb_lblprinter: TextView = dialogPrinterRegisterTemp.findViewById(R.id.rb_lblprinter)
                rb_recepprinter.setOnClickListener {
                    printertype = 1
                    callEmployeePrinterRegister(printertype!!)
                    dialogPrinterRegisterTemp.dismiss()
                    //   val selectedId = rgprinter.getCheckedRadioButtonId()
                    // val rgtype: RadioButton = dialogPrinterRegister.findViewById(selectedId);
                    //       Toast.makeText(this@BluetoothScan,"selctiontype"+rgtype.text,Toast.LENGTH_LONG).show()
//                    if (rgtype.text == "Recepit Printer") {
//                        printertype = 1
//                    } else if (rgtype.text == "Label Printer") {
//                        printertype = 2
//
//
//                    }
               //     callEmployeePrinterRegister(printertype!!)
                }
                rb_lblprinter.setOnClickListener {
                    printertype = 2
                    callEmployeePrinterRegister(printertype!!)
                    dialogPrinterRegisterTemp.dismiss()
                }

            }
            holder.itemView.setOnClickListener {

                newstring = mArrayListPairedDevices.get(position).replace("\n", " ")
                 yourArray = mArrayListPairedDevices.get(position).split(" ") as ArrayList<String>
                println("aaaaaaaaaaaaa   printer  "+yourArray.get(1)+"  newstring "+newstring)
                if (mArrayListterminalregisterprinter.size > 0) {
                    for (i in mArrayListterminalregisterprinter.indices) {
                        mArrayListterminalregisterprinter.get(i)
                        val mDeviceString: List<String> = mArrayListPairedDevices.get(position).split("\n")
                        if (mDeviceString.size > 0) {

                            if (mArrayListterminalregisterprinter[i].hardware_manufacturer.equals(newstring)) {
                                if (mArrayListterminalregisterprinter[i].hardware_status!!.equals("1", false)) {

                                    if (mArrayListterminalregisterprinter.get(i).hardware_type_id!!.equals("1", false)) {
                                        Toast.makeText(this@BluetoothScan, "Connected As Recepit Printer", Toast.LENGTH_LONG).show()
                                        Preferences.setPreference(this@BluetoothScan, DEVICE_NAME, mArrayListPairedDevices[position].split(" ")[1])

                                        //  holder.mTextViewConnected.visibility = View.VISIBLE
                                        //holder.mTextViewPairDeviceName.setText("Connected As Recepit Printer")
                                    } else {
                                        Toast.makeText(this@BluetoothScan, "Connected As Label Printer", Toast.LENGTH_LONG).show()
                                        // holder.mTextViewPairDeviceName.setText("Connected As Label Printer")
                                        // holder.mTextViewConnected.visibility = View.VISIBLE
                                    }
                                }
                            }

//                        }                            }
//                        if (mArrayListterminalregisterprinter[i].hardware_status!!.equals("1",false)) {
//                            if (mArrayListterminalregisterprinter.get(i).hardware_type_id!!.equals("1",false)) {
//                                Toast.makeText(this@BluetoothScan, "Connected As Recepit Printer", Toast.LENGTH_LONG).show()
//                                //  holder.mTextViewConnected.visibility = View.VISIBLE
//                                //holder.mTextViewPairDeviceName.setText("Connected As Recepit Printer")
//                            } else {
//                                Toast.makeText(this@BluetoothScan, "Connected As Label Printer", Toast.LENGTH_LONG).show()
//                                // holder.mTextViewPairDeviceName.setText("Connected As Label Printer")
//                                // holder.mTextViewConnected.visibility = View.VISIBLE
//                            }
//                        }
                        }
                    }
                } else {
                    //holder.mTextViewConnected.visibility = View.GONE
                    //if not find
                    dialogPrinterRegister = Dialog(this@BluetoothScan, R.style.TransparentProgressDialog)
                    val policy = StrictMode.ThreadPolicy.Builder().permitAll().build()
                    StrictMode.setThreadPolicy(policy)
                    //dialog_changepass.window.setLayout(resources.getDimensionPixelSize(R.dimen._10sdp) ,ViewGroup.LayoutParams.WRAP_CONTENT)
                    dialogPrinterRegister.requestWindowFeature(Window.FEATURE_NO_TITLE)
                    dialogPrinterRegister.setContentView(R.layout.dialog_printerregister)
                    dialogPrinterRegister.window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)
                    dialogPrinterRegister.show()
                    val lp = WindowManager.LayoutParams();
                    lp.copyFrom(dialogPrinterRegister.getWindow().getAttributes());
                    lp.width = WindowManager.LayoutParams.MATCH_PARENT;
                    lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                    lp.gravity = Gravity.CENTER;

                    dialogPrinterRegister.getWindow().setAttributes(lp);
                    //     dialogPrinterRegister.getWindow().setLayout(500, 500)

                    val txtregister: TextView = dialogPrinterRegister.findViewById(R.id.tv_printregister);
                    val rgprinter: RadioGroup = dialogPrinterRegister.findViewById(R.id.rg_printertype);

                    txtregister.setOnClickListener {
                        val selectedId = rgprinter.getCheckedRadioButtonId()
                        val rgtype: RadioButton = dialogPrinterRegister.findViewById(selectedId);
                        //       Toast.makeText(this@BluetoothScan,"selctiontype"+rgtype.text,Toast.LENGTH_LONG).show()
                        if (rgtype.text == "Recepit Printer") {
                            printertype = 1;
                        } else if (rgtype.text == "Label Printer") {
                            printertype = 2;

                        }
                        callEmployeePrinterRegister(printertype!!)
                        dialogPrinterRegister.dismiss()
                    }


                }
            }

            //temporay put in comment call  for register u connect with label or recepit
            //   Toast.makeText(this@BluetoothScan,"call",Toast.LENGTH_LONG).show()

//                if (holder.mCheckBoxPairDevice.isChecked)
//                {
//                    holder.mCheckBoxPairDevice.isChecked = false
//                }
//                else {

//                    val dialog =  Dialog(application);
//                    dialog.getWindow().setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT));
//                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//                    dialog.setCancelable(false);
//                    dialog.setContentView(R.layout.dialog_printerregister);
//                    val txtregister :TextView=dialog.findViewById(R.id.tv_printregister);
//                   // val textinamt :TextView=dialog.findViewById(R.id.tv_paybleamt);
//                    txtregister.setOnClickListener {
//
//                    }
//                    dialog.show();


//                    holder.mCheckBoxPairDevice.isChecked = true
//                    mBluetoothAdapter!!.cancelDiscovery()
//                    // Get the device MAC address, which is the last 17 chars in the View
//                    val info = mArrayListPairedDevices[position]
//                    val noDevices = resources.getText(R.string.none_paired).toString()
//                    val noNewDevice = resources.getText(R.string.none_found).toString()
//                    Log.i("tag", info)
//
//                    if (info != noDevices && info != noNewDevice) {
//                        val address = info.substring(info.length - 17)
//                        if (BluetoothAdapter.checkBluetoothAddress(address)) {
//                            val device = mBluetoothAdapter!!
//                                    .getRemoteDevice(address)
//                            Log.i(TAG, "device.getAddress() : " + device.address)
//                            Preferences.setPreference(this@BluetoothScan, DEVICE_NAME, device.getAddress());
//                            // Attempt to connect to the device
//                            Log.i(TAG, "Preferences: " + Preferences.getPreference(this@BluetoothScan, DEVICE_NAME));
//                            // Attempt to connect to the device
//
//                            println("Address DAta$address")
//                            mService!!.connect(device)
//                        }
//                    }
//

            //  }
        }


        override fun getItemCount(): Int {
            return mArrayListPairedDevices.size
        }

        inner class PairDeviceViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

            internal var mTextViewPairDeviceName: TextView
            //internal var mTextViewConnected: TextView
            internal var mCheckBoxPairDevice: CheckBox
            internal var edit: TextView


            init {

                mCheckBoxPairDevice = itemView.findViewById<View>(R.id.chk_devicecheck) as CheckBox
                mTextViewPairDeviceName = itemView.findViewById<View>(R.id.txt_devicename) as TextView
                edit = itemView.findViewById<View>(R.id.tv_edit) as TextView
                //   mTextViewConnected=itemView.findViewById<View>(R.id.txt_connected) as TextView

            }


        }
    }

    inner class ScanDeviceAdapter : RecyclerView.Adapter<ScanDeviceAdapter.ScanDeviceViewHolder>() {


        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ScanDeviceViewHolder {
            val view = LayoutInflater.from(parent.context).inflate(R.layout.raw_devicelist, parent, false)
            return ScanDeviceViewHolder(view)
        }

        override fun onBindViewHolder(holder: ScanDeviceViewHolder, position: Int) {

            holder.mTextViewScanDeviceName.text = mArrayListScanDevices[position]

            if (mArrayListScanDevices[position].equals(resources.getText(R.string.none_found).toString(), ignoreCase = true)) {
                holder.mCheckBoxScanDevice.visibility = View.GONE
            } else {
                holder.mCheckBoxScanDevice.visibility = View.VISIBLE
            }

            if (Preferences.getPreference(this@BluetoothScan, DEVICE_NAME) != "" && mConnectedDeviceName != "") {
                if (mArrayListScanDevices.get(position).contains(Preferences.getPreference(this@BluetoothScan, DEVICE_NAME))) {
                    holder.mCheckBoxScanDevice.setChecked(true);
                } else {
                    holder.mCheckBoxScanDevice.setChecked(false);
                }
            }


            holder.itemView.setOnClickListener {
                if (holder.mCheckBoxScanDevice.isChecked) {
                    holder.mCheckBoxScanDevice.isChecked = false
                } else {
                    holder.mCheckBoxScanDevice.isChecked = true
                    mBluetoothAdapter!!.cancelDiscovery()
                    // Get the device MAC address, which is the last 17 chars in the View
                    val info = mArrayListScanDevices[position]
                    val noDevices = resources.getText(R.string.none_paired).toString()
                    val noNewDevice = resources.getText(R.string.none_found).toString()
                    Log.i("tag", info)

                    if (info != noDevices && info != noNewDevice) {
                        val address = info.substring(info.length - 17)
                        if (BluetoothAdapter.checkBluetoothAddress(address)) {
                            val device = mBluetoothAdapter!!
                                    .getRemoteDevice(address)
                            // Attempt to connect to the device
                            Preferences.setPreference(this@BluetoothScan, DEVICE_NAME, device.getAddress());
                            // Attempt to connect to the device
                            Log.i(TAG, "Preferences: " + Preferences.getPreference(this@BluetoothScan, DEVICE_NAME));
                            // Attempt to connect to the device

                            println("Address DAta$address")
                            println("Address Data$address")
                            println("aaaaaaaaaa  adddress   "+address)
                            //     mService!!.connect(device)
                        }
                    }

                }
            }

        }

        override fun getItemCount(): Int {
            return mArrayListScanDevices.size
        }

        inner class ScanDeviceViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

            internal var mTextViewScanDeviceName: TextView
            internal var mCheckBoxScanDevice: CheckBox

            init {
                mCheckBoxScanDevice = itemView.findViewById<View>(R.id.chk_devicecheck) as CheckBox
                mTextViewScanDeviceName = itemView.findViewById<View>(R.id.txt_devicename) as TextView

            }


        }
    }

    companion object {

        var EXTRA_DEVICE_ADDRESS = "device_address"

        var mConnectedDeviceName: String? = ""
    }

    fun callEmployeePrinterRegister(printertype: Int) {
        // Toast.makeText(this@BluetoothScan, "registerapi call", Toast.LENGTH_LONG).show()
        var regisdata: GetPrinterRegister? = null
        if (CUC.isNetworkAvailablewithPopup(this@BluetoothScan)) {
            mDialog = CUC.createDialog(this@BluetoothScan)
            val apiService = RequetsInterface.create()
            val parameterMap = HashMap<String, String>()

            if (logindata != null) {
                parameterMap["api_key"] = "${Preferences.getPreference(this!!, "API_KEY")}"
                parameterMap["store_id"] = logindata!!.user_id
            }
            if (appSettingData != null) {
                //  parameterMap["store_id"]="${}"
                parameterMap["terminal_id"] = "${appSettingData!!.terminal_id}"
            }
           // println("aaaaaaaaaaaaa   printer  "+yourArray.get(1)+"  newstring "+newstring.toString())
           // parameterMap["printer_port"] = newstring.toString()
            parameterMap["printer_port"] = "NTPL Label Print"
            parameterMap["printer_type"] = printertype.toString()
            // parameterMap[""]
            println("aaaaaaaaaaaa   parameters "+parameterMap)
            Log.i("Result", "parameterregister : $parameterMap")
            CompositeDisposable().add(apiService.callEmployeePrinterRegister(parameterMap)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe({ result ->
                        if (mDialog != null && mDialog.isShowing)
                            mDialog.dismiss()
                        println("aaaaaaaaaaa    "+result.toString())
                        Log.i("Resultprir", "" + result.toString())
                        if (result != null)
                            callEmployeeTerminalPrinter()
                            Toast.makeText(this@BluetoothScan, result!!.message.toString(), Toast.LENGTH_LONG).show()
                       /* if(dialogPrinterRegister!=null){
                            dialogPrinterRegister.dismiss()
                        }*/



                        //CUC.displayToast(this@BluetoothScan, result!!.show_status.toString(), result!!.message.toString())
                        //   regisdata = Gson().fromJson(Preferences.getPreference(this!!, "printerdata"), GetPrinterRegister::class.java)
                        //   CUC.displayToast(this,"printetres",result.toString())

                    }, { error ->
                        mDialog.dismiss()
                        error.printStackTrace()
                        println("aaaaaaaaaaa  error  "+error.message)
                    })
            )
            //}

        }
    }

    fun callEmployeeTerminalPrinter() {


        if (CUC.isNetworkAvailablewithPopup(this@BluetoothScan)) {
            mDialog = CUC.createDialog(this@BluetoothScan)
            var printerdata: GetTerminalPrinter? = null
            val apiService = RequetsInterface.create()
            val parameterMap = java.util.HashMap<String, String>()


            if (logindata != null) {
                parameterMap["api_key"] = "${Preferences.getPreference(this@BluetoothScan, "API_KEY")}"
                parameterMap["emp_id"] = logindata!!.user_id
            }

            if (appSettingData != null) {
                //  parameterMap["store_id"]="${}"
                parameterMap["terminal_id"] = "${appSettingData!!.terminal_id}"
            }
            Log.i("Result", "parameterMapprinter : $parameterMap")
            CompositeDisposable().add(apiService.callEmployeeTerminalPrinter(parameterMap)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe({ result ->
                        if (mDialog != null && mDialog.isShowing)
                            mDialog.dismiss()
                        Log.i("Resultprinter", "" + result.toString())
                        Log.d("PrinterSize", " = " + result.get_printers.size)
                        if (result.get_printers.size > 0) {
                            mArrayListterminalregisterprinter.clear()
                            mArrayListterminalregisterprinter.addAll(result.get_printers)

                            Log.d(TAG, "PrinterSize" + mArrayListterminalregisterprinter.size)
                        }
                        // printerdata=  Gson().fromJson(Preferences.getPreference(applicationContext, "printerdata"),GetTerminalPrinter::class.java)
                        //   CUC.displayToast(this,"printetres",result.toString())


                    }, { error ->
                        error.printStackTrace()
                        mDialog.dismiss()
                        println("aaaaaaaaaaa  error1111111  "+error.message)
                    })
            )

        }
    }


}

