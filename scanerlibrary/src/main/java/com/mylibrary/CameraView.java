package com.mylibrary;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Rect;
import android.hardware.Camera;
import android.support.annotation.ColorInt;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;

public class CameraView extends FrameLayout implements Camera.PreviewCallback {
    private CWC mCWC;
    private CameraSurface mPreview;
    private IVF mViewFinderView;
    private Rect mFramingRectInPreview;
    private CameraHandler mCameraHandler;
    private Boolean mFlashState;
    private boolean mAutofocusState = true;
    private boolean mShouldScaleToFill = true;
    private boolean mIsLaserEnabled = true;
    @ColorInt
    private int mLaserColor;
    @ColorInt
    private int mBorderColor;
    private int mMaskColor;
    private int mBorderWidth;
    private int mBorderLength;
    private boolean mRoundedCorner;
    private int mCornerRadius;
    private boolean mSquaredFinder;
    private float mBorderAlpha;
    private int mViewFinderOffset;
    private float mAspectTolerance;

    public CameraView(Context context) {
        super(context);
        this.mLaserColor = this.getResources().getColor(R.color.viewfinder_laser);
        this.mBorderColor = this.getResources().getColor(R.color.viewfinder_border);
        this.mMaskColor = this.getResources().getColor(R.color.viewfinder_mask);
        this.mBorderWidth = this.getResources().getInteger(R.integer.viewfinder_border_width);
        this.mBorderLength = this.getResources().getInteger(R.integer.viewfinder_border_length);
        this.mRoundedCorner = false;
        this.mCornerRadius = 0;
        this.mSquaredFinder = false;
        this.mBorderAlpha = 1.0F;
        this.mViewFinderOffset = 0;
        this.mAspectTolerance = 0.1F;
        this.init();
    }

    public CameraView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.mLaserColor = this.getResources().getColor(R.color.viewfinder_laser);
        this.mBorderColor = this.getResources().getColor(R.color.viewfinder_border);
        this.mMaskColor = this.getResources().getColor(R.color.viewfinder_mask);
        this.mBorderWidth = this.getResources().getInteger(R.integer.viewfinder_border_width);
        this.mBorderLength = this.getResources().getInteger(R.integer.viewfinder_border_length);
        this.mRoundedCorner = false;
        this.mCornerRadius = 0;
        this.mSquaredFinder = false;
        this.mBorderAlpha = 1.0F;
        this.mViewFinderOffset = 0;
        this.mAspectTolerance = 0.1F;
        TypedArray a = context.getTheme().obtainStyledAttributes(attributeSet, R.styleable.CameraScannerView, 0, 0);

        try {
            this.setShouldScaleToFill(a.getBoolean(R.styleable.CameraScannerView_shouldScaleToFill, true));
            this.mIsLaserEnabled = a.getBoolean(R.styleable.CameraScannerView_laserEnabled, this.mIsLaserEnabled);
            this.mLaserColor = a.getColor(R.styleable.CameraScannerView_laserColor, this.mLaserColor);
            this.mBorderColor = a.getColor(R.styleable.CameraScannerView_borderColor, this.mBorderColor);
            this.mMaskColor = a.getColor(R.styleable.CameraScannerView_maskColor, this.mMaskColor);
            this.mBorderWidth = a.getDimensionPixelSize(R.styleable.CameraScannerView_borderWidth, this.mBorderWidth);
            this.mBorderLength = a.getDimensionPixelSize(R.styleable.CameraScannerView_borderLength, this.mBorderLength);
            this.mRoundedCorner = a.getBoolean(R.styleable.CameraScannerView_roundedCorner, this.mRoundedCorner);
            this.mCornerRadius = a.getDimensionPixelSize(R.styleable.CameraScannerView_cornerRadius, this.mCornerRadius);
            this.mSquaredFinder = a.getBoolean(R.styleable.CameraScannerView_squaredFinder, this.mSquaredFinder);
            this.mBorderAlpha = a.getFloat(R.styleable.CameraScannerView_borderAlpha, this.mBorderAlpha);
            this.mViewFinderOffset = a.getDimensionPixelSize(R.styleable.CameraScannerView_finderOffset, this.mViewFinderOffset);
        } finally {
            a.recycle();
        }

        this.init();
    }

    private void init() {
        this.mViewFinderView = this.createViewFinderView(this.getContext());
    }

    public final void setupLayout(CWC CWC) {
        this.removeAllViews();
        this.mPreview = new CameraSurface(this.getContext(), CWC, this);
        this.mPreview.setAspectTolerance(this.mAspectTolerance);
        this.mPreview.setShouldScaleToFill(this.mShouldScaleToFill);
        if (!this.mShouldScaleToFill) {
            RelativeLayout relativeLayout = new RelativeLayout(this.getContext());
            relativeLayout.setGravity(Gravity.CENTER);
            relativeLayout.setBackgroundColor(-16777216);
            relativeLayout.addView(this.mPreview);
            this.addView(relativeLayout);
        } else {
            this.addView(this.mPreview);
        }

        if (this.mViewFinderView instanceof View) {
            this.addView((View)this.mViewFinderView);
        } else {
            throw new IllegalArgumentException("IViewFinder object returned by 'createViewFinderView()' should be instance of android.view.View");
        }
    }

    protected IVF createViewFinderView(Context context) {
        VFC VFC = new VFC(context);
        VFC.setBorderColor(this.mBorderColor);
        VFC.setLaserColor(this.mLaserColor);
        VFC.setLaserEnabled(this.mIsLaserEnabled);
        VFC.setBorderStrokeWidth(this.mBorderWidth);
        VFC.setBorderLineLength(this.mBorderLength);
        VFC.setMaskColor(this.mMaskColor);
        VFC.setBorderCornerRounded(this.mRoundedCorner);
        VFC.setBorderCornerRadius(this.mCornerRadius);
        VFC.setSquareViewFinder(this.mSquaredFinder);
        VFC.setViewFinderOffset(this.mViewFinderOffset);
        return VFC;
    }

    public void setLaserColor(int laserColor) {
        this.mLaserColor = laserColor;
        this.mViewFinderView.setLaserColor(this.mLaserColor);
        this.mViewFinderView.setupViewFinder();
    }

    public void setMaskColor(int maskColor) {
        this.mMaskColor = maskColor;
        this.mViewFinderView.setMaskColor(this.mMaskColor);
        this.mViewFinderView.setupViewFinder();
    }

    public void setBorderColor(int borderColor) {
        this.mBorderColor = borderColor;
        this.mViewFinderView.setBorderColor(this.mBorderColor);
        this.mViewFinderView.setupViewFinder();
    }

    public void setBorderStrokeWidth(int borderStrokeWidth) {
        this.mBorderWidth = borderStrokeWidth;
        this.mViewFinderView.setBorderStrokeWidth(this.mBorderWidth);
        this.mViewFinderView.setupViewFinder();
    }

    public void setBorderLineLength(int borderLineLength) {
        this.mBorderLength = borderLineLength;
        this.mViewFinderView.setBorderLineLength(this.mBorderLength);
        this.mViewFinderView.setupViewFinder();
    }

    public void setLaserEnabled(boolean isLaserEnabled) {
        this.mIsLaserEnabled = isLaserEnabled;
        this.mViewFinderView.setLaserEnabled(this.mIsLaserEnabled);
        this.mViewFinderView.setupViewFinder();
    }

    public void setIsBorderCornerRounded(boolean isBorderCornerRounded) {
        this.mRoundedCorner = isBorderCornerRounded;
        this.mViewFinderView.setBorderCornerRounded(this.mRoundedCorner);
        this.mViewFinderView.setupViewFinder();
    }

    public void setBorderCornerRadius(int borderCornerRadius) {
        this.mCornerRadius = borderCornerRadius;
        this.mViewFinderView.setBorderCornerRadius(this.mCornerRadius);
        this.mViewFinderView.setupViewFinder();
    }

    public void setSquareViewFinder(boolean isSquareViewFinder) {
        this.mSquaredFinder = isSquareViewFinder;
        this.mViewFinderView.setSquareViewFinder(this.mSquaredFinder);
        this.mViewFinderView.setupViewFinder();
    }

    public void setBorderAlpha(float borderAlpha) {
        this.mBorderAlpha = borderAlpha;
        this.mViewFinderView.setBorderAlpha(this.mBorderAlpha);
        this.mViewFinderView.setupViewFinder();
    }

    public void startCamera(int cameraId) {
        if (this.mCameraHandler == null) {
            this.mCameraHandler = new CameraHandler(this);
        }

        this.mCameraHandler.startCamera(cameraId);
    }

    public void setupCameraPreview(CWC CWC) {
        this.mCWC = CWC;
        if (this.mCWC != null) {
            this.setupLayout(this.mCWC);
            this.mViewFinderView.setupViewFinder();
            if (this.mFlashState != null) {
                this.setFlash(this.mFlashState);
            }

            this.setAutoFocus(this.mAutofocusState);
        }

    }

    public void startCamera() {
        this.startCamera(CUC.getDefaultCameraId());
    }

    public void stopCamera() {
        if (this.mCWC != null) {
            this.mPreview.stopCameraPreview();
            this.mPreview.setCamera((CWC)null, (Camera.PreviewCallback)null);
            this.mCWC.mCamera.release();
            this.mCWC = null;
        }

        if (this.mCameraHandler != null) {
            this.mCameraHandler.quit();
            this.mCameraHandler = null;
        }

    }

    public void stopCameraPreview() {
        if (this.mPreview != null) {
            this.mPreview.stopCameraPreview();
        }

    }

    protected void resumeCameraPreview() {
        if (this.mPreview != null) {
            this.mPreview.showCameraPreview();
        }

    }

    public synchronized Rect getFramingRectInPreview(int previewWidth, int previewHeight) {
        if (this.mFramingRectInPreview == null) {
            Rect framingRect = this.mViewFinderView.getFramingRect();
            int viewFinderViewWidth = this.mViewFinderView.getWidth();
            int viewFinderViewHeight = this.mViewFinderView.getHeight();
            if (framingRect == null || viewFinderViewWidth == 0 || viewFinderViewHeight == 0) {
                return null;
            }

            Rect rect = new Rect(framingRect);
            if (previewWidth < viewFinderViewWidth) {
                rect.left = rect.left * previewWidth / viewFinderViewWidth;
                rect.right = rect.right * previewWidth / viewFinderViewWidth;
            }

            if (previewHeight < viewFinderViewHeight) {
                rect.top = rect.top * previewHeight / viewFinderViewHeight;
                rect.bottom = rect.bottom * previewHeight / viewFinderViewHeight;
            }

            this.mFramingRectInPreview = rect;
        }

        return this.mFramingRectInPreview;
    }

    public void setFlash(boolean flag) {
        this.mFlashState = flag;
        if (this.mCWC != null && CUC.isFlashSupported(this.mCWC.mCamera)) {
            Camera.Parameters parameters = this.mCWC.mCamera.getParameters();
            if (flag) {
                if (parameters.getFlashMode().equals("torch")) {
                    return;
                }

                parameters.setFlashMode("torch");
            } else {
                if (parameters.getFlashMode().equals("off")) {
                    return;
                }

                parameters.setFlashMode("off");
            }

            this.mCWC.mCamera.setParameters(parameters);
        }

    }

    public boolean getFlash() {
        if (this.mCWC != null && CUC.isFlashSupported(this.mCWC.mCamera)) {
            Camera.Parameters parameters = this.mCWC.mCamera.getParameters();
            return parameters.getFlashMode().equals("torch");
        } else {
            return false;
        }
    }

    public void toggleFlash() {
        if (this.mCWC != null && CUC.isFlashSupported(this.mCWC.mCamera)) {
            Camera.Parameters parameters = this.mCWC.mCamera.getParameters();
            if (parameters.getFlashMode().equals("torch")) {
                parameters.setFlashMode("off");
            } else {
                parameters.setFlashMode("torch");
            }

            this.mCWC.mCamera.setParameters(parameters);
        }

    }

    public void setAutoFocus(boolean state) {
        this.mAutofocusState = state;
        if (this.mPreview != null) {
            this.mPreview.setAutoFocus(state);
        }

    }

    public void setShouldScaleToFill(boolean shouldScaleToFill) {
        this.mShouldScaleToFill = shouldScaleToFill;
    }

    public void setAspectTolerance(float aspectTolerance) {
        this.mAspectTolerance = aspectTolerance;
    }

    public byte[] getRotatedData(byte[] data, Camera camera) {
        Camera.Parameters parameters = camera.getParameters();
        Camera.Size size = parameters.getPreviewSize();
        int width = size.width;
        int height = size.height;
        int rotationCount = this.getRotationCount();
        if (rotationCount == 1 || rotationCount == 3) {
            for(int i = 0; i < rotationCount; ++i) {
                byte[] rotatedData = new byte[data.length];

                int y;
                for(y = 0; y < height; ++y) {
                    for(int x = 0; x < width; ++x) {
                        rotatedData[x * height + height - y - 1] = data[x + y * width];
                    }
                }

                data = rotatedData;
                y = width;
                width = height;
                height = y;
            }
        }

        return data;
    }

    public int getRotationCount() {
        int displayOrientation = this.mPreview.getDisplayOrientation();
        return displayOrientation / 90;
    }

    @Override
    public void onPreviewFrame(byte[] bytes, Camera camera) {

    }
}
