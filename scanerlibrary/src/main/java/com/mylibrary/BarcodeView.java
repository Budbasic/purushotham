package com.mylibrary;

import android.content.Context;
import android.graphics.Rect;
import android.hardware.Camera;
import android.os.Handler;
import android.os.Looper;
import android.util.AttributeSet;
import android.util.Log;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.BinaryBitmap;
import com.google.zxing.DecodeHintType;
import com.google.zxing.LuminanceSource;
import com.google.zxing.MultiFormatReader;
import com.google.zxing.NotFoundException;
import com.google.zxing.PlanarYUVLuminanceSource;
import com.google.zxing.ReaderException;
import com.google.zxing.Result;
import com.google.zxing.common.HybridBinarizer;

import java.util.ArrayList;
import java.util.Collection;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;


public class BarcodeView extends CameraView {
    private static final String TAG = "DevyScannerView";
    private MultiFormatReader mMultiFormatReader;
    public static final List<DevyBarcodeFormat> ALL_FORMATS = new ArrayList();
    private List<DevyBarcodeFormat> mFormats;
    private BarcodeView.ResultHandler mResultHandler;

    static {
        ALL_FORMATS.add(DevyBarcodeFormat.AZTEC);
        ALL_FORMATS.add(DevyBarcodeFormat.CODABAR);
        ALL_FORMATS.add(DevyBarcodeFormat.CODE_39);
        ALL_FORMATS.add(DevyBarcodeFormat.CODE_93);
        ALL_FORMATS.add(DevyBarcodeFormat.CODE_128);
        ALL_FORMATS.add(DevyBarcodeFormat.DATA_MATRIX);
        ALL_FORMATS.add(DevyBarcodeFormat.EAN_8);
        ALL_FORMATS.add(DevyBarcodeFormat.EAN_13);
        ALL_FORMATS.add(DevyBarcodeFormat.ITF);
        ALL_FORMATS.add(DevyBarcodeFormat.MAXICODE);
        ALL_FORMATS.add(DevyBarcodeFormat.PDF_417);
        ALL_FORMATS.add(DevyBarcodeFormat.QR_CODE);
        ALL_FORMATS.add(DevyBarcodeFormat.RSS_14);
        ALL_FORMATS.add(DevyBarcodeFormat.RSS_EXPANDED);
        ALL_FORMATS.add(DevyBarcodeFormat.UPC_A);
        ALL_FORMATS.add(DevyBarcodeFormat.UPC_E);
        ALL_FORMATS.add(DevyBarcodeFormat.UPC_EAN_EXTENSION);
    }

    public BarcodeView(Context context) {
        super(context);
        this.initMultiFormatReader();
    }

    public BarcodeView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.initMultiFormatReader();
    }

    public void setFormats(List<DevyBarcodeFormat> formats) {
        this.mFormats = formats;
        this.initMultiFormatReader();
    }

    public void setResultHandler(BarcodeView.ResultHandler resultHandler) {
        this.mResultHandler = resultHandler;
    }

    public Collection<DevyBarcodeFormat> getFormats() {
        return this.mFormats == null ? ALL_FORMATS : this.mFormats;
    }

    private void initMultiFormatReader() {
        Map<DecodeHintType, Object> hints = new EnumMap(DecodeHintType.class);
        hints.put(DecodeHintType.POSSIBLE_FORMATS, this.getFormats());
        this.mMultiFormatReader = new MultiFormatReader();
        this.mMultiFormatReader.setHints(hints);
    }

    public void onPreviewFrame(byte[] data, Camera camera) {
        if (this.mResultHandler != null) {
            try {
                Camera.Parameters parameters = camera.getParameters();
                Camera.Size size = parameters.getPreviewSize();
                int width = size.width;
                int height = size.height;
                if (DUC.getScreenOrientation(this.getContext()) == 1) {
                    int rotationCount = this.getRotationCount();
                    if (rotationCount == 1 || rotationCount == 3) {
                        int tmp = width;
                        width = height;
                        height = tmp;
                    }

                    data = this.getRotatedData(data, camera);
                }

                Result rawResult = null;
                PlanarYUVLuminanceSource source = this.buildLuminanceSource(data, width, height);
                if (source != null) {
                    BinaryBitmap bitmap = new BinaryBitmap(new HybridBinarizer(source));

                    try {
                        rawResult = this.mMultiFormatReader.decodeWithState(bitmap);
                    } catch (ReaderException var29) {
                        var29.printStackTrace();
                    } catch (NullPointerException var30) {
                        var30.printStackTrace();
                    } catch (ArrayIndexOutOfBoundsException var31) {
                        var31.printStackTrace();
                    } finally {
                        this.mMultiFormatReader.reset();
                    }

                    if (rawResult == null) {
                        LuminanceSource invertedSource = source.invert();
                        bitmap = new BinaryBitmap(new HybridBinarizer(invertedSource));

                        try {
                            rawResult = this.mMultiFormatReader.decodeWithState(bitmap);
                        } catch (NotFoundException var27) {
                            ;
                        } finally {
                            this.mMultiFormatReader.reset();
                        }
                    }
                }

                if (rawResult != null) {
                    Handler handler = new Handler(Looper.getMainLooper());
                    final Result finalRawResult = rawResult;
                    handler.post(new Runnable() {
                        public void run() {
                            BarcodeView.ResultHandler tmpResultHandler = BarcodeView.this.mResultHandler;
                            BarcodeView.this.mResultHandler = null;
                            BarcodeView.this.stopCameraPreview();
                            if (tmpResultHandler != null) {
                                DevyBarcodeFormat format = null;
                                if (finalRawResult.getBarcodeFormat() == BarcodeFormat.QR_CODE){
                                    format = DevyBarcodeFormat.QR_CODE;
                                }else if (finalRawResult.getBarcodeFormat() == BarcodeFormat.PDF_417){
                                    format = DevyBarcodeFormat.PDF_417;
                                }else if (finalRawResult.getBarcodeFormat() == BarcodeFormat.AZTEC){
                                    format = DevyBarcodeFormat.AZTEC;
                                }else if (finalRawResult.getBarcodeFormat() == BarcodeFormat.CODABAR){
                                    format = DevyBarcodeFormat.CODABAR;
                                }else if (finalRawResult.getBarcodeFormat() == BarcodeFormat.CODE_39){
                                    format = DevyBarcodeFormat.CODE_39;
                                }else if (finalRawResult.getBarcodeFormat() == BarcodeFormat.CODE_93){
                                    format = DevyBarcodeFormat.CODE_93;
                                }else if (finalRawResult.getBarcodeFormat() == BarcodeFormat.CODE_128){
                                    format = DevyBarcodeFormat.CODE_128;
                                }else if (finalRawResult.getBarcodeFormat() == BarcodeFormat.DATA_MATRIX){
                                    format = DevyBarcodeFormat.DATA_MATRIX;
                                }else if (finalRawResult.getBarcodeFormat() == BarcodeFormat.EAN_8){
                                    format = DevyBarcodeFormat.EAN_8;
                                }else if (finalRawResult.getBarcodeFormat() == BarcodeFormat.EAN_13){
                                    format = DevyBarcodeFormat.EAN_13;
                                }else if (finalRawResult.getBarcodeFormat() == BarcodeFormat.ITF){
                                    format = DevyBarcodeFormat.ITF;
                                }else if (finalRawResult.getBarcodeFormat() == BarcodeFormat.MAXICODE){
                                    format = DevyBarcodeFormat.MAXICODE;
                                }else if (finalRawResult.getBarcodeFormat() == BarcodeFormat.RSS_14){
                                    format = DevyBarcodeFormat.RSS_14;
                                }else if (finalRawResult.getBarcodeFormat() == BarcodeFormat.RSS_EXPANDED){
                                    format = DevyBarcodeFormat.RSS_EXPANDED;
                                }else if (finalRawResult.getBarcodeFormat() == BarcodeFormat.UPC_A){
                                    format = DevyBarcodeFormat.UPC_A;
                                }else if (finalRawResult.getBarcodeFormat() == BarcodeFormat.UPC_E){
                                    format = DevyBarcodeFormat.UPC_E;
                                }else if (finalRawResult.getBarcodeFormat() == BarcodeFormat.UPC_EAN_EXTENSION){
                                    format = DevyBarcodeFormat.UPC_EAN_EXTENSION;
                                }
                                ScanResult scanResult =
                                        new ScanResult(finalRawResult.getText(),
                                                finalRawResult.getRawBytes(),
                                                finalRawResult.getNumBits(),
                                                finalRawResult.getResultPoints(),
                                                format,
                                                finalRawResult.getTimestamp());
                                tmpResultHandler.handleResult(scanResult);
                            }
                        }
                    });
                } else {
                    camera.setOneShotPreviewCallback(this);
                }
            } catch (RuntimeException var33) {
                Log.e(TAG, var33.toString(), var33);
            }

        }
    }

    public void resumeCameraPreview(BarcodeView.ResultHandler resultHandler) {
        this.mResultHandler = resultHandler;
        super.resumeCameraPreview();
    }

    public PlanarYUVLuminanceSource buildLuminanceSource(byte[] data, int width, int height) {
        Rect rect = this.getFramingRectInPreview(width, height);
        if (rect == null) {
            return null;
        } else {
            PlanarYUVLuminanceSource source = null;

            try {
                source = new PlanarYUVLuminanceSource(data, width, height, rect.left, rect.top, rect.width(), rect.height(), false);
            } catch (Exception var7) {
                var7.printStackTrace();
            }
            return source;
        }
    }

    public interface ResultHandler {
        void handleResult(ScanResult var1);
    }
}