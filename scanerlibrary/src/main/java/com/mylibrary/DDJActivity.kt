package com.mylibrary

import android.app.Activity
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.Window
import org.json.JSONObject


/**
 * Created by Dev009 on 9/15/2017 15.
 */

class DDJActivity : AppCompatActivity(), BarcodeView.ResultHandler {

    var zxing_scanner: BarcodeView? = null
    var beepManager: BeepManager? = null

    public override fun onCreate(state: Bundle?) {
        super.onCreate(state)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        zxing_scanner = BarcodeView(this)
        beepManager = BeepManager(this)
        setContentView(zxing_scanner)
    }

    public override fun onResume() {
        super.onResume()
        if (beepManager == null) {
            beepManager = BeepManager(this)
        }
        zxing_scanner!!.setFormats(arrayListOf(DevyBarcodeFormat.PDF_417))
        zxing_scanner!!.setResultHandler(this) // Register ourselves as a handler for scan results.
        zxing_scanner!!.startCamera()          // Start camera on resume

    }

    public override fun onPause() {
        super.onPause()
        if (beepManager != null) {
            beepManager!!.close()
            beepManager = null
        }
        zxing_scanner!!.stopCamera()
        // Stop camera on pause
    }

    override fun onBackPressed() {
        setResult(Activity.RESULT_CANCELED)
        super.onBackPressed()
    }

    override fun handleResult(rawResult: ScanResult) {
        // Do something with the result here
        if (beepManager != null)
            beepManager!!.playBeepSoundAndVibrate()

        try {
            when (rawResult.barcodeFormat) {
                DevyBarcodeFormat.PDF_417 -> {
                    val rawR = rawResult.text.split("\n".toRegex())
                    val resultOBJ = JSONObject()
                    for (rData in rawR) {
                        Log.i("ScanQRCodexxx", "rData : $rData")
                        if (rData.length >= 3) {
                            resultOBJ.put("${rData.substring(0, 3)}", "${rData.substring(3, rData.length)}")
                        }

                    }
                    intent.putExtra("scannerResult", resultOBJ.toString())
                    zxing_scanner!!.stopCamera()
                    this.setResult(Activity.RESULT_OK, intent)
                    this.finish()
                    // If you would like to resume scanning, call this method below:
                    zxing_scanner!!.resumeCameraPreview(this)
                    Log.i("ScanQRCodexxx", "rData : $resultOBJ")
                }
                DevyBarcodeFormat.QR_CODE -> {
                    intent.putExtra("scannerQrResult", rawResult.text)
                    zxing_scanner!!.stopCamera()
                    this.setResult(Activity.RESULT_OK, intent)
                    this.finish()
                    // If you would like to resume scanning, call this method below:
                    zxing_scanner!!.resumeCameraPreview(this)
                }
            }
        } catch (ee: Exception) {
            ee.printStackTrace()
        }
    }
}